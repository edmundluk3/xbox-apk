.class public final Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;
.super Lcom/bumptech/glide/load/model/stream/BaseGlideUrlLoader;
.source "ResizeUrlImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;,
        Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bumptech/glide/load/model/stream/BaseGlideUrlLoader",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_DIMEN:I = 0x78

.field private static final DIMEN_POINT_CACHE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            ">;"
        }
    .end annotation
.end field

.field private static final EDS_IMAGES:Ljava/lang/String; = "images-eds"

.field private static final HEIGHT_PATTERN:Ljava/util/regex/Pattern;

.field private static final NETWORK_QUALITY_RES_THRESHOLD:I = 0x3840

.field private static final SQUARE_DIMEN_POINTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            ">;"
        }
    .end annotation
.end field

.field private static final STORE_IMAGE:Ljava/lang/String; = "store-images"

.field private static final TAG:Ljava/lang/String;

.field private static final TALL_DIMEN_POINTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            ">;"
        }
    .end annotation
.end field

.field private static final WIDE_DIMEN_POINTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            ">;"
        }
    .end annotation
.end field

.field private static final WIDTH_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x2d0

    const/16 v7, 0x1c0

    const/16 v6, 0x96

    const/16 v5, 0x78

    const/16 v4, 0x140

    .line 24
    const-class v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TAG:Ljava/lang/String;

    .line 29
    const-string v2, ".*w=[0-9]+.*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->WIDTH_PATTERN:Ljava/util/regex/Pattern;

    .line 30
    const-string v2, ".*h=[0-9]+.*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->HEIGHT_PATTERN:Ljava/util/regex/Pattern;

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x10e

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 43
    .local v1, "allDimenPoints":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    const/16 v2, 0x26

    const/16 v3, 0x26

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    const/16 v2, 0x2b

    const/16 v3, 0x40

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    const/16 v2, 0x2c

    const/16 v3, 0x2c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    const/16 v2, 0x28

    const/16 v3, 0x28

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    const/16 v2, 0x2e

    const/16 v3, 0x3c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    const/16 v2, 0x2e

    const/16 v3, 0x3f

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    const/16 v2, 0x34

    const/16 v3, 0x34

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    const/16 v2, 0x38

    const/16 v3, 0x38

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    const/16 v2, 0x32

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    const/16 v2, 0x3c

    const/16 v3, 0x28

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    const/16 v2, 0x3c

    const/16 v3, 0x2d

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    const/16 v2, 0x3c

    const/16 v3, 0x3c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    const/16 v2, 0x3c

    const/16 v3, 0x5a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    const/16 v2, 0x40

    const/16 v3, 0x30

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    const/16 v2, 0x40

    const/16 v3, 0x40

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    const/16 v2, 0x44

    const/16 v3, 0x26

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    const/16 v2, 0x48

    const/16 v3, 0x48

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    const/16 v2, 0x4b

    const/16 v3, 0x4b

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    const/16 v2, 0x50

    const/16 v3, 0x3c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    const/16 v2, 0x54

    const/16 v3, 0x38

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    const/16 v2, 0x54

    const/16 v3, 0x54

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    const/16 v2, 0x55

    const/16 v3, 0x55

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    const/16 v2, 0x55

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    const/16 v2, 0x56

    const/16 v3, 0x56

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const/16 v2, 0x5f

    const/16 v3, 0x5f

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const/16 v2, 0x63

    const/16 v3, 0x63

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    const/16 v2, 0x63

    const/16 v3, 0x87

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    const/16 v2, 0x64

    const/16 v3, 0x64

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    const/16 v2, 0x64

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    const/16 v2, 0x64

    const/16 v3, 0x87

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    const/16 v2, 0x69

    const/16 v3, 0x69

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    const/16 v2, 0x6b

    const/16 v3, 0xa0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const/16 v2, 0x6c

    const/16 v3, 0x48

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    const/16 v2, 0x6c

    const/16 v3, 0x6c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const/16 v2, 0x6e

    const/16 v3, 0x6e

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const/16 v2, 0x70

    const/16 v3, 0x54

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const/16 v2, 0x5a

    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-static {v5, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    const/16 v2, 0x7c

    const/16 v3, 0x73

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const/16 v2, 0x80

    const/16 v3, 0x80

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const/16 v2, 0x83

    const/16 v3, 0x83

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const/16 v2, 0x84

    const/16 v3, 0x84

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    const/16 v2, 0x84

    const/16 v3, 0xb5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    const/16 v2, 0x87

    const/16 v3, 0x87

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    const/16 v2, 0x88

    const/16 v3, 0x66

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const/16 v2, 0x8a

    const/16 v3, 0xd0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    const/16 v2, 0x90

    const/16 v3, 0x6c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const/16 v2, 0x91

    const/16 v3, 0xc1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    const/16 v2, 0x93

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    const/16 v2, 0x94

    const/16 v3, 0x94

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    const/16 v2, 0x54

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    const/16 v2, 0x67

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    const/16 v2, 0x86

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-static {v6, v6}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    const/16 v2, 0xe1

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    const/16 v2, 0x97

    const/16 v3, 0x97

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    const/16 v2, 0x98

    const/16 v3, 0xd0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const/16 v2, 0x9a

    const/16 v3, 0x74

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const/16 v2, 0x9c

    const/16 v3, 0xd0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const/16 v2, 0xa0

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const/16 v2, 0xa0

    const/16 v3, 0xa0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    const/16 v2, 0xa4

    const/16 v3, 0x73

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const/16 v2, 0xa4

    const/16 v3, 0xa4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const/16 v2, 0xaa

    const/16 v3, 0xaa

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const/16 v2, 0xac

    const/16 v3, 0x102

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const/16 v2, 0xad

    const/16 v3, 0xad

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    const/16 v2, 0xaf

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    const/16 v2, 0xb1

    const/16 v3, 0x85

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    const/16 v2, 0xb2

    const/16 v3, 0x64

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    const/16 v2, 0xb4

    const/16 v3, 0xb4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    const/16 v2, 0xb4

    const/16 v3, 0xf5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const/16 v2, 0xb5

    const/16 v3, 0x66

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    const/16 v2, 0xb5

    const/16 v3, 0xb5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    const/16 v2, 0xc8

    const/16 v3, 0xc8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    const/16 v2, 0xc8

    const/16 v3, 0x12c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    const/16 v2, 0xcf

    const/16 v3, 0xd2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    const/16 v2, 0xd0

    const/16 v3, 0xd0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    const/16 v2, 0xd2

    const/16 v3, 0xbc

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    const/16 v2, 0xd2

    const/16 v3, 0xcc

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    const/16 v2, 0xd2

    const/16 v3, 0xcf

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    const/16 v2, 0xd2

    const/16 v3, 0xd2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    const/16 v2, 0xd2

    const/16 v3, 0x13c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    const/16 v2, 0xd5

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const/16 v2, 0xd5

    const/16 v3, 0xd5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    const/16 v2, 0xd7

    const/16 v3, 0xd7

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    const/16 v2, 0xd7

    const/16 v3, 0x126

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    const/16 v2, 0xdb

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    const/16 v2, 0xdb

    const/16 v3, 0x12c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    const/16 v2, 0xe0

    const/16 v3, 0xe0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    const/16 v2, 0xe6

    const/16 v3, 0x13c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    const/16 v2, 0xea

    const/16 v3, 0xea

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    const/16 v2, 0xea

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    const/16 v2, 0xeb

    const/16 v3, 0xeb

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    const/16 v2, 0xed

    const/16 v3, 0x13c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    const/16 v2, 0xf0

    const/16 v3, 0xf0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    const/16 v2, 0xf3

    const/16 v3, 0x89

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    const/16 v2, 0xf5

    const/16 v3, 0xf5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    const/16 v2, 0xf8

    const/16 v3, 0xf8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    const/16 v2, 0xfd

    const/16 v3, 0xfd

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    const/16 v2, 0x102

    const/16 v3, 0xc2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    const/16 v2, 0x102

    const/16 v3, 0x102

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    const/16 v2, 0x104

    const/16 v3, 0x164

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    const/16 v2, 0x106

    const/16 v3, 0xeb

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    const/16 v2, 0x106

    const/16 v3, 0x106

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    const/16 v2, 0x109

    const/16 v3, 0x109

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    const/16 v2, 0x109

    const/16 v3, 0x16a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    const/16 v2, 0x10b

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    const/16 v2, 0x10e

    const/16 v3, 0x10e

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    const/16 v2, 0x113

    const/16 v3, 0xce

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const/16 v2, 0x116

    const/16 v3, 0x172

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    const/16 v2, 0x118

    const/16 v3, 0x118

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    const/16 v2, 0x11a

    const/16 v3, 0x1a8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    const/16 v2, 0x11d

    const/16 v3, 0xc3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    const/16 v2, 0x120

    const/16 v3, 0x120

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const/16 v2, 0x120

    const/16 v3, 0x180

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    const/16 v2, 0x122

    const/16 v3, 0x91

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    const/16 v2, 0x122

    const/16 v3, 0x122

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    const/16 v2, 0x128

    const/16 v3, 0x91

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    const/16 v2, 0x12c

    const/16 v3, 0x12c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    const/16 v2, 0x133

    const/16 v3, 0x133

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const/16 v2, 0x136

    const/16 v3, 0x1a8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const/16 v2, 0x139

    const/16 v3, 0xeb

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const/16 v2, 0x139

    const/16 v3, 0x139

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const/16 v2, 0x13c

    const/16 v3, 0x13c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    const/16 v2, 0x13e

    const/16 v3, 0x1a8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-static {v4, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    const/16 v2, 0x1aa

    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    const/16 v2, 0x147

    const/16 v3, 0xf5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    const/16 v2, 0x149

    const/16 v3, 0x149

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    const/16 v2, 0x150

    const/16 v3, 0x150

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    const/16 v2, 0x157

    const/16 v3, 0x157

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    const/16 v2, 0x15b

    const/16 v3, 0xc3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    const/16 v2, 0x169

    const/16 v3, 0x169

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    const/16 v2, 0x16e

    const/16 v3, 0xce

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    const/16 v2, 0x16f

    const/16 v3, 0x149

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    const/16 v2, 0x172

    const/16 v3, 0xd0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    const/16 v2, 0x172

    const/16 v3, 0x172

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    const/16 v2, 0x172

    const/16 v3, 0x1f4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    const/16 v2, 0x17c

    const/16 v3, 0xb5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    const/16 v2, 0x17c

    const/16 v3, 0xd6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    const/16 v2, 0x17c

    const/16 v3, 0xef

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    const/16 v2, 0x17c

    const/16 v3, 0x17c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    const/16 v2, 0x17c

    const/16 v3, 0x197

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    const/16 v2, 0x18b

    const/16 v3, 0x10e

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    const/16 v2, 0x190

    const/16 v3, 0x12c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    const/16 v2, 0x192

    const/16 v3, 0x192

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    const/16 v2, 0x1a4

    const/16 v3, 0x5f

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    const/16 v2, 0x1a4

    const/16 v3, 0xec

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    const/16 v2, 0x1a4

    const/16 v3, 0x109

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    const/16 v2, 0x1a4

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    const/16 v2, 0x1a4

    const/16 v3, 0x1a4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    const/16 v2, 0x1a7

    const/16 v3, 0x1a7

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    const/16 v2, 0x1a8

    const/16 v3, 0x1a8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    const/16 v2, 0x1aa

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const/16 v2, 0x1ae

    const/16 v3, 0x1ae

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    const/16 v2, 0x1b9

    const/16 v3, 0x1b9

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-static {v7, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    const/16 v2, 0x1d0

    const/16 v3, 0x1d0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    const/16 v2, 0x1d0

    const/16 v3, 0x1f1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    const/16 v2, 0x1d0

    const/16 v3, 0x27c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    const/16 v2, 0x1d3

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    const/16 v2, 0x1d6

    const/16 v3, 0x108

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    const/16 v2, 0x1e0

    const/16 v3, 0x1e0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    const/16 v2, 0x1e0

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    const/16 v2, 0x1e0

    invoke-static {v2, v8}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    const/16 v2, 0x1f0

    const/16 v3, 0x1f0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    const/16 v2, 0x206

    const/16 v3, 0x206

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    const/16 v2, 0x20e

    invoke-static {v2, v8}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    const/16 v2, 0x21c

    invoke-static {v2, v8}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    const/16 v2, 0x229

    const/16 v3, 0x229

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    const/16 v2, 0x22c

    const/16 v3, 0x2e4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    const/16 v2, 0x232

    const/16 v3, 0x13c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    const/16 v2, 0x240

    const/16 v3, 0x240

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    const/16 v2, 0x244

    const/16 v3, 0x122

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    const/16 v2, 0x248

    const/16 v3, 0x320

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    const/16 v2, 0x24c

    const/16 v3, 0x24c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    const/16 v2, 0x250

    const/16 v3, 0x250

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    const/16 v2, 0x252

    const/16 v3, 0x252

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    const/16 v2, 0x256

    const/16 v3, 0x128

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    const/16 v2, 0x280

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    const/16 v2, 0x280

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    const/16 v2, 0x288

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    const/16 v2, 0x29a

    const/16 v3, 0x29a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    const/16 v2, 0x2a0

    const/16 v3, 0x25a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    const/16 v2, 0x2a0

    const/16 v3, 0x2a0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    invoke-static {v8, v8}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    const/16 v2, 0x438

    invoke-static {v8, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    const/16 v2, 0x500

    invoke-static {v8, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    const/16 v2, 0x2d4

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    const/16 v2, 0x2d4

    const/16 v3, 0x230

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    const/16 v2, 0x2d5

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    const/16 v2, 0x2f2

    const/16 v3, 0x1a8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    const/16 v2, 0x2f4

    const/16 v3, 0x2f4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    const/16 v2, 0x302

    const/16 v3, 0x1e5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    const/16 v2, 0x302

    const/16 v3, 0x302

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    const/16 v2, 0x314

    const/16 v3, 0x438

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    const/16 v2, 0x315

    const/16 v3, 0x438

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    const/16 v2, 0x31c

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    const/16 v2, 0x320

    const/16 v3, 0x320

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    const/16 v2, 0x32a

    const/16 v3, 0x438

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    const/16 v2, 0x332

    const/16 v3, 0x332

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    const/16 v2, 0x355

    const/16 v3, 0x1e0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    const/16 v2, 0x356

    const/16 v3, 0x1e0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    const/16 v2, 0x360

    const/16 v3, 0x360

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    const/16 v2, 0x380

    const/16 v3, 0x380

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    const/16 v2, 0x3a6

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    const/16 v2, 0x3a6

    const/16 v3, 0x3a6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    const/16 v2, 0x3a7

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    const/16 v2, 0x3aa

    const/16 v3, 0x3aa

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    const/16 v2, 0x3d4

    const/16 v3, 0x3d4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    const/16 v2, 0x3d5

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    const/16 v2, 0x3f4

    const/16 v3, 0x2b5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    const/16 v2, 0x3fd

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    const/16 v2, 0x3fd

    const/16 v3, 0x3fd

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    const/16 v2, 0x400

    const/16 v3, 0x300

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    const/16 v2, 0x41b

    const/16 v3, 0x41b

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    const/16 v2, 0x42a

    const/16 v3, 0x32a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    const/16 v2, 0x42b

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    const/16 v2, 0x438

    const/16 v3, 0x315

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    const/16 v2, 0x438

    const/16 v3, 0x438

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    const/16 v2, 0x472

    const/16 v3, 0x280

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    const/16 v2, 0x4a6

    const/16 v3, 0x4a6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    const/16 v2, 0x4a7

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    const/16 v2, 0x4b6

    const/16 v3, 0x4b6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    const/16 v2, 0x4fc

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    const/16 v2, 0x4fc

    const/16 v3, 0x4fc

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    const/16 v2, 0x4fd

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    const/16 v2, 0x500

    invoke-static {v2, v8}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    const/16 v2, 0x500

    const/16 v3, 0x320

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    const/16 v2, 0x523

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    const/16 v2, 0x523

    const/16 v3, 0x523

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    const/16 v2, 0x546

    const/16 v3, 0x546

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    const/16 v2, 0x547

    const/16 v3, 0x17c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    const/16 v2, 0x556

    const/16 v3, 0x300

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    const/16 v2, 0x5a0

    const/16 v3, 0x32a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    const/16 v2, 0x5a0

    const/16 v3, 0x50b

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    const/16 v2, 0x5a0

    const/16 v3, 0x5a0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    const/16 v2, 0x5db

    const/16 v3, 0x404

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    const/16 v2, 0x5dd

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    const/16 v2, 0x619

    const/16 v3, 0x21c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    const/16 v2, 0x619

    const/16 v3, 0x42d

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    const/16 v2, 0x619

    const/16 v3, 0x619

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    const/16 v2, 0x61a

    const/16 v3, 0x21c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    const/16 v2, 0x61a

    const/16 v3, 0x61a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    const/16 v2, 0x639

    const/16 v3, 0x380

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    const/16 v2, 0x701

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    const/16 v2, 0x701

    const/16 v3, 0x701

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    const/16 v2, 0x703

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    const/16 v2, 0x726

    const/16 v3, 0x21c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    const/16 v2, 0x726

    const/16 v3, 0x56f

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    const/16 v2, 0x727

    const/16 v3, 0x21c

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    const/16 v2, 0x77f

    const/16 v3, 0x437

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    const/16 v2, 0x77f

    const/16 v3, 0x4b0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    const/16 v2, 0x780

    const/16 v3, 0x438

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    const/16 v2, 0x7e4

    const/16 v3, 0x240

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    const/16 v2, 0x7e4

    const/16 v3, 0x7e4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    const/16 v2, 0x7e5

    const/16 v3, 0x240

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    const/16 v2, 0x800

    const/16 v3, 0x600

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    const/16 v2, 0x834

    const/16 v3, 0x2f1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    const/16 v2, 0x834

    const/16 v3, 0x834

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    const/16 v2, 0x95e

    const/16 v3, 0x240

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    const/16 v2, 0x95e

    const/16 v3, 0x66a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    const/16 v2, 0x95e

    const/16 v3, 0x95e

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    const/16 v2, 0x95f

    const/16 v3, 0x240

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    const/16 v2, 0x9a6

    const/16 v3, 0x9a6

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    const/16 v2, 0x9a7

    const/16 v3, 0x2f1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    const/16 v2, 0xa00

    const/16 v3, 0x5a0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->SQUARE_DIMEN_POINTS:Ljava/util/List;

    .line 315
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->WIDE_DIMEN_POINTS:Ljava/util/List;

    .line 316
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TALL_DIMEN_POINTS:Ljava/util/List;

    .line 318
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .line 319
    .local v0, "DimenPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 320
    sget-object v3, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->SQUARE_DIMEN_POINTS:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 321
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 322
    sget-object v3, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->WIDE_DIMEN_POINTS:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TALL_DIMEN_POINTS:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 328
    .end local v0    # "DimenPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TALL_DIMEN_POINTS:Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$$Lambda$3;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 330
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->DIMEN_POINT_CACHE:Ljava/util/Map;

    .line 331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/bumptech/glide/load/model/ModelCache;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/bumptech/glide/load/model/ModelCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/bumptech/glide/load/model/GlideUrl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    .local p2, "urlCache":Lcom/bumptech/glide/load/model/ModelCache;, "Lcom/bumptech/glide/load/model/ModelCache<Ljava/lang/String;Lcom/bumptech/glide/load/model/GlideUrl;>;"
    invoke-direct {p0, p1, p2}, Lcom/bumptech/glide/load/model/stream/BaseGlideUrlLoader;-><init>(Landroid/content/Context;Lcom/bumptech/glide/load/model/ModelCache;)V

    .line 335
    return-void
.end method

.method private formatUrlForDimenPoint(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)Ljava/lang/String;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "DimenPoint"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .prologue
    .line 421
    const/4 v1, 0x1

    .line 422
    .local v1, "addWidth":Z
    const/4 v0, 0x1

    .line 424
    .local v0, "addHeight":Z
    sget-object v4, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->WIDTH_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 425
    const-string v4, "w=[0-9]+"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "w="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 426
    const/4 v1, 0x0

    .line 429
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->HEIGHT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 430
    const-string v4, "h=[0-9]+"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "h="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 431
    const/4 v0, 0x0

    .line 434
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 435
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 437
    .local v2, "builder":Landroid/net/Uri$Builder;
    if-eqz v1, :cond_2

    .line 438
    const-string v4, "w"

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 441
    :cond_2
    if-eqz v0, :cond_3

    .line 442
    const-string v4, "h"

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 445
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getBestDimenPointFor(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 358
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getTargetPointForCurrentNetworkQuality(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v3

    .line 359
    .local v3, "targetPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    sget-object v4, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->DIMEN_POINT_CACHE:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .line 361
    .local v0, "bestDimenPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    if-nez v0, :cond_0

    .line 365
    if-ne p1, p2, :cond_1

    .line 366
    sget-object v1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->SQUARE_DIMEN_POINTS:Ljava/util/List;

    .line 367
    .local v1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;

    move-result-object v2

    .line 373
    .local v2, "simFunc":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    :goto_0
    invoke-direct {p0, v1, v3, v2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getClosest(Ljava/util/List;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v0

    .line 375
    sget-object v4, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Target - w:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " h:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | Best match - w:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " h:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    sget-object v4, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->DIMEN_POINT_CACHE:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    .end local v1    # "points":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    .end local v2    # "simFunc":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    :cond_0
    return-object v0

    .line 369
    :cond_1
    if-le p1, p2, :cond_2

    sget-object v1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->WIDE_DIMEN_POINTS:Ljava/util/List;

    .line 370
    .restart local v1    # "points":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;

    move-result-object v2

    .restart local v2    # "simFunc":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    goto :goto_0

    .line 369
    .end local v1    # "points":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    .end local v2    # "simFunc":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->TALL_DIMEN_POINTS:Ljava/util/List;

    goto :goto_1
.end method

.method private getClosest(Ljava/util/List;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .locals 8
    .param p2, "targetPoint"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .param p3, "simFunc"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;",
            ")",
            "Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;"
        }
    .end annotation

    .prologue
    .line 405
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;>;"
    const/4 v0, 0x0

    .line 406
    .local v0, "closestPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 408
    .local v2, "closestSim":D
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .line 409
    .local v1, "otherPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    invoke-interface {p3, p2, v1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$SimFunc;->similarity(Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)D

    move-result-wide v4

    .line 411
    .local v4, "similarity":D
    if-eqz v0, :cond_1

    cmpg-double v7, v4, v2

    if-gez v7, :cond_0

    .line 412
    :cond_1
    move-object v0, v1

    .line 413
    move-wide v2, v4

    goto :goto_0

    .line 417
    .end local v1    # "otherPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .end local v4    # "similarity":D
    :cond_2
    return-object v0
.end method

.method private getFactorForQuality(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)D
    .locals 2
    .param p1, "quality"    # Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    .prologue
    .line 397
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->MODERATE:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->isLessThan(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 400
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0
.end method

.method private getResizeUrl(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 347
    if-gez p2, :cond_0

    if-gez p3, :cond_0

    .line 350
    const/16 p3, 0x78

    move p2, p3

    .line 353
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getBestDimenPointFor(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v0

    .line 354
    .local v0, "bestDimenPoint":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->formatUrlForDimenPoint(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getTargetPointForCurrentNetworkQuality(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 383
    mul-int v1, p1, p2

    const/16 v4, 0x3840

    if-gt v1, v4, :cond_0

    .line 384
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v1

    .line 392
    :goto_0
    return-object v1

    .line 386
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->getCurrentQuality()Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    move-result-object v0

    .line 388
    .local v0, "currentQuality":Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getFactorForQuality(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)D

    move-result-wide v2

    .line 389
    .local v2, "factor":D
    int-to-double v4, p1

    mul-double/2addr v4, v2

    double-to-int p1, v4

    .line 390
    int-to-double v4, p2

    mul-double/2addr v4, v2

    double-to-int p2, v4

    .line 392
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$getBestDimenPointFor$1(Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)D
    .locals 2
    .param p0, "a"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .param p1, "b"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method static synthetic lambda$static$0(Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .param p1, "rhs"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic getUrl(Ljava/lang/Object;II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getUrl(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1
    .param p1, "model"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 339
    const-string v0, "images-eds"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "store-images"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;->getResizeUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p1

    .line 342
    .end local p1    # "model":Ljava/lang/String;
    :cond_1
    return-object p1
.end method
