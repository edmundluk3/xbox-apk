.class public Lcom/microsoft/xbox/service/model/TitleMessage;
.super Ljava/lang/Object;
.source "TitleMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/TitleMessage$DataChunk;,
        Lcom/microsoft/xbox/service/model/TitleMessage$Data;,
        Lcom/microsoft/xbox/service/model/TitleMessage$Type;
    }
.end annotation


# instance fields
.field public data:Lcom/microsoft/xbox/service/model/TitleMessage$Data;

.field public id:Ljava/lang/String;

.field public notification:Lcom/microsoft/xbox/service/model/TitleMessage$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleMessage;
    .locals 6
    .param p0, "jsonData"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 46
    .local v2, "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    :try_start_0
    const-class v4, Lcom/microsoft/xbox/service/model/TitleMessage;

    invoke-static {p0, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleMessage;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v2

    .line 50
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    .local v3, "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    :goto_0
    return-object v3

    .line 47
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "TitleMessage fromJson error: "

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v2

    .line 50
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    :catchall_0
    move-exception v4

    move-object v3, v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/TitleMessage;
    goto :goto_0
.end method
