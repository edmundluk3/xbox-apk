.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;
.source "$AutoValue_ClubAccountsDataTypes_ClubSuspension.java"


# instance fields
.field private final actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

.field private final deleteAfter:Ljava/util/Date;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;Ljava/util/Date;)V
    .locals 2
    .param p1, "actor"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .param p2, "deleteAfter"    # Ljava/util/Date;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null actor"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null deleteAfter"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->deleteAfter:Ljava/util/Date;

    .line 25
    return-void
.end method


# virtual methods
.method public actor()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    return-object v0
.end method

.method public deleteAfter()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->deleteAfter:Ljava/util/Date;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    .line 54
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;->actor()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->deleteAfter:Ljava/util/Date;

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;->deleteAfter()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;
    :cond_3
    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 62
    const/4 v0, 0x1

    .line 63
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 65
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->deleteAfter:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubSuspension{actor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->actor:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deleteAfter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubSuspension;->deleteAfter:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
