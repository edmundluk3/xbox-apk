.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$OOBE;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OOBE"
.end annotation


# static fields
.field public static final Completion:Ljava/lang/String; = "OOBE - Completion View"

.field public static final ConsoleSelection:Ljava/lang/String; = "OOBE - Console Selection View"

.field public static final PowerSettings:Ljava/lang/String; = "OOBE - Power Settings View"

.field public static final TimeZone:Ljava/lang/String; = "OOBE - Time Zone View"

.field public static final UpdateSettings:Ljava/lang/String; = "OOBE - Update Settings View"

.field public static final Welcome:Ljava/lang/String; = "OOBE - Welcome View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
