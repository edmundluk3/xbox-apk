.class Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GameProfileFriendsWhoPlayModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProfileFriendsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$1;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;-><init>(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)V

    return-void
.end method

.method private trySetBroadcaster(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "friendsWhoPlay":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->access$302(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;Lcom/microsoft/xbox/service/model/FollowersData;)Lcom/microsoft/xbox/service/model/FollowersData;

    .line 132
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 133
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-wide v4, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->access$200(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->access$302(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;Lcom/microsoft/xbox/service/model/FollowersData;)Lcom/microsoft/xbox/service/model/FollowersData;

    .line 136
    iget-boolean v1, v0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v1, :cond_0

    .line 141
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->access$100(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->access$200(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPeopleHubPeoplePlayedTitle(Ljava/lang/String;J)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v3

    .line 101
    .local v3, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v1, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 104
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 105
    .local v2, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 106
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v2    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->trySetBroadcaster(Ljava/util/List;)V

    .line 112
    return-object v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 126
    const-wide/16 v0, 0x23f0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 122
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method
