.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaderboardResponse"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final leaderboard:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;Ljava/util/List;)V
    .locals 1
    .param p1, "pagingInfo"    # Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p2, "leaderboard":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    .line 159
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->leaderboard:Ljava/util/List;

    .line 160
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    if-ne p1, p0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v1

    .line 180
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 181
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 183
    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    .line 184
    .local v0, "other":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    iget-object v4, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->leaderboard:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->leaderboard:Ljava/util/List;

    .line 185
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getContinuationToken()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    invoke-static {v0}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->access$000(Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;)Ljava/lang/Long;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLeaderboard()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->leaderboard:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 191
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    if-nez v0, :cond_0

    .line 192
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    .line 193
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->pagingInfo:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    .line 194
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->leaderboard:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    .line 197
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
