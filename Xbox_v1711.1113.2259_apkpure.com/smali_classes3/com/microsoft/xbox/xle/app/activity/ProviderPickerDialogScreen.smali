.class public Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PopupScreen;
.source "ProviderPickerDialogScreen.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final displayAirings:Z

.field private final errmsgRid:I

.field private final mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private final msgRid:I

.field private final onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;IIZ)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "onClickListener"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;
    .param p3, "msgRid"    # I
    .param p4, "errormsgRid"    # I
    .param p5, "displayAirings"    # Z

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 33
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    .line 34
    iput p3, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->msgRid:I

    .line 35
    iput p4, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->errmsgRid:I

    .line 36
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->displayAirings:Z

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->displayAirings:Z

    return v0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 41
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->onCreate()V

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->onCreateContentView()V

    .line 43
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    if-nez v1, :cond_0

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 45
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 52
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onCreate()V

    .line 53
    return-void

    .line 43
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->onClickListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->msgRid:I

    iget v4, p0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->errmsgRid:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;II)V

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f0301dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;->setContentView(I)V

    .line 58
    return-void
.end method
