.class public Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GameGalleryListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
        ">;"
    }
.end annotation


# static fields
.field private static final RES_ID:I = 0x7f020127


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v5, 0x7f020127

    .line 25
    if-nez p2, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 27
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030114

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 30
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 31
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    if-eqz v1, :cond_1

    .line 32
    const v3, 0x7f0e0617

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 33
    .local v0, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    if-eqz v0, :cond_1

    .line 34
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 37
    .end local v0    # "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%s %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070d3f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    add-int/lit8 v7, p1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 38
    return-object p2
.end method
