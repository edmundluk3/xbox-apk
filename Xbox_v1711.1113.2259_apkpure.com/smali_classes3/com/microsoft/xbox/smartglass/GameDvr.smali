.class public Lcom/microsoft/xbox/smartglass/GameDvr;
.super Ljava/lang/Object;
.source "GameDvr.java"


# instance fields
.field private final _pSessionManager:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/GameDvr;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/GameDvr;->_pSessionManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 15
    return-void
.end method

.method private native initialize(J)J
.end method

.method private native recordPrevious(JI)V
.end method


# virtual methods
.method public recordPrevious(I)V
    .locals 3
    .param p1, "seconds"    # I

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/GameDvr;->_pSessionManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/smartglass/GameDvr;->recordPrevious(JI)V

    .line 28
    return-void
.end method
