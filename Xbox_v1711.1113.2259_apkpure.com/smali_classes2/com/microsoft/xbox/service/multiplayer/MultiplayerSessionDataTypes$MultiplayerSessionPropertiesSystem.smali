.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerSessionPropertiesSystem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;
    .locals 1

    .prologue
    .line 1144
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionPropertiesSystem$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionPropertiesSystem$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1177
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionPropertiesSystem$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionPropertiesSystem$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract clubId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract joinRestriction()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract readRestriction()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract scheduledTime()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract searchHandleVisibility()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
