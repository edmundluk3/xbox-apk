.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubAdminMembersListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ClubAdminMembersViewHolder"
.end annotation


# instance fields
.field private controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final moreActions:Landroid/view/View;

.field private final presence:Landroid/widget/TextView;

.field private final realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;Landroid/view/View;)V
    .locals 4
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    .line 114
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$000()Landroid/text/style/TextAppearanceSpan;

    move-result-object v1

    if-nez v1, :cond_0

    .line 117
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080222

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$002(Landroid/text/style/TextAppearanceSpan;)Landroid/text/style/TextAppearanceSpan;

    .line 120
    :cond_0
    const v1, 0x7f0e026e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 121
    const v1, 0x7f0e0270

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 122
    const v1, 0x7f0e0271

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 123
    const v1, 0x7f0e0272

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->presence:Landroid/widget/TextView;

    .line 124
    const v1, 0x7f0e026f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->moreActions:Landroid/view/View;

    .line 126
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 128
    .local v0, "profileClickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Landroid/view/View;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    return-void
.end method

.method private closeDialogDecorator(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomSheetDialog;->hide()V

    .line 236
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 237
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->onProfileClick(J)V

    return-void
.end method

.method static synthetic lambda$new$12(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Landroid/view/View;Landroid/view/View;)V
    .locals 12
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v10, 0x7f030058

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 134
    .local v1, "controls":Landroid/view/View;
    new-instance v7, Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/support/design/widget/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    .line 135
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v7, v1}, Landroid/support/design/widget/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 136
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v7}, Landroid/support/design/widget/BottomSheetDialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/16 v10, 0x7d3

    invoke-virtual {v7, v10}, Landroid/view/Window;->setType(I)V

    .line 137
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-static {v7}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v3

    .line 138
    .local v3, "mBehavior":Landroid/support/design/widget/BottomSheetBehavior;
    const/4 v7, 0x3

    invoke-virtual {v3, v7}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 140
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v10, 0x7f0e026a

    invoke-virtual {v7, v10}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 141
    .local v4, "ownerAction":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v10, 0x7f0e026b

    invoke-virtual {v7, v10}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 142
    .local v0, "banAction":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v10, 0x7f0e026c

    invoke-virtual {v7, v10}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 143
    .local v5, "removeAction":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v10, 0x7f0e026d

    invoke-virtual {v7, v10}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 144
    .local v6, "reportAction":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 145
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 146
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 147
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 149
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->getAdapterPosition()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 150
    .local v2, "dataItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    .line 151
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 152
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getIsModerator()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 153
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    const v10, 0x7f070fe0

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    const v10, 0x7f070229

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 155
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getIsModerator()Z

    move-result v7

    if-nez v7, :cond_6

    :cond_0
    move v7, v9

    :goto_1
    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 165
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getIsModerator()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    move v8, v9

    :cond_2
    invoke-static {v5, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 167
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v7}, Landroid/support/design/widget/BottomSheetDialog;->show()V

    .line 184
    :cond_3
    return-void

    .line 157
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    const v10, 0x7f070e9a

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    const v10, 0x7f070236

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 159
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 162
    :cond_5
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    goto :goto_0

    :cond_6
    move v7, v8

    .line 164
    goto :goto_1
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->demoteFromModerator(J)V

    return-void
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getRealName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->report(JLjava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 180
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 155
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->promoteToModerator(J)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 159
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->ban(J)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 174
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->removeAndCloseDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomSheetDialog;->hide()V

    .line 170
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070225

    .line 171
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070224

    .line 172
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070614

    .line 173
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 175
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 170
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 177
    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;->remove(J)V

    return-void
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p1, "dataItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 179
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->removeAndCloseDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method private removeAndCloseDecorator(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->remove(I)V

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->notifyItemRemoved(I)V

    .line 243
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;Ljava/lang/String;)V
    .locals 12
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "searchTerm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v11, 0x7f0201fa

    const/16 v10, 0x21

    const/4 v6, 0x0

    const/4 v9, -0x1

    .line 188
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 190
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 192
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v11, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 195
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->presence:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getPresenceText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 198
    .local v0, "gamerTagSearchMatchIndex":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getRealName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 199
    .local v1, "realNameSearchMatchIndex":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    .line 201
    .local v2, "searchTermLength":I
    if-ne v0, v9, :cond_0

    .line 202
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 215
    :goto_0
    if-ne v1, v9, :cond_1

    .line 216
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getRealName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 229
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v3, 0x1

    .line 230
    .local v3, "showMoreActions":Z
    :goto_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->moreActions:Landroid/view/View;

    if-eqz v3, :cond_3

    :goto_3
    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setClickable(Z)V

    .line 232
    return-void

    .line 204
    .end local v3    # "showMoreActions":Z
    :cond_0
    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 207
    .local v4, "styledGamertag":Landroid/text/SpannableString;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$000()Landroid/text/style/TextAppearanceSpan;

    move-result-object v7

    add-int v8, v0, v2

    .line 206
    invoke-virtual {v4, v7, v0, v8, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 212
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 218
    .end local v4    # "styledGamertag":Landroid/text/SpannableString;
    :cond_1
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getRealName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 221
    .local v5, "styledRealName":Landroid/text/SpannableString;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->access$000()Landroid/text/style/TextAppearanceSpan;

    move-result-object v7

    .line 223
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v1

    .line 220
    invoke-virtual {v5, v7, v1, v8, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 226
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v7, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .end local v5    # "styledRealName":Landroid/text/SpannableString;
    :cond_2
    move v3, v6

    .line 229
    goto :goto_2

    .line 230
    .restart local v3    # "showMoreActions":Z
    :cond_3
    const/16 v6, 0x8

    goto :goto_3
.end method
