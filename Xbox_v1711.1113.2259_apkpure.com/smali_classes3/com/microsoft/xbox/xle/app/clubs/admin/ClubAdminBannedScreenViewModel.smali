.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminBannedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    }
.end annotation


# static fields
.field private static final LOAD_INCREMENT:I = 0x5a

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final allBannedUsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end field

.field private getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final loadMoreItemsObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loadedBannedModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private unbanCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->unbanCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    .line 81
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 82
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminBannedScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 87
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadMoreItemsObservable:Lio/reactivex/Observable;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadMoreItemsSync()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized ensureLoadedModelsUpdated()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v4, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 260
    .local v1, "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 262
    .local v2, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 263
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 265
    .local v3, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v3, :cond_0

    .line 266
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->getCreatedDate()Ljava/util/Date;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->with(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/Date;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 259
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v1    # "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v2    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    .end local v3    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 272
    .restart local v1    # "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .restart local v2    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273
    monitor-exit p0

    return-object v2
.end method

.method static synthetic lambda$onClubModelChanged$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private loadMoreItemsSync()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapterOnUIThread()V

    .line 219
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->ensureLoadedModelsUpdated()Ljava/util/List;

    .line 221
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    .line 222
    .local v4, "startIndexToLoad":I
    add-int/lit8 v9, v4, 0x5a

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 224
    .local v0, "endIndexToLoad":I
    new-instance v2, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v9, v4, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 226
    .local v2, "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    new-instance v8, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 228
    .local v8, "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 229
    .local v3, "rosterItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 231
    .local v7, "xuid":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v10, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 232
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 236
    .end local v3    # "rosterItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v7    # "xuid":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 238
    .local v5, "summaries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 239
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->immediateQuery(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 242
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 243
    .local v6, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v11, v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v10, v11, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 246
    .end local v6    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->ensureLoadedModelsUpdated()Ljava/util/List;

    move-result-object v1

    .line 248
    .local v1, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 249
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_2
    iput-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 251
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapterOnUIThread()V

    .line 253
    return-object v1

    .line 249
    :cond_4
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_2
.end method

.method private stopActiveTasks()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 133
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized getLoadedBannedModels()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumOfBannedUsers()I
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasMoreItemsToLoad()Z
    .locals 2

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadMoreItems()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadMoreItemsObservable:Lio/reactivex/Observable;

    return-object v0
.end method

.method protected loadOverride(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 147
    return-void
.end method

.method protected declared-synchronized onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 203
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 160
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    .line 162
    .local v0, "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->banned()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$$Lambda$2;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 171
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->ensureLoadedModelsUpdated()Ljava/util/List;

    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 174
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 175
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156
    .end local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 160
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 176
    .restart local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 179
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadMoreItems()Lio/reactivex/Observable;

    move-result-object v2

    .line 180
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 181
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 184
    invoke-virtual {v2}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v2

    .line 186
    invoke-virtual {v2}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 178
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 189
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 190
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->updateAdapter()V

    goto/16 :goto_0

    .line 198
    .end local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 199
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminBannedScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 120
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 139
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 141
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 125
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->stopActiveTasks()V

    .line 126
    return-void
.end method

.method public declared-synchronized unban(J)V
    .locals 7
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 100
    monitor-enter p0

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 102
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;

    .line 103
    .local v0, "bannedListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getXuid()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 104
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadedBannedModels:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    .end local v0    # "bannedListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 108
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 109
    .local v1, "rosterItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->allBannedUsers:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    .end local v1    # "rosterItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->unbanCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    invoke-virtual {v2, p1, p2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->unban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    monitor-exit p0

    return-void
.end method
