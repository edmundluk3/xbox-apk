.class public Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "LfgInterestedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
        "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private allowApprovals:Z

.field private final onApproveClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;"
        }
    .end annotation
.end field

.field private final onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;"
        }
    .end annotation
.end field

.field private final onReportClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "onApproveClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;>;"
    .local p2, "onDeclineClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;>;"
    .local p3, "onReportClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onApproveClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 45
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onReportClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->allowApprovals:Z

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->allowApprovals:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onReportClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onApproveClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 63
    const v0, 0x7f03015a

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 58
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V

    .line 59
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 53
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public setAllowApprovals(Z)V
    .locals 0
    .param p1, "allowApprovals"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->allowApprovals:Z

    .line 68
    return-void
.end method
