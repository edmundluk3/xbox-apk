.class Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPeopleActivityFeedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final continuationToken:Ljava/lang/String;

.field private final filters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "filters"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .param p5, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 3195
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3196
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3197
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->xuid:Ljava/lang/String;

    .line 3198
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->filters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 3199
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->continuationToken:Ljava/lang/String;

    .line 3200
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 3204
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->xuid:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->filters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->continuationToken:Ljava/lang/String;

    const/16 v5, 0x32

    move v7, v6

    invoke-interface/range {v0 .. v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPeopleActivityFeed(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Ljava/lang/String;IZZ)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3188
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3218
    const-wide/16 v0, 0xc86

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3213
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetPeopleActivityFeedRunner;->continuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, p1, v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3200(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 3214
    return-void

    .line 3213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3209
    return-void
.end method
