.class Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;
.super Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;->createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;

.field final synthetic val$li:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;->val$li:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;-><init>()V

    return-void
.end method


# virtual methods
.method public setBackground(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "img"    # Landroid/widget/ImageView;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;->val$li:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getShouldShowBackgroundColor()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 171
    :cond_0
    return-void
.end method
