.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SkuProperties"
.end annotation


# instance fields
.field private final bundledSkus:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BundledSkus"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;",
            ">;"
        }
    .end annotation
.end field

.field public hardwareProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "HardwareProperties"
    .end annotation
.end field

.field public final isBundle:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsBundle"
    .end annotation
.end field

.field public final isPreOrder:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsPreOrder"
    .end annotation
.end field

.field public final isRepurchasable:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsRepurchasable"
    .end annotation
.end field

.field public final isTrial:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsTrial"
    .end annotation
.end field

.field private packages:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Packages"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;",
            ">;"
        }
    .end annotation
.end field

.field public final skuDisplayRank:I
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SkuDisplayRank"
    .end annotation
.end field

.field public final xboxXPA:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "XboxXPA"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZZZZLjava/util/List;IZ)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "isTrial"    # Z
    .param p3, "isPreOrder"    # Z
    .param p4, "isBundle"    # Z
    .param p5, "isRepurchasable"    # Z
    .param p6    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "skuDisplayRank"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p8, "xboxXPA"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;",
            ">;ZZZZ",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 696
    .local p1, "bundledSkus":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;>;"
    .local p6, "packages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 697
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->bundledSkus:Ljava/util/List;

    .line 698
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->isTrial:Z

    .line 699
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->isPreOrder:Z

    .line 700
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->isBundle:Z

    .line 701
    iput-boolean p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->isRepurchasable:Z

    .line 702
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->packages:Ljava/util/List;

    .line 703
    iput p7, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->skuDisplayRank:I

    .line 704
    iput-boolean p8, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->xboxXPA:Z

    .line 705
    return-void
.end method


# virtual methods
.method public getBundledSkus()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->bundledSkus:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPackages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 714
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->packages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
