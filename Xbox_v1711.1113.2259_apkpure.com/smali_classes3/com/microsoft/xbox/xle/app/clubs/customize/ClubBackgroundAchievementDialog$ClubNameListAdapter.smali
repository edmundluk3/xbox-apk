.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubBackgroundAchievementDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubNameListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 91
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 100
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->bindTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    .line 101
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 95
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Landroid/view/ViewGroup;)V

    return-object v0
.end method
