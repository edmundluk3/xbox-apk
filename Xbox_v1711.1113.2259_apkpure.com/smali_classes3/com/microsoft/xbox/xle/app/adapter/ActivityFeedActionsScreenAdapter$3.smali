.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 5

    .prologue
    .line 137
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 139
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 140
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 142
    .local v0, "heightDiff":I
    const/16 v2, 0x64

    if-le v0, v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 144
    const-string v2, "ActivityFeedActionsScreenAdapter"

    const-string v3, " keyboard is Visible "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$602(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Z)Z

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    const-string v2, "ActivityFeedActionsScreenAdapter"

    const-string v3, " keyboard not visible"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$602(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Z)Z

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    goto :goto_0
.end method
