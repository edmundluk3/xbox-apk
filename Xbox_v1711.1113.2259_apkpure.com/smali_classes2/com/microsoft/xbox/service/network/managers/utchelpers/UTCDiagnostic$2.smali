.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;
.super Ljava/lang/Object;
.source "UTCDiagnostic.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;->trackDefaultHomePage(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$homeScreenPreference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;->val$homeScreenPreference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;->val$homeScreenPreference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 53
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "DefaultHomePage"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;->val$homeScreenPreference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->getTelemetrySettingName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 54
    const-string v1, "Diagnostics - Default Home Page Setting"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 55
    return-void
.end method
