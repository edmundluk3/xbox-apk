.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackageApplication;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SkuPropertyPackageApplication"
.end annotation


# instance fields
.field public applicationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ApplicationId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 835
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 837
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackageApplication;->applicationId:Ljava/lang/String;

    .line 838
    return-void
.end method
