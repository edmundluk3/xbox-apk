.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;
.super Ljava/lang/Object;
.source "SkypeMessageDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SkypeRealTimeEvent"
.end annotation


# instance fields
.field private final eventMessages:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;",
            ">;"
        }
    .end annotation
.end field

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "eventMessages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 200
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->eventMessages:Ljava/util/List;

    .line 201
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 210
    if-ne p1, p0, :cond_0

    .line 211
    const/4 v1, 0x1

    .line 216
    :goto_0
    return v1

    .line 212
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    if-nez v1, :cond_1

    .line 213
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 215
    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    .line 216
    .local v0, "other":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->eventMessages:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->eventMessages:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getEventMessages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->eventMessages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 222
    iget v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->hashCode:I

    if-nez v0, :cond_0

    .line 223
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->hashCode:I

    .line 224
    iget v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->eventMessages:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->hashCode:I

    .line 227
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
