.class final Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
.source "AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem.java"


# instance fields
.field private final createdDate:Ljava/util/Date;

.field private final personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/Date;)V
    .locals 2
    .param p1, "personSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p2, "createdDate"    # Ljava/util/Date;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null personSummary"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null createdDate"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->createdDate:Ljava/util/Date;

    .line 25
    return-void
.end method


# virtual methods
.method protected createdDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->createdDate:Ljava/util/Date;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;

    .line 52
    .local v0, "that":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->personSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->createdDate:Ljava/util/Date;

    .line 53
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->createdDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    :cond_3
    move v1, v2

    .line 55
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 60
    const/4 v0, 0x1

    .line 61
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 63
    mul-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->createdDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 65
    return v0
.end method

.method protected personSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubBannedListItem{personSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminBannedScreenViewModel_ClubBannedListItem;->createdDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
