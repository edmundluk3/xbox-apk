.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubAdminMembersListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static SEARCH_MATCH_APPEARANCE:Landroid/text/style/TextAppearanceSpan;


# instance fields
.field private final clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

.field private currentSearchTerm:Ljava/lang/String;

.field private final viewerIsOwner:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;Z)V
    .locals 1
    .param p1, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewerIsOwner"    # Z

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->currentSearchTerm:Ljava/lang/String;

    .line 64
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->viewerIsOwner:Z

    .line 65
    return-void
.end method

.method static synthetic access$000()Landroid/text/style/TextAppearanceSpan;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->SEARCH_MATCH_APPEARANCE:Landroid/text/style/TextAppearanceSpan;

    return-object v0
.end method

.method static synthetic access$002(Landroid/text/style/TextAppearanceSpan;)Landroid/text/style/TextAppearanceSpan;
    .locals 0
    .param p0, "x0"    # Landroid/text/style/TextAppearanceSpan;

    .prologue
    .line 35
    sput-object p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->SEARCH_MATCH_APPEARANCE:Landroid/text/style/TextAppearanceSpan;

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->viewerIsOwner:Z

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    return-object v0
.end method


# virtual methods
.method public contains(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Z
    .locals 1
    .param p1, "s"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 95
    const v0, 0x7f030059

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 100
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->currentSearchTerm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$MemberItemClickListener;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter$ClubAdminMembersViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public setCurrentSearchTerm(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "currentSearchTerm"    # Ljava/lang/CharSequence;

    .prologue
    .line 72
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->currentSearchTerm:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getItemCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 75
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 77
    .local v1, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getMatchesSearchTerm()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->setMatchesSearchTerm(Z)V

    .line 79
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->notifyItemChanged(I)V

    .line 74
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 72
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 80
    .restart local v0    # "i":I
    .restart local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->currentSearchTerm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->matchesSearchTerm(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->setMatchesSearchTerm(Z)V

    .line 82
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersListAdapter;->notifyItemChanged(I)V

    goto :goto_2

    .line 85
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_3
    return-void
.end method
