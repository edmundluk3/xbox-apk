.class Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;
.super Landroid/os/Handler;
.source "WebSocketWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThreadHandler"
.end annotation


# instance fields
.field private final mWebSocketWriterReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lde/tavendo/autobahn/WebSocketWriter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lde/tavendo/autobahn/WebSocketWriter;)V
    .locals 1
    .param p1, "webSocketWriter"    # Lde/tavendo/autobahn/WebSocketWriter;

    .prologue
    .line 448
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 450
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;->mWebSocketWriterReference:Ljava/lang/ref/WeakReference;

    .line 451
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 457
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;->mWebSocketWriterReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/tavendo/autobahn/WebSocketWriter;

    .line 458
    .local v0, "webSocketWriter":Lde/tavendo/autobahn/WebSocketWriter;
    if-eqz v0, :cond_0

    .line 459
    invoke-virtual {v0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->writeMessageToBuffer(Landroid/os/Message;)V

    .line 461
    :cond_0
    return-void
.end method
