.class public final enum Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;
.super Ljava/lang/Enum;
.source "BroadcastConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GameStreamError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum FailedToInitialize:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum FailedToInstantiate:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum FailedToStart:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum FailedToStop:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum General:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "General"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->General:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "FailedToInstantiate"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToInstantiate:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "FailedToInitialize"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToInitialize:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "FailedToStart"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToStart:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    const-string v1, "FailedToStop"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToStop:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    .line 37
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->General:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToInstantiate:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToInitialize:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToStart:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->FailedToStop:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    return-object v0
.end method
