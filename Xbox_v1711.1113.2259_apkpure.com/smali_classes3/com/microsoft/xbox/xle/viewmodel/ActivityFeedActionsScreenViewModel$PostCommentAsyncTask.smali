.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostCommentAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemRoot:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V
    .locals 1

    .prologue
    .line 1068
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1069
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->message:Ljava/lang/String;

    .line 1070
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1900(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 1071
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->itemRoot:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;

    .prologue
    .line 1068
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1075
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->itemRoot:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->message:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->postComment(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1068
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1085
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1068
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$2000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1081
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$2000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1100
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1068
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$PostCommentAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1090
    return-void
.end method
