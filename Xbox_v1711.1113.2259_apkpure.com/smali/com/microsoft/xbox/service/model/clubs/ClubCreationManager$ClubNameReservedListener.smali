.class public interface abstract Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;
.super Ljava/lang/Object;
.source "ClubCreationManager.java"


# annotations
.annotation runtime Lcom/microsoft/xbox/toolkit/java8/FunctionalInterface;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ClubNameReservedListener"
.end annotation


# virtual methods
.method public abstract onClubNameReserved()V
.end method
