.class public Lcom/microsoft/xbox/service/model/sls/DeleteSkypeMessageRequest;
.super Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
.source "DeleteSkypeMessageRequest.java"


# instance fields
.field public skypeeditedid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "messageType"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "skypeeditedid"    # Ljava/lang/String;

    .prologue
    .line 15
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/sls/DeleteSkypeMessageRequest;->skypeeditedid:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static getBody(Lcom/microsoft/xbox/service/model/sls/DeleteSkypeMessageRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "deleteMessageRequest"    # Lcom/microsoft/xbox/service/model/sls/DeleteSkypeMessageRequest;

    .prologue
    .line 21
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 25
    :goto_0
    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v1, "DeleteSkypeMessageRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const/4 v1, 0x0

    goto :goto_0
.end method
