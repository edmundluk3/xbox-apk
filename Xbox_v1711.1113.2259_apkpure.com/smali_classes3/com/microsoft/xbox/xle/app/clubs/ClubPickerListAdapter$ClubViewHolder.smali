.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubPickerListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClubViewHolder"
.end annotation


# instance fields
.field private final clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final clubPic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private final rootView:Landroid/widget/RelativeLayout;

.field private final subtitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    .line 57
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    move-object v0, p2

    .line 59
    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->rootView:Landroid/widget/RelativeLayout;

    .line 60
    const v0, 0x7f0e03b2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->clubPic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 61
    const v0, 0x7f0e03b3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    const v0, 0x7f0e03b4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->subtitle:Landroid/widget/TextView;

    .line 63
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v3, 0x7f0201fa

    const/4 v1, 0x1

    .line 66
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->clubPic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 72
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    invoke-virtual {v2, v0, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->subtitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 78
    :goto_0
    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 82
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 87
    return-void
.end method
