.class public Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;
.super Landroid/widget/LinearLayout;
.source "URCPowerLayout.java"


# instance fields
.field private powerLineLayoutId:I

.field private willShowSystemOnOff:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method private addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "deviceID"    # Ljava/lang/String;
    .param p4, "toggleOnOff"    # Z

    .prologue
    .line 111
    iget v8, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->powerLineLayoutId:I

    const/4 v9, 0x0

    invoke-static {p1, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 112
    .local v1, "line":Landroid/view/View;
    if-nez v1, :cond_0

    .line 193
    :goto_0
    return-void

    .line 116
    :cond_0
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->text_label:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 117
    .local v0, "label":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 118
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_1
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->off_view:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 122
    .local v3, "offGroup":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 123
    if-eqz p4, :cond_7

    const/16 v8, 0x8

    :goto_1
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 126
    :cond_2
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->txt_on:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 127
    .local v6, "onText":Landroid/view/View;
    if-eqz v6, :cond_3

    .line 128
    if-nez p4, :cond_8

    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 129
    sget v8, Lcom/microsoft/xbox/xle/urc/R$string;->power_on:I

    invoke-static {v6, v8}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->updateText(Landroid/view/View;I)V

    .line 132
    :cond_3
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->txt_on_off:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 133
    .local v7, "onoffText":Landroid/view/View;
    if-eqz v7, :cond_4

    .line 134
    if-eqz p4, :cond_9

    const/4 v8, 0x0

    :goto_3
    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 135
    sget v8, Lcom/microsoft/xbox/xle/urc/R$string;->power_on_off:I

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->updateText(Landroid/view/View;I)V

    .line 138
    :cond_4
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->txt_off:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 139
    .local v4, "offText":Landroid/view/View;
    if-eqz v4, :cond_5

    .line 140
    sget v8, Lcom/microsoft/xbox/xle/urc/R$string;->power_off:I

    invoke-static {v4, v8}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->updateText(Landroid/view/View;I)V

    .line 143
    :cond_5
    if-eqz p4, :cond_a

    .line 145
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->btn_on:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 146
    .local v5, "onButton":Landroid/view/View;
    if-eqz v5, :cond_6

    .line 147
    new-instance v8, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$1;

    invoke-direct {v8, p0, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    :cond_6
    :goto_4
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 123
    .end local v4    # "offText":Landroid/view/View;
    .end local v5    # "onButton":Landroid/view/View;
    .end local v6    # "onText":Landroid/view/View;
    .end local v7    # "onoffText":Landroid/view/View;
    :cond_7
    const/4 v8, 0x0

    goto :goto_1

    .line 128
    .restart local v6    # "onText":Landroid/view/View;
    :cond_8
    const/16 v8, 0x8

    goto :goto_2

    .line 134
    .restart local v7    # "onoffText":Landroid/view/View;
    :cond_9
    const/16 v8, 0x8

    goto :goto_3

    .line 160
    .restart local v4    # "offText":Landroid/view/View;
    :cond_a
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->btn_on:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 161
    .restart local v5    # "onButton":Landroid/view/View;
    if-eqz v5, :cond_b

    .line 162
    new-instance v8, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$2;

    invoke-direct {v8, p0, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$2;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    :cond_b
    sget v8, Lcom/microsoft/xbox/xle/urc/R$id;->btn_off:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 176
    .local v2, "offButton":Landroid/view/View;
    if-eqz v2, :cond_6

    .line 177
    new-instance v8, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;

    invoke-direct {v8, p0, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method

.method private clearPowerLines()V
    .locals 0

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->removeAllViews()V

    .line 108
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 36
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->setOrientation(I)V

    .line 38
    sget-object v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPowerLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 41
    :try_start_0
    sget v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPowerLayout_powerStripLayoutId:I

    sget v2, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_power_view_line:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->powerLineLayoutId:I

    .line 42
    sget v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPowerLayout_willShowSystemOnOff:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->willShowSystemOnOff:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    :goto_0
    return-void

    .line 44
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1

    .line 47
    :cond_0
    sget v1, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_power_view_line:I

    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->powerLineLayoutId:I

    goto :goto_0
.end method

.method private static final updateText(Landroid/view/View;I)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "text"    # I

    .prologue
    .line 196
    instance-of v1, p0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "newText":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 199
    check-cast p0, Landroid/widget/TextView;

    .end local p0    # "v":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    .end local v0    # "newText":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public createPowerLineViews()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 59
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v3

    .line 60
    .local v3, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-nez v3, :cond_1

    .line 104
    :cond_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v2

    .line 65
    .local v2, "devices":[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    if-eqz v2, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->clearPowerLines()V

    .line 71
    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->willShowSystemOnOff:Z

    if-eqz v7, :cond_2

    .line 77
    const/4 v5, 0x1

    .line 79
    .local v5, "systemToggle":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v7

    sget v8, Lcom/microsoft/xbox/xle/urc/R$string;->power_system:I

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "all"

    invoke-direct {p0, v0, v7, v8, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 81
    new-instance v4, Landroid/widget/Space;

    invoke-direct {v4, v0}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 82
    .local v4, "space":Landroid/widget/Space;
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/microsoft/xbox/xle/urc/R$dimen;->powerFlyoutSeparatorHeight:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v7}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addView(Landroid/view/View;)V

    .line 86
    .end local v4    # "space":Landroid/widget/Space;
    .end local v5    # "systemToggle":Z
    :cond_2
    array-length v8, v2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v1, v2, v7

    .line 87
    .local v1, "device":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    if-eqz v1, :cond_3

    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    if-eqz v9, :cond_3

    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v10, "tuner"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 86
    :cond_3
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 94
    :cond_4
    const/4 v6, 0x1

    .line 96
    .local v6, "toggle":Z
    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v10, "stb"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v9

    sget v10, Lcom/microsoft/xbox/xle/urc/R$string;->power_stb:I

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_id:Ljava/lang/String;

    invoke-direct {p0, v0, v9, v10, v11}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 98
    :cond_5
    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v10, "tv"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v9

    sget v10, Lcom/microsoft/xbox/xle/urc/R$string;->power_tv:I

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_id:Ljava/lang/String;

    invoke-direct {p0, v0, v9, v10, v11}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 101
    :cond_6
    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    iget-object v10, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_id:Ljava/lang/String;

    invoke-direct {p0, v0, v9, v10, v11}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method
