.class public Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.super Ljava/util/HashMap;
.source "ActivityParameters.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTIVITY_FEED_ACTION_TYPE:Ljava/lang/String; = "ActivityFeedActionType"

.field private static final ACTIVITY_FEED_ITEM_LOCATOR:Ljava/lang/String; = "ActivityFeedItemLocator"

.field private static final APP_RELAUNCH_INTENT:Ljava/lang/String; = "AppRelaunchIntent"

.field private static final BI_SHARED_DATA:Ljava/lang/String; = "BIData"

.field private static final COMPARE_ACHIEVEMENT_DETAIL_FRIEND_XUID:Ljava/lang/String; = "CompareAchievementDetailFriendXuid"

.field private static final COMPONSE_MESSAGE_CONVERSATION_SUMMARY:Ljava/lang/String; = "composeMessageConversationSummary"

.field private static final CONVERSATION_GAMER_TAG:Ljava/lang/String; = "ConversationGamerTag"

.field private static final CONVERSATION_TOPIC:Ljava/lang/String; = "ConversationTopic"

.field private static final FORCE_RELOAD:Ljava/lang/String; = "ForceReload"

.field private static final FORCE_SIGNIN:Ljava/lang/String; = "ForceSignin"

.field private static final FROM_SCREEN:Ljava/lang/String; = "FromScreen"

.field private static final GAME_CLIP:Ljava/lang/String; = "GameClip"

.field private static final GAME_COMPARE_PROGRESS_ACHIEVEMENT:Ljava/lang/String; = "GameCompareProgressAchievement"

.field private static final GAME_PROGRESS_ACHIEVEMENT:Ljava/lang/String; = "GameProgressAchievement"

.field private static final IMAGE_URL:Ljava/lang/String; = "ImageUrl"

.field private static final IS_GROUP_CONVERSATION:Ljava/lang/String; = "isGroupConversation"

.field private static final MESSAGE_ORIGINATING_SCREEN:Ljava/lang/String; = "OriginatingScreen"

.field private static final MULTIPLAYER_FILTERS:Ljava/lang/String; = "MultiplayerFilters"

.field private static final MULTIPLAYER_HANDLE:Ljava/lang/String; = "MultiplayerHandle"

.field private static final MULTIPLAYER_HANDLE_ID:Ljava/lang/String; = "MultiplayerHandleId"

.field private static final MULTIPLAYER_SESSION:Ljava/lang/String; = "MultiplayerSession"

.field private static final ORIGINATING_PAGE:Ljava/lang/String; = "OriginatingPage"

.field private static final PAGEUSER_AUTHORINFO:Ljava/lang/String; = "PageUserAuthorInfo"

.field private static final PROFILE_RECENT_ITEM:Ljava/lang/String; = "ProfileRecentItem"

.field private static final PURCHASE_AVAILABILITY_ID:Ljava/lang/String; = "PurchaseAvailabilityId"

.field private static final PURCHASE_CODE_REDEEMABLE:Ljava/lang/String; = "CodeRedeemable"

.field private static final PURCHASE_FLOW_ID:Ljava/lang/String; = "PurchaseFlowId"

.field private static final PURCHASE_ITEM_CANONICAL_ID:Ljava/lang/String; = "PurchaseItemCanonicalId"

.field private static final PURCHASE_ITEM_HOME_CONSOLE_ID:Ljava/lang/String; = "PurchaseTitleHomeConsoleId"

.field private static final PURCHASE_ORIGINATING_PAGENAME:Ljava/lang/String; = "PurchasePageName"

.field private static final PURCHASE_PRODUCT_ID:Ljava/lang/String; = "ProductId"

.field private static final PURCHASE_REDEMPTION_TOKEN:Ljava/lang/String; = "RedemptionToken"

.field private static final PURCHASE_SKU_ID:Ljava/lang/String; = "SkuId"

.field private static final PURCHASE_TITLE_CONSUMABLE:Ljava/lang/String; = "PurchaseTitleConsumable"

.field private static final PURCHASE_TITLE_TEXT:Ljava/lang/String; = "PurchaseTitleText"

.field private static final RETURN_TO_CONVERSATION_SCREEN:Ljava/lang/String; = "returnToConversationScreen"

.field private static final SCID:Ljava/lang/String; = "Scid"

.field private static final SELECTED_ACTIVITY_DATA:Ljava/lang/String; = "SelectedActivityData"

.field private static final SELECTED_CLUB_ID:Ljava/lang/String; = "SelectedClubId"

.field private static final SELECTED_MEDIA_ITEM:Ljava/lang/String; = "SelectedMediaItem"

.field private static final SELECTED_PROFILE:Ljava/lang/String; = "SelectedProfile"

.field private static final SENDER_XUID:Ljava/lang/String; = "SenderXuid"

.field private static final SHARE_TO_SHOWCASE:Ljava/lang/String; = "ShareToShowcase"

.field private static final SHOULD_AUTO_FOCUS:Ljava/lang/String; = "ShouldAutoFocus"

.field private static final TIMELINE_ID:Ljava/lang/String; = "TimelineId"

.field private static final TIMELINE_TYPE:Ljava/lang/String; = "TimelineType"

.field private static final TITLE_DETAIL_INFO:Ljava/lang/String; = "TitleDetailInfo"

.field private static final TITLE_ID:Ljava/lang/String; = "TitleId"

.field private static final TITLE_NAME:Ljava/lang/String; = "TitleName"

.field private static final TITLE_SIGNED_OFFER:Ljava/lang/String; = "TitleSignedOffer"

.field private static final TRENDING_TOPIC:Ljava/lang/String; = "TrendingTopic"

.field private static final USER_STATS:Ljava/lang/String; = "UserStats"

.field private static final VIEW_ALL_MEMBERS:Ljava/lang/String; = "viewAllMembers"

.field private static final VIEW_MEMBER_CONVERSATION_ID:Ljava/lang/String; = "viewMemberConversationId"

.field private static final WELCOME_ALT_BUTTON_TEXT:Ljava/lang/String; = "WelcomeAltButtonText"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 92
    return-void
.end method

.method private getBoolean(Ljava/lang/String;)Z
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 300
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getActivityFeedActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 2

    .prologue
    .line 290
    const-string v1, "ActivityFeedActionType"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 291
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActivityFeedItemLocator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    const-string v0, "ActivityFeedItemLocator"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAppRelaunchIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 466
    const-string v1, "AppRelaunchIntent"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 467
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Landroid/content/Intent;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/content/Intent;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    .locals 1

    .prologue
    .line 312
    const-string v0, "BIData"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    return-object v0
.end method

.method public getCompareAchievementDetailFriendXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    const-string v0, "CompareAchievementDetailFriendXuid"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getComposeMessageConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 569
    const-string v1, "composeMessageConversationSummary"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 570
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConversationGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    const-string v0, "ConversationGamerTag"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getConversationTopic()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 325
    const-string v1, "ConversationTopic"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 326
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getForceSignin()Z
    .locals 1

    .prologue
    .line 449
    const-string v0, "ForceSignin"

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1

    .prologue
    .line 129
    const-string v0, "FromScreen"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method public getGameClip()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 1

    .prologue
    .line 149
    const-string v0, "GameClip"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    return-object v0
.end method

.method public getGameCompareProgressAchievement()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .locals 2

    .prologue
    .line 431
    const-string v1, "GameCompareProgressAchievement"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 432
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGameProgressAchievement()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .locals 2

    .prologue
    .line 422
    const-string v1, "GameProgressAchievement"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 423
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    const-string v0, "ImageUrl"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMessagesOriginatingScreen()Ljava/lang/Class;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 589
    const-string v0, "OriginatingScreen"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method public getMultiplayerFilters()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 514
    const-string v1, "MultiplayerFilters"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 515
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMultiplayerHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 525
    const-string v1, "MultiplayerHandle"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 526
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMultiplayerHandleId()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 536
    const-string v1, "MultiplayerHandleId"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 537
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMultiplayerSession()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 547
    const-string v1, "MultiplayerSession"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 548
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPageUserAuthorInfo()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .locals 2

    .prologue
    .line 413
    const-string v1, "PageUserAuthorInfo"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 414
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 2

    .prologue
    .line 281
    const-string v1, "ProfileRecentItem"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 282
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPurchaseAvailabilityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    const-string v0, "PurchaseAvailabilityId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseFlowId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    const-string v0, "PurchaseFlowId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseItemCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    const-string v0, "PurchaseItemCanonicalId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseOriginatingPage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    const-string v0, "PurchasePageName"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const-string v0, "ProductId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseSkuId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    const-string v0, "SkuId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const-string v0, "PurchaseTitleText"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRedemptionToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    const-string v0, "RedemptionToken"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReturnToConversationDetailsScreen()Z
    .locals 1

    .prologue
    .line 185
    const-string v0, "returnToConversationScreen"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "returnToConversationScreen"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 105
    const-string v0, "SelectedActivityData"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    return-object v0
.end method

.method public getSelectedClubId()Ljava/lang/Long;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 472
    const-string v1, "SelectedClubId"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 473
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 113
    const-string v0, "SelectedMediaItem"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method public getSelectedProfile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string v0, "SelectedProfile"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedScid()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 503
    const-string v1, "Scid"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 504
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSenderXuid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    const-string v1, "SenderXuid"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 335
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShareToShowcase()Z
    .locals 2

    .prologue
    .line 351
    const-string v1, "ShareToShowcase"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 352
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getShouldAutoFocus()Z
    .locals 2

    .prologue
    .line 360
    const-string v1, "ShouldAutoFocus"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 361
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSourcePage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    const-string v0, "OriginatingPage"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTimelineId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 492
    const-string v1, "TimelineId"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 493
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 2

    .prologue
    .line 483
    const-string v1, "TimelineType"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 484
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    goto :goto_0
.end method

.method public getTitleDetailInfo()Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;
    .locals 2

    .prologue
    .line 440
    const-string v1, "TitleDetailInfo"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 441
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitleId()J
    .locals 4

    .prologue
    .line 395
    const-string v1, "TitleId"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 396
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_0
    return-wide v2

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 404
    const-string v1, "TitleName"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 405
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTitleSignedOffer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string v0, "TitleSignedOffer"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTrendingTopic()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    .locals 2

    .prologue
    .line 386
    const-string v1, "TrendingTopic"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 387
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUserStats()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 1

    .prologue
    .line 369
    const-string v0, "UserStats"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    return-object v0
.end method

.method public getViewAllMembers()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    const-string v0, "viewAllMembers"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public getViewMemberConversationId()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 558
    const-string v1, "viewMemberConversationId"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 559
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWelcomeAltButtonText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 457
    const-string v1, "WelcomeAltButtonText"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 458
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public isCodeRedeemable()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 261
    const-string v0, "CodeRedeemable"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isForceReload()Z
    .locals 1

    .prologue
    .line 141
    const-string v0, "ForceReload"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ForceReload"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGroupConversation()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    const-string v0, "isGroupConversation"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isPurchaseItemConsumable()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 197
    const-string v0, "PurchaseTitleConsumable"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isPurchaseItemHomeConsoleExist()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 205
    const-string v0, "PurchaseTitleHomeConsoleId"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public putActivityFeedActionType(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 295
    const-string v0, "ActivityFeedActionType"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    return-void
.end method

.method public putActivityFeedItemLocator(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 308
    const-string v0, "ActivityFeedItemLocator"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    return-void
.end method

.method public putAppRelaunchIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "relaunchIntent"    # Landroid/content/Intent;

    .prologue
    .line 462
    const-string v0, "AppRelaunchIntent"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    return-void
.end method

.method public putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V
    .locals 1
    .param p1, "biData"    # Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    .prologue
    .line 316
    const-string v0, "BIData"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    return-void
.end method

.method public putCodeRedeemable(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 257
    const-string v0, "CodeRedeemable"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    return-void
.end method

.method public putCompareAchievementDetailFriendXuid(Ljava/lang/String;)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 374
    const-string v0, "CompareAchievementDetailFriendXuid"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    return-void
.end method

.method public putComposeMessageConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 1
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 574
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 575
    const-string v0, "composeMessageConversationSummary"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    return-void
.end method

.method public putConversationGamerTag(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 173
    const-string v0, "ConversationGamerTag"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    return-void
.end method

.method public putConversationTopic(Ljava/lang/String;)V
    .locals 1
    .param p1, "topic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 330
    const-string v0, "ConversationTopic"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    return-void
.end method

.method public putForceSignin(Z)V
    .locals 2
    .param p1, "force"    # Z

    .prologue
    .line 445
    const-string v0, "ForceSignin"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    return-void
.end method

.method public putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 133
    const-string v0, "FromScreen"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    return-void
.end method

.method public putGameCompareProgressAchievement(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    .line 427
    const-string v0, "GameCompareProgressAchievement"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    return-void
.end method

.method public putGameProgressAchievement(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    .line 418
    const-string v0, "GameProgressAchievement"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    return-void
.end method

.method public putImageUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 273
    const-string v0, "ImageUrl"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    return-void
.end method

.method public putIsGroupConversation(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 169
    const-string v0, "isGroupConversation"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    return-void
.end method

.method public putMessagesOriginatingScreen(Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 593
    .local p1, "screen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 594
    const-string v0, "OriginatingScreen"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    return-void
.end method

.method public putMultiplayerHandle(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 1
    .param p1, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 530
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 531
    const-string v0, "MultiplayerHandle"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    return-void
.end method

.method public putMultiplayerHandleFilters(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V
    .locals 1
    .param p1, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 519
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 520
    const-string v0, "MultiplayerFilters"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    return-void
.end method

.method public putMultiplayerHandleId(Ljava/lang/String;)V
    .locals 1
    .param p1, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 541
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 542
    const-string v0, "MultiplayerHandleId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    return-void
.end method

.method public putMultiplayerSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 1
    .param p1, "session"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 552
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 553
    const-string v0, "MultiplayerSession"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    return-void
.end method

.method public putPageUserAuthorInfo(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .prologue
    .line 409
    const-string v0, "PageUserAuthorInfo"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    return-void
.end method

.method public putProfileRecentItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 286
    const-string v0, "ProfileRecentItem"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    return-void
.end method

.method public putPurchaseAvailabilityId(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v0, "PurchaseAvailabilityId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    return-void
.end method

.method public putPurchaseFlowId(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 225
    const-string v0, "PurchaseFlowId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    return-void
.end method

.method public putPurchaseItemCanonicalId(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string v0, "PurchaseItemCanonicalId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    return-void
.end method

.method public putPurchaseItemConsumable(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 193
    const-string v0, "PurchaseTitleConsumable"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    return-void
.end method

.method public putPurchaseItemHomeConsoleExist(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 201
    const-string v0, "PurchaseTitleHomeConsoleId"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    return-void
.end method

.method public putPurchaseOriginatingPage(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 233
    const-string v0, "PurchasePageName"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    return-void
.end method

.method public putPurchaseProductId(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 241
    const-string v0, "ProductId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    return-void
.end method

.method public putPurchaseSkuId(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 249
    const-string v0, "SkuId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    return-void
.end method

.method public putRedemptionToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string v0, "RedemptionToken"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    return-void
.end method

.method public putReturnToConversationDetailsScreen(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 189
    const-string v0, "returnToConversationScreen"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method public putSelectedActivityData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 1
    .param p1, "data"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 109
    const-string v0, "SelectedActivityData"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method public putSelectedClubId(Ljava/lang/Long;)V
    .locals 4
    .param p1, "clubId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation

        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 477
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 478
    const-wide/16 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 479
    const-string v0, "SelectedClubId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    return-void
.end method

.method public putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 117
    const-string v0, "SelectedMediaItem"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-void
.end method

.method public putSelectedProfile(Ljava/lang/String;)V
    .locals 1
    .param p1, "profileXuid"    # Ljava/lang/String;

    .prologue
    .line 125
    const-string v0, "SelectedProfile"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method

.method public putSelectedScid(Ljava/lang/String;)V
    .locals 1
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 508
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 509
    const-string v0, "Scid"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    return-void
.end method

.method public putSenderXuid(Ljava/lang/String;)V
    .locals 1
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 320
    const-string v0, "SenderXuid"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    return-void
.end method

.method public putShareToShowcase(Z)V
    .locals 2
    .param p1, "shareToShowcase"    # Z

    .prologue
    .line 347
    const-string v0, "ShareToShowcase"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    return-void
.end method

.method public putShouldAutoFocus(Z)V
    .locals 2
    .param p1, "shouldAutoFocus"    # Z

    .prologue
    .line 356
    const-string v0, "ShouldAutoFocus"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    return-void
.end method

.method public putSourcePage(Ljava/lang/String;)V
    .locals 1
    .param p1, "pageName"    # Ljava/lang/String;

    .prologue
    .line 339
    const-string v0, "OriginatingPage"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    return-void
.end method

.method public putTimelineId(Ljava/lang/String;)V
    .locals 1
    .param p1, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 497
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 498
    const-string v0, "TimelineId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    return-void
.end method

.method public putTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)V
    .locals 1
    .param p1, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .prologue
    .line 488
    const-string v0, "TimelineType"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    return-void
.end method

.method public putTitleDetailInfo(Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    .prologue
    .line 436
    const-string v0, "TitleDetailInfo"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    return-void
.end method

.method public putTitleId(J)V
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 391
    const-string v0, "TitleId"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    return-void
.end method

.method public putTitleName(Ljava/lang/String;)V
    .locals 1
    .param p1, "titleName"    # Ljava/lang/String;

    .prologue
    .line 400
    const-string v0, "TitleName"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    return-void
.end method

.method public putTrendingTopic(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V
    .locals 1
    .param p1, "trendingTopic"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .prologue
    .line 382
    const-string v0, "TrendingTopic"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    return-void
.end method

.method public putUserStats(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 1
    .param p1, "stats"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 365
    const-string v0, "UserStats"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    return-void
.end method

.method public putViewAllMembers(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 580
    .local p1, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    const-string v0, "viewAllMembers"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    return-void
.end method

.method public putViewMemberConversationId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 563
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 564
    const-string v0, "viewMemberConversationId"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    return-void
.end method

.method public putWelcomeAltButtonText(Ljava/lang/String;)V
    .locals 1
    .param p1, "buttonText"    # Ljava/lang/String;

    .prologue
    .line 453
    const-string v0, "WelcomeAltButtonText"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    return-void
.end method

.method public setForceReload(Z)V
    .locals 2
    .param p1, "forceReload"    # Z

    .prologue
    .line 137
    const-string v0, "ForceReload"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public setGameClip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 1
    .param p1, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 145
    const-string v0, "GameClip"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 95
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 96
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 97
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPurchaseTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "titleName"    # Ljava/lang/String;

    .prologue
    .line 161
    const-string v0, "PurchaseTitleText"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    return-void
.end method

.method public setTitleSignedOffer(Ljava/lang/String;)V
    .locals 1
    .param p1, "signedOffer"    # Ljava/lang/String;

    .prologue
    .line 153
    const-string v0, "TitleSignedOffer"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    return-void
.end method
