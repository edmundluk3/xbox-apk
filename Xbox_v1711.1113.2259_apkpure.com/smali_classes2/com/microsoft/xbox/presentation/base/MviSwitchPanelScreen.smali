.class public abstract Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.super Lcom/microsoft/xbox/presentation/base/MviActivityBase;
.source "MviSwitchPanelScreen.java"


# instance fields
.field protected switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0a95
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected abstract getContentScreenId()I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end method

.method protected getErrorScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 81
    const v0, 0x7f030132

    return v0
.end method

.method protected getInvalidScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 111
    const v0, 0x7f030134

    return v0
.end method

.method protected getLoadingScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 101
    const v0, 0x7f030133

    return v0
.end method

.method protected getNoContentScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 91
    const v0, 0x7f030134

    return v0
.end method

.method protected getRootViewId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 65
    const v0, 0x7f030223

    return v0
.end method

.method protected setContentView()V
    .locals 6

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getRootViewId()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->setContentView(I)V

    .line 37
    const v5, 0x7f0e0a96

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 38
    .local v4, "validStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getContentScreenId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 39
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 41
    const v5, 0x7f0e0a97

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 42
    .local v0, "errorStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getErrorScreenId()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 43
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 45
    const v5, 0x7f0e0a98

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 46
    .local v3, "noContentStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getNoContentScreenId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 47
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 49
    const v5, 0x7f0e0a99

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 50
    .local v2, "loadingStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getLoadingScreenId()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 51
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 53
    const v5, 0x7f0e0a9a

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 54
    .local v1, "invalidStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->getInvalidScreenId()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 55
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 56
    return-void
.end method
