.class final Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;
.super Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;
.source "AutoValue_RtaDataTypes_RtaSubscriptionResponse.java"


# instance fields
.field private final errorReason:Ljava/lang/String;

.field private final errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

.field private final payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

.field private final sequenceNumber:I

.field private final subscriptionId:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;ILcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;Ljava/lang/String;)V
    .locals 2
    .param p1, "subscriptionId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "payload"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "sequenceNumber"    # I
    .param p4, "errorType"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .param p5, "errorReason"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    .line 24
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    .line 25
    iput p3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->sequenceNumber:I

    .line 26
    if-nez p4, :cond_0

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null errorType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 30
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 78
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 79
    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;

    .line 80
    .local v0, "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    if-nez v3, :cond_4

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->sequenceNumber:I

    .line 82
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->errorType()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 84
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->errorReason()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 80
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 81
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 84
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->errorReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;
    :cond_6
    move v1, v2

    .line 86
    goto :goto_0
.end method

.method public errorReason()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    return-object v0
.end method

.method public errorType()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 91
    const/4 v0, 0x1

    .line 92
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 94
    mul-int/2addr v0, v3

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v3

    .line 97
    iget v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->sequenceNumber:I

    xor-int/2addr v0, v1

    .line 98
    mul-int/2addr v0, v3

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 100
    mul-int/2addr v0, v3

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 102
    return v0

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    return-object v0
.end method

.method public sequenceNumber()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->sequenceNumber:I

    return v0
.end method

.method public subscriptionId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtaSubscriptionResponse{subscriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sequenceNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->sequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorType:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;->errorReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
