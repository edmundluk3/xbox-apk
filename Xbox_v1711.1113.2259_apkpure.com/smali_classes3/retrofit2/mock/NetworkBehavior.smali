.class public final Lretrofit2/mock/NetworkBehavior;
.super Ljava/lang/Object;
.source "NetworkBehavior.java"


# static fields
.field private static final DEFAULT_DELAY_MS:I = 0x7d0

.field private static final DEFAULT_ERROR_PERCENT:I = 0x0

.field private static final DEFAULT_FAILURE_PERCENT:I = 0x3

.field private static final DEFAULT_VARIANCE_PERCENT:I = 0x28


# instance fields
.field private volatile delayMs:J

.field private volatile errorFactory:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lretrofit2/Response",
            "<*>;>;"
        }
    .end annotation
.end field

.field private volatile errorPercent:I

.field private volatile failureException:Ljava/lang/Throwable;

.field private volatile failurePercent:I

.field private final random:Ljava/util/Random;

.field private volatile variancePercent:I


# direct methods
.method private constructor <init>(Ljava/util/Random;)V
    .locals 3
    .param p1, "random"    # Ljava/util/Random;

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lretrofit2/mock/NetworkBehavior;->delayMs:J

    .line 65
    const/16 v0, 0x28

    iput v0, p0, Lretrofit2/mock/NetworkBehavior;->variancePercent:I

    .line 66
    const/4 v0, 0x3

    iput v0, p0, Lretrofit2/mock/NetworkBehavior;->failurePercent:I

    .line 68
    iput v2, p0, Lretrofit2/mock/NetworkBehavior;->errorPercent:I

    .line 69
    new-instance v0, Lretrofit2/mock/NetworkBehavior$1;

    invoke-direct {v0, p0}, Lretrofit2/mock/NetworkBehavior$1;-><init>(Lretrofit2/mock/NetworkBehavior;)V

    iput-object v0, p0, Lretrofit2/mock/NetworkBehavior;->errorFactory:Ljava/util/concurrent/Callable;

    .line 76
    iput-object p1, p0, Lretrofit2/mock/NetworkBehavior;->random:Ljava/util/Random;

    .line 78
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mock failure!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lretrofit2/mock/NetworkBehavior;->failureException:Ljava/lang/Throwable;

    .line 79
    iget-object v0, p0, Lretrofit2/mock/NetworkBehavior;->failureException:Ljava/lang/Throwable;

    new-array v1, v2, [Ljava/lang/StackTraceElement;

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 80
    return-void
.end method

.method public static create()Lretrofit2/mock/NetworkBehavior;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lretrofit2/mock/NetworkBehavior;

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-direct {v0, v1}, Lretrofit2/mock/NetworkBehavior;-><init>(Ljava/util/Random;)V

    return-object v0
.end method

.method public static create(Ljava/util/Random;)Lretrofit2/mock/NetworkBehavior;
    .locals 2
    .param p0, "random"    # Ljava/util/Random;

    .prologue
    .line 58
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "random == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    new-instance v0, Lretrofit2/mock/NetworkBehavior;

    invoke-direct {v0, p0}, Lretrofit2/mock/NetworkBehavior;-><init>(Ljava/util/Random;)V

    return-object v0
.end method


# virtual methods
.method public calculateDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 10
    .param p1, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 201
    iget v7, p0, Lretrofit2/mock/NetworkBehavior;->variancePercent:I

    int-to-float v7, v7

    const/high16 v8, 0x42c80000    # 100.0f

    div-float v4, v7, v8

    .line 202
    .local v4, "delta":F
    sub-float v5, v9, v4

    .line 203
    .local v5, "lowerBound":F
    add-float v6, v9, v4

    .line 204
    .local v6, "upperBound":F
    sub-float v0, v6, v5

    .line 205
    .local v0, "bound":F
    iget-object v7, p0, Lretrofit2/mock/NetworkBehavior;->random:Ljava/util/Random;

    invoke-virtual {v7}, Ljava/util/Random;->nextFloat()F

    move-result v7

    mul-float/2addr v7, v0

    add-float v1, v5, v7

    .line 206
    .local v1, "delayPercent":F
    iget-wide v8, p0, Lretrofit2/mock/NetworkBehavior;->delayMs:J

    long-to-float v7, v8

    mul-float/2addr v7, v1

    float-to-long v2, v7

    .line 207
    .local v2, "callDelayMs":J
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v2, v3, p1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    return-wide v8
.end method

.method public calculateIsError()Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lretrofit2/mock/NetworkBehavior;->random:Ljava/util/Random;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iget v1, p0, Lretrofit2/mock/NetworkBehavior;->errorPercent:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public calculateIsFailure()Z
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lretrofit2/mock/NetworkBehavior;->random:Ljava/util/Random;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iget v1, p0, Lretrofit2/mock/NetworkBehavior;->failurePercent:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createErrorResponse()Lretrofit2/Response;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Response",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 167
    :try_start_0
    iget-object v2, p0, Lretrofit2/mock/NetworkBehavior;->errorFactory:Ljava/util/concurrent/Callable;

    invoke-interface {v2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Response;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    .local v0, "call":Lretrofit2/Response;, "Lretrofit2/Response<*>;"
    if-nez v0, :cond_0

    .line 172
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Error factory returned null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 168
    .end local v0    # "call":Lretrofit2/Response;, "Lretrofit2/Response<*>;"
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Error factory threw an exception."

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 174
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "call":Lretrofit2/Response;, "Lretrofit2/Response<*>;"
    :cond_0
    invoke-virtual {v0}, Lretrofit2/Response;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Error factory returned successful response."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 177
    :cond_1
    return-object v0
.end method

.method public delay(Ljava/util/concurrent/TimeUnit;)J
    .locals 4
    .param p1, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 92
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Lretrofit2/mock/NetworkBehavior;->delayMs:J

    invoke-virtual {v0, v2, v3, p1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public errorPercent()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lretrofit2/mock/NetworkBehavior;->errorPercent:I

    return v0
.end method

.method public failureException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lretrofit2/mock/NetworkBehavior;->failureException:Ljava/lang/Throwable;

    return-object v0
.end method

.method public failurePercent()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lretrofit2/mock/NetworkBehavior;->failurePercent:I

    return v0
.end method

.method public setDelay(JLjava/util/concurrent/TimeUnit;)V
    .locals 3
    .param p1, "amount"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 84
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Amount must be positive value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lretrofit2/mock/NetworkBehavior;->delayMs:J

    .line 88
    return-void
.end method

.method public setErrorFactory(Ljava/util/concurrent/Callable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lretrofit2/Response",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "errorFactory":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lretrofit2/Response<*>;>;"
    if-nez p1, :cond_0

    .line 158
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "errorFactory == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    iput-object p1, p0, Lretrofit2/mock/NetworkBehavior;->errorFactory:Ljava/util/concurrent/Callable;

    .line 161
    return-void
.end method

.method public setErrorPercent(I)V
    .locals 2
    .param p1, "errorPercent"    # I

    .prologue
    .line 146
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 147
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error percentage must be between 0 and 100."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_1
    iput p1, p0, Lretrofit2/mock/NetworkBehavior;->errorPercent:I

    .line 150
    return-void
.end method

.method public setFailureException(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 129
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "exception == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_0
    iput-object p1, p0, Lretrofit2/mock/NetworkBehavior;->failureException:Ljava/lang/Throwable;

    .line 132
    return-void
.end method

.method public setFailurePercent(I)V
    .locals 2
    .param p1, "failurePercent"    # I

    .prologue
    .line 110
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 111
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failure percentage must be between 0 and 100."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_1
    iput p1, p0, Lretrofit2/mock/NetworkBehavior;->failurePercent:I

    .line 114
    return-void
.end method

.method public setVariancePercent(I)V
    .locals 2
    .param p1, "variancePercent"    # I

    .prologue
    .line 97
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Variance percentage must be between 0 and 100."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    iput p1, p0, Lretrofit2/mock/NetworkBehavior;->variancePercent:I

    .line 101
    return-void
.end method

.method public variancePercent()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lretrofit2/mock/NetworkBehavior;->variancePercent:I

    return v0
.end method
