.class public Lcom/microsoft/cll/android/PreSerializedEvent;
.super Ljava/lang/Object;
.source "PreSerializedEvent.java"


# instance fields
.field public attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public data:Ljava/lang/String;

.field public partBName:Ljava/lang/String;

.field public partCName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "partCName"    # Ljava/lang/String;
    .param p3, "partBName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 11
    .local p4, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/microsoft/cll/android/PreSerializedEvent;->data:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/microsoft/cll/android/PreSerializedEvent;->partBName:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/microsoft/cll/android/PreSerializedEvent;->partCName:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/microsoft/cll/android/PreSerializedEvent;->attributes:Ljava/util/Map;

    .line 16
    return-void
.end method
