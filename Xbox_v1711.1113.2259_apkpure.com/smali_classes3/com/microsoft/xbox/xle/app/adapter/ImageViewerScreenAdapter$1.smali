.class Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;
.super Ljava/lang/Object;
.source "ImageViewerScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getScaled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->handleTouch(Landroid/view/MotionEvent;)Z

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/ScaleGestureDetector;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 78
    const/4 v0, 0x1

    return v0
.end method
