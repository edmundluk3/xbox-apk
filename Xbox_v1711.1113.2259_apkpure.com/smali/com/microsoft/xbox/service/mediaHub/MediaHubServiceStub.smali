.class public final Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;
.super Ljava/lang/Object;
.source "MediaHubServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;


# static fields
.field private static final BROADCAST_SEARCH_FILE:Ljava/lang/String; = "stubdata/MediaHubBroadcastSearch.json"

.field private static final GAMECLIP_BATCH_FILE:Ljava/lang/String; = "stubdata/MediaHubGameclipBatch.json"

.field private static final GAMECLIP_SEARCH_FILE:Ljava/lang/String; = "stubdata/MediaHubGameclipSearch.json"

.field private static final SCREENSHOT_BATCH_FILE:Ljava/lang/String; = "stubdata/MediaHubScreenshotBatch.json"

.field private static final SCREENSHOT_SEARCH_FILE:Ljava/lang/String; = "stubdata/MediaHubScreenshotSearch.json"


# instance fields
.field private final delegate:Lretrofit2/mock/BehaviorDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/mock/BehaviorDelegate",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lretrofit2/mock/MockRetrofit;)V
    .locals 1
    .param p1, "mockRetrofit"    # Lretrofit2/mock/MockRetrofit;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-class v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-virtual {p1, v0}, Lretrofit2/mock/MockRetrofit;->create(Ljava/lang/Class;)Lretrofit2/mock/BehaviorDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    .line 44
    return-void
.end method

.method static synthetic lambda$createCustomPicRx$0(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .locals 0
    .param p0, "response"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 123
    return-object p0
.end method


# virtual methods
.method public createCustomPic(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lretrofit2/Call;
    .locals 4
    .param p1, "createRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 116
    const-string/jumbo v1, "test-contendId"

    const-string/jumbo v2, "test-publishUri"

    const-string/jumbo v3, "test-contentUploadUri"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;

    move-result-object v0

    .line 117
    .local v0, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v1, v0}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->createCustomPic(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lretrofit2/Call;

    move-result-object v1

    return-object v1
.end method

.method public createCustomPicRx(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lio/reactivex/Single;
    .locals 4
    .param p1, "createRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    const-string/jumbo v1, "test-contendId"

    const-string/jumbo v2, "test-publishUri"

    const-string/jumbo v3, "test-contentUploadUri"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;

    move-result-object v0

    .line 123
    .local v0, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-static {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)Ljava/util/concurrent/Callable;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v2

    invoke-virtual {v1, v2}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->createCustomPicRx(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lio/reactivex/Single;

    move-result-object v1

    return-object v1
.end method

.method public getBroadcasts(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lio/reactivex/Single;
    .locals 4
    .param p1, "searchRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 130
    const/4 v1, 0x0

    .line 132
    .local v1, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string/jumbo v3, "stubdata/MediaHubBroadcastSearch.json"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v2, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getBroadcasts(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lio/reactivex/Single;

    move-result-object v2

    return-object v2

    .line 133
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;
    .locals 4
    .param p1, "batchRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    const/4 v1, 0x0

    .line 63
    .local v1, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string/jumbo v3, "stubdata/MediaHubGameclipBatch.json"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v2, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;

    move-result-object v2

    return-object v2

    .line 64
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;
    .locals 4
    .param p1, "searchRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 74
    const/4 v1, 0x0

    .line 77
    .local v1, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string/jumbo v3, "stubdata/MediaHubGameclipSearch.json"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v2, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v2

    return-object v2

    .line 78
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;
    .locals 4
    .param p1, "batchRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 88
    const/4 v1, 0x0

    .line 91
    .local v1, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string/jumbo v3, "stubdata/MediaHubScreenshotBatch.json"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v2, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;

    move-result-object v2

    return-object v2

    .line 92
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;
    .locals 4
    .param p1, "searchRequest"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 102
    const/4 v1, 0x0

    .line 105
    .local v1, "response":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string/jumbo v3, "stubdata/MediaHubScreenshotSearch.json"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    move-object v1, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-virtual {v2, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v2

    return-object v2

    .line 106
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public incrementGameclipViewCount(Ljava/lang/String;)Lio/reactivex/Completable;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "contentId"
        .end annotation
    .end param

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->incrementGameclipViewCount(Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public incrementScreenshotViewCount(Ljava/lang/String;)Lio/reactivex/Completable;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "contentId"
        .end annotation
    .end param

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;->delegate:Lretrofit2/mock/BehaviorDelegate;

    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/mock/BehaviorDelegate;->returningResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->incrementScreenshotViewCount(Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method
