.class public abstract Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;
.super Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEvent;
.source "RecyclerViewChildDetachEvent.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEvent;-><init>()V

    .line 25
    return-void
.end method

.method public static create(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;
    .locals 1
    .param p0, "view"    # Landroid/support/v7/widget/RecyclerView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "child"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;

    invoke-direct {v0, p0, p1}, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewChildDetachEvent;-><init>(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    return-object v0
.end method
