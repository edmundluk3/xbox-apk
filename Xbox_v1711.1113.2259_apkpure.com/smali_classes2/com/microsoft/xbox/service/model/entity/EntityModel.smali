.class public Lcom/microsoft/xbox/service/model/entity/EntityModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "EntityModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/entity/Entity;",
        ">;"
    }
.end annotation


# static fields
.field public static final CACHE_SIZE:I = 0x80

.field private static final TAG:Ljava/lang/String;

.field private static final map:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/entity/EntityModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private entity:Lcom/microsoft/xbox/service/model/entity/Entity;

.field private hasNoContent:Z

.field private loadFailed:Z

.field private final locator:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->TAG:Ljava/lang/String;

    .line 31
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "locator"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 34
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent:Z

    .line 35
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadFailed:Z

    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->locator:Ljava/lang/String;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;-><init>(Lcom/microsoft/xbox/service/model/entity/EntityModel;Lcom/microsoft/xbox/service/model/entity/EntityModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 58
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->lifetime:J

    .line 59
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/entity/EntityModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->locator:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .locals 3
    .param p0, "locator"    # Ljava/lang/String;

    .prologue
    .line 38
    sget-object v2, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    monitor-enter v2

    .line 39
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .line 40
    .local v0, "model":Lcom/microsoft/xbox/service/model/entity/EntityModel;
    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/entity/EntityModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;-><init>(Ljava/lang/String;)V

    .line 42
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/entity/EntityModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_0
    monitor-exit v2

    return-object v0

    .line 45
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/entity/EntityModel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static reset()V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 50
    sget-object v1, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->map:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 52
    monitor-exit v1

    .line 53
    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static toAchievement(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    .locals 4
    .param p0, "json"    # Lcom/google/gson/JsonObject;

    .prologue
    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Ljava/util/Date;

    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;-><init>()V

    .line 170
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 173
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    return-object v1
.end method

.method public static toActivityFeedItem(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 4
    .param p0, "json"    # Lcom/google/gson/JsonObject;

    .prologue
    .line 178
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Ljava/util/Date;

    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;-><init>()V

    .line 179
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 180
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 182
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    return-object v1
.end method

.method public static toGameClip(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 2
    .param p0, "json"    # Lcom/google/gson/JsonObject;

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 162
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 164
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    return-object v1
.end method


# virtual methods
.method public getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->entity:Lcom/microsoft/xbox/service/model/entity/Entity;

    return-object v0
.end method

.method public getLocator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->locator:Ljava/lang/String;

    return-object v0
.end method

.method public hasNoContent()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent:Z

    return v0
.end method

.method public loadFailed()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadFailed:Z

    return v0
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/entity/Entity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/ModelBase;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/entity/Entity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/entity/Entity;>;"
    const/4 v2, 0x1

    .line 88
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/entity/Entity;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->entity:Lcom/microsoft/xbox/service/model/entity/Entity;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->entity:Lcom/microsoft/xbox/service/model/entity/Entity;

    if-nez v0, :cond_0

    .line 92
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent:Z

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load entity data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadFailed:Z

    goto :goto_0
.end method
