.class public Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;
.super Ljava/lang/Object;
.source "GameProgressXboxoneAchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaAsset"
.end annotation


# instance fields
.field private mediaAssetUri:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMediaAssetUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->mediaAssetUri:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->mediaAssetUri:Ljava/lang/String;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->mediaAssetUri:Ljava/lang/String;

    return-object v0
.end method
