.class abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;
.super Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
.source "$AutoValue_PrivacyDataTypes_PermissionValues.java"


# instance fields
.field private final isAllowed:Z

.field private final reasons:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ZLcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "isAllowed"    # Z
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "reasons":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;-><init>()V

    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->isAllowed:Z

    .line 18
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->isAllowed:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->isAllowed()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_3

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->reasons()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->reasons()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    :cond_4
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 57
    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->isAllowed:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v2

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    xor-int/2addr v0, v1

    .line 60
    return v0

    .line 57
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public isAllowed()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->isAllowed:Z

    return v0
.end method

.method public reasons()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PermissionValues{isAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->isAllowed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reasons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionValues;->reasons:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
