.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RoleInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 701
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_RoleInfo$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_RoleInfo$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(I)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
    .locals 4
    .param p0, "needCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 668
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 669
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_RoleInfo;

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->with(I)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_RoleInfo;-><init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;)V

    return-object v0
.end method


# virtual methods
.method public getConfirmedCount()I
    .locals 1

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 692
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 693
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->count()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 694
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->count()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 697
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNeedCount()I
    .locals 2

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 674
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 675
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 676
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->target()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 677
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->target()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 679
    .local v0, "needCount":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->count()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 680
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;->count()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    .line 686
    .end local v0    # "needCount":I
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract lfg()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
