.class Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;
.super Ljava/lang/Object;
.source "FriendsWhoPlayScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 60
    .local v0, "gamer":Lcom/microsoft/xbox/service/model/FollowersData;
    if-eqz v0, :cond_0

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gotoCompareAchievements(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 63
    :cond_0
    return-void
.end method
