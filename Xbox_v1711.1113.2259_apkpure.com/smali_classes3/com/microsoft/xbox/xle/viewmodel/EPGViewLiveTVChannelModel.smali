.class public Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;
.source "EPGViewLiveTVChannelModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;


# instance fields
.field private final mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 0
    .param p1, "data"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 17
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->hasListeners()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 89
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 90
    return-void
.end method

.method public getChannelGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    return-object v0
.end method

.method public getHDEquivalent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelHDEquivalent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeadendId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->LiveTV:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    return-object v0
.end method

.method public isAdult()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getIsAdult()Z

    move-result v0

    return v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v0

    return v0
.end method

.method public isHD()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getIsHD()Z

    move-result v0

    return v0
.end method

.method public isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
    .locals 3
    .param p1, "obj"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 23
    const/4 v1, 0x0

    .line 26
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 25
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;

    .line 26
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isTheSameChannel(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPropertyChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "favorite"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->notifyIsFavoriteChanged()V

    .line 121
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->hasListeners()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 98
    :cond_0
    return-void
.end method

.method public toggleFavorite()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleFavorite()V

    .line 82
    return-void
.end method

.method public tuneToChannel()V
    .locals 6

    .prologue
    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 103
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "ShowTuned"

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;->mChannelInfo:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getTuneToUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleLocation()Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
