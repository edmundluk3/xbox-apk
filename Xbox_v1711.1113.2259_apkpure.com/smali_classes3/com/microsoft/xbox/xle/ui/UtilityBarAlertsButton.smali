.class public Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;
.super Landroid/widget/LinearLayout;
.source "UtilityBarAlertsButton.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_COUNT:I = 0x63


# instance fields
.field private alertCount:I

.field private alertNoTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    .line 42
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 43
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f03026b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 44
    const v1, 0x7f0e0bbe

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    .line 46
    new-instance v1, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton$1;-><init>(Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    .prologue
    .line 26
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    return v0
.end method


# virtual methods
.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 62
    .local v3, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v3, :cond_2

    .line 63
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 64
    .local v2, "data":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityAlertsSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v5, v6, :cond_7

    .line 65
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfActivityAlerts()I

    move-result v5

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNumberOfRecentChangeCount()I

    move-result v6

    add-int v0, v5, v6

    .line 66
    .local v0, "alertCount":I
    :goto_0
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    if-eq v5, v0, :cond_2

    .line 67
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    .line 69
    const/4 v1, 0x0

    .line 70
    .local v1, "alertNumberText":Ljava/lang/String;
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    const/16 v6, 0x63

    if-le v5, v6, :cond_4

    .line 71
    const-string v1, "99+"

    .line 76
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    invoke-static {v5, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 78
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 82
    :cond_1
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    if-nez v5, :cond_5

    .line 83
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d41

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    .end local v0    # "alertCount":I
    .end local v1    # "alertNumberText":Ljava/lang/String;
    .end local v2    # "data":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_2
    :goto_2
    return-void

    .restart local v2    # "data":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_3
    move v0, v4

    .line 65
    goto :goto_0

    .line 72
    .restart local v0    # "alertCount":I
    .restart local v1    # "alertNumberText":Ljava/lang/String;
    :cond_4
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    if-lez v5, :cond_0

    .line 73
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 84
    :cond_5
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    if-ne v5, v7, :cond_6

    .line 85
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d6b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 87
    :cond_6
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070d64

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    iget v7, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 90
    .end local v0    # "alertCount":I
    .end local v1    # "alertNumberText":Ljava/lang/String;
    :cond_7
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v4, v5, :cond_2

    .line 91
    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 92
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->alertNoTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_2
.end method
