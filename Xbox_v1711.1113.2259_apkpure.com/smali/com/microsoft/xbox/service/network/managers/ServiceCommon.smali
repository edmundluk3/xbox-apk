.class public Lcom/microsoft/xbox/service/network/managers/ServiceCommon;
.super Ljava/lang/Object;
.source "ServiceCommon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;,
        Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;
    }
.end annotation


# static fields
.field public static final ACCEPT_HEADER:Ljava/lang/String; = "Accept"

.field public static final ACCEPT_LANGUAGE_HEADER:Ljava/lang/String; = "Accept-Language"

.field public static final AUTHENTICATION_HEADER:Ljava/lang/String; = "Authorization"

.field public static final AUTHENTICATION_PREFIX:Ljava/lang/String; = "XBL1.0 x="

.field public static final CACHE_CONTROL_HEADER:Ljava/lang/String; = "Cache-Control"

.field public static final CLIENT_TYPE_HEADER:Ljava/lang/String; = "x-xbl-client-type"

.field public static final CLIENT_TYPE_HEADER_VALUE:Ljava/lang/String; = "XboxApp"

.field public static final CONTENT_RESTRICTIONS_HEADER:Ljava/lang/String; = "x-xbl-contentrestrictions"

.field public static final CONTENT_TYPE_HEADER:Ljava/lang/String; = "Content-type"

.field public static final CONTRACT_VERSION_HEADER:Ljava/lang/String; = "x-xbl-contract-version"

.field public static final EXPERIMENTATION_CLIENTID_HEADER:Ljava/lang/String; = "X-MSEDGE-CLIENTID"

.field public static final EXPERIMENTATION_MARKET_HEADER:Ljava/lang/String; = "X-MSEDGE-MARKET"

.field private static final HTTP_200_CODES:[I

.field public static final JSON_TYPE_HEADER:Ljava/lang/String; = "application/json"

.field public static final LOCALE_HEADER:Ljava/lang/String; = "X-Locale"

.field private static final MAX_ERROR_BODY_LENGTH:I = 0x400

.field public static final MOBILE_PLATFORM_TYPE:Ljava/lang/String; = "5"

.field public static final PLATFORM_TYPE_HEADER:Ljava/lang/String; = "X-Platform-Type"

.field private static final TAG:Ljava/lang/String;

.field public static final XPARTNER_AUTHENTICATION_HEADER:Ljava/lang/String; = "X-PartnerAuthorization"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    .line 94
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->HTTP_200_CODES:[I

    return-void

    :array_0
    .array-data 4
        0xc8
        0xc9
        0xca
        0xcb
        0xcc
        0xcd
        0xce
        0xcf
        0xd0
        0xe2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AddWebHeaders(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/util/List;)V
    .locals 3
    .param p0, "httpRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    if-eqz p1, :cond_0

    .line 106
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 108
    .local v0, "header":Lorg/apache/http/Header;
    invoke-interface {p0, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_0

    .line 111
    .end local v0    # "header":Lorg/apache/http/Header;
    :cond_0
    return-void
.end method

.method public static GetLivenWebHeaders(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-Locale"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    :goto_0
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-Platform-Type"

    const-string v3, "5"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Cache-Control"

    const-string v3, "no-store, no-cache, must-revalidate"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "PRAGMA"

    const-string v3, "no-cache"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    if-eqz p0, :cond_0

    .line 128
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-PartnerAuthorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XBL1.0 x="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    return-object v0

    .line 121
    :cond_1
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-Locale"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMappedLocale()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static ParseHttpResponseForStatus(Ljava/lang/String;ILjava/lang/String;J)V
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "statusCode"    # I
    .param p2, "statusLine"    # Ljava/lang/String;
    .param p3, "streamLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 302
    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->ParseHttpResponseForStatus(Ljava/lang/String;ILjava/lang/String;JLjava/io/InputStream;)V

    .line 303
    return-void
.end method

.method private static ParseHttpResponseForStatus(Ljava/lang/String;ILjava/lang/String;JLjava/io/InputStream;)V
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "statusCode"    # I
    .param p2, "statusLine"    # Ljava/lang/String;
    .param p3, "streamLength"    # J
    .param p5, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 306
    const/16 v3, 0xc8

    if-lt p1, v3, :cond_0

    const/16 v3, 0x190

    if-ge p1, v3, :cond_0

    move v0, v1

    .line 308
    .local v0, "success":Z
    :goto_0
    if-nez v0, :cond_c

    .line 309
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s (%d bytes) for url: %s"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p2, v7, v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    aput-object p0, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 311
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Timeout attempting response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 313
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .end local v0    # "success":Z
    :cond_0
    move v0, v2

    .line 306
    goto :goto_0

    .line 314
    .restart local v0    # "success":Z
    :cond_1
    const/16 v1, 0x191

    if-eq p1, v1, :cond_2

    const/16 v1, 0x193

    if-ne p1, v1, :cond_4

    .line 315
    :cond_2
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 317
    if-nez p5, :cond_3

    .line 318
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x3fc

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 320
    :cond_3
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x3fc

    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;)V

    throw v1

    .line 323
    :cond_4
    const/16 v1, 0x190

    if-ne p1, v1, :cond_6

    .line 324
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 327
    if-nez p5, :cond_5

    .line 328
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xf

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 330
    :cond_5
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xf

    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;)V

    throw v1

    .line 332
    :cond_6
    const/16 v1, 0x1f4

    if-ne p1, v1, :cond_7

    .line 333
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 334
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xd

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 335
    :cond_7
    const/16 v1, 0x1f7

    if-ne p1, v1, :cond_8

    .line 336
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 337
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x12

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 338
    :cond_8
    const/16 v1, 0x194

    if-ne p1, v1, :cond_9

    .line 339
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 340
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x15

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 341
    :cond_9
    const/16 v1, 0x199

    if-ne p1, v1, :cond_a

    .line 342
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 343
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x16

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 344
    :cond_a
    const v1, 0xffff

    if-ne p1, v1, :cond_b

    .line 345
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 346
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 348
    :cond_b
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 349
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x4

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 353
    :cond_c
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s (%d bytes) for url: %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v1

    aput-object p0, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public static checkConnectivity()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 611
    new-instance v6, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v6}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 612
    .local v6, "stopwatch":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    const/4 v1, 0x0

    .line 617
    .local v1, "connected":Z
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 619
    .local v0, "cm":Landroid/net/ConnectivityManager;
    const/4 v7, 0x3

    new-array v5, v7, [I

    fill-array-data v5, :array_0

    .line 620
    .local v5, "networksToCheck":[I
    array-length v8, v5

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_2

    aget v4, v5, v7

    .line 621
    .local v4, "networkType":I
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 622
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-nez v2, :cond_0

    sget-object v3, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 623
    .local v3, "networkState":Landroid/net/NetworkInfo$State;
    :goto_1
    sget-object v9, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v3, v9, :cond_1

    .line 624
    sget-object v9, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "network not connected for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 622
    .end local v3    # "networkState":Landroid/net/NetworkInfo$State;
    :cond_0
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    goto :goto_1

    .line 626
    .restart local v3    # "networkState":Landroid/net/NetworkInfo$State;
    :cond_1
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "network connected for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const/4 v1, 0x1

    .line 632
    .end local v2    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v3    # "networkState":Landroid/net/NetworkInfo$State;
    .end local v4    # "networkType":I
    :cond_2
    if-nez v1, :cond_3

    .line 633
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x2

    invoke-direct {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v7

    .line 635
    :cond_3
    return-void

    .line 619
    :array_0
    .array-data 4
        0x0
        0x1
        0x6
    .end array-data
.end method

.method private static convertHttpHeader([Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "headerArray"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 792
    array-length v2, p0

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 793
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 794
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 797
    aget-object v2, p0, v1

    const-string v3, "Content-Length"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 794
    :goto_2
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 792
    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 802
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "i":I
    :cond_1
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    aget-object v3, p0, v1

    add-int/lit8 v4, v1, 0x1

    aget-object v4, p0, v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 805
    :cond_2
    return-object v0
.end method

.method private static convertToHttpStatusCode(J)I
    .locals 4
    .param p0, "errorCode"    # J

    .prologue
    const/16 v0, 0x1f4

    .line 730
    const-wide/16 v2, 0x3fc

    cmp-long v1, p0, v2

    if-nez v1, :cond_1

    .line 731
    const/16 v0, 0x191

    .line 739
    :cond_0
    :goto_0
    return v0

    .line 732
    :cond_1
    const-wide/16 v2, 0xf

    cmp-long v1, p0, v2

    if-nez v1, :cond_2

    .line 733
    const/16 v0, 0x190

    goto :goto_0

    .line 734
    :cond_2
    const-wide/16 v2, 0xd

    cmp-long v1, p0, v2

    if-eqz v1, :cond_0

    .line 736
    const-wide/16 v2, 0x12

    cmp-long v1, p0, v2

    if-nez v1, :cond_0

    .line 737
    const/16 v0, 0x1f7

    goto :goto_0
.end method

.method public static delete(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 228
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithStatus(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    .line 229
    .local v0, "statusCode":I
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/16 v1, 0xcc

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static delete(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 254
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    :try_start_0
    const-string v1, "ServiceCommon"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {p0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;[B)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v2, "can\'t encode string to utf8"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v1
.end method

.method public static delete(Ljava/lang/String;Ljava/util/List;[B)Z
    .locals 11
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;[B)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 264
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    .line 265
    .local v4, "uri":Ljava/net/URI;
    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 267
    new-instance v3, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 273
    .local v3, "stopwatch":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Network delete started for url \'%s\'"

    new-array v10, v6, [Ljava/lang/Object;

    aput-object p0, v10, v5

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 276
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/HttpDeleteWithRequestBody;

    invoke-direct {v1, v4}, Lcom/microsoft/xbox/service/network/managers/HttpDeleteWithRequestBody;-><init>(Ljava/net/URI;)V

    .line 277
    .local v1, "httpDelete":Lcom/microsoft/xbox/service/network/managers/HttpDeleteWithRequestBody;
    if-eqz p2, :cond_0

    array-length v7, p2

    if-lez v7, :cond_0

    .line 279
    :try_start_0
    new-instance v7, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v7, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/service/network/managers/HttpDeleteWithRequestBody;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :cond_0
    invoke-static {v1, p0, p1, v5, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 293
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 295
    iget v7, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_1

    iget v7, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v8, 0xcc

    if-ne v7, v8, :cond_2

    :cond_1
    move v5, v6

    .line 296
    .local v5, "value":Z
    :cond_2
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 297
    return v5

    .line 280
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "value":Z
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 283
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x5

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
.end method

.method public static deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "lastRedirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 234
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iput-object p2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    .line 235
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v2, 0x12d

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0
.end method

.method public static deleteWithRedirectNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "lastRedirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 244
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iput-object p2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    .line 245
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v2, 0x12d

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithRedirectNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 249
    :cond_0
    return-object v0
.end method

.method public static deleteWithStatus(Ljava/lang/String;Ljava/util/List;)I
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v8, 0x0

    .line 202
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    .line 203
    .local v3, "uri":Ljava/net/URI;
    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 205
    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 211
    .local v2, "stopwatch":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Network delete started for url \'%s\'"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p0, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 214
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, v3}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/net/URI;)V

    .line 215
    .local v0, "httpDelete":Lorg/apache/http/client/methods/HttpDelete;
    invoke-static {v0, p0, p1, v8, v8}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    .line 221
    .local v1, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 223
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 224
    iget v4, v1, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    return v4
.end method

.method private static executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 7
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "expectResponseEntity"    # Z
    .param p4, "timeoutOverride"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;ZI)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 531
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method private static executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "expectResponseEntity"    # Z
    .param p4, "timeoutOverride"    # I
    .param p5, "addUserObjectFromResponse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;ZIZ",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 535
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p6, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method private static executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 15
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "expectResponseEntity"    # Z
    .param p4, "timeoutOverride"    # I
    .param p5, "addUserObjectFromResponse"    # Z
    .param p7, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;ZIZ",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;Z)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 539
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p6, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->AddWebHeaders(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/util/List;)V

    .line 543
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/HttpClientFactory;->networkOperationsFactory:Lcom/microsoft/xbox/toolkit/network/HttpClientFactory;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/network/HttpClientFactory;->getHttpClient(I)Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;

    move-result-object v9

    .line 545
    .local v9, "client":Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->isSignedIn()Z

    move-result v3

    if-nez v3, :cond_0

    .line 546
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v4, "User signed out, cancelling request"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x3f0

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v3

    .line 551
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v9, p0, v3}, Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;->getHttpStatusAndStreamInternal(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v14

    .line 554
    .local v14, "rv":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz p6, :cond_1

    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 555
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    array-length v3, v3

    if-ge v11, v3, :cond_1

    .line 556
    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    aget-object v3, v3, v11

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 561
    .end local v11    # "i":I
    :cond_1
    iget v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v4, 0x190

    if-lt v3, v4, :cond_3

    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v3, :cond_3

    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->markSupported()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 563
    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const/16 v4, 0x400

    invoke-virtual {v3, v4}, Ljava/io/InputStream;->mark(I)V

    .line 564
    new-instance v13, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v4, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v13, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 565
    .local v13, "reader":Ljava/io/BufferedReader;
    const/16 v3, 0x400

    new-array v2, v3, [C

    .line 567
    .local v2, "charBuffer":[C
    const/4 v3, 0x0

    const/16 v4, 0x400

    :try_start_0
    invoke-virtual {v13, v2, v3, v4}, Ljava/io/BufferedReader;->read([CII)I

    move-result v12

    .line 568
    .local v12, "length":I
    if-lez v12, :cond_2

    .line 569
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error body: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6, v12}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 577
    .end local v12    # "length":I
    :cond_2
    :goto_1
    :try_start_1
    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 583
    .end local v2    # "charBuffer":[C
    .end local v13    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_2
    if-eqz p7, :cond_4

    .line 584
    if-eqz p5, :cond_5

    .line 585
    iget v4, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    iget-object v5, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    iget-wide v6, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->streamLength:J

    iget-object v8, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->ParseHttpResponseForStatus(Ljava/lang/String;ILjava/lang/String;JLjava/io/InputStream;)V

    .line 592
    :cond_4
    :goto_3
    iget-object v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-nez v3, :cond_6

    if-eqz p3, :cond_6

    .line 593
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No entity for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    sget-object v3, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 595
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x7

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v3

    .line 571
    .restart local v2    # "charBuffer":[C
    .restart local v13    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v10

    .line 572
    .local v10, "ex":Ljava/io/IOException;
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v4, "Could not read error body"

    invoke-static {v3, v4, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 578
    .end local v10    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v10

    .line 579
    .restart local v10    # "ex":Ljava/io/IOException;
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v4, "Could not reset error body stream"

    invoke-static {v3, v4, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 587
    .end local v2    # "charBuffer":[C
    .end local v10    # "ex":Ljava/io/IOException;
    .end local v13    # "reader":Ljava/io/BufferedReader;
    :cond_5
    iget v3, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    iget-object v4, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    iget-wide v6, v14, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->streamLength:J

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4, v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->ParseHttpResponseForStatus(Ljava/lang/String;ILjava/lang/String;J)V

    goto :goto_3

    .line 598
    :cond_6
    return-object v14
.end method

.method public static getActiveConnection()I
    .locals 4

    .prologue
    .line 667
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 668
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 670
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 671
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    .line 673
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getConnectionType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 638
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 641
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 643
    .local v1, "defaultNetworkInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_0

    .line 644
    const-string v3, "None"

    .line 662
    :goto_0
    return-object v3

    .line 647
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    .line 648
    .local v2, "state":Landroid/net/NetworkInfo$State;
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_1

    .line 649
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 658
    const-string v3, "None"

    goto :goto_0

    .line 652
    :sswitch_0
    const-string v3, "Cellular"

    goto :goto_0

    .line 654
    :sswitch_1
    const-string v3, "Wifi"

    goto :goto_0

    .line 656
    :sswitch_2
    const-string v3, "Wired"

    goto :goto_0

    .line 662
    :cond_1
    const-string v3, "None"

    goto :goto_0

    .line 649
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x6 -> :sswitch_0
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method public static getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 10
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;Z)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v3, 0x0

    .line 187
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    .line 188
    .local v9, "uri":Ljava/net/URI;
    invoke-virtual {v9}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 190
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Network delete started for url \'%s\'"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 193
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, v9}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/net/URI;)V

    .line 194
    .local v0, "httpDelete":Lorg/apache/http/client/methods/HttpDelete;
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move v5, v3

    move v7, p2

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v8

    .line 196
    .local v8, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 198
    return-object v8
.end method

.method public static getDeleteStreamWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static getHttp200Codes()[I
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->HTTP_200_CODES:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public static getHttpData(Ljava/lang/String;[Ljava/lang/String;III)V
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headerArray"    # [Ljava/lang/String;
    .param p2, "callbackPtr"    # I
    .param p3, "requestTypeTag"    # I
    .param p4, "timeoutOverride"    # I

    .prologue
    .line 678
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JNI calling java for getting data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->convertHttpHeader([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 682
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, p4, p2, p3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$1;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;III)Ljava/lang/Runnable;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 724
    return-void
.end method

.method public static getIsOnline()Z
    .locals 4

    .prologue
    .line 602
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 603
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 604
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 605
    const/4 v2, 0x1

    .line 607
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getResponseString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 954
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 955
    .local v1, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 957
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "length":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 958
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 961
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->GetLivenWebHeaders(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStream(Ljava/lang/String;Ljava/util/List;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method private static getStream(Ljava/lang/String;Ljava/util/List;)Ljava/io/InputStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v1, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 149
    .local v1, "stopwatch":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 155
    .local v0, "responseAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v2, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v2}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 157
    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    return-object v2
.end method

.method public static getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 358
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 359
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 362
    :cond_0
    return-object v0
.end method

.method private static getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "urlEncode"    # Z
    .param p3, "timeoutOverride"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;ZI)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 383
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZIZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method private static getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZIZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 10
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "urlEncode"    # Z
    .param p3, "timeoutOverride"    # I
    .param p4, "addUserObjectFromBadRequestResponse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;ZIZ)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v3, 0x1

    .line 387
    const/4 v8, 0x0

    .line 389
    .local v8, "uri":Ljava/net/URI;
    if-eqz p2, :cond_0

    .line 390
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v8

    .line 399
    :goto_0
    invoke-virtual {v8}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 401
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Network getStream started for url \'%s\'"

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 405
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 407
    .local v0, "httpGet":Lorg/apache/http/client/methods/HttpUriRequest;
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    return-object v1

    .line 393
    .end local v0    # "httpGet":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_0
    :try_start_0
    new-instance v9, Ljava/net/URI;

    invoke-direct {v9, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v8    # "uri":Ljava/net/URI;
    .local v9, "uri":Ljava/net/URI;
    move-object v8, v9

    .line 396
    .end local v9    # "uri":Ljava/net/URI;
    .restart local v8    # "uri":Ljava/net/URI;
    goto :goto_0

    .line 394
    :catch_0
    move-exception v7

    .line 395
    .local v7, "e":Ljava/net/URISyntaxException;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to encode url: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v2, 0x1

    .line 375
    const/4 v1, 0x0

    invoke-static {p0, p1, v2, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZIZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 376
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 379
    :cond_0
    return-object v0
.end method

.method public static getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "lastRedirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v2, 0x1

    .line 366
    const/4 v1, 0x0

    invoke-static {p0, p1, v2, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZIZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 367
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iput-object p2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    .line 368
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 371
    :cond_0
    return-object v0
.end method

.method private static getXLEHttpResponse(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .locals 9
    .param p0, "statusAndStream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 809
    new-instance v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;-><init>()V

    .line 810
    .local v3, "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    iget v5, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    .line 812
    .local v5, "rvStatusCode":I
    const/4 v6, 0x0

    new-array v4, v6, [B

    .line 814
    .local v4, "rvResponseBytes":[B
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 816
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 818
    .local v0, "byteOutputStream":Ljava/io/ByteArrayOutputStream;
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v0, v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->CopyStream(Ljava/io/OutputStream;Ljava/io/InputStream;)V

    .line 820
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 823
    .end local v0    # "byteOutputStream":Ljava/io/ByteArrayOutputStream;
    :cond_0
    iput v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->statusCode:I

    .line 824
    iput-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->responseBytes:[B

    .line 825
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->headerArray:[Ljava/lang/String;

    .line 827
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    array-length v6, v6

    if-ge v2, v6, :cond_1

    .line 828
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    aget-object v1, v6, v2

    .line 829
    .local v1, "header":Lorg/apache/http/Header;
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->headerArray:[Ljava/lang/String;

    mul-int/lit8 v7, v2, 0x2

    invoke-interface {v1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 830
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->headerArray:[Ljava/lang/String;

    mul-int/lit8 v7, v2, 0x2

    add-int/lit8 v7, v7, 0x1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 827
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 832
    .end local v1    # "header":Lorg/apache/http/Header;
    :cond_1
    return-object v3
.end method

.method static synthetic lambda$getHttpData$1(Ljava/lang/String;Ljava/util/List;III)V
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "timeoutOverride"    # I
    .param p3, "callbackPtr"    # I
    .param p4, "requestTypeTag"    # I

    .prologue
    .line 683
    const/4 v2, 0x0

    .line 685
    .local v2, "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    const/4 v3, 0x0

    .line 687
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {p0, p1, v4, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;ZI)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 688
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getXLEHttpResponse(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 702
    :goto_0
    if-eqz v3, :cond_0

    .line 703
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 707
    :cond_0
    iput p3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->callbackPtr:I

    .line 708
    iput p4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->requestTypeTag:I

    .line 710
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v5, "getHttpData background thread calling back into C++..."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getHttpData callback is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    move-object v1, v2

    .line 717
    .local v1, "finalResponse":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    sget-object v4, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 722
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v5, "getHttpData background thread stopping..."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    return-void

    .line 689
    .end local v1    # "finalResponse":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :catch_0
    move-exception v0

    .line 691
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;

    .end local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;-><init>()V

    .line 693
    .restart local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_1

    .line 694
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->convertToHttpStatusCode(J)I

    move-result v4

    iput v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->statusCode:I

    .line 699
    :goto_1
    const/4 v4, 0x0

    iput-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->responseBytes:[B

    goto :goto_0

    .line 696
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/16 v4, 0x1f4

    iput v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->statusCode:I

    goto :goto_1
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)V
    .locals 4
    .param p0, "finalResponse"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;

    .prologue
    .line 718
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "running task on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->onGetHttpDataCompleted(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)V

    .line 720
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)V
    .locals 4
    .param p0, "finalResponse"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;

    .prologue
    .line 782
    const-string v0, "ServiceCommon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "running task on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->onGetHttpDataCompleted(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)V

    .line 784
    return-void
.end method

.method static synthetic lambda$postHttpData$3(Ljava/lang/String;Ljava/util/List;[BI)V
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "body"    # [B
    .param p3, "callbackPtr"    # I

    .prologue
    .line 750
    const/4 v2, 0x0

    .line 751
    .local v2, "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    const/4 v4, 0x0

    .line 754
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 755
    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getXLEHttpResponse(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 768
    if-eqz v4, :cond_0

    .line 769
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 774
    :cond_0
    :goto_0
    iput p3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->callbackPtr:I

    .line 775
    const/4 v5, 0x0

    iput v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->requestTypeTag:I

    .line 777
    sget-object v5, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v6, "postHttpData background thread calling back into C++..."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    sget-object v5, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "postHttpData callback is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    move-object v1, v2

    .line 781
    .local v1, "finalResponse":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    sget-object v5, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 786
    sget-object v5, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v6, "postHttpData background thread stopping..."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    return-void

    .line 756
    .end local v1    # "finalResponse":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :catch_0
    move-exception v0

    .line 758
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 760
    .end local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .local v3, "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :try_start_2
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_1

    .line 761
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->convertToHttpStatusCode(J)I

    move-result v5

    iput v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->statusCode:I

    .line 766
    :goto_1
    const/4 v5, 0x0

    iput-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->responseBytes:[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 768
    if-eqz v4, :cond_3

    .line 769
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    move-object v2, v3

    .end local v3    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .restart local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    goto :goto_0

    .line 763
    .end local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :cond_1
    const/16 v5, 0x1f4

    :try_start_3
    iput v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;->statusCode:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 768
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    move-object v2, v3

    .end local v3    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .restart local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :goto_2
    if-eqz v4, :cond_2

    .line 769
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_2
    throw v5

    .line 768
    :catchall_1
    move-exception v5

    goto :goto_2

    .end local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .restart local v3    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    :cond_3
    move-object v2, v3

    .end local v3    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    .restart local v2    # "response":Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;
    goto :goto_0
.end method

.method static synthetic lambda$responseFromRequestAccepting2xxs$5(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Ljava/lang/Object;
    .locals 1
    .param p0, "returnClass"    # Ljava/lang/Class;
    .param p1, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 899
    iget-object v0, p1, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$responseFromRequestAccepting2xxsAndLogResult$4(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Ljava/lang/Object;
    .locals 6
    .param p0, "returnClass"    # Ljava/lang/Class;
    .param p1, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 884
    :try_start_0
    iget-object v2, p1, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getResponseString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 885
    .local v1, "resultString":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->logServiceResponse(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    invoke-static {v1, p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 887
    .end local v1    # "resultString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 888
    .local v0, "ex":Ljava/io/IOException;
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x8

    invoke-direct {v2, v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v2
.end method

.method private static declared-synchronized logServiceResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "returnClassName"    # Ljava/lang/String;

    .prologue
    .line 966
    const-class v5, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;

    monitor-enter v5

    const/16 v1, 0xfa0

    .line 968
    .local v1, "chunkSize":I
    :try_start_0
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logServiceResponse return class"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n response size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_1

    .line 971
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    div-int/2addr v4, v1

    add-int/lit8 v0, v4, 0x1

    .line 972
    .local v0, "chunkCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 973
    add-int/lit8 v4, v3, 0x1

    mul-int v2, v1, v4

    .line 974
    .local v2, "currentMaxLength":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 975
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logServiceResponse chunk "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    mul-int v7, v1, v3

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 977
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logServiceResponse chunk "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    mul-int v7, v1, v3

    invoke-virtual {p0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 966
    .end local v0    # "chunkCount":I
    .end local v2    # "currentMaxLength":I
    .end local v3    # "i":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 981
    :cond_1
    :try_start_1
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "logServiceResponse:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 983
    :cond_2
    monitor-exit v5

    return-void
.end method

.method public static native onGetHttpDataCompleted(Lcom/microsoft/xbox/toolkit/network/XLEHttpResponse;)V
.end method

.method public static postHttpData(Ljava/lang/String;[Ljava/lang/String;[BI)V
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headerArray"    # [Ljava/lang/String;
    .param p2, "body"    # [B
    .param p3, "callbackPtr"    # I

    .prologue
    .line 745
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JNI calling java for post data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->convertHttpHeader([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 749
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, p2, p3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$2;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;[BI)Ljava/lang/Runnable;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 788
    return-void
.end method

.method public static postStream(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "xml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v1, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 167
    .local v1, "stopwatch":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 173
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v2, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v2}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 174
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 175
    iget v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "lastRedirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 411
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "lastRedirectUri"    # Ljava/lang/String;
    .param p4, "addUserObjectFromResponse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x1

    invoke-static {p0, p1, p2, p4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 416
    .local v0, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iput-object p3, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    .line 417
    iget v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v2, 0x12d

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v1, p1, p2, v2, p4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    .line 421
    :cond_0
    return-object v0
.end method

.method public static postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;[B)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 443
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 10
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # [B
    .param p3, "addUserObjectFromResponse"    # Z
    .param p4, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;[BZZ)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 447
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    .line 448
    .local v9, "uri":Ljava/net/URI;
    invoke-virtual {v9}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 450
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Network postStream started for url \'%s\'"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p0, v5, v3

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Network postStream started for body length \'%d\'"

    new-array v6, v6, [Ljava/lang/Object;

    if-nez p2, :cond_1

    move v1, v3

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 454
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 455
    .local v0, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    array-length v1, p2

    if-lez v1, :cond_0

    .line 457
    :try_start_0
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_0
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move v5, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    return-object v1

    .line 451
    .end local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    :cond_1
    array-length v1, p2

    goto :goto_0

    .line 458
    .restart local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    :catch_0
    move-exception v8

    .line 459
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 461
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v1, v2, v3, v8}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v1
.end method

.method public static postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 425
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 4
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "addUserObjectToResponse"    # Z
    .param p4, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 434
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "post data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {p0, p1, v1, p3, p4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v2, "can\'t encode string to utf8"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v1
.end method

.method public static postStringWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    .line 429
    invoke-static {p0, p1, p2, v0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p4, "lastRedirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 487
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p3, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v7, 0x0

    .line 489
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 490
    iput-object p4, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    if-eqz v7, :cond_0

    iget v0, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v1, 0x12d

    if-ne v0, v1, :cond_0

    iget-object v0, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    iget-object v0, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    iget-object v1, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    invoke-static {v0, p1, p2, p3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 499
    :cond_0
    return-object v7

    .line 491
    :catch_0
    move-exception v6

    .line 492
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v1, "can\'t encode string to utf8"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v0, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v0
.end method

.method public static putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;[B)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 504
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 10
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # [B
    .param p3, "addUserObjectFromResponse"    # Z
    .param p5, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;[BZ",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;Z)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p4, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 509
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getEncodedUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    .line 510
    .local v9, "uri":Ljava/net/URI;
    invoke-virtual {v9}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p0

    .line 512
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Network putStream started for url \'%s\'"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p0, v5, v4

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Network putStream started for body length \'%d\'"

    new-array v6, v6, [Ljava/lang/Object;

    if-nez p2, :cond_1

    move v1, v4

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v4

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Started:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 516
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, v9}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V

    .line 517
    .local v0, "put":Lorg/apache/http/client/methods/HttpPut;
    if-eqz p2, :cond_0

    array-length v1, p2

    if-lez v1, :cond_0

    .line 519
    :try_start_0
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v5, p3

    move-object v6, p4

    move v7, p5

    .line 527
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/util/List;ZIZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    return-object v1

    .line 513
    .end local v0    # "put":Lorg/apache/http/client/methods/HttpPut;
    :cond_1
    array-length v1, p2

    goto :goto_0

    .line 520
    .restart local v0    # "put":Lorg/apache/http/client/methods/HttpPut;
    :catch_0
    move-exception v8

    .line 521
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 523
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v1, v2, v3, v8}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v1
.end method

.method public static putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 469
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "addUserObjectFromResponse"    # Z
    .param p4, "throwStatusCodeException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "put data "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[BZLjava/util/List;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 480
    :catch_0
    move-exception v6

    .line 481
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->TAG:Ljava/lang/String;

    const-string v1, "can\'t encode string to utf8"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x5

    invoke-direct {v0, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v0
.end method

.method public static putStringWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v0, 0x0

    .line 473
    invoke-static {p0, p1, p2, v0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static redirectRequest(Ljava/util/concurrent/Callable;Ljava/lang/Class;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    .locals 10
    .param p0    # Ljava/util/concurrent/Callable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    .local p1, "returnClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "acceptableCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .line 990
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 991
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 993
    const/4 v4, 0x0

    .line 994
    .local v4, "result":Ljava/lang/Object;, "TT;"
    const/4 v5, 0x0

    .line 997
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-object v5, v0

    .line 999
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1000
    .local v2, "acceptableCode":I
    iget v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v2, :cond_0

    .line 1001
    if-eqz p1, :cond_1

    .line 1002
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    .line 1004
    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    :cond_1
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    const/4 v7, 0x1

    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    iget-object v9, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;-><init>(ZLjava/lang/String;Ljava/util/List;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1015
    if-eqz v5, :cond_2

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_2

    .line 1016
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1022
    .end local v2    # "acceptableCode":I
    :cond_2
    :goto_0
    return-object v6

    .line 1015
    .restart local v4    # "result":Ljava/lang/Object;, "TT;"
    :cond_3
    if-eqz v5, :cond_4

    :try_start_2
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 1016
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1022
    :cond_4
    :goto_1
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    const/4 v7, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v7, v9, v8, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;-><init>(ZLjava/lang/String;Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_0

    .line 1007
    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v3

    .line 1008
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_3
    instance-of v6, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_6

    .line 1009
    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v3    # "ex":Ljava/lang/Exception;
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1014
    :catchall_0
    move-exception v6

    .line 1015
    if-eqz v5, :cond_5

    :try_start_4
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_5

    .line 1016
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1019
    :cond_5
    :goto_2
    throw v6

    .line 1011
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_5
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1018
    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_2

    .restart local v4    # "result":Ljava/lang/Object;, "TT;"
    :catch_2
    move-exception v6

    goto :goto_1

    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    .restart local v2    # "acceptableCode":I
    :catch_3
    move-exception v7

    goto :goto_0
.end method

.method public static redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    .locals 1
    .param p0    # Ljava/util/concurrent/Callable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 986
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    .local p1, "acceptableCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/lang/Class;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v0

    return-object v0
.end method

.method public static request(Ljava/util/concurrent/Callable;[I)Z
    .locals 10
    .param p0    # Ljava/util/concurrent/Callable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "acceptableCodes"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;[I)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 844
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 845
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 847
    const/4 v5, 0x0

    .line 848
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    .line 851
    .local v4, "result":Z
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-object v5, v0

    .line 853
    array-length v7, p1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    aget v2, p1, v6

    .line 854
    .local v2, "acceptableCode":I
    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v8, v2, :cond_2

    .line 855
    const/4 v4, 0x1

    .line 867
    .end local v2    # "acceptableCode":I
    :cond_0
    if-eqz v5, :cond_1

    :try_start_1
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_1

    .line 868
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 874
    :cond_1
    :goto_1
    return v4

    .line 853
    .restart local v2    # "acceptableCode":I
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 859
    .end local v2    # "acceptableCode":I
    :catch_0
    move-exception v3

    .line 860
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_2
    instance-of v6, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 861
    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v3    # "ex":Ljava/lang/Exception;
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 866
    :catchall_0
    move-exception v6

    .line 867
    if-eqz v5, :cond_3

    :try_start_3
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_3

    .line 868
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 871
    :cond_3
    :goto_2
    throw v6

    .line 863
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 870
    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_2

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public static requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z
    .locals 1
    .param p0    # Ljava/util/concurrent/Callable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 838
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 840
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->HTTP_200_CODES:[I

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->request(Ljava/util/concurrent/Callable;[I)Z

    move-result v0

    return v0
.end method

.method public static responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;
    .locals 10
    .param p2, "acceptableCodes"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream",
            "<TT;>;[I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 907
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    .local p1, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 908
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 909
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 911
    const/4 v4, 0x0

    .line 912
    .local v4, "result":Ljava/lang/Object;, "TT;"
    const/4 v5, 0x0

    .line 915
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-object v5, v0

    .line 917
    array-length v7, p2

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    aget v2, p2, v6

    .line 918
    .local v2, "acceptableCode":I
    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v8, v2, :cond_2

    .line 919
    invoke-interface {p1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;->run(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 931
    .end local v2    # "acceptableCode":I
    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    :cond_0
    if-eqz v5, :cond_1

    :try_start_1
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_1

    .line 932
    iget-object v6, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 938
    :cond_1
    :goto_1
    return-object v4

    .line 917
    .restart local v2    # "acceptableCode":I
    .restart local v4    # "result":Ljava/lang/Object;, "TT;"
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 923
    .end local v2    # "acceptableCode":I
    :catch_0
    move-exception v3

    .line 924
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_2
    instance-of v6, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 925
    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v3    # "ex":Ljava/lang/Exception;
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 930
    :catchall_0
    move-exception v6

    .line 931
    if-eqz v5, :cond_3

    :try_start_3
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_3

    .line 932
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 935
    :cond_3
    :goto_2
    throw v6

    .line 927
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 934
    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_2

    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public static responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 896
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    .local p1, "returnClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 897
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 899
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$4;->lambdaFactory$(Ljava/lang/Class;)Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v0

    .line 901
    .local v0, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<TT;>;"
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->HTTP_200_CODES:[I

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static responseFromRequestAccepting2xxsAndLogResult(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 879
    .local p0, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    .local p1, "returnClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 880
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 882
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$$Lambda$3;->lambdaFactory$(Ljava/lang/Class;)Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v0

    .line 892
    .local v0, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<TT;>;"
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->HTTP_200_CODES:[I

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static urlAppendFilter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "filterKey"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "filterValue"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation

    .prologue
    .line 946
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 947
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 948
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 950
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static urlDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "src"    # Ljava/lang/String;

    .prologue
    .line 1027
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1029
    :goto_0
    return-object v1

    .line 1028
    :catch_0
    move-exception v0

    .line 1029
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v1, 0x0

    goto :goto_0
.end method
