.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AdapterBaseWithXLEAdapterView.java"


# instance fields
.field private adapterViewBase:Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    return-void
.end method

.method private savePosition()V
    .locals 4

    .prologue
    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->adapterViewBase:Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;->getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v0

    .line 82
    .local v0, "state":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->getIndex()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->getOffset()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setListPosition(II)V

    .line 83
    return-void
.end method


# virtual methods
.method public getAnimateIn(Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->getAnimateIn(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 39
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    return-object v0
.end method

.method public getAnimateOut(Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->getAnimateOut(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 59
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    return-object v0
.end method

.method protected abstract getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
.end method

.method protected abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method protected getXLEAdapterViewBase()Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->adapterViewBase:Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    return-object v0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 69
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 74
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->savePosition()V

    .line 75
    return-void
.end method

.method protected restorePosition()V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    .line 88
    .local v2, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v1

    .line 89
    .local v1, "position":I
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListOffset()I

    move-result v0

    .line 91
    .local v0, "offset":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->adapterViewBase:Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-interface {v3, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;->restoreScrollState(II)V

    .line 92
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 99
    return-void
.end method

.method protected setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V
    .locals 1
    .param p1, "adapterViewBase"    # Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    .prologue
    .line 15
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->adapterViewBase:Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    .line 18
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
