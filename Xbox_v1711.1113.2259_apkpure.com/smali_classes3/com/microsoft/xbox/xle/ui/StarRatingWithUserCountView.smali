.class public Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;
.super Landroid/widget/LinearLayout;
.source "StarRatingWithUserCountView.java"


# instance fields
.field private starRatingView:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private userRateCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->init(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v6, 0x7f03020e

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 35
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 36
    .local v2, "vi":Landroid/view/LayoutInflater;
    if-eqz p2, :cond_1

    .line 37
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->StarRatingView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 39
    .local v1, "maskColor":I
    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 40
    const v3, 0x7f03020d

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 44
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v1    # "maskColor":I
    :goto_1
    const v3, 0x7f0e0a59

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->starRatingView:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 50
    const v3, 0x7f0e0a5a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->userRateCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    return-void

    .line 42
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v1    # "maskColor":I
    :cond_0
    invoke-virtual {v2, v6, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 46
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v1    # "maskColor":I
    :cond_1
    invoke-virtual {v2, v6, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_1
.end method

.method private setNarratorContent()V
    .locals 5

    .prologue
    .line 62
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%1$s, %2$s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->starRatingView:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->userRateCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method


# virtual methods
.method public setAverageUserRatingAndUserCount(FJ)V
    .locals 2
    .param p1, "averageUserRating"    # F
    .param p2, "userRateCount"    # J

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->starRatingView:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 55
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->userRateCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->setNarratorContent()V

    .line 59
    return-void
.end method
