.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Clubs;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clubs"
.end annotation


# static fields
.field public static final Admins:Ljava/lang/String; = "Admins"

.field public static final Banned:Ljava/lang/String; = "Banned"

.field public static final Everyone:Ljava/lang/String; = "Everyone"

.field public static final Hidden:Ljava/lang/String; = "Hidden"

.field public static final Member:Ljava/lang/String; = "Member"

.field public static final Owner:Ljava/lang/String; = "Owner"

.field public static final Private:Ljava/lang/String; = "Private"

.field public static final Public:Ljava/lang/String; = "Public"

.field public static final SearchResults:Ljava/lang/String; = "SearchResults"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
