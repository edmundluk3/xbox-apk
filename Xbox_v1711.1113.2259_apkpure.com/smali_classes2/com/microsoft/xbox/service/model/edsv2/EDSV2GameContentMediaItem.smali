.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2GameContentMediaItem.java"


# static fields
.field private static final FULL_SKU_TYPE:Ljava/lang/String; = "Full"

.field private static final PROVIDERFORMAT:Ljava/lang/String; = "ContentMediaId=%s;MediaId=%s;ContentMediaTypeId=%d"


# instance fields
.field public DeveloperName:Ljava/lang/String;

.field public Genres:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation
.end field

.field public ParentalRatings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation
.end field

.field public PublisherName:Ljava/lang/String;

.field public RatingId:I

.field public RelatedMedia:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;",
            ">;"
        }
    .end annotation
.end field

.field public TitleId:J

.field private availabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private parentIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private parentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private slideshows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 48
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->DeveloperName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->DeveloperName:Ljava/lang/String;

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->Genres:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->Genres:Ljava/util/ArrayList;

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->PublisherName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->PublisherName:Ljava/lang/String;

    move-object v0, p1

    .line 52
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RatingId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RatingId:I

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->TitleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->TitleId:J

    .line 54
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->MediaItemType:Ljava/lang/String;

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RelatedMedia:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RelatedMedia:Ljava/util/ArrayList;

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentIDs:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentIDs:Ljava/util/ArrayList;

    .line 57
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentItems:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentItems:Ljava/util/List;

    .line 60
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 0
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 64
    return-void
.end method


# virtual methods
.method public getAvailabilities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->availabilities:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    .line 122
    :goto_0
    return-object v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getPosterImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 119
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->imageUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParentIDs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    const/4 v1, 0x0

    .line 166
    .local v1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentIDs:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RelatedMedia:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .restart local v1    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RelatedMedia:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;

    .line 169
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;
    const-string v3, "Parent"

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;->RelationType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 170
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;->ID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 176
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentIDs:Ljava/util/ArrayList;

    .line 179
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentIDs:Ljava/util/ArrayList;

    return-object v2
.end method

.method public getParentItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentItems:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->ParentalRatings:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Util;->getLocalizedParentalRating(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->ParentalRatings:Ljava/util/List;

    return-object v0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->ParentalRatings:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->ParentalRatings:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDescriptors:Ljava/util/List;

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRatingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->RatingId:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setParentItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "parents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->parentItems:Ljava/util/List;

    .line 134
    return-void
.end method

.method protected updateDisplaySkuAvailabilities(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 10
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 68
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v0

    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Consumable:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    if-ne v0, v4, :cond_1

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->availabilities:Ljava/util/List;

    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getDisplaySkuAvailabilities()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 74
    .local v7, "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    if-eqz v7, :cond_0

    iget-object v0, v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    if-eqz v0, :cond_0

    const-string v0, "Full"

    iget-object v4, v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuType:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->getAvailabilities()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->chooseAvailability(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;

    move-result-object v6

    .line 80
    .local v6, "chosenAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    if-eqz v6, :cond_0

    .line 82
    iget-object v0, v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v1, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuId:Ljava/lang/String;

    .line 83
    .local v1, "skuId":Ljava/lang/String;
    iget-object v0, v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->getSkuLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;

    move-result-object v0

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;->skuTitle:Ljava/lang/String;

    .line 84
    .local v2, "skuTitle":Ljava/lang/String;
    iget-object v3, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->availabilityId:Ljava/lang/String;

    .line 85
    .local v3, "availabilityId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    if-eqz v0, :cond_0

    iget-object v0, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    if-eqz v0, :cond_0

    iget-object v0, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget-object v0, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->currencyCode:Ljava/lang/String;

    .line 89
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->availabilities:Ljava/util/List;

    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    iget-object v4, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->listPrice:F

    const/4 v5, 0x2

    .line 95
    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getBigDecimalFromFloat(FI)Ljava/math/BigDecimal;

    move-result-object v4

    iget-object v5, v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->currencyCode:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    .line 91
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    .end local v1    # "skuId":Ljava/lang/String;
    .end local v2    # "skuTitle":Ljava/lang/String;
    .end local v3    # "availabilityId":Ljava/lang/String;
    .end local v6    # "chosenAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    .end local v7    # "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    :cond_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateDisplaySkuAvailabilities(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 104
    :cond_2
    return-void
.end method
