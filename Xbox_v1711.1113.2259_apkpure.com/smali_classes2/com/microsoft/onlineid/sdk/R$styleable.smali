.class public final Lcom/microsoft/onlineid/sdk/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/onlineid/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final StyledTextView:[I

.field public static final StyledTextView_font:I = 0x0

.field public static final StyledTextView_isUnderlined:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/onlineid/sdk/R$styleable;->AdsAttrs:[I

    .line 190
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/microsoft/onlineid/sdk/R$styleable;->StyledTextView:[I

    return-void

    .line 186
    nop

    :array_0
    .array-data 4
        0x7f010068
        0x7f010069
        0x7f01006a
    .end array-data

    .line 190
    :array_1
    .array-data 4
        0x7f0101f5
        0x7f0101f6
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
