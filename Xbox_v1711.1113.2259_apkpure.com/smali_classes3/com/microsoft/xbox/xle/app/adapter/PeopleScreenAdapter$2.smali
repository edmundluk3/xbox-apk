.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;
.super Ljava/lang/Object;
.source "PeopleScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 191
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 196
    .local v0, "isEmpty":Z
    if-nez v0, :cond_0

    .line 197
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$702(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Z)Z

    .line 198
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildFilteredFriendList(Ljava/lang/String;)V

    .line 203
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 204
    return-void

    .line 200
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$702(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Z)Z

    .line 201
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onSearchBarClear()V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 203
    goto :goto_1
.end method
