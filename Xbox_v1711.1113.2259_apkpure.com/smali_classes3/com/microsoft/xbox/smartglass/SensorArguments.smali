.class public Lcom/microsoft/xbox/smartglass/SensorArguments;
.super Ljava/lang/Object;
.source "SensorArguments.java"


# instance fields
.field final sampleRate:I

.field final service:I

.field final titleId:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "sampleRate"    # I
    .param p2, "titleId"    # I
    .param p3, "service"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/microsoft/xbox/smartglass/SensorArguments;->sampleRate:I

    .line 20
    iput p2, p0, Lcom/microsoft/xbox/smartglass/SensorArguments;->titleId:I

    .line 21
    iput p3, p0, Lcom/microsoft/xbox/smartglass/SensorArguments;->service:I

    .line 22
    return-void
.end method
