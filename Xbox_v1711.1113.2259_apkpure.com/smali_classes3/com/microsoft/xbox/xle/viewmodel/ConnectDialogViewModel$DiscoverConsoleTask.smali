.class Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "ConnectDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscoverConsoleTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final TIMEOUT:I = 0x4e20

.field private static final TOTAL_RETRIES:I = 0x5


# instance fields
.field private mConsoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field private final mConsoleDiscoverListener:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;

.field private mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

.field private mDiscoverRetries:I

.field private mPowerOn:Z

.field private mSuccess:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Ljava/util/concurrent/ExecutorService;Lcom/microsoft/xbox/xle/model/ConsoleData;Z)V
    .locals 2
    .param p2, "executorService"    # Ljava/util/concurrent/ExecutorService;
    .param p3, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p4, "powerOn"    # Z

    .prologue
    .line 390
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    .line 391
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 393
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 395
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 396
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleDiscoverListener:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;

    .line 397
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    .line 398
    const/4 v0, 0x5

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverRetries:I

    .line 399
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mPowerOn:Z

    .line 400
    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mPowerOn:Z

    return v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mSuccess:Z

    return v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;
    .param p1, "x1"    # Z

    .prologue
    .line 373
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mSuccess:Z

    return p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverRetries:I

    return v0
.end method

.method static synthetic access$810(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)I
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverRetries:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverRetries:I

    return v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method


# virtual methods
.method protected doInBackground()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 410
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleDiscoverListener:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->addListener(Ljava/lang/Object;)V

    .line 411
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getIsDiscovering()Z

    move-result v1

    if-nez v1, :cond_0

    .line 413
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->discover()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    const/16 v2, 0x4e20

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady(I)V

    .line 422
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mSuccess:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 414
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 417
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mSuccess:Z

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->doInBackground()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x0

    .line 427
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleDiscoverListener:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->removeListener(Ljava/lang/Object;)V

    .line 428
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Z)Z

    .line 429
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mPowerOn:Z

    if-eqz v1, :cond_0

    .line 431
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Power On Success"

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->connect(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 434
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)V

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 449
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mPowerOn:Z

    if-eqz v1, :cond_2

    const v0, 0x7f070363

    .line 442
    .local v0, "errorMsgId":I
    :goto_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mPowerOn:Z

    if-eqz v1, :cond_3

    .line 443
    const-string v1, "Power On Error"

    invoke-static {v1, v3, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Lcom/microsoft/xbox/toolkit/IViewUpdate;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mConsoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setErrorMessage(Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    .end local v0    # "errorMsgId":I
    :cond_2
    const v0, 0x7f070362

    goto :goto_1

    .line 445
    .restart local v0    # "errorMsgId":I
    :cond_3
    const-string v1, "Power On Error"

    invoke-static {v1, v3, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 373
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->mDiscoverDone:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 405
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;Z)Z

    .line 406
    return-void
.end method
