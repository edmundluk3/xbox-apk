.class public Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.super Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
.source "ConsoleBarView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IViewUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;,
        Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    }
.end annotation


# instance fields
.field private final SWAP_APP_TIME_THRESHOLD:J

.field private activeTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;",
            ">;"
        }
    .end annotation
.end field

.field private focusedTabIndex:I

.field private ignoreTabChangeEvents:Z

.field private isLoadingRecents:Z

.field private final itself:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

.field private lastClosedTabWasFillOrFullAndHadFocus:Z

.field private lastClosedTimestamp:J

.field private loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

.field private nowPlayingObserver:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;

.field private tabChangeListener:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;

.field private tabHost:Landroid/widget/TabHost;

.field private viewModels:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;-><init>(Landroid/content/Context;I)V

    .line 54
    const-wide/16 v0, 0x5dc

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->SWAP_APP_TIME_THRESHOLD:J

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->isLoadingRecents:Z

    .line 76
    iput-object p0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->itself:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .line 77
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->nowPlayingObserver:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;

    .line 78
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    .line 79
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->itself:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->onRecentsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1202(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->isLoadingRecents:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getIndicatorDrawable(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->closeTitle(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    return-object v0
.end method

.method private addTabFromTabInfo(IILjava/util/ArrayList;)V
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p3, "tabInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p2, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 323
    move v0, p1

    .local v0, "i":I
    :goto_1
    if-gt v0, p2, :cond_1

    .line 324
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .line 325
    .local v1, "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$300(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    move-result-object v3

    iget-object v2, v3, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;->tabSpec:Landroid/widget/TabHost$TabSpec;

    .line 327
    .local v2, "spec":Landroid/widget/TabHost$TabSpec;
    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->attachToContainer()V

    .line 328
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 329
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 321
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    .end local v2    # "spec":Landroid/widget/TabHost$TabSpec;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 331
    .restart local v0    # "i":I
    :cond_1
    return-void
.end method

.method private addTabToEnd(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V
    .locals 8
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p2, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .param p3, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 308
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->createTabSpec(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    move-result-object v3

    .line 309
    .local v3, "specHolder":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    iget-object v1, v3, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;->tabSpec:Landroid/widget/TabHost$TabSpec;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 310
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    iget-object v1, p2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v5

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    return-void
.end method

.method private closeTitle(I)V
    .locals 11
    .param p1, "titleId"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "focusChanged":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getActiveTitleLocation(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 256
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v5, :cond_1

    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->StartView:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v5, :cond_1

    .line 258
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getTabFromActiveTitleLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-result-object v3

    .line 259
    .local v3, "tab":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v2

    .line 261
    .local v2, "index":I
    if-eq v2, v10, :cond_1

    .line 262
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ne v2, v5, :cond_3

    move v4, v6

    .line 263
    .local v4, "tabToDeleteHasFocus":Z
    :goto_0
    if-eqz v4, :cond_4

    sget-object v5, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    if-ne v3, v5, :cond_4

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->lastClosedTabWasFillOrFullAndHadFocus:Z

    .line 264
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->lastClosedTimestamp:J

    .line 265
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    .line 266
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->deleteTab(I)V

    .line 267
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    .line 269
    if-eqz v4, :cond_5

    .line 271
    sget-object v5, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 272
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ne v5, v10, :cond_0

    .line 273
    iput v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 275
    :cond_0
    const/4 v1, 0x1

    .line 283
    .end local v2    # "index":I
    .end local v3    # "tab":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .end local v4    # "tabToDeleteHasFocus":Z
    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    .line 284
    const-string v5, "ConsoleBarView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "after a delete tab, setting smartglass focus to tab "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    invoke-virtual {v5, v6}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 289
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateNowPlayingBarContent()V

    .line 290
    return-void

    .restart local v2    # "index":I
    .restart local v3    # "tab":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    :cond_3
    move v4, v7

    .line 262
    goto :goto_0

    .restart local v4    # "tabToDeleteHasFocus":Z
    :cond_4
    move v5, v7

    .line 263
    goto :goto_1

    .line 276
    :cond_5
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ge v2, v5, :cond_1

    .line 277
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 278
    const/4 v1, 0x1

    goto :goto_2
.end method

.method private createTabSpec(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;
    .locals 5
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p2, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    .prologue
    .line 293
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 294
    .local v2, "spec":Landroid/widget/TabHost$TabSpec;
    new-instance v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;

    iget-object v3, p2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    iget-object v4, p2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingTitle()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p0, p1, v3, v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Ljava/lang/String;)V

    .line 295
    .local v1, "indicator":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->container:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 296
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->attachToContainer()V

    .line 297
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->getViewId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 298
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    invoke-direct {v0, p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Landroid/widget/TabHost$TabSpec;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;)V

    .line 299
    .local v0, "holder":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;
    return-object v0
.end method

.method private createTabViewFromContentType(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .param p2, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 472
    const/4 v0, 0x0

    .line 473
    .local v0, "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NowPlayingTrayViewModel$ContentType:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 503
    const-string v1, "ConsoleBarView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unhandled app type; using GenericAppView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    new-instance v0, Lcom/microsoft/xbox/xle/ui/NowPlayingGenericAppView;

    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingGenericAppView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 508
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    :goto_0
    return-object v0

    .line 475
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/ui/NowPlayingGenericAppView;

    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingGenericAppView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 476
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    goto :goto_0

    .line 484
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;

    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 485
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    goto :goto_0

    .line 488
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/xle/ui/NowPlayingGameView;

    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingGameView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 489
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    goto :goto_0

    .line 499
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 500
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    goto :goto_0

    .line 473
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private deleteTab(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 387
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 388
    .local v0, "maxIndex":I
    if-le p1, v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 393
    .local v1, "tempActiveTabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 398
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 400
    const/4 v2, 0x0

    add-int/lit8 v3, p1, -0x1

    invoke-direct {p0, v2, v3, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    .line 401
    add-int/lit8 v2, p1, 0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    .line 403
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->cleanup()V

    goto :goto_0
.end method

.method private findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I
    .locals 3
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .prologue
    .line 421
    const/4 v1, -0x1

    .line 422
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 423
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$200(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 424
    move v1, v0

    .line 429
    :cond_0
    return v1

    .line 422
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getIndicatorDrawable(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 408
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Snapped:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne p1, v1, :cond_0

    .line 409
    const v0, 0x7f020060

    .line 417
    .local v0, "id":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1

    .line 410
    .end local v0    # "id":I
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Fill:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq p1, v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->StartView:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne p1, v1, :cond_2

    .line 411
    :cond_1
    const v0, 0x7f02005a

    .restart local v0    # "id":I
    goto :goto_0

    .line 412
    .end local v0    # "id":I
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Full:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne p1, v1, :cond_3

    .line 413
    const v0, 0x7f02005d

    .restart local v0    # "id":I
    goto :goto_0

    .line 415
    .end local v0    # "id":I
    :cond_3
    const v0, 0x7f020066

    .restart local v0    # "id":I
    goto :goto_0
.end method

.method private getTabFromActiveTitleLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .locals 4
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 529
    sget-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .line 530
    .local v0, "tab":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$2;->$SwitchMap$com$microsoft$xbox$smartglass$ActiveTitleLocation:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 542
    const-string v1, "ConsoleBarView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unhandled location: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :goto_0
    return-object v0

    .line 532
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Snap:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .line 533
    goto :goto_0

    .line 537
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .line 538
    goto :goto_0

    .line 530
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private insertTab(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;ILcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V
    .locals 4
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p2, "index"    # I
    .param p3, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .param p4, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 340
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 341
    .local v0, "maxIndex":I
    if-le p2, v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 346
    .local v1, "tempActiveTabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 351
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 353
    const/4 v2, 0x0

    add-int/lit8 v3, p2, -0x1

    invoke-direct {p0, v2, v3, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    .line 354
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabToEnd(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 355
    invoke-direct {p0, p2, v0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private onRecentsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 8
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v7, 0x0

    .line 623
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 624
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->isLoadingRecents:Z

    .line 626
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq p1, v4, :cond_0

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq p1, v4, :cond_0

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v4, :cond_1

    .line 627
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 628
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v1

    .line 629
    .local v1, "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 630
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Recent:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 631
    .local v0, "contentType":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->itself:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    sget-object v6, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->StartView:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/recents/Recent;

    invoke-direct {v3, v5, v6, v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/service/model/recents/Recent;)V

    .line 632
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;
    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->createTabViewFromContentType(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v2

    .line 634
    .local v2, "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    sget-object v4, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    invoke-direct {p0, v2, v4, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->showTab(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 635
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->update()V

    .line 643
    .end local v0    # "contentType":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .end local v1    # "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    .end local v2    # "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .end local v3    # "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;
    :cond_1
    :goto_0
    return-void

    .line 637
    .restart local v1    # "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    :cond_2
    const-string v4, "ConsoleBarView"

    const-string v5, "There are no recent apps."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 640
    .end local v1    # "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    :cond_3
    const-string v4, "ConsoleBarView"

    const-string v5, "There is an app runninng on the console. no need to show the last recently used anymore"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private replaceTab(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;ILcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V
    .locals 4
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p2, "index"    # I
    .param p3, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .param p4, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 366
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 367
    .local v0, "maxIndex":I
    if-le p2, v0, :cond_0

    .line 384
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 372
    .local v1, "tempActiveTabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 377
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 379
    const/4 v2, 0x0

    add-int/lit8 v3, p2, -0x1

    invoke-direct {p0, v2, v3, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    .line 380
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabToEnd(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 381
    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabFromTabInfo(IILjava/util/ArrayList;)V

    .line 383
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->cleanup()V

    goto :goto_0
.end method

.method private showTab(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V
    .locals 20
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .param p2, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p3, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 153
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    .line 155
    const/4 v8, 0x0

    .line 156
    .local v8, "destinationIndex":I
    const/4 v12, 0x0

    .line 157
    .local v12, "replaceExistingTab":Z
    const/4 v4, 0x0

    .line 158
    .local v4, "addToEnd":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-nez v15, :cond_4

    const/4 v9, 0x1

    .line 162
    .local v9, "hadNoTabs":Z
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v10

    .line 163
    .local v10, "index":I
    const/4 v5, -0x1

    .line 164
    .local v5, "backgroundTabIndex":I
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p2

    if-eq v0, v15, :cond_0

    .line 165
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v5

    .line 168
    :cond_0
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p2

    if-ne v0, v15, :cond_7

    .line 169
    const/4 v8, 0x0

    .line 170
    if-ltz v10, :cond_5

    .line 171
    const/4 v12, 0x1

    .line 198
    :cond_1
    :goto_1
    const/4 v13, 0x0

    .line 200
    .local v13, "tabBeingReplacedHadFocus":Z
    if-eqz v12, :cond_e

    .line 201
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ne v10, v15, :cond_c

    const/4 v13, 0x1

    .line 202
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->replaceTab(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;ILcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 204
    if-eqz v13, :cond_2

    .line 206
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p2

    if-ne v0, v15, :cond_d

    .line 207
    move-object/from16 v0, p0

    iput v8, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 229
    :cond_2
    :goto_3
    const-string v15, "ConsoleBarView"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Setting smartglass focus to tab "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 232
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateNowPlayingBarContent()V

    .line 234
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    .line 235
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v15}, Landroid/widget/TabHost;->requestLayout()V

    .line 236
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v15}, Landroid/widget/TabHost;->invalidate()V

    .line 238
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 239
    .local v6, "currentTimestamp":J
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->lastClosedTabWasFillOrFullAndHadFocus:Z

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->lastClosedTimestamp:J

    move-wide/from16 v16, v0

    sub-long v16, v6, v16

    const-wide/16 v18, 0x5dc

    cmp-long v15, v16, v18

    if-gez v15, :cond_3

    .line 240
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v14

    .line 241
    .local v14, "temp":I
    if-ltz v14, :cond_3

    .line 242
    move-object/from16 v0, p0

    iput v14, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 243
    const-string v15, "ConsoleBarView"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Restoring smartglass focus to tab "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 248
    .end local v14    # "temp":I
    :cond_3
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->lastClosedTabWasFillOrFullAndHadFocus:Z

    .line 249
    return-void

    .line 158
    .end local v5    # "backgroundTabIndex":I
    .end local v6    # "currentTimestamp":J
    .end local v9    # "hadNoTabs":Z
    .end local v10    # "index":I
    .end local v13    # "tabBeingReplacedHadFocus":Z
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 172
    .restart local v5    # "backgroundTabIndex":I
    .restart local v9    # "hadNoTabs":Z
    .restart local v10    # "index":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v15}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v15

    invoke-virtual {v15}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v15

    if-lez v15, :cond_6

    .line 174
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 176
    :cond_6
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 178
    :cond_7
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Snap:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p2

    if-ne v0, v15, :cond_a

    .line 179
    if-ltz v10, :cond_8

    .line 180
    move v8, v10

    .line 181
    const/4 v12, 0x1

    goto/16 :goto_1

    .line 182
    :cond_8
    const/4 v15, -0x1

    if-eq v5, v15, :cond_9

    .line 184
    add-int/lit8 v8, v5, -0x1

    .line 185
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 187
    :cond_9
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 189
    :cond_a
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p2

    if-ne v0, v15, :cond_1

    .line 190
    if-ltz v10, :cond_b

    .line 191
    move v8, v10

    .line 192
    const/4 v12, 0x1

    goto/16 :goto_1

    .line 194
    :cond_b
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 201
    .restart local v13    # "tabBeingReplacedHadFocus":Z
    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_2

    .line 209
    :cond_d
    sget-object v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 210
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 211
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    goto/16 :goto_3

    .line 215
    :cond_e
    if-eqz v4, :cond_f

    .line 216
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->addTabToEnd(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 217
    if-eqz v9, :cond_2

    .line 218
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    goto/16 :goto_3

    .line 223
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$200(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-result-object v11

    .line 224
    .local v11, "oldTabFocus":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->insertTab(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;ILcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 226
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    goto/16 :goto_3
.end method

.method private tabHasIncompatibleContent(ILcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)Z
    .locals 4
    .param p1, "targetTabIndex"    # I
    .param p2, "desiredContentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 512
    const-string v3, "index is guaranteed to be >= 0"

    if-ltz p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 514
    const/4 v1, 0x1

    .line 516
    .local v1, "result":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    move-result-object v0

    .line 517
    .local v0, "currentContentTypeInTab":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    if-ne p2, v0, :cond_2

    .line 518
    const/4 v1, 0x0

    .line 525
    :cond_0
    :goto_1
    return v1

    .line 512
    .end local v0    # "currentContentTypeInTab":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .end local v1    # "result":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 519
    .restart local v0    # "currentContentTypeInTab":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .restart local v1    # "result":Z
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq p2, v2, :cond_3

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq p2, v2, :cond_3

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-ne p2, v2, :cond_0

    :cond_3
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-ne v0, v2, :cond_0

    .line 522
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private updateTabSpec(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V
    .locals 6
    .param p1, "tabInfo"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .prologue
    .line 599
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$300(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    move-result-object v3

    iget-object v1, v3, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;->indicator:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;

    .line 600
    .local v1, "indicator":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingTabImageUri()Ljava/lang/String;

    move-result-object v0

    .line 601
    .local v0, "imageUri":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 602
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 603
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v4, 0x7f0e001d

    new-instance v5, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$1;

    invoke-direct {v5, p0, p2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$1;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 609
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 610
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_0

    .line 611
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    .line 615
    :goto_0
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v3, v0, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 619
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :goto_1
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabTitle:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 620
    return-void

    .line 613
    .restart local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget v4, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 617
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    iget-object v3, v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getIndicatorDrawable(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method


# virtual methods
.method public getFocusedTabLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ltz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .line 94
    .local v0, "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$000(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v1

    .line 96
    .end local v0    # "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    goto :goto_0
.end method

.method public getFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .locals 3

    .prologue
    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ltz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .line 102
    .local v0, "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 104
    .end local v0    # "info":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNonFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .locals 2

    .prologue
    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-ltz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 111
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    if-eq v0, v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 116
    .end local v0    # "i":I
    :goto_1
    return-object v1

    .line 110
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 116
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected initialize()V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->initialize()V

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    .line 137
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->ignoreTabChangeEvents:Z

    .line 139
    const v0, 0x7f0e045e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    .line 140
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Landroid/widget/TabHost;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabChangeListener:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabChangeListener:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 143
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 444
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->nowPlayingObserver:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->unRegister()V

    .line 445
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 446
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 447
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->cleanup()V

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 451
    .end local v0    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    if-eqz v2, :cond_1

    .line 452
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 453
    .local v1, "model":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->onPause()V

    goto :goto_1

    .line 457
    .end local v1    # "model":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    if-eqz v2, :cond_2

    .line 458
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;->cancel()V

    .line 459
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    .line 462
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->reset()V

    .line 463
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 434
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 436
    .local v0, "model":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->onResume()V

    goto :goto_0

    .line 439
    .end local v0    # "model":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->nowPlayingObserver:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->register()V

    .line 440
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->focusedTabIndex:I

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->viewModels:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 127
    return-void
.end method

.method public setCurrentTab(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)V
    .locals 3
    .param p1, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v0

    .line 83
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 84
    const-string v1, "ConsoleBarView"

    const-string v2, "tab does not exist"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0
.end method

.method public updateView(Ljava/lang/Object;)V
    .locals 9
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 554
    move-object v6, p1

    check-cast v6, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 555
    .local v6, "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    move-result-object v1

    .line 556
    .local v1, "contentType":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-eq v1, v7, :cond_1

    .line 557
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 560
    .local v0, "activeTitleLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v7, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->StartView:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v7, :cond_2

    .line 561
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 562
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    if-nez v7, :cond_0

    .line 563
    new-instance v7, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$1;)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    .line 566
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->loadRecentsTask:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$LoadRecentsTask;->load(Z)V

    .line 596
    .end local v0    # "activeTitleLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_1
    :goto_0
    return-void

    .line 569
    .restart local v0    # "activeTitleLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_2
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getTabFromActiveTitleLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-result-object v3

    .line 570
    .local v3, "tab":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findTabIndex(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;)I

    move-result v2

    .line 572
    .local v2, "index":I
    const/4 v5, 0x0

    .line 573
    .local v5, "view":Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    const/4 v7, -0x1

    if-eq v2, v7, :cond_3

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->tabHasIncompatibleContent(ILcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 575
    :cond_3
    invoke-direct {p0, v6, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->createTabViewFromContentType(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v5

    .line 577
    invoke-direct {p0, v5, v3, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->showTab(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V

    .line 591
    :goto_1
    if-eqz v5, :cond_1

    .line 592
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->update()V

    goto :goto_0

    .line 579
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->activeTabs:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .line 582
    .local v4, "tabInfo":Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    invoke-direct {p0, v4, v6}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->updateTabSpec(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    .line 584
    invoke-static {v4, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$002(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 585
    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    move-result-object v5

    .line 586
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->updateViewModel(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    .line 588
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateNowPlayingBarContent()V

    goto :goto_1
.end method
