.class public Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.super Ljava/lang/Object;
.source "SkypeMessagingDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNotOpenException;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;,
        Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;
    }
.end annotation


# static fields
.field private static final COUNTROWS:Ljava/lang/String; = "SELECT count(*) FROM "

.field private static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS"

.field private static final DATABASE_NAME:Ljava/lang/String; = "SkypeMessaging.db"

.field private static final DATABASE_VERSION:I = 0x4

.field private static final DIAGNOSTIC_CONVERSATION_MESSAGES_NOT_FOUND:Ljava/lang/String; = "Did not find any conversation messages with conversationId %s"

.field private static final DIAGNOSTIC_CONVERSATION_MESSAGE_NOT_FOUND:Ljava/lang/String; = "Did not find any conversation messages with messageId %s"

.field private static final DIAGNOSTIC_CONVERSATION_SUMMARY_NOT_FOUND:Ljava/lang/String; = "Did not find any conversation summaries with conversationId %s"

.field private static final DIAGNOSTIC_CONVERSATION_SUMMARY_NOT_FOUND_ANY:Ljava/lang/String; = "Did not find any conversation summaries"

.field private static final DIAGNOSTIC_DATABASE_VALIDATED:Ljava/lang/String; = "The database is available and open"

.field private static final DROP_TABLE:Ljava/lang/String; = "DROP TABLE IF EXISTS"

.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final ERRORCODE:Ljava/lang/String;

.field private static final ERROR_DATABASE_NOT_INITIALIZED:Ljava/lang/String; = "The skype messaging database is not initialized"

.field private static final ERROR_DATABASE_NOT_OPEN:Ljava/lang/String; = "The skype messaging database is not open"

.field private static final ERROR_NOT_FOUND_ERRORCODE:I = 0x1

.field private static final ERROR_NOT_FOUND_REASON:Ljava/lang/String; = "The record was not found.  Operation: %s  Id: %s"

.field private static final INSERT:Ljava/lang/String; = "INSERT INTO"

.field private static final INTEGER_TYPE:Ljava/lang/String; = "INTEGER"

.field private static final PRIMARY_KEY:Ljava/lang/String; = "PRIMARY KEY AUTOINCREMENT"

.field private static final SELECTQUERY:Ljava/lang/String; = "%s = \'%s\'"

.field private static final TAG:Ljava/lang/String;

.field private static final TEXT_TYPE:Ljava/lang/String; = "TEXT"

.field private static final UPDATE:Ljava/lang/String; = "UPDATE"

.field private static final VALUES:Ljava/lang/String; = "VALUES"

.field private static dbHelper:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

.field private static instance:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field private final ready:Lcom/microsoft/xbox/toolkit/Ready;

.field private resyncNeeded:Z

.field private final rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final saveConversationListRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;",
            ">;"
        }
    .end annotation
.end field

.field private final saveMessagesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private transactionCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    .line 60
    const-string v0, "%s error"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ERRORCODE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ready:Lcom/microsoft/xbox/toolkit/Ready;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 66
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resyncNeeded:Z

    .line 67
    iput v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    .line 69
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveConversationListRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 70
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveMessagesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 71
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 148
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setupDBRelays()V

    .line 149
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setDbHelper(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
    .param p1, "x1"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setDb(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredConversationListResultInternal(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized beginTransaction()V
    .locals 3

    .prologue
    .line 456
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 457
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 458
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->incrementTransactionCount()V

    .line 459
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "beginTransaction::success."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    :goto_0
    monitor-exit p0

    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 462
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "beginTransaction::failed to begin the transaction."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private bindNullOrString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 0
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1307
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1309
    if-nez p3, :cond_0

    .line 1310
    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1314
    :goto_0
    return-void

    .line 1312
    :cond_0
    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private constructLastMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1440
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1442
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;-><init>()V

    .line 1445
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    const-string v1, "lastmsgid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    .line 1446
    const-string v1, "type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->type:Ljava/lang/String;

    .line 1447
    const-string v1, "lastmsgmessagetype"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    .line 1448
    const-string v1, "lastmsgcontent"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    .line 1449
    const-string v1, "lastmsgfrom"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    .line 1450
    const-string v1, "lastmsgconversationlink"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    .line 1451
    const-string v1, "lastmsgskypeeditedid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    .line 1452
    const-string v1, "lastmsgconversationid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    .line 1453
    const-string v1, "lastmsgclientmessageid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    .line 1456
    new-instance v1, Ljava/util/Date;

    const-string v2, "lastmsgoriginalarrivaltime"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    .line 1458
    return-object v0
.end method

.method private constructMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1412
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1414
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;-><init>()V

    .line 1417
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    const-string v1, "id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    .line 1418
    const-string v1, "type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->type:Ljava/lang/String;

    .line 1419
    const-string v1, "messagetype"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    .line 1420
    const-string v1, "content"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    .line 1421
    const-string v1, "fromuser"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    .line 1422
    const-string v1, "conversationlink"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    .line 1423
    const-string v1, "skypeeditedid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    .line 1424
    const-string v1, "conversationid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    .line 1425
    const-string v1, "clientmessageid"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    .line 1426
    const-string v1, "syncstate"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    .line 1428
    new-instance v1, Ljava/util/Date;

    const-string v2, "originalarrivaltime"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    .line 1430
    return-object v0
.end method

.method private constructMessageRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1388
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1390
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1391
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "id"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    const-string v1, "type"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    const-string v1, "messagetype"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    const-string v1, "content"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    const-string v1, "originalarrivaltime"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1396
    const-string v1, "fromuser"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397
    const-string v1, "conversationlink"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1398
    const-string v1, "skypeeditedid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    const-string v1, "conversationid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    const-string v1, "clientmessageid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const-string v1, "syncstate"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    return-object v0
.end method

.method private constructSummary(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 968
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 970
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;-><init>()V

    .line 973
    .local v2, "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    const-string v3, "id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    .line 974
    const-string v3, "type"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->type:Ljava/lang/String;

    .line 975
    const-string v3, "messagetype"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->messageType:Ljava/lang/String;

    .line 976
    const-string v3, "sendergamertag"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 977
    const-string v3, "realname"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->realName:Ljava/lang/String;

    .line 978
    const-string v3, "gamerpicurl"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->gamerPicUrl:Ljava/lang/String;

    .line 981
    const-string v3, "isgroupconversation"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->isGroupConversation:Z

    .line 984
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructLastMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 987
    new-instance v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;-><init>()V

    .line 988
    .local v1, "properties":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;
    const-string v3, "consumptionhorizon"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->setConsumptionHorizon(Ljava/lang/String;)V

    .line 989
    const-string v3, "muted"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_1

    move v0, v4

    .line 990
    .local v0, "muted":Z
    :goto_1
    if-nez v0, :cond_2

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->alerts:Ljava/lang/Boolean;

    .line 991
    const-string v3, "clearedat"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->clearedat:Ljava/lang/String;

    .line 992
    iput-object v1, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    .line 994
    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;-><init>()V

    iput-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    .line 995
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    const-string v4, "lastjoined"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;->lastjoinat:Ljava/lang/String;

    .line 996
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    const-string v4, "topicname"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;->topic:Ljava/lang/String;

    .line 998
    return-object v2

    .end local v0    # "muted":Z
    .end local v1    # "properties":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;
    :cond_0
    move v3, v5

    .line 981
    goto :goto_0

    .restart local v1    # "properties":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;
    :cond_1
    move v0, v5

    .line 989
    goto :goto_1

    .restart local v0    # "muted":Z
    :cond_2
    move v4, v5

    .line 990
    goto :goto_2
.end method

.method private constructSummaryRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Landroid/content/ContentValues;
    .locals 5
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 905
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 907
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 908
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "id"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    const-string v1, "type"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const-string v1, "messagetype"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->messageType:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    if-eqz v1, :cond_1

    .line 913
    const-string v1, "consumptionhorizon"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->getConsumptionHorizon()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v4, "muted"

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->alerts:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 915
    const-string v1, "clearedat"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->clearedat:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    :goto_1
    const-string v1, "sendergamertag"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->senderGamerTag:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    const-string v1, "realname"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->realName:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    const-string v1, "gamerpicurl"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    if-eqz v1, :cond_2

    .line 926
    const-string v1, "topicname"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;->topic:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    const-string v1, "lastjoined"

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;->lastjoinat:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    :goto_2
    const-string v1, "isgroupconversation"

    iget-boolean v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->isGroupConversation:Z

    if-eqz v4, :cond_3

    :goto_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 934
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    if-eqz v1, :cond_4

    .line 935
    const-string v1, "lastmsgid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    const-string v1, "lastmsgtype"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const-string v1, "lastmsgmessagetype"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    const-string v1, "lastmsgcontent"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const-string v1, "lastmsgoriginalarrivaltime"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 940
    const-string v1, "lastmsgfrom"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->from:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    const-string v1, "lastmsgconversationlink"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationLink:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const-string v1, "lastmsgskypeeditedid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const-string v1, "lastmsgconversationid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    const-string v1, "lastmsgclientmessageid"

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    :goto_4
    return-object v0

    :cond_0
    move v1, v3

    .line 914
    goto/16 :goto_0

    .line 917
    :cond_1
    const-string v1, "consumptionhorizon"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v1, "muted"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 919
    const-string v1, "clearedat"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 929
    :cond_2
    const-string v1, "topicname"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    const-string v1, "lastjoined"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_3
    move v3, v2

    .line 932
    goto/16 :goto_3

    .line 946
    :cond_4
    const-string v1, "lastmsgid"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    const-string v1, "lastmsgtype"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    const-string v1, "lastmsgmessagetype"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    const-string v1, "lastmsgcontent"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    const-string v1, "lastmsgoriginalarrivaltime"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 951
    const-string v1, "lastmsgfrom"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const-string v1, "lastmsgconversationlink"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const-string v1, "lastmsgskypeeditedid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const-string v1, "lastmsgconversationid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    const-string v1, "lastmsgclientmessageid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private declared-synchronized createOrUpdateMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 10
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1324
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 1347
    :goto_0
    monitor-exit p0

    return-void

    .line 1329
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1330
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1332
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1333
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getMessage(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v4

    if-nez v4, :cond_1

    .line 1334
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructMessageRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1335
    .local v1, "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "messages"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1337
    .local v2, "rowId":J
    sget-object v4, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Added SkypeConversationMessage to the database: conversationId: %s, syncState: %s, messageId: %s. RowId: %s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1344
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "rowId":J
    :catch_0
    move-exception v0

    .line 1345
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1324
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1339
    :cond_1
    :try_start_3
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->updateMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    goto :goto_0

    .line 1342
    :cond_2
    sget-object v4, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "createOrUpdateConversationMessage: The message (id=%s) does not have a conversationId"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized decrementTransactionCount()V
    .locals 5

    .prologue
    .line 436
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    if-lez v0, :cond_0

    .line 437
    iget v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    .line 439
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "decrementTransactionCount:: transactionCount = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440
    monitor-exit p0

    return-void

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized endTransaction(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    .line 474
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 475
    iget v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    if-lez v1, :cond_1

    .line 476
    if-eqz p1, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 479
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 480
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->decrementTransactionCount()V

    .line 481
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "endTransaction::success."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "endTransaction::failed to end the transaction."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 474
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized getDb()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 645
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;
    .locals 1

    .prologue
    .line 663
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->dbHelper:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->instance:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->instance:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    .line 141
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->instance:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    return-object v0
.end method

.method private getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "value"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1375
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1376
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1378
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s = \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getTransactionCount()I
    .locals 1

    .prologue
    .line 421
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized incrementTransactionCount()V
    .locals 5

    .prologue
    .line 428
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    .line 429
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "incrementTransactionCount:: transactionCount = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    monitor-exit p0

    return-void

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized resetTransactionCount()V
    .locals 5

    .prologue
    .line 446
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    .line 447
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "resetTransactionCount:: transactionCount = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    monitor-exit p0

    return-void

    .line 446
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized saveStoredConversationListResultInternal(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)Z
    .locals 7
    .param p1, "metadata"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 272
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_0

    .line 295
    :goto_0
    monitor-exit p0

    return v3

    .line 277
    :cond_0
    const/4 v2, 0x0

    .line 279
    .local v2, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 280
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 284
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveConversationListResult(Ljava/lang/String;)V

    .line 286
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v6, "Successfully saved conversation list to storage"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v4

    .line 295
    goto :goto_0

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v4, 0x1

    :try_start_2
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 289
    const-string v1, "setStoredConversationListResult: failed to save conversation list."

    .line 290
    .local v1, "message":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v4, v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 272
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized setDb(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 654
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    monitor-exit p0

    return-void

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setDbHelper(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    .prologue
    .line 672
    monitor-enter p0

    :try_start_0
    sput-object p1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->dbHelper:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    monitor-exit p0

    return-void

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setupDBRelays()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveMessagesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 154
    invoke-virtual {v1, v4, v5, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    .line 155
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 156
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 152
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveConversationListRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 161
    invoke-virtual {v1, v4, v5, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    .line 162
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 163
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 159
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 165
    return-void
.end method

.method private declared-synchronized updateMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 9
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1355
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 1371
    :goto_0
    monitor-exit p0

    return-void

    .line 1361
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1362
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1364
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructMessageRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Landroid/content/ContentValues;

    move-result-object v2

    .line 1365
    .local v2, "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "messages"

    const-string v5, "id"

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-direct {p0, v5, v6}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1366
    .local v1, "rows":I
    sget-object v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Updated SkypeConversationMessage in the database: conversationId: %s, syncState: %s, messageId: %s. Rows affected: %s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1368
    .end local v1    # "rows":I
    .end local v2    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 1369
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1355
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized updateSummary(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)V
    .locals 9
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 878
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 896
    :goto_0
    monitor-exit p0

    return-void

    .line 883
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 884
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 886
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructSummaryRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Landroid/content/ContentValues;

    move-result-object v2

    .line 890
    .local v2, "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "conversations"

    const-string v5, "id"

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-direct {p0, v5, v6}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 891
    .local v1, "rows":I
    sget-object v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Updated conversationSummary in the database: %s.  Rows affected: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 893
    .end local v1    # "rows":I
    .end local v2    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 894
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 878
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized validateDatabase()V
    .locals 3

    .prologue
    .line 623
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady()V

    .line 625
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v1

    if-nez v1, :cond_0

    .line 626
    new-instance v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;

    const-string v2, "The skype messaging database is not initialized"

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 629
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 630
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 631
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "The skype messaging database is not initialized"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    new-instance v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;

    const-string v2, "The skype messaging database is not initialized"

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/lang/String;)V

    throw v1

    .line 633
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    .line 634
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "The skype messaging database is not open"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    new-instance v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNotOpenException;

    const-string v2, "The skype messaging database is not open"

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNotOpenException;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 533
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 534
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->close()V

    .line 535
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setDbHelper(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V

    .line 536
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setDb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 538
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "close::database closed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539
    monitor-exit p0

    return-void

    .line 533
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized createOrUpdateMessage(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 2
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "syncState"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1012
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    :goto_0
    monitor-exit p0

    return-void

    .line 1017
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1018
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1020
    iput-object p1, p3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    .line 1021
    iput-object p2, p3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    .line 1023
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->createOrUpdateMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1024
    :catch_0
    move-exception v0

    .line 1025
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1012
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized createOrUpdateMessages(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "syncState"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1037
    .local p3, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1060
    :goto_0
    monitor-exit p0

    return-void

    .line 1043
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1044
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1045
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1048
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->beginTransaction()V

    .line 1050
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1052
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    invoke-virtual {p0, p1, p2, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->createOrUpdateMessage(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1057
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :catch_0
    move-exception v0

    .line 1058
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1037
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1056
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized createOrUpdateSummaries(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 726
    .local p1, "summaries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 750
    :goto_0
    monitor-exit p0

    return-void

    .line 732
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 733
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 736
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->beginTransaction()V

    .line 739
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    .line 741
    .local v1, "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->createOrUpdateSummary(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 747
    .end local v1    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :catch_0
    move-exception v0

    .line 748
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 726
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 745
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized createOrUpdateSummary(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)V
    .locals 11
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 688
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_0

    .line 715
    :goto_0
    monitor-exit p0

    return-void

    .line 693
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 694
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 697
    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    move-result-object v5

    if-nez v5, :cond_1

    .line 699
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructSummaryRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Landroid/content/ContentValues;

    move-result-object v4

    .line 700
    .local v4, "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "conversations"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 701
    .local v2, "rowId":J
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Added new ConversationSummary to the database: %s. RowId: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 712
    .end local v2    # "rowId":J
    .end local v4    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 713
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 688
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 706
    :cond_1
    :try_start_3
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructSummaryRecord(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Landroid/content/ContentValues;

    move-result-object v4

    .line 709
    .restart local v4    # "values":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "conversations"

    const-string v7, "id"

    iget-object v8, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-direct {p0, v7, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 710
    .local v1, "rows":I
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Updated ConversationSummary in the database: %s. Rows affected: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized deleteMessage(Ljava/lang/String;)V
    .locals 8
    .param p1, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1204
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1217
    :goto_0
    monitor-exit p0

    return-void

    .line 1209
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1210
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1212
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "messages"

    const-string v4, "id"

    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1213
    .local v1, "rows":I
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Deleted SkypeConversationMessage from the database: messageId: %s. Rows affected: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1214
    .end local v1    # "rows":I
    :catch_0
    move-exception v0

    .line 1215
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1204
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized deleteMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1226
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 1241
    :goto_0
    monitor-exit p0

    return-void

    .line 1232
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1233
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1234
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1235
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id"

    invoke-direct {p0, v4, p2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "conversationid"

    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1236
    .local v2, "selectQuery":Ljava/lang/String;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "messages"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1237
    .local v1, "rows":I
    sget-object v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Deleted SkypeConversationMessage from the database: conversationId: %s, messageId: %s. Rows affected: %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1238
    .end local v1    # "rows":I
    .end local v2    # "selectQuery":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1239
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1226
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized deleteMessages(Ljava/lang/String;)V
    .locals 8
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1282
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1297
    :goto_0
    monitor-exit p0

    return-void

    .line 1288
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1289
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1291
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "messages"

    const-string v4, "conversationid"

    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1293
    .local v1, "rows":I
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Deleted all SkypeConversationMessage from the database: conversationId: %s.  Rows affected: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1294
    .end local v1    # "rows":I
    :catch_0
    move-exception v0

    .line 1295
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1282
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized deleteMessages(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1250
    .local p2, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1274
    :goto_0
    monitor-exit p0

    return-void

    .line 1256
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1257
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1258
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1261
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->beginTransaction()V

    .line 1264
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1265
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->deleteMessage(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1271
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :catch_0
    move-exception v0

    .line 1272
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1250
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1269
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized deleteSummaries(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 853
    .local p1, "conversationIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 869
    :goto_0
    monitor-exit p0

    return-void

    .line 858
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 859
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 861
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->beginTransaction()V

    .line 862
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 863
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->deleteSummary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 866
    .end local v1    # "id":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 867
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 853
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 865
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized deleteSummary(Ljava/lang/String;)V
    .locals 8
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 826
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 844
    :goto_0
    monitor-exit p0

    return-void

    .line 831
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 832
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 835
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "conversations"

    const-string v4, "id"

    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 836
    .local v1, "rows":I
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Deleted ConversationSummary from the database: %s.  Rows affected: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->deleteMessages(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 841
    .end local v1    # "rows":I
    :catch_0
    move-exception v0

    .line 842
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 826
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getIsDatabaseEmpty()Z
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 378
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    if-eqz v9, :cond_1

    .line 411
    :cond_0
    :goto_0
    monitor-exit p0

    return v7

    .line 382
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 384
    const-string v9, "%s %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "SELECT count(*) FROM "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "conversations"

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 385
    .local v1, "countSummaries":Ljava/lang/String;
    const-string v9, "%s %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "SELECT count(*) FROM "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "messages"

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "countMessages":Ljava/lang/String;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v1, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 388
    .local v6, "sCursor":Landroid/database/Cursor;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 390
    .local v4, "mCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 391
    .local v5, "sCount":I
    const/4 v3, 0x0

    .line 392
    .local v3, "mCount":I
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_2

    .line 393
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 394
    const/4 v9, 0x0

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 395
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_2
    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_3

    .line 399
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 400
    const/4 v9, 0x0

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 401
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404
    :cond_3
    add-int v9, v5, v3

    if-eqz v9, :cond_0

    move v7, v8

    goto :goto_0

    .line 405
    .end local v0    # "countMessages":Ljava/lang/String;
    .end local v1    # "countSummaries":Ljava/lang/String;
    .end local v3    # "mCount":I
    .end local v4    # "mCursor":Landroid/database/Cursor;
    .end local v5    # "sCount":I
    .end local v6    # "sCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 406
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 378
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method public declared-synchronized getMessage(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .locals 12
    .param p1, "messageId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 1108
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v10, v11

    .line 1129
    :goto_0
    monitor-exit p0

    return-object v10

    .line 1113
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1114
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1117
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "messages"

    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->COLUMNS:[Ljava/lang/String;

    const-string v3, "id"

    invoke-direct {p0, v3, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1118
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1119
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Did not find any conversation messages with messageId %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v11

    .line 1120
    goto :goto_0

    .line 1122
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1123
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v10

    .line 1124
    .local v10, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Found SkypeConversationMessage in the database: conversationId: %s, messageId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1126
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :catch_0
    move-exception v9

    .line 1127
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v10, v11

    .line 1129
    goto :goto_0

    .line 1108
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .locals 12
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 1134
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v10, v11

    .line 1158
    :goto_0
    monitor-exit p0

    return-object v10

    .line 1139
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1140
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1143
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "messages"

    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->COLUMNS:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id"

    invoke-direct {p0, v4, p2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "conversationid"

    .line 1144
    invoke-direct {p0, v4, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1143
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1145
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1146
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Did not find any conversation messages with messageId %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v11

    .line 1147
    goto :goto_0

    .line 1149
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1150
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v10

    .line 1151
    .local v10, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Found SkypeConversationMessage in the database: conversationId: %s, messageId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1155
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :catch_0
    move-exception v9

    .line 1156
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v10, v11

    .line 1158
    goto :goto_0

    .line 1134
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessages(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 1169
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v11, v12

    .line 1195
    :goto_0
    monitor-exit p0

    return-object v11

    .line 1174
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1175
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 1178
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "messages"

    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->COLUMNS:[Ljava/lang/String;

    const-string v3, "conversationid"

    invoke-direct {p0, v3, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1179
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1180
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Did not find any conversation messages with conversationId %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v11, v12

    .line 1181
    goto :goto_0

    .line 1183
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1184
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1186
    .local v11, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    :cond_3
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructMessage(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v10

    .line 1187
    .local v10, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Found SkypeConversationMessage in the database: conversationId: %s, messageId: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1189
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 1192
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v11    # "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    :catch_0
    move-exception v9

    .line 1193
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v11, v12

    .line 1195
    goto :goto_0

    .line 1169
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getReady()Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ready:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method public declared-synchronized getStoredConversationListResult()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 242
    const/4 v4, 0x0

    .line 257
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v4

    .line 245
    :cond_1
    const/4 v4, 0x0

    .line 247
    .local v4, "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getConversationListResult()Ljava/lang/String;

    move-result-object v3

    .line 248
    .local v3, "meta":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 249
    const-class v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 251
    .end local v3    # "meta":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 252
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "getStoredConversationListResult: failed to get conversation list from storage."

    .line 253
    .local v2, "message":Ljava/lang/String;
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v5, v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 254
    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 241
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "message":Ljava/lang/String;
    .end local v4    # "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getStoredConversationsMetadata()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 205
    monitor-enter p0

    const/4 v4, 0x0

    .line 206
    .local v4, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMessagingConversationsMetadata()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 207
    .local v3, "meta":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 209
    :try_start_1
    const-class v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v4

    .line 210
    :catch_0
    move-exception v1

    .line 211
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "getStoredConversationsMetadata: failed to deserialize json data."

    .line 212
    .local v2, "message":Ljava/lang/String;
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v5, v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 213
    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 205
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "message":Ljava/lang/String;
    .end local v3    # "meta":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getStoredSkypeConversationMessageCache()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 309
    const/4 v3, 0x0

    .line 327
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v3

    .line 312
    :cond_1
    const/4 v3, 0x0

    .line 314
    .local v3, "result":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getSkypeConversationMessageCache()Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "data":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 316
    const-class v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;

    .line 317
    .local v4, "skypeConversationMessageCacheWrapper":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;
    if-eqz v4, :cond_0

    .line 318
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;->access$400(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;)Ljava/util/concurrent/ConcurrentHashMap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    goto :goto_0

    .line 321
    .end local v0    # "data":Ljava/lang/String;
    .end local v4    # "skypeConversationMessageCacheWrapper":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;
    :catch_0
    move-exception v1

    .line 322
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "getStoredConversationListResult: failed to get skype conversation messages"

    .line 323
    .local v2, "message":Ljava/lang/String;
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v5, v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 324
    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 308
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "message":Ljava/lang/String;
    .end local v3    # "result":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getSummaries()Ljava/util/ArrayList;
    .locals 13
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 792
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v10, v12

    .line 816
    :goto_0
    monitor-exit p0

    return-object v10

    .line 797
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 800
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "conversations"

    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 801
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 802
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "Did not find any conversation summaries"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v12

    .line 803
    goto :goto_0

    .line 805
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 806
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 808
    .local v10, "summaries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    :cond_3
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructSummary(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    move-result-object v11

    .line 809
    .local v11, "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Found ConversationSummary in the database: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v11, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 813
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "summaries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    .end local v11    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :catch_0
    move-exception v9

    .line 814
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v10, v12

    .line 816
    goto :goto_0

    .line 792
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    .locals 11
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 760
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v10

    .line 782
    :goto_0
    monitor-exit p0

    return-object v0

    .line 765
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 766
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 769
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "conversations"

    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->COLUMNS:[Ljava/lang/String;

    const-string v3, "id"

    invoke-direct {p0, v3, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSelect(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 770
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 771
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Did not find any conversation summaries with conversationId %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v10

    .line 772
    goto :goto_0

    .line 774
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 777
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Found ConversationSummary in the database: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->constructSummary(Landroid/database/Cursor;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 779
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 780
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v10

    .line 782
    goto :goto_0

    .line 760
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized handleExceptions(Ljava/lang/Exception;)V
    .locals 7
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 544
    monitor-enter p0

    if-nez p1, :cond_0

    .line 580
    :goto_0
    monitor-exit p0

    return-void

    .line 549
    :cond_0
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v3, "handleExceptions:: original exception: %s:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ERRORCODE:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 552
    instance-of v2, p1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNullException;

    if-nez v2, :cond_1

    instance-of v2, p1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$DBNotOpenException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    .line 558
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->init()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579
    :cond_2
    :goto_1
    const/4 v2, 0x1

    :try_start_2
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->reset(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 544
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 559
    :catch_0
    move-exception v1

    .line 561
    .local v1, "reInit":Ljava/lang/Exception;
    :try_start_3
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v3, "handleExceptions:: reinitialize failed: %s:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ERRORCODE:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 566
    .end local v1    # "reInit":Ljava/lang/Exception;
    :cond_3
    iget v2, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->transactionCount:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-lez v2, :cond_2

    .line 570
    const/4 v2, 0x0

    :try_start_4
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 571
    :catch_1
    move-exception v0

    .line 575
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v3, "handleExceptions:: close transaction failed: %s:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    sget-object v2, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ERRORCODE:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized init()V
    .locals 3

    .prologue
    .line 507
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 509
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "init::started"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMessagingDBResetState()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 515
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v1

    if-nez v1, :cond_0

    .line 516
    new-instance v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Landroid/content/Context;)V

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setDbHelper(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V

    .line 520
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)V

    .line 523
    .local v0, "openDatabase":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;->execute()V

    .line 525
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "init::completed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    monitor-exit p0

    return-void

    .line 507
    .end local v0    # "openDatabase":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized initMessagingStorage()V
    .locals 2

    .prologue
    .line 495
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "initMessagingStorage::started"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMessagingDBResetState()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    monitor-exit p0

    return-void

    .line 495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isResyncNeeded()Z
    .locals 1

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resyncNeeded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset(Z)V
    .locals 6
    .param p1, "isErrorState"    # Z

    .prologue
    .line 589
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "reset: reset the database if possible"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 593
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setupDBRelays()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 596
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->validateDatabase()V

    .line 598
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDbHelper()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v1

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->reset(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607
    :goto_0
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 610
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resetTransactionCount()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 611
    monitor-exit p0

    return-void

    .line 599
    :catch_0
    move-exception v0

    .line 601
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v2, "reset:: resetting the database failed: %s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    sget-object v1, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->ERRORCODE:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 589
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized resetMessagingStorage()V
    .locals 1

    .prologue
    .line 614
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 615
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredConversationListResult(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)V

    .line 616
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredSkypeConversationMessageCache(Ljava/util/concurrent/ConcurrentHashMap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    monitor-exit p0

    return-void

    .line 614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized saveConversationsMetadata(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;)V
    .locals 4
    .param p1, "metadata"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    .prologue
    .line 225
    monitor-enter p0

    const/4 v2, 0x0

    .line 226
    .local v2, "result":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 228
    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 236
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveMessagingConversationsMetadata(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    monitor-exit p0

    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "saveConversationsMetadata: failed to serialize metadata."

    .line 231
    .local v1, "message":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v3, v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 232
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 225
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "message":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public saveStoredConversationListResult(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)V
    .locals 1
    .param p1, "metadata"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 266
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveConversationListRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 269
    return-void
.end method

.method public saveStoredSkypeConversationMessageCache(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ConcurrentHashMap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p1, "metadata":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveMessagesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 339
    return-void
.end method

.method public declared-synchronized saveStoredSkypeConversationMessageCacheInternal(Ljava/util/concurrent/ConcurrentHashMap;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "metadata":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 343
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_0

    .line 369
    :goto_0
    monitor-exit p0

    return v4

    .line 348
    :cond_0
    const/4 v2, 0x0

    .line 351
    .local v2, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 352
    :try_start_1
    new-instance v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;

    const/4 v6, 0x0

    invoke-direct {v3, p0, p1, v6}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/util/concurrent/ConcurrentHashMap;Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$1;)V

    .line 353
    .local v3, "skypeConversationMessageCacheWrapper":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 357
    .end local v3    # "skypeConversationMessageCacheWrapper":Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveSkypeConversationMessageCache(Ljava/lang/String;)V

    .line 359
    sget-object v6, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v7, "Successfully saved conversation messages to storage"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v4, v5

    .line 369
    goto :goto_0

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 363
    const-string v1, "setStoredConversationListResult: failed to save conversation messages to storage."

    .line 364
    .local v1, "message":Ljava/lang/String;
    sget-object v5, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    invoke-static {v5, v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 365
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 343
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setResyncNeeded(Z)V
    .locals 5
    .param p1, "resync"    # Z

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resyncNeeded:Z

    .line 194
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resyncNeeded:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveMessagingDBResetState(Z)V

    .line 195
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->TAG:Ljava/lang/String;

    const-string v1, "setResyncNeeded:: isResyncNeeded = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized test_CreateDatabaseTables()V
    .locals 0

    .prologue
    .line 1640
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized test_DeleteDatabaseTables()V
    .locals 0

    .prologue
    .line 1624
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized test_save_conversation()V
    .locals 0

    .prologue
    .line 1824
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized test_save_message()V
    .locals 0

    .prologue
    .line 1700
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized updateSyncStateForMessages(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "syncState"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1069
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 1098
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1075
    :cond_1
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1076
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1078
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    move-result-object v3

    .line 1079
    .local v3, "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1080
    iget-object v4, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getMessages(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1081
    .local v2, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1083
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->beginTransaction()V

    .line 1085
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1086
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->syncstate:Ljava/lang/String;

    iput-object v5, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->syncState:Ljava/lang/String;

    .line 1088
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-virtual {p0, v5, p2, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->createOrUpdateMessage(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1095
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v2    # "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    .end local v3    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :catch_0
    move-exception v0

    .line 1096
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->handleExceptions(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1069
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1092
    .restart local v2    # "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    .restart local v3    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :cond_2
    const/4 v4, 0x1

    :try_start_3
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->endTransaction(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
