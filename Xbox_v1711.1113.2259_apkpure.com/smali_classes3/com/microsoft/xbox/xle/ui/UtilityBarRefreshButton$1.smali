.class Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;
.super Ljava/lang/Object;
.source "UtilityBarRefreshButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;->this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 40
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;->this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v1, :cond_1

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;->this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    .line 46
    :cond_1
    return-void
.end method
