.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerRoles"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerRoles$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerRoles$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(I)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;
    .locals 4
    .param p0, "needCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 790
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 791
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerRoles;

    invoke-static {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;->with(I)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerRoles;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;)V

    return-object v0
.end method


# virtual methods
.method public abstract confirmed()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
