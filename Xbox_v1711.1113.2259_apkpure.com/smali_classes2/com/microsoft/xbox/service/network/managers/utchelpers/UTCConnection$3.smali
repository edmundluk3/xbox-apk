.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;
.super Ljava/lang/Object;
.source "UTCConnection.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackDisconnectSuccess(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$duration:J

.field final synthetic val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

.field final synthetic val$sessionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$sessionId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$duration:J

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 151
    new-instance v0, Lxbox/smartglass/ConsoleDisconnect;

    invoke-direct {v0}, Lxbox/smartglass/ConsoleDisconnect;-><init>()V

    .line 152
    .local v0, "disconnect":Lxbox/smartglass/ConsoleDisconnect;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$sessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->setSessionId(Ljava/lang/String;)V

    .line 153
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$duration:J

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const v1, 0x7fffffff

    :goto_0
    invoke-virtual {v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->setDuration(I)V

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->setMode(I)V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->access$000()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/ConsoleDisconnect;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 157
    const-string v1, "Disconnect - sessionId: %s duration: %d mode: %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$sessionId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$duration:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 159
    return-void

    .line 153
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;->val$duration:J

    long-to-int v1, v2

    goto :goto_0
.end method
