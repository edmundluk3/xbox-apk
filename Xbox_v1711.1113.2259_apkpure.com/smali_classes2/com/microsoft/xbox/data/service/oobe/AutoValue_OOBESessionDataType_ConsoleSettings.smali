.class final Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;
.super Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;
.source "AutoValue_OOBESessionDataType_ConsoleSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0
    .param p1, "allowAutoUpdateApps"    # Ljava/lang/Boolean;
    .param p2, "allowAutoUpdateSystem"    # Ljava/lang/Boolean;
    .param p3, "currentTimeZone"    # Ljava/lang/String;
    .param p4, "autoDSTEnabled"    # Ljava/lang/Boolean;
    .param p5, "allowInstantOn"    # Ljava/lang/Boolean;
    .param p6, "offConsoleStatus"    # Ljava/lang/String;
    .param p7, "userEmail"    # Ljava/lang/String;
    .param p8, "userGamerPicUrl"    # Ljava/lang/String;
    .param p9, "userGamerTag"    # Ljava/lang/String;
    .param p10, "isNewUser"    # Ljava/lang/Boolean;
    .param p11, "clientVersion"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 19
    return-void
.end method
