.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PartyAndLfgListViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;,
        Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final DEFAULT_BACKGROUND_COLOR:I

.field private static final HEADER_LAYOUT:I = 0x7f030135
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final LFG_LAYOUT:I = 0x7f03015d
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final PARTY_LAYOUT:I = 0x7f0301a3
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final onLfgClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final onPartyClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->TAG:Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->DEFAULT_BACKGROUND_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "onPartyClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;>;"
    .local p3, "onLfgClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 73
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 74
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onPartyClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 75
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onLfgClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 76
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 57
    sget v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->DEFAULT_BACKGROUND_COLOR:I

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onPartyClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onLfgClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->data:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;

    .line 98
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    if-eqz v1, :cond_0

    .line 99
    const v1, 0x7f0301a3

    .line 110
    :goto_0
    return v1

    .line 102
    :cond_0
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    if-eqz v1, :cond_1

    .line 103
    const v1, 0x7f03015d

    goto :goto_0

    .line 106
    :cond_1
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    if-eqz v1, :cond_2

    .line 107
    const v1, 0x7f030135

    goto :goto_0

    .line 110
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 57
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 2
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;

    .line 116
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 119
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 81
    .local v0, "view":Landroid/view/View;
    sparse-switch p2, :sswitch_data_0

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 92
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 83
    :sswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 85
    :sswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 87
    :sswitch_2
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x7f030135 -> :sswitch_2
        0x7f03015d -> :sswitch_1
        0x7f0301a3 -> :sswitch_0
    .end sparse-switch
.end method
