.class public abstract Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PinsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;,
        Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private curRequestId:J

.field private isLoadingPins:Z

.field private final moveRequests:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 27
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->curRequestId:J

    .line 28
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    .param p2, "x2"    # J
    .param p4, "x3"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    .param p5, "x4"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->onPinMoved(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private hasPins()Z
    .locals 1

    .prologue
    .line 191
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPins()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPinMoved(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "task"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    .param p2, "requestId"    # J
    .param p4, "request"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    .param p5, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 196
    invoke-virtual {p4}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->updateAdapter()V

    .line 201
    return-void
.end method

.method private onPinsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->isLoadingPins:Z

    .line 173
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 187
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->updateAdapter()V

    .line 188
    return-void

    .line 177
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->hasPins()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 181
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 182
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public cancelMovePinRequestsWithErrors()V
    .locals 7

    .prologue
    .line 153
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "cancelMovePinRequestsWithErrors"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 155
    .local v1, "toRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 156
    .local v2, "requestId":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    .line 157
    .local v0, "request":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 158
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .end local v0    # "request":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    .end local v2    # "requestId":J
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 162
    .restart local v2    # "requestId":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 166
    .end local v2    # "requestId":J
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->load(Z)V

    .line 167
    return-void
.end method

.method protected abstract createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.end method

.method public getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    return-object v0
.end method

.method public getPins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPins()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hasMovePinErrors()Z
    .locals 3

    .prologue
    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    .line 133
    .local v0, "req":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 134
    const/4 v1, 0x1

    .line 137
    .end local v0    # "req":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasPendingMovePinRequests()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPin(Ljava/lang/String;)Z
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->hasPin(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->isLoadingPins:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->isLoadingPins:Z

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadAsync(Z)V

    .line 89
    return-void
.end method

.method public movePin(Lcom/microsoft/xbox/service/model/pins/PinItem;II)V
    .locals 12
    .param p1, "pin"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p2, "fromPosition"    # I
    .param p3, "toPosition"    # I

    .prologue
    const/4 v7, 0x0

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Pin Reorder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ":"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v6, v8, v7}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    if-eq p2, p3, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v0

    .line 109
    .local v0, "pins":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 110
    .local v2, "sz":I
    if-ltz p2, :cond_2

    if-ge p2, v2, :cond_2

    .line 111
    if-ltz p3, :cond_1

    if-ge p3, v2, :cond_1

    .line 112
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/pins/PinItem;

    if-le p2, p3, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-direct {v1, v4, v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;-><init>(Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    .line 113
    .local v1, "req":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->curRequestId:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->curRequestId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->curRequestId:J

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;)V

    .line 115
    .local v3, "t":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->load(Z)V

    .line 125
    .end local v0    # "pins":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    .end local v1    # "req":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    .end local v2    # "sz":I
    .end local v3    # "t":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    :goto_1
    return-void

    .restart local v0    # "pins":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    .restart local v2    # "sz":I
    :cond_0
    move v6, v7

    .line 112
    goto :goto_0

    .line 117
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid toPosition: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 120
    :cond_2
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid fromPosition: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    .end local v0    # "pins":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    .end local v2    # "sz":I
    :cond_3
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fromPosition "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is the same as toPosition "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 44
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 35
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPins()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 38
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 39
    return-void

    .line 35
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 49
    return-void
.end method

.method public resubmitMovePinRequests()V
    .locals 7

    .prologue
    .line 141
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "resubmitMovePinRequests"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 143
    .local v2, "requestId":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->moveRequests:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;

    .line 144
    .local v0, "request":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 145
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->setLastResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 146
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;JLcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;)V

    .line 147
    .local v1, "t":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;->load(Z)V

    goto :goto_0

    .line 150
    .end local v0    # "request":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
    .end local v1    # "t":Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinTask;
    .end local v2    # "requestId":J
    :cond_1
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 64
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 77
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown update type ignore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :goto_0
    return-void

    .line 66
    .restart local v0    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->onPinsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 74
    :pswitch_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 81
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "ignoring non-final update"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
