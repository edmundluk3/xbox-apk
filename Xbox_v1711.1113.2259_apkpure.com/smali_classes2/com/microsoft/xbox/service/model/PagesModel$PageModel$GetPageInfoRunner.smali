.class Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PagesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPageInfoRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Lcom/microsoft/xbox/service/model/PagesModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/PagesModel$1;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;-><init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->access$100(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "XboxNews"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->this$0:Lcom/microsoft/xbox/service/model/PagesModel;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/PagesModel;->editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/service/editorial/EditorialService;->getXboxNewsInfo()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    .line 142
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;->buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 158
    const-wide/16 v0, 0xb

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;->this$1:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 153
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method
