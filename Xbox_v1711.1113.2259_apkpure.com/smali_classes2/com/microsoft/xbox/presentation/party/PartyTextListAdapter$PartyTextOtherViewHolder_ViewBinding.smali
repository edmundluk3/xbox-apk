.class public Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PartyTextListAdapter$PartyTextOtherViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;

    .line 23
    const v0, 0x7f0e049c

    const-string v1, "field \'gamertag\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->gamertag:Landroid/widget/TextView;

    .line 24
    const v0, 0x7f0e049d

    const-string v1, "field \'realname\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->realname:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0e04a0

    const-string v1, "field \'text\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->text:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0e049e

    const-string v1, "field \'arrow\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->arrow:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f0e0499

    const-string v1, "field \'timeSizer\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeSizer:Landroid/view/View;

    .line 28
    const v0, 0x7f0e049a

    const-string v1, "field \'timeStamp\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeStamp:Landroid/widget/TextView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;

    .line 35
    .local v0, "target":Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;

    .line 38
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->gamertag:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->realname:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->text:Landroid/widget/TextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->arrow:Landroid/widget/ImageView;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeSizer:Landroid/view/View;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeStamp:Landroid/widget/TextView;

    .line 44
    return-void
.end method
