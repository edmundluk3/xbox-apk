.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubSocialScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserClubsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V
    .locals 0

    .prologue
    .line 735
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;

    .prologue
    .line 735
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V

    return-void
.end method

.method static synthetic lambda$loadDataInBackground$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsFollowerOf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 739
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->loadDataInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 755
    .local v0, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 757
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 758
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v1

    .line 763
    :goto_0
    return-object v1

    .line 760
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1602(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    goto :goto_0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->onError()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 748
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1602(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 749
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 744
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 735
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 774
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1800(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Ljava/util/List;)V

    .line 775
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1702(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 769
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1602(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 770
    return-void
.end method
