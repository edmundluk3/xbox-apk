.class public Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;
.super Lcom/microsoft/xbox/presentation/base/ReactActivityBase;
.source "TournamentDetailsPageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;
    }
.end annotation


# static fields
.field private static final ID_KEY:Ljava/lang/String; = "id"

.field private static final ORGANIZER_KEY:Ljava/lang/String; = "organizer"


# instance fields
.field private parameters:Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/ReactActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchOptions()Landroid/os/Bundle;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 24
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;->parameters:Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    .line 25
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "launchOptions":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;->parameters:Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    if-eqz v1, :cond_0

    .line 28
    const-string v1, "organizer"

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;->parameters:Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    iget-object v2, v2, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;->organizer:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v1, "id"

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;->parameters:Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    iget-object v2, v2, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    return-object v0
.end method

.method public getLayoutId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public getModuleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    const-string v0, "TDP"

    return-object v0
.end method

.method public onCreateFinished()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method
