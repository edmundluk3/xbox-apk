.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedPostScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckPinStatusAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)V
    .locals 1

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 134
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$1;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    .line 155
    .local v1, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "timelineId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2, v1, v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->checkPinStatus(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->loadDataInBackground()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->onError()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Ljava/lang/Boolean;)V

    .line 145
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "isPinned"    # Ljava/lang/Boolean;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Ljava/lang/Boolean;)V

    .line 168
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 133
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->updateAdapter()V

    .line 163
    return-void
.end method
