.class public final Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;
.super Ljava/lang/Object;
.source "CommentsServiceDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommentAction"
.end annotation


# instance fields
.field public final comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;)V
    .locals 0
    .param p1, "comment"    # Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 129
    iput-object p1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    .line 130
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 134
    if-ne p1, p0, :cond_0

    .line 135
    const/4 v1, 0x1

    .line 140
    :goto_0
    return v1

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;

    if-nez v1, :cond_1

    .line 137
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 139
    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;

    .line 140
    .local v0, "other":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;
    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    iget-object v2, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->hashCode:I

    if-nez v0, :cond_0

    .line 147
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->hashCode:I

    .line 148
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->hashCode:I

    .line 151
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
