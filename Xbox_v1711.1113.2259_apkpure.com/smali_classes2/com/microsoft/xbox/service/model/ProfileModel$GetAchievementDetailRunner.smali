.class Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAchievementDetailRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private achievementId:I

.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private scid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "scid"    # Ljava/lang/String;
    .param p5, "achievementId"    # I

    .prologue
    .line 3923
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3924
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3925
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->xuid:Ljava/lang/String;

    .line 3926
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->scid:Ljava/lang/String;

    .line 3927
    iput p5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->achievementId:I

    .line 3928
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3932
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->xuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->scid:Ljava/lang/String;

    iget v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->achievementId:I

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getAchievementDetailInfo(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3916
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3946
    const-wide/16 v0, 0xbdc

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3941
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->scid:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetAchievementDetailRunner;->achievementId:I

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$4800(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;I)V

    .line 3942
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3937
    return-void
.end method
