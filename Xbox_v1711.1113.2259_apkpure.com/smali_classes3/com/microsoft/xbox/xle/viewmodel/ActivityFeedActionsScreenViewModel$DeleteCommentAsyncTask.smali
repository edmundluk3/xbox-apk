.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteCommentAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final commentId:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Ljava/lang/String;)V
    .locals 1
    .param p2, "commentId"    # Ljava/lang/String;

    .prologue
    .line 1009
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1006
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 1010
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->commentId:Ljava/lang/String;

    .line 1011
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1015
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1016
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->commentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->deleteComment(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1027
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 1021
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1022
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1700(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1023
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1700(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1042
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1005
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$DeleteCommentAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1037
    return-void
.end method
