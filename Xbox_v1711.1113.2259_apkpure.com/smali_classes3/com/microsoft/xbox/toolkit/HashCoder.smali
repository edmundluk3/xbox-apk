.class public final Lcom/microsoft/xbox/toolkit/HashCoder;
.super Ljava/lang/Object;
.source "HashCoder.java"


# static fields
.field public static final INITIAL_VALUE:I = 0x11

.field public static final PRIME_FACTOR:I = 0x1f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hashCode(B)I
    .locals 0
    .param p0, "b"    # B

    .prologue
    .line 13
    return p0
.end method

.method public static hashCode(C)I
    .locals 0
    .param p0, "c"    # C

    .prologue
    .line 17
    return p0
.end method

.method public static hashCode(D)I
    .locals 4
    .param p0, "d"    # D

    .prologue
    .line 37
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 38
    .local v0, "l":J
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v2

    return v2
.end method

.method public static hashCode(F)I
    .locals 1
    .param p0, "f"    # F

    .prologue
    .line 33
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    return v0
.end method

.method public static hashCode(I)I
    .locals 0
    .param p0, "i"    # I

    .prologue
    .line 25
    return p0
.end method

.method public static hashCode(J)I
    .locals 2
    .param p0, "l"    # J

    .prologue
    .line 29
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    xor-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method

.method public static hashCode(Ljava/lang/Object;)I
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 42
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static hashCode(S)I
    .locals 0
    .param p0, "s"    # S

    .prologue
    .line 21
    return p0
.end method

.method public static hashCode(Z)I
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 9
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hashCodeLowercase(Ljava/lang/String;)I
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 46
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
