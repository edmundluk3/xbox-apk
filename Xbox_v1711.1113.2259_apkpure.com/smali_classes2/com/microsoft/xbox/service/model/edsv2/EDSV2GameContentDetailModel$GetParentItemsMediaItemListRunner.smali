.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2GameContentDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetParentItemsMediaItemListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->getParentItemBigCatIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    .line 138
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->getParentItemBigCatIds()Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    .line 139
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->access$100(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;)Ljava/util/List;

    move-result-object v3

    .line 138
    invoke-interface {v2, v1, v3}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromIdsWithFilter(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    .line 140
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getListFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1

    .line 143
    .end local v0    # "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x2334

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 153
    const-wide/16 v0, 0x2334

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel$GetParentItemsMediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->access$200(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 149
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method
