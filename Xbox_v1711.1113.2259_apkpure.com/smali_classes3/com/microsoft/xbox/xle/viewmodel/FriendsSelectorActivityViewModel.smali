.class public Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "FriendsSelectorActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;
    }
.end annotation


# static fields
.field private static final MAX_ALLOWED_FRIENDS_SELECTED:I = 0x20

.field private static final MAX_ALLOWED_FRIENDS_SELECTED_BEFORE_WARNING:I = 0x1b


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final bundleKey:Ljava/lang/String;

.field private filteredFriendList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private friendList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingFollowerProfile:Z

.field private loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

.field private model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final originatingScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final returnToConversationDetailsScreen:Z

.field private snapShotSelection:Lcom/microsoft/xbox/toolkit/MultiSelection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/MultiSelection",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private totalSelected:I

.field private trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

.field private useSingleChoiceModeMaxCount:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 35
    const-string v1, "FriendSelector"

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->bundleKey:Ljava/lang/String;

    .line 36
    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->TAG:Ljava/lang/String;

    .line 42
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 47
    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    .line 50
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->useSingleChoiceModeMaxCount:Z

    .line 56
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 60
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMessagesOriginatingScreen()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->originatingScreen:Ljava/lang/Class;

    .line 61
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getReturnToConversationDetailsScreen()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->returnToConversationDetailsScreen:Z

    .line 63
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    .line 64
    return-void
.end method

.method private static GetTrieInputs(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v2, "trieInputs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;>;"
    if-eqz p0, :cond_0

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 120
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 121
    .local v1, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_0
    return-object v2
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isLoadingFollowerProfile:Z

    return p1
.end method

.method private initializePeopleSearch()V
    .locals 2

    .prologue
    .line 102
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isLoadingFollowerProfile:Z

    if-eqz v1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->GetTrieInputs(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 108
    .local v0, "trieInputs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->initialize(Ljava/util/List;)V

    goto :goto_0
.end method

.method private search(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p2, "searchText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "people":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v5, "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    if-eqz p1, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    if-eqz v8, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 291
    :cond_0
    return-object v5

    .line 264
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-virtual {v8, p2}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->search(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 266
    .local v3, "matchingWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    iget-object v8, v8, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->WordsDictionary:Ljava/util/Hashtable;

    if-eqz v8, :cond_0

    .line 267
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 270
    .local v2, "indexesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 271
    .local v7, "word":Ljava/lang/String;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    iget-object v9, v9, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->WordsDictionary:Ljava/util/Hashtable;

    invoke-virtual {v9, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 272
    .local v1, "indexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz v1, :cond_2

    .line 273
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 274
    .local v0, "index":Ljava/lang/Object;
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "index":Ljava/lang/Object;
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    .end local v1    # "indexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v7    # "word":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    .local v6, "sortedIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 284
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 285
    .local v0, "index":I
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 286
    .local v4, "person":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    new-instance v8, Lcom/microsoft/xbox/service/model/SearchResultPerson;

    invoke-direct {v8, v4, p2}, Lcom/microsoft/xbox/service/model/SearchResultPerson;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setSearchResultPerson(Lcom/microsoft/xbox/service/model/SearchResultPerson;)V

    .line 287
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private updateFriendsList()V
    .locals 7

    .prologue
    .line 156
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 157
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 159
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    const/4 v3, 0x0

    .line 160
    .local v3, "selected":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 161
    .local v0, "friend":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 162
    .local v4, "selectorItem":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/toolkit/MultiSelection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 163
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setSelected(Z)V

    .line 164
    add-int/lit8 v3, v3, 0x1

    .line 167
    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    .end local v0    # "friend":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v4    # "selectorItem":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    .line 172
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getConversationMembers()Ljava/util/Collection;

    move-result-object v2

    .line 173
    .local v2, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    if-eqz v2, :cond_3

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 174
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    .line 179
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getSortedByOnlineFavoritesAndFriendsList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    .line 181
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    .end local v2    # "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    .end local v3    # "selected":I
    :cond_2
    return-void

    .line 176
    .restart local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    .restart local v2    # "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    .restart local v3    # "selected":I
    :cond_3
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    goto :goto_1
.end method

.method private updateViewModelState()V
    .locals 2

    .prologue
    .line 86
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 88
    .local v0, "newState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 89
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 96
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v0, :cond_0

    .line 97
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 99
    :cond_0
    return-void

    .line 90
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 91
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 93
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public buildFilteredFriendList(Ljava/lang/String;)V
    .locals 3
    .param p1, "gamerSearchTag"    # Ljava/lang/String;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->search(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->filteredFriendList:Ljava/util/List;

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->filteredFriendList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->filteredFriendList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->filteredFriendList:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateAdapter()V

    .line 251
    :cond_1
    return-void
.end method

.method public canSelectFriends()Z
    .locals 2

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->useSingleChoiceModeMaxCount:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    const/16 v1, 0x20

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancel()V
    .locals 3

    .prologue
    .line 231
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->reset()V

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->snapShotSelection:Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->toArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 233
    .local v0, "key":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 236
    .end local v0    # "key":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->goBack()V

    .line 237
    return-void
.end method

.method public confirm()V
    .locals 0

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->goBack()V

    .line 214
    return-void
.end method

.method public getConversationMembers()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 390
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getViewAllMembers()Ljava/util/Collection;

    move-result-object v0

    .line 391
    .local v0, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    return-object v0
.end method

.method public getFilteredPeopleBasedOnSearchText()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->filteredFriendList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFriends()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 3

    .prologue
    .line 383
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 384
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getConversationGamerTag()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "gamerTag":Ljava/lang/String;
    return-object v0
.end method

.method public getSelectionCount()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    return v0
.end method

.method protected getSortedByOnlineFavoritesAndFriendsList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 396
    .local v1, "favoritesOnline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 398
    .local v0, "favoritesOffline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 399
    .local v3, "onlineFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 401
    .local v2, "offlineFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 403
    .local v5, "sortedFriendsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    if-eqz v6, :cond_3

    .line 404
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 405
    .local v4, "person":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-boolean v7, v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->isFavorite:Z

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsOnline()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 406
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    :cond_0
    iget-boolean v7, v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->isFavorite:Z

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsOnline()Z

    move-result v7

    if-nez v7, :cond_1

    .line 408
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409
    :cond_1
    iget-boolean v7, v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->isFavorite:Z

    if-nez v7, :cond_2

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsOnline()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 410
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 417
    .end local v4    # "person":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 418
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 421
    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_5

    .line 422
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 425
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 426
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 429
    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_7

    .line 430
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 432
    :cond_7
    return-object v5
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isLoadingFollowerProfile:Z

    return v0
.end method

.method public isFromMessage()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->originatingScreen:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->originatingScreen:Ljava/lang/Class;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;

    if-eq v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->returnToConversationDetailsScreen:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;->cancel()V

    .line 188
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;->load(Z)V

    .line 190
    return-void
.end method

.method public navigateToComposeMessage()V
    .locals 5

    .prologue
    .line 366
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Navigating to create message"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 370
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->originatingScreen:Ljava/lang/Class;

    if-eqz v1, :cond_0

    .line 371
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->originatingScreen:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 374
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->returnToConversationDetailsScreen:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putReturnToConversationDetailsScreen(Z)V

    .line 375
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 380
    .end local v0    # "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "User has no privilege to send message"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const v1, 0x7f07061c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->showError(I)V

    goto :goto_0
.end method

.method public onBackButtonPressed()V
    .locals 0

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->cancel()V

    .line 297
    return-void
.end method

.method public onLoadFollowerProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadFollowerProfileCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isLoadingFollowerProfile:Z

    .line 302
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 322
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateAdapter()V

    .line 323
    return-void

    .line 306
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 307
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateFriendsList()V

    .line 308
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->initializePeopleSearch()V

    .line 309
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateViewModelState()V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 317
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 318
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 69
    return-void
.end method

.method public onSearchBarClear()V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateAdapter()V

    .line 113
    return-void
.end method

.method protected onStartOverride()V
    .locals 3

    .prologue
    .line 194
    new-instance v1, Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->snapShotSelection:Lcom/microsoft/xbox/toolkit/MultiSelection;

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onstart called. make copy from global data"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->toArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 199
    .local v0, "key":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->snapShotSelection:Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 202
    .end local v0    # "key":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel$LoadFollowerProfileAsyncTask;->cancel()V

    .line 210
    :cond_0
    return-void
.end method

.method public saveSelection()V
    .locals 3

    .prologue
    .line 217
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->reset()V

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->friendList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 221
    .local v0, "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 226
    .end local v0    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_1
    return-void
.end method

.method public showWarningText()Z
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    const/16 v1, 0x1a

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleSelection(Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V
    .locals 1
    .param p1, "friend"    # Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->toggleSelection()V

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    .line 136
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->updateAdapter()V

    .line 137
    return-void

    .line 134
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->totalSelected:I

    goto :goto_0
.end method

.method public useSingleChoiceMode()V
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->useSingleChoiceModeMaxCount:Z

    .line 327
    return-void
.end method
