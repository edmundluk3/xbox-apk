.class final Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;
.super Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
.source "AutoValue_OOBEViewStates_OOBECodeEntryState.java"


# instance fields
.field private final badCode:Z

.field private final fatalError:Z

.field private final oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V
    .locals 0
    .param p1, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "badCode"    # Z
    .param p3, "fatalError"    # Z

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 20
    iput-boolean p2, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->badCode:Z

    .line 21
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->fatalError:Z

    .line 22
    return-void
.end method


# virtual methods
.method public badCode()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->badCode:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    .line 56
    .local v0, "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->badCode:Z

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->badCode()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->fatalError:Z

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->fatalError()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 56
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    :cond_4
    move v1, v2

    .line 60
    goto :goto_0
.end method

.method public fatalError()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->fatalError:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const v4, 0xf4243

    .line 65
    const/4 v0, 0x1

    .line 66
    .local v0, "h":I
    mul-int/2addr v0, v4

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 68
    mul-int/2addr v0, v4

    .line 69
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->badCode:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 70
    mul-int/2addr v0, v4

    .line 71
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->fatalError:Z

    if-eqz v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 72
    return v0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    move v1, v3

    .line 69
    goto :goto_1

    :cond_2
    move v2, v3

    .line 71
    goto :goto_2
.end method

.method public oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBECodeEntryState{oobeSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", badCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->badCode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;->fatalError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
