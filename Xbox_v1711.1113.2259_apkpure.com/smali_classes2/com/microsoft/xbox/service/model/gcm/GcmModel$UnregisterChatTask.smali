.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;
.super Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnregisterChatTask"
.end annotation


# instance fields
.field private isUnregChatSuccess:Z

.field private final meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private systemId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 1

    .prologue
    .line 1372
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    .line 1375
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;

    .prologue
    .line 1372
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    return-void
.end method

.method private unregisterWithChatBackend()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1438
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    .line 1439
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RegisterChatNotification - xuid is 0"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    const/4 v0, 0x0

    .line 1443
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2000()Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->systemId:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/chat/IChatService;->unregisterForNotifications(JLjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1372
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 1389
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Un-registering from Chat"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1800()[I

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget v0, v3, v2

    .line 1392
    .local v0, "delay":I
    if-lez v0, :cond_0

    .line 1393
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Waiting for %d seconds"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    mul-int/lit16 v5, v0, 0x3e8

    int-to-long v6, v5

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 1397
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isCancelled()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-eqz v5, :cond_2

    .line 1416
    .end local v0    # "delay":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    return-object v2

    .line 1402
    .restart local v0    # "delay":I
    :cond_2
    :try_start_1
    iget-boolean v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isUnregChatSuccess:Z

    if-nez v5, :cond_3

    .line 1403
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->unregisterWithChatBackend()Z

    move-result v5

    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isUnregChatSuccess:Z
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1409
    :cond_3
    :goto_2
    :try_start_2
    iget-boolean v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isUnregChatSuccess:Z

    if-nez v5, :cond_1

    .line 1391
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1405
    :catch_0
    move-exception v1

    .line 1406
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UnregisterChatTask failed with unregisterWithChatBackend:"

    invoke-static {v5, v6, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 1413
    .end local v0    # "delay":I
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_1
    move-exception v1

    .line 1414
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1372
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v5, 0x0

    .line 1421
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1422
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "post chat un-registeration"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1424
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->isUnregChatSuccess:Z

    if-eqz v1, :cond_0

    .line 1425
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1426
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "chat.am.channels"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1427
    const-string v1, "chat.dm.channels"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1428
    const-string v1, "chat.no.channels"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1429
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1432
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2402(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 1433
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1435
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1380
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->onPreExecute()V

    .line 1381
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "system.id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->systemId:Ljava/lang/String;

    .line 1383
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1384
    return-void
.end method
