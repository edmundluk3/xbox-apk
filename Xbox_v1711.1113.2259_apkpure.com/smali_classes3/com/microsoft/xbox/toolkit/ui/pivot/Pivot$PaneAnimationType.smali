.class public final enum Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;
.super Ljava/lang/Enum;
.source "Pivot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaneAnimationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

.field public static final enum EASE_OUT_EXPO_7:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

.field public static final enum LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    const-string v1, "LINEAR"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    const-string v1, "EASE_OUT_EXPO_7"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->EASE_OUT_EXPO_7:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->EASE_OUT_EXPO_7:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    return-object v0
.end method
