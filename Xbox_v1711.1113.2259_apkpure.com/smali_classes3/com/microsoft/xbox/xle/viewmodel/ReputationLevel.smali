.class public final enum Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;
.super Ljava/lang/Enum;
.source "ReputationLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum AvoidMe:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum GoodPlayer:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum InDanger:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum NeedsImprovement:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum Superstar:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

.field public static final enum Unknown:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "AvoidMe"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->AvoidMe:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "InDanger"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->InDanger:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "NeedsImprovement"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->NeedsImprovement:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "GoodPlayer"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->GoodPlayer:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "Superstar"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->Superstar:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    const-string v1, "Unknown"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->Unknown:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->AvoidMe:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->InDanger:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->NeedsImprovement:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->GoodPlayer:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->Superstar:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->Unknown:Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    return-object v0
.end method
