.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "FacebookManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UpdateFacebookOptInStatusAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p2, "status"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 676
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 677
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 678
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 682
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    .line 698
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v3, v4, :cond_1

    .line 700
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 701
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 702
    .local v2, "newSettings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;>;"
    new-instance v3, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;

    sget-object v4, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    sget-object v5, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;-><init>(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-direct {v4, v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;-><init>(Ljava/util/ArrayList;)V

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setPrivacySettings(Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 704
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 730
    .end local v2    # "newSettings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;>;"
    :goto_0
    return-object v3

    .line 707
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Facebook:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->updateThirdPartyToken(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 708
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 712
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1200(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v3, v4, :cond_2

    .line 713
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Facebook:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    sget-object v5, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z

    .line 715
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Facebook:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 716
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 719
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v4

    const-wide/16 v6, 0xf

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 721
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-class v4, Lcom/microsoft/xbox/service/model/errorresponse/BadRequestErrorResponse;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/errorresponse/BadRequestErrorResponse;

    .line 722
    .local v1, "errorResponse":Lcom/microsoft/xbox/service/model/errorresponse/BadRequestErrorResponse;
    if-eqz v1, :cond_3

    .line 723
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iget-wide v4, v1, Lcom/microsoft/xbox/service/model/errorresponse/BadRequestErrorResponse;->code:J

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1302(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;J)J

    .line 726
    .end local v1    # "errorResponse":Lcom/microsoft/xbox/service/model/errorresponse/BadRequestErrorResponse;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UpdateFacebookOptInStatusAsyncTask failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    .line 730
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 692
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 688
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 750
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq v0, v1, :cond_2

    .line 755
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->onUpdateFacebookOptInStatusCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 762
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackLoginStatus(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 763
    return-void

    .line 756
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_1

    .line 757
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "reset friend finder state failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070b6d

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 673
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 735
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_0

    .line 740
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    new-instance v1, Landroid/app/Dialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1402(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 741
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0301d9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 742
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0c0145

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 743
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->access$1500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    .line 746
    return-void
.end method
