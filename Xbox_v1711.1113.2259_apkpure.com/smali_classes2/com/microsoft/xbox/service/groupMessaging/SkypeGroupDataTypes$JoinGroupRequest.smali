.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "JoinGroupRequest"
.end annotation


# instance fields
.field private final role:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V
    .locals 0
    .param p1, "role"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 183
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;->role:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    .line 184
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
