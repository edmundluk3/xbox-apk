.class Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameDetailsProgressPhoneScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSocialAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private clipData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 435
    .local p2, "gameClipData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 436
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->clipData:Ljava/util/ArrayList;

    .line 437
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 441
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 442
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 474
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 475
    const/4 v1, 0x0

    .line 476
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->clipData:Ljava/util/ArrayList;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 478
    .local v2, "userClipRequestStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 479
    const-string v3, "GameDetailsProgressPhoneScreenViewModel"

    const-string v4, "Error loading game details game clips social info for the user"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 483
    .local v0, "clips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 484
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v3, v0, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 485
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 486
    const-string v3, "GameDetailsProgressPhoneScreenViewModel"

    const-string v4, "Error loading game details game clips social info for the community"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_1
    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 469
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 447
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$702(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z

    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z

    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 452
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$702(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z

    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z

    .line 463
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 464
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 465
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 432
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 456
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 457
    return-void
.end method
