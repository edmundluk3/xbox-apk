.class public Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ProfileShowcaseListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;",
            ">;"
        }
    .end annotation
.end field

.field private final likeContainers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/LikeControl;",
            ">;"
        }
    .end annotation
.end field

.field private final primaryViewResourceId:I

.field private profileGamerTag:Ljava/lang/String;

.field private useGamertag:Z

.field private vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vmWithLikeControlClick"    # Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;IZ)V

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "useGamerTag"    # Z

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;IZLcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZLcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "useGamerTag"    # Z
    .param p4, "vmWithLikeControlClick"    # Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->likeContainers:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->data:Ljava/util/ArrayList;

    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->context:Landroid/content/Context;

    .line 55
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->primaryViewResourceId:I

    .line 56
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->useGamertag:Z

    .line 57
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    .line 58
    return-void
.end method

.method public static createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    .locals 2
    .param p0, "layoutResId"    # I
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 61
    const/4 v1, 0x0

    invoke-virtual {p2, p0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public static createViewHolder(Landroid/content/Context;)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const v0, 0x7f0301d8

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$0(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToPostCommentScreen(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$1(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    goto :goto_0
.end method

.method static synthetic lambda$onBindViewHolder$10(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 166
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700bc

    .line 167
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700bb

    .line 168
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    .line 169
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 171
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 166
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 172
    return-void
.end method

.method static synthetic lambda$onBindViewHolder$2(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$3(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$4(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$5(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;

    check-cast p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .end local p1    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;->navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V

    .line 138
    :goto_0
    return-void

    .line 136
    .restart local p1    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;

    check-cast p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .end local p1    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;->navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    goto :goto_0
.end method

.method static synthetic lambda$onBindViewHolder$6(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getItemRoot()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;->navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$8(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Landroid/view/View;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 152
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700bc

    .line 153
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700bb

    .line 154
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    .line 155
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 157
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 152
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 158
    return-void
.end method


# virtual methods
.method public add(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 1
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method

.method public cleanupLikeClicks()V
    .locals 3

    .prologue
    .line 183
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->likeContainers:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->likeContainers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/LikeControl;

    .line 185
    .local v0, "control":Lcom/microsoft/xbox/xle/ui/LikeControl;
    if-eqz v0, :cond_0

    .line 186
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 190
    .end local v0    # "control":Lcom/microsoft/xbox/xle/ui/LikeControl;
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 71
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getProfileGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->profileGamerTag:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;I)V
    .locals 4
    .param p1, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 96
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->data:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 98
    .local v1, "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->vmWithLikeControlClick:Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;

    invoke-virtual {p1, v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->setLikeClickListener(Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    .line 100
    if-eqz v1, :cond_8

    .line 101
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_0

    .line 102
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_0
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_1

    .line 107
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_1
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_2

    .line 117
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_2
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_3

    .line 121
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_3
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v2, :cond_4

    .line 125
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_4
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->useGamertag:Z

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {p1, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->updateWithData(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)V

    .line 131
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_5

    .line 132
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_5
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_6

    .line 142
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_6
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_7

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canDeleteFeedItem(Ljava/lang/String;)Z

    move-result v0

    .line 149
    .local v0, "canDelete":Z
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 151
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    .end local v0    # "canDelete":Z
    :cond_7
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_8

    .line 162
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canDeleteFeedItem(Ljava/lang/String;)Z

    move-result v0

    .line 163
    .restart local v0    # "canDelete":Z
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 165
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    .end local v0    # "canDelete":Z
    :cond_8
    return-void

    .line 129
    :cond_9
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->titleName:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 87
    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->primaryViewResourceId:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->createViewHolder(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    move-result-object v0

    .line 88
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->likeContainers:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    return-object v0
.end method

.method public setProfileGamerTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "profileGamerTag"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->profileGamerTag:Ljava/lang/String;

    .line 83
    return-void
.end method
