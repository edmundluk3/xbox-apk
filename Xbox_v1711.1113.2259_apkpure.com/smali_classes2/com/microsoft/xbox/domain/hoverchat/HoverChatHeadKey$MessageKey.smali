.class public abstract Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
.super Ljava/lang/Object;
.source "HoverChatHeadKey.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MessageKey"
.end annotation


# static fields
.field private static GENERIC_INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static genericInstance()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->GENERIC_INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-nez v0, :cond_0

    .line 51
    const-string v0, "id_does_not_matter"

    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->GENERIC_INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .line 54
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->GENERIC_INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    return-object v0
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
    .locals 1
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_MessageKey;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_MessageKey;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract conversationId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
