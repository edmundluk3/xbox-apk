.class public Lcom/microsoft/xbox/service/model/TimestampedObject;
.super Ljava/lang/Object;
.source "TimestampedObject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public data:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public timestamp:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<TT;>;"
    .local p1, "data":Ljava/lang/Object;, "TT;"
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/model/TimestampedObject;-><init>(Ljava/util/Date;Ljava/lang/Object;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/util/Date;Ljava/lang/Object;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "this":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<TT;>;"
    .local p2, "data":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TimestampedObject;->timestamp:Ljava/util/Date;

    .line 14
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TimestampedObject;->data:Ljava/lang/Object;

    .line 15
    return-void
.end method
