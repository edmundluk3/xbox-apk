.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;
.source "HomeScreenAdapterUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CellListenerRecents"
.end annotation


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;-><init>()V

    .line 332
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 333
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method


# virtual methods
.method public bindRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;Ljava/lang/String;I)V
    .locals 7
    .param p1, "cell"    # Landroid/view/View;
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;
    .param p3, "userAction"    # Ljava/lang/String;
    .param p4, "position"    # I

    .prologue
    const v6, 0x7f0e0069

    const v5, 0x7f0e001e

    const v3, 0x7f0e001b

    const/4 v4, 0x0

    .line 357
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 358
    .local v2, "oldRecent":Lcom/microsoft/xbox/service/model/recents/Recent;
    if-eq p2, v2, :cond_1

    .line 359
    invoke-virtual {p1, v3, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 360
    const v3, 0x7f0e06d6

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 361
    .local v0, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    if-eqz p2, :cond_0

    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    if-nez v3, :cond_2

    .line 362
    :cond_0
    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    invoke-virtual {p1, v6, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 364
    invoke-virtual {p1, v5, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 365
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->clearImage()V

    .line 366
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 367
    invoke-virtual {p1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 368
    invoke-static {p1, v4, v4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 385
    .end local v0    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    :goto_0
    return-void

    .line 370
    .restart local v0    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_2
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    invoke-virtual {p1, v6, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 372
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v5, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 373
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 374
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 375
    :cond_3
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x7f070fca

    :goto_1
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 376
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 381
    :goto_2
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {p1, v3, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 375
    :cond_4
    const v3, 0x7f070fdb

    goto :goto_1

    .line 378
    :cond_5
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setImageUrl(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Ljava/lang/String;)V

    .line 379
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackgroundOptional(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/view/View;)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "cell"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 337
    const v6, 0x7f0e001b

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 338
    .local v4, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    if-eqz v4, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 339
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecent(Lcom/microsoft/xbox/service/model/recents/Recent;)V

    .line 340
    const v6, 0x7f0e0069

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 341
    .local v5, "userAction":Ljava/lang/String;
    const v6, 0x7f0e0018

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 342
    .local v1, "hasStats":Ljava/lang/Boolean;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_2

    :cond_0
    const-string v0, "0"

    .line 343
    .local v0, "content":Ljava/lang/String;
    :goto_1
    const v6, 0x7f0e001e

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 345
    .local v2, "indexPosition":Ljava/lang/Integer;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v6

    iget-object v9, v4, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    invoke-virtual {v6, v5, v0, v9, v8}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 346
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;->getPopup()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v3

    .line 347
    .local v3, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    new-instance v6, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    new-instance v8, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents$1;

    invoke-direct {v8, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;)V

    invoke-direct {v6, v8}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;-><init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 353
    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 354
    return-void

    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "hasStats":Ljava/lang/Boolean;
    .end local v2    # "indexPosition":Ljava/lang/Integer;
    .end local v3    # "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .end local v5    # "userAction":Ljava/lang/String;
    :cond_1
    move v6, v8

    .line 338
    goto :goto_0

    .line 342
    .restart local v1    # "hasStats":Ljava/lang/Boolean;
    .restart local v5    # "userAction":Ljava/lang/String;
    :cond_2
    const-string v0, "1"

    goto :goto_1

    .line 345
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v2    # "indexPosition":Ljava/lang/Integer;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2
.end method
