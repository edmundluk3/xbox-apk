.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V
    .locals 1

    .prologue
    .line 883
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 884
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;

    .prologue
    .line 883
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 907
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 883
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 898
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 883
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 893
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/entity/EntityModel;)V

    .line 894
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 912
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/entity/EntityModel;)V

    .line 913
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 883
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 903
    return-void
.end method
