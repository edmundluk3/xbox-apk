.class public Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ImageViewerScreenViewModel.java"


# instance fields
.field private images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 14
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedImages()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->images:Ljava/util/List;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 16
    return-void
.end method


# virtual methods
.method public getImageCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->images:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageUris()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->images:Ljava/util/List;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 43
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 27
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
