.class final synthetic Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field private static final instance:Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;

    invoke-direct {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;->instance:Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiFunction;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;->instance:Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    check-cast p2, Lcom/google/common/collect/ImmutableList;

    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->lambda$null$2(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v0

    return-object v0
.end method
