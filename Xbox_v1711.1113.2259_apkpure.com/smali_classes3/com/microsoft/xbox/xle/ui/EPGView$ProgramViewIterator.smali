.class Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;
.super Ljava/lang/Object;
.source "EPGView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgramViewIterator"
.end annotation


# instance fields
.field private mForward:Z

.field private mIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

.field private mRow:I

.field private mShowPrograms:Z

.field private mTime:J

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V
    .locals 0

    .prologue
    .line 839
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/ui/EPGView$1;

    .prologue
    .line 839
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    return-void
.end method

.method private getGapPlaceholder(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 947
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    if-eqz v0, :cond_0

    .line 948
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    long-to-float v0, v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1000(Lcom/microsoft/xbox/xle/ui/EPGView;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-long v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;JJ)Landroid/view/View;

    move-result-object v0

    .line 950
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    long-to-float v0, v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1000(Lcom/microsoft/xbox/xle/ui/EPGView;)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;JJ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private getGapPlaceholder(Landroid/view/ViewGroup;JJ)Landroid/view/View;
    .locals 10
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "start"    # J
    .param p4, "end"    # J

    .prologue
    const/4 v6, 0x0

    .line 956
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v1

    .line 959
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getEmptyProgramData()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v9

    .line 960
    .local v9, "program":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    move-result-object v0

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mRow:I

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v7

    .line 961
    .local v7, "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVerticalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v8

    .line 963
    .local v8, "fastScrolling":Z
    if-nez v8, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$600(Lcom/microsoft/xbox/xle/ui/EPGView;)Z

    move-result v2

    invoke-virtual {v1, v9, v7, v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;ZZ)V

    .line 965
    sub-long v4, p4, p2

    move-object v0, p0

    move-wide v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->setStartAndDuration(Landroid/view/View;JJZ)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v6

    .line 963
    goto :goto_0
.end method

.method private getLoadPlaceholder(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 934
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    if-eqz v0, :cond_0

    .line 935
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    .line 936
    .local v2, "start":J
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    long-to-float v0, v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1000(Lcom/microsoft/xbox/xle/ui/EPGView;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-long v8, v0

    .line 943
    .local v8, "end":J
    :goto_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    move-result-object v1

    sub-long v4, v8, v2

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->setStartAndDuration(Landroid/view/View;JJZ)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 938
    .end local v2    # "start":J
    .end local v8    # "end":J
    :cond_0
    iget-wide v8, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    .line 939
    .restart local v8    # "end":J
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    long-to-float v0, v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1000(Lcom/microsoft/xbox/xle/ui/EPGView;)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-long v2, v0

    .restart local v2    # "start":J
    goto :goto_0
.end method

.method private setStartAndDuration(Landroid/view/View;JJZ)Landroid/view/View;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "start"    # J
    .param p4, "duration"    # J
    .param p6, "temporary"    # Z

    .prologue
    .line 916
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$900(Lcom/microsoft/xbox/xle/ui/EPGView;J)I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v1, p4, p5}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertSecondsToPixels(J)I

    move-result v1

    invoke-static {p1, v0, v1, p6}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setChildViewLayoutParams(Landroid/view/View;IIZ)Landroid/view/View;

    .line 919
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    if-eqz v0, :cond_0

    .line 920
    add-long v0, p2, p4

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    .line 924
    :goto_0
    return-object p1

    .line 922
    :cond_0
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    goto :goto_0
.end method


# virtual methods
.method public getNext(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 872
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mShowPrograms:Z

    if-nez v0, :cond_0

    .line 873
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getLoadPlaceholder(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 910
    :goto_0
    return-object v0

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    if-nez v0, :cond_1

    .line 877
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->get()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v9

    .line 883
    .local v9, "program":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    if-nez v9, :cond_2

    .line 884
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 888
    :cond_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    if-eqz v0, :cond_3

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v0

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 889
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v0

    int-to-long v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;JJ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 890
    :cond_3
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    if-nez v0, :cond_4

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getEndTime()I

    move-result v0

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    .line 891
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getEndTime()I

    move-result v0

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->getGapPlaceholder(Landroid/view/ViewGroup;JJ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 895
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->readNext()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 900
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v1

    .line 901
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    move-result-object v0

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mRow:I

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v7

    .line 903
    .local v7, "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVerticalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v8

    .line 904
    .local v8, "fastScrolling":Z
    if-nez v8, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$600(Lcom/microsoft/xbox/xle/ui/EPGView;)Z

    move-result v2

    invoke-virtual {v1, v9, v7, v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;ZZ)V

    .line 907
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$800(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v0

    invoke-static {v0, v9}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setSelected(Z)V

    .line 910
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getDuration()I

    move-result v0

    int-to-long v4, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->setStartAndDuration(Landroid/view/View;JJZ)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move v0, v6

    .line 904
    goto :goto_1
.end method

.method public reset(IJZ)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "time"    # J
    .param p4, "forward"    # Z

    .prologue
    .line 849
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mTime:J

    .line 850
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mForward:Z

    .line 851
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mRow:I

    .line 853
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGView$11;->$SwitchMap$com$microsoft$xbox$xle$ui$virtualgrid$VirtualGrid$ScrollState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVerticalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 863
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mShowPrograms:Z

    .line 866
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getProgramEnumerator(IJZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    .line 867
    return-void

    .line 855
    :pswitch_0
    sget-boolean v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->SHOW_PROGRAM_TITLE_ON_VERTICAL_FLING:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mShowPrograms:Z

    goto :goto_0

    .line 859
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->mShowPrograms:Z

    goto :goto_0

    .line 853
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
