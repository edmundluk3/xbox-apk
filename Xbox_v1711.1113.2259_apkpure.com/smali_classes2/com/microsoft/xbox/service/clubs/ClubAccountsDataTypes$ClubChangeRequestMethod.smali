.class public final enum Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;
.super Ljava/lang/Enum;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubChangeRequestMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

.field public static final enum ChangeName:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

.field public static final enum TransferOwnership:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 256
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    const-string v1, "ChangeName"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->ChangeName:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    .line 257
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    const-string v1, "TransferOwnership"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->TransferOwnership:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    .line 255
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->ChangeName:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->TransferOwnership:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 255
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    return-object v0
.end method
