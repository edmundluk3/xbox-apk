.class final synthetic Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field private final arg$2:Ljava/util/List;

.field private final arg$3:J

.field private final arg$4:Landroid/app/AlertDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$2:Ljava/util/List;

    iput-wide p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$3:J

    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$4:Landroid/app/AlertDialog;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;)Landroid/view/View$OnClickListener;
    .locals 8

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;)V

    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$2:Ljava/util/List;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$3:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->arg$4:Landroid/app/AlertDialog;

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->lambda$hideActivitiesFromTitle$4(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;Landroid/view/View;)V

    return-void
.end method
