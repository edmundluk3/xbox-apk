.class public Lde/tavendo/autobahn/ByteBufferOutputStream;
.super Ljava/io/OutputStream;
.source "ByteBufferOutputStream.java"


# instance fields
.field private mBuffer:Ljava/nio/ByteBuffer;

.field private final mGrowSize:I

.field private final mInitialSize:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    const/high16 v0, 0x20000

    const/high16 v1, 0x10000

    invoke-direct {p0, v0, v1}, Lde/tavendo/autobahn/ByteBufferOutputStream;-><init>(II)V

    .line 47
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "initialSize"    # I
    .param p2, "growSize"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 56
    iput p1, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mInitialSize:I

    .line 57
    iput p2, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mGrowSize:I

    .line 58
    iget v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mInitialSize:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    .line 59
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 60
    return-void
.end method


# virtual methods
.method public clear()Ljava/nio/Buffer;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized crlf()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/ByteBufferOutputStream;->write(I)V

    .line 173
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/ByteBufferOutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized expand(I)V
    .locals 5
    .param p1, "requestSize"    # I

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-le p1, v3, :cond_0

    .line 101
    iget-object v1, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    .line 102
    .local v1, "oldBuffer":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    .line 103
    .local v2, "oldPosition":I
    iget v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mGrowSize:I

    div-int v3, p1, v3

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mGrowSize:I

    mul-int v0, v3, v4

    .line 104
    .local v0, "newCapacity":I
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    .line 105
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 106
    iget-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 107
    iget-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 108
    iget-object v3, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    .end local v0    # "newCapacity":I
    .end local v1    # "oldBuffer":Ljava/nio/ByteBuffer;
    .end local v2    # "oldPosition":I
    :cond_0
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public flip()Ljava/nio/Buffer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public getBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public remaining()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    return v0
.end method

.method public declared-synchronized write(I)V
    .locals 2
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 121
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/ByteBufferOutputStream;->expand(I)V

    .line 123
    :cond_0
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/ByteBufferOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    monitor-exit p0

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([B)V
    .locals 2
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lde/tavendo/autobahn/ByteBufferOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 2
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p3

    iget-object v1, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 140
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/ByteBufferOutputStream;->expand(I)V

    .line 142
    :cond_0
    iget-object v0, p0, Lde/tavendo/autobahn/ByteBufferOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
