.class Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;
.super Ljava/lang/Object;
.source "EPGIteratorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EPGFetchCache"
.end annotation


# instance fields
.field private cache:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field private startTime:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    .line 524
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    .line 528
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    .prologue
    .line 522
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    return v0
.end method

.method private static final getChannelIndex(I)I
    .locals 1
    .param p0, "channel"    # I

    .prologue
    .line 539
    div-int/lit8 v0, p0, 0x32

    return v0
.end method

.method private static final getTimeIndex(I)I
    .locals 1
    .param p0, "time"    # I

    .prologue
    .line 543
    div-int/lit16 v0, p0, 0x7080

    return v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 531
    const-string v0, "EPGIteratorFactory"

    const-string v1, "Reset Cache"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    .line 534
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    .line 535
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    div-int/lit16 v0, v0, 0xe10

    mul-int/lit16 v0, v0, 0xe10

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    .line 536
    return-void
.end method


# virtual methods
.method public add(II)V
    .locals 4
    .param p1, "time"    # I
    .param p2, "channel"    # I

    .prologue
    .line 577
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->check(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 586
    :goto_0
    return-void

    .line 581
    :cond_0
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getChannelIndex(I)I

    move-result v0

    .line 582
    .local v0, "chIndex":I
    iget v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    sub-int v3, p1, v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getTimeIndex(I)I

    move-result v2

    .line 584
    .local v2, "tmIndex":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/BitSet;

    .line 585
    .local v1, "flags":Ljava/util/BitSet;
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    goto :goto_0
.end method

.method public check(II)Z
    .locals 5
    .param p1, "time"    # I
    .param p2, "channel"    # I

    .prologue
    .line 551
    iget v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    if-ge p1, v3, :cond_0

    .line 554
    const/4 v3, 0x1

    .line 573
    :goto_0
    return v3

    .line 557
    :cond_0
    iget v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    if-nez v3, :cond_2

    .line 558
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->init()V

    .line 561
    :cond_2
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getChannelIndex(I)I

    move-result v0

    .line 562
    .local v0, "chIndex":I
    iget v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    sub-int v3, p1, v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getTimeIndex(I)I

    move-result v2

    .line 564
    .local v2, "tmIndex":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v3, v0, :cond_3

    .line 565
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Vector;->setSize(I)V

    .line 568
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4

    .line 569
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    new-instance v4, Ljava/util/BitSet;

    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v3, v0, v4}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 572
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/BitSet;

    .line 573
    .local v1, "flags":Ljava/util/BitSet;
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    goto :goto_0
.end method

.method public createFetchState(II)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .locals 6
    .param p1, "time"    # I
    .param p2, "channel"    # I

    .prologue
    .line 596
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    if-nez v2, :cond_1

    .line 597
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->init()V

    .line 599
    :cond_1
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getChannelIndex(I)I

    move-result v0

    .line 600
    .local v0, "chIndex":I
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    sub-int v2, p1, v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->getTimeIndex(I)I

    move-result v1

    .line 602
    .local v1, "tmIndex":I
    new-instance v2, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    mul-int/lit8 v3, v0, 0x32

    iget v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    mul-int/lit16 v5, v1, 0x7080

    add-int/2addr v4, v5

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;-><init>(II)V

    return-object v2
.end method

.method public createInitialState()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .locals 3

    .prologue
    .line 589
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->cache:Ljava/util/Vector;

    if-nez v0, :cond_1

    .line 590
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->init()V

    .line 592
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    const/4 v1, 0x0

    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->startTime:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;-><init>(II)V

    return-object v0
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 547
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->init()V

    .line 548
    return-void
.end method
