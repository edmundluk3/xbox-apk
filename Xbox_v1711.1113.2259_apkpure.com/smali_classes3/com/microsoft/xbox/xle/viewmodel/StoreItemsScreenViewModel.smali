.class public Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "StoreItemsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_LIST_ROW_LAYOUT_ID:I = 0x7f030216

.field private static final GAMEPASS_LIST_ROW_LAYOUT_ID:I = 0x7f030218

.field public static final MAX_GAME_ITEMS:I = 0xc8

.field public static final UNDEFINED_LIST_ROW_LAYOUT_ID:I = -0x80000000


# instance fields
.field private filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field private isLoadingStoreItems:Z

.field private lastItemSelected:I

.field private listRowLayoutId:I

.field private loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

.field private model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

.field private result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

.field private searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;
    .param p2, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 37
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 43
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 44
    const v0, 0x7f030216

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->listRowLayoutId:I

    .line 52
    const/high16 v0, -0x80000000

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->init(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;I)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;
    .param p2, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p3, "listRowLayoutId"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 37
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 43
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 44
    const v0, 0x7f030216

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->listRowLayoutId:I

    .line 56
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->init(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;I)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->isLoadingStoreItems:Z

    return p1
.end method


# virtual methods
.method public getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    return-object v0
.end method

.method public getListRowLayoutId()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->listRowLayoutId:I

    return v0
.end method

.method public getResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    return-object v0
.end method

.method public getSearchType()Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getlastSelectedItemIndex()I
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->lastItemSelected:I

    .line 169
    .local v0, "index":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->lastItemSelected:I

    .line 170
    return v0
.end method

.method public gotoDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;I)V
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "index"    # I

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "pageUri":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v6

    add-int/lit8 v0, v6, 0x1

    .line 127
    .local v0, "filterPosition":I
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->lastItemSelected:I

    .line 129
    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 130
    .local v4, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    .line 131
    .local v1, "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putStoreFilterPosition(I)V

    .line 134
    const-string v6, "Store"

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 137
    const-string v5, "NotUsed"

    .line 139
    .local v5, "pivotName":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 156
    :goto_0
    const-string v6, "NotUsed"

    if-eq v5, v6, :cond_0

    .line 157
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 158
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v6, "MediaId"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    const-string v6, "ListIndex"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    const-string v6, "MediaType"

    invoke-virtual {v2, v6, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    const-string v6, "Store - View Store Item"

    invoke-static {v6, v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 164
    .end local v2    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    invoke-virtual {p0, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 165
    return-void

    .line 141
    :pswitch_0
    const-string v5, "Add-ons"

    .line 142
    goto :goto_0

    .line 145
    :pswitch_1
    const-string v5, "Games"

    .line 146
    goto :goto_0

    .line 149
    :pswitch_2
    const-string v5, "Apps"

    .line 150
    goto :goto_0

    .line 153
    :pswitch_3
    const-string v5, "Game Pass"

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected init(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;I)V
    .locals 2
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;
    .param p2, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p3, "listRowLayoutId"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 63
    const/high16 v0, -0x80000000

    if-ne p3, v0, :cond_0

    const p3, 0x7f030216

    .end local p3    # "listRowLayoutId":I
    :cond_0
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->listRowLayoutId:I

    .line 65
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 85
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 88
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0, p2, v1}, Lcom/microsoft/xbox/service/model/StoreModel;->getStoreBrowseModel(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 90
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    .line 92
    return-void

    .line 67
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    goto :goto_0

    .line 71
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Recent:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->New:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    goto :goto_0

    .line 79
    :pswitch_3
    const-string v0, "Unexpected: setting not set to using Arches RTM lists"

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->useArchesRTMLists()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 80
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->GamepassRTMList1:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 81
    const v0, 0x7f030218

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->listRowLayoutId:I

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->isLoadingStoreItems:Z

    return v0
.end method

.method public isNeedLoadMore()Z
    .locals 3

    .prologue
    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 238
    .local v0, "storeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0xc8

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getTotalCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 239
    const/4 v1, 0x1

    .line 242
    .end local v0    # "storeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->cancel()V

    .line 195
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->load(Z)V

    .line 197
    return-void
.end method

.method public loadMore()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->cancel()V

    .line 231
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->load(Z)V

    .line 233
    return-void
.end method

.method public onLoadItemsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .prologue
    .line 246
    const-string v0, "Store Item ViewModel"

    const-string v1, "onLoadItemsCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->isLoadingStoreItems:Z

    .line 248
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->updateAdapter()V

    .line 263
    return-void

    .line 252
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 257
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 258
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getIsActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    const-string v0, "Store - Featured"

    .line 204
    .local v0, "pivotName":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 222
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 224
    .end local v0    # "pivotName":Ljava/lang/String;
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->onSetActive()V

    .line 225
    return-void

    .line 206
    .restart local v0    # "pivotName":Ljava/lang/String;
    :pswitch_0
    const-string v0, "Store - Add-ons"

    .line 207
    goto :goto_0

    .line 210
    :pswitch_1
    const-string v0, "Store - Games"

    .line 211
    goto :goto_0

    .line 214
    :pswitch_2
    const-string v0, "Store - Apps"

    .line 215
    goto :goto_0

    .line 218
    :pswitch_3
    const-string v0, "Store - Game Pass"

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->loadStoreItemsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->cancel()V

    .line 188
    :cond_0
    return-void
.end method

.method public setFilter(Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V
    .locals 3
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    if-ne p1, v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->result:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 118
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->searchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreModel;->getStoreBrowseModel(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->load(Z)V

    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->updateAdapter()V

    .line 121
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->getTelemetryName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackFilter(Ljava/lang/String;)V

    goto :goto_0
.end method
