.class public final enum Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
.super Ljava/lang/Enum;
.source "DelegateRPSTicketResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_INVALID_APPURI:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_INVALID_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_INVALID_SIGNATURE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_NOCID:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_UNEXPECTED:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum RESULT_UNKNOWN_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field public static final enum UNKNOWN_RESULT_CODE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_NOCID"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_NOCID:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_UNEXPECTED"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNEXPECTED:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_UNKNOWN_PACKAGE"

    invoke-direct {v0, v1, v7, v7}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNKNOWN_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_INVALID_PACKAGE"

    invoke-direct {v0, v1, v8, v8}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_INVALID_SIGNATURE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_SIGNATURE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "RESULT_INVALID_APPURI"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_APPURI:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    const-string v1, "UNKNOWN_RESULT_CODE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->UNKNOWN_RESULT_CODE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_NOCID:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNEXPECTED:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNKNOWN_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_PACKAGE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_SIGNATURE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_INVALID_APPURI:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->UNKNOWN_RESULT_CODE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->$VALUES:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->value:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->$VALUES:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->value:I

    return v0
.end method
