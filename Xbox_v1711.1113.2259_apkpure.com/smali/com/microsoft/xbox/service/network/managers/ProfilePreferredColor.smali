.class public Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
.super Ljava/lang/Object;
.source "ProfilePreferredColor.java"


# static fields
.field private static transient DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;


# instance fields
.field private primary:I

.field private primaryColorString:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "primaryColor"
    .end annotation
.end field

.field private secondary:I

.field private secondaryColorString:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "secondaryColor"
    .end annotation
.end field

.field private tertiary:I

.field private tertiaryColorString:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tertiaryColor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primary:I

    .line 13
    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondary:I

    .line 14
    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiary:I

    return-void
.end method

.method public static convertColorFromString(Ljava/lang/String;)I
    .locals 2
    .param p0, "colorString"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 94
    if-nez p0, :cond_1

    .line 95
    const/4 v0, 0x0

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    const-string v1, "#"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 98
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 100
    :cond_2
    const/16 v1, 0x10

    invoke-static {p0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 101
    .local v0, "color":I
    shr-int/lit8 v1, v0, 0x18

    if-nez v1, :cond_0

    .line 102
    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public static defaultProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    const v1, 0x107c10

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primary:I

    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    const v1, 0x102b14

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondary:I

    .line 47
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    const v1, 0x155715

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiary:I

    .line 50
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->DEFAULT_PROFILE_COLOR:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public static with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1
    .param p0, "primaryColorString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "secondaryColorString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "tertiaryColorString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 32
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 33
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;-><init>()V

    .line 36
    .local v0, "color":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    iput-object p0, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primaryColorString:Ljava/lang/String;

    .line 37
    iput-object p1, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondaryColorString:Ljava/lang/String;

    .line 38
    iput-object p2, v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiaryColorString:Ljava/lang/String;

    .line 39
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 110
    instance-of v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v2, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 114
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 116
    .local v0, "otherColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getPrimaryColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 67
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primary:I

    if-gez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primaryColorString:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primary:I

    .line 71
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primary:I

    return v0
.end method

.method public getPrimaryColorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->primaryColorString:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 76
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondary:I

    if-gez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondaryColorString:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondary:I

    .line 80
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondary:I

    return v0
.end method

.method public getSecondaryColorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->secondaryColorString:Ljava/lang/String;

    return-object v0
.end method

.method public getTertiaryColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 85
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiary:I

    if-gez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiaryColorString:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiary:I

    .line 89
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiary:I

    return v0
.end method

.method public getTertiaryColorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->tertiaryColorString:Ljava/lang/String;

    return-object v0
.end method
