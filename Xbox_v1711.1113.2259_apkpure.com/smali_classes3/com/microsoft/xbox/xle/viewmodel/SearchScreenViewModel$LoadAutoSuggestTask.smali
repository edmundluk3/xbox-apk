.class Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SearchScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAutoSuggestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private suggested:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;"
        }
    .end annotation
.end field

.field private term:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "term"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 206
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->term:Ljava/lang/String;

    .line 207
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 227
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->term:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/service/store/IStoreService;->getSearchAutoSuggestions(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    .line 229
    .local v2, "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 231
    const-string v3, "Games"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResultByProductFamilyName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;

    move-result-object v1

    .line 233
    .local v1, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 234
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->suggested:Ljava/util/List;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    .end local v1    # "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    .end local v2    # "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->suggested:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 244
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 246
    :goto_1
    return-object v3

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to get auto suggest "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->suggested:Ljava/util/List;

    goto :goto_0

    .line 246
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 216
    const-string v0, "This should not happen"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->suggested:Ljava/util/List;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V

    .line 257
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 200
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method
