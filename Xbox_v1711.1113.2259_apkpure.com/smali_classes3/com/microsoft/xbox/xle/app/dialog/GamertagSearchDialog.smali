.class public Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "GamertagSearchDialog.java"


# instance fields
.field private closeIcon:Landroid/widget/TextView;

.field private gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private gamertagSearching:Landroid/widget/TextView;

.field private handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

.field private searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

.field private searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private searchList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private softDismissViewBottom:Landroid/view/View;

.field private softDismissViewLeft:Landroid/view/View;

.field private softDismissViewRight:Landroid/view/View;

.field private softDismissViewTop:Landroid/view/View;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 51
    const v1, 0x7f0802b9

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 54
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030131

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLERootView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->dialogBody:Landroid/view/View;

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b4

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070591

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setHint(Ljava/lang/String;)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b3

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewTop:Landroid/view/View;

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06ba

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewLeft:Landroid/view/View;

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b6

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b7

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearching:Landroid/widget/TextView;

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b9

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewBottom:Landroid/view/View;

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06bb

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewRight:Landroid/view/View;

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b5

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->closeIcon:Landroid/widget/TextView;

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->closeIcon:Landroid/widget/TextView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    const v2, 0x7f0e06b8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewBottom:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setEmptyView(Landroid/view/View;)V

    .line 73
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->requestWindowFeature(I)Z

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->handleImeRootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->setContentView(Landroid/view/View;)V

    .line 77
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->setCanceledOnTouchOutside(Z)V

    .line 78
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->setCancelable(Z)V

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewTop:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewLeft:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewBottom:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewRight:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->softDismissViewRight:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$4;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_2

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$6;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 47
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)Lcom/microsoft/xbox/xle/ui/SearchBarView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    return-object v0
.end method


# virtual methods
.method public OnAnimationInEnd()V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->OnAnimationInEnd()V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->requestFocusAndShowIME()V

    .line 190
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method protected dismissKeyboard()V
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 196
    :cond_0
    return-void
.end method

.method protected getBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .locals 3
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;
    .param p2, "goingBack"    # Z

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 230
    .local v0, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->getDialogBody()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 232
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v1

    const-string v2, "SearchDialog"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->getDialogBody()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v0

    .line 234
    :cond_0
    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->close()V

    .line 205
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$8;-><init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onStart()V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSetActive()V

    .line 174
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->dismissKeyboard()V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSetInactive()V

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onStop()V

    .line 183
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 184
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->dismiss()V

    .line 135
    :cond_0
    return-void
.end method

.method public showError(Z)V
    .locals 2
    .param p1, "isError"    # Z

    .prologue
    const/16 v1, 0x8

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearching:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 214
    if-eqz p1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showGamertagSearchDialog()V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->show()V

    .line 129
    return-void
.end method

.method protected showKeyboard(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 199
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showKeyboard(Landroid/view/View;I)V

    .line 200
    return-void
.end method

.method public showLoading()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearchError:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->gamertagSearching:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    return-void
.end method
