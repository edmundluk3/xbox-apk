.class public Lcom/microsoft/playready/ResourceUtil$Peeker;
.super Ljava/lang/Object;
.source "ResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/ResourceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Peeker"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Util.Net.Peeker"


# instance fields
.field mContentHeader:[B

.field mMimeType:Ljava/lang/String;

.field mUrl:Ljava/net/URL;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mUrl:Ljava/net/URL;

    .line 244
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mMimeType:Ljava/lang/String;

    .line 245
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mContentHeader:[B

    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/playready/ResourceUtil$Peeker;->peek(Ljava/lang/String;I)V

    .line 252
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "contentLength"    # I

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mUrl:Ljava/net/URL;

    .line 244
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mMimeType:Ljava/lang/String;

    .line 245
    iput-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mContentHeader:[B

    .line 259
    invoke-direct {p0, p1, p2}, Lcom/microsoft/playready/ResourceUtil$Peeker;->peek(Ljava/lang/String;I)V

    .line 260
    return-void
.end method

.method private peek(Ljava/lang/String;I)V
    .locals 11
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "peekSize"    # I

    .prologue
    .line 272
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mUrl:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    const/4 v1, 0x0

    .line 279
    .local v1, "conn":Ljava/net/HttpURLConnection;
    const/4 v3, 0x0

    .line 281
    .local v3, "is":Ljava/io/InputStream;
    :try_start_1
    iget-object v9, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mUrl:Ljava/net/URL;

    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 282
    const/16 v9, 0x1f40

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 283
    const/16 v9, 0x1f40

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 284
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 285
    const-string v9, "GET"

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 287
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 288
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mMimeType:Ljava/lang/String;

    .line 290
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    .end local v3    # "is":Ljava/io/InputStream;
    .local v4, "is":Ljava/io/InputStream;
    if-lez p2, :cond_2

    .line 294
    const/4 v5, 0x0

    .line 295
    .local v5, "nReadSize":I
    :try_start_2
    new-array v7, p2, [B

    .line 299
    .local v7, "readBuf":[B
    :cond_0
    sub-int v9, p2, v5

    invoke-virtual {v4, v7, v5, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 300
    .local v6, "r":I
    const/4 v9, -0x1

    if-ne v6, v9, :cond_4

    .line 307
    :goto_0
    if-gtz v5, :cond_5

    .line 308
    const/4 v7, 0x0

    .line 316
    :cond_1
    :goto_1
    iput-object v7, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mContentHeader:[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 322
    .end local v5    # "nReadSize":I
    .end local v6    # "r":I
    .end local v7    # "readBuf":[B
    :cond_2
    if-eqz v1, :cond_3

    .line 323
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 324
    :cond_3
    const/4 v1, 0x0

    .line 326
    .end local v1    # "conn":Ljava/net/HttpURLConnection;
    .end local v4    # "is":Ljava/io/InputStream;
    :goto_2
    return-void

    .line 273
    :catch_0
    move-exception v2

    .line 274
    .local v2, "e":Ljava/net/MalformedURLException;
    const-string v9, "Util.Net.Peeker"

    invoke-virtual {v2}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 302
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .restart local v1    # "conn":Ljava/net/HttpURLConnection;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "nReadSize":I
    .restart local v6    # "r":I
    .restart local v7    # "readBuf":[B
    :cond_4
    add-int/2addr v5, v6

    .line 303
    if-ne v5, p2, :cond_0

    goto :goto_0

    .line 309
    :cond_5
    if-eq v5, p2, :cond_1

    .line 311
    :try_start_3
    new-array v8, v5, [B

    .line 312
    .local v8, "tmp":[B
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v9, v8, v10, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 313
    move-object v7, v8

    goto :goto_1

    .line 318
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v5    # "nReadSize":I
    .end local v6    # "r":I
    .end local v7    # "readBuf":[B
    .end local v8    # "tmp":[B
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v2

    .line 319
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    const-string v9, "Util.Net.Peeker"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 322
    if-eqz v1, :cond_6

    .line 323
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 324
    :cond_6
    const/4 v1, 0x0

    .line 320
    goto :goto_2

    .line 321
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 322
    :goto_4
    if-eqz v1, :cond_7

    .line 323
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 324
    :cond_7
    const/4 v1, 0x0

    .line 325
    throw v9

    .line 321
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "nReadSize":I
    :catchall_1
    move-exception v9

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_4

    .line 318
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_3
.end method


# virtual methods
.method public getContentHeader()[B
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mContentHeader:[B

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Peeker;->mMimeType:Ljava/lang/String;

    return-object v0
.end method
