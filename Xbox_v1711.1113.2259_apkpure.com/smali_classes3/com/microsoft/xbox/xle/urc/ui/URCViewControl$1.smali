.class Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;
.super Ljava/lang/Object;
.source "URCViewControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->performFirstRunAnimation(Landroid/widget/ScrollView;Landroid/animation/TimeInterpolator;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

.field final synthetic val$interpolator:Landroid/animation/TimeInterpolator;

.field final synthetic val$scrollView:Landroid/widget/ScrollView;

.field final synthetic val$timeDelay:J


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;Landroid/widget/ScrollView;Landroid/animation/TimeInterpolator;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$interpolator:Landroid/animation/TimeInterpolator;

    iput-wide p4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$timeDelay:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;)V

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$timeDelay:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    return-void
.end method
