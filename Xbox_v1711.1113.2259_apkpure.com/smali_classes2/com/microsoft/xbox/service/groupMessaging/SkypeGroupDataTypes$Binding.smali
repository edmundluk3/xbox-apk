.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Binding"
.end annotation


# instance fields
.field private final clientDescription:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

.field private final registrationId:Ljava/lang/String;

.field private final transports:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;)V
    .locals 0
    .param p1, "clientDescription"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "registrationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "transports"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 362
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->clientDescription:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    .line 363
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->registrationId:Ljava/lang/String;

    .line 364
    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->transports:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    .line 365
    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 393
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;

    return-object v0
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->clientDescription:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->clientDescription:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;->access$200(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 379
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->transports:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    if-eqz v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->transports:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;->access$300(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 381
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->transports:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;->access$300(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;

    .line 382
    .local v0, "gcmEntry":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;
    if-eqz v0, :cond_0

    .line 383
    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;->access$400(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;)Ljava/lang/String;

    move-result-object v1

    .line 389
    .end local v0    # "gcmEntry":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->registrationId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRegistrationId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 374
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->registrationId:Ljava/lang/String;

    return-object v0
.end method
