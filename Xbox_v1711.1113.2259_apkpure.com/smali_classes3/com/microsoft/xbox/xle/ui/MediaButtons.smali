.class public Lcom/microsoft/xbox/xle/ui/MediaButtons;
.super Landroid/widget/RelativeLayout;
.source "MediaButtons.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IViewUpdate;


# instance fields
.field private buttonContainer:Landroid/widget/LinearLayout;

.field private canNextTrack:Z

.field private canPrevTrack:Z

.field private hidePrevNextTrackButtonsOnPhone:Z

.field private mediaButtonsConnected:Z

.field private nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaNext:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaPrevious:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private showNextTrack:Z

.field private showPrevTrack:Z

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->mediaButtonsConnected:Z

    .line 46
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 47
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030166

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 49
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 50
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->MediaButtons:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    const v3, 0x7f0e078a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->buttonContainer:Landroid/widget/LinearLayout;

    .line 57
    const v3, 0x7f0e078b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPrevious:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 58
    const v3, 0x7f0e078c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 59
    const v3, 0x7f0e078e

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 60
    const v3, 0x7f0e078d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 61
    const v3, 0x7f0e078f

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 62
    const v3, 0x7f0e0790

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaNext:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 64
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setHapticFeedbackEnabled(Z)V

    .line 65
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setHapticFeedbackEnabled(Z)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canPrevTrack:Z

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canNextTrack:Z

    return v0
.end method

.method private setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 0
    .param p1, "button"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "longClickListener"    # Landroid/view/View$OnLongClickListener;

    .prologue
    .line 189
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    invoke-virtual {p1, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 191
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->mediaButtonsConnected:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPrevious:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaNext:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-direct {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->mediaButtonsConnected:Z

    .line 84
    :cond_0
    return-void
.end method

.method public initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;)V
    .locals 0
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    .line 71
    return-void
.end method

.method public updateView(Ljava/lang/Object;)V
    .locals 9
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 88
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->shouldShowMediaProgress()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 89
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->buttonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 91
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getEnabledMediaCommands()Ljava/util/EnumSet;

    move-result-object v2

    .line 92
    .local v2, "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    if-eqz v2, :cond_2

    .line 93
    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PreviousTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canPrevTrack:Z

    .line 94
    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->NextTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canNextTrack:Z

    .line 95
    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Rewind:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 96
    .local v1, "canRewind":Z
    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->FastForward:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 102
    .local v0, "canFastForward":Z
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canPrevTrack:Z

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    if-eqz v3, :cond_0

    if-nez v1, :cond_4

    :cond_0
    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->showPrevTrack:Z

    .line 103
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->canNextTrack:Z

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->hidePrevNextTrackButtonsOnPhone:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_5

    :cond_1
    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->showNextTrack:Z

    .line 105
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v5

    :goto_2
    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 106
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v5

    :goto_3
    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 108
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPrevious:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->showPrevTrack:Z

    if-eqz v3, :cond_8

    move v3, v5

    :goto_4
    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 109
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaNext:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->showNextTrack:Z

    if-eqz v3, :cond_9

    move v3, v5

    :goto_5
    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 111
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_a

    move v3, v5

    :goto_6
    invoke-virtual {v7, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_b

    :goto_7
    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 117
    .end local v0    # "canFastForward":Z
    .end local v1    # "canRewind":Z
    :cond_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->mediaButtonsConnected:Z

    if-nez v3, :cond_3

    .line 118
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPrevious:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$1;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v8}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaNext:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$2;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$2;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v8}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaRewind:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$3;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$3;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    new-instance v6, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v6}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaFastForward:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$5;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$5;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    new-instance v6, Lcom/microsoft/xbox/xle/ui/MediaButtons$6;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$6;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v6}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 166
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPlay:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$7;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$7;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v8}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 173
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->nowPlayingMediaPause:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/MediaButtons$8;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons$8;-><init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V

    invoke-direct {p0, v3, v5, v8}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 180
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->mediaButtonsConnected:Z

    .line 186
    .end local v2    # "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    :cond_3
    :goto_8
    return-void

    .restart local v0    # "canFastForward":Z
    .restart local v1    # "canRewind":Z
    .restart local v2    # "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    :cond_4
    move v3, v5

    .line 102
    goto/16 :goto_0

    :cond_5
    move v3, v5

    .line 103
    goto/16 :goto_1

    :cond_6
    move v3, v6

    .line 105
    goto/16 :goto_2

    :cond_7
    move v3, v6

    .line 106
    goto :goto_3

    :cond_8
    move v3, v6

    .line 108
    goto :goto_4

    :cond_9
    move v3, v6

    .line 109
    goto :goto_5

    :cond_a
    move v3, v6

    .line 111
    goto :goto_6

    :cond_b
    move v5, v6

    .line 112
    goto :goto_7

    .line 183
    .end local v0    # "canFastForward":Z
    .end local v1    # "canRewind":Z
    .end local v2    # "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    :cond_c
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons;->buttonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->cleanup()V

    goto :goto_8
.end method
