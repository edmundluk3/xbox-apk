.class public Lcom/microsoft/xbox/toolkit/rn/XLEReactPackage;
.super Ljava/lang/Object;
.source "XLEReactPackage.java"

# interfaces
.implements Lcom/facebook/react/ReactPackage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createNativeModules(Lcom/facebook/react/bridge/ReactApplicationContext;)Ljava/util/List;
    .locals 3
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/react/bridge/ReactApplicationContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/bridge/NativeModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/react/bridge/NativeModule;

    const/4 v1, 0x0

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/LoggerRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/LoggerRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/RefreshScreenInvokerRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public createViewManagers(Lcom/facebook/react/bridge/ReactApplicationContext;)Ljava/util/List;
    .locals 1
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/react/bridge/ReactApplicationContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
