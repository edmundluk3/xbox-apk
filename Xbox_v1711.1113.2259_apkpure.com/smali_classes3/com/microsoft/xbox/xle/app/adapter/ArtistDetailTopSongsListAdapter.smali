.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ArtistDetailTopSongsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 20
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    .line 21
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 27
    if-nez p2, :cond_2

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 29
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030209

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$1;)V

    .line 32
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    const v3, 0x7f0e0a4f

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v3, 0x7f0e0a50

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 35
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 40
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 41
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    if-eqz v1, :cond_1

    .line 42
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 43
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 46
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getFormattedDuration()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    :cond_1
    return-object p2

    .line 37
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    goto :goto_0
.end method
