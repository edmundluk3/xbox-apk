.class public final enum Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
.super Ljava/lang/Enum;
.source "PrivacyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PermissionReasonTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

.field public static final enum Error:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "error"
    .end annotation
.end field

.field public static final enum IsAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowed"
    .end annotation
.end field

.field public static final enum MuteListRestrictsTarget:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MuteListRestrictsTarget"
    .end annotation
.end field

.field public static final enum NotAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "NotAllowed"
    .end annotation
.end field

.field public static final enum UserBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BlockListRestrictsTarget"
    .end annotation
.end field

.field public static final enum UserPrivacyBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PrivacySettingRestrictsTarget"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "IsAllowed"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->IsAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "NotAllowed"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->NotAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "UserBlocked"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->UserBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "UserPrivacyBlocked"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->UserPrivacyBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "MuteListRestrictsTarget"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->MuteListRestrictsTarget:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    const-string v1, "Error"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->Error:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->IsAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->NotAllowed:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->UserBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->UserPrivacyBlocked:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->MuteListRestrictsTarget:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->Error:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    return-object v0
.end method
