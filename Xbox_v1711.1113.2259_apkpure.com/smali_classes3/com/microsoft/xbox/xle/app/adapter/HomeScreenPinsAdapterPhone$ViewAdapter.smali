.class Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "HomeScreenPinsAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewAdapter"
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 84
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 85
    return-void
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 137
    const/16 v0, 0xf

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->MORE:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->PIN:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$300(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 101
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 116
    move-object v0, p2

    .line 117
    .local v0, "cell":Landroid/view/View;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$3;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenPinsAdapterPhone$ViewType:[I

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 132
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->resizeCellForTwoWayView(Landroid/view/View;)V

    .line 133
    return-object v0

    .line 119
    :pswitch_0
    if-nez v0, :cond_0

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03013d

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 122
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$400(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v3

    invoke-virtual {v2, v0, v3, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;->bindPin(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/PinItem;I)V

    goto :goto_0

    .line 125
    :pswitch_1
    if-nez v0, :cond_1

    .line 126
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03013f

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 128
    :cond_1
    const v2, 0x7f0e06d6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 129
    .local v1, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->access$500(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 106
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
