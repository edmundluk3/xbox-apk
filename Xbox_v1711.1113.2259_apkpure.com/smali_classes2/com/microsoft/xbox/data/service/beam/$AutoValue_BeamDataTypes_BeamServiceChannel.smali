.class abstract Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;
.super Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
.source "$AutoValue_BeamDataTypes_BeamServiceChannel.java"


# instance fields
.field private final audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

.field private final id:Ljava/lang/Integer;

.field private final name:Ljava/lang/String;

.field private final token:Ljava/lang/String;

.field private final viewersCurrent:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "token"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "viewersCurrent"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "audience"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    .line 26
    iput-object p5, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .line 27
    return-void
.end method


# virtual methods
.method public audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    if-eqz v3, :cond_8

    move-object v0, p1

    .line 76
    check-cast v0, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    .line 77
    .local v0, "that":Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->id()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 78
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->name()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 79
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->token()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    if-nez v3, :cond_6

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->viewersCurrent()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    if-nez v3, :cond_7

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 77
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->id()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 78
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 79
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->token()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 80
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->viewersCurrent()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 81
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    :cond_8
    move v1, v2

    .line 83
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 88
    const/4 v0, 0x1

    .line 89
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 91
    mul-int/2addr v0, v3

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 93
    mul-int/2addr v0, v3

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 95
    mul-int/2addr v0, v3

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 97
    mul-int/2addr v0, v3

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    if-nez v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 99
    return v0

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 94
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 96
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    .line 98
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;->hashCode()I

    move-result v2

    goto :goto_4
.end method

.method public id()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BeamServiceChannel{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", viewersCurrent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", audience="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->audience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public token()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->token:Ljava/lang/String;

    return-object v0
.end method

.method public viewersCurrent()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/$AutoValue_BeamDataTypes_BeamServiceChannel;->viewersCurrent:Ljava/lang/Integer;

    return-object v0
.end method
