.class public Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;
.super Landroid/widget/ImageView;
.source "CroppedImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;
    }
.end annotation


# static fields
.field private static final CROP_WAIT_TIMEOUT_MS:I = 0x5dc


# instance fields
.field private destRect:Landroid/graphics/Rect;

.field private onBitmapBindComplete:Ljava/lang/Runnable;

.field private pendingBitmap:Landroid/graphics/Bitmap;

.field private sourceRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->pendingBitmap:Landroid/graphics/Bitmap;

    .line 41
    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->onBitmapBindComplete:Ljava/lang/Runnable;

    .line 46
    sget-object v5, Lcom/microsoft/xboxone/smartglass/R$styleable;->CroppedImageView:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 47
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 48
    .local v3, "startX":I
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 49
    .local v4, "startY":I
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 50
    .local v1, "endX":I
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 51
    .local v2, "endY":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v3, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->sourceRect:Landroid/graphics/Rect;

    .line 54
    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->sourceRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->sourceRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->destRect:Landroid/graphics/Rect;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->destRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->sourceRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->onBitmapCropCompleted(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private bindToBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->pendingBitmap:Landroid/graphics/Bitmap;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->onBitmapBindComplete:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->onBitmapBindComplete:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 93
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    return-void
.end method

.method private hasSize()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBitmapCropCompleted(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "cropped"    # Landroid/graphics/Bitmap;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->bindToBitmap(Landroid/graphics/Bitmap;)V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->pendingBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 69
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 71
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->pendingBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->pendingBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->bindToBitmap(Landroid/graphics/Bitmap;)V

    .line 76
    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 59
    if-nez p1, :cond_0

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;-><init>(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;Landroid/graphics/Bitmap;)V

    .line 63
    .local v0, "cropTask":Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->execute()V

    goto :goto_0
.end method

.method public setOnBitmapBindCompleteRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->onBitmapBindComplete:Ljava/lang/Runnable;

    .line 102
    return-void
.end method
