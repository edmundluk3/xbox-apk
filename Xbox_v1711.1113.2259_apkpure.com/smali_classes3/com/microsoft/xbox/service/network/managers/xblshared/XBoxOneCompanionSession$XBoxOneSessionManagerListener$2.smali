.class Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;
.super Ljava/lang/Object;
.source "XBoxOneCompanionSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

.field final synthetic val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 156
    const/4 v9, 0x0

    .line 162
    .local v9, "timeConsumed":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    const-string v2, "XBoxOneCompanionSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTitleChanged with "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget v5, v5, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-boolean v1, v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    if-eqz v1, :cond_2

    const-string v1, ", focus"

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->access$000(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 172
    sget-object v7, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 173
    .local v7, "oldLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 174
    .local v0, "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget v2, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget v5, v5, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-ne v2, v5, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v2, v5, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-static {v2, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->access$000(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 175
    iget-object v7, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 180
    .end local v0    # "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v7, v1, :cond_4

    .line 182
    const-string v1, "XBoxOneCompanionSession"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "closing %d in %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget v11, v11, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v3

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v2, v5, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/microsoft/xbox/smartglass/SGActiveTitleState;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget v1, v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    sget-object v2, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/SGActiveTitleState;-><init>(IIZLjava/lang/String;Ljava/lang/String;)V

    .line 185
    .local v0, "ats":Lcom/microsoft/xbox/smartglass/SGActiveTitleState;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/SGActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;

    .line 188
    .local v6, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
    invoke-interface {v6, v0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;->onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_1

    .line 162
    .end local v0    # "ats":Lcom/microsoft/xbox/smartglass/SGActiveTitleState;
    .end local v6    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
    .end local v7    # "oldLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_2
    const-string v1, ""

    goto/16 :goto_0

    .line 190
    .restart local v0    # "ats":Lcom/microsoft/xbox/smartglass/SGActiveTitleState;
    .restart local v7    # "oldLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;

    .line 191
    .local v6, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;
    invoke-interface {v6, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;->onTitleChangeCompleted(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_2

    .line 198
    .end local v0    # "ats":Lcom/microsoft/xbox/smartglass/SGActiveTitleState;
    .end local v6    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;
    .end local v7    # "oldLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 199
    .local v8, "oldState":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    invoke-virtual {v1, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;

    .line 202
    .local v6, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    invoke-interface {v6, v8, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;->onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_3

    .line 205
    .end local v6    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;

    .line 206
    .local v6, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;->val$activeTitleState:Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    invoke-interface {v6, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;->onTitleChangeCompleted(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_4

    .line 209
    .end local v6    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;
    :cond_6
    if-eqz v9, :cond_7

    .line 210
    const-string v1, "XBoxOneCompanionSession"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "onTitleChanged completed.  Time used=%dms"

    new-array v5, v12, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v5, v3

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-lez v1, :cond_7

    .line 212
    const-string v1, "XBoxOneCompanionSession"

    const-string v2, "spent too much time in onTitleChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_7
    return-void
.end method
