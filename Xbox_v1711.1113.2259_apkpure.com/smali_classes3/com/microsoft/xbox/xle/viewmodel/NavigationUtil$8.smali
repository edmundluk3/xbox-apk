.class final Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;
.super Ljava/lang/Object;
.source "NavigationUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field final synthetic val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field final synthetic val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field final synthetic val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

.field final synthetic val$onPreClick:Ljava/lang/Runnable;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$onPreClick:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 577
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$onPreClick:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$000(Ljava/lang/Runnable;)V

    .line 578
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NavigationUtil$NavigationScreenshot:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationScreenshot:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 586
    :goto_0
    return-void

    .line 580
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 583
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 578
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
