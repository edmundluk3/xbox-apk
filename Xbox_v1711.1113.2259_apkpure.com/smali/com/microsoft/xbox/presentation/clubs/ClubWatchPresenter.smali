.class public Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "ClubWatchPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/clubs/ClubWatchView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clubId:Ljava/lang/Long;

.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 4
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;
    .param p3, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p4, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p5, "navigationManager"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 56
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 58
    invoke-virtual {p5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    .line 59
    .local v0, "parameters":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    iget-wide v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;->clubId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->clubId:Ljava/lang/Long;

    .line 62
    invoke-static {p0, p2, p3}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 88
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$6(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p3, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 63
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->clubId:Ljava/lang/Long;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .param p1, "refreshIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->clubId:Ljava/lang/Long;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;)V
    .locals 1
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p1, "itemClickedIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;->item()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->navigateToChannel(Lcom/microsoft/xbox/domain/beam/BeamChannel;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p3, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 66
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 67
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 68
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->initialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    .line 70
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 72
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->refreshTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;

    .line 74
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 75
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;

    .line 77
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 78
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;

    .line 79
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 80
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$13;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 81
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;

    .line 83
    invoke-virtual {p3, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 84
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 65
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p3, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-static {p3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 64
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 8
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 95
    instance-of v4, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    if-eqz v4, :cond_2

    move-object v1, p2

    .line 96
    check-cast v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    .line 98
    .local v1, "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    iget-object v4, v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    check-cast v4, Lcom/google/common/collect/ImmutableList;

    invoke-static {v4}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    .line 130
    .end local v1    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    :goto_0
    return-object p1

    .line 100
    .restart local v1    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isFailure()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    iget-object v4, v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->error:Ljava/lang/Throwable;

    invoke-static {v4}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 103
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 105
    .end local v1    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_2
    instance-of v4, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    if-eqz v4, :cond_5

    move-object v3, p2

    .line 106
    check-cast v3, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    .line 108
    .local v3, "refreshResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 109
    iget-object v4, v3, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->content:Ljava/lang/Object;

    check-cast v4, Lcom/google/common/collect/ImmutableList;

    invoke-static {v4}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->isFailure()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 111
    iget-object v4, v3, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->error:Ljava/lang/Throwable;

    invoke-static {v4}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 113
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 115
    .end local v3    # "refreshResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_5
    instance-of v4, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;

    if-eqz v4, :cond_6

    move-object v2, p2

    .line 116
    check-cast v2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;

    .line 118
    .local v2, "mustConfirmResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .line 119
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;->toConfirm()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;->with(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;

    move-result-object v4

    .line 120
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;->toConfirm()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;->with(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 121
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->beamAppDeeplinkEnabled()Z

    move-result v6

    invoke-direct {v0, v4, v5, v6}, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;-><init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Z)V

    .line 123
    .local v0, "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    invoke-static {p1, v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 125
    .end local v0    # "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    .end local v2    # "mustConfirmResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_6
    instance-of v4, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$CancelledIntentResult;

    if-nez v4, :cond_7

    instance-of v4, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ConfirmedIntentResult;

    if-eqz v4, :cond_8

    .line 126
    :cond_7
    invoke-static {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withoutDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 129
    :cond_8
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Couldn\'t explicitly reduce state. previousState: %s. result: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 135
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 136
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 137
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 138
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 139
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 141
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 142
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 144
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 145
    return-void
.end method
