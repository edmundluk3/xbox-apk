.class public Lcom/microsoft/playready/FragmentException;
.super Ljava/lang/Exception;
.source "FragmentException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/FragmentException$FragmentError;
    }
.end annotation


# instance fields
.field private final mFragmentError:Lcom/microsoft/playready/FragmentException$FragmentError;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/playready/FragmentException$FragmentError;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "error"    # Lcom/microsoft/playready/FragmentException$FragmentError;

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/microsoft/playready/FragmentException$FragmentError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 34
    iput-object p2, p0, Lcom/microsoft/playready/FragmentException;->mFragmentError:Lcom/microsoft/playready/FragmentException$FragmentError;

    .line 35
    return-void
.end method


# virtual methods
.method public getFragmentError()Lcom/microsoft/playready/FragmentException$FragmentError;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/playready/FragmentException;->mFragmentError:Lcom/microsoft/playready/FragmentException$FragmentError;

    return-object v0
.end method
