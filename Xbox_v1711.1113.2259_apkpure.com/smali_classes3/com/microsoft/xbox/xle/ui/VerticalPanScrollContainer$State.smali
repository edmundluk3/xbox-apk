.class final enum Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
.super Ljava/lang/Enum;
.source "VerticalPanScrollContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

.field public static final enum PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

.field public static final enum SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

.field public static final enum STILL:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 383
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    const-string v1, "STILL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->STILL:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    const-string v1, "PANNING"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    const-string v1, "SCROLLING"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    .line 382
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->STILL:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 382
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 382
    const-class v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    return-object v0
.end method
