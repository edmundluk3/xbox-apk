.class public Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ProgramItemDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;


# instance fields
.field private final animationName:Ljava/lang/String;

.field private item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p3, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p4, "model"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    .prologue
    .line 35
    const v4, 0x7f0802d3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 32
    const-string v4, "Screen"

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->animationName:Ljava/lang/String;

    .line 36
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->_init()V

    .line 38
    const v4, 0x7f0e0b2f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 40
    .local v12, "lr":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setupConnectButton(Landroid/widget/LinearLayout;)V

    .line 41
    const v4, 0x7f0e0b30

    invoke-virtual {v12, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 42
    .local v16, "v":Landroid/view/View;
    if-eqz v16, :cond_0

    .line 43
    const/16 v4, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 46
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f070473

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 49
    .local v10, "channelFormattedString":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v17, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelNumber()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v4, v17

    invoke-static {v10, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 51
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v4, v10, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setChannelInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const/4 v5, 0x0

    .line 58
    .local v5, "imageUri":Ljava/lang/String;
    const/4 v11, 0x0

    .line 60
    .local v11, "hideInfoButton":Z
    if-eqz p2, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getIsAdult()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanViewAdultTVContent()Z

    move-result v4

    if-nez v4, :cond_5

    .line 61
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f070481

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "title":Ljava/lang/String;
    const-string v7, ""

    .line 63
    .local v7, "subtitle":Ljava/lang/String;
    const-string v8, ""

    .line 64
    .local v8, "info":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f070480

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 65
    .local v9, "description":Ljava/lang/String;
    const/4 v11, 0x1

    :cond_2
    :goto_0
    move-object/from16 v4, p0

    .line 92
    invoke-direct/range {v4 .. v9}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setProgramInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const v4, 0x7f0e0589

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 95
    .local v15, "mTuneButton":Landroid/view/View;
    if-eqz v15, :cond_3

    .line 96
    new-instance v4, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v4, v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V

    invoke-virtual {v15, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_3
    const v4, 0x7f0e0b3b

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 113
    .local v13, "mInfoButton":Landroid/view/View;
    if-eqz v13, :cond_4

    .line 114
    if-eqz v11, :cond_9

    .line 115
    const/16 v4, 0x8

    invoke-virtual {v13, v4}, Landroid/view/View;->setVisibility(I)V

    .line 131
    :cond_4
    :goto_1
    const v4, 0x7f0e058a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 132
    .local v14, "mStreamButton":Landroid/view/View;
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->shouldEnableStreaming(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 133
    const/16 v4, 0x8

    invoke-virtual {v14, v4}, Landroid/view/View;->setVisibility(I)V

    .line 152
    :goto_2
    return-void

    .line 66
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subtitle":Ljava/lang/String;
    .end local v8    # "info":Ljava/lang/String;
    .end local v9    # "description":Ljava/lang/String;
    .end local v13    # "mInfoButton":Landroid/view/View;
    .end local v14    # "mStreamButton":Landroid/view/View;
    .end local v15    # "mTuneButton":Landroid/view/View;
    :cond_5
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 68
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f070459

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 69
    .restart local v6    # "title":Ljava/lang/String;
    const-string v7, ""

    .line 70
    .restart local v7    # "subtitle":Ljava/lang/String;
    const-string v8, ""

    .line 71
    .restart local v8    # "info":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f07045f

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 72
    .restart local v9    # "description":Ljava/lang/String;
    const/4 v11, 0x1

    goto :goto_0

    .line 74
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subtitle":Ljava/lang/String;
    .end local v8    # "info":Ljava/lang/String;
    .end local v9    # "description":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 75
    :cond_7
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v6

    .line 76
    .restart local v6    # "title":Ljava/lang/String;
    const-string v7, ""

    .line 82
    .restart local v7    # "subtitle":Ljava/lang/String;
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getIsHD()Z

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-static {v0, v4, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getTVInformationString(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 83
    .restart local v8    # "info":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowDescription()Ljava/lang/String;

    move-result-object v9

    .line 84
    .restart local v9    # "description":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v5

    .line 87
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v4

    const-string v17, "ba5eba11-dea1-4bad-ba11-feddeadfab1e"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 88
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 78
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subtitle":Ljava/lang/String;
    .end local v8    # "info":Ljava/lang/String;
    .end local v9    # "description":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v6

    .line 79
    .restart local v6    # "title":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "subtitle":Ljava/lang/String;
    goto :goto_3

    .line 117
    .restart local v8    # "info":Ljava/lang/String;
    .restart local v9    # "description":Ljava/lang/String;
    .restart local v13    # "mInfoButton":Landroid/view/View;
    .restart local v15    # "mTuneButton":Landroid/view/View;
    :cond_9
    new-instance v4, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    invoke-virtual {v13, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 135
    .restart local v14    # "mStreamButton":Landroid/view/View;
    :cond_a
    new-instance v4, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v4, v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V

    invoke-virtual {v14, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "channel"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p3, "item"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    const/16 v10, 0x8

    .line 155
    const v0, 0x7f0802d3

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 32
    const-string v0, "Screen"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->animationName:Ljava/lang/String;

    .line 156
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->_init()V

    .line 158
    const v0, 0x7f0e0b2f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 160
    .local v6, "lr":Landroid/widget/LinearLayout;
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setupConnectButton(Landroid/widget/LinearLayout;)V

    .line 161
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setupRefreshButton(Landroid/widget/LinearLayout;)V

    .line 163
    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .line 165
    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p3, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 167
    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->loadExtra()V

    .line 170
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setChannelInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setProgramInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const v0, 0x7f0e0589

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 174
    .local v9, "mTuneButton":Landroid/view/View;
    if-eqz v9, :cond_1

    .line 175
    const/4 v0, 0x4

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 178
    :cond_1
    const v0, 0x7f0e0b3b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 179
    .local v7, "mInfoButton":Landroid/view/View;
    if-eqz v7, :cond_2

    .line 180
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 183
    :cond_2
    const v0, 0x7f0e058a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 184
    .local v8, "mStreamButton":Landroid/view/View;
    if-eqz v8, :cond_3

    .line 185
    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 188
    :cond_3
    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->updateChannelItemExtra()V

    .line 191
    :cond_4
    return-void
.end method

.method private _init()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 207
    const v1, 0x7f030247

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setContentView(I)V

    .line 208
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setCanceledOnTouchOutside(Z)V

    .line 210
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 212
    const v1, 0x7f0e0aff

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->dialogBody:Landroid/view/View;

    .line 213
    const-string v1, "Screen"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setBodyAnimationName(Ljava/lang/String;)V

    .line 215
    const v1, 0x7f0e0b00

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 216
    .local v0, "root":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 217
    new-instance v1, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$4;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$4;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->onClosePressed()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->onRefreshPressed()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->onConnectPressed()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    return-object v0
.end method

.method private onClosePressed()V
    .locals 0

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->safeDismiss()V

    .line 394
    return-void
.end method

.method private onConnectPressed()V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method private onRefreshPressed()V
    .locals 0

    .prologue
    .line 399
    return-void
.end method

.method private setChannelInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "channelNumber"    # Ljava/lang/String;
    .param p3, "channelName"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 264
    const v3, 0x7f0e056b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 265
    .local v2, "mLogoView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    if-eqz v2, :cond_0

    .line 266
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 267
    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v2, p1, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 268
    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 274
    :cond_0
    :goto_0
    const v3, 0x7f0e0b32

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 275
    .local v0, "mChannelName":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v0, :cond_2

    .line 277
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 278
    :cond_1
    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 286
    :cond_2
    :goto_1
    const v3, 0x7f0e056c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 287
    .local v1, "mChannelNumber":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v1, :cond_4

    .line 288
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 289
    :cond_3
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 296
    :cond_4
    :goto_2
    return-void

    .line 270
    .end local v0    # "mChannelName":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v1    # "mChannelNumber":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_5
    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    goto :goto_0

    .line 280
    .restart local v0    # "mChannelName":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_6
    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_1

    .line 291
    .restart local v1    # "mChannelNumber":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_7
    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private setProgramInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "info"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f0e0b38

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 299
    const v4, 0x7f0e0b36

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 300
    .local v2, "mThumbnailView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v4, 0x7f0e0b35

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 301
    .local v1, "mLoadingRingsView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v4, 0x7f0e0b34

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, "mImageUnavailableView":Landroid/view/View;
    if-eqz v2, :cond_6

    .line 303
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 304
    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    const v5, 0x7f0201f7

    invoke-virtual {v2, p1, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 305
    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 306
    if-eqz v1, :cond_0

    .line 307
    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 309
    :cond_0
    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 330
    :cond_1
    :goto_0
    const v4, 0x7f0e0b37

    invoke-direct {p0, v4, p2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setTextIfNonNull(ILjava/lang/String;)V

    .line 331
    if-eqz p3, :cond_8

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 332
    invoke-direct {p0, v8, p3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setTextIfNonNull(ILjava/lang/String;)V

    .line 340
    :cond_2
    :goto_1
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 341
    const v4, 0x7f0e0b39

    invoke-direct {p0, v4, p4}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setTextIfNonNull(ILjava/lang/String;)V

    .line 343
    :cond_3
    const v4, 0x7f0e0b3a

    invoke-direct {p0, v4, p5}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setTextIfNonNull(ILjava/lang/String;)V

    .line 344
    return-void

    .line 313
    :cond_4
    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 314
    if-eqz v1, :cond_5

    .line 315
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 317
    :cond_5
    if-eqz v0, :cond_1

    .line 318
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 322
    :cond_6
    if-eqz v1, :cond_7

    .line 323
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 325
    :cond_7
    if-eqz v0, :cond_1

    .line 326
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 334
    :cond_8
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 335
    .local v3, "subtitleview":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 336
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 337
    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    goto :goto_1
.end method

.method private setTextIfNonNull(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 382
    .local v0, "mTitleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    :cond_0
    return-void
.end method

.method private setupConnectButton(Landroid/widget/LinearLayout;)V
    .locals 2
    .param p1, "lr"    # Landroid/widget/LinearLayout;

    .prologue
    .line 247
    const v1, 0x7f0e022b

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 248
    .local v0, "connectButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    if-eqz v0, :cond_0

    .line 249
    new-instance v1, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$6;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$6;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    :cond_0
    return-void
.end method

.method private setupRefreshButton(Landroid/widget/LinearLayout;)V
    .locals 2
    .param p1, "lr"    # Landroid/widget/LinearLayout;

    .prologue
    .line 232
    const v1, 0x7f0e0b31

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 233
    .local v0, "refreshButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    new-instance v1, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$5;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$5;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    return-void
.end method

.method public static show(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p2, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p3, "model"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    .prologue
    .line 194
    new-instance v0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    .line 195
    .local v0, "dlg":Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "ShowExpanded"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v5

    if-eqz p2, :cond_0

    .line 196
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v1

    .line 195
    :goto_0
    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 198
    return-void

    .line 196
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p2, "item"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    .line 201
    new-instance v0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)V

    .line 202
    .local v0, "dlg":Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "ShowExpanded"

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v5

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideAppPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 204
    return-void

    .line 202
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method private updateChannelItemExtra()V
    .locals 7

    .prologue
    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getImageURL()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "imageUrl":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getAppChannelInformationString(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Ljava/lang/String;

    move-result-object v4

    .line 355
    .local v4, "info":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getEpisodeName()Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "subtitle":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->item:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getDescription()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->setProgramInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const v0, 0x7f0e0589

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 360
    .local v6, "mTuneButton":Landroid/view/View;
    if-eqz v6, :cond_0

    .line 361
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 362
    new-instance v0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;-><init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->onClosePressed()V

    .line 390
    return-void
.end method

.method public onPropertyChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 408
    const-string v0, "extra"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->updateChannelItemExtra()V

    .line 411
    :cond_0
    return-void
.end method
