.class public Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
.super Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;
.source "CommonActionsAndResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/common/CommonActionsAndResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InitialLoadResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult",
        "<TT;>;"
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "inFlight"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Throwable;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<TT;>;"
    .local p1, "content":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V

    .line 241
    return-void
.end method

.method public static inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 254
    new-instance v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V

    return-object v0
.end method

.method public static withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 244
    .local p0, "content":Ljava/lang/Object;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 245
    new-instance v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .locals 3
    .param p0, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 249
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 250
    new-instance v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V

    return-object v0
.end method
