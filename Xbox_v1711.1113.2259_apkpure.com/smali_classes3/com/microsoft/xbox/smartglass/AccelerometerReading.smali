.class public Lcom/microsoft/xbox/smartglass/AccelerometerReading;
.super Ljava/lang/Object;
.source "AccelerometerReading.java"


# instance fields
.field public timeStamp:J

.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>(DDDJ)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "timeStamp"    # J

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-wide p1, p0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->x:D

    .line 27
    iput-wide p3, p0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->y:D

    .line 28
    iput-wide p5, p0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->z:D

    .line 29
    iput-wide p7, p0, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->timeStamp:J

    .line 30
    return-void
.end method
