.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
.super Landroid/widget/LinearLayout;
.source "VirtualGridBaseRow.java"


# static fields
.field protected static BOTTOM_FRAME_ANIMATION_DURATION_MS:I


# instance fields
.field protected mBottomFrame:Landroid/view/ViewGroup;

.field protected mBottomFrameAnimation:Landroid/animation/AnimatorSet;

.field protected mBottomView:Landroid/view/View;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field protected mEasingExponent:F

.field protected mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

.field protected mRightLayout:Landroid/widget/RelativeLayout;

.field protected mRow:I

.field protected mTitleView:Landroid/view/View;

.field protected mTopFrame:Landroid/view/ViewGroup;

.field protected mTopView:Landroid/view/View;

.field protected mXTweenPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x29b

    sput v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 61
    const/high16 v0, 0x40e00000    # 7.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mEasingExponent:F

    .line 62
    const/16 v0, 0xc8

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mXTweenPosition:I

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$1;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mClickListener:Landroid/view/View$OnClickListener;

    .line 97
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V
    .locals 6
    .param p1, "grid"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 60
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 61
    const/high16 v0, 0x40e00000    # 7.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mEasingExponent:F

    .line 62
    const/16 v0, 0xc8

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mXTweenPosition:I

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$1;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mClickListener:Landroid/view/View$OnClickListener;

    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .line 81
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setOrientation(I)V

    .line 82
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030272

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRightLayout:Landroid/widget/RelativeLayout;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRightLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRightLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0bc6

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopFrame:Landroid/view/ViewGroup;

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRightLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0bc7

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRightLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->addView(Landroid/view/View;)V

    .line 93
    return-void
.end method

.method private stopAnimateBottomFrame()V
    .locals 2

    .prologue
    .line 339
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    if-nez v1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 343
    .local v0, "animation":Landroid/animation/AnimatorSet;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 344
    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0
.end method


# virtual methods
.method public bind(I)V
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRow:I

    .line 162
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->stopAnimateBottomFrame()V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRow:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->getTitleView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getTitleView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setTitleView(Landroid/view/View;)V

    .line 171
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->updateBottomView(Z)V

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRow:I

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getHeightSpec(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setTopViewHeight(I)V

    .line 175
    return-void
.end method

.method public getTitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    return-object v0
.end method

.method protected minimumLayoutParamDimen(I)F
    .locals 2
    .param p1, "spec"    # I

    .prologue
    .line 355
    if-lez p1, :cond_0

    int-to-double v0, p1

    :goto_0
    double-to-float v0, v0

    return v0

    :cond_0
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    goto :goto_0
.end method

.method public refresh(Z)V
    .locals 0
    .param p1, "clear"    # Z

    .prologue
    .line 126
    return-void
.end method

.method public setBottomView(Landroid/view/View;Z)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "animate"    # Z

    .prologue
    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    if-ne v1, p1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 193
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->stopAnimateBottomFrame()V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    .line 198
    .local v0, "removeView":Landroid/view/View;
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    .line 199
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 203
    :cond_3
    if-eqz p2, :cond_4

    .line 204
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->startAnimateBottomFrame(Landroid/view/View;)Z

    .line 208
    :cond_4
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    if-nez v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setTitleView(Landroid/view/View;)V
    .locals 2
    .param p1, "titleView"    # Landroid/view/View;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    if-ne v0, p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->removeView(Landroid/view/View;)V

    .line 140
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$2;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 153
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTitleView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method protected setTopView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 106
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 113
    :cond_1
    return-void
.end method

.method public setTopViewHeight(I)V
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 118
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, p1, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mTopView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method

.method public setXTweenPosition(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mXTweenPosition:I

    .line 66
    return-void
.end method

.method protected startAnimateBottomFrame(Landroid/view/View;)Z
    .locals 18
    .param p1, "removeView"    # Landroid/view/View;

    .prologue
    .line 218
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 221
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getHeight()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->minimumLayoutParamDimen(I)F

    move-result v12

    .line 224
    .local v12, "startHeight":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    if-eqz v14, :cond_0

    .line 225
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/ViewGroup;->measure(II)V

    .line 227
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->minimumLayoutParamDimen(I)F

    move-result v9

    .line 231
    .local v9, "finishHeight":F
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 232
    .local v6, "animatorSet":Landroid/animation/AnimatorSet;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v7, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    cmpl-float v14, v12, v9

    if-eqz v14, :cond_1

    .line 236
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    .line 237
    .local v4, "animatorOfHeight":Landroid/animation/ValueAnimator;
    sget v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v14, v14

    invoke-virtual {v4, v14, v15}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4, v14}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 239
    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    float-to-int v0, v12

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x1

    float-to-int v0, v9

    move/from16 v16, v0

    aput v16, v14, v15

    invoke-virtual {v4, v14}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 240
    new-instance v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    invoke-virtual {v4, v14}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 248
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    .end local v4    # "animatorOfHeight":Landroid/animation/ValueAnimator;
    :cond_1
    float-to-double v14, v12

    const-wide v16, 0x3f947ae147ae147bL    # 0.02

    cmpg-double v14, v14, v16

    if-gez v14, :cond_5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mXTweenPosition:I

    int-to-float v13, v14

    .line 254
    .local v13, "startX":F
    :goto_1
    const/4 v10, 0x0

    .line 255
    .local v10, "finishX":F
    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-eqz v14, :cond_2

    .line 257
    new-instance v5, Landroid/animation/ValueAnimator;

    invoke-direct {v5}, Landroid/animation/ValueAnimator;-><init>()V

    .line 258
    .local v5, "animatorOfX":Landroid/animation/ValueAnimator;
    sget v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v14, v14

    invoke-virtual {v5, v14, v15}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 259
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5, v14}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 260
    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v13, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x0

    aput v16, v14, v15

    invoke-virtual {v5, v14}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 261
    new-instance v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$4;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$4;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    invoke-virtual {v5, v14}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 268
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    .end local v5    # "animatorOfX":Landroid/animation/ValueAnimator;
    :cond_2
    float-to-double v14, v9

    const-wide v16, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v14, v14, v16

    if-lez v14, :cond_6

    const/4 v11, 0x0

    .line 276
    .local v11, "startAlpha":F
    :goto_2
    float-to-double v14, v9

    const-wide v16, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v14, v14, v16

    if-lez v14, :cond_7

    const/high16 v8, 0x3f800000    # 1.0f

    .line 278
    .local v8, "finishAlpha":F
    :goto_3
    const v14, 0x3c23d70a    # 0.01f

    cmpg-float v14, v11, v14

    if-gtz v14, :cond_8

    .line 279
    sget v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-float v14, v14

    const v15, 0x3dcccccd    # 0.1f

    mul-float v2, v14, v15

    .line 283
    .local v2, "alphaStartDelay":F
    :goto_4
    new-instance v3, Landroid/animation/ValueAnimator;

    invoke-direct {v3}, Landroid/animation/ValueAnimator;-><init>()V

    .line 284
    .local v3, "animatorOfAlpha":Landroid/animation/ValueAnimator;
    sget v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->BOTTOM_FRAME_ANIMATION_DURATION_MS:I

    int-to-long v14, v14

    float-to-long v0, v2

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    invoke-virtual {v3, v14, v15}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 285
    float-to-long v14, v2

    invoke-virtual {v3, v14, v15}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 286
    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v11, v14, v15

    const/4 v15, 0x1

    aput v8, v14, v15

    invoke-virtual {v3, v14}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 287
    new-instance v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$5;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$5;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V

    invoke-virtual {v3, v14}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 294
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_3

    .line 298
    new-instance v14, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v8, v9}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;-><init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;FF)V

    invoke-virtual {v6, v14}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 321
    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 322
    new-instance v14, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mEasingExponent:F

    sget-object v16, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseOut:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    invoke-direct/range {v14 .. v16}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    invoke-virtual {v6, v14}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 323
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 325
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrameAnimation:Landroid/animation/AnimatorSet;

    .line 326
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    float-to-int v15, v12

    iput v15, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 327
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->requestLayout()V

    .line 329
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 333
    :cond_3
    const/4 v14, 0x1

    return v14

    .line 227
    .end local v2    # "alphaStartDelay":F
    .end local v3    # "animatorOfAlpha":Landroid/animation/ValueAnimator;
    .end local v6    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v7    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .end local v8    # "finishAlpha":F
    .end local v9    # "finishHeight":F
    .end local v10    # "finishX":F
    .end local v11    # "startAlpha":F
    .end local v13    # "startX":F
    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 253
    .restart local v6    # "animatorSet":Landroid/animation/AnimatorSet;
    .restart local v7    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .restart local v9    # "finishHeight":F
    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 275
    .restart local v10    # "finishX":F
    .restart local v13    # "startX":F
    :cond_6
    const/high16 v11, 0x3f800000    # 1.0f

    goto/16 :goto_2

    .line 276
    .restart local v11    # "startAlpha":F
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 281
    .restart local v8    # "finishAlpha":F
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "alphaStartDelay":F
    goto :goto_4
.end method

.method public updateBottomView(Z)V
    .locals 3
    .param p1, "animate"    # Z

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mGrid:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mAdapter:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mRow:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;->getBottomView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setBottomView(Landroid/view/View;Z)V

    .line 180
    return-void
.end method
