.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;
.super Ljava/lang/Object;
.source "HomeScreenAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$BaseCellListener;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p0, "cell"    # Landroid/view/View;
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 92
    const v0, 0x7f0e06d7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 93
    return-void
.end method

.method public static bindCellTitle(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p0, "title"    # Landroid/widget/TextView;
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 96
    if-eqz p2, :cond_0

    .line 97
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 103
    :goto_0
    return-void

    .line 98
    :cond_0
    if-eqz p1, :cond_1

    .line 99
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 101
    :cond_1
    const-string v0, ""

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public static setImageUrl(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Ljava/lang/String;)V
    .locals 2
    .param p0, "img"    # Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .param p1, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "imgUrl"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 85
    if-eqz p0, :cond_0

    .line 86
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 87
    .local v0, "defaultRid":I
    :goto_0
    invoke-virtual {p0, p2, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 89
    .end local v0    # "defaultRid":I
    :cond_0
    return-void

    .line 86
    :cond_1
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0
.end method

.method public static setUserBackground(Landroid/view/View;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 71
    if-eqz p0, :cond_0

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 73
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_1

    .line 74
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 80
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    :goto_0
    return-void

    .line 77
    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public static setUserBackgroundOptional(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/view/View;)V
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 44
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz p1, :cond_0

    .line 45
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getShouldShowBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getBoxArtBackgroundColor()I

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getBoxArtBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 53
    :cond_2
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getBoxArtBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public static setUserBackgroundOptional(Ljava/lang/String;Landroid/view/View;)V
    .locals 2
    .param p0, "contentType"    # Ljava/lang/String;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 61
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->getShouldShowBackgroundColor(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 68
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    :goto_0
    return-void

    .line 65
    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method
