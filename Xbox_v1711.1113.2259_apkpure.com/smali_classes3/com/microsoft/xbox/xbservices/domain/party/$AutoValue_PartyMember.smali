.class abstract Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
.source "$AutoValue_PartyMember.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;
    }
.end annotation


# instance fields
.field private final isHost:Z

.field private final isSelfMuted:Ljava/lang/Boolean;

.field private final isTalking:Z

.field private final multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

.field private final privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

.field private final rosterKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;ZZLcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "rosterKey"    # Ljava/lang/String;
    .param p2, "isHost"    # Z
    .param p3, "isTalking"    # Z
    .param p4, "multiplayerMember"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .param p5, "privacyPermissions"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "isSelfMuted"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;-><init>()V

    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null rosterKey"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->rosterKey:Ljava/lang/String;

    .line 31
    iput-boolean p2, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isHost:Z

    .line 32
    iput-boolean p3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isTalking:Z

    .line 33
    if-nez p4, :cond_1

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null multiplayerMember"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 37
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    .line 38
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 93
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 94
    .local v0, "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->rosterKey:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->rosterKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isHost:Z

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isTalking:Z

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isTalking()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    if-nez v3, :cond_3

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    if-nez v3, :cond_4

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 98
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 99
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_5
    move v1, v2

    .line 101
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v4, 0x0

    const v5, 0xf4243

    .line 106
    const/4 v0, 0x1

    .line 107
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->rosterKey:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v5

    .line 110
    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isHost:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v5

    .line 112
    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isTalking:Z

    if-eqz v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 113
    mul-int/2addr v0, v5

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 115
    mul-int/2addr v0, v5

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    if-nez v1, :cond_2

    move v1, v4

    :goto_2
    xor-int/2addr v0, v1

    .line 117
    mul-int/2addr v0, v5

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v4

    .line 119
    return v0

    :cond_0
    move v1, v3

    .line 110
    goto :goto_0

    :cond_1
    move v2, v3

    .line 112
    goto :goto_1

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v4

    goto :goto_3
.end method

.method public isHost()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isHost:Z

    return v0
.end method

.method public isSelfMuted()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isTalking()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isTalking:Z

    return v0
.end method

.method public multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    return-object v0
.end method

.method public privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    return-object v0
.end method

.method public rosterKey()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->rosterKey:Ljava/lang/String;

    return-object v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyMember{rosterKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->rosterKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isHost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isHost:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isTalking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isTalking:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", multiplayerMember="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->multiplayerMember:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", privacyPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->privacyPermissions:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSelfMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->isSelfMuted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
