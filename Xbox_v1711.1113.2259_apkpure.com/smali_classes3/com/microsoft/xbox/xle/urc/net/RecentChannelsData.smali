.class public Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
.super Ljava/lang/Object;
.source "RecentChannelsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
    }
.end annotation


# instance fields
.field private channelInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private exception:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->exception:Ljava/lang/Throwable;

    return-void
.end method

.method public static instantiateWithException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    .locals 1
    .param p0, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;-><init>()V

    .line 71
    .local v0, "result":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    iput-object p0, v0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->exception:Ljava/lang/Throwable;

    .line 72
    return-object v0
.end method

.method public static parseJSON(Lorg/json/JSONArray;)Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    .locals 8
    .param p0, "channelInfo"    # Lorg/json/JSONArray;

    .prologue
    .line 45
    new-instance v6, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    invoke-direct {v6}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;-><init>()V

    .line 49
    .local v6, "result":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    :try_start_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 50
    .local v1, "count":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;>;"
    const/4 v7, 0x0

    .line 52
    .local v7, "tmp":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 53
    invoke-virtual {p0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 54
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;

    invoke-direct {v0, v7}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;-><init>(Lorg/json/JSONObject;)V

    .line 55
    .local v0, "cInfo":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 58
    .end local v0    # "cInfo":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
    :cond_0
    iput-object v5, v6, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->channelInfoList:Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 66
    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;>;"
    .end local v7    # "tmp":Lorg/json/JSONObject;
    :goto_1
    return-object v6

    .line 60
    :catch_0
    move-exception v2

    .line 61
    .local v2, "ex":Lorg/json/JSONException;
    iput-object v2, v6, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->exception:Ljava/lang/Throwable;

    goto :goto_1

    .line 62
    .end local v2    # "ex":Lorg/json/JSONException;
    :catch_1
    move-exception v4

    .line 63
    .local v4, "iex":Ljava/lang/IndexOutOfBoundsException;
    iput-object v4, v6, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->exception:Ljava/lang/Throwable;

    goto :goto_1
.end method


# virtual methods
.method public getChannelCount()I
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->channelInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->channelInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChannelInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->channelInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->exception:Ljava/lang/Throwable;

    return-object v0
.end method
