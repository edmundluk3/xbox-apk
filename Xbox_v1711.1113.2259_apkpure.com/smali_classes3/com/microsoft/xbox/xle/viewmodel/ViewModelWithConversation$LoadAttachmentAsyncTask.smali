.class Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ViewModelWithConversation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAttachmentAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V
    .locals 0
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 443
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .line 444
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 448
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shouldReloadEntity(Lcom/microsoft/xbox/service/model/entity/EntityModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadEntity(Lcom/microsoft/xbox/service/model/entity/EntityModel;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 473
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 454
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 455
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V

    .line 456
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V

    .line 469
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 439
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 460
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->message:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->updateAdapter()V

    .line 464
    return-void
.end method
