.class Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetLeaderBoardRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;",
        ">;"
    }
.end annotation


# instance fields
.field private stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 0
    .param p2, "stat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 1330
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1331
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 1332
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1336
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1338
    .local v16, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 1339
    .local v9, "result":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->SortOrder:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->SortOrder:Ljava/lang/String;

    .line 1343
    .local v6, "sortOrder":Ljava/lang/String;
    :goto_0
    const/4 v8, 0x0

    .line 1344
    .local v8, "meInFirstBatchResult":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->scid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1345
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->scid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getTitleLeaderBoardInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    move-result-object v9

    .line 1346
    if-eqz v9, :cond_6

    iget-object v1, v9, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 1348
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    iput-object v1, v9, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->statName:Ljava/lang/String;

    .line 1350
    new-instance v15, Ljava/util/Hashtable;

    invoke-direct {v15}, Ljava/util/Hashtable;-><init>()V

    .line 1351
    .local v15, "xuidToUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;>;"
    iget-object v1, v9, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 1352
    .local v13, "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    iget-object v2, v13, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1353
    iget-object v2, v13, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-virtual {v15, v2, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    iget-object v3, v13, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 1355
    const/4 v8, 0x1

    goto :goto_1

    .line 1339
    .end local v6    # "sortOrder":Ljava/lang/String;
    .end local v8    # "meInFirstBatchResult":Z
    .end local v13    # "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .end local v15    # "xuidToUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;>;"
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/StatisticsSortOrder;->descending:Lcom/microsoft/xbox/service/model/StatisticsSortOrder;

    .line 1341
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StatisticsSortOrder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1360
    .restart local v6    # "sortOrder":Ljava/lang/String;
    .restart local v8    # "meInFirstBatchResult":Z
    .restart local v15    # "xuidToUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;>;"
    :cond_2
    if-nez v8, :cond_3

    .line 1362
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->scid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface/range {v1 .. v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getTitleLeaderBoardInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    move-result-object v10

    .line 1363
    .local v10, "resultForMe":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    if-eqz v10, :cond_3

    iget-object v1, v10, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, v10, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 1365
    iget-object v1, v10, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 1366
    .local v7, "me":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->setUserProfilePicUri(Ljava/lang/String;)V

    .line 1367
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->setGamerRealName(Ljava/lang/String;)V

    .line 1368
    invoke-virtual {v9, v7}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->setMe(Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V

    .line 1374
    .end local v7    # "me":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .end local v10    # "resultForMe":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    :cond_3
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 1375
    new-instance v14, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1376
    .local v14, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    const/4 v11, 0x0

    .line 1377
    .local v11, "tempResult":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    iget-object v1, v14, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    iget-object v1, v14, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 1378
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    invoke-static {v14}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v11

    .line 1380
    if-eqz v11, :cond_6

    iget-object v1, v11, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    iget-object v1, v11, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 1381
    iget-object v1, v11, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 1382
    .local v13, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    if-eqz v13, :cond_4

    iget-object v1, v13, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 1383
    iget-object v1, v13, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 1384
    .local v12, "u":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    if-eqz v12, :cond_4

    .line 1385
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-static {v13, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->setUserProfilePicUri(Ljava/lang/String;)V

    .line 1386
    iget-object v1, v13, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v12, v1}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->setGamerRealName(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-static {v13, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 1397
    .end local v11    # "tempResult":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v12    # "u":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .end local v13    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .end local v14    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    .end local v15    # "xuidToUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;>;"
    :cond_6
    return-object v9
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1326
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1411
    const-wide/16 v0, 0x2134

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1406
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$1000(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1407
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1402
    return-void
.end method
