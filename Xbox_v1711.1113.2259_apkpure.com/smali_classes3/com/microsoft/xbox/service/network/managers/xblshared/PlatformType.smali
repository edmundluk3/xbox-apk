.class public final enum Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;
.super Ljava/lang/Enum;
.source "PlatformType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

.field public static final enum PlatformType_AndroidPhone:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

.field public static final enum PlatformType_AndroidSlate:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    const-string v1, "PlatformType_AndroidPhone"

    invoke-direct {v0, v1, v3, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidPhone:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    const-string v1, "PlatformType_AndroidSlate"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidSlate:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    .line 3
    new-array v0, v5, [Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidPhone:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidSlate:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->value:I

    .line 11
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 18
    packed-switch p0, :pswitch_data_0

    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidPhone:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    :goto_0
    return-object v0

    .line 20
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidPhone:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    goto :goto_0

    .line 22
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->PlatformType_AndroidSlate:Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/PlatformType;->value:I

    return v0
.end method
