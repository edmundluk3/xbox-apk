.class public Lcom/microsoft/xbox/toolkit/ui/SwitchPanelItemRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "SwitchPanelItemRelativeLayout.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/SwitchPanel$SwitchPanelChild;


# instance fields
.field private final INVALID_STATE_ID:I

.field private state:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, -0x1

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanelItemRelativeLayout;->INVALID_STATE_ID:I

    .line 29
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->SwitchPanelItem:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 30
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanelItemRelativeLayout;->state:I

    .line 31
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 33
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanelItemRelativeLayout;->state:I

    if-gez v1, :cond_0

    .line 34
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "You must specify the state attribute in the xml, and the value must be positive."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanelItemRelativeLayout;->state:I

    return v0
.end method
