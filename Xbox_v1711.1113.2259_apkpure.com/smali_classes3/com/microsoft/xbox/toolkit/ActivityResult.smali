.class public abstract Lcom/microsoft/xbox/toolkit/ActivityResult;
.super Ljava/lang/Object;
.source "ActivityResult.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(IILandroid/content/Intent;)Lcom/microsoft/xbox/toolkit/ActivityResult;
    .locals 1
    .param p0, "requestCode"    # I
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    new-instance v0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;-><init>(IILandroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public abstract data()Landroid/content/Intent;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract requestCode()I
.end method

.method public abstract resultCode()I
.end method
