.class Lcom/microsoft/playready/PRActivator;
.super Ljava/lang/Object;
.source "PRActivator.java"

# interfaces
.implements Lcom/microsoft/playready/IPRActivator;


# instance fields
.field private final mExecService:Ljava/util/concurrent/ExecutorService;

.field mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;


# direct methods
.method private constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 1
    .param p1, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/PRActivator;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 33
    iput-object p1, p0, Lcom/microsoft/playready/PRActivator;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 34
    return-void
.end method

.method static createPRActivator(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/PRActivator;
    .locals 1
    .param p0, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 23
    new-instance v0, Lcom/microsoft/playready/PRActivator;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRActivator;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    return-object v0
.end method


# virtual methods
.method public activateAsync()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/microsoft/playready/PRActivator$1;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRActivator$1;-><init>(Lcom/microsoft/playready/PRActivator;)V

    .line 64
    .local v0, "doActivate":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Void;>;"
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 65
    .local v1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/microsoft/playready/PRActivator;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 67
    return-object v1
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRActivator;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 48
    return-void

    .line 45
    :catchall_0
    move-exception v0

    .line 46
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 47
    throw v0
.end method
