.class Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "FutureShowtimesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private channelCallSign:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private channelLogoImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

.field private channelNumber:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private episodeProviderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$1;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeProviderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->episodeProviderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelNumber:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelNumber:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/EPGImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelLogoImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/ui/EPGImageView;)Lcom/microsoft/xbox/xle/ui/EPGImageView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/EPGImageView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelLogoImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelCallSign:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter$ViewHolder;->channelCallSign:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method
