.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;
.super Ljava/lang/Enum;
.source "UTCPeopleHub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FriendsFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum Followers:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum Friends:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum FriendsAndGames:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum Games:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "FriendsAndGames"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->FriendsAndGames:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "Friends"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Friends:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "Games"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Games:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "Followers"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Followers:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    const-string v1, "Block"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->FriendsAndGames:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Friends:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Games:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Followers:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->Block:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;

    return-object v0
.end method
