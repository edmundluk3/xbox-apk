.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileCapturesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeClickAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

.field private newLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;ZLjava/lang/String;)V
    .locals 0
    .param p2, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p3, "newLikeState"    # Z
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 487
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    .line 488
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 489
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    .line 490
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 494
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 495
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->likeCapture(ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 505
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 500
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 501
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 519
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    .line 520
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 481
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LikeClickAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 515
    return-void
.end method
