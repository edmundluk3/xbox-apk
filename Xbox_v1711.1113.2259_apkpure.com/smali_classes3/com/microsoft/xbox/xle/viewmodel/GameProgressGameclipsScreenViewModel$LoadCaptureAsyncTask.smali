.class Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProgressGameclipsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCaptureAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 292
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 293
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 294
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 298
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 304
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 306
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 320
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 287
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 310
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z

    .line 314
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 315
    return-void
.end method
