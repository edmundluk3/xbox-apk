.class public Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "GamerscoreLeaderboardListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LeaderboardItemViewHolder"
.end annotation


# instance fields
.field private final gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private final ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

.field private final ribbonLayout:Landroid/widget/RelativeLayout;

.field private final rootView:Landroid/view/View;

.field private final subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    .line 72
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 74
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    const v1, 0x7f0e06a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;)Landroid/view/View$OnLongClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->rootView:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder$$Lambda$2;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    return-void
.end method

.method static synthetic lambda$bindTo$3(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->goToProfile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;Landroid/view/View;)Z
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 83
    new-instance v2, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v2, v4, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 84
    .local v2, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0f0007

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->getAdapterPosition()I

    move-result v3

    .line 86
    .local v3, "position":I
    if-ltz v3, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->getDataCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 87
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 88
    .local v1, "leaderboardListItem":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getXuid()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "friendXuid":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;Ljava/lang/String;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 104
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    .line 108
    .end local v0    # "friendXuid":Ljava/lang/String;
    .end local v1    # "leaderboardListItem":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    :cond_0
    const/4 v4, 0x1

    return v4
.end method

.method static synthetic lambda$new$2(Landroid/view/View;)V
    .locals 4
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "screenName":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currently on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreen;

    if-nez v1, :cond_0

    .line 117
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->TAG:Ljava/lang/String;

    const-string v2, "do navigate to Gamerscore Leaderboard"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 120
    :cond_0
    return-void
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;Ljava/lang/String;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/view/MenuItem;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;
    .param p1, "friendXuid"    # Ljava/lang/String;
    .param p2, "leaderboardListItem"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .param p3, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 91
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 102
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 93
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->shareLeaderboardCompare(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getUser()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->navigateToSendMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c2b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)V
    .locals 18
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 125
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    const-string v12, "%1$s, %2$s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sget-object v15, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v16, 0x7f07066c

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamertag()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070600

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamertag()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0705ff

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v5, "gamerscoreString":Ljava/lang/StringBuilder;
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v12}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getSelfMonthlyGamerscore()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 135
    .local v2, "gamerscoreDiff":J
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070f51

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x20

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 137
    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-lez v10, :cond_1

    .line 138
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0706a5

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 148
    .local v7, "unconvertedText":Ljava/lang/String;
    :goto_0
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070f51

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 149
    .local v4, "gamerscoreIconIndex":I
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 150
    .local v6, "gamerscoreText":Landroid/text/SpannableString;
    if-ltz v4, :cond_0

    .line 151
    new-instance v10, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v11}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v11

    const-string v12, "fonts/SegXboxSymbol.ttf"

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    add-int/lit8 v11, v4, 0x1

    const/16 v12, 0x21

    invoke-virtual {v6, v10, v4, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 153
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v10, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    const/high16 v10, 0x42c80000    # 100.0f

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    long-to-float v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;

    invoke-static {v12}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardViewModelBase;->getTopMonthlyGamerscore()J

    move-result-wide v12

    long-to-float v12, v12

    div-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-long v8, v10

    .line 156
    .local v8, "percentOfMax":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const-wide/16 v12, 0x64

    sub-long/2addr v12, v8

    invoke-virtual {v10, v8, v9, v12, v13}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->getAdapterPosition()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 178
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 179
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getPrimaryUserColor()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 180
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getSecondaryUserColor()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 183
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    return-void

    .line 139
    .end local v4    # "gamerscoreIconIndex":I
    .end local v6    # "gamerscoreText":Landroid/text/SpannableString;
    .end local v7    # "unconvertedText":Ljava/lang/String;
    .end local v8    # "percentOfMax":J
    :cond_1
    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-gez v10, :cond_2

    .line 140
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0706a6

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 141
    .end local v7    # "unconvertedText":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v10, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->isSelf:Z

    if-eqz v10, :cond_3

    .line 142
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0706a8

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 144
    .end local v7    # "unconvertedText":Ljava/lang/String;
    :cond_3
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0706a7

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 160
    .restart local v4    # "gamerscoreIconIndex":I
    .restart local v6    # "gamerscoreText":Landroid/text/SpannableString;
    .restart local v8    # "percentOfMax":J
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 161
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c00c5

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 162
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c00c5

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c00c6

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto :goto_1

    .line 166
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 167
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c012d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 168
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c012d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 169
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c012e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto/16 :goto_1

    .line 172
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 173
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0034

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 174
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0034

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 175
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0035

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto/16 :goto_1

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
