.class final Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ReplayRelay.java"

# interfaces
.implements Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxrelay2/ReplayRelay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SizeBoundReplayBuffer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0xf5f291fe2c1030bL


# instance fields
.field volatile head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/ReplayRelay$Node",
            "<TT;>;"
        }
    .end annotation
.end field

.field final maxSize:I

.field size:I

.field tail:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/ReplayRelay$Node",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .locals 4
    .param p1, "maxSize"    # I

    .prologue
    .line 530
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 531
    if-gtz p1, :cond_0

    .line 532
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "maxSize > 0 required but it was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 534
    :cond_0
    iput p1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->maxSize:I

    .line 535
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;-><init>(Ljava/lang/Object;)V

    .line 536
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 537
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 538
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 550
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    invoke-direct {v0, p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;-><init>(Ljava/lang/Object;)V

    .line 551
    .local v0, "n":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 553
    .local v1, "t":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 554
    iget v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size:I

    .line 555
    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->set(Ljava/lang/Object;)V

    .line 557
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->trim()V

    .line 558
    return-void
.end method

.method public getValue()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 563
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 566
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 567
    .local v1, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    if-nez v1, :cond_0

    .line 573
    iget-object v2, v0, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->value:Ljava/lang/Object;

    return-object v2

    .line 570
    :cond_0
    move-object v0, v1

    .line 571
    goto :goto_0
.end method

.method public getValues([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    const/4 v5, 0x0

    .line 579
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 580
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size()I

    move-result v3

    .line 582
    .local v3, "s":I
    if-nez v3, :cond_1

    .line 583
    array-length v4, p1

    if-eqz v4, :cond_0

    .line 584
    const/4 v4, 0x0

    aput-object v5, p1, v4

    .line 603
    :cond_0
    :goto_0
    return-object p1

    .line 587
    :cond_1
    array-length v4, p1

    if-ge v4, v3, :cond_2

    .line 588
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object p1, v4

    check-cast p1, [Ljava/lang/Object;

    .line 591
    :cond_2
    const/4 v1, 0x0

    .line 592
    .local v1, "i":I
    :goto_1
    if-eq v1, v3, :cond_3

    .line 593
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 594
    .local v2, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    iget-object v4, v2, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->value:Ljava/lang/Object;

    aput-object v4, p1, v1

    .line 595
    add-int/lit8 v1, v1, 0x1

    .line 596
    move-object v0, v2

    .line 597
    goto :goto_1

    .line 598
    .end local v2    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :cond_3
    array-length v4, p1

    if-le v4, v3, :cond_0

    .line 599
    aput-object v5, p1, v3

    goto :goto_0
.end method

.method public replay(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 609
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->getAndIncrement()I

    move-result v5

    if-eqz v5, :cond_0

    .line 653
    :goto_0
    return-void

    .line 613
    :cond_0
    const/4 v2, 0x1

    .line 614
    .local v2, "missed":I
    iget-object v0, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->actual:Lio/reactivex/Observer;

    .line 616
    .local v0, "a":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    iget-object v1, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 617
    .local v1, "index":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    if-nez v1, :cond_1

    .line 618
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 624
    :cond_1
    :goto_1
    iget-boolean v5, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v5, :cond_2

    .line 625
    const/4 v5, 0x0

    iput-object v5, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_0

    .line 629
    :cond_2
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 631
    .local v3, "n":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    if-nez v3, :cond_3

    .line 642
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 646
    iput-object v1, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    .line 648
    neg-int v5, v2

    invoke-virtual {p1, v5}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->addAndGet(I)I

    move-result v2

    .line 649
    if-nez v2, :cond_1

    goto :goto_0

    .line 635
    :cond_3
    iget-object v4, v3, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->value:Ljava/lang/Object;

    .line 637
    .local v4, "o":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, v4}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 639
    move-object v1, v3

    .line 640
    goto :goto_1
.end method

.method public size()I
    .locals 4

    .prologue
    .line 657
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    const/4 v2, 0x0

    .line 658
    .local v2, "s":I
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 659
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :goto_0
    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    .line 660
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 661
    .local v1, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    if-nez v1, :cond_1

    .line 668
    .end local v1    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :cond_0
    return v2

    .line 664
    .restart local v1    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 665
    move-object v0, v1

    .line 666
    goto :goto_0
.end method

.method trim()V
    .locals 3

    .prologue
    .line 541
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer<TT;>;"
    iget v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size:I

    iget v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->maxSize:I

    if-le v1, v2, :cond_0

    .line 542
    iget v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->size:I

    .line 543
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 544
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$Node;

    .line 546
    .end local v0    # "h":Lcom/jakewharton/rxrelay2/ReplayRelay$Node;, "Lcom/jakewharton/rxrelay2/ReplayRelay$Node<TT;>;"
    :cond_0
    return-void
.end method
