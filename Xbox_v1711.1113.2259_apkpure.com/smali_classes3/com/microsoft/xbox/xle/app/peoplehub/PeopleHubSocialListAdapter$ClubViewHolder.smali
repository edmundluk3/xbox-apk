.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;
.super Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
.source "PeopleHubSocialListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClubViewHolder"
.end annotation


# instance fields
.field clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d5
    .end annotation
.end field

.field clubGlyph:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d2
    .end annotation
.end field

.field clubImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08cf
    .end annotation
.end field

.field clubManagementIndicator:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d6
    .end annotation
.end field

.field clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d1
    .end annotation
.end field

.field clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d3
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V
    .locals 3
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    .line 155
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    .line 156
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802c6

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 158
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;Lcom/microsoft/xbox/toolkit/generics/Action;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;
    .param p1, "clubListItem"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 186
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "targetXuid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubId()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-static {v2, v3, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackPeopleHubNavigateToClub(JLjava/lang/String;I)V

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 189
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "itemAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    const v5, 0x7f0201fa

    .line 162
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 163
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 165
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;

    .line 168
    .local v0, "clubListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubImage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 169
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubTypeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getSubText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubGlyph()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 174
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->getClubGlyph()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 180
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;->hasManagementActions()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 181
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 182
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    .line 183
    .local v1, "color":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 185
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, v0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;Lcom/microsoft/xbox/toolkit/generics/Action;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    .end local v0    # "clubListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;
    .end local v1    # "color":I
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :goto_2
    return-void

    .line 177
    .restart local v0    # "clubListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 182
    .restart local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_1

    .line 191
    .end local v0    # "clubListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t bind ClubViewModel to non-ClubListItem: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_2
.end method
