.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;
.super Ljava/lang/Object;
.source "ClubChatDirectMentionListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DirectMentionViewHolder"
.end annotation


# instance fields
.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final itemView:Landroid/view/View;

.field private final realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 59
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->itemView:Landroid/view/View;

    .line 61
    const v0, 0x7f0e02c9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 62
    const v0, 0x7f0e02ca

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 63
    const v0, 0x7f0e02cb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 66
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fa

    .line 69
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->itemView:Landroid/view/View;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerPic:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->realName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method
