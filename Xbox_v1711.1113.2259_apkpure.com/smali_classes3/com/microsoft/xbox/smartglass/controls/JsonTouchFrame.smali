.class Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
.super Lorg/json/JSONObject;
.source "JsonTouchFrame.java"


# instance fields
.field public final timeStamp:I

.field public final titleId:I

.field public final touchPoints:[Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 17
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 19
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "titleId"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->titleId:I

    .line 20
    const-string v4, "timeStamp"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->timeStamp:I

    .line 23
    const-string v4, "touchPoints"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 24
    .local v2, "jsonTouchPoints":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 25
    .local v3, "touchPointsCount":I
    new-array v4, v3, [Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->touchPoints:[Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;

    .line 26
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 27
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->touchPoints:[Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;

    new-instance v6, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    invoke-direct {v6, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;-><init>(Lorg/json/JSONObject;)V

    aput-object v6, v5, v0

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_0
    return-void
.end method
