.class public Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubDiscoveryInvitationListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final inviteClickListener:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;

.field private final layoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inviteClickListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 43
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->inviteClickListener:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 47
    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 56
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->inviteClickListener:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;)V

    .line 57
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03008a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;Landroid/view/View;)V

    return-object v0
.end method
