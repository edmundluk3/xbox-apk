.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "PeopleHubActivityScreenAdapter.java"


# instance fields
.field private pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    .prologue
    const v2, 0x7f0c0149

    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 16
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    .line 18
    const v0, 0x7f0e090f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 23
    :cond_0
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->getHeaderBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 31
    :cond_0
    return-void
.end method
