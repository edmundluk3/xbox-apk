.class public Lcom/microsoft/playready/networkdevice/TransmitterContent;
.super Ljava/lang/Object;
.source "TransmitterContent.java"


# instance fields
.field protected mContentID:Ljava/lang/String;

.field protected mIsContainer:Z

.field protected mName:Ljava/lang/String;

.field protected mParent:Lcom/microsoft/playready/networkdevice/TransmitterContent;

.field protected mProperties:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mURL:Ljava/lang/String;

.field protected mUUID:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterContent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;)V
    .locals 2
    .param p1, "parent"    # Lcom/microsoft/playready/networkdevice/TransmitterContent;
    .param p2, "contentID"    # Ljava/lang/String;
    .param p3, "contentName"    # Ljava/lang/String;
    .param p4, "contentURLPath"    # Ljava/lang/String;
    .param p5, "isContainer"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p6, "properties":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mIsContainer:Z

    .line 25
    iput-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mParent:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    .line 26
    iput-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mContentID:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mName:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mUUID:Ljava/util/UUID;

    .line 40
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mParent:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    .line 41
    iput-object p6, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mProperties:Ljava/util/Hashtable;

    .line 42
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mContentID:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mName:Ljava/lang/String;

    .line 44
    const-string v0, " "

    const-string v1, "%20"

    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    .line 45
    iput-boolean p5, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mIsContainer:Z

    .line 46
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mUUID:Ljava/util/UUID;

    .line 47
    return-void
.end method


# virtual methods
.method public getContentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mContentID:Ljava/lang/String;

    return-object v0
.end method

.method public getContentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getContentURL()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    .line 81
    .local v2, "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    const-string v4, "http:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    const-string v4, "https:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 86
    .local v0, "isAbsoluteURI":Z
    :goto_0
    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mParent:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    if-eqz v3, :cond_1

    .line 88
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mParent:Lcom/microsoft/playready/networkdevice/TransmitterContent;

    invoke-virtual {v3}, Lcom/microsoft/playready/networkdevice/TransmitterContent;->getContentURL()Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "parentURL":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 92
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    .end local v1    # "parentURL":Ljava/lang/String;
    :cond_1
    const-string v3, " "

    const-string v4, "%20"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 98
    return-object v2

    .line 81
    .end local v0    # "isAbsoluteURI":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentUUID()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mUUID:Ljava/util/UUID;

    return-object v0
.end method

.method public getPropertyValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 144
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mProperties:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mProperties:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "value":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 149
    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public isContainer()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mIsContainer:Z

    return v0
.end method

.method public setContentUUID(Ljava/util/UUID;)V
    .locals 0
    .param p1, "contentUUID"    # Ljava/util/UUID;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mUUID:Ljava/util/UUID;

    .line 131
    return-void
.end method
