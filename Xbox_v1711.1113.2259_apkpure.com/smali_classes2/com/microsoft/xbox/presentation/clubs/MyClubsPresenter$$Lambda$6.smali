.class final synthetic Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

.field private final arg$3:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$1:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$2:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$3:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;-><init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$1:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$2:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$6;->arg$3:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;

    check-cast p1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->lambda$null$4(Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
