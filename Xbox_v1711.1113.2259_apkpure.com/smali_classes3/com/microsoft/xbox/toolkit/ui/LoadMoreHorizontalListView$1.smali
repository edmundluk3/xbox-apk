.class Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;
.super Ljava/lang/Object;
.source "LoadMoreHorizontalListView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;III)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 47
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 48
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-nez v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    add-int v1, p2, p3

    .line 53
    .local v1, "rightViewIndex":I
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->onLoadMoreStart()V

    goto :goto_0
.end method

.method public onScrollStateChanged(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "scrollState"    # I

    .prologue
    .line 38
    if-nez p2, :cond_0

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    goto :goto_0
.end method
