.class public abstract Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;
.super Ljava/lang/Object;
.source "LanguageSettingsUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSystemConfiguredLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 20
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLanguage(Ljava/util/Locale;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemConfiguredLanguage(Ljava/util/Locale;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .locals 7
    .param p0, "deviceLocale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .line 35
    .local v4, "systemConfiguredLanguage":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->values()[Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 36
    .local v2, "languages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;>;"
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 39
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .line 40
    .local v0, "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->getLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 41
    move-object v4, v0

    .line 47
    .end local v0    # "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    if-ne v4, v5, :cond_4

    .line 48
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "languageComponent":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .line 51
    .restart local v0    # "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->getLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 52
    move-object v4, v0

    .line 58
    .end local v0    # "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    :cond_3
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    if-eq v4, v5, :cond_4

    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLocation(Ljava/util/Locale;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v3

    .line 61
    .local v3, "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_4

    .line 62
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils$1;->$SwitchMap$com$microsoft$xbox$domain$settings$language$LanguageSettingsDataTypes$SupportedLanguage:[I

    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 113
    .end local v1    # "languageComponent":Ljava/lang/String;
    .end local v3    # "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    :cond_4
    :goto_0
    return-object v4

    .line 65
    .restart local v1    # "languageComponent":Ljava/lang/String;
    .restart local v3    # "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    :pswitch_0
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Mexico:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_5

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->UnitedStates:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_5

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Argentina:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_5

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Colombia:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_5

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Chile:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-ne v3, v5, :cond_4

    .line 70
    :cond_5
    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->LATAMSpanish:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    goto :goto_0

    .line 76
    :pswitch_1
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->UnitedKingdom:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Ireland:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Australia:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->NewZealand:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Greece:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->CzechRepublic:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Hungary:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->India:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Israel:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->SaudiArabia:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Slovakia:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->SouthAfrica:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Singapore:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->UAE:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->HongKong:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Canada:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->SouthKorea:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-ne v3, v5, :cond_4

    .line 93
    :cond_6
    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->UKEnglish:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    goto :goto_0

    .line 99
    :pswitch_2
    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Mexico:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_7

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->UnitedStates:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_7

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Argentina:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_7

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Colombia:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_7

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Chile:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-eq v3, v5, :cond_7

    sget-object v5, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->Brazil:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-ne v3, v5, :cond_4

    .line 105
    :cond_7
    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->BrazilPortuguese:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getSystemConfiguredLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 25
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLocation(Ljava/util/Locale;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemConfiguredLocation(Ljava/util/Locale;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .locals 6
    .param p0, "deviceLocale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 117
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 119
    sget-object v3, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .line 121
    .local v3, "systemConfiguredLocation":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "country":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->values()[Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 124
    .local v2, "locations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;>;"
    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .line 127
    .local v1, "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getMarket()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 128
    move-object v3, v1

    .line 133
    .end local v1    # "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    :cond_1
    return-object v3
.end method
