.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract branch(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
.end method

.method public abstract changeNumber(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract constants(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract contractVersion(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract correlationId(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract members(Ljava/util/Map;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;)",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;"
        }
    .end annotation
.end method

.method public abstract properties(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract roleTypes(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract searchHandle(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract servers(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public abstract startTime(Ljava/util/Date;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
.end method

.method public withLfgApproval(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
    .locals 4
    .param p1, "memberIndex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 954
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 956
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 957
    .local v1, "memberMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v2

    const-string v3, "confirmed"

    .line 958
    invoke-static {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->roles(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v2

    .line 959
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v0

    .line 961
    .local v0, "approvedMember":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 963
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->members(Ljava/util/Map;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    return-object v2
.end method

.method public withSelf(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
    .locals 2
    .param p1, "member"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .prologue
    .line 947
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 948
    .local v0, "membersMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    const-string v1, "me"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->members(Ljava/util/Map;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    return-object v1
.end method
