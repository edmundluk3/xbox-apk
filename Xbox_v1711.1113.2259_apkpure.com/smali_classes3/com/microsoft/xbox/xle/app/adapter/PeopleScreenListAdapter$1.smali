.class synthetic Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 406
    invoke-static {}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->values()[Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_FACEBOOK:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_2
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_PHONE_CONTACT:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_4
    :try_start_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_CLUBS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_5
    :try_start_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_SEARCH_TAG:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_6
    :try_start_7
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_NO_DATA:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$DummyType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_ERROR:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    .line 75
    :goto_8
    invoke-static {}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->values()[Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    :try_start_9
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Person:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Club:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Dummy:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    move-exception v0

    goto :goto_b

    :catch_1
    move-exception v0

    goto :goto_a

    :catch_2
    move-exception v0

    goto :goto_9

    .line 406
    :catch_3
    move-exception v0

    goto :goto_8

    :catch_4
    move-exception v0

    goto :goto_7

    :catch_5
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_4

    :catch_8
    move-exception v0

    goto :goto_3

    :catch_9
    move-exception v0

    goto/16 :goto_2

    :catch_a
    move-exception v0

    goto/16 :goto_1

    :catch_b
    move-exception v0

    goto/16 :goto_0
.end method
