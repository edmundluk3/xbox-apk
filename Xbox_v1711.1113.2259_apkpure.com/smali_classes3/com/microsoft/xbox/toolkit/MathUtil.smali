.class public Lcom/microsoft/xbox/toolkit/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clamp(FFF)F
    .locals 1
    .param p0, "v"    # F
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 55
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public static clamp(III)I
    .locals 1
    .param p0, "v"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 65
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static positiveMod(FI)F
    .locals 2
    .param p0, "n"    # F
    .param p1, "d"    # I

    .prologue
    const/4 v1, 0x0

    .line 24
    cmpg-float v0, p0, v1

    if-gez v0, :cond_0

    .line 25
    int-to-float v0, p1

    rem-float/2addr p0, v0

    .line 26
    int-to-float v0, p1

    add-float/2addr p0, v0

    .line 27
    cmpl-float v0, p0, v1

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 30
    :cond_0
    int-to-float v0, p1

    rem-float v0, p0, v0

    return v0

    .line 27
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static positiveMod(II)I
    .locals 1
    .param p0, "n"    # I
    .param p1, "d"    # I

    .prologue
    .line 39
    if-gez p0, :cond_0

    .line 40
    rem-int/2addr p0, p1

    .line 41
    add-int/2addr p0, p1

    .line 42
    if-ltz p0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 45
    :cond_0
    rem-int v0, p0, p1

    return v0

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
