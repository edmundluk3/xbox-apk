.class public final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityInfoAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private defaultActivityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

.field private defaultId:Ljava/lang/String;

.field private defaultRelatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

.field private defaultRoleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

.field private defaultSearchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

.field private defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private defaultType:Ljava/lang/String;

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final relatedInfoAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final roleInfoAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final searchAttributesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionRefAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;",
            ">;"
        }
    .end annotation
.end field

.field private final typeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultType:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultActivityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSearchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRelatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRoleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .line 38
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->activityInfoAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->searchAttributesAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->relatedInfoAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->roleInfoAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .locals 10
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v9, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 103
    const/4 v0, 0x0

    .line 154
    :goto_0
    return-object v0

    .line 105
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 107
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultType:Ljava/lang/String;

    .line 108
    .local v2, "type":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 109
    .local v3, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    iget-object v4, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultActivityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .line 110
    .local v4, "activityInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;
    iget-object v5, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSearchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .line 111
    .local v5, "searchAttributes":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
    iget-object v6, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRelatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .line 112
    .local v6, "relatedInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;
    iget-object v7, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRoleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .line 113
    .local v7, "roleInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v8

    .line 115
    .local v8, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v9, :cond_1

    .line 116
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 119
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 149
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 119
    :sswitch_0
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v9, "type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v9, "sessionRef"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v9, "activityInfo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v9, "searchAttributes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v9, "relatedInfo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v9, "roleInfo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    .line 121
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 122
    .restart local v1    # "id":Ljava/lang/String;
    goto :goto_1

    .line 125
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "type":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 126
    .restart local v2    # "type":Ljava/lang/String;
    goto :goto_1

    .line 129
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    check-cast v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 130
    .restart local v3    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    goto/16 :goto_1

    .line 133
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->activityInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "activityInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;
    check-cast v4, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .line 134
    .restart local v4    # "activityInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;
    goto/16 :goto_1

    .line 137
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->searchAttributesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "searchAttributes":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
    check-cast v5, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .line 138
    .restart local v5    # "searchAttributes":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
    goto/16 :goto_1

    .line 141
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->relatedInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "relatedInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;
    check-cast v6, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .line 142
    .restart local v6    # "relatedInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;
    goto/16 :goto_1

    .line 145
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->roleInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "roleInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    check-cast v7, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .line 146
    .restart local v7    # "roleInfo":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    goto/16 :goto_1

    .line 153
    .end local v8    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 154
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)V

    goto/16 :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        -0x66eb06e1 -> :sswitch_4
        -0xfe8d1bc -> :sswitch_6
        0xd1b -> :sswitch_0
        0x368f3a -> :sswitch_1
        0x42526779 -> :sswitch_5
        0x6110ac5d -> :sswitch_3
        0x630dae1d -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultActivityInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultActivityInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultActivityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .line 60
    return-object p0
.end method

.method public setDefaultId(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultId"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public setDefaultRelatedInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRelatedInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRelatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .line 68
    return-object p0
.end method

.method public setDefaultRoleInfo(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRoleInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultRoleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .line 72
    return-object p0
.end method

.method public setDefaultSearchAttributes(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSearchAttributes"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSearchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .line 64
    return-object p0
.end method

.method public setDefaultSessionRef(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 56
    return-object p0
.end method

.method public setDefaultType(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultType"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->defaultType:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    if-nez p2, :cond_0

    .line 78
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 97
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 82
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 84
    const-string v0, "type"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 86
    const-string v0, "sessionRef"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 88
    const-string v0, "activityInfo"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->activityInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 90
    const-string v0, "searchAttributes"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->searchAttributesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 92
    const-string v0, "relatedInfo"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->relatedInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 94
    const-string v0, "roleInfo"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->roleInfoAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 96
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    return-void
.end method
