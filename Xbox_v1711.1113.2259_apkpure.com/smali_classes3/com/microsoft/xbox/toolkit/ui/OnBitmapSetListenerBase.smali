.class public abstract Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListenerBase;
.super Ljava/lang/Object;
.source "OnBitmapSetListenerBase.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAfterImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 16
    return-void
.end method

.method public onBeforeImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 12
    return-void
.end method
