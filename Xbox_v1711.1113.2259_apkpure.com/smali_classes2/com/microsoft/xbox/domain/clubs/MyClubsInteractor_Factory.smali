.class public final Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;
.super Ljava/lang/Object;
.source "MyClubsInteractor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final clubModelManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;"
        }
    .end annotation
.end field

.field private final xuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "xuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p2, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-boolean v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->xuidProvider:Ljavax/inject/Provider;

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->clubModelManagerProvider:Ljavax/inject/Provider;

    .line 24
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "xuidProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;>;"
    .local p1, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
    .locals 3

    .prologue
    .line 28
    new-instance v2, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->xuidProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->clubModelManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    invoke-direct {v2, v0, v1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;-><init>(Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;)V

    return-object v2
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->get()Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    move-result-object v0

    return-object v0
.end method
