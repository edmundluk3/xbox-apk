.class public Lcom/microsoft/xbox/toolkit/PerfDB;
.super Ljava/lang/Object;
.source "PerfDB.java"


# static fields
.field private static inst:Lcom/microsoft/xbox/toolkit/PerfDB;


# instance fields
.field private data:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private enabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/toolkit/PerfDB;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/PerfDB;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/PerfDB;->inst:Lcom/microsoft/xbox/toolkit/PerfDB;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->enabled:Z

    .line 22
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->data:Ljava/util/Hashtable;

    return-void
.end method

.method public static instance()Lcom/microsoft/xbox/toolkit/PerfDB;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/toolkit/PerfDB;->inst:Lcom/microsoft/xbox/toolkit/PerfDB;

    return-object v0
.end method


# virtual methods
.method public buildId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getData(Ljava/lang/String;)J
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->data:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->data:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    .line 49
    :cond_0
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t find "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getData(Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-virtual {p0, p2, p1}, Lcom/microsoft/xbox/toolkit/PerfDB;->buildId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/PerfDB;->getData(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public saveData(Ljava/lang/String;J)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "val"    # J

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->enabled:Z

    if-nez v0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->data:Ljava/util/Hashtable;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public saveData(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "val"    # J

    .prologue
    .line 37
    invoke-virtual {p0, p2, p1}, Lcom/microsoft/xbox/toolkit/PerfDB;->buildId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3, p4}, Lcom/microsoft/xbox/toolkit/PerfDB;->saveData(Ljava/lang/String;J)V

    .line 38
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "e"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/PerfDB;->enabled:Z

    .line 26
    return-void
.end method
