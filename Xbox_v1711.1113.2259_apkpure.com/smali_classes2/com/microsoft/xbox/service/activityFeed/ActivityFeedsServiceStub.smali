.class public final enum Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;
.super Ljava/lang/Enum;
.source "ActivityFeedsServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

.field private static final ACTIVITY_FEED_SETTING:Ljava/lang/String; = "stubdata/ActivityFeedSettings.json"

.field private static final FEED_PIN_INFO:Ljava/lang/String; = "stubdata/ActivityFeedPinInfo.json"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsServiceStub;

    return-object v0
.end method


# virtual methods
.method public getActivityFeedMeSettings()Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 26
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ActivityFeedSettings.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    return-object v1

    .line 27
    :catch_0
    move-exception v0

    .line 28
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFeedPinInfo(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    .locals 3
    .param p1, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p2, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 50
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/ActivityFeedPinInfo.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-object v1

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hideActivityItem(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "hide"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 35
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public pinActivityItem(Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "pin"    # Z
    .param p3, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p4, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 43
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    const/4 v0, 0x0

    return v0
.end method
