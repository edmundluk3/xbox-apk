.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;
.super Ljava/lang/Enum;
.source "UTCLfg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LFGType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

.field public static final enum Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

.field public static final enum Suggested:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

.field public static final enum Upcoming:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    const-string v1, "Upcoming"

    const-string v2, "Upcoming"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Upcoming:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    const-string v1, "Game"

    const-string v2, "Game"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    const-string v1, "Suggested"

    const-string v2, "Suggested"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Suggested:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Upcoming:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Game:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->Suggested:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "typeName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->name:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$LFGType;->name:Ljava/lang/String;

    return-object v0
.end method
