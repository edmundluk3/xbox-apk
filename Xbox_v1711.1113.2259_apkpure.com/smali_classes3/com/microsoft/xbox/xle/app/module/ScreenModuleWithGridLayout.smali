.class public abstract Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGridLayout;
.super Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
.source "ScreenModuleWithGridLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected abstract getGridLayout()Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;
.end method

.method protected abstract getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
.end method

.method public setState(I)V
    .locals 1
    .param p1, "listStateOrdinal"    # I

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGridLayout;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGridLayout;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGridLayout;->getGridLayout()Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->getGridAdapter()Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->setGridLayoutModelState(I)V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGridLayout;->getGridLayout()Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->notifyDataChanged()V

    .line 22
    return-void
.end method
