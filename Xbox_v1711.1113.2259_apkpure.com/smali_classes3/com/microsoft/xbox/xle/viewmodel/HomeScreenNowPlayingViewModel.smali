.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "HomeScreenNowPlayingViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithPosition;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

.field private bindViewsOnResize:Z

.field private final containerResourceId:I

.field private currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

.field private currentModelRecents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation
.end field

.field private loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

.field private final pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

.field private position:I

.field private final profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

.field private final viewModelRecents:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 74
    invoke-direct {p0, v0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;-><init>(II)V

    .line 75
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "containerResourceId"    # I
    .param p2, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 61
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v0, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->bindViewsOnResize:Z

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currentModelRecents:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    .line 84
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->containerResourceId:I

    .line 85
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->position:I

    .line 86
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModelNoScreen;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModelNoScreen;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 89
    return-void

    .line 88
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ILjava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/microsoft/xbox/service/model/TitleModel;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->onLoadXboxoneTitleSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ILjava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    return-object p1
.end method

.method private cancelTitleSummaryTask(I)Z
    .locals 3
    .param p1, "newTitleId"    # I

    .prologue
    const/4 v0, 0x1

    .line 395
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->getIsBusy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 396
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v2, "cancelTitleSummaryTask: the task is not running"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :goto_0
    return v0

    .line 399
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->getTitleId()I

    move-result v1

    if-eq v1, p1, :cond_2

    .line 400
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->cancel()V

    .line 401
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    .line 402
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v2, "cancelTitleSummaryTask: the has been cancelled"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v1, "cancelTitleSummaryTask: the has NOT been cancelled"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ensureCurrentProgressItem(Z)V
    .locals 14
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v13, 0x0

    const/4 v12, -0x1

    .line 341
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    if-eqz v7, :cond_3

    .line 342
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    .line 343
    .local v3, "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v3, :cond_7

    .line 345
    const/4 v0, 0x0

    .line 346
    .local v0, "isApp":Z
    const/4 v1, 0x0

    .line 347
    .local v1, "isGame":Z
    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 348
    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v4

    .line 349
    .local v4, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v6

    .line 350
    .local v6, "titleId":I
    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    .line 351
    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    .line 358
    .end local v4    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    if-nez p1, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEDSV2MediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    iget-wide v8, v7, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    int-to-long v10, v6

    cmp-long v7, v8, v10

    if-eqz v7, :cond_3

    .line 359
    :cond_0
    if-nez v0, :cond_1

    if-eqz v1, :cond_6

    .line 360
    :cond_1
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "Getting progress data"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v2

    .line 363
    .local v2, "localProgressItem":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    if-eqz v2, :cond_2

    if-eqz p1, :cond_5

    .line 364
    :cond_2
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->cancelTitleSummaryTask(I)Z

    .line 365
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "No progress data found locally, loading progress data"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    new-instance v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    invoke-direct {v7, p0, v6, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;IZ)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    .line 367
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->load(Z)V

    .line 386
    .end local v0    # "isApp":Z
    .end local v1    # "isGame":Z
    .end local v2    # "localProgressItem":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    .end local v3    # "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .end local v6    # "titleId":I
    :cond_3
    :goto_1
    return-void

    .line 353
    .restart local v0    # "isApp":Z
    .restart local v1    # "isGame":Z
    .restart local v3    # "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    :cond_4
    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v5

    .line 354
    .local v5, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v7, v5, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v8

    long-to-int v6, v8

    .line 355
    .restart local v6    # "titleId":I
    iget-object v7, v5, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    .line 356
    iget-object v7, v5, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    goto :goto_0

    .line 368
    .end local v5    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    .restart local v2    # "localProgressItem":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_5
    if-eqz v2, :cond_3

    .line 370
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 371
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    goto :goto_1

    .line 374
    .end local v2    # "localProgressItem":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_6
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "The title is not an app nor a game, skipping loading progress data"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-direct {p0, v12}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->cancelTitleSummaryTask(I)Z

    .line 376
    iput-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 377
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    goto :goto_1

    .line 381
    .end local v0    # "isApp":Z
    .end local v1    # "isGame":Z
    .end local v6    # "titleId":I
    :cond_7
    invoke-direct {p0, v12}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->cancelTitleSummaryTask(I)Z

    .line 382
    iput-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 383
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    goto :goto_1
.end method

.method private isLoadingGameProgress()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->loadTitleSummaryTask:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onLoadXboxoneTitleSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ILjava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "titleId"    # I
    .param p3, "strTitleId"    # Ljava/lang/String;
    .param p4, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;

    .prologue
    .line 440
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadXboxoneTitleSummaryCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$3;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 468
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->updateAdapter()V

    .line 469
    return-void

    .line 446
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 447
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-nez v0, :cond_1

    .line 451
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v1, "No achivements loaded"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    int-to-long v2, p2

    iput-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    .line 457
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {p4}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setEDSV2MediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 458
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    .line 459
    invoke-direct {p0, p3, p4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->updateTitleStatistics(Ljava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V

    goto :goto_0

    .line 455
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Progress data loaded successfully"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 463
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 464
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private tappedAtModel(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "indexPos"    # I

    .prologue
    const/4 v4, 0x1

    .line 412
    if-eqz p1, :cond_1

    .line 413
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 414
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackMRUAction(Ljava/lang/String;I)V

    .line 419
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v1

    .line 420
    .local v1, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    new-instance v2, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$2;

    invoke-direct {v3, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;-><init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 426
    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 433
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v1    # "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    :goto_0
    return-void

    .line 428
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Destination media item is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 431
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Destination Now Playing Model is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateScreenAdapter()V
    .locals 0

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->updateAdapter()V

    .line 437
    return-void
.end method

.method private updateTitleStatistics(Ljava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V
    .locals 6
    .param p1, "titleId"    # Ljava/lang/String;
    .param p2, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;

    .prologue
    .line 472
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleStatisticsData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v3

    .line 473
    .local v3, "statsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    if-eqz v3, :cond_3

    .line 474
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 475
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 476
    .local v0, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 477
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 478
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setHeroStats(Ljava/util/ArrayList;)V

    .line 484
    .end local v0    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    :cond_1
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 485
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 486
    .local v1, "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v1, :cond_3

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 487
    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 488
    .local v2, "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v2, :cond_2

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 489
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 490
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setMinutesPlayedFromStats(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 498
    .end local v1    # "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v2    # "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    :cond_3
    return-void
.end method


# virtual methods
.method protected createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    return-object v0
.end method

.method public getAchievementsEarned()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 305
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v1, :cond_0

    .line 306
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 308
    :cond_0
    return-object v0
.end method

.method public getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    return-object v0
.end method

.method public getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I
    .locals 1
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 225
    if-nez p1, :cond_0

    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getDefaultResourceId()I

    move-result v0

    goto :goto_0
.end method

.method public getBatGameUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 312
    const/4 v0, 0x0

    .line 313
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getSuperHeroArtUri()Ljava/lang/String;

    move-result-object v0

    .line 316
    :cond_0
    return-object v0
.end method

.method public getContainerResourceId()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->containerResourceId:I

    return v0
.end method

.method public getGamerScoreSummary()Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    const/4 v0, 0x0

    .line 330
    .local v0, "ret":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v1, :cond_0

    .line 331
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "ret":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 333
    .restart local v0    # "ret":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_0
    return-object v0
.end method

.method public getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;
    .locals 1
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 274
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getHeader()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeroStat()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 282
    const/4 v1, 0x0

    .line 283
    .local v1, "heroStatLabel":Ljava/lang/String;
    const/4 v2, 0x0

    .line 284
    .local v2, "heroStatValue":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v3, :cond_1

    .line 285
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    const/4 v4, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v0

    .line 286
    .local v0, "firstHeroStat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v0, :cond_1

    .line 287
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v3, :cond_0

    .line 288
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v1, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    .line 290
    :cond_0
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    .line 294
    .end local v0    # "firstHeroStat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 295
    const/4 v3, 0x0

    .line 300
    :goto_0
    return-object v3

    .line 297
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 298
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": --"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 300
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getImageUri(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;
    .locals 1
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 270
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getImageUri()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMediaType(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I
    .locals 1
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 278
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    goto :goto_0
.end method

.method public getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    return-object v0
.end method

.method public getPinsViewModel()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->position:I

    return v0
.end method

.method public getProgressPercentage()I
    .locals 6

    .prologue
    .line 320
    const/4 v1, 0x0

    .line 321
    .local v1, "ret":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v2

    if-lez v2, :cond_0

    .line 322
    const/high16 v2, 0x42c80000    # 100.0f

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 323
    .local v0, "percent":F
    const/4 v4, 0x0

    const/16 v5, 0x64

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    :goto_0
    double-to-int v2, v2

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 325
    .end local v0    # "percent":F
    :cond_0
    return v1

    .line 323
    .restart local v0    # "percent":F
    :cond_1
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    goto :goto_0
.end method

.method public getRecents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 0

    .prologue
    .line 211
    return-object p0
.end method

.method public hasStats()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBindViewOnResize()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->bindViewsOnResize:Z

    return v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->isLoadingGameProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOOBE(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z
    .locals 3
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    const/4 v0, 0x0

    .line 233
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v1

    const v2, 0x38b0b8c2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 149
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load() forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->loadAsync(Z)V

    .line 152
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->ensureCurrentProgressItem(Z)V

    .line 153
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 127
    return-void
.end method

.method protected onStartOverride()V
    .locals 4

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 108
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPostOOBEStatus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)V

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 122
    :cond_1
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 136
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 137
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 138
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->cancelTitleSummaryTask(I)Z

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->bindViewsOnResize:Z

    .line 140
    return-void
.end method

.method public setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 97
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 221
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->position:I

    .line 222
    return-void
.end method

.method public tappedAtBat()V
    .locals 7

    .prologue
    .line 245
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 246
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v1

    .line 247
    .local v1, "liBat":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 248
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    .line 249
    .local v2, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 250
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$3;->$SwitchMap$com$microsoft$xbox$smartglass$ActiveTitleLocation:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 258
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ActiveTitleLocation is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    .end local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .end local v1    # "liBat":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .end local v2    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_0
    :goto_0
    return-void

    .line 254
    .restart local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .restart local v1    # "liBat":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .restart local v2    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :pswitch_0
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tappedAt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v5, "Home BAT"

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "1"

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->computeHasStateForBat(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v4

    sget-object v6, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_COMPANION:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    if-ne v4, v6, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-direct {p0, v2, v5, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtModel(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const-string v3, "0"

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public tappedOnSnappedNPM(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 3
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 266
    const-string v0, "Home Recents"

    const-string v1, "1"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtModel(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Ljava/lang/String;Ljava/lang/String;I)V

    .line 267
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v7, 0x0

    .line 157
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 158
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 159
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "Final update"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v5

    .line 161
    .local v5, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v5, v6, :cond_0

    .line 162
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v3

    .line 163
    .local v3, "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currentModelRecents:Ljava/util/ArrayList;

    if-eq v3, v6, :cond_0

    .line 164
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "Resetting viewModelRecents"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->currentModelRecents:Ljava/util/ArrayList;

    .line 166
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->clear()V

    .line 167
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 168
    .local v2, "r":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    new-instance v9, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;

    invoke-direct {v9, v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;-><init>(Lcom/microsoft/xbox/service/model/recents/Recent;)V

    invoke-virtual {v8, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    .end local v2    # "r":Lcom/microsoft/xbox/service/model/recents/Recent;
    .end local v3    # "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    :cond_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelRecents:Ljava/util/LinkedList;

    const/4 v9, 0x0

    invoke-static {v6, v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    .line 173
    .local v0, "newState":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 174
    .local v1, "newViewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v5, v6, :cond_1

    .line 175
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v8, "RecentsData update"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    if-nez v6, :cond_6

    .line 177
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 182
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    const/4 v4, 0x1

    .line 183
    .local v4, "stateChanged":Z
    :goto_2
    if-nez v4, :cond_2

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v6, :cond_4

    .line 184
    :cond_2
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->state:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    .line 185
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 186
    if-eqz v4, :cond_3

    .line 187
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->ensureCurrentProgressItem(Z)V

    .line 189
    :cond_3
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v7, "Updating screen adapter"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->updateScreenAdapter()V

    .line 195
    .end local v0    # "newState":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .end local v1    # "newViewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    .end local v4    # "stateChanged":Z
    .end local v5    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_4
    :goto_3
    return-void

    .line 177
    .restart local v0    # "newState":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .restart local v1    # "newViewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    .restart local v5    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 179
    :cond_6
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    :cond_7
    move v4, v7

    .line 182
    goto :goto_2

    .line 193
    .end local v0    # "newState":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .end local v1    # "newViewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    .end local v5    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_8
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->TAG:Ljava/lang/String;

    const-string v7, "not final update"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
