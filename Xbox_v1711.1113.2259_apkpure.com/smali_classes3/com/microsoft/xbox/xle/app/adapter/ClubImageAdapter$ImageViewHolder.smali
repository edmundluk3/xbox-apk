.class public Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageViewHolder"
.end annotation


# instance fields
.field public final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    .line 51
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 53
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;ILandroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;)Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;->navigateToTitle(I)V

    return-void
.end method


# virtual methods
.method public bindTo(Landroid/support/v4/util/Pair;I)V
    .locals 4
    .param p1    # Landroid/support/v4/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "item":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v0, p1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const v2, 0x7f020122

    const v3, 0x7f020115

    invoke-virtual {v1, v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v0, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;)Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    :cond_0
    return-void
.end method
