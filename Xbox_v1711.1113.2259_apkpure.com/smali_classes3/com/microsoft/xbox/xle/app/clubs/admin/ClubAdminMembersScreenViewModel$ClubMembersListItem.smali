.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
.super Ljava/lang/Object;
.source "ClubAdminMembersScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubMembersListItem"
.end annotation


# instance fields
.field private final data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

.field private volatile transient hashCode:I

.field private isModerator:Z

.field private matchesSearchTerm:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)V
    .locals 0
    .param p1, "data"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isModerator"    # Z

    .prologue
    .line 484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 487
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 488
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->isModerator:Z

    .line 489
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->isModerator:Z

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 542
    if-ne p1, p0, :cond_1

    .line 548
    :cond_0
    :goto_0
    return v1

    .line 544
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 545
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 547
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 548
    .local v0, "other":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getGamerpic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 497
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 502
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamertag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsModerator()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->isModerator:Z

    return v0
.end method

.method public declared-synchronized getMatchesSearchTerm()Z
    .locals 1

    .prologue
    .line 525
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->matchesSearchTerm:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPresenceText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 512
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->presenceText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 507
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXuid()J
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 554
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 555
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->hashCode:I

    .line 556
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->data:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->hashCode:I

    .line 559
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->hashCode:I

    return v0
.end method

.method public matchesSearchTerm(Ljava/lang/String;)Z
    .locals 2
    .param p1, "term"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 516
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 518
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 520
    .local v0, "lowerTerm":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 521
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getRealName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 520
    :goto_0
    return v1

    .line 521
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIsModerator(Z)V
    .locals 0
    .param p1, "isModerator"    # Z

    .prologue
    .line 537
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->isModerator:Z

    .line 538
    return-void
.end method

.method public declared-synchronized setMatchesSearchTerm(Z)V
    .locals 1
    .param p1, "match"    # Z

    .prologue
    .line 529
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->matchesSearchTerm:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    monitor-exit p0

    return-void

    .line 529
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
