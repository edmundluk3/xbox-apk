.class public Lcom/flipkart/chatheads/ui/ChatHead;
.super Landroid/widget/ImageView;
.source "ChatHead.java"

# interfaces
.implements Lcom/facebook/rebound/SpringListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipkart/chatheads/ui/ChatHead$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Landroid/widget/ImageView;",
        "Lcom/facebook/rebound/SpringListener;"
    }
.end annotation


# instance fields
.field final CLOSE_ATTRACTION_THRESHOLD:I

.field private final DELTA:F

.field private downTranslationX:F

.field private downTranslationY:F

.field private downX:F

.field private downY:F

.field private extras:Landroid/os/Bundle;

.field private imageView:Landroid/widget/ImageView;

.field private isDragging:Z

.field private isHero:Z

.field private isSticky:Z

.field private key:Ljava/io/Serializable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

.field private scaleSpring:Lcom/facebook/rebound/Spring;

.field private springSystem:Lcom/facebook/rebound/SpringSystem;

.field private state:Lcom/flipkart/chatheads/ui/ChatHead$State;

.field private final touchSlop:I

.field private unreadCount:I

.field private velocityTracker:Landroid/view/VelocityTracker;

.field private xPositionListener:Lcom/facebook/rebound/SpringListener;

.field private xPositionSpring:Lcom/facebook/rebound/Spring;

.field private yPositionListener:Lcom/facebook/rebound/SpringListener;

.field private yPositionSpring:Lcom/facebook/rebound/Spring;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 54
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    .line 30
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->touchSlop:I

    .line 31
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->DELTA:F

    .line 34
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    .line 37
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    .line 38
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    .line 43
    iput v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This constructor cannot be used"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    .line 30
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->touchSlop:I

    .line 31
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->DELTA:F

    .line 34
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    .line 37
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    .line 38
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    .line 43
    iput v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This constructor cannot be used"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    .line 30
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->touchSlop:I

    .line 31
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->DELTA:F

    .line 34
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    .line 37
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    .line 38
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    .line 43
    iput v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This constructor cannot be used"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/facebook/rebound/SpringSystem;Landroid/content/Context;Z)V
    .locals 4
    .param p1, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "springsHolder"    # Lcom/facebook/rebound/SpringSystem;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "isSticky"    # Z

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 69
    invoke-direct {p0, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    .line 30
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->touchSlop:I

    .line 31
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->DELTA:F

    .line 34
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    .line 37
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    .line 38
    iput v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    .line 43
    iput v3, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    .line 70
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 71
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->springSystem:Lcom/facebook/rebound/SpringSystem;

    .line 72
    iput-boolean p4, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    .line 73
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->init()V

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 105
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    new-instance v0, Lcom/flipkart/chatheads/ui/ChatHead$1;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/ChatHead$1;-><init>(Lcom/flipkart/chatheads/ui/ChatHead;)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionListener:Lcom/facebook/rebound/SpringListener;

    .line 117
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->springSystem:Lcom/facebook/rebound/SpringSystem;

    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    .line 118
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 119
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0, p0}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 121
    new-instance v0, Lcom/flipkart/chatheads/ui/ChatHead$2;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/ChatHead$2;-><init>(Lcom/flipkart/chatheads/ui/ChatHead;)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionListener:Lcom/facebook/rebound/SpringListener;

    .line 133
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->springSystem:Lcom/facebook/rebound/SpringSystem;

    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    .line 134
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 135
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0, p0}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 137
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->springSystem:Lcom/facebook/rebound/SpringSystem;

    invoke-virtual {v0}, Lcom/facebook/rebound/SpringSystem;->createSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    .line 138
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    new-instance v1, Lcom/flipkart/chatheads/ui/ChatHead$3;

    invoke-direct {v1, p0}, Lcom/flipkart/chatheads/ui/ChatHead$3;-><init>(Lcom/flipkart/chatheads/ui/ChatHead;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 147
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 148
    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 85
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getHorizontalPositionListener()Lcom/facebook/rebound/SpringListener;
    .locals 1

    .prologue
    .line 208
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionListener:Lcom/facebook/rebound/SpringListener;

    return-object v0
.end method

.method public getHorizontalSpring()Lcom/facebook/rebound/Spring;
    .locals 1

    .prologue
    .line 93
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    return-object v0
.end method

.method public getKey()Ljava/io/Serializable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 170
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->key:Ljava/io/Serializable;

    return-object v0
.end method

.method public getState()Lcom/flipkart/chatheads/ui/ChatHead$State;
    .locals 1

    .prologue
    .line 162
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->state:Lcom/flipkart/chatheads/ui/ChatHead$State;

    return-object v0
.end method

.method public getUnreadCount()I
    .locals 1

    .prologue
    .line 151
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    return v0
.end method

.method public getVerticalPositionListener()Lcom/facebook/rebound/SpringListener;
    .locals 1

    .prologue
    .line 212
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionListener:Lcom/facebook/rebound/SpringListener;

    return-object v0
.end method

.method public getVerticalSpring()Lcom/facebook/rebound/Spring;
    .locals 1

    .prologue
    .line 97
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    return-object v0
.end method

.method public isHero()Z
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isHero:Z

    return v0
.end method

.method public isSticky()Z
    .locals 1

    .prologue
    .line 101
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky:Z

    return v0
.end method

.method public onRemove()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    const/4 v1, 0x0

    .line 308
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 309
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->removeAllListeners()Lcom/facebook/rebound/Spring;

    .line 310
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->destroy()V

    .line 311
    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    .line 312
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 313
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->removeAllListeners()Lcom/facebook/rebound/Spring;

    .line 314
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->destroy()V

    .line 315
    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    .line 316
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 317
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->removeAllListeners()Lcom/facebook/rebound/Spring;

    .line 318
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->destroy()V

    .line 319
    iput-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    .line 320
    return-void
.end method

.method public onSpringActivate(Lcom/facebook/rebound/Spring;)V
    .locals 1
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 198
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onChatHeadAnimateStart(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 200
    :cond_0
    return-void
.end method

.method public onSpringAtRest(Lcom/facebook/rebound/Spring;)V
    .locals 1
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 192
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onChatHeadAnimateEnd(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 194
    :cond_0
    return-void
.end method

.method public onSpringEndStateChange(Lcom/facebook/rebound/Spring;)V
    .locals 0
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 205
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    return-void
.end method

.method public onSpringUpdate(Lcom/facebook/rebound/Spring;)V
    .locals 9
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 179
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    if-eqz v0, :cond_0

    .line 180
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    .line 181
    .local v6, "activeHorizontalSpring":Lcom/facebook/rebound/Spring;
    iget-object v7, p0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    .line 182
    .local v7, "activeVerticalSpring":Lcom/facebook/rebound/Spring;
    if-eq p1, v6, :cond_1

    if-eq p1, v7, :cond_1

    .line 188
    .end local v6    # "activeHorizontalSpring":Lcom/facebook/rebound/Spring;
    .end local v7    # "activeVerticalSpring":Lcom/facebook/rebound/Spring;
    :cond_0
    :goto_0
    return-void

    .line 184
    .restart local v6    # "activeHorizontalSpring":Lcom/facebook/rebound/Spring;
    .restart local v7    # "activeVerticalSpring":Lcom/facebook/rebound/Spring;
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v0

    invoke-virtual {v7}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-int v8, v0

    .line 185
    .local v8, "totalVelocity":I
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v0

    iget-boolean v2, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isDragging:Z

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxWidth()I

    move-result v3

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxHeight()I

    move-result v4

    move-object v1, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v8}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onSpringUpdate(Lcom/flipkart/chatheads/ui/ChatHead;ZIILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "event"    # Landroid/view/MotionEvent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 217
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-super/range {p0 .. p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    .line 304
    :goto_0
    return v2

    .line 221
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    .line 222
    .local v6, "activeHorizontalSpring":Lcom/facebook/rebound/Spring;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    .line 224
    .local v7, "activeVerticalSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 225
    .local v9, "action":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v15

    .line 226
    .local v15, "rawX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v16

    .line 227
    .local v16, "rawY":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    sub-float v11, v15, v2

    .line 228
    .local v11, "offsetX":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    sub-float v14, v16, v2

    .line 229
    .local v14, "offsetY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->shouldShowCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v17

    .line 230
    .local v17, "showCloseButton":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->getViewX(Landroid/view/View;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-interface {v3, v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->getViewY(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 231
    if-nez v9, :cond_4

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    if-nez v2, :cond_3

    .line 233
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    .line 238
    :goto_1
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 239
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v7, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 240
    sget-object v2, Lcom/flipkart/chatheads/ui/ChatHead$State;->FREE:Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flipkart/chatheads/ui/ChatHead;->setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V

    .line 241
    move-object/from16 v0, p0

    iput v15, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downX:F

    .line 242
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/flipkart/chatheads/ui/ChatHead;->downY:F

    .line 243
    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downTranslationX:F

    .line 244
    invoke-virtual {v7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downTranslationY:F

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide v18, 0x3fecccccc0000000L    # 0.8999999761581421

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 246
    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 247
    invoke-virtual {v7}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 304
    :cond_2
    :goto_2
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 235
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1

    .line 251
    :cond_4
    const/4 v2, 0x2

    if-ne v9, v2, :cond_7

    .line 252
    float-to-double v2, v11

    float-to-double v0, v14

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/flipkart/chatheads/ui/ChatHead;->touchSlop:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    cmpl-double v2, v2, v18

    if-lez v2, :cond_5

    .line 253
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->isDragging:Z

    .line 254
    if-eqz v17, :cond_5

    .line 255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->appear()V

    .line 258
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 260
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->isDragging:Z

    if-eqz v2, :cond_2

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->pointTo(FF)V

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->canDrag(Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move/from16 v0, v16

    invoke-interface {v2, v15, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDistanceCloseButtonFromHead(FF)D

    move-result-wide v12

    .line 264
    .local v12, "distanceCloseButtonFromHead":D
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    int-to-double v2, v2

    cmpg-double v2, v12, v2

    if-gez v2, :cond_6

    if-eqz v17, :cond_6

    .line 265
    sget-object v2, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flipkart/chatheads/ui/ChatHead;->setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V

    .line 266
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 267
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v7, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadCoordsForCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)[I

    move-result-object v10

    .line 269
    .local v10, "coords":[I
    const/4 v2, 0x0

    aget v2, v10, v2

    int-to-double v2, v2

    invoke-virtual {v6, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 270
    const/4 v2, 0x1

    aget v2, v10, v2

    int-to-double v2, v2

    invoke-virtual {v7, v2, v3}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->onCapture()V

    .line 282
    .end local v10    # "coords":[I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    goto/16 :goto_2

    .line 274
    :cond_6
    sget-object v2, Lcom/flipkart/chatheads/ui/ChatHead$State;->FREE:Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/flipkart/chatheads/ui/ChatHead;->setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V

    .line 275
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 276
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v7, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 277
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downTranslationX:F

    add-float/2addr v2, v11

    float-to-double v2, v2

    invoke-virtual {v6, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 278
    move-object/from16 v0, p0

    iget v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->downTranslationY:F

    add-float/2addr v2, v14

    float-to-double v2, v2

    invoke-virtual {v7, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->onRelease()V

    goto :goto_3

    .line 288
    .end local v12    # "distanceCloseButtonFromHead":D
    :cond_7
    const/4 v2, 0x1

    if-eq v9, v2, :cond_8

    const/4 v2, 0x3

    if-ne v9, v2, :cond_2

    .line 289
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/flipkart/chatheads/ui/ChatHead;->isDragging:Z

    .line 290
    .local v8, "wasDragging":Z
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 291
    sget-object v2, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v7, v2}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 292
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->isDragging:Z

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->scaleSpring:Lcom/facebook/rebound/Spring;

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    float-to-int v4, v2

    .line 295
    .local v4, "xVelocity":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    float-to-int v5, v2

    .line 296
    .local v5, "yVelocity":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    .line 297
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->velocityTracker:Landroid/view/VelocityTracker;

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->xPositionSpring:Lcom/facebook/rebound/Spring;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->yPositionSpring:Lcom/facebook/rebound/Spring;

    if-eqz v2, :cond_2

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v2

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v8}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->handleTouchUp(Lcom/flipkart/chatheads/ui/ChatHead;IILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Z)Z

    goto/16 :goto_2
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 89
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->extras:Landroid/os/Bundle;

    .line 90
    return-void
.end method

.method public setHero(Z)V
    .locals 0
    .param p1, "hero"    # Z

    .prologue
    .line 81
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iput-boolean p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->isHero:Z

    .line 82
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "chatHeadDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 323
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 324
    return-void
.end method

.method public setKey(Ljava/io/Serializable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->key:Ljava/io/Serializable;

    .line 175
    return-void
.end method

.method public setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V
    .locals 0
    .param p1, "state"    # Lcom/flipkart/chatheads/ui/ChatHead$State;

    .prologue
    .line 166
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->state:Lcom/flipkart/chatheads/ui/ChatHead$State;

    .line 167
    return-void
.end method

.method public setUnreadCount(I)V
    .locals 2
    .param p1, "unreadCount"    # I

    .prologue
    .line 155
    .local p0, "this":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    if-eq p1, v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHead;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->key:Ljava/io/Serializable;

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->reloadDrawable(Ljava/io/Serializable;)V

    .line 158
    :cond_0
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHead;->unreadCount:I

    .line 159
    return-void
.end method
