.class public final enum Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
.super Ljava/lang/Enum;
.source "ConsoleBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tab"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

.field public static final enum Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

.field public static final enum FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

.field public static final enum Snap:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    const-string v1, "Snap"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Snap:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    const-string v1, "FillOrFull"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    const-string v1, "Background"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Snap:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->FillOrFull:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->Background:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->$VALUES:[Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->$VALUES:[Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    return-object v0
.end method
