.class public final Lcom/microsoft/xbox/domain/hoverchat/RxHoverChat;
.super Ljava/lang/Object;
.source "RxHoverChat.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static getChatHeadXPositionChangedEvents(Lcom/flipkart/chatheads/ui/ChatHead;)Lio/reactivex/Observable;
    .locals 4
    .param p0, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;-><init>(Lcom/facebook/rebound/Spring;)V

    const-wide/16 v2, 0xfa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;->sample(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public static managerEvents(Lcom/flipkart/chatheads/ui/ChatHeadManager;)Lio/reactivex/Observable;
    .locals 1
    .param p0    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;)",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    return-object v0
.end method
