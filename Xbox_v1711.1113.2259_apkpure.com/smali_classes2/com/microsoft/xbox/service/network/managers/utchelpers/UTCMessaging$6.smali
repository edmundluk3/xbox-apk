.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackReadConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 156
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 159
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ConversationId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    const-string v1, "Messaging - Read Conversation"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    if-lez v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$6;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackMarkConversationRead(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 168
    :cond_0
    return-void
.end method
