.class Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;
.super Ljava/lang/Object;
.source "SuggestionsPeopleListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->updateUI(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

.field final synthetic val$spinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;->val$spinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->setSuggestionsPeopleFilter(I)V

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;->val$spinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackSuggestionsFilterAction(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;)V

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->setListPosition(II)V

    .line 246
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
