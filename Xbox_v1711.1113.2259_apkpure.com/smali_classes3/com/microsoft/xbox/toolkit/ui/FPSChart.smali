.class public Lcom/microsoft/xbox/toolkit/ui/FPSChart;
.super Landroid/view/SurfaceView;
.source "FPSChart.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final HEIGHT:I = 0x3c

.field private static final SAFE_FPS:I = 0x1e

.field private static final WIDTH:I = 0x78

.field private static fpsFrames:[I

.field private static fpsFramesIndex:I


# instance fields
.field private paint:Landroid/graphics/Paint;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private thread:Ljava/lang/Thread;

.field private threadRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    sput v2, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFramesIndex:I

    .line 35
    const/16 v1, 0x78

    new-array v1, v1, [I

    sput-object v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    .line 38
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 39
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    aput v2, v1, v0

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 48
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 49
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    .line 50
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->thread:Ljava/lang/Thread;

    .line 51
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->threadRunning:Z

    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    .line 59
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/toolkit/ui/FPSChart$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->thread:Ljava/lang/Thread;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->thread:Ljava/lang/Thread;

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->threadRunning:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/FPSChart;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/FPSChart;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/FPSChart;Landroid/graphics/Canvas;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/FPSChart;
    .param p1, "x1"    # Landroid/graphics/Canvas;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->drawCanvas(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public static addFPS(I)V
    .locals 2
    .param p0, "fps"    # I

    .prologue
    .line 44
    sget v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFramesIndex:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    sput v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFramesIndex:I

    .line 45
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    sget v1, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFramesIndex:I

    aput p0, v0, v1

    .line 46
    return-void
.end method

.method private drawCanvas(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v3, 0x42ee0000    # 119.0f

    const/high16 v4, 0x426c0000    # 59.0f

    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    const/high16 v2, 0x41e80000    # 29.0f

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 123
    sget v7, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFramesIndex:I

    .line 125
    .local v7, "frameIndex":I
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    array-length v0, v0

    if-ge v8, v0, :cond_1

    .line 128
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    aget v6, v0, v7

    .line 129
    .local v6, "fps":I
    rsub-int/lit8 v0, v6, 0x3b

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 131
    .local v9, "top":I
    int-to-float v1, v8

    int-to-float v2, v9

    add-int/lit8 v0, v8, 0x1

    int-to-float v3, v0

    const/high16 v4, 0x42c80000    # 100.0f

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 134
    add-int/lit8 v7, v7, 0x1

    .line 135
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->fpsFrames:[I

    array-length v0, v0

    if-ne v7, v0, :cond_0

    .line 136
    const/4 v7, 0x0

    .line 126
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 139
    .end local v6    # "fps":I
    .end local v9    # "top":I
    :cond_1
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    .line 111
    .local v0, "c":Landroid/graphics/Canvas;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->drawCanvas(Landroid/graphics/Canvas;)V

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 113
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onMeasure(II)V

    .line 83
    const/16 v0, 0x78

    const/16 v1, 0x3c

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->setMeasuredDimension(II)V

    .line 84
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 106
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->threadRunning:Z

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 101
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->threadRunning:Z

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/FPSChart;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    goto :goto_0
.end method
