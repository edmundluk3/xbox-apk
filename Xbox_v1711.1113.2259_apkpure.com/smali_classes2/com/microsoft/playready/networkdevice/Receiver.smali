.class public Lcom/microsoft/playready/networkdevice/Receiver;
.super Ljava/lang/Object;
.source "Receiver.java"


# static fields
.field private static final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private static s_activator:Lcom/microsoft/playready/IPRActivator;

.field private static s_isActivated:Z

.field private static s_syncObject:Ljava/lang/Object;


# instance fields
.field private final mFactory:Lcom/microsoft/playready/networkdevice/IPRNDFactory;

.field private mReceiverPlugin:Lcom/microsoft/playready/networkdevice/IReceiverPlugin;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_isActivated:Z

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_activator:Lcom/microsoft/playready/IPRActivator;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_syncObject:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;)V
    .locals 2
    .param p1, "prFactory"    # Lcom/microsoft/playready/networkdevice/IPRNDFactory;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mFactory:Lcom/microsoft/playready/networkdevice/IPRNDFactory;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mReceiverPlugin:Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    .line 46
    sget-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_activator:Lcom/microsoft/playready/IPRActivator;

    if-nez v0, :cond_1

    .line 48
    sget-object v1, Lcom/microsoft/playready/networkdevice/Receiver;->s_syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 50
    :try_start_0
    sget-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_activator:Lcom/microsoft/playready/IPRActivator;

    if-nez v0, :cond_0

    .line 52
    invoke-interface {p1}, Lcom/microsoft/playready/networkdevice/IPRNDFactory;->createPRActivator()Lcom/microsoft/playready/IPRActivator;

    move-result-object v0

    sput-object v0, Lcom/microsoft/playready/networkdevice/Receiver;->s_activator:Lcom/microsoft/playready/IPRActivator;

    .line 54
    :cond_0
    monitor-exit v1

    .line 56
    :cond_1
    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method getPlayReadyFactory()Lcom/microsoft/playready/networkdevice/IPRNDFactory;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mFactory:Lcom/microsoft/playready/networkdevice/IPRNDFactory;

    return-object v0
.end method

.method getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mReceiverPlugin:Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    return-object v0
.end method

.method public setReceiverPlugin(Lcom/microsoft/playready/networkdevice/IReceiverPlugin;)V
    .locals 0
    .param p1, "plugin"    # Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mReceiverPlugin:Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    .line 67
    return-void
.end method

.method public startSession(Lcom/microsoft/playready/networkdevice/TransmitterInfo;)Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;
    .locals 12
    .param p1, "transmitter"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    .prologue
    .line 83
    const-string v6, "Receiver"

    const-string v7, "starting session"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    sget-boolean v6, Lcom/microsoft/playready/networkdevice/Receiver;->s_isActivated:Z

    if-nez v6, :cond_1

    .line 87
    sget-object v7, Lcom/microsoft/playready/networkdevice/Receiver;->s_syncObject:Ljava/lang/Object;

    monitor-enter v7

    .line 89
    :try_start_0
    sget-boolean v6, Lcom/microsoft/playready/networkdevice/Receiver;->s_isActivated:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_0

    .line 93
    :try_start_1
    sget-object v6, Lcom/microsoft/playready/networkdevice/Receiver;->s_activator:Lcom/microsoft/playready/IPRActivator;

    invoke-interface {v6}, Lcom/microsoft/playready/IPRActivator;->activateAsync()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 94
    .local v0, "activator":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 95
    const/4 v6, 0x1

    sput-boolean v6, Lcom/microsoft/playready/networkdevice/Receiver;->s_isActivated:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    .end local v0    # "activator":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Void;>;"
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 113
    :cond_1
    new-instance v5, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    const/4 v6, 0x0

    invoke-direct {v5, p0, p1, v6}, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;-><init>(Lcom/microsoft/playready/networkdevice/Receiver;Lcom/microsoft/playready/networkdevice/TransmitterInfo;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    .line 114
    .local v5, "worker":Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;
    new-instance v4, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;

    invoke-direct {v4, v5}, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;-><init>(Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;)V

    .line 116
    .local v4, "task":Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;
    sget-object v6, Lcom/microsoft/playready/networkdevice/Receiver;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 118
    return-object v4

    .line 97
    .end local v4    # "task":Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;
    .end local v5    # "worker":Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;
    :catch_0
    move-exception v1

    .line 99
    .local v1, "ex":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v6, "Receiver"

    const-string v8, "startSession interrupted"

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    .end local v1    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 101
    :catch_1
    move-exception v2

    .line 103
    .local v2, "ex2":Ljava/util/concurrent/ExecutionException;
    :try_start_4
    const-string v6, "Receiver"

    const-string v8, "Activation failure: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    .end local v2    # "ex2":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v3

    .line 107
    .local v3, "ex3":Ljava/lang/Exception;
    const-string v6, "Receiver"

    const-string v8, "Activation failure: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public updateRevocationList(Ljava/lang/String;)Z
    .locals 4
    .param p1, "revocationList"    # Ljava/lang/String;

    .prologue
    .line 134
    const/4 v1, 0x0

    .line 136
    .local v1, "result":Z
    new-instance v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/Receiver;->mFactory:Lcom/microsoft/playready/networkdevice/IPRNDFactory;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V

    .line 138
    .local v0, "proxy":Lcom/microsoft/playready/networkdevice/PrndDrmProxy;
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->receiverUpdateRevocationList(Ljava/lang/String;)Z

    move-result v1

    .line 143
    :cond_0
    return v1
.end method
