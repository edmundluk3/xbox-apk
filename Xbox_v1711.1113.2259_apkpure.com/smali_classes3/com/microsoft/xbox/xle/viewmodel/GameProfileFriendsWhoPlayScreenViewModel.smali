.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;
.source "GameProfileFriendsWhoPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;
    }
.end annotation


# static fields
.field private static final TWITCH_BROADCASTING_IMAGE_URL_FORMAT:Ljava/lang/String; = "https://static-cdn.jtvnw.net/previews-ttv/live_user_%s-1280x720.jpg"


# instance fields
.field private broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

.field private broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

.field private loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 41
    return-void
.end method

.method private getBroadcasterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBroadcasterProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBroadcatingImageUrl()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    if-eqz v0, :cond_1

    .line 78
    const-string v0, "Twitch"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "https://static-cdn.jtvnw.net/previews-ttv/live_user_%s-1280x720.jpg"

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://thumbs.beam.pro/channel/%s.small.jpg"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getGameTitleId()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method public getSpotlightData()Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getBroadcasterName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getBroadcatingImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getBroadcasterProvider()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hasSpotlight()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isViewingBroadcastsRestricted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchBroadcast()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 95
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v1, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    .line 96
    .local v1, "provider":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    iget-object v0, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    .line 98
    .local v0, "broadcastId":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 99
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v3

    const-string v4, "Watch Broadcast"

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    long-to-int v5, v6

    invoke-virtual {v3, v4, v2, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    :goto_2
    return-void

    .end local v0    # "broadcastId":Ljava/lang/String;
    .end local v1    # "provider":Ljava/lang/String;
    :cond_0
    move-object v1, v2

    .line 95
    goto :goto_0

    .restart local v1    # "provider":Ljava/lang/String;
    :cond_1
    move-object v0, v2

    .line 96
    goto :goto_1

    .line 102
    .restart local v0    # "broadcastId":Ljava/lang/String;
    :cond_2
    const-string v2, "Attempted to launch a broadcast without the required info."

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->cancel()V

    .line 137
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->load(Z)V

    .line 139
    return-void
.end method

.method protected onLoadGameProfileFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v1, 0x0

    .line 107
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 129
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->updateAdapter()V

    .line 130
    return-void

    .line 111
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    .line 112
    .local v0, "m":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getResult(J)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->peopleList:Ljava/util/ArrayList;

    .line 113
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getBroadcaster(J)Lcom/microsoft/xbox/service/model/FollowersData;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 114
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    const/4 v2, 0x0

    .line 115
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 123
    .end local v0    # "m":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;
    :pswitch_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->peopleList:Ljava/util/ArrayList;

    .line 124
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 125
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    .line 126
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->cancel()V

    .line 61
    :cond_0
    return-void
.end method
