.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
.source "$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse.java"


# instance fields
.field private final clubs:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;"
        }
    .end annotation
.end field

.field private final maximumOpenAndClosedClubs:I

.field private final maximumSecretClubs:I

.field private final owner:Ljava/lang/String;

.field private final remainingOpenAndClosedClubs:I

.field private final remainingSecretClubs:I


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;IIII)V
    .locals 2
    .param p1, "owner"    # Ljava/lang/String;
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "remainingOpenAndClosedClubs"    # I
    .param p4, "remainingSecretClubs"    # I
    .param p5, "maximumOpenAndClosedClubs"    # I
    .param p6, "maximumSecretClubs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;IIII)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "clubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;-><init>()V

    .line 26
    if-nez p1, :cond_0

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null owner"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->owner:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    .line 31
    iput p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingOpenAndClosedClubs:I

    .line 32
    iput p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingSecretClubs:I

    .line 33
    iput p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumOpenAndClosedClubs:I

    .line 34
    iput p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumSecretClubs:I

    .line 35
    return-void
.end method


# virtual methods
.method public clubs()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    if-ne p1, p0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 87
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    .line 88
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->owner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_3

    .line 89
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingOpenAndClosedClubs:I

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingOpenAndClosedClubs()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingSecretClubs:I

    .line 91
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingSecretClubs()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumOpenAndClosedClubs:I

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumOpenAndClosedClubs()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumSecretClubs:I

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumSecretClubs()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 89
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    :cond_4
    move v1, v2

    .line 95
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 100
    const/4 v0, 0x1

    .line 101
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->owner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 103
    mul-int/2addr v0, v2

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 105
    mul-int/2addr v0, v2

    .line 106
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingOpenAndClosedClubs:I

    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v2

    .line 108
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingSecretClubs:I

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v2

    .line 110
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumOpenAndClosedClubs:I

    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v2

    .line 112
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumSecretClubs:I

    xor-int/2addr v0, v1

    .line 113
    return v0

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public maximumOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumOpenAndClosedClubs:I

    return v0
.end method

.method public maximumSecretClubs()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumSecretClubs:I

    return v0
.end method

.method public owner()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->owner:Ljava/lang/String;

    return-object v0
.end method

.method public remainingOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingOpenAndClosedClubs:I

    return v0
.end method

.method public remainingSecretClubs()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingSecretClubs:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubsByOwnerResponse{owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->clubs:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainingOpenAndClosedClubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingOpenAndClosedClubs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainingSecretClubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->remainingSecretClubs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maximumOpenAndClosedClubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumOpenAndClosedClubs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maximumSecretClubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;->maximumSecretClubs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
