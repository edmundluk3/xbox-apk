.class public Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ClubHomeScreenPlayersListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 25
    if-nez p2, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 27
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030085

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 30
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    const v6, 0x7f0e035e

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 31
    .local v1, "image":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    const v6, 0x7f0e035f

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    .local v3, "more":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;

    .line 34
    .local v2, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;
    instance-of v6, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;

    if-eqz v6, :cond_1

    .line 35
    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;

    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;->userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v4

    .line 36
    .local v4, "user":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v4}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 37
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 44
    .end local v4    # "user":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :goto_0
    return-object p2

    .line 39
    .restart local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;
    :cond_1
    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;

    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;->count()I

    move-result v0

    .line 40
    .local v0, "count":I
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0705ff

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {v3, v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0
.end method
