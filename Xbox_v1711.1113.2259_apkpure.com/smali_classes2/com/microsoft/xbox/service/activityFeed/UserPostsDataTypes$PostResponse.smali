.class public abstract Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
.super Ljava/lang/Object;
.source "UserPostsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PostResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract postAuthor()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract postDate()Ljava/util/Date;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract postId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract postText()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract postType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract postTypeData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract postUri()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract timelines()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;"
        }
    .end annotation
.end method
