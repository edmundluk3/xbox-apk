.class Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchLfgLanguagesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field languages:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;)V
    .locals 0

    .prologue
    .line 680
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 681
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 700
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/service/editorial/EditorialService;->getLfgLanguages()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->languages:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 706
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 701
    :catch_0
    move-exception v0

    .line 702
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Caught an exception fetching LFG language list"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 703
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 678
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 694
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 678
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 690
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 715
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->languages:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->languages:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 716
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 717
    .local v0, "languagesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;>;"
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "all"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 718
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->languages:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 720
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$502(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    .line 722
    .end local v0    # "languagesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;>;"
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 678
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 711
    return-void
.end method
