.class public Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "TvTrendingScreenAdapter.java"


# instance fields
.field private liveTop10Items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;",
            ">;"
        }
    .end annotation
.end field

.field private liveTop10ListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

.field private liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private popularVodItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private popularVodListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

.field private popularVodListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 37
    const v0, 0x7f0e0b52

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->screenBody:Landroid/view/View;

    .line 38
    const v0, 0x7f0e0b53

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    .line 41
    const v0, 0x7f0e0b54

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 42
    const v0, 0x7f0e0b55

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03024c

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03023f

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->updateLoadingIndicator(Z)V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->getLiveTop10Data()Ljava/util/List;

    move-result-object v1

    .line 72
    .local v1, "newTop10Data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;>;"
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10Items:Ljava/util/List;

    if-eq v2, v1, :cond_0

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;->clear()V

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;->addAll(Ljava/util/Collection;)V

    .line 75
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10ListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->onDataUpdated()V

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->restorePosition()V

    .line 78
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->liveTop10Items:Ljava/util/List;

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->getPopularVodData()Ljava/util/List;

    move-result-object v0

    .line 82
    .local v0, "newPopularVodData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;>;"
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodItems:Ljava/util/List;

    if-eq v2, v0, :cond_1

    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;->clear()V

    .line 84
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingVODListAdapter;->addAll(Ljava/util/Collection;)V

    .line 85
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->onDataUpdated()V

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->restorePosition()V

    .line 88
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;->popularVodItems:Ljava/util/List;

    .line 90
    :cond_1
    return-void
.end method
