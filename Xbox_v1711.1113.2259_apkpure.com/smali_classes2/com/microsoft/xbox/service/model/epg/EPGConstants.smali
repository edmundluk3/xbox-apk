.class public Lcom/microsoft/xbox/service/model/epg/EPGConstants;
.super Ljava/lang/Object;
.source "EPGConstants.java"


# static fields
.field public static final AppChannelTypeName:Ljava/lang/String; = "App"

.field public static final ChannelFavoritedActionName:Ljava/lang/String; = "ChannelFavorited"

.field public static final ChannelUnfavoritedActionName:Ljava/lang/String; = "ChannelUnfavorited"

.field public static final EpgChannelTypeName:Ljava/lang/String; = "EPG"

.field public static final EpgLoadActionName:Ljava/lang/String; = "EpgLoad"

.field public static final EpgLoadPerformanceName:Ljava/lang/String; = "EPGLoad"

.field public static final ErrorMalformedHeadendCode:Ljava/lang/String; = "-1"

.field public static final ErrorMalformedHeadendName:Ljava/lang/String; = "OneGuide Headend"

.field public static final HDMIInputHeadendId:Ljava/lang/String; = "ba5eba11-dea1-4bad-ba11-feddeadfab1e"

.field public static final HeadendCountActionName:Ljava/lang/String; = "Headend Count"

.field public static final ProviderChangedActionName:Ljava/lang/String; = "Provider Change"

.field public static final ShowDetailsActionName:Ljava/lang/String; = "ShowDetails"

.field public static final ShowExpandedActionName:Ljava/lang/String; = "ShowExpanded"

.field public static final ShowTunedActionName:Ljava/lang/String; = "ShowTuned"

.field public static final StreamCloseActionName:Ljava/lang/String; = "Stream Close"

.field public static final StreamExpandPipActionName:Ljava/lang/String; = "Stream Expand PIP"

.field public static final StreamForceCloseActionName:Ljava/lang/String; = "Stream ForceClose"

.field public static final StreamFullScreenPageViewName:Ljava/lang/String; = "Stream FullScreen"

.field public static final StreamLaunchActionName:Ljava/lang/String; = "Stream Launch"

.field public static final StreamLiveActionName:Ljava/lang/String; = "Stream FullScreen Live"

.field public static final StreamOneGuideActionName:Ljava/lang/String; = "Stream FullScreen OneGuide"

.field public static final StreamPauseActionName:Ljava/lang/String; = "Stream FullScreen Pause"

.field public static final StreamPlayActionName:Ljava/lang/String; = "Stream FullScreen Play"

.field public static final StreamQualityPicker:Ljava/lang/String; = "Stream Quality Picker"

.field public static final StreamScrubberActionName:Ljava/lang/String; = "Stream FullScreen Scrubber"

.field public static final StreamSeekPerformanceName:Ljava/lang/String; = "Stream Seek"

.field public static final StreamSkipBackActionName:Ljava/lang/String; = "Stream FullScreen SkipBack"

.field public static final StreamSkipForwardActionName:Ljava/lang/String; = "Stream FullScreen SkipForward"

.field public static final UrcDisabledPageViewName:Ljava/lang/String; = "URC Disabled"

.field public static final UrcPageViewName:Ljava/lang/String; = "URC"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
