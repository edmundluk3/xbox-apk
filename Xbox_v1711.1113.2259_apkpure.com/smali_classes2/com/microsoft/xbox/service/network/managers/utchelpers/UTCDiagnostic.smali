.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;
.super Ljava/lang/Object;
.source "UTCDiagnostic.java"


# static fields
.field private static final CLIENTDIAGNOSTICVERSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 45
    return-void
.end method

.method public static trackDefaultHomePage(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 1
    .param p0, "homeScreenPreference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$2;-><init>(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 57
    return-void
.end method
