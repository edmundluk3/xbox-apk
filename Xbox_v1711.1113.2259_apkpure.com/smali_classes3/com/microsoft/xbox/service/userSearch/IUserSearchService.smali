.class public interface abstract Lcom/microsoft/xbox/service/userSearch/IUserSearchService;
.super Ljava/lang/Object;
.source "IUserSearchService.java"


# virtual methods
.method public abstract clubSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract liveSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
