.class Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubChatModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebSocketConnectAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)V
    .locals 0

    .prologue
    .line 651
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;

    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$100(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/model/chat/ChatManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$100(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/model/chat/ChatManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connect()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 664
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 659
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$200(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 660
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 681
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$200(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 684
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 651
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 674
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WebSocketConnectAsyncTask connecting"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    return-void
.end method
