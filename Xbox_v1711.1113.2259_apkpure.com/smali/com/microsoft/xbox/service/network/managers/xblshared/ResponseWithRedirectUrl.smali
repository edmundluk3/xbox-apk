.class public final Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
.super Ljava/lang/Object;
.source "ResponseWithRedirectUrl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field public final redirectUrl:Ljava/lang/String;

.field public final response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final result:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/util/List;Ljava/lang/Object;)V
    .locals 0
    .param p1, "result"    # Z
    .param p2, "redirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;TT;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "this":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;, "Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl<TT;>;"
    .local p3, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p4, "response":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 14
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    .line 16
    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->response:Ljava/lang/Object;

    .line 17
    return-void
.end method
