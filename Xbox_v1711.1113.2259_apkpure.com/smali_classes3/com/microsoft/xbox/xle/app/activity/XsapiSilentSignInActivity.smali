.class public Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "XsapiSilentSignInActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "XsapiSilentSignIn"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->onCreateContentView()V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setDesiredDrawerLockState(I)V

    .line 35
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    const v0, 0x7f03027e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setContentView(I)V

    .line 47
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setAppBarLayout(IZZ)V

    .line 48
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setShowUtilityBar(Z)V

    .line 49
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setShowRightPane(Z)V

    .line 50
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 40
    return-void
.end method

.method public onRehydrateOverride()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 55
    return-void
.end method

.method public setDisableNavigationOnUpdate(Z)V
    .locals 2
    .param p1, "shouldDisable"    # Z

    .prologue
    .line 20
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;

    if-eqz v1, :cond_0

    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;

    .line 22
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->setDisableNavigationOnUpdate(Z)V

    .line 24
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;
    :cond_0
    return-void
.end method
