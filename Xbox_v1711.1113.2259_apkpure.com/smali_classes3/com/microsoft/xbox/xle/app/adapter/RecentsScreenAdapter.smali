.class public Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "RecentsScreenAdapter.java"


# instance fields
.field private final listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

.field private recentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation
.end field

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->recentsList:Ljava/util/ArrayList;

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    .line 28
    const v0, 0x7f0e09a4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    const v0, 0x7f0e09a6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 30
    const v0, 0x7f0e09a5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301e2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->recentsList:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/MainActivity;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v0

    .line 46
    .local v0, "newRecentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->recentsList:Ljava/util/ArrayList;

    if-eq v0, v1, :cond_0

    .line 47
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->recentsList:Ljava/util/ArrayList;

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->clear()V

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->recentsList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 52
    :cond_0
    return-void
.end method
