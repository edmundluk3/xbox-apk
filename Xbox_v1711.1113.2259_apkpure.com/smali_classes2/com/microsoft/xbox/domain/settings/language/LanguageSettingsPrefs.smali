.class public abstract Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
.super Ljava/lang/Object;
.source "LanguageSettingsPrefs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsPrefs$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsPrefs$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .locals 1
    .param p0, "language"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "location"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 22
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsPrefs;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsPrefs;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V

    return-object v0
.end method


# virtual methods
.method public abstract currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
