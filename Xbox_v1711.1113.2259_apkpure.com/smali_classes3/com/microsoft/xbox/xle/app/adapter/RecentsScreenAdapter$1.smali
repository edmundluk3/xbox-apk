.class Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter$1;
.super Ljava/lang/Object;
.source "RecentsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 37
    .local v0, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Clicked on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    .line 38
    return-void
.end method
