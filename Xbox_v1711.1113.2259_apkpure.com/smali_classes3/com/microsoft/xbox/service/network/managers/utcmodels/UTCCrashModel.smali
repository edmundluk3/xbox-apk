.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
.super Ljava/lang/Object;
.source "UTCCrashModel.java"


# static fields
.field public static final CRASHVERSION:I = 0x1

.field public static final MAXCALLSTACKLENGTH:I = 0xc8

.field public static final UNKNOWNCALLSTACK:Ljava/lang/String; = "unknown"

.field private static final logFolder:Ljava/lang/String;

.field private static final logName:Ljava/lang/String; = "telemetrycrashstorage"

.field private static syncObject:Ljava/lang/Object;


# instance fields
.field private appVersion:Ljava/lang/String;

.field private callStack:Ljava/lang/String;

.field private errorMessage:Ljava/lang/String;

.field private exception:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->syncObject:Ljava/lang/Object;

    .line 30
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->logFolder:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->callStack:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->errorMessage:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->appVersion:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->callStack:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->errorMessage:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->appVersion:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->gatherData()V

    .line 49
    return-void
.end method

.method public static delete()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 135
    :try_start_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s/%s.log"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->logFolder:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "telemetrycrashstorage"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 138
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CrashLog"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "An error occurred deleting the old crash log. %s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static exists()Z
    .locals 7

    .prologue
    .line 176
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s/%s.log"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->logFolder:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "telemetrycrashstorage"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "fileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 178
    .local v0, "crashFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    return v2
.end method

.method private gatherData()V
    .locals 6

    .prologue
    .line 185
    const-string v0, "CAUSE STACK TRACE\n"

    .line 186
    .local v0, "CAUSEHEADER":Ljava/lang/String;
    const-string v1, "MAIN THREAD STACK TRACE\n"

    .line 187
    .local v1, "MAINHEADER":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 189
    .local v3, "cause":Ljava/lang/Throwable;
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    if-eqz v4, :cond_1

    .line 190
    const-string v2, ""

    .line 192
    .local v2, "callstack":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 193
    const-string v2, "CAUSE STACK TRACE\n"

    .line 194
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getStackData(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 197
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MAIN THREAD STACK TRACE\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getStackData(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->setCallStack(Ljava/lang/String;)V

    .line 200
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->setErrorMessage(Ljava/lang/String;)V

    .line 203
    .end local v2    # "callstack":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->setAppVersion(Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method private getStackData(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 13
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    const/4 v6, 0x0

    .line 213
    const/16 v0, 0xa

    .line 214
    .local v0, "MAXLINES":I
    const-string v4, ""

    .line 215
    .local v4, "text":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 216
    const/4 v1, 0x0

    .line 217
    .local v1, "count":I
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v7

    array-length v8, v7

    move v5, v6

    :goto_0
    if-ge v5, v8, :cond_0

    aget-object v2, v7, v5

    .line 218
    .local v2, "elem":Ljava/lang/StackTraceElement;
    add-int/lit8 v1, v1, 0x1

    .line 219
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "temp":Ljava/lang/String;
    const-string v9, ""

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 221
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 226
    :goto_1
    const/16 v9, 0x9

    if-le v1, v9, :cond_2

    .line 231
    .end local v1    # "count":I
    .end local v2    # "elem":Ljava/lang/StackTraceElement;
    .end local v3    # "temp":Ljava/lang/String;
    :cond_0
    return-object v4

    .line 223
    .restart local v1    # "count":I
    .restart local v2    # "elem":Ljava/lang/StackTraceElement;
    .restart local v3    # "temp":Ljava/lang/String;
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "\n%s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v3, v12, v6

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 217
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public static loadMostRecentCrashData()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    .locals 12

    .prologue
    .line 152
    const/4 v4, 0x0

    .line 153
    .local v4, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    const/4 v5, 0x0

    .line 155
    .local v5, "stream":Ljava/io/FileInputStream;
    :try_start_0
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s/%s.log"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget-object v11, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->logFolder:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "telemetrycrashstorage"

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v1, "crashFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 158
    const/4 v4, 0x0

    .line 167
    .end local v1    # "crashFile":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    :goto_0
    return-object v4

    .line 160
    .restart local v1    # "crashFile":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v4    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    :cond_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .local v6, "stream":Ljava/io/FileInputStream;
    :try_start_1
    const-class v7, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;

    move-object v4, v0

    .line 162
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v6

    .line 165
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 163
    .end local v1    # "crashFile":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 164
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    const-string v7, "CrashLog"

    const-string v8, "An error occurred loading the crash log"

    invoke-static {v7, v8, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 163
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "crashFile":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_1
.end method


# virtual methods
.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->appVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getCallStack()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->callStack:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstCallstackFrame()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 240
    const-string v1, "unknown"

    .line 241
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v0, v2, v4

    .line 243
    .local v0, "elem":Ljava/lang/StackTraceElement;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    const-string v2, "%s (Line:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 247
    const-string v2, "[^a-zA-Z0-9.()]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 251
    .end local v0    # "elem":Ljava/lang/StackTraceElement;
    :cond_0
    return-object v1
.end method

.method public saveToFile()V
    .locals 12

    .prologue
    .line 110
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->syncObject:Ljava/lang/Object;

    monitor-enter v6

    .line 111
    const/4 v3, 0x0

    .line 113
    .local v3, "stream":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s/%s.log"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->logFolder:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "telemetrycrashstorage"

    aput-object v10, v8, v9

    invoke-static {v5, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 118
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 119
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .local v4, "stream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 121
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 122
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v4

    .line 127
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_2
    monitor-exit v6

    .line 128
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v5, "CrashLog"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "An error occurred saving the crash log. %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "fileName":Ljava/lang/String;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 124
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->appVersion:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setCallStack(Ljava/lang/String;)V
    .locals 0
    .param p1, "callStack"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->callStack:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->errorMessage:Ljava/lang/String;

    .line 103
    return-void
.end method
