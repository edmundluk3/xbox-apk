.class Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1;
.super Ljava/lang/Object;
.source "URCViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 2
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 167
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewModel$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 174
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connection_error"    # Ljava/lang/String;

    .prologue
    .line 163
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 179
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 184
    return-void
.end method
