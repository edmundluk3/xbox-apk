.class public interface abstract Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;
.super Ljava/lang/Object;
.source "ProfileColorService.java"


# virtual methods
.method public abstract getProfileColor(Ljava/lang/String;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "public/content/ppl/colors/{id}.json"
    .end annotation
.end method

.method public abstract getProfileColorList()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileColorList;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "public/content/ppl/colors/colorsmanifest.json"
    .end annotation
.end method
