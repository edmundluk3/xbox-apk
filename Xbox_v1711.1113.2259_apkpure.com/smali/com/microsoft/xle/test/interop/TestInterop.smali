.class public Lcom/microsoft/xle/test/interop/TestInterop;
.super Ljava/lang/Object;
.source "TestInterop.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;,
        Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;
    }
.end annotation


# static fields
.field private static TestHooks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xle/test/interop/TestHookName;",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private static activityReady:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

.field private static activityReadyMonitor:Ljava/lang/Object;

.field private static allPivotPageStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private static allowDismissSoftKeyboard:Z

.field private static automaticSignInDelegate:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Landroid/webkit/WebView;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static automaticSigninUserIndex:I

.field private static clientId:Ljava/lang/String;

.field private static companionDebugAllowedUrls:Ljava/lang/String;

.field private static companionDebugTitleId:Ljava/lang/String;

.field private static companionDebugUrl:Ljava/lang/String;

.field private static currentSandboxId:Ljava/lang/String;

.field private static currentVer:I

.field private static dismissSoftKeyboardCallback:Lcom/microsoft/xle/test/interop/delegates/Action;

.field private static enableLogging:Ljava/lang/Boolean;

.field private static errorOnStart:Z

.field private static hasLaunchedCompanion:Ljava/lang/Boolean;

.field private static hasTappedLaunch:Ljava/lang/Boolean;

.field private static isChild:Z

.field private static isGold:Z

.field private static isMonkey:Ljava/lang/Boolean;

.field private static latestVer:I

.field private static loginReturnUrl:Ljava/lang/String;

.field private static loginStateError:Ljava/lang/String;

.field private static markerNameToTrigger:Ljava/lang/String;

.field private static markerTrigger:Lcom/microsoft/xbox/toolkit/Ready;

.field private static minVer:I

.field private static monitorLPS:Z

.field private static onGetMediaStateAction:Lcom/microsoft/xle/test/interop/delegates/Action;

.field private static onLoginStateChangedCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/XLEException;",
            ">;"
        }
    .end annotation
.end field

.field private static onServiceManagerActivityCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;",
            ">;"
        }
    .end annotation
.end field

.field private static overrideChildSetting:Z

.field private static overrideLoginState:Z

.field private static overrideMembershipLevel:Z

.field private static pageToApplyError:Ljava/lang/String;

.field private static showConnectDialogOnLaunch:Ljava/lang/Boolean;

.field private static signinAutomatically:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->monitorLPS:Z

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->DISABLED:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->activityReady:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    .line 61
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivityCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 63
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->dismissSoftKeyboardCallback:Lcom/microsoft/xle/test/interop/delegates/Action;

    .line 65
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->allowDismissSoftKeyboard:Z

    .line 70
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->signinAutomatically:Z

    .line 71
    sput v1, Lcom/microsoft/xle/test/interop/TestInterop;->automaticSigninUserIndex:I

    .line 72
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->onLoginStateChangedCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 73
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->overrideLoginState:Z

    .line 74
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->errorOnStart:Z

    .line 75
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->pageToApplyError:Ljava/lang/String;

    .line 77
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->automaticSignInDelegate:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 80
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->onGetMediaStateAction:Lcom/microsoft/xle/test/interop/delegates/Action;

    .line 139
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->isGold:Z

    .line 140
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->overrideMembershipLevel:Z

    .line 158
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->isChild:Z

    .line 159
    sput-boolean v1, Lcom/microsoft/xle/test/interop/TestInterop;->overrideChildSetting:Z

    .line 187
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->allPivotPageStates:Ljava/util/HashMap;

    .line 240
    sput v3, Lcom/microsoft/xle/test/interop/TestInterop;->minVer:I

    .line 241
    sput v3, Lcom/microsoft/xle/test/interop/TestInterop;->latestVer:I

    .line 242
    sput v3, Lcom/microsoft/xle/test/interop/TestInterop;->currentVer:I

    .line 390
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->TestHooks:Ljava/util/HashMap;

    .line 427
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->isMonkey:Ljava/lang/Boolean;

    .line 443
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->companionDebugUrl:Ljava/lang/String;

    .line 458
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->hasLaunchedCompanion:Ljava/lang/Boolean;

    .line 473
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->hasTappedLaunch:Ljava/lang/Boolean;

    .line 488
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->companionDebugTitleId:Ljava/lang/String;

    .line 503
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->companionDebugAllowedUrls:Ljava/lang/String;

    .line 518
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerTrigger:Lcom/microsoft/xbox/toolkit/Ready;

    .line 519
    const-string v0, ""

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerNameToTrigger:Ljava/lang/String;

    .line 540
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->enableLogging:Ljava/lang/Boolean;

    .line 555
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->showConnectDialogOnLaunch:Ljava/lang/Boolean;

    .line 571
    sput-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->currentSandboxId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static AutoLoginManualOverride()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static DoAutomaticSignin(Landroid/webkit/WebView;)V
    .locals 2
    .param p0, "view"    # Landroid/webkit/WebView;

    .prologue
    .line 305
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 312
    return-void

    .line 305
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getCompanionDebugAllowedUrls()Ljava/lang/String;
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getCompanionDebugTitleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getCompanionDebugUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getCurrentVersion(I)I
    .locals 0
    .param p0, "currentVersion"    # I

    .prologue
    .line 262
    return p0
.end method

.method public static getDismissSoftKeyboard()Lcom/microsoft/xle/test/interop/delegates/Action;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getHasTappedLaunch()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getIsMonkey()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getLatestVersionAvailable(I)I
    .locals 0
    .param p0, "currentLatestVersion"    # I

    .prologue
    .line 255
    return p0
.end method

.method public static getLoggingEnabled()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 546
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getLoginErrorOnPageFinish(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "currentPage"    # Ljava/lang/String;
    .param p1, "currentState"    # Ljava/lang/String;

    .prologue
    .line 362
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 370
    return-object p1

    .line 362
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLoginErrorOnPageStart(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "currentPage"    # Ljava/lang/String;
    .param p1, "currentState"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 358
    return-object p1

    .line 350
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLoginReturnUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getMembershipLevel(Z)Z
    .locals 0
    .param p0, "currentIsGold"    # Z

    .prologue
    .line 155
    return p0
.end method

.method public static getMinimumVersionRequired(I)I
    .locals 0
    .param p0, "currentMinimumVersion"    # I

    .prologue
    .line 248
    return p0
.end method

.method public static getMonitorLPS()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public static getPageStates()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getSandoxId(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "current"    # Ljava/lang/String;

    .prologue
    .line 585
    return-object p0
.end method

.method private static declared-synchronized getTestHooks()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xle/test/interop/TestHookName;",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 393
    const-class v0, Lcom/microsoft/xle/test/interop/TestInterop;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->TestHooks:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getTotalPages()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 219
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 228
    return v1

    :cond_0
    move v0, v1

    .line 219
    goto :goto_0
.end method

.method public static getUserChildSetting(Z)Z
    .locals 0
    .param p0, "current"    # Z

    .prologue
    .line 177
    return p0
.end method

.method public static hasLaunchedCompanionDebug()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private static notifyTestHook(Lcom/microsoft/xle/test/interop/TestHookName;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "name"    # Lcom/microsoft/xle/test/interop/TestHookName;
    .param p1, "isCompleted"    # Ljava/lang/Boolean;

    .prologue
    .line 402
    return-void
.end method

.method public static notifyTestHookComplete(Lcom/microsoft/xle/test/interop/TestHookName;)V
    .locals 0
    .param p0, "name"    # Lcom/microsoft/xle/test/interop/TestHookName;

    .prologue
    .line 408
    return-void
.end method

.method public static notifyTestHookStart(Lcom/microsoft/xle/test/interop/TestHookName;)V
    .locals 0
    .param p0, "name"    # Lcom/microsoft/xle/test/interop/TestHookName;

    .prologue
    .line 414
    return-void
.end method

.method public static onGetMediaState()Z
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public static onLoginStateChanged(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 0
    .param p0, "loginState"    # Ljava/lang/String;
    .param p1, "exception"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    .line 336
    return-void
.end method

.method public static onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V
    .locals 0
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "change"    # Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    .prologue
    .line 111
    return-void
.end method

.method public static setAllowDismissSoftKeyboard(Z)V
    .locals 0
    .param p0, "allow"    # Z

    .prologue
    .line 86
    return-void
.end method

.method public static setAutomaticSignin(ZILcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .param p0, "automaticSignin"    # Z
    .param p1, "userIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Landroid/webkit/WebView;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p2, "signInDelegate":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Landroid/webkit/WebView;Ljava/lang/Integer;>;"
    sput-boolean p0, Lcom/microsoft/xle/test/interop/TestInterop;->signinAutomatically:Z

    .line 300
    sput p1, Lcom/microsoft/xle/test/interop/TestInterop;->automaticSigninUserIndex:I

    .line 301
    sput-object p2, Lcom/microsoft/xle/test/interop/TestInterop;->automaticSignInDelegate:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 302
    return-void
.end method

.method public static setClientId(Ljava/lang/String;)V
    .locals 0
    .param p0, "newClientId"    # Ljava/lang/String;

    .prologue
    .line 274
    sput-object p0, Lcom/microsoft/xle/test/interop/TestInterop;->clientId:Ljava/lang/String;

    .line 275
    return-void
.end method

.method public static setCompanionDebugAllowedUrls(Ljava/lang/String;)V
    .locals 0
    .param p0, "debugAllowedUrls"    # Ljava/lang/String;

    .prologue
    .line 516
    return-void
.end method

.method public static setCompanionDebugTitleId(Ljava/lang/String;)V
    .locals 0
    .param p0, "debugTitleId"    # Ljava/lang/String;

    .prologue
    .line 501
    return-void
.end method

.method public static setCompanionDebugUrl(Ljava/lang/String;)V
    .locals 0
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 449
    return-void
.end method

.method public static setDismissSoftKeyboard(Lcom/microsoft/xle/test/interop/delegates/Action;)V
    .locals 0
    .param p0, "action"    # Lcom/microsoft/xle/test/interop/delegates/Action;

    .prologue
    .line 92
    return-void
.end method

.method public static setEnableLogging(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "enable"    # Ljava/lang/Boolean;

    .prologue
    .line 553
    return-void
.end method

.method public static setHasLaunchedCompanionDebug(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "hasLaunched"    # Ljava/lang/Boolean;

    .prologue
    .line 464
    return-void
.end method

.method public static setHasTappedLaunch(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "hasTappedLaunch"    # Ljava/lang/Boolean;

    .prologue
    .line 486
    return-void
.end method

.method public static setIsMonkey(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "monkey"    # Ljava/lang/Boolean;

    .prologue
    .line 434
    return-void
.end method

.method public static setLoginError(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "overrideState"    # Z
    .param p1, "errorOnPageStart"    # Z
    .param p2, "errorState"    # Ljava/lang/String;
    .param p3, "page"    # Ljava/lang/String;

    .prologue
    .line 347
    return-void
.end method

.method public static setLoginReturnUrl(Ljava/lang/String;)V
    .locals 0
    .param p0, "newReturnUrl"    # Ljava/lang/String;

    .prologue
    .line 286
    sput-object p0, Lcom/microsoft/xle/test/interop/TestInterop;->loginReturnUrl:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public static setMarkerReady(Ljava/lang/String;)V
    .locals 2
    .param p0, "currentMarker"    # Ljava/lang/String;

    .prologue
    .line 529
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->markerNameToTrigger:Ljava/lang/String;

    monitor-enter v1

    .line 530
    :try_start_0
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerNameToTrigger:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerTrigger:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 533
    :cond_0
    monitor-exit v1

    .line 534
    return-void

    .line 533
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setMarkerTrigger(Ljava/lang/String;)V
    .locals 2
    .param p0, "marker"    # Ljava/lang/String;

    .prologue
    .line 522
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->markerNameToTrigger:Ljava/lang/String;

    monitor-enter v1

    .line 523
    :try_start_0
    sput-object p0, Lcom/microsoft/xle/test/interop/TestInterop;->markerNameToTrigger:Ljava/lang/String;

    .line 524
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerTrigger:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 525
    monitor-exit v1

    .line 526
    return-void

    .line 525
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setMembershipLevel(ZZ)V
    .locals 0
    .param p0, "overrideMembership"    # Z
    .param p1, "newIsGold"    # Z

    .prologue
    .line 149
    return-void
.end method

.method public static setMonitorLPS(Z)V
    .locals 0
    .param p0, "monitor"    # Z

    .prologue
    .line 49
    return-void
.end method

.method public static setNotReady()V
    .locals 2

    .prologue
    .line 114
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->WAITING:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->activityReady:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    .line 116
    monitor-exit v1

    .line 117
    return-void

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setOnGetMediaStateAction(Lcom/microsoft/xle/test/interop/delegates/Action;)V
    .locals 0
    .param p0, "action"    # Lcom/microsoft/xle/test/interop/delegates/Action;

    .prologue
    .line 377
    return-void
.end method

.method public static setOnLoginStateChangedCallback(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/XLEException;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p0, "callback":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/String;Lcom/microsoft/xbox/toolkit/XLEException;>;"
    return-void
.end method

.method public static setPageState(II)V
    .locals 0
    .param p0, "page"    # I
    .param p1, "state"    # I

    .prologue
    .line 216
    return-void
.end method

.method public static setReady()V
    .locals 2

    .prologue
    .line 133
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->READY:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->activityReady:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    .line 135
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 136
    monitor-exit v1

    .line 137
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setSandbox(Ljava/lang/String;)V
    .locals 0
    .param p0, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 577
    return-void
.end method

.method public static setServiceManagerNotification(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "action":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;>;"
    return-void
.end method

.method public static setShowConnectDialogOnLaunch(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "show"    # Ljava/lang/Boolean;

    .prologue
    .line 569
    return-void
.end method

.method public static setTestHook(Lcom/microsoft/xle/test/interop/TestHookName;Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .locals 0
    .param p0, "name"    # Lcom/microsoft/xle/test/interop/TestHookName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/TestHookName;",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 425
    .local p1, "action":Lcom/microsoft/xle/test/interop/delegates/Action1;, "Lcom/microsoft/xle/test/interop/delegates/Action1<Ljava/lang/Boolean;>;"
    return-void
.end method

.method public static setTotalPageCount(I)V
    .locals 0
    .param p0, "count"    # I

    .prologue
    .line 206
    return-void
.end method

.method public static setUserIsChild(ZZ)V
    .locals 0
    .param p0, "enableOverride"    # Z
    .param p1, "isChildAccount"    # Z

    .prologue
    .line 171
    return-void
.end method

.method public static setVersion(III)V
    .locals 0
    .param p0, "minVersionRequired"    # I
    .param p1, "latestVersionAvailable"    # I
    .param p2, "currentVersion"    # I

    .prologue
    .line 271
    return-void
.end method

.method public static showConnectDialog()V
    .locals 0

    .prologue
    .line 563
    return-void
.end method

.method public static waitForMarker(I)V
    .locals 1
    .param p0, "timeout"    # I

    .prologue
    .line 537
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop;->markerTrigger:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady(I)V

    .line 538
    return-void
.end method

.method public static waitForReady()V
    .locals 4

    .prologue
    .line 120
    sget-object v2, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    monitor-enter v2

    .line 122
    .local v0, "e":Ljava/lang/InterruptedException;
    :goto_0
    :try_start_0
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->activityReady:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    sget-object v3, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->WAITING:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v3, :cond_0

    .line 124
    :try_start_1
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop;->activityReadyMonitor:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 130
    return-void
.end method
