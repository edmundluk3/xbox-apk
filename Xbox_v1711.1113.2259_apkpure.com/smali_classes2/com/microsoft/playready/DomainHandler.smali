.class Lcom/microsoft/playready/DomainHandler;
.super Ljava/lang/Object;
.source "DomainHandler.java"

# interfaces
.implements Lcom/microsoft/playready/IDomainHandler;


# instance fields
.field private mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

.field private final mExecService:Ljava/util/concurrent/ExecutorService;

.field private mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;


# direct methods
.method private constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 2
    .param p1, "factory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/DomainHandler;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 19
    iput-object v1, p0, Lcom/microsoft/playready/DomainHandler;->mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 20
    iput-object v1, p0, Lcom/microsoft/playready/DomainHandler;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 31
    iput-object p1, p0, Lcom/microsoft/playready/DomainHandler;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 32
    return-void
.end method

.method static createDomainHandler(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/IDomainHandler;
    .locals 1
    .param p0, "factory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 26
    new-instance v0, Lcom/microsoft/playready/DomainHandler;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/DomainHandler;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    return-object v0
.end method

.method private startDomainOp(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)Lcom/microsoft/playready/IDomainHandlingTask;
    .locals 7
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p2, "domainServerUri"    # Ljava/lang/String;
    .param p3, "opType"    # Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .prologue
    .line 70
    if-nez p1, :cond_0

    .line 72
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid argument to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", domainInfo must not be null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :cond_0
    if-nez p2, :cond_1

    .line 79
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid argument to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", domainServerUri must not be null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84
    :cond_1
    new-instance v0, Lcom/microsoft/playready/DomainHandlingWorker;

    .line 85
    iget-object v1, p0, Lcom/microsoft/playready/DomainHandler;->mPRFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/playready/DomainHandler;->getDomainHandlingPlugin()Lcom/microsoft/playready/IDomainHandlingPlugin;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 84
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/DomainHandlingWorker;-><init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/IDomainHandlingPlugin;Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)V

    .line 91
    .local v0, "worker":Lcom/microsoft/playready/DomainHandlingWorker;
    new-instance v6, Lcom/microsoft/playready/DomainHandlingTask;

    invoke-direct {v6, v0}, Lcom/microsoft/playready/DomainHandlingTask;-><init>(Lcom/microsoft/playready/DomainHandlingWorker;)V

    .line 92
    .local v6, "task":Lcom/microsoft/playready/DomainHandlingTask;
    iget-object v1, p0, Lcom/microsoft/playready/DomainHandler;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 94
    return-object v6
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandler;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 46
    return-void

    .line 43
    :catchall_0
    move-exception v0

    .line 44
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 45
    throw v0
.end method

.method public getDomainHandlingPlugin()Lcom/microsoft/playready/IDomainHandlingPlugin;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandler;->mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;

    invoke-direct {v0}, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/DomainHandler;->mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandler;->mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    return-object v0
.end method

.method public joinDomain(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)Lcom/microsoft/playready/IDomainHandlingTask;
    .locals 1
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p2, "domainServerUri"    # Ljava/lang/String;

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/playready/DomainHandler;->startDomainOp(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)Lcom/microsoft/playready/IDomainHandlingTask;

    move-result-object v0

    return-object v0
.end method

.method public leaveDomain(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)Lcom/microsoft/playready/IDomainHandlingTask;
    .locals 1
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p2, "domainServerUri"    # Ljava/lang/String;

    .prologue
    .line 110
    sget-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/playready/DomainHandler;->startDomainOp(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)Lcom/microsoft/playready/IDomainHandlingTask;

    move-result-object v0

    return-object v0
.end method

.method public setDomainHandlingPlugin(Lcom/microsoft/playready/IDomainHandlingPlugin;)V
    .locals 0
    .param p1, "dhPlugin"    # Lcom/microsoft/playready/IDomainHandlingPlugin;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/playready/DomainHandler;->mDHPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 52
    return-void
.end method
