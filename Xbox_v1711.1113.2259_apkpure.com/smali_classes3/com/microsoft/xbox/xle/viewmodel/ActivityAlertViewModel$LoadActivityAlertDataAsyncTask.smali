.class Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityAlertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadActivityAlertDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$1;

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 196
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadActivityAlerts(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 202
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 204
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 215
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 192
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 208
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->updateAdapter()V

    .line 210
    return-void
.end method
