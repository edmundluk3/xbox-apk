.class public Lcom/microsoft/playready/DomainInfo;
.super Ljava/lang/Object;
.source "DomainInfo.java"


# instance fields
.field private final m_accountID:Ljava/lang/String;

.field private final m_friendlyName:Ljava/lang/String;

.field private final m_revision:I

.field private final m_serviceID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "serviceID"    # Ljava/lang/String;
    .param p2, "accountID"    # Ljava/lang/String;
    .param p3, "friendlyName"    # Ljava/lang/String;
    .param p4, "revision"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/microsoft/playready/DomainInfo;->m_serviceID:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/microsoft/playready/DomainInfo;->m_accountID:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/microsoft/playready/DomainInfo;->m_friendlyName:Ljava/lang/String;

    .line 34
    iput p4, p0, Lcom/microsoft/playready/DomainInfo;->m_revision:I

    .line 35
    return-void
.end method


# virtual methods
.method public getAccountID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/playready/DomainInfo;->m_accountID:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/playready/DomainInfo;->m_friendlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getRevision()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/microsoft/playready/DomainInfo;->m_revision:I

    return v0
.end method

.method public getServiceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/playready/DomainInfo;->m_serviceID:Ljava/lang/String;

    return-object v0
.end method
