.class public interface abstract Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
.super Ljava/lang/Object;
.source "TagRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTagSelectedListener"
.end annotation


# virtual methods
.method public abstract isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z
    .param p1    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .param p1    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
