.class final Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ReplayRelay.java"

# interfaces
.implements Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxrelay2/ReplayRelay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "UnboundedReplayBuffer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0xa2f4068c73be4b3L


# instance fields
.field final buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field volatile size:I


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "capacityHint"    # I

    .prologue
    .line 385
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 386
    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "capacityHint <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->buffer:Ljava/util/List;

    .line 388
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->buffer:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    iget v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    .line 394
    return-void
.end method

.method public getValue()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 399
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    iget v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    .line 400
    .local v0, "s":I
    if-eqz v0, :cond_0

    .line 401
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->buffer:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 403
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getValues([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    const/4 v5, 0x0

    .line 409
    iget v3, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    .line 410
    .local v3, "s":I
    if-nez v3, :cond_1

    .line 411
    array-length v4, p1

    if-eqz v4, :cond_0

    .line 412
    const/4 v4, 0x0

    aput-object v5, p1, v4

    :cond_0
    move-object v0, p1

    .line 428
    .end local p1    # "array":[Ljava/lang/Object;, "[TT;"
    .local v0, "array":[Ljava/lang/Object;, "[TT;"
    :goto_0
    return-object v0

    .line 417
    .end local v0    # "array":[Ljava/lang/Object;, "[TT;"
    .restart local p1    # "array":[Ljava/lang/Object;, "[TT;"
    :cond_1
    array-length v4, p1

    if-ge v4, v3, :cond_2

    .line 418
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object p1, v4

    check-cast p1, [Ljava/lang/Object;

    .line 420
    :cond_2
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->buffer:Ljava/util/List;

    .line 421
    .local v1, "b":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_3

    .line 422
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, p1, v2

    .line 421
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 424
    :cond_3
    array-length v4, p1

    if-le v4, v3, :cond_4

    .line 425
    aput-object v5, p1, v3

    :cond_4
    move-object v0, p1

    .line 428
    .end local p1    # "array":[Ljava/lang/Object;, "[TT;"
    .restart local v0    # "array":[Ljava/lang/Object;, "[TT;"
    goto :goto_0
.end method

.method public replay(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    const/4 v8, 0x0

    .line 434
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->getAndIncrement()I

    move-result v7

    if-eqz v7, :cond_0

    .line 484
    :goto_0
    return-void

    .line 438
    :cond_0
    const/4 v4, 0x1

    .line 439
    .local v4, "missed":I
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->buffer:Ljava/util/List;

    .line 440
    .local v1, "b":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v0, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->actual:Lio/reactivex/Observer;

    .line 442
    .local v0, "a":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    iget-object v3, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    .line 444
    .local v3, "indexObject":Ljava/lang/Integer;
    if-eqz v3, :cond_2

    .line 445
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 453
    .local v2, "index":I
    :cond_1
    :goto_1
    iget-boolean v7, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v7, :cond_3

    .line 454
    iput-object v8, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_0

    .line 447
    .end local v2    # "index":I
    :cond_2
    const/4 v2, 0x0

    .line 448
    .restart local v2    # "index":I
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_1

    .line 458
    :cond_3
    iget v6, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    .line 460
    .local v6, "s":I
    :goto_2
    if-eq v6, v2, :cond_5

    .line 462
    iget-boolean v7, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v7, :cond_4

    .line 463
    iput-object v8, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_0

    .line 467
    :cond_4
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 469
    .local v5, "o":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, v5}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 470
    add-int/lit8 v2, v2, 0x1

    .line 471
    goto :goto_2

    .line 473
    .end local v5    # "o":Ljava/lang/Object;, "TT;"
    :cond_5
    iget v7, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    if-ne v2, v7, :cond_1

    .line 477
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    .line 479
    neg-int v7, v4

    invoke-virtual {p1, v7}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->addAndGet(I)I

    move-result v4

    .line 480
    if-nez v4, :cond_1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 488
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer<TT;>;"
    iget v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;->size:I

    .line 489
    .local v0, "s":I
    if-eqz v0, :cond_0

    .end local v0    # "s":I
    :goto_0
    return v0

    .restart local v0    # "s":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
