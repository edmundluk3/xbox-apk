.class public final enum Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;
.super Ljava/lang/Enum;
.source "ApplicationBarManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppBarAnimationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum HIDE_APPBAR:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum HIDE_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum READY:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum SHOW_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum SHOW_APPBAR_FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field public static final enum SWAP_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->READY:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "SHOW_APPBAR_FULL"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SHOW_APPBAR_FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "HIDE_APPBAR"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->HIDE_APPBAR:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "SHOW_APPBAR_BUTTONS"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SHOW_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "SWAP_APPBAR_BUTTONS"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SWAP_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "HIDE_APPBAR_BUTTONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->HIDE_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    const-string v1, "DONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    .line 66
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->READY:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SHOW_APPBAR_FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->HIDE_APPBAR:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SHOW_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SWAP_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->HIDE_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->$VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->$VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    return-object v0
.end method
