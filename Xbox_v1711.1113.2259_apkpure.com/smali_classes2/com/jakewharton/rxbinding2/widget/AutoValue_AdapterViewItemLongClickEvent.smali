.class final Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;
.super Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;
.source "AutoValue_AdapterViewItemLongClickEvent.java"


# instance fields
.field private final clickedView:Landroid/view/View;

.field private final id:J

.field private final position:I

.field private final view:Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "view":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;-><init>()V

    .line 20
    if-nez p1, :cond_0

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->view:Landroid/widget/AdapterView;

    .line 24
    if-nez p2, :cond_1

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null clickedView"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->clickedView:Landroid/view/View;

    .line 28
    iput p3, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->position:I

    .line 29
    iput-wide p4, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    .line 30
    return-void
.end method


# virtual methods
.method public clickedView()Landroid/view/View;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->clickedView:Landroid/view/View;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 70
    check-cast v0, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;

    .line 71
    .local v0, "that":Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;
    iget-object v3, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->view:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;->view()Landroid/widget/AdapterView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->clickedView:Landroid/view/View;

    .line 72
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;->clickedView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->position:I

    .line 73
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;->position()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    .line 74
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;->id()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/jakewharton/rxbinding2/widget/AdapterViewItemLongClickEvent;
    :cond_3
    move v1, v2

    .line 76
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v2, 0xf4243

    .line 81
    const/4 v0, 0x1

    .line 82
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 83
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->view:Landroid/widget/AdapterView;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 84
    mul-int/2addr v0, v2

    .line 85
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->clickedView:Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 86
    mul-int/2addr v0, v2

    .line 87
    iget v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->position:I

    xor-int/2addr v0, v1

    .line 88
    mul-int/2addr v0, v2

    .line 89
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 90
    return v0
.end method

.method public id()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    return-wide v0
.end method

.method public position()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->position:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdapterViewItemLongClickEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->view:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clickedView="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->clickedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public view()Landroid/widget/AdapterView;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/AdapterView",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/widget/AutoValue_AdapterViewItemLongClickEvent;->view:Landroid/widget/AdapterView;

    return-object v0
.end method
