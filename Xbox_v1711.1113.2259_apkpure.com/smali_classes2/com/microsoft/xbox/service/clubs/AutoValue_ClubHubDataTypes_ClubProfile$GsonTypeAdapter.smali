.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_ClubProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;",
        ">;"
    }
.end annotation


# instance fields
.field private final associatedTitlesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final backgroundImageUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private defaultAssociatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private defaultBackgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultDescription:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultDisplayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultIsRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultIsSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultLeaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultMatureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultName:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultPreferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultPrimaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultRequestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultSecondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTransferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultWatchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final isRecommendableAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final isSearchableAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final leaveEnabledAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final matureContentEnabledAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final preferredLocaleAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final primaryColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final requestToJoinEnabledAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final secondaryColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tagsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tertiaryColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transferOwnershipEnabledAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final watchClubTitlesOnlyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultAssociatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 52
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultBackgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 53
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPrimaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 54
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultSecondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 55
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 56
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 57
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDisplayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 58
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultName:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 59
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPreferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 60
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 61
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 62
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 63
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultLeaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 64
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultMatureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 65
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultRequestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 66
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTransferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 67
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultWatchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 69
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Long;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->associatedTitlesAdapter:Lcom/google/gson/TypeAdapter;

    .line 70
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->backgroundImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 71
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->primaryColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 72
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->secondaryColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 73
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tertiaryColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 74
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 75
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 76
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 77
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->preferredLocaleAdapter:Lcom/google/gson/TypeAdapter;

    .line 78
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    .line 79
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isRecommendableAdapter:Lcom/google/gson/TypeAdapter;

    .line 80
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isSearchableAdapter:Lcom/google/gson/TypeAdapter;

    .line 81
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->leaveEnabledAdapter:Lcom/google/gson/TypeAdapter;

    .line 82
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->matureContentEnabledAdapter:Lcom/google/gson/TypeAdapter;

    .line 83
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->requestToJoinEnabledAdapter:Lcom/google/gson/TypeAdapter;

    .line 84
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->transferOwnershipEnabledAdapter:Lcom/google/gson/TypeAdapter;

    .line 85
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->watchClubTitlesOnlyAdapter:Lcom/google/gson/TypeAdapter;

    .line 86
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .locals 21
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v20, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v20

    if-ne v1, v0, :cond_0

    .line 202
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 203
    const/4 v1, 0x0

    .line 304
    :goto_0
    return-object v1

    .line 205
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultAssociatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 207
    .local v2, "associatedTitles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultBackgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 208
    .local v3, "backgroundImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPrimaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 209
    .local v4, "primaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultSecondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 210
    .local v5, "secondaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 211
    .local v6, "tertiaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 212
    .local v7, "description":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDisplayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 213
    .local v8, "displayImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultName:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 214
    .local v9, "name":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPreferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 215
    .local v10, "preferredLocale":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 216
    .local v11, "tags":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 217
    .local v12, "isRecommendable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 218
    .local v13, "isSearchable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultLeaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 219
    .local v14, "leaveEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultMatureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 220
    .local v15, "matureContentEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultRequestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v16, v0

    .line 221
    .local v16, "requestToJoinEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTransferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v17, v0

    .line 222
    .local v17, "transferOwnershipEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultWatchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-object/from16 v18, v0

    .line 223
    .local v18, "watchClubTitlesOnly":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 224
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v19

    .line 225
    .local v19, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v20, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v20

    if-ne v1, v0, :cond_1

    .line 226
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 229
    :cond_1
    const/4 v1, -0x1

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->hashCode()I

    move-result v20

    sparse-switch v20, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 299
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 229
    :sswitch_0
    const-string v20, "associatedTitles"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v20, "backgroundImageUrl"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v20, "primaryColor"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v20, "secondaryColor"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v20, "tertiaryColor"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v20, "description"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v20, "displayImageUrl"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_7
    const-string v20, "name"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v1, 0x7

    goto :goto_2

    :sswitch_8
    const-string v20, "preferredLocale"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0x8

    goto :goto_2

    :sswitch_9
    const-string v20, "tags"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0x9

    goto :goto_2

    :sswitch_a
    const-string v20, "isRecommendable"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xa

    goto :goto_2

    :sswitch_b
    const-string v20, "isSearchable"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v20, "leaveEnabled"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v20, "matureContentEnabled"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v20, "requestToJoinEnabled"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v20, "transferOwnershipEnabled"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0xf

    goto/16 :goto_2

    :sswitch_10
    const-string v20, "watchClubTitlesOnly"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/16 v1, 0x10

    goto/16 :goto_2

    .line 231
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->associatedTitlesAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "associatedTitles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/Long;>;"
    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 232
    .restart local v2    # "associatedTitles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/Long;>;"
    goto/16 :goto_1

    .line 235
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->backgroundImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "backgroundImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 236
    .restart local v3    # "backgroundImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 239
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->primaryColorAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "primaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 240
    .restart local v4    # "primaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 243
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->secondaryColorAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "secondaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 244
    .restart local v5    # "secondaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 247
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tertiaryColorAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "tertiaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 248
    .restart local v6    # "tertiaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 251
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "description":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 252
    .restart local v7    # "description":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 255
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "displayImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 256
    .restart local v8    # "displayImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 259
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "name":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v9, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 260
    .restart local v9    # "name":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 263
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->preferredLocaleAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "preferredLocale":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    check-cast v10, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 264
    .restart local v10    # "preferredLocale":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 267
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "tags":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/String;>;"
    check-cast v11, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 268
    .restart local v11    # "tags":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 271
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isRecommendableAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "isRecommendable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v12, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 272
    .restart local v12    # "isRecommendable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 275
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isSearchableAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "isSearchable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v13, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 276
    .restart local v13    # "isSearchable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 279
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->leaveEnabledAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "leaveEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v14, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 280
    .restart local v14    # "leaveEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 283
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->matureContentEnabledAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "matureContentEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v15, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 284
    .restart local v15    # "matureContentEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 287
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->requestToJoinEnabledAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "requestToJoinEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v16, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 288
    .restart local v16    # "requestToJoinEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 291
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->transferOwnershipEnabledAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "transferOwnershipEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v17, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 292
    .restart local v17    # "transferOwnershipEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 295
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->watchClubTitlesOnlyAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "watchClubTitlesOnly":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    check-cast v18, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 296
    .restart local v18    # "watchClubTitlesOnly":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    goto/16 :goto_1

    .line 303
    .end local v19    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 304
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile;

    invoke-direct/range {v1 .. v18}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    goto/16 :goto_0

    .line 229
    :sswitch_data_0
    .sparse-switch
        -0x7a79030a -> :sswitch_6
        -0x66ca7c04 -> :sswitch_5
        -0x59fe9113 -> :sswitch_e
        -0x572365cf -> :sswitch_4
        -0x52ca3894 -> :sswitch_10
        -0x474f69b1 -> :sswitch_3
        -0x43f65d7f -> :sswitch_2
        -0x4347723f -> :sswitch_0
        -0x39a2f29e -> :sswitch_1
        -0x359b1dc5 -> :sswitch_8
        -0x22c29950 -> :sswitch_d
        -0x1c55d776 -> :sswitch_c
        0x337a8b -> :sswitch_7
        0x363419 -> :sswitch_9
        0x3d343f0c -> :sswitch_a
        0x5717e3cc -> :sswitch_b
        0x7184c2dd -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAssociatedTitles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "defaultAssociatedTitles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultAssociatedTitles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 89
    return-object p0
.end method

.method public setDefaultBackgroundImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "defaultBackgroundImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultBackgroundImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 93
    return-object p0
.end method

.method public setDefaultDescription(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "defaultDescription":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 109
    return-object p0
.end method

.method public setDefaultDisplayImageUrl(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "defaultDisplayImageUrl":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultDisplayImageUrl:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 113
    return-object p0
.end method

.method public setDefaultIsRecommendable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "defaultIsRecommendable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsRecommendable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 129
    return-object p0
.end method

.method public setDefaultIsSearchable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "defaultIsSearchable":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultIsSearchable:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 133
    return-object p0
.end method

.method public setDefaultLeaveEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "defaultLeaveEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultLeaveEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 137
    return-object p0
.end method

.method public setDefaultMatureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "defaultMatureContentEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultMatureContentEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 141
    return-object p0
.end method

.method public setDefaultName(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "defaultName":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultName:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 117
    return-object p0
.end method

.method public setDefaultPreferredLocale(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "defaultPreferredLocale":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPreferredLocale:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 121
    return-object p0
.end method

.method public setDefaultPrimaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "defaultPrimaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultPrimaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 97
    return-object p0
.end method

.method public setDefaultRequestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "defaultRequestToJoinEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultRequestToJoinEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 145
    return-object p0
.end method

.method public setDefaultSecondaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "defaultSecondaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultSecondaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 101
    return-object p0
.end method

.method public setDefaultTags(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "defaultTags":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTags:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    .line 125
    return-object p0
.end method

.method public setDefaultTertiaryColor(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "defaultTertiaryColor":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTertiaryColor:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 105
    return-object p0
.end method

.method public setDefaultTransferOwnershipEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "defaultTransferOwnershipEnabled":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultTransferOwnershipEnabled:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 149
    return-object p0
.end method

.method public setDefaultWatchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "defaultWatchClubTitlesOnly":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->defaultWatchClubTitlesOnly:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 153
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    if-nez p2, :cond_0

    .line 159
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 198
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 163
    const-string v0, "associatedTitles"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->associatedTitlesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 165
    const-string v0, "backgroundImageUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->backgroundImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 167
    const-string v0, "primaryColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->primaryColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 169
    const-string v0, "secondaryColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->secondaryColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 171
    const-string v0, "tertiaryColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tertiaryColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 173
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 175
    const-string v0, "displayImageUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 177
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 179
    const-string v0, "preferredLocale"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->preferredLocaleAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->preferredLocale()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 181
    const-string v0, "tags"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 183
    const-string v0, "isRecommendable"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isRecommendableAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 185
    const-string v0, "isSearchable"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->isSearchableAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 187
    const-string v0, "leaveEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->leaveEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 189
    const-string v0, "matureContentEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->matureContentEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 191
    const-string v0, "requestToJoinEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->requestToJoinEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 193
    const-string v0, "transferOwnershipEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->transferOwnershipEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 195
    const-string v0, "watchClubTitlesOnly"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->watchClubTitlesOnlyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 197
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)V

    return-void
.end method
