.class final synthetic Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;->arg$1:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;->arg$1:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    check-cast p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;->render(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    return-void
.end method
