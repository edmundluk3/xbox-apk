.class public final Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "MyXuidProviderRnModule.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "MyXuidProvider"


# instance fields
.field myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 1
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 21
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;)V

    .line 22
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "MyXuidProvider"

    return-object v0
.end method

.method public getXuid(Lcom/facebook/react/bridge/Promise;)V
    .locals 1
    .param p1, "promise"    # Lcom/facebook/react/bridge/Promise;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v0}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    .line 32
    return-void
.end method
