.class Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "CanvasFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;

    .prologue
    .line 380
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V

    return-void
.end method


# virtual methods
.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 10
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 406
    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isFailure()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 413
    .local v3, "jsonChannels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v4

    iget-object v5, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v5

    .line 414
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v4

    iget-object v4, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 415
    .local v0, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v6, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v6, v6, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget v7, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    if-ne v6, v7, :cond_2

    .line 416
    const/4 v6, 0x1

    iput-boolean v6, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->isChannelEstablished:Z

    .line 419
    :cond_2
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    .local v2, "json":Lorg/json/JSONObject;
    :try_start_1
    const-string v6, "titleId"

    iget-object v7, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v7, v7, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-static {v7}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v8

    invoke-virtual {v2, v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 422
    const-string v6, "isChannelEstablished"

    iget-boolean v7, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->isChannelEstablished:Z

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    :goto_2
    :try_start_2
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 428
    .end local v0    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    .end local v2    # "json":Lorg/json/JSONObject;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :cond_3
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 431
    :try_start_4
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$700(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 432
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;-><init>(Ljava/util/List;)V

    .line 433
    .local v2, "json":Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$700(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    move-result-object v4

    const-string v5, "titleChannelChanged"

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 435
    .end local v2    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;
    :catch_0
    move-exception v1

    .line 436
    .local v1, "e":Lorg/json/JSONException;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v4

    const-string v5, "Failed to raise title channel changed event"

    sget-object v6, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0

    .line 423
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v0    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    .local v2, "json":Lorg/json/JSONObject;
    :catch_1
    move-exception v6

    goto :goto_2
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 443
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v2

    if-nez v2, :cond_1

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;->$SwitchMap$com$microsoft$xbox$smartglass$ConnectionState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ConnectionState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 450
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles()Ljava/util/List;

    move-result-object v1

    .line 451
    .local v1, "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 452
    .local v0, "activeTitle":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$600(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_1

    .line 458
    .end local v0    # "activeTitle":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    .end local v1    # "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 447
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 6
    .param p1, "activeTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 383
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v2, v3, :cond_3

    .line 384
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v2

    iget-object v3, v2, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v3

    .line 385
    const/4 v0, 0x0

    .line 386
    .local v0, "associatedChannel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 387
    .local v1, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v4, v4, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget v5, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-ne v4, v5, :cond_0

    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isValid()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 388
    move-object v0, v1

    .line 393
    .end local v1    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :cond_1
    if-eqz v0, :cond_2

    .line 394
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 395
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$500(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 397
    :cond_2
    monitor-exit v3

    .line 401
    .end local v0    # "associatedChannel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :goto_0
    return-void

    .line 397
    .restart local v0    # "associatedChannel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 399
    .end local v0    # "associatedChannel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$600(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_0
.end method
