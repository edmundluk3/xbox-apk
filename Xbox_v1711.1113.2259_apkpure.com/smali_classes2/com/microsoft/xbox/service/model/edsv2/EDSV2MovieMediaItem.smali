.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2MovieMediaItem.java"


# instance fields
.field public CriticRating:I

.field public Duration:Ljava/lang/String;

.field private DurationInMMSS:Ljava/lang/String;

.field public ParentalRating:Ljava/lang/String;

.field public ParentalRatingSystem:Ljava/lang/String;

.field public RatingId:I

.field public ReviewSources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;",
            ">;"
        }
    .end annotation
.end field

.field public Studios:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;",
            ">;"
        }
    .end annotation
.end field

.field public ZuneId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 28
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 29
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->AllTimeAverageRating:F

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->AllTimeAverageRating:F

    move-object v0, p1

    .line 30
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->CriticRating:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->CriticRating:I

    move-object v0, p1

    .line 31
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Duration:Ljava/lang/String;

    move-object v0, p1

    .line 32
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Genres:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Genres:Ljava/util/List;

    move-object v0, p1

    .line 33
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ParentalRating:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ParentalRating:Ljava/lang/String;

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->RatingId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->RatingId:I

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Studios:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Studios:Ljava/util/ArrayList;

    .line 38
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ZuneId:Ljava/lang/String;

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getMediaType()I

    move-result v0

    if-nez v0, :cond_1

    .line 42
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->setMediaItemTypeFromInt(I)V

    .line 44
    :cond_1
    return-void
.end method


# virtual methods
.method public getDuration()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->DurationInMMSS:Ljava/lang/String;

    .line 74
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->DurationInMMSS:Ljava/lang/String;

    return-object v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Duration:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->durationStringToSeconds(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getTimeStringMMSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->DurationInMMSS:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMetaCriticReviewScore()F
    .locals 5

    .prologue
    .line 55
    const/4 v1, 0x0

    .line 56
    .local v1, "score":F
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 57
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    .line 58
    .local v0, "review":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->Name:Ljava/lang/String;

    const-string v4, "Metacritic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    iget v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->ReviewScore:F

    goto :goto_0

    .line 63
    .end local v0    # "review":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    :cond_1
    return v1
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ParentalRating:Ljava/lang/String;

    return-object v0
.end method

.method public getStudio()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Studios:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Studios:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->Studios:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;->Name:Ljava/lang/String;

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method
