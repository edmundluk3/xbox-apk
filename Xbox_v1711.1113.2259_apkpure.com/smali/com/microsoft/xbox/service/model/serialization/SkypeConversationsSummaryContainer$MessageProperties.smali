.class public Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;
.super Ljava/lang/Object;
.source "SkypeConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MessageProperties"
.end annotation


# instance fields
.field public alerts:Ljava/lang/Boolean;

.field public clearedat:Ljava/lang/String;

.field private consumptionhorizon:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "consumptionhorizon"
    .end annotation
.end field

.field private legacyConsumptionHorizon:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "consumptionHorizon"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getConsumptionHorizon()Ljava/lang/String;
    .locals 11
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v10, 0x0

    .line 75
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 76
    const/4 v8, 0x0

    .line 94
    :goto_0
    return-object v8

    .line 77
    :cond_0
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 78
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    goto :goto_0

    .line 79
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 80
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    goto :goto_0

    .line 83
    :cond_2
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "currentConsumptionHorizonFields":[Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "legacyConsumptionHorizonFields":[Ljava/lang/String;
    aget-object v1, v0, v10

    .line 86
    .local v1, "currentTime":Ljava/lang/String;
    aget-object v5, v4, v10

    .line 88
    .local v5, "legacyTime":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 89
    .local v2, "currentTimeMs":J
    :goto_1
    if-eqz v5, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 91
    .local v6, "legacyTimeMs":J
    :cond_3
    cmp-long v8, v2, v6

    if-lez v8, :cond_5

    .line 92
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    goto :goto_0

    .end local v2    # "currentTimeMs":J
    .end local v6    # "legacyTimeMs":J
    :cond_4
    move-wide v2, v6

    .line 88
    goto :goto_1

    .line 94
    .restart local v2    # "currentTimeMs":J
    .restart local v6    # "legacyTimeMs":J
    :cond_5
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    goto :goto_0
.end method

.method public setConsumptionHorizon(Ljava/lang/String;)V
    .locals 1
    .param p1, "ch"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->consumptionhorizon:Ljava/lang/String;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->legacyConsumptionHorizon:Ljava/lang/String;

    .line 103
    return-void
.end method
