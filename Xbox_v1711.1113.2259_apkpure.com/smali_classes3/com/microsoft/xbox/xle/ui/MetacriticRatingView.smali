.class public Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;
.super Landroid/widget/LinearLayout;
.source "MetacriticRatingView.java"


# instance fields
.field private ratingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 32
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030174

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 33
    const v1, 0x7f0e07a8

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->ratingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    return-void
.end method


# virtual methods
.method public setRating(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 22
    if-nez p1, :cond_0

    .line 23
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->setVisibility(I)V

    .line 28
    :goto_0
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->setVisibility(I)V

    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MetacriticRatingView;->ratingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
