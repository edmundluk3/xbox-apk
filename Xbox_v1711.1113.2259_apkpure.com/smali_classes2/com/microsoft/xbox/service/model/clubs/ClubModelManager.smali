.class public final enum Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
.super Ljava/lang/Enum;
.source "ClubModelManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;,
        Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;",
        ">;",
        "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

.field private static final MAX_MODEL_COUNT:I = 0xff

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final clubs:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->$VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    .line 34
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->updateClubs(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    return-void
.end method

.method private updateClubs(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 142
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    monitor-enter v2

    .line 143
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 144
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v5, 0x0

    invoke-direct {v4, v0, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v3, v4, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    goto :goto_0

    .line 150
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 147
    .restart local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_0
    :try_start_1
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid club from service: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    :cond_2
    :goto_1
    return-void

    .line 151
    :cond_3
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to update club data"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->$VALUES:[Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method


# virtual methods
.method public getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 44
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    monitor-enter v2

    .line 45
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 46
    .local v0, "club":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .end local v0    # "club":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;-><init>(J)V

    .line 48
    .restart local v0    # "club":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_0
    monitor-exit v2

    return-object v0

    .line 52
    .end local v0    # "club":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public loadClubs(ZLjava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 10
    .param p1, "forceRefresh"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 80
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 82
    if-eqz p1, :cond_0

    .line 83
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 93
    :goto_0
    return-object v1

    .line 85
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 86
    .local v7, "staleClubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    monitor-enter v5

    .line 87
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 88
    .local v0, "clubId":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 89
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    .end local v0    # "clubId":Ljava/lang/Long;
    :catchall_0
    move-exception v1

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    goto :goto_0
.end method

.method public loadClubs(ZLjava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 10
    .param p1, "forceRefresh"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p3, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 109
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 110
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 112
    if-eqz p1, :cond_0

    .line 113
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0, p2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;Ljava/util/List;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 123
    :goto_0
    return-object v1

    .line 115
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    .local v7, "staleClubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    monitor-enter v5

    .line 117
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 118
    .local v0, "clubId":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v8

    invoke-virtual {v8, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->shouldRefresh(Ljava/util/List;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 119
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 122
    .end local v0    # "clubId":Ljava/lang/Long;
    :catchall_0
    move-exception v1

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0, v7, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;Ljava/util/List;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    goto :goto_0
.end method

.method public loadRecommendedClubs()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 58
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadRecommendedClubs(JI)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 11
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1

    const/4 v5, 0x0

    .line 72
    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 73
    int-to-long v0, p3

    invoke-static {v2, v3, v0, v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 74
    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    new-instance v6, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v6}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;JILcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;)V

    move v2, v7

    move-wide v3, v8

    move-object v7, v0

    invoke-static/range {v2 .. v7}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadRecommendedClubs(Ljava/util/List;I)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p1, "criteria":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 64
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 65
    const-wide/16 v0, 0x1

    int-to-long v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 66
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;

    invoke-direct {v6, p0, p1, p2, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;ILcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadUserClubs(J)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 130
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 132
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;

    invoke-direct {v6, p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;J)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method removeClub(J)V
    .locals 3
    .param p1, "clubId"    # J

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->clubs:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public rxLoad(ZLjava/lang/Long;Ljava/util/List;)Lio/reactivex/Single;
    .locals 4
    .param p1, "forceRefresh"    # Z
    .param p2, "clubId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    .local p3, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    const-wide/16 v0, 0x1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 101
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 103
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rxLoad(ZLjava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
