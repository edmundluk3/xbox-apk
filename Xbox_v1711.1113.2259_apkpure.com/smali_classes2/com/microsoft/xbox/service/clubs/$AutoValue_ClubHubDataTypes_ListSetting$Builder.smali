.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
.source "$AutoValue_ClubHubDataTypes_ListSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private allowedValues:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private canViewerAct:Ljava/lang/Boolean;

.field private canViewerChangeSetting:Ljava/lang/Boolean;

.field private value:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 99
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;-><init>()V

    .line 100
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    .local p1, "source":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;-><init>()V

    .line 102
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->value:Lcom/google/common/collect/ImmutableList;

    .line 103
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->allowedValues:Lcom/google/common/collect/ImmutableList;

    .line 104
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->canViewerAct()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerAct:Ljava/lang/Boolean;

    .line 105
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->canViewerChangeSetting()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerChangeSetting:Ljava/lang/Boolean;

    .line 106
    return-void
.end method


# virtual methods
.method public allowedValues(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    .local p1, "allowedValues":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->allowedValues:Lcom/google/common/collect/ImmutableList;

    .line 115
    return-object p0

    .line 114
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    const-string v0, ""

    .line 130
    .local v0, "missing":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerAct:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " canViewerAct"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerChangeSetting:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " canViewerChangeSetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 137
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 139
    :cond_2
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->value:Lcom/google/common/collect/ImmutableList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->allowedValues:Lcom/google/common/collect/ImmutableList;

    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerAct:Ljava/lang/Boolean;

    .line 142
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerChangeSetting:Ljava/lang/Boolean;

    .line 143
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;ZZ)V

    .line 139
    return-object v1
.end method

.method public canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .locals 1
    .param p1, "canViewerAct"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerAct:Ljava/lang/Boolean;

    .line 120
    return-object p0
.end method

.method public canViewerChangeSetting(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .locals 1
    .param p1, "canViewerChangeSetting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->canViewerChangeSetting:Ljava/lang/Boolean;

    .line 125
    return-object p0
.end method

.method public value(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 109
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder<TT;>;"
    .local p1, "value":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;->value:Lcom/google/common/collect/ImmutableList;

    .line 110
    return-object p0

    .line 109
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method
