.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;
.super Ljava/lang/Object;
.source "PartyDetailsNavigator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;)V
    .locals 0
    .param p1, "telemetryService"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .line 23
    return-void
.end method


# virtual methods
.method public navigateBack()V
    .locals 3

    .prologue
    .line 52
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    const-string v2, "navigateBack"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->goBack()V

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate back"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public navigateToProfile(J)V
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 26
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 27
    sget-object v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "navigateToProfile: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 30
    .local v0, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 33
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->viewProfile(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    const-string v3, "Failed to navigate to profile"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public navigateToTextChat()V
    .locals 3

    .prologue
    .line 41
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    const-string v2, "navigateToTextChat"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->viewChat()V

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to text chat"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
