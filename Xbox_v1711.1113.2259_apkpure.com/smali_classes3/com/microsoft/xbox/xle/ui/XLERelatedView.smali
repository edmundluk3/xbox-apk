.class public Lcom/microsoft/xbox/xle/ui/XLERelatedView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
.source "XLERelatedView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/XLERelatedView;->setSoundEffectsEnabled(Z)V

    .line 15
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/XLERelatedView;->setSelector(I)V

    .line 16
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 20
    if-eqz p1, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLERelatedView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 22
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLERelatedView;->getNumColumns()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->initItemLayout(II)V

    .line 24
    .end local v0    # "adapter":Landroid/widget/BaseAdapter;
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->onLayout(ZIIII)V

    .line 26
    return-void
.end method
