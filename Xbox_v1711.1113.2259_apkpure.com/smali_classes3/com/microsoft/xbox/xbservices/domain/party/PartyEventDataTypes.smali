.class public final Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes;
.super Ljava/lang/Object;
.source "PartyEventDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;,
        Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;,
        Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;,
        Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;,
        Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
