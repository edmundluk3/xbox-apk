.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;
.super Ljava/lang/Object;
.source "PartyAndLfgListViewAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PartyListItem"
.end annotation


# instance fields
.field public volatile transient hashCode:I

.field public final host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field public final party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p1, "party"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "host"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 377
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    .line 378
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 379
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383
    if-ne p1, p0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v1

    .line 385
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 386
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 388
    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    .line 389
    .local v0, "other":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 390
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 396
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 397
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    .line 398
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    .line 399
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    .line 402
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
