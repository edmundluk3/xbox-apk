.class final Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "AbsListViewScrollEventObservable.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private currentScrollState:I

.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;Lio/reactivex/Observer;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AbsListView;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->currentScrollState:I

    .line 32
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    .line 33
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 34
    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 59
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    iget v2, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->currentScrollState:I

    .line 51
    invoke-static {v1, v2, p2, p3, p4}, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;->create(Landroid/widget/AbsListView;IIII)Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;

    move-result-object v0

    .line 53
    .local v0, "event":Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, v0}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 55
    .end local v0    # "event":Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 5
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 37
    iput p2, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->currentScrollState:I

    .line 38
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    .line 40
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v3, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    .line 41
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    iget-object v4, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->view:Landroid/widget/AbsListView;

    invoke-virtual {v4}, Landroid/widget/AbsListView;->getCount()I

    move-result v4

    .line 40
    invoke-static {v1, p2, v2, v3, v4}, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;->create(Landroid/widget/AbsListView;IIII)Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;

    move-result-object v0

    .line 42
    .local v0, "event":Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, v0}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 44
    .end local v0    # "event":Lcom/jakewharton/rxbinding2/widget/AbsListViewScrollEvent;
    :cond_0
    return-void
.end method
