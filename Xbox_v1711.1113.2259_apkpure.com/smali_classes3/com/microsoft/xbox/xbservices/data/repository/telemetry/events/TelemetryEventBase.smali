.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
.super Ljava/lang/Object;
.source "TelemetryEventBase.java"


# instance fields
.field private name:Ljava/lang/String;

.field private payload:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->name:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->payload:Ljava/util/concurrent/ConcurrentHashMap;

    .line 22
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 23
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->setName(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    .line 24
    return-void
.end method


# virtual methods
.method public declared-synchronized addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "value"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 41
    invoke-static {p2}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->payload:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->payload:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :cond_0
    monitor-exit p0

    return-object p0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->name:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getPayload()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->payload:Ljava/util/concurrent/ConcurrentHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setName(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->name:Ljava/lang/String;

    .line 35
    return-object p0
.end method
