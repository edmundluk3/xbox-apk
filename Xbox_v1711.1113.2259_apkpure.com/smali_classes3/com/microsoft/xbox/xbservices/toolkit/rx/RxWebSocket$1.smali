.class Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;
.super Lokhttp3/WebSocketListener;
.source "RxWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->subscribeActual(Lio/reactivex/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

.field final synthetic val$observer:Lio/reactivex/Observer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    invoke-direct {p0}, Lokhttp3/WebSocketListener;-><init>()V

    return-void
.end method

.method static synthetic lambda$onOpen$0(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$400(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lokhttp3/WebSocket;

    move-result-object v0

    invoke-interface {v0, p1}, Lokhttp3/WebSocket;->send(Ljava/lang/String;)Z

    return-void
.end method


# virtual methods
.method public onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 4
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "code"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$100(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClosed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$300(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketClose:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    invoke-interface {v0}, Lio/reactivex/Observer;->onComplete()V

    .line 104
    return-void
.end method

.method public onClosing(Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 4
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "code"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$100(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClosing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-super {p0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onClosing(Lokhttp3/WebSocket;ILjava/lang/String;)V

    .line 97
    return-void
.end method

.method public onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V
    .locals 3
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "t"    # Ljava/lang/Throwable;
    .param p3, "response"    # Lokhttp3/Response;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$100(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onFailure"

    invoke-interface {v0, v1, v2, p2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$300(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatUDPActiveError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    .line 110
    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->setException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    invoke-interface {v0, p2}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    .line 112
    return-void
.end method

.method public onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V
    .locals 2
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p2}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;->with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method public onMessage(Lokhttp3/WebSocket;Lokio/ByteString;)V
    .locals 2
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "bytes"    # Lokio/ByteString;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    invoke-static {p2}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;->with(Lokio/ByteString;)Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method public onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V
    .locals 3
    .param p1, "webSocket"    # Lokhttp3/WebSocket;
    .param p2, "response"    # Lokhttp3/Response;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$100(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onOpen"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$200(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lio/reactivex/subjects/ReplaySubject;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/ReplaySubject;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->this$0:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->access$300(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatWebSocketOpen:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;->val$observer:Lio/reactivex/Observer;

    sget-object v1, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->INSTANCE:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 81
    return-void
.end method
