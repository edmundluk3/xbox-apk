.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;
.super Ljava/lang/Object;
.source "ContentRatingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RatingBoard"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final localizedProperties:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocalizedProperties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Name"
    .end annotation
.end field

.field private final ratings:Ljava/util/Map;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Ratings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "ratings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;>;"
    .local p3, "localizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 90
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 91
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 93
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->name:Ljava/lang/String;

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    .line 96
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    if-ne p1, p0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v1

    .line 132
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;

    if-nez v3, :cond_2

    move v1, v2

    .line 133
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 135
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;

    .line 136
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    .line 137
    invoke-interface {v3, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    .line 138
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getDefaultContentRatingLocalizedProperty(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;
    .locals 3
    .param p1, "ratingId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 110
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;

    .line 112
    .local v0, "contentRating":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->getLocalizedProperties()Ljava/util/List;

    move-result-object v1

    .line 115
    .local v1, "contentRatingLocalizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 116
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;

    .line 120
    .end local v1    # "contentRatingLocalizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;>;"
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDefaultRatingBoardLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalizedProperties()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getRatings()Ljava/util/Map;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    if-nez v0, :cond_0

    .line 145
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    .line 146
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    .line 147
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->ratings:Ljava/util/Map;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    .line 148
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->localizedProperties:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    .line 151
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
