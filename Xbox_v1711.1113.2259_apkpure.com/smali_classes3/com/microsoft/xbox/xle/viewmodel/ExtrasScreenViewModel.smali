.class public Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ExtrasScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;
    }
.end annotation


# instance fields
.field private companionData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

.field private dlcData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private isLoading:Z

.field private loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

.field private mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

.field private screenData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getExtrasAdapter(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 46
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 49
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 50
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->onLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->isLoading:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method private onLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 107
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->isLoading:Z

    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "changed":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getActivitiesList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 110
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->companionData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getActivitiesList()Ljava/util/ArrayList;

    move-result-object v5

    if-eq v4, v5, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 112
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dataModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getActivitiesList()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->companionData:Ljava/util/ArrayList;

    .line 116
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v4

    const/16 v5, 0x2329

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v4

    if-ne v4, v2, :cond_2

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 118
    .local v1, "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 119
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dlcData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v5

    if-eq v4, v5, :cond_2

    .line 120
    const/4 v0, 0x1

    .line 121
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dlcData:Ljava/util/ArrayList;

    .line 127
    .end local v1    # "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    :cond_2
    if-eqz v0, :cond_4

    .line 128
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->screenData:Ljava/util/ArrayList;

    .line 129
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->companionData:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 130
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->screenData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->companionData:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 133
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dlcData:Ljava/util/ArrayList;

    if-eqz v4, :cond_4

    .line 134
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->screenData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->dlcData:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 138
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->screenData:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 140
    .local v2, "hasValidData":Z
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 156
    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v4, :cond_8

    .line 157
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 161
    :goto_2
    return-void

    .end local v2    # "hasValidData":Z
    :cond_6
    move v2, v3

    .line 138
    goto :goto_0

    .line 145
    .restart local v2    # "hasValidData":Z
    :pswitch_0
    if-eqz v2, :cond_7

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_3
    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    :cond_7
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3

    .line 150
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v3, v4, :cond_5

    if-nez v2, :cond_5

    .line 151
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 159
    :cond_8
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->updateAdapter()V

    goto :goto_2

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->screenData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public gotoDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 64
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .end local p1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 66
    .restart local p1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->cancel()V

    .line 96
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->load(Z)V

    .line 99
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getExtrasAdapter(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 57
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->cancel()V

    .line 79
    :cond_0
    return-void
.end method
