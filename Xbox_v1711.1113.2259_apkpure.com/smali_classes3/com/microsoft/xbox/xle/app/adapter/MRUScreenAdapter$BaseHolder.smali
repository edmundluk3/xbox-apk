.class abstract Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MRUScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "BaseHolder"
.end annotation


# instance fields
.field protected final nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

.field protected npi:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

.field protected final pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p3, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 185
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 186
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .line 187
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    return-void
.end method

.method protected static bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p1, "smartglassIcon"    # Landroid/widget/TextView;

    .prologue
    .line 198
    if-eqz p0, :cond_0

    .line 199
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->computeHasStateForBat(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 205
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 201
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    const v0, 0x7f071014

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected static bindRowTitle(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p0, "title"    # Landroid/widget/TextView;
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 225
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 232
    :goto_0
    return-void

    .line 227
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 228
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 230
    :cond_1
    const-string v0, ""

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method


# virtual methods
.method public abstract bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
.end method

.method protected bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 0
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->npi:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .line 195
    return-void
.end method

.method protected final getPopup()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .locals 1

    .prologue
    .line 256
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    return-object v0
.end method

.method protected onRecentClick(I)V
    .locals 5
    .param p1, "indexPosition"    # I

    .prologue
    const/4 v3, 0x1

    .line 235
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->npi:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v1

    .line 236
    .local v1, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    if-eqz v1, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 237
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecent(Lcom/microsoft/xbox/service/model/recents/Recent;)V

    .line 241
    if-eqz v1, :cond_0

    .line 242
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackMRUAction(Ljava/lang/String;I)V

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->getPopup()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    .line 246
    .local v0, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    new-instance v2, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;)V

    invoke-direct {v2, v4}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;-><init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 252
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 253
    return-void

    .line 236
    .end local v0    # "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected setImageWidth(Landroid/widget/ImageView;II)V
    .locals 8
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "parentWidth"    # I
    .param p3, "percent"    # I

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 219
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    int-to-double v2, p2

    int-to-double v4, p3

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    .line 220
    .local v1, "width":I
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    sub-int v2, v1, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 221
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    return-void
.end method

.method protected setRowHeight(II)V
    .locals 8
    .param p1, "parentWidth"    # I
    .param p2, "percent"    # I

    .prologue
    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 213
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    int-to-double v2, p1

    int-to-double v4, p2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 214
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    return-void
.end method

.method protected updateImageBackground(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "img"    # Landroid/widget/ImageView;

    .prologue
    .line 260
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 261
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method
