.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;
.super Ljava/lang/Object;
.source "UTCComparePeoplePicker.java"


# static fields
.field private static final NOTITLEID:J

.field private static gameTitleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->gameTitleId:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 11
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->gameTitleId:J

    return-wide v0
.end method

.method public static getGameTitleId()J
    .locals 2

    .prologue
    .line 16
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->gameTitleId:J

    return-wide v0
.end method

.method public static setGameTitleId(J)V
    .locals 0
    .param p0, "gameTitleId"    # J

    .prologue
    .line 20
    sput-wide p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->gameTitleId:J

    .line 21
    return-void
.end method

.method public static trackCompareAction(Ljava/lang/String;)V
    .locals 2
    .param p0, "xuid"    # Ljava/lang/String;

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->trackCompareAction(Ljava/lang/String;J)V

    .line 54
    return-void
.end method

.method public static trackCompareAction(Ljava/lang/String;J)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "titleId"    # J

    .prologue
    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;

    invoke-direct {v0, p1, p2, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 44
    return-void
.end method

.method public static trackComparePeoplePickerView()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 70
    return-void
.end method
