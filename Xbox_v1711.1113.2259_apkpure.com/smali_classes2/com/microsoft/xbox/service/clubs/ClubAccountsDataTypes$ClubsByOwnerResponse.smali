.class public abstract Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubsByOwnerResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;Ljava/util/List;IIII)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 7
    .param p0, "owner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "remainingOpenAndClosedClubs"    # I
    .param p3, "remainingSecretClubs"    # I
    .param p4, "maximumOpenAndClosedClubs"    # I
    .param p5, "maximumSecretClubs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;IIII)",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "clubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;

    if-eqz p1, :cond_0

    .line 48
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    :goto_0
    move-object v1, p0

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;IIII)V

    .line 46
    return-object v0

    .line 48
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract clubs()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;"
        }
    .end annotation
.end method

.method public abstract maximumOpenAndClosedClubs()I
.end method

.method public abstract maximumSecretClubs()I
.end method

.method public abstract owner()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract remainingOpenAndClosedClubs()I
.end method

.method public abstract remainingSecretClubs()I
.end method
