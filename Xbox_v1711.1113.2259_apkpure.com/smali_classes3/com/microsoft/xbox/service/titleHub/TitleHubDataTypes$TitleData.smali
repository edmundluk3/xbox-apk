.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleData"
.end annotation


# instance fields
.field public final achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final alternateTitleIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final bingId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final displayImage:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final friendsWhoPlayed:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleImage;",
            ">;"
        }
    .end annotation
.end field

.field public final mediaItemType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public modernTitleId:J

.field public final name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final pfn:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final serviceConfigId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final titleHistory:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final titleId:J

.field public final type:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final windowsPhoneProductId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "bingId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "detail"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "displayImage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "mediaItemType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "modernTitleId"    # J
    .param p10, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "pfn"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p12, "titleId"    # J
    .param p14, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p15, "windowsPhoneProductId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p16, "friendsWhoPlayed"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p17, "achievement"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p18, "titleHistory"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p19, "serviceConfigId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleImage;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "alternateTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p6, "images":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleImage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    if-nez p1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->alternateTitleIds:Ljava/util/List;

    .line 148
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->bingId:Ljava/lang/String;

    .line 149
    iput-object p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    .line 150
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    .line 151
    iput-object p5, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    .line 152
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->images:Ljava/util/List;

    .line 153
    iput-object p7, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->mediaItemType:Ljava/lang/String;

    .line 154
    iput-wide p8, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    .line 155
    iput-object p10, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    .line 156
    iput-object p11, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->pfn:Ljava/lang/String;

    .line 157
    iput-wide p12, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    .line 158
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->type:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->windowsPhoneProductId:Ljava/lang/String;

    .line 160
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->friendsWhoPlayed:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;

    .line 161
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    .line 162
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleHistory:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;

    .line 163
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    .line 164
    return-void

    .line 147
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    if-ne p1, p0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-nez v3, :cond_2

    move v1, v2

    .line 195
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 197
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 198
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->alternateTitleIds:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->alternateTitleIds:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->bingId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->bingId:Ljava/lang/String;

    .line 199
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    .line 200
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    .line 201
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    .line 202
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->images:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->images:Ljava/util/List;

    .line 203
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->mediaItemType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->mediaItemType:Ljava/lang/String;

    .line 204
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    .line 206
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->pfn:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->pfn:Ljava/lang/String;

    .line 207
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->type:Ljava/lang/String;

    .line 209
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->windowsPhoneProductId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->windowsPhoneProductId:Ljava/lang/String;

    .line 210
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->friendsWhoPlayed:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->friendsWhoPlayed:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;

    .line 211
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    .line 212
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public getBoxArt()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    return-object v0
.end method

.method public getDevices()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->images:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getScid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 218
    const/16 v0, 0x11

    .line 219
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->alternateTitleIds:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 220
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->bingId:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 221
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 222
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 223
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 224
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->images:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 225
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->mediaItemType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 226
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v2

    add-int v0, v1, v2

    .line 227
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 228
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->pfn:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 229
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v2

    add-int v0, v1, v2

    .line 230
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->type:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 231
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->windowsPhoneProductId:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 232
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->friendsWhoPlayed:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 233
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 235
    return v0
.end method

.method public isXbox360Achievement()Z
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->getTypeId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXbox360Game()Z
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXboxOneAchievement()Z
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->sourceVersion:I

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->getTypeId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXboxOneGame()Z
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->devices:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
