.class Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCompanionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$1;

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 9

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    iget-object v8, v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 197
    .local v8, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->access$100()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    move-result-object v0

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->MediaItemType:Ljava/lang/String;

    .line 199
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaGroup()I

    move-result v3

    .line 200
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v4

    .line 197
    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;->getCompanions(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    move-result-object v6

    .line 201
    .local v6, "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    if-eqz v6, :cond_0

    .line 202
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->getDefault()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 208
    .end local v6    # "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    :goto_0
    return-object v0

    .line 204
    :catch_0
    move-exception v7

    .line 205
    .local v7, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load companion data for game: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208
    .end local v7    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->loadDataInBackground()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->onError()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 1
    .param p1, "companion"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 218
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 177
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->onPostExecute(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method
