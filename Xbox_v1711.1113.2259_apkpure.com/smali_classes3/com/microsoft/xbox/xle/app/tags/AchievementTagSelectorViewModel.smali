.class public Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;
.super Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
.source "AchievementTagSelectorViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

.field private titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private titleId:J

.field private titleModel:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;J)V
    .locals 4
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 39
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleId:J

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 42
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->onLoadTitleAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private onLoadTitleAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 85
    sget-object v2, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->updateAdapter()V

    .line 112
    return-void

    .line 89
    :pswitch_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v1, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 96
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 97
    .local v0, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    if-eqz v0, :cond_1

    .line 98
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->tags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->with(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 92
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXbox360Achievement()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 102
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->tags:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_3
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3

    .line 107
    .end local v1    # "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to load achievements"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getErrorString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->getIsLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->cancel()V

    .line 77
    :cond_0
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->titleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 78
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 79
    new-instance v0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->execute()V

    .line 82
    :cond_1
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->loadTitleAchievementsAsyncTask:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->cancel()V

    .line 64
    :cond_0
    return-void
.end method

.method public setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V
    .locals 1
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->setSelectedTagListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 117
    return-void
.end method
