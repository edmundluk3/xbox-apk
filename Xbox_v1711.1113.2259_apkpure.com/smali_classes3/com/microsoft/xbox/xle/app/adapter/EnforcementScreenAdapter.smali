.class public Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "EnforcementScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final cancelButton:Landroid/view/View;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final contentTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final loadingView:Landroid/view/View;

.field private final reportCategorySpinner:Landroid/widget/Spinner;

.field private reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final reportDetailedReasonText:Landroid/widget/EditText;

.field private selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field private final sendButton:Landroid/view/View;

.field private final sendReportSpinner:Landroid/widget/Spinner;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;)V
    .locals 1
    .param p1, "enforcementViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    .line 48
    const v0, 0x7f0e0561

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->screenBody:Landroid/view/View;

    .line 49
    const v0, 0x7f0e0988

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->loadingView:Landroid/view/View;

    .line 51
    const v0, 0x7f0e0563

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->contentTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    const v0, 0x7f0e0564

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e0565

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinner:Landroid/widget/Spinner;

    .line 54
    const v0, 0x7f0e0566

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendReportSpinner:Landroid/widget/Spinner;

    .line 55
    const v0, 0x7f0e0567

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportDetailedReasonText:Landroid/widget/EditText;

    .line 56
    const v0, 0x7f0e0498

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->cancelButton:Landroid/view/View;

    .line 57
    const v0, 0x7f0e0562

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 58
    const v0, 0x7f0e0568

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendButton:Landroid/view/View;

    .line 59
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->close()V

    .line 72
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportDetailedReasonText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    iput-object v0, v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackType:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendReportAsync()V

    .line 90
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 114
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->setClubReportDescription()V

    .line 115
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Selected item = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .prologue
    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->setShouldReportToClubModerator(Z)V

    .line 140
    return-void

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setClubReportDescription()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    if-eqz v0, :cond_0

    .line 145
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ReportReasonCategory:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected club report description requested for category: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->selectedCategory:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 147
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f07033d

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 151
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070337

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 155
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f07033b

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 159
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070339

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onStart()V
    .locals 11

    .prologue
    const v10, 0x7f03020b

    const v9, 0x1090008

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 76
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 78
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->loadingView:Landroid/view/View;

    const v6, 0x7f0c0004

    invoke-virtual {v3, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 80
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->contentTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070a7a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v8, v8, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->displayName:Ljava/lang/String;

    aput-object v8, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->cancelButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportDetailedReasonText:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v6, v6, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    iget-object v0, v3, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 96
    .local v0, "reportSource":Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    .line 99
    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->getDisplayReasons(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v3, v6, v9, v7}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 101
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v3, v10}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 102
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinner:Landroid/widget/Spinner;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v3, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 103
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getCount()I

    move-result v3

    if-le v3, v4, :cond_4

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 105
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-ne v0, v3, :cond_5

    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->setClubReportDescription()V

    .line 107
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 112
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportCategorySpinner:Landroid/widget/Spinner;

    new-instance v6, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v3, v6}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 119
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v2, "sendReportToList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070a84

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsChat:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-ne v0, v3, :cond_1

    .line 126
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f07023a

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_1
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v1, v3, v9, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 130
    .local v1, "sendReportAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v1, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 131
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendReportSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendReportSpinner:Landroid/widget/Spinner;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v4, :cond_2

    move v5, v4

    :cond_2
    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 134
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->getShouldReportToClubModerator()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 135
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendReportSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 138
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->sendReportSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 141
    return-void

    .end local v1    # "sendReportAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v2    # "sendReportToList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    move v3, v5

    .line 103
    goto/16 :goto_0

    .line 109
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->reportClubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 171
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard()V

    .line 172
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 173
    return-void
.end method

.method public updateView()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->updateView()V

    .line 64
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->loadingView:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->isBusy()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 68
    return-void
.end method
