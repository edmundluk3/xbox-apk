.class public Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
.super Ljava/lang/Object;
.source "ProfileStatisticsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StatisticGroup"
.end annotation


# instance fields
.field private minutesPlayedStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

.field public name:Ljava/lang/String;

.field public statlistscollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;"
        }
    .end annotation
.end field

.field public titleid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMinutesPlayed()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->minutesPlayedStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    return-object v0
.end method

.method public setMinutesPlayed(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;)V
    .locals 0
    .param p1, "data"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->minutesPlayedStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 48
    return-void
.end method
