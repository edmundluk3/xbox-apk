.class Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;
.super Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
.source "AvailableConsolesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConsoleDiscoverListener"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)V

    return-void
.end method

.method private declared-synchronized addDiscoveredConsole(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->access$800(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 317
    new-instance v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;-><init>(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V

    .line 319
    .local v0, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->access$900(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setAutoConnect(Z)V

    .line 322
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    sget-object v2, Lcom/microsoft/xbox/smartglass/DeviceStatus;->Unavailable:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    if-ne v1, v2, :cond_1

    .line 323
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setIsSleeping(Z)V

    .line 325
    :cond_1
    const-string v1, "AvailableConsolesModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add avaible console"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v1, "AvailableConsolesModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "console should auto connect"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->access$800(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    monitor-exit p0

    return-void

    .line 316
    .end local v0    # "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public onDiscoverCompleted()V
    .locals 2

    .prologue
    .line 309
    const-string v0, "AvailableConsolesModel"

    const-string v1, "discover complete"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->this$0:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isDiscovering:Z

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;->access$700(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;)Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 312
    return-void
.end method

.method public onPresenceUpdated(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    .prologue
    .line 302
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    sget-object v1, Lcom/microsoft/xbox/smartglass/DeviceStatus;->Available:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPowerEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    sget-object v1, Lcom/microsoft/xbox/smartglass/DeviceStatus;->Unavailable:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    if-ne v0, v1, :cond_1

    .line 303
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner$ConsoleDiscoverListener;->addDiscoveredConsole(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V

    .line 305
    :cond_1
    return-void
.end method
