.class public interface abstract Lcom/microsoft/onlineid/internal/ui/IWebPropertyProvider;
.super Ljava/lang/Object;
.source "IWebPropertyProvider.java"


# virtual methods
.method public abstract getProperty(Lcom/microsoft/onlineid/internal/ui/PropertyBag$Key;)Ljava/lang/String;
.end method

.method public abstract handlesProperty(Lcom/microsoft/onlineid/internal/ui/PropertyBag$Key;)Z
.end method

.method public abstract setProperty(Lcom/microsoft/onlineid/internal/ui/PropertyBag$Key;Ljava/lang/String;)V
.end method
