.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.source "EDSV2NowPlayingDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "internalModel"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 45
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 46
    return-void
.end method

.method public static getModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .locals 2
    .param p0, "titleId"    # J
    .param p2, "partnerMediaId"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getNowPlayingModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 190
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 141
    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 274
    if-nez p1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq p1, p0, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-ne p1, v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    return-object v0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoading()Z

    move-result v0

    .line 126
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoading()Z

    move-result v0

    goto :goto_0
.end method

.method public getIsLoadingRelated()Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoadingRelated()Z

    move-result v0

    .line 135
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoadingRelated()Z

    move-result v0

    goto :goto_0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v0

    .line 148
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 233
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    goto :goto_0
.end method

.method public getPosterImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getRelated()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getShouldCheckActivity()Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getShouldCheckActivity()Z

    move-result v0

    .line 117
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getShouldCheckActivity()Z

    move-result v0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 160
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Unknown:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public isGameType()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isGameType()Z

    move-result v0

    .line 172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 206
    const-string v0, "don\'t use this method for load details. use loadDetail instead"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 207
    return-void
.end method

.method public loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 4
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadBigCatItemDetailFromTitleId(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getTitleId()J

    move-result-wide v0

    invoke-super {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadBigCatItemDetailFromTitleId(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0
.end method

.method public loadRelated(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 224
    const-string v0, "NowPlayingDetailModel"

    const-string v1, "load related"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadRelated(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->updateDataForBrowser(Lcom/microsoft/xbox/toolkit/AsyncResult;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object p1

    .line 242
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 244
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v5, v6, :cond_1

    .line 245
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 247
    .local v1, "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_1

    .line 250
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v4

    .line 251
    .local v4, "partnerMediaId":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setPartnerMediaId(Ljava/lang/String;)V

    .line 253
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->createModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getObservers()Ljava/util/List;

    move-result-object v0

    .line 257
    .local v0, "currentObservers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->clearObservers()V

    .line 259
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 260
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 261
    .local v3, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    goto :goto_0

    .line 264
    .end local v3    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 270
    .end local v0    # "currentObservers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;>;"
    .end local v1    # "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;>;"
    .end local v4    # "partnerMediaId":Ljava/lang/String;
    :goto_1
    return-void

    .line 269
    :cond_1
    new-instance v5, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v6, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v7, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v8, 0x1

    invoke-direct {v6, v7, v8}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v7

    invoke-direct {v5, v6, p0, v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_1
.end method

.method public declared-synchronized removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->internalModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 200
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
