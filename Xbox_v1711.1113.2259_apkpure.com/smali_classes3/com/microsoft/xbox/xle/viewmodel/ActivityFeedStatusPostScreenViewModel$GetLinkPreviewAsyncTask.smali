.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedStatusPostScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetLinkPreviewAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private previewLink:Ljava/lang/String;

.field private previewResponse:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "previewLink"    # Ljava/lang/String;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 338
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 340
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->previewLink:Ljava/lang/String;

    .line 341
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 365
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->previewLink:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->getLinkPostPreview(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 367
    .local v0, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 369
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->previewResponse:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .line 370
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 350
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->previewResponse:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V

    .line 376
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 333
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateAdapter()V

    .line 361
    return-void
.end method
