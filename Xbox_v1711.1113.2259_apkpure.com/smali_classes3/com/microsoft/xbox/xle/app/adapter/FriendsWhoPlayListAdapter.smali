.class public Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FriendsWhoPlayListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;"
    }
.end annotation


# instance fields
.field private useDarkStyle:Z

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->useDarkStyle:Z

    .line 26
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .line 27
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gameTitleId:J

    .line 28
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 37
    sget-wide v2, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gameTitleId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->getGameTitleId()J

    move-result-wide v2

    sput-wide v2, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->gameTitleId:J

    .line 40
    :cond_0
    if-nez p2, :cond_2

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->inflateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 42
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;

    .line 46
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 47
    .local v1, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->updateUI(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 48
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->useDarkStyle:Z

    if-eqz v2, :cond_1

    .line 49
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;->setDarkStyle()V

    .line 51
    :cond_1
    return-object p2

    .line 44
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter$FriendsWhoPlayViewHolder;
    goto :goto_0
.end method

.method public setUseDarkStyle(Z)V
    .locals 0
    .param p1, "useDark"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->useDarkStyle:Z

    .line 32
    return-void
.end method
