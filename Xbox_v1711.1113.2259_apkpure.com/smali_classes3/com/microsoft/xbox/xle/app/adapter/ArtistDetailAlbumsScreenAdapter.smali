.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "ArtistDetailAlbumsScreenAdapter.java"


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    .line 28
    const v0, 0x7f0e01dd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    const v0, 0x7f0e01de

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->content:Landroid/view/View;

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->content:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    const v0, 0x7f0e01df

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03003c

    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;-><init>(Landroid/app/Activity;ILcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->items:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->getArtistAlbums()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->getArtistAlbums()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->items:Ljava/util/List;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;->clear()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 58
    :cond_1
    return-void
.end method
