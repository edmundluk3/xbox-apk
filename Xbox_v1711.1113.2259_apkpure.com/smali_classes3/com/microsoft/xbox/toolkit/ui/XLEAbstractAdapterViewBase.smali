.class public interface abstract Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;
.super Ljava/lang/Object;
.source "XLEAbstractAdapterViewBase.java"


# virtual methods
.method public abstract getAnimationName()Ljava/lang/String;
.end method

.method public abstract getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract onDataUpdated()V
.end method

.method public abstract restoreScrollState(II)V
.end method
