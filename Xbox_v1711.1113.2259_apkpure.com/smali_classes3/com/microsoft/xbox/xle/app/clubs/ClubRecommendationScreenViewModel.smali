.class public Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubRecommendationScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;
    }
.end annotation


# static fields
.field private static final MAX_SEE_ALL_RESULTS:I = 0x64


# instance fields
.field private loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

.field private final loadedClubs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation
.end field

.field private final parameters:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 37
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    .line 39
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadedClubs:Ljava/util/List;

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubRecommendationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 42
    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->onClubsLoaded(Ljava/util/List;)V

    return-void
.end method

.method private onClubsLoaded(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "newClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    if-nez p1, :cond_1

    .line 89
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 90
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 95
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadedClubs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 96
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 97
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 98
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v1

    .line 99
    .local v1, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadedClubs:Ljava/util/List;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 105
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->updateView()V

    .line 106
    return-void
.end method


# virtual methods
.method public getClubCardModels()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadedClubs:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 81
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    if-nez v0, :cond_1

    .line 82
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->load(Z)V

    .line 85
    :cond_1
    return-void
.end method

.method public navigateToSearch()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 55
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubRecommendationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 64
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->cancel()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->loadRecommendedClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;

    .line 72
    :cond_0
    return-void
.end method
