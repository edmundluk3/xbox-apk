.class public abstract Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
.super Ljava/lang/Object;
.source "PartyTextViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadingInstance()Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 37
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Lio/reactivex/Observable;Lio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public static withContent(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 7
    .param p0, "oldState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->textStream()Lio/reactivex/Observable;

    move-result-object v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Lio/reactivex/Observable;Lio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 7
    .param p0, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;

    const/4 v5, 0x0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Lio/reactivex/Observable;Lio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public static withStreams(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 7
    .param p0, "oldState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    .local p2, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 45
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->isLoading()Z

    move-result v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->error()Ljava/lang/Throwable;

    move-result-object v6

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Lio/reactivex/Observable;Lio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract isLoading()Z
.end method

.method public abstract memberStream()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end method

.method public abstract partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract textStream()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string v1, "Loading"

    .line 73
    :goto_0
    return-object v1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->error()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 66
    const-string v1, "Error"

    goto :goto_0

    .line 68
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v0, "str":Ljava/lang/StringBuilder;
    const-string v1, "party="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "party"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    const-string v1, ",summaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 70
    :cond_2
    const-string v1, "null"

    goto :goto_1

    .line 72
    :cond_3
    const-string v1, "null"

    goto :goto_2
.end method

.method public abstract userSummaries()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end method
