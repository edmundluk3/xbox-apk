.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
.super Ljava/lang/Object;
.source "EDSV2RatingDescriptor.java"


# instance fields
.field public Image:Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Id"
    .end annotation
.end field

.field private nonLocalizedDescriptor:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "NonLocalizedDescriptor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getNonLocalizedDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->nonLocalizedDescriptor:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->id:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setNonLocalizedDescriptor(Ljava/lang/String;)V
    .locals 0
    .param p1, "nonLocalizedDescriptor"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->nonLocalizedDescriptor:Ljava/lang/String;

    .line 18
    return-void
.end method
