.class public Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;
.super Ljava/lang/Object;
.source "PartyEventNotifier.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private appPaused:Z

.field private final myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

.field private final navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

.field private final notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;)V
    .locals 2
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "myXuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p3, "navigationManager"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p4, "notificationDisplay"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p5, "partyChatRepository"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    .line 35
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .line 36
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->appPaused:Z

    .line 40
    invoke-virtual {p5}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartyEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 43
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->handleEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;)V

    return-void
.end method

.method private handleEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;

    .prologue
    .line 51
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;

    if-eqz v0, :cond_1

    .line 52
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;

    .end local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->handleKickedEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;)V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 53
    .restart local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->shouldShowNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    if-eqz v0, :cond_2

    .line 55
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    .end local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->handleJoinedEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;)V

    goto :goto_0

    .line 56
    .restart local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    :cond_2
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;

    if-eqz v0, :cond_3

    .line 57
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;

    .end local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->handleLeftEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;)V

    goto :goto_0

    .line 58
    .restart local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    :cond_3
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;

    if-eqz v0, :cond_0

    .line 59
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;

    .end local p1    # "event":Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyEvent;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->handleStartedBroadcastingEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;)V

    goto :goto_0
.end method

.method private handleJoinedEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v1}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    const v1, 0x7f070a3c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberJoinedEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayLocalPartyNotification(I[Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void
.end method

.method private handleKickedEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberKickedEvent;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    const v1, 0x7f070a21

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayLocalPartyNotification(I[Ljava/lang/Object;)V

    .line 94
    return-void
.end method

.method private handleLeftEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v1}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    const v1, 0x7f070a3d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberLeftEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayLocalPartyNotification(I[Ljava/lang/Object;)V

    .line 83
    :cond_0
    return-void
.end method

.method private handleStartedBroadcastingEvent(Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v1}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->notificationDisplay:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    const v1, 0x7f070a39

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyEventDataTypes$PartyMemberStartedBroadcastingEvent;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayLocalPartyNotification(I[Ljava/lang/Object;)V

    .line 90
    :cond_0
    return-void
.end method

.method private shouldShowNotification()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->appPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setAppPaused(Z)V
    .locals 0
    .param p1, "isAppPaused"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->appPaused:Z

    .line 47
    return-void
.end method
