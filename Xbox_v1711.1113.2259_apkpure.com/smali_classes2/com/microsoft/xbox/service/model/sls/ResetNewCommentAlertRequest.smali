.class public Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;
.super Ljava/lang/Object;
.source "ResetNewCommentAlertRequest.java"


# instance fields
.field public lastSeenAlertId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastSeenAlertId"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;->lastSeenAlertId:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static getNewCommentAlertRequestBody(Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "resetNewCommentAlertRequest"    # Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;

    .prologue
    .line 15
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 20
    :goto_0
    return-object v1

    .line 16
    :catch_0
    move-exception v0

    .line 17
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ResetNewCommentAlertRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const/4 v1, 0x0

    goto :goto_0
.end method
