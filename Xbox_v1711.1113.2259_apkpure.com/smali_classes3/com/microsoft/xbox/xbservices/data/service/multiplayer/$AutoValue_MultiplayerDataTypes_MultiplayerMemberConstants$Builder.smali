.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants$Builder;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;
.source "$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private custom:Lcom/google/gson/JsonObject;

.field private system:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;-><init>()V

    .line 68
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants$Builder;->custom:Lcom/google/gson/JsonObject;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants$Builder;->system:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants;-><init>(Lcom/google/gson/JsonObject;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)V

    return-object v0
.end method

.method public custom(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;
    .locals 0
    .param p1, "custom"    # Lcom/google/gson/JsonObject;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants$Builder;->custom:Lcom/google/gson/JsonObject;

    .line 72
    return-object p0
.end method

.method public system(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants$Builder;
    .locals 0
    .param p1, "system"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberConstants$Builder;->system:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;

    .line 77
    return-object p0
.end method
