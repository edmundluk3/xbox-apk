.class Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackMessage(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->toggleSendButton()V

    .line 138
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 132
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 126
    return-void
.end method
