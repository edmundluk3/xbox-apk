.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onAppChannelProgramDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$object:Ljava/lang/Object;

.field final synthetic val$programID:Ljava/lang/String;

.field final synthetic val$providerID:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$object:Ljava/lang/Object;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$providerID:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$programID:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 510
    const/4 v1, 0x0

    .line 511
    .local v1, "error":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$object:Ljava/lang/Object;

    instance-of v3, v3, Ljava/lang/Throwable;

    if-eqz v3, :cond_0

    .line 512
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$object:Ljava/lang/Object;

    .end local v1    # "error":Ljava/lang/Throwable;
    check-cast v1, Ljava/lang/Throwable;

    .line 514
    .restart local v1    # "error":Ljava/lang/Throwable;
    :cond_0
    const/4 v0, 0x0

    .line 515
    .local v0, "data":Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$object:Ljava/lang/Object;

    instance-of v3, v3, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-eqz v3, :cond_1

    .line 516
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$object:Ljava/lang/Object;

    .end local v0    # "data":Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    .line 518
    .restart local v0    # "data":Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$500(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;

    .line 519
    .local v2, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$providerID:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;->val$programID:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v4, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;->onAppChannelProgramDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 521
    .end local v2    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;
    :cond_2
    return-void
.end method
