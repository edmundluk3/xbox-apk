.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PartyDetailsHeaderViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;

.field private view2131626046:Landroid/view/View;

.field private view2131626055:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v4, 0x7f0e0847

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;

    .line 32
    const v1, 0x7f0e083e

    const-string v2, "field \'headerView\' and method \'toggleDetails\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "view":Landroid/view/View;
    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->headerView:Landroid/view/View;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626046:Landroid/view/View;

    .line 35
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding$1;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v1, 0x7f0e083f

    const-string v2, "field \'gamePic\'"

    const-class v3, Landroid/widget/ImageView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->gamePic:Landroid/widget/ImageView;

    .line 42
    const v1, 0x7f0e0840

    const-string v2, "field \'name\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->name:Landroid/widget/TextView;

    .line 43
    const v1, 0x7f0e0842

    const-string v2, "field \'status\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->status:Landroid/widget/TextView;

    .line 44
    const v1, 0x7f0e0841

    const-string v2, "field \'descriptionExpandIcon\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->descriptionExpandIcon:Landroid/widget/TextView;

    .line 45
    const v1, 0x7f0e0843

    const-string v2, "field \'tags\'"

    const-class v3, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->tags:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 46
    const v1, 0x7f0e0844

    const-string v2, "field \'description\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->description:Landroid/widget/TextView;

    .line 47
    const v1, 0x7f0e0845

    const-string v2, "field \'lfgButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->lfgButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 48
    const v1, 0x7f0e0846

    const-string v2, "field \'inviteButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 49
    const-string v1, "field \'optionsButton\' and method \'toggleOptions\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 50
    const-string v1, "field \'optionsButton\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626055:Landroid/view/View;

    .line 52
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding$2;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v1, 0x7f0e0848

    const-string v2, "field \'optionsExpandIcon\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsExpandIcon:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f0e0849

    const-string v2, "field \'optionsContainer\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsContainer:Landroid/view/View;

    .line 60
    const v1, 0x7f0e084a

    const-string v2, "field \'joinableToggleButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 61
    const v1, 0x7f0e084b

    const-string v2, "field \'mutePartyToggleButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 62
    const v1, 0x7f0e084c

    const-string v2, "field \'textChatButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->textChatButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 63
    const v1, 0x7f0e084d

    const-string v2, "field \'leaveButton\'"

    const-class v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->leaveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 64
    const v1, 0x7f0e084e

    const-string v2, "field \'broadcastAlertContainer\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertContainer:Landroid/view/View;

    .line 65
    const v1, 0x7f0e0850

    const-string v2, "field \'broadcastAlertDescription\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertDescription:Landroid/widget/TextView;

    .line 66
    const v1, 0x7f0e0851

    const-string v2, "field \'broadcastAlertCheckbox\'"

    const-class v3, Landroid/widget/CheckBox;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertCheckbox:Landroid/widget/CheckBox;

    .line 67
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;

    .line 73
    .local v0, "target":Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;

    .line 76
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->headerView:Landroid/view/View;

    .line 77
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->gamePic:Landroid/widget/ImageView;

    .line 78
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->name:Landroid/widget/TextView;

    .line 79
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->status:Landroid/widget/TextView;

    .line 80
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->descriptionExpandIcon:Landroid/widget/TextView;

    .line 81
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->tags:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 82
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->description:Landroid/widget/TextView;

    .line 83
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->lfgButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 84
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 85
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 86
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsExpandIcon:Landroid/widget/TextView;

    .line 87
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->optionsContainer:Landroid/view/View;

    .line 88
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->joinableToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 89
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->mutePartyToggleButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 90
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->textChatButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 91
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->leaveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 92
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertContainer:Landroid/view/View;

    .line 93
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertDescription:Landroid/widget/TextView;

    .line 94
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;->broadcastAlertCheckbox:Landroid/widget/CheckBox;

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626046:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626046:Landroid/view/View;

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626055:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder_ViewBinding;->view2131626055:Landroid/view/View;

    .line 100
    return-void
.end method
