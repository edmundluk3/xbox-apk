.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullGameHolder"
.end annotation


# instance fields
.field protected final achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final gameStatsContainer:Landroid/view/ViewGroup;

.field protected final gamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final gamerscoreContainer:Landroid/view/ViewGroup;

.field protected final gamescoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field protected final maxGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final progress:Lcom/microsoft/xbox/xle/ui/LabeledArcView;

.field protected final smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final trophyIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 365
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 366
    const v0, 0x7f0e00df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 367
    const v0, 0x7f0e07cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 368
    const v0, 0x7f0e07cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 369
    const v0, 0x7f0e07d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->progress:Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    .line 370
    const v0, 0x7f0e07d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gameStatsContainer:Landroid/view/ViewGroup;

    .line 371
    const v0, 0x7f0e07d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamescoreIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 372
    const v0, 0x7f0e031f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamerscoreContainer:Landroid/view/ViewGroup;

    .line 373
    const v0, 0x7f0e07d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 374
    const v0, 0x7f0e07d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->maxGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 375
    const v0, 0x7f0e07ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->trophyIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 376
    const v0, 0x7f0e07cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 377
    const v0, 0x7f0e07d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 378
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->setRowHeight(II)V

    .line 379
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 6
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    const/4 v4, 0x0

    .line 383
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 385
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 387
    .local v1, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBatGameUri()Ljava/lang/String;

    move-result-object v2

    .line 388
    .local v2, "uri":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v0

    .line 389
    .local v0, "defaultRid":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v5, 0x7f0e001d

    invoke-virtual {v3, v5, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 390
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v2, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 391
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 394
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v2, :cond_0

    const/4 v3, 0x4

    :goto_0
    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 396
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 398
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 399
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->setStatsControlsVisible(Z)V

    .line 400
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 401
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->progress:Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getProgressPercentage()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setPercentage(I)V

    .line 402
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->bindGamerScore(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)V

    .line 403
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 408
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 409
    return-void

    :cond_0
    move v3, v4

    .line 394
    goto :goto_0

    .line 405
    :cond_1
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->setStatsControlsVisible(Z)V

    goto :goto_1
.end method

.method protected bindGamerScore(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .prologue
    .line 419
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getGamerScoreSummary()Landroid/util/Pair;

    move-result-object v0

    .line 420
    .local v0, "score":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v0, :cond_0

    .line 422
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamerscoreContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 428
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamerscoreContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 425
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 426
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->maxGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    .line 433
    return-void
.end method

.method protected setStatsControlsVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 412
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 413
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 414
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->progress:Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 415
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;->gameStatsContainer:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 416
    return-void

    .line 412
    .end local v0    # "visibility":I
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
