.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$OOBE;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OOBE"
.end annotation


# static fields
.field public static final Back:Ljava/lang/String; = "OOBE - Back"

.field public static final ChooseConsoleToSetup:Ljava/lang/String; = "OOBE - Choose a Console To Setup"

.field public static final Close:Ljava/lang/String; = "OOBE - Close"

.field public static final Next:Ljava/lang/String; = "OOBE - Next"

.field public static final PowerSettingsEnergySaving:Ljava/lang/String; = "OOBE - Power Settings Energy Saving"

.field public static final PowerSettingsInstantOn:Ljava/lang/String; = "OOBE - Power Settings Instant On"

.field public static final Skip:Ljava/lang/String; = "OOBE - Skip"

.field public static final TimeZone:Ljava/lang/String; = "OOBE - Time Zone"

.field public static final TimeZoneAutoDSTDisabled:Ljava/lang/String; = "OOBE - Time Zone AutoDST Disabled"

.field public static final TimeZoneAutoDSTEnabled:Ljava/lang/String; = "OOBE - Time Zone AutoDST Enabled"

.field public static final UpdateSettingsKeepConsoleUpToDateDisabled:Ljava/lang/String; = "OOBE - Update Settings Keep Console Up To Date Disabled"

.field public static final UpdateSettingsKeepConsoleUpToDateEnabled:Ljava/lang/String; = "OOBE - Update Settings Keep Console Up To Date Enabled"

.field public static final UpdateSettingsKeepContentUpToDateDisabled:Ljava/lang/String; = "OOBE - Update Settings Keep Content Up To Date Disabled"

.field public static final UpdateSettingsKeepContentUpToDateEnabled:Ljava/lang/String; = "OOBE - Update Settings Keep Content Up To Date Enabled"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
