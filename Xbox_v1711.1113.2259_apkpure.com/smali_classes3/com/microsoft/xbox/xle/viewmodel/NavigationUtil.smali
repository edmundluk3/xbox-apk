.class public final Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;
.super Ljava/lang/Object;
.source "NavigationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;,
        Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;,
        Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;,
        Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;
    }
.end annotation


# static fields
.field public static NAV_FEED:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

.field public static NAV_OLD:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 87
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;->ITEM_CONTENT:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_CONTENT:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->AUTHOR_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_OLD:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_FEED:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    return-void
.end method

.method static synthetic access$000(Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/Runnable;

    .prologue
    .line 86
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->runRunnableIfNotNull(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .prologue
    .line 86
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p3, "x3"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;

    .prologue
    .line 86
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemDetail(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p3, "x3"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;

    .prologue
    .line 86
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemContent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 86
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->onGameClipLoaded(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method public static attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
    .locals 7
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "onPreClick"    # Ljava/lang/Runnable;
    .param p4, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .param p5, "nav"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    .prologue
    .line 325
    if-nez p2, :cond_1

    .line 326
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v2, "attachItemListeners: item is null"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    const-string v0, "attachItemListeners"

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getViewHolder(Landroid/view/View;Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v1

    .line 330
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    if-eqz v1, :cond_0

    .line 331
    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    goto :goto_0
.end method

.method public static attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
    .locals 10
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p4, "onPreClick"    # Ljava/lang/Runnable;
    .param p5, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .param p6, "nav"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    .prologue
    .line 442
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v8

    .line 443
    .local v8, "activityType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 444
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$1;

    invoke-direct {v2, p4, p5, p0, p3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$1;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 454
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$2;

    invoke-direct {v2, p4, p5, p3, p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$2;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    :cond_1
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 473
    iget-object v7, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$3;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 500
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Followed:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v8, v1, :cond_e

    .line 501
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Ljava/lang/Runnable;)V

    .line 527
    .local v0, "targetNavigator":Landroid/view/View$OnClickListener;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v1, :cond_3

    .line 528
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 530
    :cond_3
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 531
    iget-object v9, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;

    move-object/from16 v2, p6

    move-object v3, v0

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Landroid/view/View$OnClickListener;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 573
    :cond_4
    :goto_0
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    .line 574
    iget-object v9, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;

    move-object v2, p4

    move-object/from16 v3, p6

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$8;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    invoke-virtual {v9, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589
    :cond_5
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_6

    .line 590
    iget-object v7, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$9;

    move-object v2, p4

    move-object v3, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$9;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598
    :cond_6
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_7

    .line 599
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$10;

    invoke-direct {v2, p4, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$10;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 608
    :cond_7
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    if-eqz v1, :cond_8

    .line 609
    iget-object v7, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    move-object v1, p4

    move-object/from16 v2, p6

    move-object v3, p2

    move-object v4, p0

    move-object v5, p5

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$2;->lambdaFactory$(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 624
    :cond_8
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    if-eqz v1, :cond_9

    .line 625
    iget-object v7, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    move-object v1, p4

    move-object/from16 v2, p6

    move-object v3, p2

    move-object v4, p0

    move-object v5, p5

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$3;->lambdaFactory$(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640
    :cond_9
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    if-eqz v1, :cond_a

    .line 641
    iget-object v7, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    move-object v1, p4

    move-object/from16 v2, p6

    move-object v3, p2

    move-object v4, p0

    move-object v5, p3

    move-object v6, p5

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->lambdaFactory$(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 656
    :cond_a
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    if-eqz v1, :cond_b

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isTrending()Z

    move-result v1

    if-nez v1, :cond_b

    .line 657
    iget-object v9, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$11;

    move-object v2, p4

    move-object/from16 v3, p6

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$11;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    :cond_b
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-eqz v1, :cond_c

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 673
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-nez p5, :cond_10

    const/4 v6, 0x0

    :goto_1
    move-object v1, p0

    move-object v3, p2

    move-object v5, p4

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    .line 676
    :cond_c
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-eqz v1, :cond_d

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isTrending()Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 677
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 678
    .local v3, "trendingItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-object v1, p0

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    .line 680
    .end local v3    # "trendingItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_d
    return-void

    .line 546
    .end local v0    # "targetNavigator":Landroid/view/View$OnClickListener;
    :cond_e
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;

    invoke-direct {v0, p4, p5, p0, p3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 554
    .restart local v0    # "targetNavigator":Landroid/view/View$OnClickListener;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v1, :cond_f

    .line 555
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    :cond_f
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 558
    iget-object v9, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$7;

    move-object/from16 v2, p6

    move-object v3, v0

    move-object v4, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$7;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Landroid/view/View$OnClickListener;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 673
    :cond_10
    invoke-interface {p5}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;->getBiForSharedItem()Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    move-result-object v6

    goto :goto_1
.end method

.method public static canShareToShowcase(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Z
    .locals 7
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 354
    const/4 v4, 0x0

    .line 355
    .local v4, "ret":Z
    if-eqz p0, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    .line 357
    .local v1, "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 374
    .end local v1    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :cond_0
    :goto_0
    return v4

    .line 361
    .restart local v1    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 362
    .local v3, "pm":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v3, :cond_0

    .line 363
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    .line 364
    .local v2, "myXuid":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->originalAuthorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .line 365
    .local v0, "ai":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    :goto_1
    if-eqz v0, :cond_0

    .line 366
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    goto :goto_0

    .line 364
    .end local v0    # "ai":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    goto :goto_1

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static detachItemListeners(Landroid/view/View;)V
    .locals 2
    .param p0, "itemView"    # Landroid/view/View;

    .prologue
    .line 336
    const-string v1, "detachItemListeners"

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getViewHolder(Landroid/view/View;Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    .line 337
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    if-eqz v0, :cond_0

    .line 338
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 340
    :cond_0
    return-void
.end method

.method public static detachItemListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V
    .locals 1
    .param p0, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .prologue
    .line 683
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 684
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 686
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 687
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 688
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 689
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 690
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 693
    :cond_0
    return-void
.end method

.method public static getGameClipUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    .locals 5
    .param p0, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, "uri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    if-eqz p0, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    .line 188
    .local v0, "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    sget-object v3, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/DVRVideoType;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    move-object v1, v0

    .line 194
    .end local v0    # "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    :cond_1
    return-object v1
.end method

.method public static getScreenshotUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    .locals 5
    .param p0, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 198
    const/4 v1, 0x0

    .line 199
    .local v1, "uri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    if-eqz p0, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 200
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    .line 201
    .local v0, "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    sget-object v3, Lcom/microsoft/xbox/service/model/ScreenshotType;->Download:Lcom/microsoft/xbox/service/model/ScreenshotType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ScreenshotType;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uriType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    move-object v1, v0

    goto :goto_0

    .line 206
    .end local v0    # "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    :cond_1
    return-object v1
.end method

.method private static getStringWithHttpPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "link"    # Ljava/lang/String;

    .prologue
    .line 716
    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 719
    :cond_0
    return-object p0
.end method

.method private static getViewHolder(Landroid/view/View;Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 4
    .param p0, "itemView"    # Landroid/view/View;
    .param p1, "methodName"    # Ljava/lang/String;

    .prologue
    .line 378
    const/4 v0, 0x0

    .line 379
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    if-eqz p0, :cond_1

    .line 380
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 381
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    if-nez v0, :cond_0

    .line 382
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": holder is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_0
    :goto_0
    return-object v0

    .line 385
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": itemView is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$attachItemListeners$1(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Landroid/view/View;)V
    .locals 2
    .param p0, "onPreClick"    # Ljava/lang/Runnable;
    .param p1, "nav"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p4, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .param p5, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p6, "v"    # Landroid/view/View;

    .prologue
    .line 610
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->runRunnableIfNotNull(Ljava/lang/Runnable;)V

    .line 611
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NavigationUtil$NavigationSelf:[I

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationSelf:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 621
    :cond_0
    :goto_0
    return-void

    .line 613
    :pswitch_0
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 614
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {p3, p2, v0, p4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemContent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 618
    :pswitch_1
    invoke-static {p3, p2, p5, p4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemDetail(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 611
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic lambda$attachItemListeners$2(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Landroid/view/View;)V
    .locals 3
    .param p0, "onPreClick"    # Ljava/lang/Runnable;
    .param p1, "nav"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p4, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .param p5, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p6, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 626
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->runRunnableIfNotNull(Ljava/lang/Runnable;)V

    .line 627
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NavigationUtil$NavigationSelf:[I

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationSelf:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 629
    :pswitch_0
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 630
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->data:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;->leaderboardEntries:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;->userInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {p3, p2, v0, p4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemContent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 634
    :pswitch_1
    invoke-static {p3, p2, p5, p4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemDetail(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 627
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic lambda$attachItemListeners$3(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Landroid/view/View;)V
    .locals 2
    .param p0, "onPreClick"    # Ljava/lang/Runnable;
    .param p1, "nav"    # Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p4, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p5, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .param p6, "v"    # Landroid/view/View;

    .prologue
    .line 642
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->runRunnableIfNotNull(Ljava/lang/Runnable;)V

    .line 643
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NavigationUtil$NavigationSelf:[I

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationSelf:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationSelf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 645
    :pswitch_0
    iget-object v0, p2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgSessionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 646
    invoke-static {p3, p2, p4, p5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemContent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 650
    :pswitch_1
    invoke-static {p3, p2, p4, p5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToItemDetail(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 643
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic lambda$navigateToClub$0(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 2
    .param p0, "clubId"    # J
    .param p2, "c"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 266
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    cmp-long v0, v0, p0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static loadAndPlayGameClip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .prologue
    const/4 v3, 0x1

    .line 838
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$12;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$12;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    invoke-direct {v1, p1, v2}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;)V

    .line 844
    .local v1, "t":Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->load(Z)V

    .line 845
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 846
    .local v0, "adp":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    if-eqz v0, :cond_0

    .line 847
    const-string v2, ""

    invoke-virtual {v0, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 849
    :cond_0
    return-void
.end method

.method public static navigateAchievementToComparison(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p2, "strTitleId"    # Ljava/lang/String;
    .param p3, "platform"    # Ljava/lang/String;

    .prologue
    .line 292
    if-nez p1, :cond_0

    .line 293
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v5, "navigateAchievementToComparison: authorInfo is null"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    .line 298
    .local v1, "xuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 299
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v5, "navigateAchievementToComparison: xuid is null or empty"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 304
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    goto :goto_0

    .line 306
    :cond_2
    const-wide/16 v2, 0x0

    .line 308
    .local v2, "titleId":J
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 313
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_3

    .line 314
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v5, "titleId: xuid is 0"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "ProfileRecentsScreen"

    const-string v5, "failed to parse titleid"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 318
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedTitleId(J)V

    .line 319
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 320
    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public static navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 5
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 343
    if-nez p1, :cond_0

    .line 344
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v2, "navigateToAchievementDetails: item is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :goto_0
    return-void

    .line 347
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 348
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementScid:Ljava/lang/String;

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleDetailInfo(Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;)V

    .line 349
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 350
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/AchievementsDetailsScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public static navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V
    .locals 3
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .param p3, "isCapture"    # Z

    .prologue
    .line 105
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->with(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 112
    return-void
.end method

.method public static navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V
    .locals 3
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .param p3, "isCapture"    # Z

    .prologue
    .line 95
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->with(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 102
    return-void
.end method

.method public static navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V
    .locals 1
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "clubId"    # J

    .prologue
    .line 278
    const/4 v0, 0x1

    invoke-static {p0, v0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;ZJ)V

    .line 279
    return-void
.end method

.method public static navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;ZJ)V
    .locals 8
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "addToStack"    # Z
    .param p2, "clubId"    # J

    .prologue
    .line 260
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 262
    const-wide/16 v6, 0x1

    cmp-long v5, p2, v6

    if-ltz v5, :cond_3

    .line 263
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 264
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 266
    .local v4, "usersClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :goto_0
    invoke-static {p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$1;->lambdaFactory$(J)Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v0

    .line 267
    .local v0, "isMember":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {v4, v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v3, 0x1

    .line 269
    .local v3, "userIsMember":Z
    :goto_1
    if-eqz v3, :cond_2

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreen;

    .line 270
    .local v2, "startPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    :goto_2
    const-class v5, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    new-instance v6, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    invoke-direct {v6, p2, p3, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(JLjava/lang/Class;)V

    invoke-virtual {p0, v5, p1, v6}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 275
    .end local v0    # "isMember":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    .end local v1    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "startPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    .end local v3    # "userIsMember":Z
    .end local v4    # "usersClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :goto_3
    return-void

    .line 264
    .restart local v1    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    goto :goto_0

    .line 267
    .restart local v0    # "isMember":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    .restart local v4    # "usersClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 269
    .restart local v3    # "userIsMember":Z
    :cond_2
    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreen;

    goto :goto_2

    .line 273
    .end local v0    # "isMember":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    .end local v1    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v3    # "userIsMember":Z
    .end local v4    # "usersClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_3
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v6, "navigateToClub: Invalid clubId"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static navigateToGameHub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;JLjava/lang/String;)V
    .locals 1
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "titleId"    # J
    .param p3, "titleName"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->navigateToGameProfile(JLjava/lang/String;)V

    .line 235
    return-void
.end method

.method public static navigateToGameclip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Z
    .locals 5
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 391
    const/4 v2, 0x0

    .line 392
    .local v2, "success":Z
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getGameClipUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    move-result-object v0

    .line 393
    .local v0, "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    if-eqz v0, :cond_0

    .line 394
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedDataSource(Ljava/lang/String;)V

    .line 395
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 396
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setGameClip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .line 397
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 399
    const/4 v2, 0x1

    .line 401
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return v2
.end method

.method private static navigateToItemContent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V
    .locals 6
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p3, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;

    .prologue
    .line 729
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 778
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 779
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 782
    :goto_0
    return-void

    .line 732
    :pswitch_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 738
    :goto_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 736
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    invoke-static {p0, p2, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateAchievementToComparison(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 741
    :pswitch_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastProvider:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 745
    :pswitch_2
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 746
    const v0, 0x7f0704c7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    .line 750
    :goto_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 748
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getXuidForLoadingGameClip()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->loadAndPlayGameClip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V

    goto :goto_2

    .line 753
    :pswitch_3
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotScid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 754
    const v0, 0x7f0704c6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0

    .line 756
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 757
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 758
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 759
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;)V

    goto :goto_0

    .line 763
    :pswitch_4
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->openWebLink(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto/16 :goto_0

    .line 766
    :pswitch_5
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    goto/16 :goto_0

    .line 769
    :pswitch_6
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->lfgSessionId:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToLfgDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 772
    :pswitch_7
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentOrganizer:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->tournamentId:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto/16 :goto_0

    .line 729
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static navigateToItemDetail(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V
    .locals 2
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    .param p3, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;

    .prologue
    .line 785
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 786
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 801
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 804
    :goto_0
    return-void

    .line 788
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 791
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 794
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 797
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 798
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    goto :goto_0

    .line 786
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static navigateToLfgDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;JLjava/lang/String;)V
    .locals 3
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 227
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 228
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->navigateToLfg(JLjava/lang/String;)V

    .line 231
    return-void
.end method

.method public static navigateToPageHub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V
    .locals 0
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->navigateToPageHub(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 239
    return-void
.end method

.method public static navigateToPeopleHubSocialScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "userXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 282
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 283
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 285
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 286
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreen;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 288
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 289
    return-void
.end method

.method public static navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V
    .locals 5
    .param p0    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p3, "requiresTransition"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/PivotActivity;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 878
    .local p0, "destPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/PivotActivity;>;"
    .local p1, "pane":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 879
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 881
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 882
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-nez p3, :cond_0

    if-nez v0, :cond_1

    .line 883
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 884
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2, p0, p2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 891
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_0
    return-void

    .line 886
    .restart local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;

    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->animateToActivePivotPane(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 888
    :catch_0
    move-exception v1

    .line 889
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "NavigationUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to navigate to pivot \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' / pane \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "isCapture"    # Z

    .prologue
    .line 115
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    const/4 v3, 0x1

    .line 115
    invoke-static {p1, v1, v2, v3, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;->with(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZ)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$ActivityFeedActionsScreenParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 122
    return-void
.end method

.method public static navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V
    .locals 2
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "authorInfo"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .prologue
    .line 210
    if-nez p1, :cond_0

    .line 211
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v1, "authorInfo is null"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :goto_0
    return-void

    .line 215
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$AuthorInfo$AuthorType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 221
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :pswitch_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V
    .locals 7
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "userXuid"    # Ljava/lang/String;

    .prologue
    .line 242
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 243
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 246
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToViewProfile()Z

    move-result v2

    .line 249
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->canViewOtherProfiles()Z

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704b5

    .line 250
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061c

    .line 251
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 247
    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public static navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 4
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 127
    return-void
.end method

.method public static navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 405
    const/4 v1, 0x0

    .line 406
    .local v1, "success":Z
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getScreenshotUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    move-result-object v0

    .line 407
    .local v0, "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    if-eqz v0, :cond_0

    .line 408
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uri:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 409
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;)V

    .line 411
    const/4 v1, 0x1

    .line 413
    :cond_0
    return v1
.end method

.method public static navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZJLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 4
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "shareToShowcase"    # Z
    .param p3, "addToStack"    # Z
    .param p4, "clubId"    # J
    .param p6, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p7, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 135
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putActivityFeedItemLocator(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putShareToShowcase(Z)V

    .line 137
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-eqz v2, :cond_0

    .line 138
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedClubId(Ljava/lang/Long;)V

    .line 141
    :cond_0
    invoke-virtual {v1, p6}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)V

    .line 142
    if-eqz p7, :cond_1

    .line 143
    invoke-virtual {v1, p7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineId(Ljava/lang/String;)V

    .line 146
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 147
    .local v0, "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 148
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToFeedScreen;

    invoke-virtual {p0, v2, p3, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 149
    return-void
.end method

.method public static navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 8
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "shareToShowcase"    # Z
    .param p3, "addToStack"    # Z
    .param p4, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p5, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 130
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZJLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public static navigateToShareToMessageScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "addToStack"    # Z

    .prologue
    .line 152
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 154
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putActivityFeedItemLocator(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 156
    .local v0, "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 157
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 160
    :cond_0
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;

    invoke-virtual {p0, v2, p2, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 165
    .end local v0    # "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 162
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "User has no privilege to send message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const v2, 0x7f07061c

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method public static navigateToShareToMessageScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZLcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 4
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "addToStack"    # Z
    .param p3, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 168
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 169
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 170
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putActivityFeedItemLocator(Ljava/lang/String;)V

    .line 171
    if-eqz p3, :cond_0

    .line 172
    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putComposeMessageConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 174
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 175
    .local v0, "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 176
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 177
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;

    invoke-virtual {p0, v2, p2, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 182
    .end local v0    # "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 179
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "User has no privilege to send message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const v2, 0x7f07061c

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method private static onGameClipLoaded(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 852
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 853
    .local v0, "adp":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    if-eqz v0, :cond_0

    .line 854
    const/4 v2, 0x0

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 856
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loaded game clip with status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 875
    :goto_0
    return-void

    .line 861
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "Loaded game clip"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getGameClip()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v1

    .line 863
    .local v1, "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v2, :cond_1

    .line 864
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToGameclip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Z

    goto :goto_0

    .line 866
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "Loaded empty game clip"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    const v2, 0x7f07059a

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0

    .line 872
    .end local v1    # "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "Failed to load game clip"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const v2, 0x7f0705a3

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0

    .line 857
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static openWebLink(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 7
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    const v6, 0x7f0700f4

    .line 696
    if-eqz p1, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->link:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 697
    :cond_0
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    .line 708
    :goto_0
    return-void

    .line 700
    :cond_1
    :try_start_0
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->link:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getStringWithHttpPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 701
    .local v1, "uriString":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 702
    .end local v1    # "uriString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 704
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to open web link: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->postDetails:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->link:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method private static postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V
    .locals 1
    .param p0, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;
    .param p1, "biPoint"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .prologue
    .line 807
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;I)V

    .line 808
    return-void
.end method

.method private static postBIIfExists(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;I)V
    .locals 1
    .param p0, "bi"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;
    .param p1, "biPoint"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;
    .param p2, "indexPos"    # I

    .prologue
    .line 829
    if-eqz p0, :cond_0

    .line 830
    invoke-interface {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;->getEvent(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;

    move-result-object v0

    .line 831
    .local v0, "event":Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;
    if-eqz v0, :cond_0

    .line 832
    invoke-interface {v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;->post(I)V

    .line 835
    .end local v0    # "event":Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;
    :cond_0
    return-void
.end method

.method private static runRunnableIfNotNull(Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 723
    if-eqz p0, :cond_0

    .line 724
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 726
    :cond_0
    return-void
.end method

.method private static toInt(Ljava/lang/String;)I
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 811
    if-nez p0, :cond_0

    .line 812
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "error converting null string to int"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    :goto_0
    return v1

    .line 815
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 816
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 817
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    const-string v3, "error converting empty string to int"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 821
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 822
    :catch_0
    move-exception v0

    .line 823
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot convert "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to int"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
