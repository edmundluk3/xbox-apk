.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;
.source "HomeScreenFeaturedAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;
    }
.end annotation


# static fields
.field private static final ASPECT_RATIO:F = 0.75f

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;

.field private final adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private dataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->getContainerResourceId()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->getPosition()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;-><init>(II)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->section:Landroid/view/View;

    const v1, 0x7f0e06fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->section:Landroid/view/View;

    const v1, 0x7f0e06fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->cellMargin:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setItemMargin(I)V

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->container:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->getListSize()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->computeHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    return-object v0
.end method

.method public static computeAspectRatio(ILcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)F
    .locals 4
    .param p0, "idx"    # I
    .param p1, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemRelatedAspectXY(I)[I

    move-result-object v1

    .line 99
    .local v1, "arBuf":[I
    const/4 v2, 0x0

    aget v2, v1, v2

    int-to-float v2, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 103
    .end local v1    # "arBuf":[I
    .local v0, "ar":F
    :goto_0
    return v0

    .line 101
    .end local v0    # "ar":F
    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->computeAspectRatioOld(I)F

    move-result v0

    .restart local v0    # "ar":F
    goto :goto_0
.end method

.method private static computeAspectRatioOld(I)F
    .locals 2
    .param p0, "idx"    # I

    .prologue
    .line 108
    if-nez p0, :cond_0

    .line 109
    const v0, 0x3faacb46

    .line 115
    .local v0, "ar":F
    :goto_0
    return v0

    .line 110
    .end local v0    # "ar":F
    :cond_0
    add-int/lit8 v1, p0, -0x1

    div-int/lit8 v1, v1, 0x2

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_1

    .line 111
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "ar":F
    goto :goto_0

    .line 113
    .end local v0    # "ar":F
    :cond_1
    const v0, 0x3faa89ab

    .restart local v0    # "ar":F
    goto :goto_0
.end method

.method private computeHeight()I
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->getRootSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->computeCellSize(I)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->cellMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x3f400000    # 0.75f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getListSize()I
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method private postForceLayout()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->section:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method


# virtual methods
.method protected onLayoutChanged()V
    .locals 2

    .prologue
    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 71
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->computeHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->onStart()V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->applyScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 48
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->createScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 53
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->onStop()V

    .line 54
    return-void
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 58
    const-string v1, "HomeScreenFeaturedAdapter"

    const-string v2, "update view is called"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenFeaturedViewModel;->getDataList()Ljava/util/List;

    move-result-object v0

    .line 60
    .local v0, "modelData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    if-eq v0, v1, :cond_0

    .line 61
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->dataList:Ljava/util/List;

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->notifyDataSetChanged()V

    .line 63
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->postForceLayout()V

    .line 65
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->getListSize()I

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->EMPTY:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->ordinal()I

    move-result v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 66
    return-void

    .line 65
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->CONTENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->ordinal()I

    move-result v1

    goto :goto_0
.end method
