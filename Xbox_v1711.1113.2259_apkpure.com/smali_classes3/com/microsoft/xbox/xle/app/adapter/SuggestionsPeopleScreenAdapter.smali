.class public Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "SuggestionsPeopleScreenAdapter.java"


# instance fields
.field private currentPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .line 30
    const v0, 0x7f0e0a91

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->screenBody:Landroid/view/View;

    .line 31
    const v0, 0x7f0e0a92

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03021f

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onResume()V

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->restoreListPosition()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->notifyDataSetChanged()V

    .line 53
    return-void
.end method

.method public onSetActive()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onSetActive()V

    .line 41
    return-void
.end method

.method public onSetInactive()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onSetInactive()V

    .line 46
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->onStop()V

    .line 59
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 73
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->updateLoadingIndicator(Z)V

    .line 74
    const/4 v2, 0x0

    .line 76
    .local v2, "notifyAdapter":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 77
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->currentPeople:Ljava/util/List;

    if-eq v0, v3, :cond_0

    .line 78
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->clear()V

    .line 79
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->addAll(Ljava/util/Collection;)V

    .line 80
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->currentPeople:Ljava/util/List;

    .line 81
    const/4 v2, 0x1

    .line 85
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    .line 86
    .local v1, "newVmState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v3, v1, :cond_1

    .line 87
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->currentVmState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 88
    const/4 v2, 0x1

    .line 91
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->notifyDataSetChanged()V

    .line 94
    :cond_2
    return-void
.end method
