.class public Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TVEpisodeOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 15
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 16
    return-void
.end method


# virtual methods
.method public getEpisodeExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "TODO: Hookup to live data"

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f070683

    return v0
.end method

.method public getHasParentSeasons()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getNetwork()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReleaseData()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 51
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f071084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "S"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getSeasonNumber()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", Ep"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    .line 52
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getEpisodeNumber()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v3, 0x2

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 51
    invoke-static {v1, v6, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->getDurationInMinutes()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07061b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getSeriesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public navigateToParentSeries()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 44
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 21
    return-void
.end method
