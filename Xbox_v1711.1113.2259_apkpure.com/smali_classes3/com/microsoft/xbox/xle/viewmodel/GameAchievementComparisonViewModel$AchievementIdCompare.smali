.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;
.super Ljava/lang/Object;
.source "GameAchievementComparisonViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AchievementIdCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;

    .prologue
    .line 1044
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)I
    .locals 7
    .param p1, "first"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .param p2, "second"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 1048
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 1064
    :cond_0
    :goto_0
    return v2

    .line 1050
    :cond_1
    if-nez p1, :cond_2

    move v2, v3

    .line 1051
    goto :goto_0

    .line 1052
    :cond_2
    if-nez p2, :cond_3

    move v2, v4

    .line 1053
    goto :goto_0

    .line 1056
    :cond_3
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v0

    .line 1057
    .local v0, "firstVal":I
    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    const/4 v6, -0x2

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v1

    .line 1059
    .local v1, "secondVal":I
    if-le v0, v1, :cond_4

    move v2, v3

    .line 1060
    goto :goto_0

    .line 1061
    :cond_4
    if-ge v0, v1, :cond_0

    move v2, v4

    .line 1062
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1044
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;->compare(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)I

    move-result v0

    return v0
.end method
