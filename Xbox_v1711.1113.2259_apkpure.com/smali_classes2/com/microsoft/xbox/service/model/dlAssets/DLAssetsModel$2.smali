.class final Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;
.super Ljava/lang/Object;
.source "DLAssetsModel.java"

# interfaces
.implements Lretrofit2/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->getGamerpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/Callback",
        "<",
        "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$failureCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

.field final synthetic val$successCallback:Lcom/microsoft/xbox/toolkit/generics/Action;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$successCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$failureCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V
    .locals 2
    .param p2, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to get gamerpic list"

    invoke-static {v0, v1, p2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$failureCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$failureCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 86
    :cond_0
    return-void
.end method

.method public onResponse(Lretrofit2/Call;Lretrofit2/Response;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
            ">;",
            "Lretrofit2/Response",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;>;"
    .local p2, "response":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v0, "gamerpicList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;

    .line 69
    .local v1, "gamerpicManifest":Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;->gamerpics()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    .local v2, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    if-eqz v2, :cond_0

    .line 70
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;

    .line 71
    .local v3, "picItem":Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "https://dlassets-ssl.xboxlive.com/public/content/ppl/gamerpics/%s-md.png"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;->id()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    .end local v2    # "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    .end local v3    # "picItem":Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$successCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    if-eqz v4, :cond_1

    .line 76
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;->val$successCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v4, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 78
    :cond_1
    return-void
.end method
