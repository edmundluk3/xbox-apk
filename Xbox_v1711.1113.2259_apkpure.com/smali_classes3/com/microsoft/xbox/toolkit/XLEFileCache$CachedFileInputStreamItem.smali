.class Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;
.super Ljava/lang/Object;
.source "XLEFileCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/XLEFileCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CachedFileInputStreamItem"
.end annotation


# instance fields
.field private computedMd5:[B

.field private contentInputStream:Ljava/io/InputStream;

.field private mDigest:Ljava/security/MessageDigest;

.field private savedMd5:[B

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/XLEFileCache;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/XLEFileCache;Lcom/microsoft/xbox/toolkit/XLEFileCacheItemKey;Ljava/io/File;)V
    .locals 15
    .param p2, "key"    # Lcom/microsoft/xbox/toolkit/XLEFileCacheItemKey;
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->this$0:Lcom/microsoft/xbox/toolkit/XLEFileCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    .line 187
    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p3

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 188
    .local v8, "wrappedFileInputStream":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 190
    .local v5, "keyLength":I
    :try_start_0
    const-string v9, "MD5"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    .line 191
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    invoke-virtual {v9}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v9

    new-array v9, v9, [B

    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->savedMd5:[B

    .line 192
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->savedMd5:[B

    invoke-virtual {v8, v9}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    .line 193
    .local v6, "length":I
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    invoke-virtual {v9}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v9

    if-eq v6, v9, :cond_0

    .line 194
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 195
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Ddigest lengh check failed!"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 205
    .end local v6    # "length":I
    :catch_0
    move-exception v4

    .line 206
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 207
    new-instance v9, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "File digest failed! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 198
    .end local v4    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v6    # "length":I
    :cond_0
    :try_start_1
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEFileCache;->access$000(Ljava/io/InputStream;)I

    move-result v5

    .line 199
    new-array v2, v5, [B

    .line 200
    .local v2, "cacheItemKey":[B
    invoke-virtual {v8, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v7

    .line 201
    .local v7, "readKeyLength":I
    if-ne v5, v7, :cond_1

    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/XLEFileCacheItemKey;->getKeyString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 202
    :cond_1
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 203
    new-instance v9, Ljava/io/IOException;

    const-string v10, "File key check failed because keyLength != readKeyLength or !key.getKeyString().equals(new String(urlOrSomething))"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 208
    .end local v2    # "cacheItemKey":[B
    .end local v6    # "length":I
    .end local v7    # "readKeyLength":I
    :catch_1
    move-exception v4

    .line 209
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    const-string v9, "XLEFileCache"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "OutOfMemoryError with keyLength=%d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 211
    new-instance v9, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "File digest failed! Out of memory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 214
    .end local v4    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "cacheItemKey":[B
    .restart local v6    # "length":I
    .restart local v7    # "readKeyLength":I
    :cond_2
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 215
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-static {v1, v8}, Lcom/microsoft/xbox/toolkit/StreamUtil;->CopyStream(Ljava/io/OutputStream;Ljava/io/InputStream;)V

    .line 216
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 217
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 218
    .local v3, "content":[B
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    invoke-virtual {v9, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 219
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    invoke-virtual {v9}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->computedMd5:[B

    .line 220
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->isMd5Error()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 221
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    .line 222
    new-instance v9, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "the saved md5 is not equal computed md5.ComputedMd5:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->computedMd5:[B

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "     SavedMd5:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->savedMd5:[B

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 225
    :cond_3
    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-direct {v9, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->contentInputStream:Ljava/io/InputStream;

    .line 226
    return-void
.end method

.method private isMd5Error()Z
    .locals 3

    .prologue
    .line 233
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->mDigest:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->savedMd5:[B

    aget-byte v1, v1, v0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->computedMd5:[B

    aget-byte v2, v2, v0

    if-eq v1, v2, :cond_0

    .line 235
    const/4 v1, 0x1

    .line 238
    :goto_1
    return v1

    .line 233
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getContentInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEFileCache$CachedFileInputStreamItem;->contentInputStream:Ljava/io/InputStream;

    return-object v0
.end method
