.class Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateProfileRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private setting:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "setting"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .param p4, "value"    # Ljava/lang/String;

    .prologue
    .line 2533
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2534
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2535
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->setting:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 2536
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->value:Ljava/lang/String;

    .line 2537
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2541
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->setting:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->value:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->updateUserProfileInfo(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2528
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2550
    const-wide/16 v0, 0xbba

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2555
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->setting:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$UpdateProfileRunner;->value:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$1700(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)V

    .line 2556
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2546
    return-void
.end method
