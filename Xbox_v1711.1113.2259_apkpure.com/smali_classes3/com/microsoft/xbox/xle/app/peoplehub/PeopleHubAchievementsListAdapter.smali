.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PeopleHubAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final ACHIEVEMENT_HEADER_LAYOUT:I = 0x7f0301bc

.field private static final ACHIEVEMENT_ITEM_LAYOUT:I = 0x7f0301bf

.field private static final LEADERBOARD_HEADER_LAYOUT:I = 0x7f0301bd

.field private static final LEADERBOARD_ITEM_LAYOUT:I = 0x7f0301be

.field private static final TAG:Ljava/lang/String;

.field private static final xbox360ImageUrlFormat:Ljava/lang/String; = "http://tiles.xbox.com/consoleAssets/%s/%s/largeboxart.jpg"


# instance fields
.field private gamerscoreLeaderboardInsertViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

.field private peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;)V
    .locals 0
    .param p1, "peopleHubAchievementsScreenViewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p2, "gamerscoreLeaderboardInsertViewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .line 77
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->gamerscoreLeaderboardInsertViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->gamerscoreLeaderboardInsertViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->peopleHubAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    .line 112
    .local v0, "item":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const v1, 0x7f0301bd

    .line 122
    :goto_0
    return v1

    .line 114
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isAchievementHeader()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    const v1, 0x7f0301bc

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isLeaderboardItem()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    const v1, 0x7f0301be

    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isAchievementItem()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 119
    const v1, 0x7f0301bf

    goto :goto_0

    .line 121
    :cond_3
    const-string v1, "Attempted to getItemViewType for an unsupported PeopleHubAchivementsCompositeDataItem"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 122
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 2
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    .line 103
    .local v0, "compositeItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 106
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewTypeLayout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 83
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 85
    .local v1, "rootView":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 95
    const-string v2, "Attempted to create an invalid view holder"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 96
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 87
    :pswitch_0
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 89
    :pswitch_1
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 91
    :pswitch_2
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 93
    :pswitch_3
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x7f0301bc
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
