.class public Lcom/microsoft/xbox/xle/ui/XLECheckableButton;
.super Lcom/microsoft/xbox/toolkit/ui/XLEButton;
.source "XLECheckableButton.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private mChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->mChecked:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 24
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 25
    .local v0, "drawableState":[I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    sget-object v1, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->mergeDrawableStates([I[I)[I

    .line 29
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->mChecked:Z

    if-ne v0, p1, :cond_0

    .line 53
    :goto_0
    return-void

    .line 48
    :cond_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->mChecked:Z

    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->refreshDrawableState()V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/XLECheckableButton;->setChecked(Z)V

    .line 58
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
