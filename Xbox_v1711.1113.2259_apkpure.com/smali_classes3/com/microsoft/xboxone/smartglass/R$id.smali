.class public final Lcom/microsoft/xboxone/smartglass/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CropOverlayView:I = 0x7f0e04e2

.field public static final CropProgressBar:I = 0x7f0e04e3

.field public static final Hidden:I = 0x7f0e0094

.field public static final ImageView_image:I = 0x7f0e04e1

.field public static final Private:I = 0x7f0e0095

.field public static final ProgressBar:I = 0x7f0e0322

.field public static final Public:I = 0x7f0e0096

.field public static final about_activity_body:I = 0x7f0e010d

.field public static final about_activity_layout:I = 0x7f0e010c

.field public static final about_content:I = 0x7f0e010e

.field public static final accelerator_background:I = 0x7f0e058c

.field public static final accelerator_slider:I = 0x7f0e058d

.field public static final acceptButton:I = 0x7f0e0ab4

.field public static final accountPickerBase:I = 0x7f0e010f

.field public static final achievement_btn_compare_icon:I = 0x7f0e0115

.field public static final achievement_btn_share_icon:I = 0x7f0e0117

.field public static final achievement_btn_share_text:I = 0x7f0e0118

.field public static final achievement_btn_text:I = 0x7f0e0116

.field public static final achievement_comparison_achievement_me_layout:I = 0x7f0e03fc

.field public static final achievement_comparison_achievement_you_layout:I = 0x7f0e0407

.field public static final achievement_comparison_gamerscores_me_layout:I = 0x7f0e03f8

.field public static final achievement_comparison_gamerscores_you_layout:I = 0x7f0e0403

.field public static final achievement_comparison_list:I = 0x7f0e05fd

.field public static final achievement_comparison_me_achievement:I = 0x7f0e03fd

.field public static final achievement_comparison_me_gamerpic:I = 0x7f0e03f7

.field public static final achievement_comparison_me_gamerscore:I = 0x7f0e03f9

.field public static final achievement_comparison_me_gamerscore_change:I = 0x7f0e03fa

.field public static final achievement_comparison_me_gamerscore_month:I = 0x7f0e03fb

.field public static final achievement_comparison_me_gamertag:I = 0x7f0e03f6

.field public static final achievement_comparison_me_rare:I = 0x7f0e0400

.field public static final achievement_comparison_nodata:I = 0x7f0e05ff

.field public static final achievement_comparison_rare_me_layout:I = 0x7f0e03fe

.field public static final achievement_comparison_rare_you_layout:I = 0x7f0e0409

.field public static final achievement_comparison_screen_body:I = 0x7f0e05fa

.field public static final achievement_comparison_switch_panel:I = 0x7f0e05fe

.field public static final achievement_comparison_title:I = 0x7f0e05fb

.field public static final achievement_comparison_you_achievement:I = 0x7f0e0408

.field public static final achievement_comparison_you_gamerpic:I = 0x7f0e0402

.field public static final achievement_comparison_you_gamerscore:I = 0x7f0e0404

.field public static final achievement_comparison_you_gamerscore_change:I = 0x7f0e0405

.field public static final achievement_comparison_you_gamerscore_month:I = 0x7f0e0406

.field public static final achievement_comparison_you_gamertag:I = 0x7f0e0401

.field public static final achievement_comparison_you_rare:I = 0x7f0e040a

.field public static final achievements:I = 0x7f0e07cf

.field public static final achievements_cotnainer:I = 0x7f0e07cd

.field public static final achievements_friends_who_earned_listitem_achievement_text:I = 0x7f0e011b

.field public static final achievements_friends_who_earned_listitem_friend_name:I = 0x7f0e011a

.field public static final achievements_friends_who_earned_listitem_image:I = 0x7f0e0119

.field public static final achievements_list_row_image_container:I = 0x7f0e068c

.field public static final action0:I = 0x7f0e07e2

.field public static final action_bar:I = 0x7f0e00fd

.field public static final action_bar_activity_content:I = 0x7f0e0000

.field public static final action_bar_container:I = 0x7f0e00fc

.field public static final action_bar_root:I = 0x7f0e00f8

.field public static final action_bar_spinner:I = 0x7f0e0001

.field public static final action_bar_subtitle:I = 0x7f0e00db

.field public static final action_bar_title:I = 0x7f0e00da

.field public static final action_container:I = 0x7f0e07df

.field public static final action_context_bar:I = 0x7f0e00fe

.field public static final action_dismiss:I = 0x7f0e0c15

.field public static final action_divider:I = 0x7f0e07e6

.field public static final action_image:I = 0x7f0e07e0

.field public static final action_menu_divider:I = 0x7f0e0002

.field public static final action_menu_presenter:I = 0x7f0e0003

.field public static final action_mode_bar:I = 0x7f0e00fa

.field public static final action_mode_bar_stub:I = 0x7f0e00f9

.field public static final action_mode_close_button:I = 0x7f0e00dc

.field public static final action_settings:I = 0x7f0e0c22

.field public static final action_text:I = 0x7f0e07e1

.field public static final actions:I = 0x7f0e07ef

.field public static final activity_alert_container:I = 0x7f0e0122

.field public static final activity_alert_list:I = 0x7f0e0125

.field public static final activity_alert_listitem_action:I = 0x7f0e0121

.field public static final activity_alert_listitem_date:I = 0x7f0e0120

.field public static final activity_alert_listitem_gamertag:I = 0x7f0e011e

.field public static final activity_alert_listitem_image:I = 0x7f0e011c

.field public static final activity_alert_listitem_realname:I = 0x7f0e011f

.field public static final activity_alert_loading_spinner:I = 0x7f0e0127

.field public static final activity_alert_nodata:I = 0x7f0e0126

.field public static final activity_alert_switch_panel:I = 0x7f0e0124

.field public static final activity_alert_title_text:I = 0x7f0e0123

.field public static final activity_chooser_view_content:I = 0x7f0e00dd

.field public static final activity_feed_action_delete:I = 0x7f0e0c1e

.field public static final activity_feed_action_hide:I = 0x7f0e0c16

.field public static final activity_feed_action_hide_club_feed:I = 0x7f0e0c17

.field public static final activity_feed_action_hide_title_feed:I = 0x7f0e0c18

.field public static final activity_feed_action_list_row_gamer_content:I = 0x7f0e0128

.field public static final activity_feed_action_list_row_gamerpic:I = 0x7f0e0129

.field public static final activity_feed_action_list_row_gamertag:I = 0x7f0e012a

.field public static final activity_feed_action_list_row_realname:I = 0x7f0e012b

.field public static final activity_feed_action_list_row_spinner:I = 0x7f0e012f

.field public static final activity_feed_action_list_row_text:I = 0x7f0e012d

.field public static final activity_feed_action_list_row_timestamp:I = 0x7f0e012c

.field public static final activity_feed_action_pin:I = 0x7f0e0c1c

.field public static final activity_feed_action_report:I = 0x7f0e0c19

.field public static final activity_feed_action_report_to_admin:I = 0x7f0e0c1b

.field public static final activity_feed_action_report_to_xbox:I = 0x7f0e0c1a

.field public static final activity_feed_action_social_bar:I = 0x7f0e012e

.field public static final activity_feed_action_unpin:I = 0x7f0e0c1d

.field public static final activity_feed_actions_comment_container:I = 0x7f0e0137

.field public static final activity_feed_actions_feed_item_container:I = 0x7f0e0134

.field public static final activity_feed_actions_feed_item_container_switch_panel:I = 0x7f0e0133

.field public static final activity_feed_actions_list_view:I = 0x7f0e0136

.field public static final activity_feed_actions_root:I = 0x7f0e0135

.field public static final activity_feed_add_link_get_preview_button:I = 0x7f0e0139

.field public static final activity_feed_add_link_preview_add_to_post:I = 0x7f0e0140

.field public static final activity_feed_add_link_preview_container:I = 0x7f0e013b

.field public static final activity_feed_add_link_preview_error_text:I = 0x7f0e013f

.field public static final activity_feed_add_link_preview_image:I = 0x7f0e013d

.field public static final activity_feed_add_link_preview_image_icon:I = 0x7f0e013e

.field public static final activity_feed_add_link_preview_image_layout:I = 0x7f0e013c

.field public static final activity_feed_add_link_text_entry:I = 0x7f0e013a

.field public static final activity_feed_add_link_title:I = 0x7f0e0138

.field public static final activity_feed_add_link_to_post_cancel:I = 0x7f0e0141

.field public static final activity_feed_comment_inline_send:I = 0x7f0e0132

.field public static final activity_feed_comment_inline_text_entry:I = 0x7f0e0131

.field public static final activity_feed_filters_apply:I = 0x7f0e0154

.field public static final activity_feed_filters_clubs:I = 0x7f0e014d

.field public static final activity_feed_filters_clubs_checkbox:I = 0x7f0e014b

.field public static final activity_feed_filters_clubs_subtitle:I = 0x7f0e014c

.field public static final activity_feed_filters_favorites:I = 0x7f0e014a

.field public static final activity_feed_filters_favorites_checkbox:I = 0x7f0e0148

.field public static final activity_feed_filters_favorites_subtitle:I = 0x7f0e0149

.field public static final activity_feed_filters_friends:I = 0x7f0e0147

.field public static final activity_feed_filters_friends_checkbox:I = 0x7f0e0145

.field public static final activity_feed_filters_friends_subtitle:I = 0x7f0e0146

.field public static final activity_feed_filters_games:I = 0x7f0e0150

.field public static final activity_feed_filters_games_checkbox:I = 0x7f0e014e

.field public static final activity_feed_filters_games_subtitle:I = 0x7f0e014f

.field public static final activity_feed_filters_popular:I = 0x7f0e0153

.field public static final activity_feed_filters_popular_checkbox:I = 0x7f0e0151

.field public static final activity_feed_filters_popular_subtitle:I = 0x7f0e0152

.field public static final activity_feed_filters_subtitle:I = 0x7f0e0144

.field public static final activity_feed_filters_title:I = 0x7f0e0143

.field public static final activity_feed_filters_title_frame:I = 0x7f0e0142

.field public static final activity_feed_header:I = 0x7f0e06c1

.field public static final activity_feed_post_checkbox:I = 0x7f0e016a

.field public static final activity_feed_post_link_image:I = 0x7f0e016d

.field public static final activity_feed_recommendation_close:I = 0x7f0e06ca

.field public static final activity_feed_recommendation_header:I = 0x7f0e06c6

.field public static final activity_feed_recommendation_header_text:I = 0x7f0e06c7

.field public static final activity_feed_recommendation_profile_view:I = 0x7f0e06c9

.field public static final activity_feed_recommendation_see_all_button:I = 0x7f0e06c8

.field public static final activity_feed_share_checkbox:I = 0x7f0e015e

.field public static final activity_feed_share_to_feed_button_cancel:I = 0x7f0e0161

.field public static final activity_feed_share_to_feed_button_container:I = 0x7f0e015f

.field public static final activity_feed_share_to_feed_button_share:I = 0x7f0e0160

.field public static final activity_feed_share_to_feed_caption:I = 0x7f0e0158

.field public static final activity_feed_share_to_feed_close_button:I = 0x7f0e0157

.field public static final activity_feed_share_to_feed_count:I = 0x7f0e015d

.field public static final activity_feed_share_to_feed_error:I = 0x7f0e0162

.field public static final activity_feed_share_to_feed_item_container:I = 0x7f0e015a

.field public static final activity_feed_share_to_feed_main_container:I = 0x7f0e015c

.field public static final activity_feed_share_to_feed_root:I = 0x7f0e0156

.field public static final activity_feed_share_to_feed_scroll_container:I = 0x7f0e0159

.field public static final activity_feed_share_to_feed_text_entry:I = 0x7f0e015b

.field public static final activity_feed_share_to_feed_xle_root:I = 0x7f0e0155

.field public static final activity_feed_share_to_message_item_container:I = 0x7f0e0421

.field public static final activity_feed_status_post_add_link:I = 0x7f0e016b

.field public static final activity_feed_status_post_close:I = 0x7f0e0166

.field public static final activity_feed_status_post_container:I = 0x7f0e0163

.field public static final activity_feed_status_post_count:I = 0x7f0e016c

.field public static final activity_feed_status_post_error:I = 0x7f0e016e

.field public static final activity_feed_status_post_link_preview_layout:I = 0x7f0e0165

.field public static final activity_feed_status_post_send:I = 0x7f0e0169

.field public static final activity_feed_status_post_text_entry:I = 0x7f0e0168

.field public static final activity_feed_status_post_text_entry_layout:I = 0x7f0e0167

.field public static final activity_feed_status_post_text_layout:I = 0x7f0e0164

.field public static final activity_feed_unshared_list:I = 0x7f0e0173

.field public static final activity_feed_unshared_list_swipe_container:I = 0x7f0e0172

.field public static final activity_feed_unshared_screen_body:I = 0x7f0e016f

.field public static final activity_feed_unshared_screen_close:I = 0x7f0e0170

.field public static final activity_feed_unshared_screen_header:I = 0x7f0e0171

.field public static final activity_loading_spinner:I = 0x7f0e06c4

.field public static final activity_overview_activity:I = 0x7f0e0174

.field public static final activity_overview_activity_body:I = 0x7f0e0175

.field public static final activity_overview_description:I = 0x7f0e017a

.field public static final activity_overview_device_requirement:I = 0x7f0e0182

.field public static final activity_overview_pegi_ratingview:I = 0x7f0e017c

.field public static final activity_overview_play:I = 0x7f0e0177

.field public static final activity_overview_play_btn_icon:I = 0x7f0e0178

.field public static final activity_overview_play_btn_text:I = 0x7f0e0179

.field public static final activity_overview_providers:I = 0x7f0e0181

.field public static final activity_overview_providers_layout:I = 0x7f0e0180

.field public static final activity_overview_publisher:I = 0x7f0e017f

.field public static final activity_overview_publisher_layout:I = 0x7f0e017e

.field public static final activity_overview_ratingview:I = 0x7f0e017b

.field public static final activity_overview_release_year:I = 0x7f0e017d

.field public static final activity_overview_switch_panel:I = 0x7f0e0176

.field public static final add:I = 0x7f0e008c

.field public static final add_as_favorite:I = 0x7f0e0238

.field public static final add_as_friend:I = 0x7f0e0237

.field public static final add_people_frame:I = 0x7f0e0479

.field public static final add_tags_btn_icon:I = 0x7f0e072a

.field public static final add_tags_btn_text:I = 0x7f0e072b

.field public static final adjust_height:I = 0x7f0e00b7

.field public static final adjust_width:I = 0x7f0e00b8

.field public static final airing_item_channel_name:I = 0x7f0e019e

.field public static final airing_item_channel_number:I = 0x7f0e019f

.field public static final airing_item_provider:I = 0x7f0e01a2

.field public static final airing_item_title:I = 0x7f0e01a1

.field public static final airing_list_row_text_container:I = 0x7f0e01a0

.field public static final airing_logo_image:I = 0x7f0e019d

.field public static final album_detail_item_duration:I = 0x7f0e01dc

.field public static final album_detail_item_title:I = 0x7f0e01db

.field public static final album_detail_list_switch_panel:I = 0x7f0e01ae

.field public static final album_details_activity_body:I = 0x7f0e01a4

.field public static final album_details_artist:I = 0x7f0e01a9

.field public static final album_details_button_primary:I = 0x7f0e01af

.field public static final album_details_explicit_text:I = 0x7f0e01ab

.field public static final album_details_image_tile:I = 0x7f0e01a8

.field public static final album_details_pin_button:I = 0x7f0e01b0

.field public static final album_details_release_date_and_studio:I = 0x7f0e01aa

.field public static final album_details_scrollview:I = 0x7f0e01a5

.field public static final album_details_title:I = 0x7f0e01a7

.field public static final album_details_title_small:I = 0x7f0e01ad

.field public static final alertTitle:I = 0x7f0e00f1

.field public static final all:I = 0x7f0e0074

.field public static final always:I = 0x7f0e00b9

.field public static final anyone_can__title_textview:I = 0x7f0e031c

.field public static final anyone_can_textview:I = 0x7f0e031d

.field public static final appdetail_header_screen_body:I = 0x7f0e01b2

.field public static final appdetail_image_tile:I = 0x7f0e01b3

.field public static final appdetail_overview_description_text:I = 0x7f0e01c4

.field public static final appdetail_overview_genre_container:I = 0x7f0e01c8

.field public static final appdetail_overview_genre_text:I = 0x7f0e01c3

.field public static final appdetail_overview_genre_title:I = 0x7f0e01c9

.field public static final appdetail_overview_genre_value:I = 0x7f0e01ca

.field public static final appdetail_overview_help_btn_icon:I = 0x7f0e01bd

.field public static final appdetail_overview_help_btn_text:I = 0x7f0e01be

.field public static final appdetail_overview_languages_container:I = 0x7f0e01d1

.field public static final appdetail_overview_languages_title:I = 0x7f0e01d2

.field public static final appdetail_overview_languages_value:I = 0x7f0e01d3

.field public static final appdetail_overview_launch_app_help:I = 0x7f0e01bc

.field public static final appdetail_overview_pin_button:I = 0x7f0e01bb

.field public static final appdetail_overview_play_button:I = 0x7f0e01b9

.field public static final appdetail_overview_publisher_container:I = 0x7f0e01c5

.field public static final appdetail_overview_publisher_title:I = 0x7f0e01c6

.field public static final appdetail_overview_publisher_value:I = 0x7f0e01c7

.field public static final appdetail_overview_purchase_button:I = 0x7f0e01ba

.field public static final appdetail_overview_purchased_date:I = 0x7f0e01c1

.field public static final appdetail_overview_purchased_label:I = 0x7f0e01c0

.field public static final appdetail_overview_purchased_layout:I = 0x7f0e01bf

.field public static final appdetail_overview_rating_container:I = 0x7f0e01cb

.field public static final appdetail_overview_rating_title:I = 0x7f0e01cc

.field public static final appdetail_overview_rating_value:I = 0x7f0e01cd

.field public static final appdetail_overview_release_date_container:I = 0x7f0e01ce

.field public static final appdetail_overview_release_date_title:I = 0x7f0e01cf

.field public static final appdetail_overview_release_date_value:I = 0x7f0e01d0

.field public static final appdetail_overview_release_year_text:I = 0x7f0e01c2

.field public static final appdetail_overview_screen_body:I = 0x7f0e01b7

.field public static final appdetail_overview_switch_panel:I = 0x7f0e01b8

.field public static final appdetail_star_raitings:I = 0x7f0e01b5

.field public static final appdetail_title:I = 0x7f0e01b4

.field public static final appdetail_title_small:I = 0x7f0e01b6

.field public static final artist_album_tile_image:I = 0x7f0e01da

.field public static final artist_detail_header_body:I = 0x7f0e01d4

.field public static final artist_details_header_extra_text:I = 0x7f0e01d8

.field public static final artist_details_header_tile:I = 0x7f0e01d5

.field public static final artist_details_header_tile_bottom_space:I = 0x7f0e01d7

.field public static final artist_details_header_title:I = 0x7f0e01d6

.field public static final artist_details_header_title_small:I = 0x7f0e01d9

.field public static final artistdetail_albums_list:I = 0x7f0e01df

.field public static final artistdetail_albums_screen_body:I = 0x7f0e01dd

.field public static final artistdetail_albums_switch_panel:I = 0x7f0e01de

.field public static final artistdetail_biography_screen_body:I = 0x7f0e01e0

.field public static final artistdetail_biography_switch_panel:I = 0x7f0e01e1

.field public static final artistdetail_biography_text:I = 0x7f0e01e2

.field public static final artistdetail_topsongs_list:I = 0x7f0e01e9

.field public static final artistdetail_topsongs_pin_button:I = 0x7f0e01e7

.field public static final artistdetail_topsongs_play_btn_icon:I = 0x7f0e01e5

.field public static final artistdetail_topsongs_play_btn_text:I = 0x7f0e01e6

.field public static final artistdetail_topsongs_play_button:I = 0x7f0e01e4

.field public static final artistdetail_topsongs_screen_body:I = 0x7f0e01e3

.field public static final artistdetail_topsongs_switch_panel:I = 0x7f0e01e8

.field public static final attachment_indicator:I = 0x7f0e0484

.field public static final attachment_loading_error_text:I = 0x7f0e01eb

.field public static final attainment_detail_attainment_description:I = 0x7f0e01f1

.field public static final attainment_detail_attainment_image:I = 0x7f0e020a

.field public static final attainment_detail_attainment_image_layout:I = 0x7f0e0209

.field public static final attainment_detail_attainment_rarity:I = 0x7f0e01f2

.field public static final attainment_detail_attainment_title:I = 0x7f0e01ec

.field public static final attainment_detail_btn_club_change:I = 0x7f0e0205

.field public static final attainment_detail_btn_goto:I = 0x7f0e0202

.field public static final attainment_detail_btn_goto_text:I = 0x7f0e0203

.field public static final attainment_detail_btn_ring_icon_share:I = 0x7f0e0200

.field public static final attainment_detail_change_club_background_button:I = 0x7f0e0204

.field public static final attainment_detail_comparison_item_me_bar:I = 0x7f0e01f8

.field public static final attainment_detail_comparison_item_you_bar:I = 0x7f0e01fc

.field public static final attainment_detail_description_text_me_layout:I = 0x7f0e01f5

.field public static final attainment_detail_description_text_you_layout:I = 0x7f0e01f9

.field public static final attainment_detail_gamerscore_icon:I = 0x7f0e01ef

.field public static final attainment_detail_gamerscore_text:I = 0x7f0e01f0

.field public static final attainment_detail_goto_button:I = 0x7f0e0201

.field public static final attainment_detail_ingame_reward_label:I = 0x7f0e01fd

.field public static final attainment_detail_list_header_text:I = 0x7f0e0206

.field public static final attainment_detail_me_text:I = 0x7f0e01f6

.field public static final attainment_detail_rarity_icon:I = 0x7f0e01ee

.field public static final attainment_detail_reward_icon:I = 0x7f0e01ed

.field public static final attainment_detail_rewards_layout:I = 0x7f0e01fe

.field public static final attainment_detail_screen_body:I = 0x7f0e0207

.field public static final attainment_detail_screen_container:I = 0x7f0e0208

.field public static final attainment_detail_share_button:I = 0x7f0e01ff

.field public static final attainment_detail_switch_panel:I = 0x7f0e020b

.field public static final attainment_detail_time_remaining_label:I = 0x7f0e01f4

.field public static final attainment_detail_time_remaining_text:I = 0x7f0e01f3

.field public static final attainment_detail_unlocked_date_me_text:I = 0x7f0e01f7

.field public static final attainment_detail_unlocked_date_you_text:I = 0x7f0e01fb

.field public static final attainment_detail_you_text:I = 0x7f0e01fa

.field public static final auto:I = 0x7f0e0093

.field public static final automatic:I = 0x7f0e00d5

.field public static final average_user_rating:I = 0x7f0e0a59

.field public static final baseScreenBody:I = 0x7f0e0210

.field public static final baseScreenHeader:I = 0x7f0e020f

.field public static final baseScreenProgressView:I = 0x7f0e020e

.field public static final baseScreenView:I = 0x7f0e020d

.field public static final basic:I = 0x7f0e0075

.field public static final bat_full_state:I = 0x7f0e0004

.field public static final beginning:I = 0x7f0e00b5

.field public static final blocking_dialog_cancel:I = 0x7f0e021b

.field public static final blocking_dialog_container:I = 0x7f0e021a

.field public static final blocking_dialog_progress_bar:I = 0x7f0e0211

.field public static final blocking_dialog_status_text:I = 0x7f0e0212

.field public static final blocking_layer:I = 0x7f0e09b9

.field public static final blocking_text:I = 0x7f0e09ba

.field public static final bottom:I = 0x7f0e0097

.field public static final bottom_button_layout:I = 0x7f0e0232

.field public static final box_count:I = 0x7f0e00d2

.field public static final btn_cancel:I = 0x7f0e055e

.field public static final btn_more:I = 0x7f0e0bb7

.field public static final btn_off:I = 0x7f0e0a39

.field public static final btn_on:I = 0x7f0e0a35

.field public static final btn_power:I = 0x7f0e0bb3

.field public static final btn_save_real_name:I = 0x7f0e055f

.field public static final bundles_list:I = 0x7f0e0217

.field public static final bundles_listitem_image:I = 0x7f0e0213

.field public static final bundles_listitem_title:I = 0x7f0e0214

.field public static final bundles_screen_body:I = 0x7f0e0215

.field public static final bundles_switch_panel:I = 0x7f0e0216

.field public static final button:I = 0x7f0e00d3

.field public static final buttonPanel:I = 0x7f0e00e4

.field public static final button_about:I = 0x7f0e057c

.field public static final button_favorite:I = 0x7f0e056e

.field public static final button_next:I = 0x7f0e0bd1

.field public static final button_previous:I = 0x7f0e0bd0

.field public static final button_stream:I = 0x7f0e057e

.field public static final button_tune:I = 0x7f0e057d

.field public static final call_me_image:I = 0x7f0e0966

.field public static final cancelButton:I = 0x7f0e0ab5

.field public static final cancel_action:I = 0x7f0e07e3

.field public static final cancel_button:I = 0x7f0e023b

.field public static final canvas_body:I = 0x7f0e021e

.field public static final canvas_companion_cancel:I = 0x7f0e0226

.field public static final canvas_companion_description:I = 0x7f0e0222

.field public static final canvas_companion_loading_text:I = 0x7f0e0225

.field public static final canvas_companion_name:I = 0x7f0e0221

.field public static final canvas_companion_progressbar:I = 0x7f0e0224

.field public static final canvas_companion_refresh:I = 0x7f0e0227

.field public static final canvas_loaded:I = 0x7f0e0228

.field public static final canvas_loading:I = 0x7f0e0220

.field public static final canvas_switch_panel:I = 0x7f0e021f

.field public static final canvas_webview_layout:I = 0x7f0e021d

.field public static final castcrew_name_text:I = 0x7f0e07aa

.field public static final castcrew_role_text:I = 0x7f0e07ab

.field public static final catalyst_redbox_title:I = 0x7f0e09ab

.field public static final cell_aspect_ratio:I = 0x7f0e0005

.field public static final cell_col_span:I = 0x7f0e0006

.field public static final cell_image:I = 0x7f0e06d6

.field public static final cell_item_title:I = 0x7f0e06d7

.field public static final cell_row_span:I = 0x7f0e0007

.field public static final center:I = 0x7f0e0098

.field public static final centerCrop:I = 0x7f0e00a9

.field public static final centerInside:I = 0x7f0e00aa

.field public static final center_horizontal:I = 0x7f0e0099

.field public static final center_vertical:I = 0x7f0e009a

.field public static final chains:I = 0x7f0e0076

.field public static final change_friendship_banner:I = 0x7f0e022e

.field public static final change_friendship_error_message_text:I = 0x7f0e0235

.field public static final change_friendship_inline_error_message_view:I = 0x7f0e0233

.field public static final change_friendship_profile_pic:I = 0x7f0e022f

.field public static final change_friendship_switch_panel:I = 0x7f0e022d

.field public static final change_realname_profilepic:I = 0x7f0e0a27

.field public static final change_region_image:I = 0x7f0e0943

.field public static final channel_callSign:I = 0x7f0e0b01

.field public static final channel_detail_space:I = 0x7f0e0b02

.field public static final channel_id:I = 0x7f0e0569

.field public static final channel_name:I = 0x7f0e0b32

.field public static final channel_number:I = 0x7f0e056d

.field public static final channel_title:I = 0x7f0e056c

.field public static final character_count_frame:I = 0x7f0e0411

.field public static final chat_message_of_the_day:I = 0x7f0e02f8

.field public static final chat_message_of_the_day_frame:I = 0x7f0e02f7

.field public static final chat_message_of_the_day_frame_separator:I = 0x7f0e02f9

.field public static final checkbox:I = 0x7f0e00f4

.field public static final choose_background_custom:I = 0x7f0e0329

.field public static final choose_gamerpic_body:I = 0x7f0e0252

.field public static final choose_gamerpic_close_button:I = 0x7f0e0254

.field public static final choose_gamerpic_custom:I = 0x7f0e0255

.field public static final choose_gamerpic_grid:I = 0x7f0e0256

.field public static final choose_gamerpic_image:I = 0x7f0e0257

.field public static final choose_gamerpic_text:I = 0x7f0e0253

.field public static final choose_gamertag_availability_result:I = 0x7f0e024e

.field public static final choose_gamertag_check_availability_button:I = 0x7f0e024d

.field public static final choose_gamertag_claim_button:I = 0x7f0e0251

.field public static final choose_gamertag_close_button:I = 0x7f0e0247

.field public static final choose_gamertag_edit_text:I = 0x7f0e024c

.field public static final choose_gamertag_no_free_change:I = 0x7f0e0248

.field public static final choose_gamertag_policy_1:I = 0x7f0e024a

.field public static final choose_gamertag_policy_2:I = 0x7f0e024b

.field public static final choose_gamertag_suggestion_item:I = 0x7f0e0258

.field public static final choose_gamertag_suggestions_header:I = 0x7f0e024f

.field public static final choose_gamertag_suggestions_list:I = 0x7f0e0250

.field public static final choose_gamertag_text:I = 0x7f0e0246

.field public static final choose_gamertag_xbox_link:I = 0x7f0e0249

.field public static final choose_profile_color_body:I = 0x7f0e0259

.field public static final choose_profile_color_close_button:I = 0x7f0e025a

.field public static final choose_profile_color_grid:I = 0x7f0e025c

.field public static final choose_profile_color_item:I = 0x7f0e025d

.field public static final choose_profile_color_text:I = 0x7f0e025b

.field public static final chronometer:I = 0x7f0e07eb

.field public static final clip_horizontal:I = 0x7f0e00a3

.field public static final clip_vertical:I = 0x7f0e00a4

.field public static final close_button:I = 0x7f0e022a

.field public static final close_stream_button:I = 0x7f0e018f

.field public static final club_activity_text:I = 0x7f0e08d5

.field public static final club_admin_banned_gamerpic:I = 0x7f0e025e

.field public static final club_admin_banned_gamertag:I = 0x7f0e025f

.field public static final club_admin_banned_presence:I = 0x7f0e0261

.field public static final club_admin_banned_realname:I = 0x7f0e0260

.field public static final club_admin_banned_unban:I = 0x7f0e0262

.field public static final club_admin_home_banned:I = 0x7f0e0268

.field public static final club_admin_home_members:I = 0x7f0e0267

.field public static final club_admin_home_reports:I = 0x7f0e0266

.field public static final club_admin_home_requests:I = 0x7f0e0265

.field public static final club_admin_home_settings:I = 0x7f0e0269

.field public static final club_admin_members_action:I = 0x7f0e026f

.field public static final club_admin_members_ban_action:I = 0x7f0e026b

.field public static final club_admin_members_gamerpic:I = 0x7f0e026e

.field public static final club_admin_members_gamertag:I = 0x7f0e0270

.field public static final club_admin_members_list:I = 0x7f0e0278

.field public static final club_admin_members_mod_action:I = 0x7f0e026a

.field public static final club_admin_members_presence:I = 0x7f0e0272

.field public static final club_admin_members_realname:I = 0x7f0e0271

.field public static final club_admin_members_remove_action:I = 0x7f0e026c

.field public static final club_admin_members_report_action:I = 0x7f0e026d

.field public static final club_admin_members_search_button:I = 0x7f0e0273

.field public static final club_admin_members_search_clear_button:I = 0x7f0e0275

.field public static final club_admin_members_search_input:I = 0x7f0e0274

.field public static final club_admin_members_spinner:I = 0x7f0e0276

.field public static final club_admin_members_too_many_warning:I = 0x7f0e0277

.field public static final club_admin_report_ago:I = 0x7f0e0279

.field public static final club_admin_report_buttons:I = 0x7f0e0281

.field public static final club_admin_report_comment_content:I = 0x7f0e0287

.field public static final club_admin_report_comment_gamerpic:I = 0x7f0e0284

.field public static final club_admin_report_comment_gamertag:I = 0x7f0e0285

.field public static final club_admin_report_comment_realname:I = 0x7f0e0286

.field public static final club_admin_report_content:I = 0x7f0e027c

.field public static final club_admin_report_controls_ban_creator:I = 0x7f0e0289

.field public static final club_admin_report_controls_ban_reporter:I = 0x7f0e028b

.field public static final club_admin_report_controls_ignore:I = 0x7f0e0288

.field public static final club_admin_report_controls_message_creator:I = 0x7f0e028a

.field public static final club_admin_report_controls_message_reporter:I = 0x7f0e028c

.field public static final club_admin_report_delete:I = 0x7f0e0282

.field public static final club_admin_report_error:I = 0x7f0e027e

.field public static final club_admin_report_item_count:I = 0x7f0e0280

.field public static final club_admin_report_item_count_symbol:I = 0x7f0e027f

.field public static final club_admin_report_more:I = 0x7f0e0283

.field public static final club_admin_report_reason:I = 0x7f0e027b

.field public static final club_admin_report_stub:I = 0x7f0e027d

.field public static final club_admin_report_title:I = 0x7f0e027a

.field public static final club_admin_reports_screen_title:I = 0x7f0e028d

.field public static final club_admin_request_accept:I = 0x7f0e0296

.field public static final club_admin_request_gamerpic:I = 0x7f0e0292

.field public static final club_admin_request_gamertag:I = 0x7f0e0293

.field public static final club_admin_request_ignore:I = 0x7f0e0291

.field public static final club_admin_request_realname:I = 0x7f0e0294

.field public static final club_admin_request_reason:I = 0x7f0e0295

.field public static final club_admin_switch_panel_screen_header:I = 0x7f0e02b2

.field public static final club_banned_list:I = 0x7f0e0264

.field public static final club_banned_screen_title:I = 0x7f0e0263

.field public static final club_cant_view_reason:I = 0x7f0e02b5

.field public static final club_card_banner:I = 0x7f0e02b8

.field public static final club_card_category_header:I = 0x7f0e02c6

.field public static final club_card_category_header_frame:I = 0x7f0e02c5

.field public static final club_card_category_list:I = 0x7f0e02c8

.field public static final club_card_category_see_all_button:I = 0x7f0e02c7

.field public static final club_card_club_name:I = 0x7f0e02ba

.field public static final club_card_club_pic:I = 0x7f0e02b9

.field public static final club_card_club_pic_name_frame:I = 0x7f0e02b7

.field public static final club_card_club_type:I = 0x7f0e02be

.field public static final club_card_description:I = 0x7f0e02c3

.field public static final club_card_glyph:I = 0x7f0e02bd

.field public static final club_card_here_today_header:I = 0x7f0e02c0

.field public static final club_card_here_today_number:I = 0x7f0e02c2

.field public static final club_card_icon:I = 0x7f0e02bc

.field public static final club_card_icon_glyph_frame:I = 0x7f0e02bb

.field public static final club_card_members_header:I = 0x7f0e02bf

.field public static final club_card_members_number:I = 0x7f0e02c1

.field public static final club_card_root:I = 0x7f0e02b6

.field public static final club_card_tags:I = 0x7f0e02c4

.field public static final club_chat_activity_body:I = 0x7f0e02f4

.field public static final club_chat_character_count:I = 0x7f0e0300

.field public static final club_chat_compose_container:I = 0x7f0e02f6

.field public static final club_chat_direct_mention_gamertag:I = 0x7f0e02ca

.field public static final club_chat_direct_mention_image:I = 0x7f0e02c9

.field public static final club_chat_direct_mention_list:I = 0x7f0e02fc

.field public static final club_chat_direct_mention_realname:I = 0x7f0e02cb

.field public static final club_chat_edit_report_back_button:I = 0x7f0e02ce

.field public static final club_chat_edit_report_close_button:I = 0x7f0e02cd

.field public static final club_chat_edit_report_dialog_edit_button:I = 0x7f0e02d3

.field public static final club_chat_edit_report_dialog_report_button:I = 0x7f0e02d4

.field public static final club_chat_edit_report_messageoftheday:I = 0x7f0e02cf

.field public static final club_chat_edit_report_post_error:I = 0x7f0e02d2

.field public static final club_chat_edit_report_postby_text:I = 0x7f0e02d0

.field public static final club_chat_edit_report_timestamp:I = 0x7f0e02d1

.field public static final club_chat_edit_text_entry:I = 0x7f0e02fe

.field public static final club_chat_gamername:I = 0x7f0e02ef

.field public static final club_chat_gamerpic:I = 0x7f0e02ec

.field public static final club_chat_gamertag:I = 0x7f0e02ee

.field public static final club_chat_gamertag_container:I = 0x7f0e02ed

.field public static final club_chat_manage_notifications_button:I = 0x7f0e02fd

.field public static final club_chat_message_container:I = 0x7f0e02f5

.field public static final club_chat_message_content:I = 0x7f0e02f0

.field public static final club_chat_message_list:I = 0x7f0e02fb

.field public static final club_chat_messageoftheday_body:I = 0x7f0e02cc

.field public static final club_chat_messageoftheday_close_button:I = 0x7f0e02db

.field public static final club_chat_messageoftheday_edittext:I = 0x7f0e02dd

.field public static final club_chat_messageoftheday_post_button:I = 0x7f0e02e0

.field public static final club_chat_messageoftheday_post_error:I = 0x7f0e02df

.field public static final club_chat_messageoftheday_postby_text:I = 0x7f0e02de

.field public static final club_chat_messageoftheday_title:I = 0x7f0e02dc

.field public static final club_chat_messageoftheday_title_layout:I = 0x7f0e02da

.field public static final club_chat_notification_close:I = 0x7f0e02e5

.field public static final club_chat_notification_no_notification:I = 0x7f0e02ea

.field public static final club_chat_notification_notify_by:I = 0x7f0e02e7

.field public static final club_chat_notification_notify_by_all_messages:I = 0x7f0e02e8

.field public static final club_chat_notification_notify_by_direct_mention:I = 0x7f0e02e9

.field public static final club_chat_notification_relative_layout:I = 0x7f0e02e2

.field public static final club_chat_notification_root:I = 0x7f0e02e1

.field public static final club_chat_notification_screen:I = 0x7f0e02e3

.field public static final club_chat_notification_title:I = 0x7f0e02e6

.field public static final club_chat_popup_menu_copy:I = 0x7f0e0c24

.field public static final club_chat_popup_menu_delete:I = 0x7f0e0c23

.field public static final club_chat_popup_menu_report:I = 0x7f0e0c25

.field public static final club_chat_popup_menu_report_admins:I = 0x7f0e0c26

.field public static final club_chat_send_message:I = 0x7f0e02ff

.field public static final club_chat_settings_enable_hover:I = 0x7f0e02eb

.field public static final club_chat_settings_notifications:I = 0x7f0e0302

.field public static final club_chat_settings_open_hover:I = 0x7f0e0301

.field public static final club_chat_status:I = 0x7f0e02f3

.field public static final club_chat_text:I = 0x7f0e02f2

.field public static final club_chat_timestamp:I = 0x7f0e02f1

.field public static final club_chat_typing_indicator:I = 0x7f0e02fa

.field public static final club_create_back:I = 0x7f0e0307

.field public static final club_create_check:I = 0x7f0e030f

.field public static final club_create_club_name_check_availability:I = 0x7f0e0313

.field public static final club_create_club_name_editText:I = 0x7f0e0314

.field public static final club_create_confirm:I = 0x7f0e0308

.field public static final club_create_confirmation_text:I = 0x7f0e0304

.field public static final club_create_confirmation_type_view:I = 0x7f0e0305

.field public static final club_create_controls:I = 0x7f0e0306

.field public static final club_create_name_progress_bar:I = 0x7f0e0310

.field public static final club_create_next:I = 0x7f0e030d

.field public static final club_create_public_private_remaining:I = 0x7f0e0317

.field public static final club_create_public_private_total:I = 0x7f0e0316

.field public static final club_create_switch_panel:I = 0x7f0e030a

.field public static final club_create_viewpager:I = 0x7f0e030b

.field public static final club_creation_close:I = 0x7f0e0309

.field public static final club_creation_name_text:I = 0x7f0e030e

.field public static final club_customize_browse_achievements:I = 0x7f0e032c

.field public static final club_customize_club_background_achievement_image:I = 0x7f0e0326

.field public static final club_customize_club_background_achievement_title:I = 0x7f0e0325

.field public static final club_customize_club_background_game_filter_spinner:I = 0x7f0e032b

.field public static final club_customize_club_background_image_close:I = 0x7f0e0324

.field public static final club_customize_club_background_recycler:I = 0x7f0e0328

.field public static final club_customize_club_background_row_days_ago:I = 0x7f0e0321

.field public static final club_customize_club_background_row_image:I = 0x7f0e02b3

.field public static final club_customize_club_background_row_sub_title:I = 0x7f0e0320

.field public static final club_customize_club_background_row_title:I = 0x7f0e02b4

.field public static final club_customize_club_background_selection_spinner:I = 0x7f0e032a

.field public static final club_customize_club_background_switch_panel:I = 0x7f0e0327

.field public static final club_customize_club_color:I = 0x7f0e0331

.field public static final club_customize_club_description:I = 0x7f0e033c

.field public static final club_customize_club_description_btn:I = 0x7f0e033f

.field public static final club_customize_club_description_text:I = 0x7f0e033e

.field public static final club_customize_club_description_title:I = 0x7f0e033d

.field public static final club_customize_club_games:I = 0x7f0e0340

.field public static final club_customize_club_games_edit:I = 0x7f0e0344

.field public static final club_customize_club_games_text:I = 0x7f0e0343

.field public static final club_customize_club_games_title:I = 0x7f0e0342

.field public static final club_customize_club_img:I = 0x7f0e0333

.field public static final club_customize_club_img_background_image:I = 0x7f0e0332

.field public static final club_customize_club_img_display_image:I = 0x7f0e0335

.field public static final club_customize_club_img_display_image_frame:I = 0x7f0e0334

.field public static final club_customize_club_img_progressbar:I = 0x7f0e0348

.field public static final club_customize_club_img_text:I = 0x7f0e0336

.field public static final club_customize_club_name_container:I = 0x7f0e032f

.field public static final club_customize_club_name_text:I = 0x7f0e0330

.field public static final club_customize_club_tag_edit:I = 0x7f0e0339

.field public static final club_customize_club_tags_text:I = 0x7f0e033b

.field public static final club_customize_club_tags_title:I = 0x7f0e033a

.field public static final club_customize_dialog_tags_list:I = 0x7f0e034f

.field public static final club_customize_dialog_tags_selected:I = 0x7f0e034c

.field public static final club_customize_done:I = 0x7f0e0345

.field public static final club_customize_pic_close:I = 0x7f0e0346

.field public static final club_customize_pic_filter:I = 0x7f0e0347

.field public static final club_customize_pic_recycler_view:I = 0x7f0e0349

.field public static final club_customize_remove_background:I = 0x7f0e032d

.field public static final club_customize_root:I = 0x7f0e032e

.field public static final club_customize_tags_close:I = 0x7f0e034a

.field public static final club_customize_tags_done:I = 0x7f0e034e

.field public static final club_customize_tags_edit:I = 0x7f0e0337

.field public static final club_customize_tags_title:I = 0x7f0e034b

.field public static final club_discovery_create_button:I = 0x7f0e0352

.field public static final club_discovery_invitation_group:I = 0x7f0e0353

.field public static final club_discovery_invitation_header:I = 0x7f0e0354

.field public static final club_discovery_invitation_list:I = 0x7f0e0356

.field public static final club_discovery_recommendation_list:I = 0x7f0e0357

.field public static final club_discovery_search_button:I = 0x7f0e0351

.field public static final club_discovery_switch_panel_screen_header:I = 0x7f0e0358

.field public static final club_discovery_your_club:I = 0x7f0e0350

.field public static final club_feed_screen_body:I = 0x7f0e0359

.field public static final club_firstline_frame:I = 0x7f0e08d0

.field public static final club_glyph:I = 0x7f0e08d2

.field public static final club_home_background:I = 0x7f0e0363

.field public static final club_home_banned_text:I = 0x7f0e036b

.field public static final club_home_bio:I = 0x7f0e037a

.field public static final club_home_bio_header:I = 0x7f0e0379

.field public static final club_home_center_anchor:I = 0x7f0e0373

.field public static final club_home_customize:I = 0x7f0e036e

.field public static final club_home_display:I = 0x7f0e0366

.field public static final club_home_follow:I = 0x7f0e0371

.field public static final club_home_followers_count:I = 0x7f0e0377

.field public static final club_home_followers_title:I = 0x7f0e0376

.field public static final club_home_glyph:I = 0x7f0e0364

.field public static final club_home_here_today_count:I = 0x7f0e0368

.field public static final club_home_here_today_title:I = 0x7f0e0367

.field public static final club_home_invite:I = 0x7f0e036f

.field public static final club_home_keep_club:I = 0x7f0e036c

.field public static final club_home_members_count:I = 0x7f0e0375

.field public static final club_home_members_title:I = 0x7f0e0374

.field public static final club_home_membership_action:I = 0x7f0e0370

.field public static final club_home_owner:I = 0x7f0e037e

.field public static final club_home_owner_gamerpic:I = 0x7f0e0380

.field public static final club_home_owner_gamerscore:I = 0x7f0e0384

.field public static final club_home_owner_gamerscore_icon:I = 0x7f0e0383

.field public static final club_home_owner_gamertag:I = 0x7f0e0381

.field public static final club_home_owner_header:I = 0x7f0e037f

.field public static final club_home_owner_realname:I = 0x7f0e0382

.field public static final club_home_play:I = 0x7f0e036d

.field public static final club_home_play_list_more:I = 0x7f0e035f

.field public static final club_home_player_list:I = 0x7f0e036a

.field public static final club_home_player_list_image:I = 0x7f0e035e

.field public static final club_home_playing_now:I = 0x7f0e0369

.field public static final club_home_privacy:I = 0x7f0e0365

.field public static final club_home_report:I = 0x7f0e0372

.field public static final club_home_root:I = 0x7f0e0360

.field public static final club_home_suspended_banner:I = 0x7f0e0361

.field public static final club_home_suspended_text:I = 0x7f0e0362

.field public static final club_home_tags:I = 0x7f0e0378

.field public static final club_home_titles:I = 0x7f0e037d

.field public static final club_home_titles_header:I = 0x7f0e037b

.field public static final club_home_titles_loading:I = 0x7f0e037c

.field public static final club_image:I = 0x7f0e08cf

.field public static final club_invitations_decline_all_button:I = 0x7f0e0385

.field public static final club_invitations_list:I = 0x7f0e0386

.field public static final club_invitations_see_all_button:I = 0x7f0e0355

.field public static final club_invitations_switch_panel_screen_header:I = 0x7f0e0387

.field public static final club_invite_accept:I = 0x7f0e0394

.field public static final club_invite_banner:I = 0x7f0e038a

.field public static final club_invite_checked:I = 0x7f0e0395

.field public static final club_invite_favorite:I = 0x7f0e0397

.field public static final club_invite_gamerpic:I = 0x7f0e0398

.field public static final club_invite_gamertag:I = 0x7f0e0399

.field public static final club_invite_glyph:I = 0x7f0e038f

.field public static final club_invite_header:I = 0x7f0e0389

.field public static final club_invite_icon:I = 0x7f0e038e

.field public static final club_invite_icon_glyph_frame:I = 0x7f0e038d

.field public static final club_invite_ignore:I = 0x7f0e0391

.field public static final club_invite_name:I = 0x7f0e038c

.field public static final club_invite_online:I = 0x7f0e0396

.field public static final club_invite_owner:I = 0x7f0e0392

.field public static final club_invite_pic:I = 0x7f0e038b

.field public static final club_invite_presence:I = 0x7f0e039b

.field public static final club_invite_realname:I = 0x7f0e039a

.field public static final club_invite_reason:I = 0x7f0e0393

.field public static final club_invite_root:I = 0x7f0e0388

.field public static final club_invite_type:I = 0x7f0e0390

.field public static final club_lfg_create:I = 0x7f0e03b8

.field public static final club_management_indicator:I = 0x7f0e08d6

.field public static final club_member_name_text_view:I = 0x7f0e03a4

.field public static final club_member_owner_text_view:I = 0x7f0e03a5

.field public static final club_member_pic:I = 0x7f0e03a3

.field public static final club_mod_picker_dialog_close:I = 0x7f0e03a7

.field public static final club_mod_picker_dialog_header:I = 0x7f0e03a6

.field public static final club_mod_picker_list:I = 0x7f0e03a8

.field public static final club_mod_picker_row_gamerpic:I = 0x7f0e03a9

.field public static final club_mod_picker_row_gamertag:I = 0x7f0e03aa

.field public static final club_mod_picker_row_presence:I = 0x7f0e03ac

.field public static final club_mod_picker_row_realname:I = 0x7f0e03ab

.field public static final club_name:I = 0x7f0e08d1

.field public static final club_picker_dialog_close:I = 0x7f0e03ae

.field public static final club_picker_dialog_close_frame:I = 0x7f0e03ad

.field public static final club_picker_dialog_header:I = 0x7f0e03af

.field public static final club_picker_item_club_name:I = 0x7f0e03b3

.field public static final club_picker_item_club_pic:I = 0x7f0e03b2

.field public static final club_picker_item_root:I = 0x7f0e03b1

.field public static final club_picker_item_subtitle:I = 0x7f0e03b4

.field public static final club_picker_list:I = 0x7f0e03b0

.field public static final club_pivot:I = 0x7f0e03b5

.field public static final club_play_error_text:I = 0x7f0e03b6

.field public static final club_play_list:I = 0x7f0e03b7

.field public static final club_recommendation_search_button:I = 0x7f0e03b9

.field public static final club_rename_clear:I = 0x7f0e0315

.field public static final club_reports_list:I = 0x7f0e028e

.field public static final club_requests_ignore_all:I = 0x7f0e0290

.field public static final club_requests_list:I = 0x7f0e0298

.field public static final club_requests_screen_title:I = 0x7f0e0297

.field public static final club_requests_send_all:I = 0x7f0e028f

.field public static final club_search_button:I = 0x7f0e03bb

.field public static final club_search_clear_button:I = 0x7f0e03c2

.field public static final club_search_criteria:I = 0x7f0e03cc

.field public static final club_search_criteria_header:I = 0x7f0e03cd

.field public static final club_search_empty_search:I = 0x7f0e03c5

.field public static final club_search_input:I = 0x7f0e03c4

.field public static final club_search_input_div:I = 0x7f0e03c3

.field public static final club_search_list:I = 0x7f0e03ba

.field public static final club_search_num_results:I = 0x7f0e03ce

.field public static final club_search_recent_1:I = 0x7f0e03c7

.field public static final club_search_recent_2:I = 0x7f0e03c8

.field public static final club_search_recent_3:I = 0x7f0e03c9

.field public static final club_search_recents_header:I = 0x7f0e03c6

.field public static final club_search_tags:I = 0x7f0e03cb

.field public static final club_search_tags_button:I = 0x7f0e03bd

.field public static final club_search_tags_count:I = 0x7f0e03be

.field public static final club_search_tags_header:I = 0x7f0e03ca

.field public static final club_search_titles_button:I = 0x7f0e03c0

.field public static final club_search_titles_count:I = 0x7f0e03c1

.field public static final club_secondline_frame:I = 0x7f0e08d4

.field public static final club_settings_broadcasts_mature_content_enabled:I = 0x7f0e02ac

.field public static final club_settings_broadcasts_mature_content_enabled_loading:I = 0x7f0e02ad

.field public static final club_settings_broadcasts_watch_club_titles_only:I = 0x7f0e02ae

.field public static final club_settings_broadcasts_watch_club_titles_only_loading:I = 0x7f0e02af

.field public static final club_settings_chat_loading:I = 0x7f0e02ab

.field public static final club_settings_chat_spinner:I = 0x7f0e02aa

.field public static final club_settings_delete:I = 0x7f0e02b1

.field public static final club_settings_feed_loading:I = 0x7f0e02a3

.field public static final club_settings_feed_spinner:I = 0x7f0e02a2

.field public static final club_settings_invite_loading:I = 0x7f0e02a1

.field public static final club_settings_invite_spinner:I = 0x7f0e02a0

.field public static final club_settings_lfg_enable_join:I = 0x7f0e02a7

.field public static final club_settings_lfg_join_description:I = 0x7f0e02a9

.field public static final club_settings_lfg_join_loading:I = 0x7f0e02a8

.field public static final club_settings_lfg_loading:I = 0x7f0e02a6

.field public static final club_settings_lfg_spinner:I = 0x7f0e02a5

.field public static final club_settings_membership_enable_join:I = 0x7f0e029e

.field public static final club_settings_membership_loading:I = 0x7f0e029f

.field public static final club_settings_privacy:I = 0x7f0e029a

.field public static final club_settings_privacy_description:I = 0x7f0e029b

.field public static final club_settings_set_to_default:I = 0x7f0e029c

.field public static final club_settings_transfer:I = 0x7f0e02b0

.field public static final club_type:I = 0x7f0e08d3

.field public static final club_type_hidden_btn:I = 0x7f0e031a

.field public static final club_type_private_btn:I = 0x7f0e0319

.field public static final club_type_public_btn:I = 0x7f0e0318

.field public static final club_type_title:I = 0x7f0e031b

.field public static final club_watch_list:I = 0x7f0e03d0

.field public static final club_watch_swipe_refresh_content_screen:I = 0x7f0e03cf

.field public static final club_watch_swipe_refresh_no_content_screen:I = 0x7f0e03d1

.field public static final club_welcome_card:I = 0x7f0e0afe

.field public static final club_whos_here_filter_spinner:I = 0x7f0e03df

.field public static final club_whos_here_list:I = 0x7f0e03e1

.field public static final club_whos_here_row_header:I = 0x7f0e03d6

.field public static final club_whos_here_row_image:I = 0x7f0e03d3

.field public static final club_whos_here_row_image_frame:I = 0x7f0e03d2

.field public static final club_whos_here_row_primary_text:I = 0x7f0e03d7

.field public static final club_whos_here_row_secondary_text:I = 0x7f0e03d8

.field public static final club_whos_here_row_tertiary_text:I = 0x7f0e03d9

.field public static final club_whos_here_screen_body:I = 0x7f0e03da

.field public static final club_whos_here_search_button:I = 0x7f0e03dc

.field public static final club_whos_here_search_clear_button:I = 0x7f0e03de

.field public static final club_whos_here_search_input:I = 0x7f0e03dd

.field public static final club_whos_here_search_section:I = 0x7f0e03db

.field public static final club_whos_here_too_many_warning:I = 0x7f0e03e0

.field public static final clubs_customize_dialog_progressbar:I = 0x7f0e034d

.field public static final clubs_customize_titles_flow_recyclerview:I = 0x7f0e0341

.field public static final clubs_invite_close:I = 0x7f0e039c

.field public static final clubs_invite_list:I = 0x7f0e03a2

.field public static final clubs_invite_recipients:I = 0x7f0e039f

.field public static final clubs_invite_recipients_div:I = 0x7f0e03a0

.field public static final clubs_invite_send:I = 0x7f0e03a1

.field public static final clubs_invite_title_div:I = 0x7f0e039e

.field public static final collapseActionView:I = 0x7f0e00ba

.field public static final com_facebook_body_frame:I = 0x7f0e03e9

.field public static final com_facebook_button_xout:I = 0x7f0e03eb

.field public static final com_facebook_device_auth_instructions:I = 0x7f0e03e5

.field public static final com_facebook_fragment_container:I = 0x7f0e03e2

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f0e03e6

.field public static final com_facebook_smart_instructions_0:I = 0x7f0e03e7

.field public static final com_facebook_smart_instructions_or:I = 0x7f0e03e8

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f0e03ed

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f0e03ec

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f0e03ea

.field public static final comment_popup_menu_delete:I = 0x7f0e0c27

.field public static final companion_bar:I = 0x7f0e0229

.field public static final companion_btn_icon:I = 0x7f0e0627

.field public static final companion_override_allowed_urls:I = 0x7f0e03f1

.field public static final companion_override_dialog:I = 0x7f0e03ee

.field public static final companion_override_titleid:I = 0x7f0e03f0

.field public static final companion_override_url:I = 0x7f0e03ef

.field public static final companion_splash_body:I = 0x7f0e0223

.field public static final companiondetail_header_screen_body:I = 0x7f0e03f2

.field public static final companiondetail_image_tile:I = 0x7f0e03f3

.field public static final companiondetail_title:I = 0x7f0e03f4

.field public static final companiondetail_title_small:I = 0x7f0e03f5

.field public static final compare_achievement_button:I = 0x7f0e08fb

.field public static final comparison_listheader_stat_comparisonbar:I = 0x7f0e05e8

.field public static final comparison_listheader_stat_friend_value:I = 0x7f0e05e7

.field public static final comparison_listheader_stat_my_value:I = 0x7f0e05e6

.field public static final comparison_listheader_stat_title:I = 0x7f0e05e5

.field public static final composemessage_contacts_container:I = 0x7f0e0419

.field public static final composemessage_container:I = 0x7f0e0415

.field public static final composemessage_layout:I = 0x7f0e0414

.field public static final composemessage_message:I = 0x7f0e0418

.field public static final composemessage_title:I = 0x7f0e0416

.field public static final composemessagewithattachement_close_button:I = 0x7f0e041e

.field public static final composemessagewithattachement_container:I = 0x7f0e041d

.field public static final composemessagewithattachement_layout:I = 0x7f0e041c

.field public static final composemessagewithattachement_message:I = 0x7f0e0420

.field public static final composemessagewithattachement_message_container:I = 0x7f0e041f

.field public static final confirmation_code:I = 0x7f0e03e3

.field public static final connect_button:I = 0x7f0e022b

.field public static final connect_dialog_addbyip:I = 0x7f0e0429

.field public static final connect_dialog_autoconnect_checkbox:I = 0x7f0e042f

.field public static final connect_dialog_autoconnect_checkbox_label:I = 0x7f0e0430

.field public static final connect_dialog_close:I = 0x7f0e0422

.field public static final connect_dialog_connect_button:I = 0x7f0e0438

.field public static final connect_dialog_connection_state:I = 0x7f0e0428

.field public static final connect_dialog_console_name:I = 0x7f0e042d

.field public static final connect_dialog_container:I = 0x7f0e042b

.field public static final connect_dialog_container_view:I = 0x7f0e0425

.field public static final connect_dialog_disconnect_button:I = 0x7f0e0439

.field public static final connect_dialog_enter_ip:I = 0x7f0e0426

.field public static final connect_dialog_error_msg:I = 0x7f0e0434

.field public static final connect_dialog_expanded_section:I = 0x7f0e042a

.field public static final connect_dialog_main_section:I = 0x7f0e0427

.field public static final connect_dialog_power_on_connect_view:I = 0x7f0e0431

.field public static final connect_dialog_power_on_off_connect_message:I = 0x7f0e0433

.field public static final connect_dialog_power_on_spinner:I = 0x7f0e0432

.field public static final connect_dialog_sleeping_console_icon:I = 0x7f0e042c

.field public static final connect_dialog_turn_off_button:I = 0x7f0e0437

.field public static final connect_dialog_turn_on_button:I = 0x7f0e0436

.field public static final connected_dropdown_button_label:I = 0x7f0e0440

.field public static final console_bar_tabhost:I = 0x7f0e045e

.field public static final console_connection_connect_button:I = 0x7f0e043d

.field public static final console_connection_dropdown_autoconnect:I = 0x7f0e044a

.field public static final console_connection_dropdown_button:I = 0x7f0e043f

.field public static final console_connection_dropdown_disconnect:I = 0x7f0e0448

.field public static final console_connection_dropdown_forget_console:I = 0x7f0e044b

.field public static final console_connection_dropdown_symbol:I = 0x7f0e0441

.field public static final console_connection_dropdown_turnoff:I = 0x7f0e0449

.field public static final console_connection_dropdown_view:I = 0x7f0e0447

.field public static final console_connection_header_text:I = 0x7f0e044c

.field public static final console_connection_mru_list:I = 0x7f0e0445

.field public static final console_connection_oobe_entry:I = 0x7f0e044d

.field public static final console_connection_screen_body:I = 0x7f0e0442

.field public static final console_connection_screen_top:I = 0x7f0e0444

.field public static final console_connection_switch_panel:I = 0x7f0e0443

.field public static final console_connection_wifi_symbol:I = 0x7f0e043e

.field public static final console_management_dialog_close:I = 0x7f0e044f

.field public static final console_management_dialog_navigate_to_oobe_settings_button:I = 0x7f0e0453

.field public static final console_management_dialog_navigate_to_oobe_settings_button_text:I = 0x7f0e0454

.field public static final console_management_dialog_root:I = 0x7f0e044e

.field public static final console_management_dialog_toggle_connection_button:I = 0x7f0e0450

.field public static final console_management_dialog_toggle_connection_button_icon:I = 0x7f0e0451

.field public static final console_management_dialog_toggle_connection_button_text:I = 0x7f0e0452

.field public static final container_for_invisible_gridrow:I = 0x7f0e0b06

.field public static final contentPanel:I = 0x7f0e00e7

.field public static final content_frame:I = 0x7f0e0786

.field public static final control_button_text:I = 0x7f0e0640

.field public static final conversation_arrow:I = 0x7f0e04a3

.field public static final conversation_arrow_other:I = 0x7f0e049e

.field public static final conversation_drop_down_button:I = 0x7f0e0473

.field public static final conversation_external_timestamp:I = 0x7f0e049a

.field public static final conversation_friends_picker_selected_count:I = 0x7f0e0472

.field public static final conversation_gamertag_title:I = 0x7f0e0470

.field public static final conversation_group_images_container:I = 0x7f0e0469

.field public static final conversation_icon_header:I = 0x7f0e0466

.field public static final conversation_image_container:I = 0x7f0e0467

.field public static final conversation_images_container:I = 0x7f0e04b1

.field public static final conversation_layout:I = 0x7f0e045f

.field public static final conversation_list:I = 0x7f0e0465

.field public static final conversation_listItem_sender_gametag:I = 0x7f0e049c

.field public static final conversation_listItem_sender_realname:I = 0x7f0e049d

.field public static final conversation_listItem_time:I = 0x7f0e04a2

.field public static final conversation_listItem_time_sizer:I = 0x7f0e0499

.field public static final conversation_listItem_title:I = 0x7f0e04a0

.field public static final conversation_menu_view:I = 0x7f0e02d5

.field public static final conversation_message_container:I = 0x7f0e049f

.field public static final conversation_online_count:I = 0x7f0e0471

.field public static final conversation_popup_menu_block:I = 0x7f0e0c2a

.field public static final conversation_popup_menu_delete:I = 0x7f0e0c29

.field public static final conversation_popup_menu_hover_chat:I = 0x7f0e0c28

.field public static final conversation_status:I = 0x7f0e04a6

.field public static final conversation_switch_panel:I = 0x7f0e0462

.field public static final conversation_tile:I = 0x7f0e0468

.field public static final conversations_header:I = 0x7f0e04a8

.field public static final conversations_list:I = 0x7f0e04bf

.field public static final conversations_listItem_firstLine:I = 0x7f0e04b2

.field public static final conversations_listItem_secondline:I = 0x7f0e04b6

.field public static final conversations_listItem_sender:I = 0x7f0e04b3

.field public static final conversations_listItem_sender_realname:I = 0x7f0e04b4

.field public static final conversations_listItem_tile:I = 0x7f0e04af

.field public static final conversations_listItem_time:I = 0x7f0e04b5

.field public static final conversations_listItem_title:I = 0x7f0e04a5

.field public static final conversations_message_container:I = 0x7f0e04a4

.field public static final conversations_selector_body:I = 0x7f0e04b7

.field public static final conversations_selector_cancel_button:I = 0x7f0e04bd

.field public static final conversations_selector_confirm:I = 0x7f0e04bc

.field public static final conversations_selector_description:I = 0x7f0e04c8

.field public static final conversations_selector_icon_selected:I = 0x7f0e04c9

.field public static final conversations_selector_sender:I = 0x7f0e04c6

.field public static final conversations_selector_start_conversation:I = 0x7f0e04c0

.field public static final conversations_selector_switch_panel:I = 0x7f0e04be

.field public static final conversations_selector_tile:I = 0x7f0e04c3

.field public static final conversations_tile1:I = 0x7f0e046b

.field public static final conversations_tile2:I = 0x7f0e046c

.field public static final conversations_tile3:I = 0x7f0e046e

.field public static final conversations_tile4:I = 0x7f0e046f

.field public static final create_group_conversation_frame:I = 0x7f0e0477

.field public static final create_lfg_add_edit_description_btn:I = 0x7f0e072e

.field public static final create_lfg_add_edit_description_frame:I = 0x7f0e072d

.field public static final create_lfg_add_edit_label:I = 0x7f0e072f

.field public static final create_lfg_day_spinner:I = 0x7f0e0732

.field public static final create_lfg_description_text:I = 0x7f0e072c

.field public static final create_lfg_language_spinner:I = 0x7f0e073e

.field public static final create_lfg_need_count_header:I = 0x7f0e0737

.field public static final create_lfg_need_spinner:I = 0x7f0e0738

.field public static final create_lfg_time_spinner:I = 0x7f0e0735

.field public static final create_lfg_visibility_frame:I = 0x7f0e0739

.field public static final create_lfg_visibility_header:I = 0x7f0e073a

.field public static final create_lfg_visibility_spinner:I = 0x7f0e073b

.field public static final cropImageView:I = 0x7f0e04e0

.field public static final crop_image_menu_crop:I = 0x7f0e0c21

.field public static final crop_image_menu_rotate_left:I = 0x7f0e0c1f

.field public static final crop_image_menu_rotate_right:I = 0x7f0e0c20

.field public static final custom:I = 0x7f0e00ee

.field public static final customPanel:I = 0x7f0e00ed

.field public static final customTypefaceTextView:I = 0x7f0e02a4

.field public static final customTypefaceTextView2:I = 0x7f0e029d

.field public static final customTypefaceTextView3:I = 0x7f0e0299

.field public static final customTypefaceTextView4:I = 0x7f0e03ff

.field public static final customize_button_layout:I = 0x7f0e0556

.field public static final customize_profile_add_bio_button:I = 0x7f0e04ed

.field public static final customize_profile_add_bio_count:I = 0x7f0e04ef

.field public static final customize_profile_add_bio_edit_text:I = 0x7f0e04ee

.field public static final customize_profile_add_location_button:I = 0x7f0e04ea

.field public static final customize_profile_add_location_count:I = 0x7f0e04ec

.field public static final customize_profile_add_location_edit_text:I = 0x7f0e04eb

.field public static final customize_profile_cancel_button:I = 0x7f0e04f1

.field public static final customize_profile_change_color_button:I = 0x7f0e04e9

.field public static final customize_profile_change_gamerpic_button:I = 0x7f0e04e7

.field public static final customize_profile_change_gamertag_button:I = 0x7f0e04e8

.field public static final customize_profile_profile_image:I = 0x7f0e04e6

.field public static final customize_profile_root:I = 0x7f0e04e4

.field public static final customize_profile_save_button:I = 0x7f0e04f0

.field public static final customize_profile_switch_panel:I = 0x7f0e04e5

.field public static final dPad:I = 0x7f0e0c06

.field public static final dark:I = 0x7f0e00c1

.field public static final decor_content_parent:I = 0x7f0e00fb

.field public static final default_activity_button:I = 0x7f0e00e0

.field public static final delete_console_icon:I = 0x7f0e042e

.field public static final description_text:I = 0x7f0e057b

.field public static final design_bottom_sheet:I = 0x7f0e04f3

.field public static final design_menu_item_action_area:I = 0x7f0e04fa

.field public static final design_menu_item_action_area_stub:I = 0x7f0e04f9

.field public static final design_menu_item_text:I = 0x7f0e04f8

.field public static final design_navigation_view:I = 0x7f0e04f7

.field public static final details_description:I = 0x7f0e04fc

.field public static final details_header_background:I = 0x7f0e04fe

.field public static final details_more_or_less:I = 0x7f0e04fd

.field public static final details_pivot:I = 0x7f0e0500

.field public static final details_provider_grid_item_image:I = 0x7f0e0501

.field public static final details_related_body:I = 0x7f0e0503

.field public static final details_related_empty:I = 0x7f0e0507

.field public static final details_related_layout:I = 0x7f0e0502

.field public static final details_related_list:I = 0x7f0e0508

.field public static final details_related_list_module:I = 0x7f0e0505

.field public static final details_related_switch_panel:I = 0x7f0e0504

.field public static final details_root_layout:I = 0x7f0e04ff

.field public static final disableHome:I = 0x7f0e0081

.field public static final discover_clubs_card_hide_button:I = 0x7f0e08d9

.field public static final discover_clubs_card_ok_button:I = 0x7f0e08d8

.field public static final discover_clubs_card_root_frame:I = 0x7f0e08d7

.field public static final discover_select_grid_item_content:I = 0x7f0e0509

.field public static final discover_select_grid_item_title:I = 0x7f0e050a

.field public static final display_always:I = 0x7f0e00d6

.field public static final dlc_detail_header_body:I = 0x7f0e050b

.field public static final dlc_details_header_ratingbar:I = 0x7f0e050d

.field public static final dlc_details_header_tile:I = 0x7f0e050c

.field public static final dlc_details_header_title:I = 0x7f0e050e

.field public static final dlc_details_header_title_small:I = 0x7f0e050f

.field public static final dlc_overview_category_text:I = 0x7f0e051f

.field public static final dlc_overview_consumable_purchase_btns_layout:I = 0x7f0e0514

.field public static final dlc_overview_description_text:I = 0x7f0e051d

.field public static final dlc_overview_download_btn_ring_icon:I = 0x7f0e0516

.field public static final dlc_overview_download_btn_text:I = 0x7f0e0517

.field public static final dlc_overview_download_button:I = 0x7f0e0515

.field public static final dlc_overview_download_size_text:I = 0x7f0e0520

.field public static final dlc_overview_durable_purchase_button:I = 0x7f0e0512

.field public static final dlc_overview_genre_text:I = 0x7f0e0521

.field public static final dlc_overview_languages_text:I = 0x7f0e0523

.field public static final dlc_overview_pin_button:I = 0x7f0e0513

.field public static final dlc_overview_publisher_text:I = 0x7f0e051e

.field public static final dlc_overview_purchased_date:I = 0x7f0e051c

.field public static final dlc_overview_purchased_label:I = 0x7f0e051b

.field public static final dlc_overview_purchased_layout:I = 0x7f0e051a

.field public static final dlc_overview_releasedate_text:I = 0x7f0e0522

.field public static final dlc_overview_screen_body:I = 0x7f0e0510

.field public static final dlc_overview_switch_panel:I = 0x7f0e0511

.field public static final dlc_pegi_ratingview:I = 0x7f0e0519

.field public static final dlc_ratingview:I = 0x7f0e0518

.field public static final dpad_container:I = 0x7f0e0c05

.field public static final drawer_achievements:I = 0x7f0e052d

.field public static final drawer_achievements_icon:I = 0x7f0e052e

.field public static final drawer_achievements_text:I = 0x7f0e052f

.field public static final drawer_activity_feed:I = 0x7f0e052a

.field public static final drawer_clubs:I = 0x7f0e0533

.field public static final drawer_clubs_icon:I = 0x7f0e0534

.field public static final drawer_clubs_text:I = 0x7f0e0535

.field public static final drawer_connection:I = 0x7f0e0542

.field public static final drawer_connection_icon:I = 0x7f0e0543

.field public static final drawer_connection_text:I = 0x7f0e0544

.field public static final drawer_dvr:I = 0x7f0e0530

.field public static final drawer_dvr_icon:I = 0x7f0e0531

.field public static final drawer_dvr_text:I = 0x7f0e0532

.field public static final drawer_feedback:I = 0x7f0e0551

.field public static final drawer_feedback_icon:I = 0x7f0e0552

.field public static final drawer_feedback_text:I = 0x7f0e0553

.field public static final drawer_guide:I = 0x7f0e053f

.field public static final drawer_guide_icon:I = 0x7f0e0540

.field public static final drawer_guide_text:I = 0x7f0e0541

.field public static final drawer_home_icon:I = 0x7f0e052b

.field public static final drawer_home_text:I = 0x7f0e052c

.field public static final drawer_layout:I = 0x7f0e0785

.field public static final drawer_network_test:I = 0x7f0e054e

.field public static final drawer_network_test_icon:I = 0x7f0e054f

.field public static final drawer_network_test_text:I = 0x7f0e0550

.field public static final drawer_pins:I = 0x7f0e0545

.field public static final drawer_pins_icon:I = 0x7f0e0546

.field public static final drawer_pins_text:I = 0x7f0e0547

.field public static final drawer_profile:I = 0x7f0e0525

.field public static final drawer_profile_gamer_score:I = 0x7f0e0529

.field public static final drawer_profile_gamerscore:I = 0x7f0e0528

.field public static final drawer_profile_name:I = 0x7f0e0527

.field public static final drawer_profile_pic:I = 0x7f0e0526

.field public static final drawer_search:I = 0x7f0e054b

.field public static final drawer_search_icon:I = 0x7f0e054c

.field public static final drawer_search_text:I = 0x7f0e054d

.field public static final drawer_settings:I = 0x7f0e0548

.field public static final drawer_settings_icon:I = 0x7f0e0549

.field public static final drawer_settings_text:I = 0x7f0e054a

.field public static final drawer_store:I = 0x7f0e0539

.field public static final drawer_store_icon:I = 0x7f0e053a

.field public static final drawer_store_text:I = 0x7f0e053b

.field public static final drawer_trending:I = 0x7f0e0536

.field public static final drawer_trending_icon:I = 0x7f0e0537

.field public static final drawer_trending_text:I = 0x7f0e0538

.field public static final drawer_tutorial:I = 0x7f0e053c

.field public static final drawer_tutorial_icon:I = 0x7f0e053d

.field public static final drawer_tutorial_text:I = 0x7f0e053e

.field public static final edit_first_name:I = 0x7f0e055c

.field public static final edit_last_name:I = 0x7f0e055d

.field public static final edit_profile_bio_body:I = 0x7f0e0554

.field public static final edit_profile_bio_done_button:I = 0x7f0e0559

.field public static final edit_profile_bio_edit_text:I = 0x7f0e0558

.field public static final edit_profile_resize_body:I = 0x7f0e0555

.field public static final edit_query:I = 0x7f0e00ff

.field public static final edit_real_name:I = 0x7f0e0a29

.field public static final edit_real_name_content_status_switchpanel:I = 0x7f0e055a

.field public static final edit_text_dialog_header:I = 0x7f0e0557

.field public static final edit_view_fixed_length_character_count:I = 0x7f0e0412

.field public static final edit_view_fixed_length_edit:I = 0x7f0e0410

.field public static final edit_view_fixed_length_edit_frame:I = 0x7f0e040d

.field public static final edit_view_fixed_length_title:I = 0x7f0e040b

.field public static final end:I = 0x7f0e009b

.field public static final end_padder:I = 0x7f0e07f5

.field public static final enforcement_relative_layout:I = 0x7f0e0561

.field public static final enforcement_root:I = 0x7f0e0560

.field public static final enforcement_screen:I = 0x7f0e0493

.field public static final enterAlways:I = 0x7f0e0087

.field public static final enterAlwaysCollapsed:I = 0x7f0e0088

.field public static final enter_ip:I = 0x7f0e043b

.field public static final epg_appchannel_connection_error_text_container:I = 0x7f0e0572

.field public static final epg_appchannel_no_shows_error_text_container:I = 0x7f0e0574

.field public static final epg_appchannel_view_instance:I = 0x7f0e0570

.field public static final epg_connection_error_text_line_1:I = 0x7f0e0573

.field public static final epg_firstrun_header:I = 0x7f0e0008

.field public static final epg_firstrun_message:I = 0x7f0e0713

.field public static final epg_inline_details_view_instance:I = 0x7f0e0578

.field public static final epg_inpage_indeterminateProgressBar:I = 0x7f0e0580

.field public static final epg_inpage_progressBarContainer:I = 0x7f0e057f

.field public static final epg_no_shows_text:I = 0x7f0e0575

.field public static final epg_obscured_start:I = 0x7f0e0581

.field public static final epg_progress_icon:I = 0x7f0e0b24

.field public static final epg_progress_text:I = 0x7f0e0b25

.field public static final epg_progress_wait_text:I = 0x7f0e0b26

.field public static final epg_too_many_favorites_message_text:I = 0x7f0e058f

.field public static final error_header:I = 0x7f0e0b1c

.field public static final error_icon_pip:I = 0x7f0e0188

.field public static final error_line_1:I = 0x7f0e0b2a

.field public static final error_screen_refresh:I = 0x7f0e0590

.field public static final error_symbol:I = 0x7f0e0234

.field public static final error_text:I = 0x7f0e0b1d

.field public static final error_text_message:I = 0x7f0e0b21

.field public static final exitUntilCollapsed:I = 0x7f0e0089

.field public static final exo_artwork:I = 0x7f0e0009

.field public static final exo_content_frame:I = 0x7f0e000a

.field public static final exo_controller_placeholder:I = 0x7f0e000b

.field public static final exo_duration:I = 0x7f0e000c

.field public static final exo_ffwd:I = 0x7f0e000d

.field public static final exo_next:I = 0x7f0e000e

.field public static final exo_overlay:I = 0x7f0e000f

.field public static final exo_pause:I = 0x7f0e0010

.field public static final exo_play:I = 0x7f0e0011

.field public static final exo_position:I = 0x7f0e0012

.field public static final exo_prev:I = 0x7f0e0013

.field public static final exo_progress:I = 0x7f0e0014

.field public static final exo_rew:I = 0x7f0e0015

.field public static final exo_shutter:I = 0x7f0e0016

.field public static final exo_subtitles:I = 0x7f0e0017

.field public static final exo_videoplayer_activity_body:I = 0x7f0e0592

.field public static final exo_videoplayer_view:I = 0x7f0e0593

.field public static final expand_activities_button:I = 0x7f0e00de

.field public static final expanded_appbar:I = 0x7f0e0455

.field public static final expanded_appbar_button_container:I = 0x7f0e045d

.field public static final expanded_appbar_now_playing_primary_text:I = 0x7f0e045b

.field public static final expanded_appbar_now_playing_secondary_text:I = 0x7f0e045c

.field public static final expanded_appbar_now_playing_secondary_tile:I = 0x7f0e0459

.field public static final expanded_appbar_now_playing_tile:I = 0x7f0e045a

.field public static final expanded_appbar_now_playing_title_container:I = 0x7f0e0458

.field public static final expanded_appbar_remote_launch:I = 0x7f0e0456

.field public static final expanded_appbar_volume_launch:I = 0x7f0e0457

.field public static final expanded_menu:I = 0x7f0e00f3

.field public static final extras_image:I = 0x7f0e0791

.field public static final extras_text:I = 0x7f0e0792

.field public static final facebook_login_behavior_hint_text:I = 0x7f0e05c0

.field public static final facebook_login_behavior_text:I = 0x7f0e05bf

.field public static final facebook_login_behavior_toggleButton:I = 0x7f0e05be

.field public static final facebook_not_interested_button:I = 0x7f0e09e3

.field public static final facebook_not_interested_button_subtext:I = 0x7f0e09e4

.field public static final facebook_repair_icon:I = 0x7f0e09e6

.field public static final facebook_welcome_card:I = 0x7f0e0afc

.field public static final faq_link:I = 0x7f0e0435

.field public static final favorite_idicator:I = 0x7f0e056a

.field public static final favorites_icon:I = 0x7f0e04b0

.field public static final featured_challenge_tile_challenge_icon:I = 0x7f0e0597

.field public static final featured_challenge_tile_description:I = 0x7f0e059b

.field public static final featured_challenge_tile_image:I = 0x7f0e0596

.field public static final featured_challenge_tile_image_frame:I = 0x7f0e0595

.field public static final featured_challenge_tile_root:I = 0x7f0e0594

.field public static final featured_challenge_tile_timer_text:I = 0x7f0e0599

.field public static final featured_challenge_tile_timerlabel_text:I = 0x7f0e0598

.field public static final featured_challenge_tile_title:I = 0x7f0e059a

.field public static final featured_image:I = 0x7f0e05a2

.field public static final featured_landing_header_text:I = 0x7f0e05a0

.field public static final featured_landing_screen_content:I = 0x7f0e059d

.field public static final featured_landing_screen_switch_panel:I = 0x7f0e059c

.field public static final featured_list:I = 0x7f0e05a1

.field public static final feedback_alias:I = 0x7f0e05ae

.field public static final feedback_dialog:I = 0x7f0e05a3

.field public static final feedback_dialog_close:I = 0x7f0e05a5

.field public static final feedback_message:I = 0x7f0e05ad

.field public static final feedback_negative:I = 0x7f0e05aa

.field public static final feedback_positive:I = 0x7f0e05a9

.field public static final feedback_rate_experience_text:I = 0x7f0e05a7

.field public static final feedback_rating_buttons:I = 0x7f0e05a8

.field public static final feedback_report_problem_checkbox:I = 0x7f0e05ab

.field public static final feedback_report_suggestion_checkbox:I = 0x7f0e05ac

.field public static final feedback_screen_scroll_view:I = 0x7f0e05a6

.field public static final feedback_send:I = 0x7f0e05af

.field public static final feedback_title:I = 0x7f0e05a4

.field public static final fill:I = 0x7f0e0078

.field public static final fill_horizontal:I = 0x7f0e00a5

.field public static final fill_vertical:I = 0x7f0e009c

.field public static final firstline_frame:I = 0x7f0e04db

.field public static final firstplace_container:I = 0x7f0e0ad2

.field public static final firstplace_profile_pic:I = 0x7f0e0ad3

.field public static final firstrow:I = 0x7f0e011d

.field public static final fit:I = 0x7f0e0079

.field public static final fitCenter:I = 0x7f0e00ab

.field public static final fitEnd:I = 0x7f0e00b1

.field public static final fitStart:I = 0x7f0e00b2

.field public static final fitXY:I = 0x7f0e00b3

.field public static final fixed:I = 0x7f0e00c5

.field public static final fixed_height:I = 0x7f0e007a

.field public static final fixed_width:I = 0x7f0e007b

.field public static final focusCrop:I = 0x7f0e00b4

.field public static final follow_btn_icon:I = 0x7f0e0664

.field public static final follow_btn_label:I = 0x7f0e0665

.field public static final font_star_rating_background_stars:I = 0x7f0e05b0

.field public static final font_star_rating_count:I = 0x7f0e05b3

.field public static final font_star_rating_foreground_space:I = 0x7f0e05b2

.field public static final font_star_rating_foreground_stars:I = 0x7f0e05b1

.field public static final footer_faq_link:I = 0x7f0e043c

.field public static final fps_text:I = 0x7f0e05b4

.field public static final friend_finder_cancel_button:I = 0x7f0e05bc

.field public static final friend_finder_confirm_button:I = 0x7f0e05bb

.field public static final friend_finder_confirm_message_error:I = 0x7f0e05b9

.field public static final friend_finder_confirm_message_text:I = 0x7f0e05ba

.field public static final friend_finder_confirm_message_title:I = 0x7f0e05b6

.field public static final friend_finder_confirm_switchpanel:I = 0x7f0e05b5

.field public static final friend_finder_dialog_close:I = 0x7f0e05b7

.field public static final friend_finder_error_symbol:I = 0x7f0e05b8

.field public static final friend_finder_reset_button:I = 0x7f0e05c1

.field public static final friend_picker_button_layout:I = 0x7f0e05cd

.field public static final friendfinder_facebook_login_behavior_dialog:I = 0x7f0e05bd

.field public static final friends_and_games_filter_spinner:I = 0x7f0e093a

.field public static final friends_and_games_list:I = 0x7f0e093c

.field public static final friends_and_games_no_content_state_view:I = 0x7f0e093d

.field public static final friends_and_games_screen_body:I = 0x7f0e0939

.field public static final friends_and_games_switch_panel:I = 0x7f0e093b

.field public static final friends_compare_no_content_text:I = 0x7f0e04d1

.field public static final friends_count_text:I = 0x7f0e097c

.field public static final friends_image:I = 0x7f0e0935

.field public static final friends_listItem_gamertag:I = 0x7f0e05d5

.field public static final friends_listItem_realname:I = 0x7f0e05d6

.field public static final friends_listItem_tile:I = 0x7f0e05d3

.field public static final friends_listItem_tile_container:I = 0x7f0e05d2

.field public static final friends_list_header:I = 0x7f0e05c2

.field public static final friends_picker_bottom_layout:I = 0x7f0e05cc

.field public static final friends_picker_cancel:I = 0x7f0e05c5

.field public static final friends_picker_cancel_button:I = 0x7f0e05cf

.field public static final friends_picker_confirm:I = 0x7f0e05ce

.field public static final friends_picker_layout:I = 0x7f0e05c3

.field public static final friends_picker_search_bar_layout:I = 0x7f0e05c7

.field public static final friends_picker_search_button:I = 0x7f0e05c8

.field public static final friends_picker_search_tag_input:I = 0x7f0e05c9

.field public static final friends_picker_searchbar_clear_button:I = 0x7f0e05ca

.field public static final friends_picker_switch_panel:I = 0x7f0e05d0

.field public static final friends_picker_title:I = 0x7f0e05c6

.field public static final friends_picker_top_layout:I = 0x7f0e05c4

.field public static final friends_played:I = 0x7f0e097d

.field public static final friends_selector_list:I = 0x7f0e05d1

.field public static final friends_selector_no_context_text:I = 0x7f0e04d0

.field public static final friends_who_earned_achievement_list:I = 0x7f0e020c

.field public static final friends_who_play_data_layout:I = 0x7f0e05dc

.field public static final friends_who_play_favorites_icon:I = 0x7f0e05da

.field public static final friends_who_play_gamerpic_image:I = 0x7f0e05d9

.field public static final friends_who_play_gamertag:I = 0x7f0e05dd

.field public static final friends_who_play_image_layout:I = 0x7f0e05d8

.field public static final friends_who_play_list:I = 0x7f0e05e3

.field public static final friends_who_play_page_label:I = 0x7f0e05e1

.field public static final friends_who_play_presence_image:I = 0x7f0e05db

.field public static final friends_who_play_presence_string:I = 0x7f0e05df

.field public static final friends_who_play_realname:I = 0x7f0e05de

.field public static final friends_who_play_screen_body:I = 0x7f0e05e0

.field public static final friends_who_play_switch_panel:I = 0x7f0e05e2

.field public static final game_achievement_comparison_container:I = 0x7f0e05e9

.field public static final game_achievement_comparison_image_container:I = 0x7f0e05ea

.field public static final game_achievement_comparison_item_description_text:I = 0x7f0e05f2

.field public static final game_achievement_comparison_item_gamerscore_icon:I = 0x7f0e05f0

.field public static final game_achievement_comparison_item_gamerscore_text:I = 0x7f0e05f1

.field public static final game_achievement_comparison_item_me_bar:I = 0x7f0e05f8

.field public static final game_achievement_comparison_item_me_date:I = 0x7f0e05f5

.field public static final game_achievement_comparison_item_name:I = 0x7f0e05ed

.field public static final game_achievement_comparison_item_rare_icon:I = 0x7f0e05ef

.field public static final game_achievement_comparison_item_rariry_text:I = 0x7f0e05f3

.field public static final game_achievement_comparison_item_rewards_icon:I = 0x7f0e05ee

.field public static final game_achievement_comparison_item_you_bar:I = 0x7f0e05f9

.field public static final game_achievement_comparison_item_you_date:I = 0x7f0e05f7

.field public static final game_achievement_comparison_stat_container:I = 0x7f0e05e4

.field public static final game_achievement_image_overlay:I = 0x7f0e05ec

.field public static final game_available_on_grid:I = 0x7f0e0639

.field public static final game_available_on_layout:I = 0x7f0e0637

.field public static final game_available_on_title:I = 0x7f0e0638

.field public static final game_companion_button:I = 0x7f0e0626

.field public static final game_desktop_minimum_requirements:I = 0x7f0e0635

.field public static final game_desktop_recommended_requirements:I = 0x7f0e0633

.field public static final game_desktop_requirement_field_name:I = 0x7f0e0602

.field public static final game_desktop_requirement_field_value:I = 0x7f0e0603

.field public static final game_desktop_requirement_min_title:I = 0x7f0e0636

.field public static final game_desktop_requirement_rec_title:I = 0x7f0e0634

.field public static final game_desktop_system_requirements_layout:I = 0x7f0e0631

.field public static final game_detail_header_body:I = 0x7f0e0604

.field public static final game_details_coop_text:I = 0x7f0e0611

.field public static final game_details_developer_text:I = 0x7f0e060b

.field public static final game_details_gameclips_more_button:I = 0x7f0e0616

.field public static final game_details_genre_text:I = 0x7f0e060e

.field public static final game_details_header_ratingbar:I = 0x7f0e0608

.field public static final game_details_header_release_data:I = 0x7f0e0607

.field public static final game_details_header_tile:I = 0x7f0e0605

.field public static final game_details_header_title:I = 0x7f0e0606

.field public static final game_details_header_title_small:I = 0x7f0e060a

.field public static final game_details_multiplayer_text:I = 0x7f0e0610

.field public static final game_details_publisher_text:I = 0x7f0e060c

.field public static final game_details_rating_text:I = 0x7f0e060d

.field public static final game_details_releasedate_text:I = 0x7f0e060f

.field public static final game_gallery_image:I = 0x7f0e0617

.field public static final game_gallery_list:I = 0x7f0e061a

.field public static final game_gallery_screen_body:I = 0x7f0e0618

.field public static final game_gallery_switch_panel:I = 0x7f0e0619

.field public static final game_image:I = 0x7f0e097a

.field public static final game_overview_description_text:I = 0x7f0e0630

.field public static final game_overview_description_title:I = 0x7f0e062f

.field public static final game_overview_gameprofile_button:I = 0x7f0e0624

.field public static final game_overview_help_btn_ring_icon:I = 0x7f0e062a

.field public static final game_overview_help_btn_text:I = 0x7f0e062b

.field public static final game_overview_help_button:I = 0x7f0e0629

.field public static final game_overview_install_button:I = 0x7f0e061e

.field public static final game_overview_pegi_ratingview:I = 0x7f0e063b

.field public static final game_overview_pin_button:I = 0x7f0e0625

.field public static final game_overview_play_button:I = 0x7f0e061f

.field public static final game_overview_purchase_button:I = 0x7f0e0620

.field public static final game_overview_purchased_date:I = 0x7f0e062e

.field public static final game_overview_purchased_label:I = 0x7f0e062d

.field public static final game_overview_purchased_layout:I = 0x7f0e062c

.field public static final game_overview_ratingview:I = 0x7f0e063a

.field public static final game_overview_screen_body:I = 0x7f0e061b

.field public static final game_overview_switch_panel:I = 0x7f0e061c

.field public static final game_platform_icon:I = 0x7f0e0600

.field public static final game_platform_label:I = 0x7f0e0601

.field public static final game_profile_btn_icon:I = 0x7f0e0663

.field public static final game_profile_btn_label:I = 0x7f0e0628

.field public static final game_progressdetails_screen_body:I = 0x7f0e0612

.field public static final game_progressdetails_switch_panel:I = 0x7f0e0613

.field public static final game_scrore_view:I = 0x7f0e0685

.field public static final game_stats_container:I = 0x7f0e07d2

.field public static final game_stream_test_message:I = 0x7f0e063d

.field public static final game_stream_test_spinner:I = 0x7f0e063e

.field public static final game_title:I = 0x7f0e097b

.field public static final game_xpa_badging:I = 0x7f0e0609

.field public static final game_xpa_icon:I = 0x7f0e0641

.field public static final game_xpa_label_play_anywhere:I = 0x7f0e0643

.field public static final game_xpa_label_xbox:I = 0x7f0e0642

.field public static final game_xpa_platform_filter:I = 0x7f0e0623

.field public static final game_xpa_platform_filter_label:I = 0x7f0e0622

.field public static final game_xpa_platform_filter_layout:I = 0x7f0e0621

.field public static final game_xpa_system_requirements_title:I = 0x7f0e0632

.field public static final gamedetail_gameclip_list_nodata:I = 0x7f0e0614

.field public static final gamedetail_gameclips_list_layout:I = 0x7f0e0615

.field public static final gamepass_description_text:I = 0x7f0e061d

.field public static final gameprofile_achievement_gamerscore:I = 0x7f0e0687

.field public static final gameprofile_achievement_hero_stat:I = 0x7f0e0683

.field public static final gameprofile_achievement_leaderboard_crown:I = 0x7f0e068b

.field public static final gameprofile_achievement_stat_details:I = 0x7f0e0684

.field public static final gameprofile_achievements_filter_spinner:I = 0x7f0e0644

.field public static final gameprofile_achievements_list:I = 0x7f0e0647

.field public static final gameprofile_achievements_nodata:I = 0x7f0e06a2

.field public static final gameprofile_achievements_switch_panel:I = 0x7f0e0646

.field public static final gameprofile_broadcast_icon:I = 0x7f0e0680

.field public static final gameprofile_broadcast_icon_frame:I = 0x7f0e067f

.field public static final gameprofile_broadcasting_subtitle:I = 0x7f0e0682

.field public static final gameprofile_capture_screen_body:I = 0x7f0e064d

.field public static final gameprofile_captures_filter_spinner:I = 0x7f0e064f

.field public static final gameprofile_captures_list:I = 0x7f0e0651

.field public static final gameprofile_captures_nodata:I = 0x7f0e0652

.field public static final gameprofile_captures_screen_body:I = 0x7f0e064e

.field public static final gameprofile_captures_switch_panel:I = 0x7f0e0650

.field public static final gameprofile_compare_achievement_button:I = 0x7f0e0645

.field public static final gameprofile_friends_and_suggestions_screen_body:I = 0x7f0e0655

.field public static final gameprofile_friends_favorites_icon:I = 0x7f0e065c

.field public static final gameprofile_friends_gamerpic_image:I = 0x7f0e065b

.field public static final gameprofile_friends_gamertag:I = 0x7f0e0660

.field public static final gameprofile_friends_image_layout:I = 0x7f0e065a

.field public static final gameprofile_friends_presence_image:I = 0x7f0e065d

.field public static final gameprofile_friends_presence_string:I = 0x7f0e0662

.field public static final gameprofile_friends_realname:I = 0x7f0e0661

.field public static final gameprofile_friends_spotlight_image:I = 0x7f0e067e

.field public static final gameprofile_friends_spotlight_label:I = 0x7f0e067d

.field public static final gameprofile_friends_who_play_list:I = 0x7f0e0657

.field public static final gameprofile_friends_who_play_switch_panel:I = 0x7f0e0656

.field public static final gameprofile_gameprogress_text:I = 0x7f0e0688

.field public static final gameprofile_gamerscore_icon:I = 0x7f0e0686

.field public static final gameprofile_info_description:I = 0x7f0e066f

.field public static final gameprofile_info_developer:I = 0x7f0e066e

.field public static final gameprofile_info_follow_button:I = 0x7f0e066d

.field public static final gameprofile_info_image:I = 0x7f0e066a

.field public static final gameprofile_info_play_button:I = 0x7f0e066b

.field public static final gameprofile_info_screen_body:I = 0x7f0e0668

.field public static final gameprofile_info_switch_panel:I = 0x7f0e0669

.field public static final gameprofile_info_view_in_store_button:I = 0x7f0e066c

.field public static final gameprofile_lfg_screen_body:I = 0x7f0e0670

.field public static final gameprofile_people_list_gamer:I = 0x7f0e065e

.field public static final gameprofile_people_list_header:I = 0x7f0e0658

.field public static final gameprofile_people_list_state_text:I = 0x7f0e0659

.field public static final gameprofile_pivot:I = 0x7f0e067b

.field public static final gameprofile_spotlight_section:I = 0x7f0e067c

.field public static final gameprofile_who_is_broadcasting:I = 0x7f0e0681

.field public static final gameprofile_your_clubs_header:I = 0x7f0e0653

.field public static final gameprofile_your_clubs_list:I = 0x7f0e0654

.field public static final gameprogress_achievements_comparisonbar:I = 0x7f0e0696

.field public static final gameprogress_achievements_description_text:I = 0x7f0e0691

.field public static final gameprogress_achievements_gamerscore_icon:I = 0x7f0e0690

.field public static final gameprogress_achievements_gamerscore_text:I = 0x7f0e0323

.field public static final gameprogress_achievements_image:I = 0x7f0e05eb

.field public static final gameprogress_achievements_lock_icon:I = 0x7f0e0693

.field public static final gameprogress_achievements_me_lock_icon:I = 0x7f0e05f4

.field public static final gameprogress_achievements_progress_text:I = 0x7f0e0695

.field public static final gameprogress_achievements_rare_icon:I = 0x7f0e068f

.field public static final gameprogress_achievements_rarity_text:I = 0x7f0e0692

.field public static final gameprogress_achievements_rewards_icon:I = 0x7f0e068e

.field public static final gameprogress_achievements_timer_icon:I = 0x7f0e0697

.field public static final gameprogress_achievements_timer_text:I = 0x7f0e0698

.field public static final gameprogress_achievements_title:I = 0x7f0e068d

.field public static final gameprogress_achievements_unlocked_details_text:I = 0x7f0e0694

.field public static final gameprogress_achievements_unlocked_text:I = 0x7f0e0699

.field public static final gameprogress_achievements_you_lock_icon:I = 0x7f0e05f6

.field public static final gameprogress_challenges_list:I = 0x7f0e069c

.field public static final gameprogress_challenges_screen_body:I = 0x7f0e069a

.field public static final gameprogress_challenges_switch_panel:I = 0x7f0e069b

.field public static final gameprogress_gameclips_filter_spinner:I = 0x7f0e069e

.field public static final gameprogress_gameclips_list:I = 0x7f0e06a0

.field public static final gameprogress_gameclips_nodata:I = 0x7f0e06a1

.field public static final gameprogress_gameclips_screen_body:I = 0x7f0e069d

.field public static final gameprogress_gameclips_switch_panel:I = 0x7f0e069f

.field public static final gameprogress_leaderboard_header_text:I = 0x7f0e0aca

.field public static final gameprogress_leaderboard_list:I = 0x7f0e0acc

.field public static final gameprogress_leaderboard_my_entry:I = 0x7f0e0acd

.field public static final gameprogress_leaderboard_row_gamertag:I = 0x7f0e0ac6

.field public static final gameprogress_leaderboard_row_rank:I = 0x7f0e0ac5

.field public static final gameprogress_leaderboard_row_realname:I = 0x7f0e0ac7

.field public static final gameprogress_leaderboard_row_score:I = 0x7f0e0ac8

.field public static final gameprogress_leaderboard_screen_body:I = 0x7f0e0ac9

.field public static final gameprogress_leaderboard_switch_panel:I = 0x7f0e0acb

.field public static final gameprogress_summary_switch_panel:I = 0x7f0e0423

.field public static final gamerscore:I = 0x7f0e07d4

.field public static final gamerscore_container:I = 0x7f0e031f

.field public static final gamerscore_leaderboard_date:I = 0x7f0e06ab

.field public static final gamerscore_leaderboard_leader_gamerpic:I = 0x7f0e06ac

.field public static final gamerscore_leaderboard_leader_gamertag:I = 0x7f0e06ad

.field public static final gamerscore_leaderboard_leader_monthly_gamerscore:I = 0x7f0e06ae

.field public static final gamerscore_leaderboard_list:I = 0x7f0e06af

.field public static final gamerscore_leaderboard_summary_date:I = 0x7f0e08ff

.field public static final gamerscore_leaderboard_summary_header:I = 0x7f0e08fe

.field public static final gamerscore_leaderboard_summary_title:I = 0x7f0e06aa

.field public static final gamertag_search_bar:I = 0x7f0e06b4

.field public static final gamertag_search_button:I = 0x7f0e06b5

.field public static final gamertag_search_close_icon:I = 0x7f0e06b2

.field public static final gamertag_search_data_activity_body:I = 0x7f0e06b1

.field public static final gamertag_search_error:I = 0x7f0e06b6

.field public static final gamertag_search_flyout_layout:I = 0x7f0e06b0

.field public static final gamertag_search_flyout_soft_dismiss_bottom:I = 0x7f0e06b9

.field public static final gamertag_search_flyout_soft_dismiss_left:I = 0x7f0e06ba

.field public static final gamertag_search_flyout_soft_dismiss_right:I = 0x7f0e06bb

.field public static final gamertag_search_flyout_soft_dismiss_top:I = 0x7f0e06b3

.field public static final gamertag_search_list:I = 0x7f0e06b8

.field public static final gamertag_searching:I = 0x7f0e06b7

.field public static final gamertag_text:I = 0x7f0e023f

.field public static final gamescore_icon:I = 0x7f0e07d3

.field public static final generic_motion_text:I = 0x7f0e063f

.field public static final generic_no_content_icon:I = 0x7f0e06bc

.field public static final generic_no_content_text:I = 0x7f0e06bd

.field public static final gray:I = 0x7f0e00c3

.field public static final groupAction:I = 0x7f0e0bb4

.field public static final groupColor:I = 0x7f0e0bb8

.field public static final groupCustom:I = 0x7f0e0bb5

.field public static final groupCustomButton:I = 0x7f0e0bb6

.field public static final groupNavigation:I = 0x7f0e0bbb

.field public static final groupNumpad:I = 0x7f0e0bba

.field public static final groupPlayback:I = 0x7f0e0bb9

.field public static final groupPower:I = 0x7f0e0bb2

.field public static final groupVolume:I = 0x7f0e0bbc

.field public static final group_action:I = 0x7f0e00c7

.field public static final group_color:I = 0x7f0e00c8

.field public static final group_custom:I = 0x7f0e00c9

.field public static final group_custom_full:I = 0x7f0e00ca

.field public static final group_messaging_dialog_close_button:I = 0x7f0e04cc

.field public static final group_messaging_dialog_title_text:I = 0x7f0e09be

.field public static final group_messaging_rename_editText:I = 0x7f0e09bf

.field public static final group_messaging_title_layout:I = 0x7f0e09bd

.field public static final group_messaging_view_all_list:I = 0x7f0e04cf

.field public static final group_messaging_view_all_title_layout:I = 0x7f0e04cb

.field public static final group_nav:I = 0x7f0e00cb

.field public static final group_numpad:I = 0x7f0e00cc

.field public static final group_playback:I = 0x7f0e00cd

.field public static final group_selector_body:I = 0x7f0e04b8

.field public static final group_selector_bottom_layout:I = 0x7f0e04ba

.field public static final group_selector_button_layout:I = 0x7f0e04bb

.field public static final group_selector_firstLine:I = 0x7f0e04c5

.field public static final group_selector_header_section:I = 0x7f0e04c1

.field public static final group_selector_image_layout:I = 0x7f0e04c2

.field public static final group_selector_images_container:I = 0x7f0e04c4

.field public static final group_selector_secondline:I = 0x7f0e04c7

.field public static final group_selector_top_layout:I = 0x7f0e04b9

.field public static final group_volume:I = 0x7f0e00ce

.field public static final has_stats:I = 0x7f0e0018

.field public static final header_main:I = 0x7f0e0461

.field public static final header_row_count:I = 0x7f0e06bf

.field public static final header_row_text:I = 0x7f0e06be

.field public static final header_section:I = 0x7f0e04ad

.field public static final height:I = 0x7f0e006d

.field public static final help_for_no_wifi_error:I = 0x7f0e0424

.field public static final hero_stat:I = 0x7f0e07cc

.field public static final home:I = 0x7f0e0019

.field public static final homeAsUp:I = 0x7f0e0082

.field public static final home_screen_activity_feed_recommendation_person_row:I = 0x7f0e06c5

.field public static final home_screen_activity_feed_trending_container:I = 0x7f0e06d1

.field public static final home_screen_activity_feed_trending_description:I = 0x7f0e06d2

.field public static final home_screen_activity_feed_trending_see_all_button:I = 0x7f0e06d3

.field public static final home_screen_pin:I = 0x7f0e001a

.field public static final home_screen_popup_close:I = 0x7f0e06d9

.field public static final home_screen_popup_cmd_image:I = 0x7f0e06e3

.field public static final home_screen_popup_cmd_text:I = 0x7f0e06e4

.field public static final home_screen_popup_companion_text:I = 0x7f0e06e0

.field public static final home_screen_popup_container:I = 0x7f0e06e2

.field public static final home_screen_popup_provider:I = 0x7f0e06de

.field public static final home_screen_popup_root:I = 0x7f0e06d8

.field public static final home_screen_popup_scroller:I = 0x7f0e06e1

.field public static final home_screen_popup_tile_header_container:I = 0x7f0e06da

.field public static final home_screen_popup_tile_image:I = 0x7f0e06dc

.field public static final home_screen_popup_tile_image_container:I = 0x7f0e06db

.field public static final home_screen_popup_title:I = 0x7f0e06df

.field public static final home_screen_popup_top_header:I = 0x7f0e06dd

.field public static final home_screen_recent:I = 0x7f0e001b

.field public static final home_screen_section_bat_full_achievements:I = 0x7f0e06ee

.field public static final home_screen_section_bat_full_achievements_cotnainer:I = 0x7f0e06ec

.field public static final home_screen_section_bat_full_game_stats_container:I = 0x7f0e06f1

.field public static final home_screen_section_bat_full_gamescore:I = 0x7f0e06f4

.field public static final home_screen_section_bat_full_gamescore_container:I = 0x7f0e06f3

.field public static final home_screen_section_bat_full_gamescore_icon:I = 0x7f0e06f2

.field public static final home_screen_section_bat_full_hero_stat:I = 0x7f0e06eb

.field public static final home_screen_section_bat_full_icons:I = 0x7f0e06f6

.field public static final home_screen_section_bat_full_image:I = 0x7f0e06e8

.field public static final home_screen_section_bat_full_max_gamescore:I = 0x7f0e06f5

.field public static final home_screen_section_bat_full_media_title:I = 0x7f0e06ea

.field public static final home_screen_section_bat_full_new_feature:I = 0x7f0e06f8

.field public static final home_screen_section_bat_full_oneguide_icon:I = 0x7f0e06f7

.field public static final home_screen_section_bat_full_progress:I = 0x7f0e06f0

.field public static final home_screen_section_bat_full_promo_container:I = 0x7f0e06f9

.field public static final home_screen_section_bat_full_promo_text:I = 0x7f0e06fa

.field public static final home_screen_section_bat_full_smartglass_icon:I = 0x7f0e06ef

.field public static final home_screen_section_bat_full_title_container:I = 0x7f0e06e9

.field public static final home_screen_section_bat_full_trophy_icon:I = 0x7f0e06ed

.field public static final home_screen_section_body:I = 0x7f0e06e5

.field public static final home_screen_section_body_container:I = 0x7f0e06e7

.field public static final home_screen_section_featured_container:I = 0x7f0e06fc

.field public static final home_screen_section_featured_switch_panel:I = 0x7f0e06fb

.field public static final home_screen_section_grid:I = 0x7f0e06fd

.field public static final home_screen_section_now_playing:I = 0x7f0e06fe

.field public static final home_screen_section_now_playing_bat_extra_grid:I = 0x7f0e06ff

.field public static final home_screen_section_now_playing_bat_full_container:I = 0x7f0e0700

.field public static final home_screen_section_now_playing_promo_container:I = 0x7f0e0703

.field public static final home_screen_section_now_playing_promo_image:I = 0x7f0e0704

.field public static final home_screen_section_now_playing_promo_text1:I = 0x7f0e0705

.field public static final home_screen_section_now_playing_promo_text2:I = 0x7f0e0706

.field public static final home_screen_section_now_playing_recents_switch_panel:I = 0x7f0e0701

.field public static final home_screen_section_now_playing_rectents_grid:I = 0x7f0e0702

.field public static final home_screen_section_subheader:I = 0x7f0e06e6

.field public static final home_screen_swipe_container:I = 0x7f0e06c0

.field public static final hover_chat_menu_pivot:I = 0x7f0e0707

.field public static final hover_chat_menu_settings:I = 0x7f0e0708

.field public static final html_surface:I = 0x7f0e021c

.field public static final icon:I = 0x7f0e00e2

.field public static final icon_group:I = 0x7f0e07f0

.field public static final icon_only:I = 0x7f0e00be

.field public static final icon_selected:I = 0x7f0e05d7

.field public static final iconfont_toggle_btn_icon:I = 0x7f0e070e

.field public static final iconfont_toggle_btn_text:I = 0x7f0e070f

.field public static final icons:I = 0x7f0e07d6

.field public static final ie_clear_button:I = 0x7f0e07fb

.field public static final ie_controls:I = 0x7f0e07f6

.field public static final ie_controls_container:I = 0x7f0e07f7

.field public static final ie_editText:I = 0x7f0e07f8

.field public static final ie_refresh_button:I = 0x7f0e07f9

.field public static final ie_stop_button:I = 0x7f0e07fa

.field public static final ifRoom:I = 0x7f0e00bb

.field public static final image:I = 0x7f0e00df

.field public static final imageUserTile:I = 0x7f0e0111

.field public static final image_bound:I = 0x7f0e001c

.field public static final image_callback:I = 0x7f0e001d

.field public static final images_container_row1:I = 0x7f0e046a

.field public static final images_container_row2:I = 0x7f0e046d

.field public static final imageviewer_image:I = 0x7f0e0710

.field public static final imageviewer_imageFrame:I = 0x7f0e0712

.field public static final imageviewer_loading:I = 0x7f0e023e

.field public static final imageviwer_layout:I = 0x7f0e0711

.field public static final included_content_list:I = 0x7f0e071a

.field public static final included_content_listitem_image:I = 0x7f0e0714

.field public static final included_content_listitem_rating:I = 0x7f0e0717

.field public static final included_content_listitem_release_date:I = 0x7f0e0716

.field public static final included_content_listitem_title:I = 0x7f0e0715

.field public static final included_content_screen_body:I = 0x7f0e0718

.field public static final included_content_switch_panel:I = 0x7f0e0719

.field public static final index_position:I = 0x7f0e001e

.field public static final info:I = 0x7f0e07ec

.field public static final info_button:I = 0x7f0e0b3b

.field public static final inline:I = 0x7f0e00d4

.field public static final install_btn_icon:I = 0x7f0e071b

.field public static final install_btn_label:I = 0x7f0e071c

.field public static final install_btn_subscription_icon:I = 0x7f0e071f

.field public static final install_btn_subscription_layout:I = 0x7f0e071d

.field public static final install_btn_subscription_text:I = 0x7f0e071e

.field public static final invite_phone_call_me_button:I = 0x7f0e0965

.field public static final invite_phone_change_region_button:I = 0x7f0e0942

.field public static final invite_phone_contact_image_frame:I = 0x7f0e096f

.field public static final invite_phone_friends_subtext:I = 0x7f0e0970

.field public static final invite_phone_friends_text:I = 0x7f0e0964

.field public static final invite_phone_invitation_button:I = 0x7f0e096e

.field public static final invite_phone_resend_the_code_button:I = 0x7f0e0962

.field public static final invite_screen_title:I = 0x7f0e039d

.field public static final item_touch_helper_previous_elevation:I = 0x7f0e001f

.field public static final jfilter_search_activity_body:I = 0x7f0e09cc

.field public static final jfilter_search_activity_layout:I = 0x7f0e09cb

.field public static final jfilter_search_list_view:I = 0x7f0e09cd

.field public static final jfilter_search_type_name:I = 0x7f0e09ce

.field public static final large:I = 0x7f0e00d8

.field public static final leaderboard_item_bar:I = 0x7f0e06a9

.field public static final leaderboard_item_gamerpic:I = 0x7f0e06a3

.field public static final leaderboard_item_gamertag:I = 0x7f0e06a6

.field public static final leaderboard_item_monthly_gamerscore:I = 0x7f0e06a8

.field public static final leaderboard_item_ribbon:I = 0x7f0e06a5

.field public static final leaderboard_item_ribbon_layout:I = 0x7f0e06a4

.field public static final leaderboard_item_subtext:I = 0x7f0e06a7

.field public static final leaderboard_leader_action_send_message:I = 0x7f0e0c2d

.field public static final leaderboard_leader_action_share:I = 0x7f0e0c2b

.field public static final leaderboard_leader_action_view_profile:I = 0x7f0e0c2c

.field public static final leaderboard_nodata:I = 0x7f0e0acf

.field public static final leaderboard_nostats:I = 0x7f0e0ace

.field public static final leave_conversation_frame:I = 0x7f0e047c

.field public static final left:I = 0x7f0e009d

.field public static final left_button:I = 0x7f0e0ac1

.field public static final lfg_available_sessions_list_view:I = 0x7f0e0679

.field public static final lfg_content_switch_panel:I = 0x7f0e0678

.field public static final lfg_create_button:I = 0x7f0e0675

.field public static final lfg_create_details_body:I = 0x7f0e0720

.field public static final lfg_create_details_body_upper_frame:I = 0x7f0e0722

.field public static final lfg_create_details_close:I = 0x7f0e0724

.field public static final lfg_create_details_header:I = 0x7f0e0723

.field public static final lfg_create_letsplay_button:I = 0x7f0e0721

.field public static final lfg_create_need_count:I = 0x7f0e0736

.field public static final lfg_create_select_game_screen_layout:I = 0x7f0e0ad9

.field public static final lfg_create_session_image:I = 0x7f0e0726

.field public static final lfg_create_session_tags:I = 0x7f0e0728

.field public static final lfg_create_session_text:I = 0x7f0e0727

.field public static final lfg_create_session_title:I = 0x7f0e0725

.field public static final lfg_create_tags_button_frame:I = 0x7f0e0729

.field public static final lfg_current_tag_count:I = 0x7f0e0aa8

.field public static final lfg_data_container:I = 0x7f0e0756

.field public static final lfg_description:I = 0x7f0e074a

.field public static final lfg_description_report_button:I = 0x7f0e074e

.field public static final lfg_detail_row_have_text_count:I = 0x7f0e075a

.field public static final lfg_detail_row_need_text_count:I = 0x7f0e0758

.field public static final lfg_detail_row_postedtime:I = 0x7f0e074b

.field public static final lfg_detail_row_start_text_time:I = 0x7f0e075c

.field public static final lfg_details_body:I = 0x7f0e0740

.field public static final lfg_details_close:I = 0x7f0e0744

.field public static final lfg_details_header:I = 0x7f0e0742

.field public static final lfg_edit_tag_button:I = 0x7f0e0674

.field public static final lfg_error_loading_sessions:I = 0x7f0e067a

.field public static final lfg_filter_language_spinner:I = 0x7f0e0677

.field public static final lfg_filter_time_spinner:I = 0x7f0e0676

.field public static final lfg_header_close:I = 0x7f0e0743

.field public static final lfg_interested_button:I = 0x7f0e074f

.field public static final lfg_interested_list:I = 0x7f0e0750

.field public static final lfg_join_button:I = 0x7f0e0741

.field public static final lfg_list_error:I = 0x7f0e083d

.field public static final lfg_member_gamertag:I = 0x7f0e076e

.field public static final lfg_member_image:I = 0x7f0e076d

.field public static final lfg_member_popup_decline:I = 0x7f0e0c2f

.field public static final lfg_member_popup_view_profile:I = 0x7f0e0c2e

.field public static final lfg_member_row_header:I = 0x7f0e076c

.field public static final lfg_member_stat:I = 0x7f0e076f

.field public static final lfg_party_language_frame:I = 0x7f0e073c

.field public static final lfg_party_language_text:I = 0x7f0e073d

.field public static final lfg_party_start_day_frame:I = 0x7f0e0730

.field public static final lfg_party_start_day_text:I = 0x7f0e0731

.field public static final lfg_party_start_time_frame:I = 0x7f0e0733

.field public static final lfg_party_start_time_text:I = 0x7f0e0734

.field public static final lfg_row_club_title_text:I = 0x7f0e0765

.field public static final lfg_row_data:I = 0x7f0e0755

.field public static final lfg_row_description:I = 0x7f0e0767

.field public static final lfg_row_have_text:I = 0x7f0e0759

.field public static final lfg_row_have_text_count:I = 0x7f0e0761

.field public static final lfg_row_header:I = 0x7f0e0769

.field public static final lfg_row_header_text:I = 0x7f0e076a

.field public static final lfg_row_item:I = 0x7f0e0763

.field public static final lfg_row_need_text:I = 0x7f0e0757

.field public static final lfg_row_need_text_count:I = 0x7f0e0760

.field public static final lfg_row_postedtime:I = 0x7f0e0768

.field public static final lfg_row_session_title:I = 0x7f0e0764

.field public static final lfg_row_start_text:I = 0x7f0e075b

.field public static final lfg_row_start_text_time:I = 0x7f0e0762

.field public static final lfg_row_tag_list:I = 0x7f0e0766

.field public static final lfg_row_title_game_image:I = 0x7f0e075f

.field public static final lfg_row_title_host_image:I = 0x7f0e075e

.field public static final lfg_row_title_image:I = 0x7f0e075d

.field public static final lfg_see_detail:I = 0x7f0e074c

.field public static final lfg_session_title:I = 0x7f0e0745

.field public static final lfg_share_checkbox:I = 0x7f0e073f

.field public static final lfg_share_post:I = 0x7f0e074d

.field public static final lfg_tag_list:I = 0x7f0e0749

.field public static final lfg_tag_search_button:I = 0x7f0e0673

.field public static final lfg_tag_search_layout:I = 0x7f0e0671

.field public static final lfg_title_banner:I = 0x7f0e06c2

.field public static final lfg_title_image:I = 0x7f0e0746

.field public static final lfg_title_text:I = 0x7f0e0747

.field public static final lfg_top_data_layout:I = 0x7f0e0748

.field public static final lfg_upcoming_no_data:I = 0x7f0e076b

.field public static final lfg_vetting_back_button:I = 0x7f0e0776

.field public static final lfg_vetting_body:I = 0x7f0e0770

.field public static final lfg_vetting_bottom_row:I = 0x7f0e0775

.field public static final lfg_vetting_close:I = 0x7f0e0771

.field public static final lfg_vetting_confirm_button:I = 0x7f0e0752

.field public static final lfg_vetting_decline_button:I = 0x7f0e0753

.field public static final lfg_vetting_interested_label:I = 0x7f0e0772

.field public static final lfg_vetting_interested_list:I = 0x7f0e0774

.field public static final lfg_vetting_interested_text:I = 0x7f0e0751

.field public static final lfg_vetting_more_button:I = 0x7f0e0754

.field public static final lfg_vetting_need_label:I = 0x7f0e0773

.field public static final lfg_vetting_popup_report:I = 0x7f0e0c31

.field public static final lfg_vetting_popup_view_profile:I = 0x7f0e0c30

.field public static final lfg_view_all_details_back_button:I = 0x7f0e0778

.field public static final lfg_view_all_details_description_text:I = 0x7f0e077c

.field public static final lfg_view_all_details_description_title:I = 0x7f0e077b

.field public static final lfg_view_all_details_tag_title:I = 0x7f0e0779

.field public static final lfg_view_all_details_tags_list:I = 0x7f0e077a

.field public static final lfg_view_all_details_title:I = 0x7f0e0777

.field public static final light:I = 0x7f0e00c2

.field public static final light_details_instance:I = 0x7f0e0aff

.field public static final line1:I = 0x7f0e07f1

.field public static final line3:I = 0x7f0e07f3

.field public static final link_facebook_image:I = 0x7f0e09dd

.field public static final link_facebook_view:I = 0x7f0e09dc

.field public static final link_to_facebook_image:I = 0x7f0e08df

.field public static final link_to_facebook_item:I = 0x7f0e08de

.field public static final link_to_facebook_subtext:I = 0x7f0e08e1

.field public static final link_to_facebook_text:I = 0x7f0e08e0

.field public static final link_to_phone_contact_image:I = 0x7f0e08e4

.field public static final link_to_phone_contact_image_frame:I = 0x7f0e08e3

.field public static final link_to_phone_contact_item:I = 0x7f0e08e2

.field public static final link_to_phone_contact_subtext:I = 0x7f0e08e6

.field public static final link_to_phone_contact_text:I = 0x7f0e08e5

.field public static final listAccounts:I = 0x7f0e0110

.field public static final listMode:I = 0x7f0e007e

.field public static final list_content:I = 0x7f0e05d4

.field public static final list_item:I = 0x7f0e00e1

.field public static final list_item_view_type:I = 0x7f0e0020

.field public static final loadSpinner:I = 0x7f0e0190

.field public static final load_more_listview_footer:I = 0x7f0e077e

.field public static final load_more_listview_footer_layout:I = 0x7f0e077d

.field public static final loader_acc_prov_get_profile:I = 0x7f0e0021

.field public static final loader_claim_gamertag:I = 0x7f0e0022

.field public static final loader_event_initialization:I = 0x7f0e0023

.field public static final loader_finish_sign_in:I = 0x7f0e0024

.field public static final loader_get_privacy_settings:I = 0x7f0e0025

.field public static final loader_header_get_profile:I = 0x7f0e0026

.field public static final loader_intro_gamer_image:I = 0x7f0e0027

.field public static final loader_intro_gamer_profile:I = 0x7f0e0028

.field public static final loader_post_profile:I = 0x7f0e0029

.field public static final loader_reserve_gamertag:I = 0x7f0e002a

.field public static final loader_set_privacy_settings:I = 0x7f0e002b

.field public static final loader_sign_out:I = 0x7f0e002c

.field public static final loader_start_sign_in:I = 0x7f0e002d

.field public static final loader_suggestions:I = 0x7f0e002e

.field public static final loader_user_image_url:I = 0x7f0e002f

.field public static final loader_welcome_gamer_image:I = 0x7f0e0030

.field public static final loader_welcome_gamer_profile:I = 0x7f0e0031

.field public static final loader_x_token:I = 0x7f0e0032

.field public static final loader_xb_login:I = 0x7f0e0033

.field public static final loader_xb_logout:I = 0x7f0e0034

.field public static final login_body:I = 0x7f0e077f

.field public static final login_buttons:I = 0x7f0e0782

.field public static final login_logo:I = 0x7f0e0781

.field public static final login_version:I = 0x7f0e0784

.field public static final logo:I = 0x7f0e056b

.field public static final manage_notifications_frame:I = 0x7f0e02d8

.field public static final max_gamerscore:I = 0x7f0e07d5

.field public static final media_actions:I = 0x7f0e07e5

.field public static final media_buttons:I = 0x7f0e07ff

.field public static final media_buttons_container:I = 0x7f0e078a

.field public static final media_extras_list:I = 0x7f0e0795

.field public static final media_extras_screen_body:I = 0x7f0e0793

.field public static final media_extras_switch_panel:I = 0x7f0e0794

.field public static final media_future_showtimes_airdate:I = 0x7f0e0797

.field public static final media_future_showtimes_callSign:I = 0x7f0e079a

.field public static final media_future_showtimes_channelNumber:I = 0x7f0e079b

.field public static final media_future_showtimes_channel_logo:I = 0x7f0e0799

.field public static final media_future_showtimes_provider:I = 0x7f0e0798

.field public static final media_future_showtimes_title:I = 0x7f0e0796

.field public static final media_futureshowtimes_list:I = 0x7f0e079e

.field public static final media_futureshowtimes_screen_body:I = 0x7f0e079c

.field public static final media_futureshowtimes_switch_panel:I = 0x7f0e079d

.field public static final media_item_primary_text:I = 0x7f0e07a0

.field public static final media_item_secondary_text:I = 0x7f0e07a1

.field public static final media_item_tile_image:I = 0x7f0e079f

.field public static final media_progress_bar:I = 0x7f0e07a6

.field public static final media_progress_bar_layout:I = 0x7f0e07a3

.field public static final media_progress_text:I = 0x7f0e07a4

.field public static final media_seek_bar:I = 0x7f0e07a5

.field public static final media_title:I = 0x7f0e07cb

.field public static final member_data_layout:I = 0x7f0e04da

.field public static final member_favorites_icon:I = 0x7f0e04d8

.field public static final member_gameractivity:I = 0x7f0e04df

.field public static final member_gamertag:I = 0x7f0e04dc

.field public static final member_image:I = 0x7f0e04d7

.field public static final member_image_layout:I = 0x7f0e04d6

.field public static final member_item_presence_image:I = 0x7f0e04d9

.field public static final member_item_section:I = 0x7f0e04d4

.field public static final member_realname:I = 0x7f0e04dd

.field public static final member_section:I = 0x7f0e04d5

.field public static final members_can_textview:I = 0x7f0e031e

.field public static final menu_alert:I = 0x7f0e0035

.field public static final menu_console_connect:I = 0x7f0e0036

.field public static final menu_debug_companion:I = 0x7f0e0037

.field public static final menu_debug_crashme:I = 0x7f0e0038

.field public static final menu_debug_create_database:I = 0x7f0e0039

.field public static final menu_debug_create_database_tables:I = 0x7f0e003a

.field public static final menu_debug_delete_database_tables:I = 0x7f0e003b

.field public static final menu_debug_enable_arches:I = 0x7f0e003c

.field public static final menu_debug_enable_arches_rtm:I = 0x7f0e003d

.field public static final menu_debug_enable_beam_trending:I = 0x7f0e003e

.field public static final menu_debug_enable_custom_pic_upload:I = 0x7f0e003f

.field public static final menu_debug_facebook_login_behavior:I = 0x7f0e0040

.field public static final menu_debug_goto_react_store:I = 0x7f0e0041

.field public static final menu_debug_goto_tdp:I = 0x7f0e0042

.field public static final menu_debug_party_broadcast_off:I = 0x7f0e0043

.field public static final menu_debug_party_broadcast_on:I = 0x7f0e0044

.field public static final menu_debug_save_test_conversation:I = 0x7f0e0045

.field public static final menu_debug_save_test_message:I = 0x7f0e0046

.field public static final menu_debug_show_react:I = 0x7f0e0047

.field public static final menu_debug_show_tutorial_on_startup:I = 0x7f0e0048

.field public static final menu_debug_switch_connect:I = 0x7f0e0049

.field public static final menu_debug_switch_error:I = 0x7f0e004a

.field public static final menu_debug_switch_loading:I = 0x7f0e004b

.field public static final menu_debug_switch_nocontent:I = 0x7f0e004c

.field public static final menu_debug_switch_valid:I = 0x7f0e004d

.field public static final menu_debug_test_experimentation_request:I = 0x7f0e004e

.field public static final menu_debug_text_input:I = 0x7f0e004f

.field public static final menu_dots:I = 0x7f0e022c

.field public static final menu_eds_dlc:I = 0x7f0e0050

.field public static final menu_feedback:I = 0x7f0e0051

.field public static final menu_frame:I = 0x7f0e0524

.field public static final menu_friends:I = 0x7f0e0052

.field public static final menu_inventory_consumables:I = 0x7f0e0053

.field public static final menu_inventory_durables:I = 0x7f0e0054

.field public static final menu_item_icon:I = 0x7f0e02d7

.field public static final menu_item_icon2:I = 0x7f0e02d9

.field public static final menu_item_text:I = 0x7f0e0475

.field public static final menu_message:I = 0x7f0e0055

.field public static final menu_partyLfg:I = 0x7f0e0056

.field public static final menu_refresh:I = 0x7f0e0057

.field public static final message_compose_container:I = 0x7f0e0463

.field public static final message_send:I = 0x7f0e0413

.field public static final message_send_frame:I = 0x7f0e040f

.field public static final message_share:I = 0x7f0e040e

.field public static final message_share_frame:I = 0x7f0e040c

.field public static final message_typing_indicator:I = 0x7f0e0464

.field public static final messagedetails_attachment_container:I = 0x7f0e04a1

.field public static final messages_activity_body:I = 0x7f0e0460

.field public static final messages_attch_achievement_icon:I = 0x7f0e0491

.field public static final messages_attch_achievement_item_text:I = 0x7f0e0492

.field public static final messages_attch_achievement_score:I = 0x7f0e0490

.field public static final messages_attch_action:I = 0x7f0e0488

.field public static final messages_attch_content:I = 0x7f0e0489

.field public static final messages_attch_gamertag:I = 0x7f0e0486

.field public static final messages_attch_item_text:I = 0x7f0e048c

.field public static final messages_attch_metadata:I = 0x7f0e0485

.field public static final messages_attch_play_image:I = 0x7f0e048f

.field public static final messages_attch_realname:I = 0x7f0e0487

.field public static final messages_attch_screenshot:I = 0x7f0e048e

.field public static final messages_attch_screenshot_layout:I = 0x7f0e048d

.field public static final messages_attch_target_generic:I = 0x7f0e048a

.field public static final messages_attch_target_user_image:I = 0x7f0e048b

.field public static final messages_compose:I = 0x7f0e04ac

.field public static final messages_icon_header:I = 0x7f0e04ab

.field public static final messages_layout:I = 0x7f0e04a7

.field public static final messages_list:I = 0x7f0e04aa

.field public static final messages_listItem_date:I = 0x7f0e0482

.field public static final messages_listItem_sender:I = 0x7f0e047f

.field public static final messages_listItem_sender_realname:I = 0x7f0e0480

.field public static final messages_listItem_tile:I = 0x7f0e047d

.field public static final messages_listItem_time:I = 0x7f0e0481

.field public static final messages_listItem_title:I = 0x7f0e0483

.field public static final messages_popup_menu_copy:I = 0x7f0e0c33

.field public static final messages_popup_menu_delete:I = 0x7f0e0c32

.field public static final messages_popup_menu_report:I = 0x7f0e0c34

.field public static final messages_rename_dialog_cancel_button:I = 0x7f0e09c2

.field public static final messages_rename_dialog_save_button:I = 0x7f0e09c1

.field public static final messages_switch_panel:I = 0x7f0e04a9

.field public static final messages_unread_icon:I = 0x7f0e047e

.field public static final messages_view_all_add_button:I = 0x7f0e04d2

.field public static final messages_view_all_bottom_layout:I = 0x7f0e04ce

.field public static final messages_view_all_cancel_button:I = 0x7f0e04d3

.field public static final messenger_send_button:I = 0x7f0e07a7

.field public static final metacritic_rating_logo:I = 0x7f0e07a9

.field public static final metacritic_rating_value:I = 0x7f0e07a8

.field public static final middle:I = 0x7f0e00b6

.field public static final middle_button:I = 0x7f0e0ac2

.field public static final mini:I = 0x7f0e00b0

.field public static final movie_castcrew_list:I = 0x7f0e07ae

.field public static final movie_castcrew_screen_body:I = 0x7f0e07ac

.field public static final movie_castcrew_switch_panel:I = 0x7f0e07ad

.field public static final movie_details_header_body:I = 0x7f0e07af

.field public static final movie_details_header_extra_info:I = 0x7f0e07b5

.field public static final movie_details_header_ratingbar:I = 0x7f0e07b2

.field public static final movie_details_header_release_data:I = 0x7f0e07b3

.field public static final movie_details_header_rt_rating:I = 0x7f0e07b4

.field public static final movie_details_header_tile:I = 0x7f0e07b0

.field public static final movie_details_header_title:I = 0x7f0e07b1

.field public static final movie_details_header_title_small:I = 0x7f0e07b6

.field public static final movieoverview_description_text:I = 0x7f0e07c2

.field public static final movieoverview_duration_text:I = 0x7f0e07c5

.field public static final movieoverview_extras_layout_list:I = 0x7f0e07be

.field public static final movieoverview_genre_text:I = 0x7f0e07c7

.field public static final movieoverview_layout:I = 0x7f0e07b7

.field public static final movieoverview_layout_body:I = 0x7f0e07b8

.field public static final movieoverview_pin_button:I = 0x7f0e07bd

.field public static final movieoverview_playxbox_button:I = 0x7f0e07ba

.field public static final movieoverview_playxbox_play_btn_icon:I = 0x7f0e07bb

.field public static final movieoverview_playxbox_play_btn_text:I = 0x7f0e07bc

.field public static final movieoverview_rating_text:I = 0x7f0e07c6

.field public static final movieoverview_ratingbar:I = 0x7f0e07bf

.field public static final movieoverview_release_data:I = 0x7f0e07c1

.field public static final movieoverview_releasedate_text:I = 0x7f0e07c8

.field public static final movieoverview_resolution_text:I = 0x7f0e07c9

.field public static final movieoverview_rt_rating:I = 0x7f0e07c0

.field public static final movieoverview_staring_text:I = 0x7f0e07c3

.field public static final movieoverview_studio_text:I = 0x7f0e07c4

.field public static final movieoverview_switch_panel:I = 0x7f0e07b9

.field public static final msa_sdk_webflow_webview_resolve_interrupt:I = 0x7f0e0058

.field public static final msa_sdk_webflow_webview_sign_in:I = 0x7f0e0059

.field public static final msa_sdk_webflow_webview_sign_up:I = 0x7f0e005a

.field public static final multilineBox:I = 0x7f0e0ab3

.field public static final multiply:I = 0x7f0e008d

.field public static final mute_conversation_frame:I = 0x7f0e047a

.field public static final my_clubs_list:I = 0x7f0e07dc

.field public static final name_club_container:I = 0x7f0e030c

.field public static final name_club_title:I = 0x7f0e0303

.field public static final name_tag_frame:I = 0x7f0e065f

.field public static final navigation_container:I = 0x7f0e0b66

.field public static final navigation_header_container:I = 0x7f0e04f6

.field public static final never:I = 0x7f0e00bc

.field public static final never_display:I = 0x7f0e00d7

.field public static final new_feature:I = 0x7f0e07d8

.field public static final nondismissable_view:I = 0x7f0e0b9d

.field public static final none:I = 0x7f0e0077

.field public static final normal:I = 0x7f0e007f

.field public static final not_interested_icon:I = 0x7f0e09e2

.field public static final not_listed:I = 0x7f0e043a

.field public static final notification_background:I = 0x7f0e07ed

.field public static final notification_body:I = 0x7f0e07dd

.field public static final notification_main_column:I = 0x7f0e07e8

.field public static final notification_main_column_container:I = 0x7f0e07e7

.field public static final notification_message:I = 0x7f0e07de

.field public static final now_play_panel_in_tray:I = 0x7f0e0802

.field public static final now_playing_line3:I = 0x7f0e0807

.field public static final now_playing_snap_fill_full_icon:I = 0x7f0e0803

.field public static final now_playing_subtitle:I = 0x7f0e0806

.field public static final now_playing_tile:I = 0x7f0e0804

.field public static final now_playing_title:I = 0x7f0e0805

.field public static final now_playing_tray_media_controls:I = 0x7f0e07fd

.field public static final nowplaying_companion_button:I = 0x7f0e080a

.field public static final nowplaying_details_button:I = 0x7f0e080b

.field public static final nowplaying_gameprofile_button:I = 0x7f0e080c

.field public static final nowplaying_help_button:I = 0x7f0e080d

.field public static final nowplaying_oneguide_button:I = 0x7f0e0800

.field public static final nowplaying_panel_buttons:I = 0x7f0e0808

.field public static final nowplaying_pull_down:I = 0x7f0e07fc

.field public static final nowplaying_record_that_button:I = 0x7f0e09a7

.field public static final nowplaying_stream_tv_button:I = 0x7f0e0801

.field public static final nowplaying_unsnap_button:I = 0x7f0e0809

.field public static final nowplayingtray_media_fastforward:I = 0x7f0e078f

.field public static final nowplayingtray_media_next:I = 0x7f0e0790

.field public static final nowplayingtray_media_pause:I = 0x7f0e078d

.field public static final nowplayingtray_media_play:I = 0x7f0e078e

.field public static final nowplayingtray_media_previous:I = 0x7f0e078b

.field public static final nowplayingtray_media_progress_bar:I = 0x7f0e07fe

.field public static final nowplayingtray_media_rewind:I = 0x7f0e078c

.field public static final off:I = 0x7f0e00a6

.field public static final off_view:I = 0x7f0e0a38

.field public static final on:I = 0x7f0e00a7

.field public static final onTouch:I = 0x7f0e00a8

.field public static final oneguide_icon:I = 0x7f0e07d7

.field public static final oobe_back_button:I = 0x7f0e0826

.field public static final oobe_close_button:I = 0x7f0e080e

.field public static final oobe_console_setup_complete_discription_line1:I = 0x7f0e0825

.field public static final oobe_console_setup_complete_header:I = 0x7f0e0824

.field public static final oobe_enter_code_details:I = 0x7f0e0817

.field public static final oobe_enter_code_error:I = 0x7f0e0819

.field public static final oobe_enter_code_header:I = 0x7f0e0816

.field public static final oobe_enter_code_textinput:I = 0x7f0e0818

.field public static final oobe_keep_console_up_to_date:I = 0x7f0e0822

.field public static final oobe_keep_game_up_to_date:I = 0x7f0e0823

.field public static final oobe_next_button:I = 0x7f0e0827

.field public static final oobe_power_energy_saving_radiobutton:I = 0x7f0e081f

.field public static final oobe_power_instant_on_radiobutton:I = 0x7f0e0820

.field public static final oobe_power_settings_header:I = 0x7f0e081d

.field public static final oobe_power_settings_radiogroup:I = 0x7f0e081e

.field public static final oobe_progress_1:I = 0x7f0e0810

.field public static final oobe_progress_2:I = 0x7f0e0811

.field public static final oobe_progress_3:I = 0x7f0e0812

.field public static final oobe_progress_4:I = 0x7f0e0813

.field public static final oobe_progress_5:I = 0x7f0e0814

.field public static final oobe_progress_container:I = 0x7f0e080f

.field public static final oobe_switchpanel:I = 0x7f0e0815

.field public static final oobe_time_zone_auto_adjust_checkbox:I = 0x7f0e081c

.field public static final oobe_time_zone_header:I = 0x7f0e081a

.field public static final oobe_time_zone_spinner:I = 0x7f0e081b

.field public static final oobe_update_settings_header:I = 0x7f0e0821

.field public static final open_graph:I = 0x7f0e00cf

.field public static final open_hover_chat_frame:I = 0x7f0e0476

.field public static final other_name_holder:I = 0x7f0e049b

.field public static final oval:I = 0x7f0e00ac

.field public static final overlay_layout:I = 0x7f0e058e

.field public static final overlay_loading_indicator:I = 0x7f0e023d

.field public static final packed:I = 0x7f0e0072

.field public static final page:I = 0x7f0e00d0

.field public static final page_title:I = 0x7f0e0b6e

.field public static final pageuser_info_body:I = 0x7f0e0829

.field public static final pageuser_info_description:I = 0x7f0e082e

.field public static final pageuser_info_follow_button:I = 0x7f0e082d

.field public static final pageuser_info_image:I = 0x7f0e082c

.field public static final pageuser_info_root:I = 0x7f0e0828

.field public static final pageuser_info_scrollview:I = 0x7f0e082b

.field public static final pageuser_info_switch_panel:I = 0x7f0e082a

.field public static final pageuser_pivot:I = 0x7f0e082f

.field public static final parallax:I = 0x7f0e00a1

.field public static final parent:I = 0x7f0e006f

.field public static final parentPanel:I = 0x7f0e00e6

.field public static final parent_rating_descriptor_container:I = 0x7f0e099e

.field public static final parent_rating_disclaimer_container:I = 0x7f0e099d

.field public static final parent_rating_level_tile:I = 0x7f0e099b

.field public static final parent_rating_text_layout:I = 0x7f0e099c

.field public static final parentitem__list:I = 0x7f0e0835

.field public static final parentitem_icon:I = 0x7f0e0830

.field public static final parentitem_name_text:I = 0x7f0e0831

.field public static final parentitem_screen_body:I = 0x7f0e0833

.field public static final parentitem_switch_panel:I = 0x7f0e0834

.field public static final parentitem_type_text:I = 0x7f0e0832

.field public static final party_and_lfg_beta_indicator:I = 0x7f0e0839

.field public static final party_and_lfg_screen_body:I = 0x7f0e0837

.field public static final party_and_lfg_title:I = 0x7f0e0838

.field public static final party_andlfg_screen_layout:I = 0x7f0e0836

.field public static final party_details_broadcast_alert_checkbox:I = 0x7f0e0851

.field public static final party_details_broadcast_alert_container:I = 0x7f0e084e

.field public static final party_details_broadcast_alert_description:I = 0x7f0e0850

.field public static final party_details_broadcast_alert_icon:I = 0x7f0e084f

.field public static final party_details_description:I = 0x7f0e0844

.field public static final party_details_description_expand_icon:I = 0x7f0e0841

.field public static final party_details_game_pic:I = 0x7f0e083f

.field public static final party_details_header:I = 0x7f0e083e

.field public static final party_details_invite_button:I = 0x7f0e0846

.field public static final party_details_joinable_option:I = 0x7f0e084a

.field public static final party_details_leave_button:I = 0x7f0e084d

.field public static final party_details_lfg_button:I = 0x7f0e0845

.field public static final party_details_member_list:I = 0x7f0e0852

.field public static final party_details_mute_option:I = 0x7f0e084b

.field public static final party_details_name:I = 0x7f0e0840

.field public static final party_details_options_button:I = 0x7f0e0847

.field public static final party_details_options_container:I = 0x7f0e0849

.field public static final party_details_options_expand_icon:I = 0x7f0e0848

.field public static final party_details_status:I = 0x7f0e0842

.field public static final party_details_tags:I = 0x7f0e0843

.field public static final party_details_text_chat_button:I = 0x7f0e084c

.field public static final party_lfg_create_button:I = 0x7f0e083c

.field public static final party_lfg_create_party_button:I = 0x7f0e083a

.field public static final party_lfg_view_party_button:I = 0x7f0e083b

.field public static final party_member_broadcasting_icon:I = 0x7f0e085f

.field public static final party_member_detail_container:I = 0x7f0e0859

.field public static final party_member_expand_icon:I = 0x7f0e085e

.field public static final party_member_gamertag:I = 0x7f0e085b

.field public static final party_member_icon_root:I = 0x7f0e0865

.field public static final party_member_image:I = 0x7f0e0867

.field public static final party_member_image_layout:I = 0x7f0e085a

.field public static final party_member_kick_button:I = 0x7f0e0864

.field public static final party_member_leader_icon:I = 0x7f0e085d

.field public static final party_member_mute_option:I = 0x7f0e0862

.field public static final party_member_options_container:I = 0x7f0e0861

.field public static final party_member_presence:I = 0x7f0e0860

.field public static final party_member_profile_button:I = 0x7f0e0863

.field public static final party_member_realname:I = 0x7f0e085c

.field public static final party_member_squawker_ring:I = 0x7f0e0866

.field public static final party_row_description:I = 0x7f0e0858

.field public static final party_row_gamertag_text:I = 0x7f0e0857

.field public static final party_row_here_now_text:I = 0x7f0e0856

.field public static final party_row_host_image:I = 0x7f0e0854

.field public static final party_row_party_text:I = 0x7f0e0855

.field public static final party_row_session_title:I = 0x7f0e0853

.field public static final party_text_character_count:I = 0x7f0e0871

.field public static final party_text_compose_container:I = 0x7f0e086e

.field public static final party_text_entry:I = 0x7f0e086f

.field public static final party_text_header:I = 0x7f0e086a

.field public static final party_text_header_container:I = 0x7f0e0869

.field public static final party_text_member_list:I = 0x7f0e086c

.field public static final party_text_message_list:I = 0x7f0e086d

.field public static final party_text_row_text:I = 0x7f0e0868

.field public static final party_text_send_message:I = 0x7f0e0870

.field public static final party_text_status:I = 0x7f0e086b

.field public static final passwordBox:I = 0x7f0e0ab2

.field public static final pegi_rating_descriptor_container:I = 0x7f0e099f

.field public static final pegi_rating_disclaimer_container:I = 0x7f0e09a0

.field public static final people_activity_feed_lfg_titleHeader:I = 0x7f0e087f

.field public static final people_activity_feed_list_row_root:I = 0x7f0e0885

.field public static final people_activity_feed_trending_content:I = 0x7f0e06d4

.field public static final people_activity_feed_trending_content_title:I = 0x7f0e08cc

.field public static final people_activity_feed_trending_ugc_caption:I = 0x7f0e08cb

.field public static final people_activityfeed_achievement_icon:I = 0x7f0e0893

.field public static final people_activityfeed_achievement_item_text:I = 0x7f0e0896

.field public static final people_activityfeed_achievement_item_weblink_text:I = 0x7f0e0897

.field public static final people_activityfeed_achievement_rare_icon:I = 0x7f0e0892

.field public static final people_activityfeed_achievement_score:I = 0x7f0e0894

.field public static final people_activityfeed_action:I = 0x7f0e08b9

.field public static final people_activityfeed_attach_root:I = 0x7f0e089a

.field public static final people_activityfeed_attach_screenshot_icon:I = 0x7f0e08a4

.field public static final people_activityfeed_attch_achievement_icon:I = 0x7f0e08a6

.field public static final people_activityfeed_attch_achievement_item_text:I = 0x7f0e08a9

.field public static final people_activityfeed_attch_achievement_item_weblink_text:I = 0x7f0e08aa

.field public static final people_activityfeed_attch_achievement_score:I = 0x7f0e08a7

.field public static final people_activityfeed_attch_action:I = 0x7f0e089e

.field public static final people_activityfeed_attch_content:I = 0x7f0e089f

.field public static final people_activityfeed_attch_gamertag:I = 0x7f0e089c

.field public static final people_activityfeed_attch_item_text:I = 0x7f0e08a1

.field public static final people_activityfeed_attch_legacy_achievement_item_image:I = 0x7f0e08a8

.field public static final people_activityfeed_attch_metadata:I = 0x7f0e089b

.field public static final people_activityfeed_attch_play_image:I = 0x7f0e08a5

.field public static final people_activityfeed_attch_realname:I = 0x7f0e089d

.field public static final people_activityfeed_attch_screenshot:I = 0x7f0e08a3

.field public static final people_activityfeed_attch_screenshot_layout:I = 0x7f0e08a2

.field public static final people_activityfeed_attch_target_user_image:I = 0x7f0e08a0

.field public static final people_activityfeed_comment_button:I = 0x7f0e0194

.field public static final people_activityfeed_content:I = 0x7f0e0887

.field public static final people_activityfeed_date:I = 0x7f0e08b7

.field public static final people_activityfeed_delete_button:I = 0x7f0e0199

.field public static final people_activityfeed_dismiss_button:I = 0x7f0e08b0

.field public static final people_activityfeed_gamertag:I = 0x7f0e08b5

.field public static final people_activityfeed_image:I = 0x7f0e08b1

.field public static final people_activityfeed_image_title:I = 0x7f0e08b2

.field public static final people_activityfeed_item_gameprofile_button:I = 0x7f0e0895

.field public static final people_activityfeed_item_gamerscore_Layout:I = 0x7f0e088c

.field public static final people_activityfeed_item_score:I = 0x7f0e088d

.field public static final people_activityfeed_item_text:I = 0x7f0e088b

.field public static final people_activityfeed_leaderboard_container:I = 0x7f0e0872

.field public static final people_activityfeed_leaderboard_gamerpics_container:I = 0x7f0e0873

.field public static final people_activityfeed_leaderboard_primary_gamer:I = 0x7f0e0874

.field public static final people_activityfeed_leaderboard_primary_gamerpic:I = 0x7f0e0875

.field public static final people_activityfeed_leaderboard_primary_gamerscore:I = 0x7f0e0877

.field public static final people_activityfeed_leaderboard_primary_gamertag:I = 0x7f0e0876

.field public static final people_activityfeed_leaderboard_secondary_gamer:I = 0x7f0e0878

.field public static final people_activityfeed_leaderboard_secondary_gamerpic:I = 0x7f0e0879

.field public static final people_activityfeed_leaderboard_secondary_gamerscore:I = 0x7f0e087b

.field public static final people_activityfeed_leaderboard_secondary_gamertag:I = 0x7f0e087a

.field public static final people_activityfeed_leaderboard_subtitle:I = 0x7f0e087d

.field public static final people_activityfeed_leaderboard_title:I = 0x7f0e087c

.field public static final people_activityfeed_legacy_achievement_item_image:I = 0x7f0e0898

.field public static final people_activityfeed_lfg_container:I = 0x7f0e087e

.field public static final people_activityfeed_lfg_description:I = 0x7f0e0884

.field public static final people_activityfeed_lfg_postedTime:I = 0x7f0e0882

.field public static final people_activityfeed_lfg_tagList:I = 0x7f0e0883

.field public static final people_activityfeed_lfg_titleImage:I = 0x7f0e0880

.field public static final people_activityfeed_lfg_titleText:I = 0x7f0e0881

.field public static final people_activityfeed_like_control:I = 0x7f0e0192

.field public static final people_activityfeed_list:I = 0x7f0e035d

.field public static final people_activityfeed_list_container:I = 0x7f0e035c

.field public static final people_activityfeed_list_empty_item:I = 0x7f0e06c3

.field public static final people_activityfeed_list_nodata:I = 0x7f0e06d5

.field public static final people_activityfeed_list_row_attachment:I = 0x7f0e08ba

.field public static final people_activityfeed_list_row_entry_text_content:I = 0x7f0e08ac

.field public static final people_activityfeed_list_row_entry_text_frame:I = 0x7f0e08ab

.field public static final people_activityfeed_list_row_filter_button:I = 0x7f0e08ae

.field public static final people_activityfeed_list_row_share_button:I = 0x7f0e08ad

.field public static final people_activityfeed_list_swipe_container:I = 0x7f0e035b

.field public static final people_activityfeed_metadata:I = 0x7f0e08b3

.field public static final people_activityfeed_metadata_container:I = 0x7f0e0886

.field public static final people_activityfeed_metadata_firstLine:I = 0x7f0e08b4

.field public static final people_activityfeed_more_action_button:I = 0x7f0e0198

.field public static final people_activityfeed_pinIcon:I = 0x7f0e08b8

.field public static final people_activityfeed_play_image:I = 0x7f0e0891

.field public static final people_activityfeed_realname:I = 0x7f0e08b6

.field public static final people_activityfeed_report_button:I = 0x7f0e019a

.field public static final people_activityfeed_screen_body:I = 0x7f0e035a

.field public static final people_activityfeed_screenshot:I = 0x7f0e088f

.field public static final people_activityfeed_screenshot_icon:I = 0x7f0e0890

.field public static final people_activityfeed_screenshot_layout:I = 0x7f0e088e

.field public static final people_activityfeed_share_button:I = 0x7f0e0196

.field public static final people_activityfeed_social_bar:I = 0x7f0e0899

.field public static final people_activityfeed_social_bar_actions:I = 0x7f0e0191

.field public static final people_activityfeed_social_bar_values:I = 0x7f0e019b

.field public static final people_activityfeed_social_bar_values_comments:I = 0x7f0e0195

.field public static final people_activityfeed_social_bar_values_likes:I = 0x7f0e0193

.field public static final people_activityfeed_social_bar_values_shares:I = 0x7f0e0197

.field public static final people_activityfeed_target_generic:I = 0x7f0e0889

.field public static final people_activityfeed_target_user_image:I = 0x7f0e088a

.field public static final people_activityfeed_trending_achievement_icon:I = 0x7f0e08c3

.field public static final people_activityfeed_trending_achievement_item_text:I = 0x7f0e08c6

.field public static final people_activityfeed_trending_achievement_item_weblink_text:I = 0x7f0e08c7

.field public static final people_activityfeed_trending_achievement_score:I = 0x7f0e08c4

.field public static final people_activityfeed_trending_content:I = 0x7f0e08bc

.field public static final people_activityfeed_trending_gamertag:I = 0x7f0e08c9

.field public static final people_activityfeed_trending_item_text:I = 0x7f0e08be

.field public static final people_activityfeed_trending_legacy_achievement_item_image:I = 0x7f0e08c5

.field public static final people_activityfeed_trending_metadata:I = 0x7f0e08c8

.field public static final people_activityfeed_trending_play_image:I = 0x7f0e08c2

.field public static final people_activityfeed_trending_realname:I = 0x7f0e08ca

.field public static final people_activityfeed_trending_root:I = 0x7f0e08bb

.field public static final people_activityfeed_trending_screenshot:I = 0x7f0e08c0

.field public static final people_activityfeed_trending_screenshot_icon:I = 0x7f0e08c1

.field public static final people_activityfeed_trending_screenshot_layout:I = 0x7f0e08bf

.field public static final people_activityfeed_trending_social_bar:I = 0x7f0e08cd

.field public static final people_activityfeed_trending_target_user_image:I = 0x7f0e08bd

.field public static final people_activityfeed_ugc_caption_text:I = 0x7f0e0888

.field public static final people_activityfeed_undo_button:I = 0x7f0e08af

.field public static final people_broadcasting_icon:I = 0x7f0e08ea

.field public static final people_favorites_icon:I = 0x7f0e0230

.field public static final people_filter_spinner:I = 0x7f0e08f6

.field public static final people_friends_count:I = 0x7f0e08f5

.field public static final people_gameractivity:I = 0x7f0e08ec

.field public static final people_gamertag:I = 0x7f0e06cb

.field public static final people_gamertag_search_button:I = 0x7f0e08f0

.field public static final people_hub_no_content_text:I = 0x7f0e08f9

.field public static final people_image:I = 0x7f0e03d4

.field public static final people_image_layout:I = 0x7f0e04ae

.field public static final people_item_presence_image:I = 0x7f0e03d5

.field public static final people_list:I = 0x7f0e08f8

.field public static final people_list_club_item:I = 0x7f0e08ce

.field public static final people_list_person_item_root:I = 0x7f0e08e9

.field public static final people_list_row:I = 0x7f0e08da

.field public static final people_list_state_text:I = 0x7f0e08e8

.field public static final people_party_icon:I = 0x7f0e08eb

.field public static final people_realname:I = 0x7f0e06cc

.field public static final people_recommendation_add_friend:I = 0x7f0e06d0

.field public static final people_recommendation_icon_image:I = 0x7f0e06cd

.field public static final people_recommendation_icon_text:I = 0x7f0e06ce

.field public static final people_recommendation_reason:I = 0x7f0e06cf

.field public static final people_screen_body:I = 0x7f0e08ee

.field public static final people_screen_controls_layout:I = 0x7f0e08f3

.field public static final people_screen_list_header:I = 0x7f0e08f4

.field public static final people_screen_pivot:I = 0x7f0e0932

.field public static final people_search_bar_layout:I = 0x7f0e08ef

.field public static final people_search_tag_input:I = 0x7f0e08f1

.field public static final people_searchbar_clear_button:I = 0x7f0e08f2

.field public static final people_section:I = 0x7f0e08e7

.field public static final people_switch_panel:I = 0x7f0e08f7

.field public static final peoplehub_achievement_data_container:I = 0x7f0e0903

.field public static final peoplehub_achievements_comparison_header:I = 0x7f0e08fc

.field public static final peoplehub_achievements_comparisonbar_me:I = 0x7f0e090a

.field public static final peoplehub_achievements_comparisonbar_profile:I = 0x7f0e090b

.field public static final peoplehub_achievements_filter_spinner:I = 0x7f0e08fd

.field public static final peoplehub_achievements_game_title:I = 0x7f0e0902

.field public static final peoplehub_achievements_gamerscore_status:I = 0x7f0e0905

.field public static final peoplehub_achievements_image:I = 0x7f0e0901

.field public static final peoplehub_achievements_image_layout:I = 0x7f0e0900

.field public static final peoplehub_achievements_list:I = 0x7f0e090e

.field public static final peoplehub_achievements_percent_complete_me:I = 0x7f0e0908

.field public static final peoplehub_achievements_percent_complete_profile:I = 0x7f0e0909

.field public static final peoplehub_achievements_screen_body:I = 0x7f0e090c

.field public static final peoplehub_achievements_status_icon:I = 0x7f0e0906

.field public static final peoplehub_achievements_status_text:I = 0x7f0e0907

.field public static final peoplehub_achievements_switch_panel:I = 0x7f0e090d

.field public static final peoplehub_achievements_total_gamerscore_icon:I = 0x7f0e0904

.field public static final peoplehub_info_bio_value_text:I = 0x7f0e092e

.field public static final peoplehub_info_block_button:I = 0x7f0e091f

.field public static final peoplehub_info_body:I = 0x7f0e0911

.field public static final peoplehub_info_broadcast_button_ring:I = 0x7f0e0923

.field public static final peoplehub_info_club_ban_button:I = 0x7f0e091b

.field public static final peoplehub_info_club_invite_button:I = 0x7f0e091a

.field public static final peoplehub_info_customize_button:I = 0x7f0e091d

.field public static final peoplehub_info_follow_button:I = 0x7f0e0919

.field public static final peoplehub_info_followers_label_text:I = 0x7f0e0928

.field public static final peoplehub_info_followers_text:I = 0x7f0e0929

.field public static final peoplehub_info_following_label_text:I = 0x7f0e092a

.field public static final peoplehub_info_following_text:I = 0x7f0e092b

.field public static final peoplehub_info_gamer_tag:I = 0x7f0e0930

.field public static final peoplehub_info_gamerscore:I = 0x7f0e0243

.field public static final peoplehub_info_gamerscore_icon:I = 0x7f0e0242

.field public static final peoplehub_info_image_layout:I = 0x7f0e0917

.field public static final peoplehub_info_image_tile:I = 0x7f0e0918

.field public static final peoplehub_info_joinable_header:I = 0x7f0e0921

.field public static final peoplehub_info_location_header_text:I = 0x7f0e092c

.field public static final peoplehub_info_location_value_text:I = 0x7f0e092d

.field public static final peoplehub_info_online_des_text:I = 0x7f0e0927

.field public static final peoplehub_info_online_header:I = 0x7f0e0924

.field public static final peoplehub_info_online_image:I = 0x7f0e0925

.field public static final peoplehub_info_online_title:I = 0x7f0e0926

.field public static final peoplehub_info_party_button_ring:I = 0x7f0e0922

.field public static final peoplehub_info_realname:I = 0x7f0e0916

.field public static final peoplehub_info_report_button:I = 0x7f0e0920

.field public static final peoplehub_info_reputation_banner:I = 0x7f0e0914

.field public static final peoplehub_info_reputation_text:I = 0x7f0e0915

.field public static final peoplehub_info_root:I = 0x7f0e0910

.field public static final peoplehub_info_send_message_button:I = 0x7f0e091c

.field public static final peoplehub_info_settings_button:I = 0x7f0e091e

.field public static final peoplehub_info_warning_banner:I = 0x7f0e0912

.field public static final peoplehub_info_warning_text:I = 0x7f0e0913

.field public static final peoplehub_info_watermark_image:I = 0x7f0e0931

.field public static final peoplehub_info_watermark_list:I = 0x7f0e092f

.field public static final peoplehub_pivot:I = 0x7f0e090f

.field public static final peoplehub_social_row_game_image:I = 0x7f0e0933

.field public static final peoplehub_social_row_game_text:I = 0x7f0e0934

.field public static final peoplehub_social_row_gamertag:I = 0x7f0e0936

.field public static final peoplehub_social_row_presence:I = 0x7f0e0938

.field public static final peoplehub_social_row_realname:I = 0x7f0e0937

.field public static final phone_contact_finder_dialog_close_button:I = 0x7f0e093f

.field public static final phone_contact_finder_dialog_error_message:I = 0x7f0e094e

.field public static final phone_contact_finder_dialog_error_panel:I = 0x7f0e094d

.field public static final phone_contact_finder_dialog_loading_indicator:I = 0x7f0e0952

.field public static final phone_contact_finder_dialog_message:I = 0x7f0e094c

.field public static final phone_contact_finder_dialog_message_panel:I = 0x7f0e094b

.field public static final phone_contact_finder_dialog_title:I = 0x7f0e094a

.field public static final phone_contact_finder_phone_number:I = 0x7f0e0945

.field public static final phone_contact_finder_phone_number_editor:I = 0x7f0e0940

.field public static final phone_contact_finder_region:I = 0x7f0e0944

.field public static final phone_contact_finder_region_name:I = 0x7f0e0941

.field public static final phone_contact_friend_finder_title_layout:I = 0x7f0e093e

.field public static final phone_contact_invite_friends_cancel_button:I = 0x7f0e095d

.field public static final phone_contact_invite_friends_confirm_button:I = 0x7f0e095c

.field public static final phone_contact_invite_friends_dialog_close_button:I = 0x7f0e0955

.field public static final phone_contact_invite_friends_dialog_error_panel:I = 0x7f0e0958

.field public static final phone_contact_invite_friends_dialog_message:I = 0x7f0e0957

.field public static final phone_contact_invite_friends_dialog_message_panel:I = 0x7f0e0956

.field public static final phone_contact_invite_friends_dialog_title:I = 0x7f0e0954

.field public static final phone_contact_invite_friends_error_message:I = 0x7f0e095a

.field public static final phone_contact_invite_friends_error_symbol:I = 0x7f0e0959

.field public static final phone_contact_invite_friends_list_panel:I = 0x7f0e095b

.field public static final phone_contact_invite_friends_loading_indicator:I = 0x7f0e095e

.field public static final phone_contact_invite_friends_title_layout:I = 0x7f0e0953

.field public static final phone_contact_list_panel:I = 0x7f0e094f

.field public static final phone_contact_name:I = 0x7f0e095f

.field public static final phone_contact_onxbox:I = 0x7f0e0960

.field public static final phone_contacts_invitation_switch_panel:I = 0x7f0e0950

.field public static final phone_friend_finder_dialog_cancel_button:I = 0x7f0e0951

.field public static final phone_friend_finder_dialog_confirm_button:I = 0x7f0e0946

.field public static final phone_not_interested_subtext:I = 0x7f0e0a12

.field public static final phone_not_interested_text:I = 0x7f0e0a11

.field public static final phone_verification_code:I = 0x7f0e0961

.field public static final pin:I = 0x7f0e00a2

.field public static final pin_icon:I = 0x7f0e0218

.field public static final pin_label:I = 0x7f0e0219

.field public static final pins_content_switch_panel:I = 0x7f0e0975

.field public static final pins_list:I = 0x7f0e0976

.field public static final pins_screen_body:I = 0x7f0e0972

.field public static final pins_screen_layout:I = 0x7f0e0971

.field public static final pins_tutorial_close:I = 0x7f0e0974

.field public static final pins_tutorial_layout:I = 0x7f0e0973

.field public static final pivot_header_left_arrow:I = 0x7f0e0b60

.field public static final pivot_header_right_arrow:I = 0x7f0e0b62

.field public static final pivot_header_title:I = 0x7f0e0b61

.field public static final pivot_page_indicator:I = 0x7f0e005b

.field public static final placeholder:I = 0x7f0e056f

.field public static final playing_recordthat_subtitle:I = 0x7f0e09a8

.field public static final playto_btn_label:I = 0x7f0e0667

.field public static final playto_btn_ring_icon:I = 0x7f0e0666

.field public static final playto_btn_subscription_image:I = 0x7f0e0977

.field public static final playto_btn_subscription_text:I = 0x7f0e0978

.field public static final popular_now_listItem_title:I = 0x7f0e0979

.field public static final popular_with_friends_header:I = 0x7f0e097f

.field public static final popular_with_friends_list:I = 0x7f0e0981

.field public static final popular_with_friends_screen_body:I = 0x7f0e097e

.field public static final popular_with_friends_switch_panel:I = 0x7f0e0980

.field public static final presence_image:I = 0x7f0e0231

.field public static final presence_text:I = 0x7f0e0241

.field public static final profile_gamer_real_name:I = 0x7f0e0649

.field public static final profile_gamer_tag:I = 0x7f0e0648

.field public static final profile_info_ring_btn_count:I = 0x7f0e070b

.field public static final profile_info_ring_btn_des_text:I = 0x7f0e070d

.field public static final profile_info_ring_btn_icon:I = 0x7f0e070a

.field public static final profile_info_ring_btn_label_text:I = 0x7f0e070c

.field public static final profile_info_ring_ring:I = 0x7f0e0709

.field public static final profile_showcase_image:I = 0x7f0e0983

.field public static final profile_showcase_image_layout:I = 0x7f0e0982

.field public static final profile_showcase_nodata:I = 0x7f0e0446

.field public static final profile_showcase_overlay:I = 0x7f0e0984

.field public static final profile_showcase_play_image:I = 0x7f0e0985

.field public static final profile_showcase_social_bar:I = 0x7f0e0986

.field public static final profile_showcase_times_viewed_count:I = 0x7f0e064c

.field public static final profile_showcase_video_gamename:I = 0x7f0e064b

.field public static final program_description:I = 0x7f0e0b3a

.field public static final program_info:I = 0x7f0e0b39

.field public static final program_subtitle:I = 0x7f0e0b38

.field public static final program_thumbnail:I = 0x7f0e0b36

.field public static final program_thumbnail_container:I = 0x7f0e0b33

.field public static final program_thumbnail_spinner:I = 0x7f0e0b35

.field public static final program_thumbnail_unavailable:I = 0x7f0e0b34

.field public static final program_title:I = 0x7f0e0b37

.field public static final progress:I = 0x7f0e07d1

.field public static final progressView:I = 0x7f0e0a5b

.field public static final progress_bar:I = 0x7f0e03e4

.field public static final progress_circular:I = 0x7f0e005c

.field public static final progress_dialog:I = 0x7f0e0988

.field public static final progress_horizontal:I = 0x7f0e005d

.field public static final promo_container:I = 0x7f0e07d9

.field public static final promo_text:I = 0x7f0e07da

.field public static final promptView:I = 0x7f0e0ab0

.field public static final provider_item_descriptoin:I = 0x7f0e098c

.field public static final provider_item_title:I = 0x7f0e098b

.field public static final provider_list_row_text_container:I = 0x7f0e098a

.field public static final provider_logo_image:I = 0x7f0e0989

.field public static final provider_picker_companion_icon:I = 0x7f0e01a3

.field public static final purchase_button_layout:I = 0x7f0e098d

.field public static final purchase_button_price:I = 0x7f0e098f

.field public static final purchase_button_ring_icon:I = 0x7f0e098e

.field public static final purchase_button_title:I = 0x7f0e0990

.field public static final purchase_result_dialog_explaination_text:I = 0x7f0e0992

.field public static final purchase_result_dialog_skipdownload_button:I = 0x7f0e0993

.field public static final purchase_result_dialog_title:I = 0x7f0e0991

.field public static final purchasewebview_activity_body:I = 0x7f0e0995

.field public static final purchasewebview_error_close_button:I = 0x7f0e0998

.field public static final purchasewebview_layout:I = 0x7f0e0994

.field public static final purchasewebview_loading_cancel_button:I = 0x7f0e099a

.field public static final purchasewebview_loading_spinner:I = 0x7f0e0999

.field public static final purchasewebview_webview:I = 0x7f0e0996

.field public static final purchasewebview_webview_coderedemption:I = 0x7f0e0997

.field public static final radio:I = 0x7f0e00f6

.field public static final radio_message:I = 0x7f0e0187

.field public static final react_root_view:I = 0x7f0e0ae5

.field public static final react_test_id:I = 0x7f0e005e

.field public static final realname_text:I = 0x7f0e0240

.field public static final recent_channel_call_sign:I = 0x7f0e0588

.field public static final recent_channel_current_episode_image:I = 0x7f0e0583

.field public static final recent_channel_current_episode_showtime:I = 0x7f0e0585

.field public static final recent_channel_image:I = 0x7f0e0587

.field public static final recent_channel_parent_series_title:I = 0x7f0e0584

.field public static final recent_channel_provider_name:I = 0x7f0e0586

.field public static final recent_channel_thumbnail_unavailable:I = 0x7f0e0582

.field public static final recent_channels_linear_layout:I = 0x7f0e0b3e

.field public static final recent_item_image:I = 0x7f0e09a1

.field public static final recent_item_name:I = 0x7f0e09a2

.field public static final recent_items_content_switch_panel:I = 0x7f0e09a5

.field public static final recent_items_list:I = 0x7f0e09a6

.field public static final recent_items_screen_body:I = 0x7f0e09a4

.field public static final recent_items_screen_layout:I = 0x7f0e09a3

.field public static final recipientHolderString:I = 0x7f0e041a

.field public static final recipientholder_button:I = 0x7f0e041b

.field public static final recipientsHolder:I = 0x7f0e0417

.field public static final recipientsHolderFrame:I = 0x7f0e05cb

.field public static final recommendations_section:I = 0x7f0e08dc

.field public static final recommendations_section_header:I = 0x7f0e08db

.field public static final recommendations_section_see_all_button:I = 0x7f0e08dd

.field public static final rectangle:I = 0x7f0e00ad

.field public static final redeem_code_button:I = 0x7f0e059f

.field public static final redeem_welcome_card:I = 0x7f0e0afa

.field public static final refresh_button:I = 0x7f0e0b31

.field public static final refresh_holder:I = 0x7f0e0b30

.field public static final region_list_view:I = 0x7f0e0949

.field public static final remote_app_input_container:I = 0x7f0e0c02

.field public static final remote_app_input_switch_panel:I = 0x7f0e0c03

.field public static final remote_b:I = 0x7f0e0c0e

.field public static final remote_b_bar:I = 0x7f0e0c0f

.field public static final remote_button:I = 0x7f0e0bdf

.field public static final remote_control:I = 0x7f0e09b4

.field public static final remote_control_root:I = 0x7f0e09b5

.field public static final remote_jack:I = 0x7f0e0c08

.field public static final remote_jill:I = 0x7f0e0c09

.field public static final remote_switch_panel:I = 0x7f0e09b6

.field public static final remote_x:I = 0x7f0e0c0c

.field public static final remote_x_bar:I = 0x7f0e0c0d

.field public static final remote_xenon:I = 0x7f0e0c07

.field public static final remote_y:I = 0x7f0e0c0a

.field public static final remote_y_bar:I = 0x7f0e0c0b

.field public static final remove_friend_btn_icon:I = 0x7f0e0244

.field public static final remove_friend_btn_layout:I = 0x7f0e023a

.field public static final remove_friend_btn_txt:I = 0x7f0e0245

.field public static final rename_clear:I = 0x7f0e09c0

.field public static final rename_club_dialog_close_button:I = 0x7f0e0312

.field public static final rename_club_title_frame:I = 0x7f0e0311

.field public static final rename_conversation_frame:I = 0x7f0e047b

.field public static final report_dialog_Category_spinner:I = 0x7f0e0565

.field public static final report_dialog_cancel_button:I = 0x7f0e0498

.field public static final report_dialog_close:I = 0x7f0e0562

.field public static final report_dialog_content_title:I = 0x7f0e0563

.field public static final report_dialog_detailed_reason:I = 0x7f0e0567

.field public static final report_dialog_report_club_text:I = 0x7f0e0564

.field public static final report_dialog_save_button:I = 0x7f0e0497

.field public static final report_dialog_send_button:I = 0x7f0e0568

.field public static final report_dialog_sendto_spinner:I = 0x7f0e0566

.field public static final report_editText:I = 0x7f0e0496

.field public static final report_spinner:I = 0x7f0e0495

.field public static final report_title:I = 0x7f0e0494

.field public static final report_title_layout:I = 0x7f0e02e4

.field public static final resend_code_image:I = 0x7f0e0963

.field public static final resume_streaming_btn:I = 0x7f0e018d

.field public static final resume_streaming_btn_container:I = 0x7f0e018c

.field public static final resume_streaming_btn_pip:I = 0x7f0e0189

.field public static final resume_streaming_label:I = 0x7f0e018e

.field public static final reward_item_layout_image:I = 0x7f0e09c3

.field public static final reward_item_layout_text:I = 0x7f0e09c4

.field public static final ribbon_background:I = 0x7f0e068a

.field public static final ribbon_view:I = 0x7f0e0689

.field public static final right:I = 0x7f0e009e

.field public static final right_button:I = 0x7f0e0ac3

.field public static final right_icon:I = 0x7f0e07ee

.field public static final right_pane_container:I = 0x7f0e0788

.field public static final right_pane_content:I = 0x7f0e0789

.field public static final right_side:I = 0x7f0e07e9

.field public static final rn_frame_file:I = 0x7f0e09aa

.field public static final rn_frame_method:I = 0x7f0e09a9

.field public static final rn_redbox_copy_button:I = 0x7f0e09b2

.field public static final rn_redbox_dismiss_button:I = 0x7f0e09b0

.field public static final rn_redbox_line_separator:I = 0x7f0e09ad

.field public static final rn_redbox_loading_indicator:I = 0x7f0e09ae

.field public static final rn_redbox_reload_button:I = 0x7f0e09b1

.field public static final rn_redbox_report_button:I = 0x7f0e09b3

.field public static final rn_redbox_report_label:I = 0x7f0e09af

.field public static final rn_redbox_stack:I = 0x7f0e09ac

.field public static final root_app_bar:I = 0x7f0e005f

.field public static final root_view:I = 0x7f0e0b00

.field public static final rotten_tomatoes_rating_icon:I = 0x7f0e09c5

.field public static final rotten_tomatoes_rating_textview:I = 0x7f0e09c6

.field public static final screen:I = 0x7f0e008e

.field public static final scroll:I = 0x7f0e008a

.field public static final scrollIndicatorDown:I = 0x7f0e00ec

.field public static final scrollIndicatorUp:I = 0x7f0e00e8

.field public static final scrollView:I = 0x7f0e00e9

.field public static final scrollable:I = 0x7f0e00c6

.field public static final search_badge:I = 0x7f0e0101

.field public static final search_bar:I = 0x7f0e0100

.field public static final search_bar_layout:I = 0x7f0e09c7

.field public static final search_button:I = 0x7f0e0102

.field public static final search_by_games_container:I = 0x7f0e03bf

.field public static final search_by_tag_container:I = 0x7f0e03bc

.field public static final search_close_btn:I = 0x7f0e0107

.field public static final search_data_activity_body:I = 0x7f0e09d0

.field public static final search_data_result_activity_layout:I = 0x7f0e09d9

.field public static final search_data_result_list:I = 0x7f0e09d5

.field public static final search_data_result_query_term:I = 0x7f0e09da

.field public static final search_data_result_switch_panel:I = 0x7f0e09db

.field public static final search_edit_frame:I = 0x7f0e0103

.field public static final search_flyout_close:I = 0x7f0e09d3

.field public static final search_flyout_layout:I = 0x7f0e09cf

.field public static final search_flyout_soft_dismiss_bottom:I = 0x7f0e09d8

.field public static final search_flyout_soft_dismiss_left:I = 0x7f0e09d2

.field public static final search_flyout_soft_dismiss_right:I = 0x7f0e09d7

.field public static final search_flyout_soft_dismiss_top:I = 0x7f0e09d1

.field public static final search_flyout_switch_panel:I = 0x7f0e09d4

.field public static final search_go_btn:I = 0x7f0e0109

.field public static final search_help:I = 0x7f0e09d6

.field public static final search_icon:I = 0x7f0e09c8

.field public static final search_mag_icon:I = 0x7f0e0104

.field public static final search_plate:I = 0x7f0e0105

.field public static final search_region_bottom_frame:I = 0x7f0e0948

.field public static final search_src_text:I = 0x7f0e0106

.field public static final search_tag_input:I = 0x7f0e09c9

.field public static final search_voice_btn:I = 0x7f0e010a

.field public static final searchbar_clear_button:I = 0x7f0e09ca

.field public static final seasons_episode_airdate:I = 0x7f0e0b88

.field public static final seasons_episode_description:I = 0x7f0e0b87

.field public static final seasons_episode_title:I = 0x7f0e0b86

.field public static final secondline_frame:I = 0x7f0e04de

.field public static final secondplace_container:I = 0x7f0e0ad0

.field public static final secondplace_profile_pic:I = 0x7f0e0ad1

.field public static final see_all_symbol:I = 0x7f0e08ed

.field public static final select_dialog_listview:I = 0x7f0e010b

.field public static final selected_tags_list:I = 0x7f0e0672

.field public static final selected_view:I = 0x7f0e0060

.field public static final separator:I = 0x7f0e0130

.field public static final settings_account_text:I = 0x7f0e09eb

.field public static final settings_edit_and_share_real_name_switchpanel:I = 0x7f0e0a21

.field public static final settings_facebook_not_interested_button_view:I = 0x7f0e09e1

.field public static final settings_facebook_repair_button_view:I = 0x7f0e09e5

.field public static final settings_facebook_repair_buuton:I = 0x7f0e09e7

.field public static final settings_facebook_repair_buuton_subtext:I = 0x7f0e09e8

.field public static final settings_facebook_secondary_button_view:I = 0x7f0e09e0

.field public static final settings_friends_sharing_realname:I = 0x7f0e0a2f

.field public static final settings_home_screen_spinner:I = 0x7f0e0a31

.field public static final settings_how_you_look:I = 0x7f0e0a26

.field public static final settings_language_description:I = 0x7f0e09f1

.field public static final settings_language_spinner:I = 0x7f0e09f0

.field public static final settings_language_title:I = 0x7f0e09ef

.field public static final settings_language_warning:I = 0x7f0e09f5

.field public static final settings_linked_account_title:I = 0x7f0e09ed

.field public static final settings_location_description:I = 0x7f0e09f4

.field public static final settings_location_spinner:I = 0x7f0e09f3

.field public static final settings_location_title:I = 0x7f0e09f2

.field public static final settings_name_sharing_settings_description:I = 0x7f0e09ee

.field public static final settings_notification_body:I = 0x7f0e09f6

.field public static final settings_phone_contact_view:I = 0x7f0e0a0c

.field public static final settings_phone_link_not_interested_view:I = 0x7f0e0a10

.field public static final settings_phone_link_unlink_view:I = 0x7f0e0a0d

.field public static final settings_pivot:I = 0x7f0e0a13

.field public static final settings_screen_about_help_button:I = 0x7f0e0a17

.field public static final settings_screen_about_privacy_button:I = 0x7f0e0a19

.field public static final settings_screen_about_text:I = 0x7f0e0a14

.field public static final settings_screen_about_version_value:I = 0x7f0e0a15

.field public static final settings_screen_account_sign_out:I = 0x7f0e09ec

.field public static final settings_screen_always_radiobutton:I = 0x7f0e0a1e

.field public static final settings_screen_body:I = 0x7f0e09e9

.field public static final settings_screen_developer_sandbox_id_value:I = 0x7f0e0a1c

.field public static final settings_screen_developer_section:I = 0x7f0e0a1b

.field public static final settings_screen_edit_and_share_realname_section:I = 0x7f0e0a24

.field public static final settings_screen_edit_realname_section:I = 0x7f0e0a25

.field public static final settings_screen_link_to_facebook_buuton:I = 0x7f0e09de

.field public static final settings_screen_link_to_facebook_subtext:I = 0x7f0e09df

.field public static final settings_screen_notifications_achievement_checkbox:I = 0x7f0e09ff

.field public static final settings_screen_notifications_club_demoted_checkbox:I = 0x7f0e0a03

.field public static final settings_screen_notifications_club_invite_request_checkbox:I = 0x7f0e0a04

.field public static final settings_screen_notifications_club_invited_checkbox:I = 0x7f0e0a07

.field public static final settings_screen_notifications_club_joined_checkbox:I = 0x7f0e0a00

.field public static final settings_screen_notifications_club_new_member_checkbox:I = 0x7f0e0a01

.field public static final settings_screen_notifications_club_promoted_checkbox:I = 0x7f0e0a02

.field public static final settings_screen_notifications_club_recommendation_checkbox:I = 0x7f0e0a05

.field public static final settings_screen_notifications_club_transfer_checkbox:I = 0x7f0e0a08

.field public static final settings_screen_notifications_favorite_broadcast_checkbox:I = 0x7f0e09fa

.field public static final settings_screen_notifications_favorite_online_checkbox:I = 0x7f0e09f9

.field public static final settings_screen_notifications_global_hover_chat_checkbox:I = 0x7f0e09f8

.field public static final settings_screen_notifications_lfg_checkbox:I = 0x7f0e09fe

.field public static final settings_screen_notifications_message_checkbox:I = 0x7f0e09fb

.field public static final settings_screen_notifications_message_hover_chat_checkbox:I = 0x7f0e09fc

.field public static final settings_screen_notifications_moderation_report_checkbox:I = 0x7f0e0a06

.field public static final settings_screen_notifications_party_invite_checkbox:I = 0x7f0e09fd

.field public static final settings_screen_notifications_section:I = 0x7f0e09f7

.field public static final settings_screen_notifications_tournaments_match:I = 0x7f0e0a0a

.field public static final settings_screen_notifications_tournaments_state:I = 0x7f0e0a09

.field public static final settings_screen_notifications_tournaments_team:I = 0x7f0e0a0b

.field public static final settings_screen_only_remote_radiobutton:I = 0x7f0e0a1f

.field public static final settings_screen_phone_subtext:I = 0x7f0e0a0f

.field public static final settings_screen_phone_text:I = 0x7f0e0a0e

.field public static final settings_screen_share_realname_section:I = 0x7f0e0a2a

.field public static final settings_screen_stay_awake_radiogroup:I = 0x7f0e0a1d

.field public static final settings_screen_stress_discovery_button:I = 0x7f0e0a1a

.field public static final settings_screen_third_party_notice_button:I = 0x7f0e0a18

.field public static final settings_screen_use_device_settings_radiobutton:I = 0x7f0e0a20

.field public static final settings_screen_whats_new_details_button:I = 0x7f0e0a16

.field public static final settings_share_real_name:I = 0x7f0e0236

.field public static final settings_share_transitively_checkbox:I = 0x7f0e0a30

.field public static final settings_share_with_everyone:I = 0x7f0e0a2c

.field public static final settings_share_with_friends:I = 0x7f0e0a2d

.field public static final settings_share_with_friends_selective:I = 0x7f0e0a2e

.field public static final settings_switch_panel:I = 0x7f0e09ea

.field public static final setup_welcome_card:I = 0x7f0e0af9

.field public static final sg_urc_view_control_instance:I = 0x7f0e0a3b

.field public static final share_achievement_button:I = 0x7f0e08fa

.field public static final share_decision_dialog_activityfeed_button:I = 0x7f0e0a43

.field public static final share_decision_dialog_activityfeed_icon:I = 0x7f0e0a44

.field public static final share_decision_dialog_button_layout:I = 0x7f0e0a42

.field public static final share_decision_dialog_close:I = 0x7f0e0a40

.field public static final share_decision_dialog_clubs_button:I = 0x7f0e0a45

.field public static final share_decision_dialog_clubs_icon:I = 0x7f0e0a46

.field public static final share_decision_dialog_content_title:I = 0x7f0e0a41

.field public static final share_decision_dialog_messages_button:I = 0x7f0e0a47

.field public static final share_decision_dialog_messages_icon:I = 0x7f0e0a48

.field public static final share_decision_dialog_softclose:I = 0x7f0e0a3f

.field public static final share_decision_dialog_specific_club_button:I = 0x7f0e0a49

.field public static final share_decision_dialog_specific_club_name:I = 0x7f0e0a4b

.field public static final share_decision_dialog_specific_club_pic:I = 0x7f0e0a4a

.field public static final share_name_txt:I = 0x7f0e0a2b

.field public static final share_real_name_checkbox:I = 0x7f0e0239

.field public static final share_real_name_preview_gamertag:I = 0x7f0e0a28

.field public static final share_real_name_preview_realname:I = 0x7f0e055b

.field public static final share_toggle_button:I = 0x7f0e0a22

.field public static final share_toogle_text:I = 0x7f0e0a23

.field public static final shop_welcome_card:I = 0x7f0e0afb

.field public static final shortcut:I = 0x7f0e00f5

.field public static final showCustom:I = 0x7f0e0083

.field public static final showHome:I = 0x7f0e0084

.field public static final showTitle:I = 0x7f0e0085

.field public static final showcase_item_title:I = 0x7f0e064a

.field public static final shows:I = 0x7f0e0571

.field public static final showtime_text:I = 0x7f0e057a

.field public static final signOutCheckBox:I = 0x7f0e0a4c

.field public static final signout_btn_icon:I = 0x7f0e0a32

.field public static final signout_button_label:I = 0x7f0e0a33

.field public static final small:I = 0x7f0e00d9

.field public static final smartglass_icon:I = 0x7f0e07d0

.field public static final smartglass_play_icon:I = 0x7f0e0a4d

.field public static final smartglass_play_text:I = 0x7f0e0a4e

.field public static final smartglassicon:I = 0x7f0e07a2

.field public static final snackbar_action:I = 0x7f0e04f5

.field public static final snackbar_text:I = 0x7f0e04f4

.field public static final snap:I = 0x7f0e008b

.field public static final socialbar_layout:I = 0x7f0e0987

.field public static final soft_dismiss_area:I = 0x7f0e0b9a

.field public static final song_detail_duration:I = 0x7f0e0a50

.field public static final song_detail_title:I = 0x7f0e0a4f

.field public static final songs_list_layout:I = 0x7f0e01b1

.field public static final spacer:I = 0x7f0e00e5

.field public static final split_action_bar:I = 0x7f0e0061

.field public static final spread:I = 0x7f0e0070

.field public static final spread_inside:I = 0x7f0e0073

.field public static final src_atop:I = 0x7f0e008f

.field public static final src_in:I = 0x7f0e0090

.field public static final src_over:I = 0x7f0e0091

.field public static final standard:I = 0x7f0e00bf

.field public static final star_rating_dialog_close:I = 0x7f0e0a51

.field public static final star_rating_dialog_content_title:I = 0x7f0e0a53

.field public static final star_rating_dialog_five_star_layout:I = 0x7f0e0a58

.field public static final star_rating_dialog_four_star_layout:I = 0x7f0e0a57

.field public static final star_rating_dialog_one_star_layout:I = 0x7f0e0a54

.field public static final star_rating_dialog_softclose:I = 0x7f0e0a52

.field public static final star_rating_dialog_three_star_layout:I = 0x7f0e0a56

.field public static final star_rating_dialog_two_star_layout:I = 0x7f0e0a55

.field public static final start:I = 0x7f0e009f

.field public static final start_network_test_btn:I = 0x7f0e063c

.field public static final static_page_body_first:I = 0x7f0e0a5d

.field public static final static_page_body_second:I = 0x7f0e0a5e

.field public static final static_page_buttons:I = 0x7f0e0a5f

.field public static final static_page_header:I = 0x7f0e0a5c

.field public static final status_bar_latest_event_content:I = 0x7f0e07e4

.field public static final stop_comparing:I = 0x7f0e05fc

.field public static final store_addons_filter_spinner:I = 0x7f0e0a62

.field public static final store_addons_list:I = 0x7f0e0a63

.field public static final store_addons_screen_body:I = 0x7f0e0a60

.field public static final store_addons_switch_panel:I = 0x7f0e0a61

.field public static final store_apps_filter_spinner:I = 0x7f0e0a66

.field public static final store_apps_list:I = 0x7f0e0a67

.field public static final store_apps_screen_body:I = 0x7f0e0a64

.field public static final store_apps_switch_panel:I = 0x7f0e0a65

.field public static final store_gamepass_filter_spinner:I = 0x7f0e0a6a

.field public static final store_gamepass_list:I = 0x7f0e0a6b

.field public static final store_gamepass_screen_body:I = 0x7f0e0a68

.field public static final store_gamepass_switch_panel:I = 0x7f0e0a69

.field public static final store_games_filter_spinner:I = 0x7f0e0a6e

.field public static final store_games_list:I = 0x7f0e0a6f

.field public static final store_games_screen_body:I = 0x7f0e0a6c

.field public static final store_games_switch_panel:I = 0x7f0e0a6d

.field public static final store_gold_list:I = 0x7f0e0a72

.field public static final store_gold_screen_body:I = 0x7f0e0a70

.field public static final store_gold_switch_panel:I = 0x7f0e0a71

.field public static final store_item_full_price:I = 0x7f0e0a78

.field public static final store_item_gamepass_row:I = 0x7f0e0a77

.field public static final store_item_gold_row:I = 0x7f0e0a7b

.field public static final store_item_gold_weekend:I = 0x7f0e0a7d

.field public static final store_item_ic_gamepass:I = 0x7f0e0a7a

.field public static final store_item_ic_gold:I = 0x7f0e0a7e

.field public static final store_item_image:I = 0x7f0e0a73

.field public static final store_item_rating_with_count:I = 0x7f0e0a76

.field public static final store_item_reduced_price:I = 0x7f0e0a79

.field public static final store_item_release:I = 0x7f0e0a75

.field public static final store_item_title:I = 0x7f0e0a74

.field public static final store_list_subheader:I = 0x7f0e0a7c

.field public static final store_pivot:I = 0x7f0e0a80

.field public static final store_pivot_title_text:I = 0x7f0e0a7f

.field public static final store_redeem_icon:I = 0x7f0e0a81

.field public static final store_search_button:I = 0x7f0e059e

.field public static final stream_button:I = 0x7f0e058a

.field public static final stream_quality_group:I = 0x7f0e0c35

.field public static final stream_quality_high:I = 0x7f0e0c36

.field public static final stream_quality_low:I = 0x7f0e0c38

.field public static final stream_quality_medium:I = 0x7f0e0c37

.field public static final streamer_controls:I = 0x7f0e0b8d

.field public static final streaming_frame:I = 0x7f0e0787

.field public static final submenuarrow:I = 0x7f0e00f7

.field public static final submit_area:I = 0x7f0e0108

.field public static final submit_button:I = 0x7f0e023c

.field public static final subtitle_text:I = 0x7f0e0579

.field public static final suggested_friends_welcome_card:I = 0x7f0e0afd

.field public static final suggestions_filter_spinner:I = 0x7f0e0a83

.field public static final suggestions_filter_spinner_layout:I = 0x7f0e0a82

.field public static final suggestions_firstline_frame:I = 0x7f0e0a8b

.field public static final suggestions_invalid_state_text:I = 0x7f0e0a84

.field public static final suggestions_people_add_button:I = 0x7f0e0a8a

.field public static final suggestions_people_favorites_icon:I = 0x7f0e0a88

.field public static final suggestions_people_gameractivity:I = 0x7f0e0a90

.field public static final suggestions_people_gamertag:I = 0x7f0e0a8c

.field public static final suggestions_people_image:I = 0x7f0e0a87

.field public static final suggestions_people_image_layout:I = 0x7f0e0a86

.field public static final suggestions_people_item_presence_image:I = 0x7f0e0a89

.field public static final suggestions_people_list:I = 0x7f0e0a92

.field public static final suggestions_people_realname:I = 0x7f0e0a8d

.field public static final suggestions_people_recommendation_icon_image:I = 0x7f0e0a8e

.field public static final suggestions_people_recommendation_icon_text:I = 0x7f0e0a8f

.field public static final suggestions_people_screen_body:I = 0x7f0e0a91

.field public static final suggestions_person_item_section:I = 0x7f0e0a85

.field public static final surface_view:I = 0x7f0e007c

.field public static final switch_pane_item_text:I = 0x7f0e0a94

.field public static final switch_pane_with_refresh:I = 0x7f0e0a93

.field public static final switch_panel_screen_error:I = 0x7f0e0a97

.field public static final switch_panel_screen_invalid:I = 0x7f0e0a9a

.field public static final switch_panel_screen_loading:I = 0x7f0e0a99

.field public static final switch_panel_screen_no_content:I = 0x7f0e0a98

.field public static final switch_panel_screen_panel:I = 0x7f0e0a95

.field public static final switch_panel_screen_valid:I = 0x7f0e0a96

.field public static final system_tag_list:I = 0x7f0e0aad

.field public static final tabMode:I = 0x7f0e0080

.field public static final tab_icon:I = 0x7f0e0a9b

.field public static final tab_title:I = 0x7f0e0a9c

.field public static final tag_cancel:I = 0x7f0e0aa3

.field public static final tag_container:I = 0x7f0e0aa0

.field public static final tag_error_label:I = 0x7f0e0aae

.field public static final tag_gamerscore:I = 0x7f0e0aa1

.field public static final tag_header:I = 0x7f0e0a9d

.field public static final tag_header_text:I = 0x7f0e0a9e

.field public static final tag_list_switch_panel:I = 0x7f0e0aac

.field public static final tag_no_content_label:I = 0x7f0e0aaf

.field public static final tag_picker_dialog_close:I = 0x7f0e0aa5

.field public static final tag_picker_header:I = 0x7f0e0aa4

.field public static final tag_picker_pivot:I = 0x7f0e0aa9

.field public static final tag_picker_pivot_layout:I = 0x7f0e0aa6

.field public static final tag_row:I = 0x7f0e0a9f

.field public static final tag_search_bar:I = 0x7f0e0aa7

.field public static final tag_search_data_result_list:I = 0x7f0e0aaa

.field public static final tag_selection_done_button:I = 0x7f0e0aab

.field public static final tag_text:I = 0x7f0e0aa2

.field public static final tags_flow_layout:I = 0x7f0e0338

.field public static final text:I = 0x7f0e07f4

.field public static final text2:I = 0x7f0e07f2

.field public static final textAddAccount:I = 0x7f0e019c

.field public static final textBox:I = 0x7f0e0ab1

.field public static final textCapSentences:I = 0x7f0e00ae

.field public static final textCapWords:I = 0x7f0e00af

.field public static final textEmail:I = 0x7f0e0112

.field public static final textFirstLast:I = 0x7f0e0113

.field public static final textSpacerNoButtons:I = 0x7f0e00eb

.field public static final textSpacerNoTitle:I = 0x7f0e00ea

.field public static final text_ch:I = 0x7f0e0062

.field public static final text_exit:I = 0x7f0e0063

.field public static final text_info:I = 0x7f0e0064

.field public static final text_input_password_toggle:I = 0x7f0e04fb

.field public static final text_label:I = 0x7f0e0a34

.field public static final text_page:I = 0x7f0e0065

.field public static final text_select:I = 0x7f0e0066

.field public static final text_vol:I = 0x7f0e0067

.field public static final textinput_clear_button:I = 0x7f0e0abd

.field public static final textinput_close_button:I = 0x7f0e0ab7

.field public static final textinput_confirm_button:I = 0x7f0e0abb

.field public static final textinput_control:I = 0x7f0e0ab6

.field public static final textinput_editText:I = 0x7f0e0abc

.field public static final textinput_edittext_frame:I = 0x7f0e0aba

.field public static final textinput_prompt:I = 0x7f0e0ab9

.field public static final textinput_prompt_close_button:I = 0x7f0e0ab8

.field public static final texture_view:I = 0x7f0e007d

.field public static final third_party_notice_button_close:I = 0x7f0e0ac0

.field public static final third_party_notices:I = 0x7f0e0abf

.field public static final third_party_notices_header:I = 0x7f0e0abe

.field public static final thirdplace_container:I = 0x7f0e0ad4

.field public static final thirdplace_profile_pic:I = 0x7f0e0ad5

.field public static final time:I = 0x7f0e07ea

.field public static final time_separator:I = 0x7f0e0577

.field public static final title:I = 0x7f0e00e3

.field public static final titleDividerNoCustom:I = 0x7f0e00f2

.field public static final title_bar:I = 0x7f0e0ac4

.field public static final title_bar_indeterminateProgressBar:I = 0x7f0e01ea

.field public static final title_container:I = 0x7f0e07ca

.field public static final title_picker_banner:I = 0x7f0e0adc

.field public static final title_picker_body:I = 0x7f0e0ada

.field public static final title_picker_close:I = 0x7f0e0add

.field public static final title_picker_done:I = 0x7f0e0ae0

.field public static final title_picker_header:I = 0x7f0e0adb

.field public static final title_picker_recent_titles_list:I = 0x7f0e0ae3

.field public static final title_picker_result_list:I = 0x7f0e0ae1

.field public static final title_picker_row_checked:I = 0x7f0e0ad6

.field public static final title_picker_row_image:I = 0x7f0e0ad7

.field public static final title_picker_row_text:I = 0x7f0e0ad8

.field public static final title_picker_search_bar:I = 0x7f0e0ade

.field public static final title_picker_suggestion_list:I = 0x7f0e0adf

.field public static final title_picker_switch_panel:I = 0x7f0e0ae2

.field public static final title_template:I = 0x7f0e00f0

.field public static final title_text:I = 0x7f0e0576

.field public static final toast_details_related_list_error:I = 0x7f0e0506

.field public static final top:I = 0x7f0e00a0

.field public static final topPanel:I = 0x7f0e00ef

.field public static final top_bar_light_details:I = 0x7f0e0b2f

.field public static final top_level_urc:I = 0x7f0e0b97

.field public static final touch_outside:I = 0x7f0e04f2

.field public static final tournament_details_screen_header:I = 0x7f0e0ae4

.field public static final trending_beam_channel_image:I = 0x7f0e0ae6

.field public static final trending_beam_channel_subtitle:I = 0x7f0e0ae8

.field public static final trending_beam_channel_title:I = 0x7f0e0ae7

.field public static final trending_beam_channel_viewer_count:I = 0x7f0e0ae9

.field public static final trending_beam_list:I = 0x7f0e0aea

.field public static final trending_filter_spinner:I = 0x7f0e0af6

.field public static final trending_pivot:I = 0x7f0e0aeb

.field public static final trending_screen_carousel:I = 0x7f0e0af4

.field public static final trending_screen_empty_list_container:I = 0x7f0e0af3

.field public static final trending_screen_viewpager:I = 0x7f0e0af5

.field public static final trending_topic_caption:I = 0x7f0e0af1

.field public static final trending_topic_displayImage:I = 0x7f0e0aee

.field public static final trending_topic_gradient:I = 0x7f0e0aef

.field public static final trending_topic_name:I = 0x7f0e0af0

.field public static final trending_topic_screen_body:I = 0x7f0e0af2

.field public static final trending_topic_screen_description:I = 0x7f0e0aed

.field public static final trending_topic_screen_title:I = 0x7f0e0aec

.field public static final trophy_icon:I = 0x7f0e07ce

.field public static final tune_button:I = 0x7f0e0589

.field public static final tutorial_screen_description:I = 0x7f0e0af8

.field public static final tutorial_screen_header:I = 0x7f0e0af7

.field public static final tv_common_boxart_image:I = 0x7f0e0b03

.field public static final tv_episode_details_header_body:I = 0x7f0e0b56

.field public static final tv_episode_details_header_extra_info:I = 0x7f0e0b5b

.field public static final tv_episode_details_header_release_data:I = 0x7f0e0b58

.field public static final tv_episode_details_header_series_title:I = 0x7f0e0b5a

.field public static final tv_episode_details_header_tile:I = 0x7f0e0b57

.field public static final tv_episode_details_header_title:I = 0x7f0e0b59

.field public static final tv_episode_details_header_title_small:I = 0x7f0e0b5c

.field public static final tv_listings_channels:I = 0x7f0e0b20

.field public static final tv_listings_error_provider:I = 0x7f0e0b22

.field public static final tv_listings_error_provider_name:I = 0x7f0e0b23

.field public static final tv_listings_header:I = 0x7f0e0b04

.field public static final tv_listings_provider_chevron:I = 0x7f0e0b09

.field public static final tv_listings_provider_logo:I = 0x7f0e0b07

.field public static final tv_listings_provider_name:I = 0x7f0e0b08

.field public static final tv_listings_screen_body:I = 0x7f0e0b1e

.field public static final tv_listings_switch_panel:I = 0x7f0e0b1f

.field public static final tv_menu_favorites:I = 0x7f0e0b6a

.field public static final tv_menu_favorites_underline:I = 0x7f0e0b6b

.field public static final tv_menu_recentchannels:I = 0x7f0e0b6c

.field public static final tv_menu_recentchannels_underline:I = 0x7f0e0b6d

.field public static final tv_menu_tvlistings:I = 0x7f0e0b68

.field public static final tv_menu_tvlistings_underline:I = 0x7f0e0b69

.field public static final tv_mychannels_channels:I = 0x7f0e0b29

.field public static final tv_mychannels_provider_bar:I = 0x7f0e0b05

.field public static final tv_mychannels_screen_body:I = 0x7f0e0b27

.field public static final tv_mychannels_switch_panel:I = 0x7f0e0b28

.field public static final tv_myshows_movies_list:I = 0x7f0e0b2e

.field public static final tv_myshows_screen_body:I = 0x7f0e0b2b

.field public static final tv_myshows_switch_panel:I = 0x7f0e0b2c

.field public static final tv_myshows_tv_list:I = 0x7f0e0b2d

.field public static final tv_page_container:I = 0x7f0e0b70

.field public static final tv_recent_channels_channels:I = 0x7f0e058b

.field public static final tv_recent_channels_screen_body:I = 0x7f0e0b3c

.field public static final tv_recent_channels_switch_panel:I = 0x7f0e0b3d

.field public static final tv_recent_channels_view:I = 0x7f0e0b3f

.field public static final tv_streamer:I = 0x7f0e0183

.field public static final tv_streamer_control_bar:I = 0x7f0e0b48

.field public static final tv_streamer_current_time:I = 0x7f0e0b4e

.field public static final tv_streamer_current_timeline:I = 0x7f0e0b4a

.field public static final tv_streamer_end_time:I = 0x7f0e0b4d

.field public static final tv_streamer_error:I = 0x7f0e018b

.field public static final tv_streamer_error_container:I = 0x7f0e018a

.field public static final tv_streamer_radio_indicator:I = 0x7f0e0186

.field public static final tv_streamer_resume_btn:I = 0x7f0e0b49

.field public static final tv_streamer_seekbar:I = 0x7f0e0b4b

.field public static final tv_streamer_start_time:I = 0x7f0e0b4c

.field public static final tv_streaming_view:I = 0x7f0e0185

.field public static final tv_trending_desc:I = 0x7f0e0b51

.field public static final tv_trending_image:I = 0x7f0e0b4f

.field public static final tv_trending_livetop10_list:I = 0x7f0e0b54

.field public static final tv_trending_popular_vod_list:I = 0x7f0e0b55

.field public static final tv_trending_screen_body:I = 0x7f0e0b52

.field public static final tv_trending_switch_panel:I = 0x7f0e0b53

.field public static final tv_trending_title:I = 0x7f0e0b50

.field public static final tvepisode_overview_all_seasons_text:I = 0x7f0e0b13

.field public static final tvepisode_overview_description_text:I = 0x7f0e0b11

.field public static final tvepisode_overview_duration_text:I = 0x7f0e0b17

.field public static final tvepisode_overview_genre_text:I = 0x7f0e0b19

.field public static final tvepisode_overview_languages_text:I = 0x7f0e0b1b

.field public static final tvepisode_overview_layout:I = 0x7f0e0b0a

.field public static final tvepisode_overview_layout_body:I = 0x7f0e0b0b

.field public static final tvepisode_overview_network_text:I = 0x7f0e0b16

.field public static final tvepisode_overview_pin_button:I = 0x7f0e0b10

.field public static final tvepisode_overview_playxbox_button:I = 0x7f0e0b0d

.field public static final tvepisode_overview_playxbox_play_btn_icon:I = 0x7f0e0b0e

.field public static final tvepisode_overview_playxbox_play_btn_text:I = 0x7f0e0b0f

.field public static final tvepisode_overview_rating_text:I = 0x7f0e0b18

.field public static final tvepisode_overview_release_data:I = 0x7f0e0b12

.field public static final tvepisode_overview_releasedate_text:I = 0x7f0e0b15

.field public static final tvepisode_overview_resolution_text:I = 0x7f0e0b1a

.field public static final tvepisode_overview_staring_text:I = 0x7f0e0b14

.field public static final tvepisode_overview_switch_panel:I = 0x7f0e0b0c

.field public static final tvepisode_season_list:I = 0x7f0e0b5f

.field public static final tvepisode_season_screen_body:I = 0x7f0e0b5d

.field public static final tvepisode_season_switch_panel:I = 0x7f0e0b5e

.field public static final tvhub_pivot:I = 0x7f0e0b63

.field public static final tvhub_table_time:I = 0x7f0e0b6f

.field public static final tvhub_tablet_profile_pic:I = 0x7f0e0b67

.field public static final tvhub_tablet_screen_body:I = 0x7f0e0b65

.field public static final tvhub_tablet_screen_instance:I = 0x7f0e0b64

.field public static final tvseries_details_header_body:I = 0x7f0e0b40

.field public static final tvseries_details_header_extra_info:I = 0x7f0e0b46

.field public static final tvseries_details_header_mc_rating:I = 0x7f0e0b43

.field public static final tvseries_details_header_ratingbar:I = 0x7f0e0b42

.field public static final tvseries_details_header_release_data:I = 0x7f0e0b44

.field public static final tvseries_details_header_tile:I = 0x7f0e0b41

.field public static final tvseries_details_header_title:I = 0x7f0e0b45

.field public static final tvseries_details_header_title_small:I = 0x7f0e0b47

.field public static final tvseries_overview_description_text:I = 0x7f0e0b7e

.field public static final tvseries_overview_duration_text:I = 0x7f0e0b81

.field public static final tvseries_overview_extras_layout_list:I = 0x7f0e0b7a

.field public static final tvseries_overview_genre_text:I = 0x7f0e0b83

.field public static final tvseries_overview_languages_text:I = 0x7f0e0b85

.field public static final tvseries_overview_mc_rating:I = 0x7f0e0b7c

.field public static final tvseries_overview_network_text:I = 0x7f0e0b80

.field public static final tvseries_overview_pin_button:I = 0x7f0e0b79

.field public static final tvseries_overview_play_btn_icon:I = 0x7f0e0b74

.field public static final tvseries_overview_play_btn_text:I = 0x7f0e0b75

.field public static final tvseries_overview_playnow_button:I = 0x7f0e0b76

.field public static final tvseries_overview_playnow_play_btn_icon:I = 0x7f0e0b77

.field public static final tvseries_overview_playnow_play_btn_text:I = 0x7f0e0b78

.field public static final tvseries_overview_playxbox_button:I = 0x7f0e0b73

.field public static final tvseries_overview_rating_text:I = 0x7f0e0b82

.field public static final tvseries_overview_ratingbar:I = 0x7f0e0b7b

.field public static final tvseries_overview_release_data:I = 0x7f0e0b7d

.field public static final tvseries_overview_resolution_text:I = 0x7f0e0b84

.field public static final tvseries_overview_screen_body:I = 0x7f0e0b71

.field public static final tvseries_overview_staring_text:I = 0x7f0e0b7f

.field public static final tvseries_overview_switch_panel:I = 0x7f0e0b72

.field public static final tvseries_seasons_filter_spinner:I = 0x7f0e0b8b

.field public static final tvseries_seasons_list:I = 0x7f0e0b8c

.field public static final tvseries_seasons_screen_body:I = 0x7f0e0b89

.field public static final tvseries_seasons_switch_panel:I = 0x7f0e0b8a

.field public static final tvstreamer_control_epg:I = 0x7f0e0b8f

.field public static final tvstreamer_control_live:I = 0x7f0e0b95

.field public static final tvstreamer_control_next:I = 0x7f0e0b93

.field public static final tvstreamer_control_pause:I = 0x7f0e0b91

.field public static final tvstreamer_control_play:I = 0x7f0e0b92

.field public static final tvstreamer_control_previous:I = 0x7f0e0b90

.field public static final tvstreamer_control_quality:I = 0x7f0e0b96

.field public static final tvstreamer_control_remote:I = 0x7f0e0b94

.field public static final tvstreamer_media_progress_bar:I = 0x7f0e0b8e

.field public static final txt_off:I = 0x7f0e0a3a

.field public static final txt_on:I = 0x7f0e0a36

.field public static final txt_on_off:I = 0x7f0e0a37

.field public static final txt_search_region:I = 0x7f0e0947

.field public static final universal_button:I = 0x7f0e09bc

.field public static final universal_remote_control:I = 0x7f0e09b8

.field public static final unknown:I = 0x7f0e00d1

.field public static final unshared_activity_feed_achievement_score:I = 0x7f0e0ba1

.field public static final unshared_activity_feed_background_image:I = 0x7f0e0b9e

.field public static final unshared_activity_feed_icon:I = 0x7f0e0ba0

.field public static final unshared_activity_feed_legacy_achievement_item_image:I = 0x7f0e0b9f

.field public static final unshared_activity_feed_primary_text:I = 0x7f0e0ba2

.field public static final unshared_activity_feed_secondary_text:I = 0x7f0e0ba3

.field public static final unshared_activity_feed_view_count:I = 0x7f0e0ba5

.field public static final unshared_activity_feed_view_count_container:I = 0x7f0e0ba4

.field public static final up:I = 0x7f0e0068

.field public static final upload_pic_close_button:I = 0x7f0e0baf

.field public static final upload_pic_crop_view:I = 0x7f0e0baa

.field public static final upload_pic_edit_subtitle:I = 0x7f0e0ba7

.field public static final upload_pic_edit_title:I = 0x7f0e0ba6

.field public static final upload_pic_error_details:I = 0x7f0e0bab

.field public static final upload_pic_success_button:I = 0x7f0e0bae

.field public static final upload_pic_success_subtitle:I = 0x7f0e0bad

.field public static final upload_pic_success_title:I = 0x7f0e0bac

.field public static final upload_pic_upload_button:I = 0x7f0e0ba8

.field public static final upload_pic_upload_div:I = 0x7f0e0ba9

.field public static final upload_pic_upload_overlay:I = 0x7f0e0bb0

.field public static final urc_control_scrollView_instance:I = 0x7f0e0bb1

.field public static final urc_extraviews_root:I = 0x7f0e0b99

.field public static final urc_main_container:I = 0x7f0e0a3c

.field public static final urc_more_view:I = 0x7f0e0b9b

.field public static final urc_power_view:I = 0x7f0e0b9c

.field public static final urc_status:I = 0x7f0e0a3d

.field public static final urc_sub_status:I = 0x7f0e0a3e

.field public static final urc_view_control_instance:I = 0x7f0e0b98

.field public static final useLogo:I = 0x7f0e0086

.field public static final userTileOverflowMenu:I = 0x7f0e0114

.field public static final user_action:I = 0x7f0e0069

.field public static final user_rate_count:I = 0x7f0e0a5a

.field public static final utility_bar_alert_no_text:I = 0x7f0e0bbe

.field public static final utility_bar_alerts_icon:I = 0x7f0e0bbd

.field public static final utility_bar_friends_icon:I = 0x7f0e0bbf

.field public static final utility_bar_message_no_text:I = 0x7f0e0bc1

.field public static final utility_bar_messages_icon:I = 0x7f0e0bc0

.field public static final utility_bar_party_icon:I = 0x7f0e0bc2

.field public static final utility_bar_refresh_icon:I = 0x7f0e0bc3

.field public static final vertical_scrolldock_header_large_view:I = 0x7f0e01a6

.field public static final vertical_scrolldock_header_small_view:I = 0x7f0e01ac

.field public static final videoSurfaceViewGroup:I = 0x7f0e0184

.field public static final video_surface:I = 0x7f0e0bc5

.field public static final videoplayer_activity_body:I = 0x7f0e0bc4

.field public static final videoplayer_layout:I = 0x7f0e0591

.field public static final view_all_member_layout:I = 0x7f0e04ca

.field public static final view_all_member_switch_panel:I = 0x7f0e04cd

.field public static final view_all_members_frame:I = 0x7f0e0478

.field public static final view_mentions_frame:I = 0x7f0e02d6

.field public static final view_offset_helper:I = 0x7f0e006a

.field public static final view_profile_frame:I = 0x7f0e0474

.field public static final view_tag_native_id:I = 0x7f0e006b

.field public static final virtualgrid_row_right_frame_bottom:I = 0x7f0e0bc7

.field public static final virtualgrid_row_right_frame_top:I = 0x7f0e0bc6

.field public static final volume_button_container:I = 0x7f0e0bcb

.field public static final volume_dialog:I = 0x7f0e0bc8

.field public static final volume_dialog_close:I = 0x7f0e0bca

.field public static final volume_dialog_soft_dismiss:I = 0x7f0e0bc9

.field public static final volume_down:I = 0x7f0e0bcc

.field public static final volume_launcher:I = 0x7f0e0c04

.field public static final volume_muteToggle:I = 0x7f0e0bcd

.field public static final volume_up:I = 0x7f0e0bce

.field public static final webFlowButtons:I = 0x7f0e0bcf

.field public static final webview:I = 0x7f0e07db

.field public static final webview_dialog_close_button:I = 0x7f0e0bd2

.field public static final webview_dialog_webview:I = 0x7f0e0bd3

.field public static final welcome_card_button:I = 0x7f0e0bd8

.field public static final welcome_card_completion_indicator:I = 0x7f0e0bd4

.field public static final welcome_card_description:I = 0x7f0e0bd7

.field public static final welcome_card_icon:I = 0x7f0e0bd5

.field public static final welcome_card_title:I = 0x7f0e0bd6

.field public static final welcome_login:I = 0x7f0e0783

.field public static final welcomearea:I = 0x7f0e0780

.field public static final whats_new_button_close:I = 0x7f0e0bdc

.field public static final whats_new_details_feature_list:I = 0x7f0e0bdb

.field public static final whats_new_details_header:I = 0x7f0e0bd9

.field public static final whats_new_details_subheader:I = 0x7f0e0bda

.field public static final whatsnew_listItem_bullet:I = 0x7f0e0bdd

.field public static final whatsnew_listItem_text:I = 0x7f0e0bde

.field public static final white:I = 0x7f0e00c4

.field public static final wide:I = 0x7f0e00c0

.field public static final width:I = 0x7f0e006e

.field public static final withText:I = 0x7f0e00bd

.field public static final wrap:I = 0x7f0e0071

.field public static final wrap_content:I = 0x7f0e0092

.field public static final xbid_aleady_have_gamer_tag_answer:I = 0x7f0e0bfc

.field public static final xbid_body_fragment:I = 0x7f0e0be1

.field public static final xbid_bottom_bar_shadow:I = 0x7f0e0bf0

.field public static final xbid_claim_it:I = 0x7f0e0bf1

.field public static final xbid_claim_it_bar:I = 0x7f0e0bfd

.field public static final xbid_clear_text:I = 0x7f0e0bf6

.field public static final xbid_close:I = 0x7f0e0bea

.field public static final xbid_different_gamer_tag_answer:I = 0x7f0e0bff

.field public static final xbid_display_name:I = 0x7f0e0bef

.field public static final xbid_done:I = 0x7f0e0bec

.field public static final xbid_enter_gamertag:I = 0x7f0e0bf5

.field public static final xbid_enter_gamertag_comment:I = 0x7f0e0bf8

.field public static final xbid_enter_gamertag_container:I = 0x7f0e0bf4

.field public static final xbid_error_buttons:I = 0x7f0e0be2

.field public static final xbid_error_left_button:I = 0x7f0e0be4

.field public static final xbid_error_message:I = 0x7f0e0be6

.field public static final xbid_error_right_button:I = 0x7f0e0be5

.field public static final xbid_gamerpic:I = 0x7f0e0bed

.field public static final xbid_gamerscore:I = 0x7f0e0bfe

.field public static final xbid_gamertag:I = 0x7f0e0bee

.field public static final xbid_greeting_text:I = 0x7f0e0be3

.field public static final xbid_header_fragment:I = 0x7f0e0be0

.field public static final xbid_privacy:I = 0x7f0e0bfa

.field public static final xbid_privacy_details:I = 0x7f0e0bfb

.field public static final xbid_scroll_container:I = 0x7f0e0beb

.field public static final xbid_search:I = 0x7f0e0bf7

.field public static final xbid_suggestion_text:I = 0x7f0e0c00

.field public static final xbid_suggestions_list:I = 0x7f0e0bf9

.field public static final xbid_title:I = 0x7f0e0bf2

.field public static final xbid_title_comment:I = 0x7f0e0bf3

.field public static final xbid_user_email:I = 0x7f0e0be9

.field public static final xbid_user_image:I = 0x7f0e0be7

.field public static final xbid_user_name:I = 0x7f0e0be8

.field public static final xbox_button:I = 0x7f0e09bb

.field public static final xbox_remote_control:I = 0x7f0e09b7

.field public static final xbox_remote_control_buttons:I = 0x7f0e0c01

.field public static final xboxpurchase_button_label:I = 0x7f0e0968

.field public static final xboxpurchase_button_price:I = 0x7f0e0969

.field public static final xboxpurchase_button_ring_icon:I = 0x7f0e0967

.field public static final xboxpurchase_button_strikethrough_text:I = 0x7f0e096a

.field public static final xboxpurchase_button_text:I = 0x7f0e096b

.field public static final xboxpurchase_buy_btn_subscription_image:I = 0x7f0e096c

.field public static final xboxpurchase_buy_btn_subscription_text:I = 0x7f0e096d

.field public static final xbt_header_text_view:I = 0x7f0e0c10

.field public static final xbt_header_view:I = 0x7f0e0c13

.field public static final xbt_sub_header_text_view:I = 0x7f0e0c11

.field public static final xbt_tab_layout:I = 0x7f0e0c12

.field public static final xbt_two_line_title_view_title:I = 0x7f0e006c

.field public static final xbt_view_pager:I = 0x7f0e0c14


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
