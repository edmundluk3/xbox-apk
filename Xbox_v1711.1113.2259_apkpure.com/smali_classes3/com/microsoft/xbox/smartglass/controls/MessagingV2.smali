.class Lcom/microsoft/xbox/smartglass/controls/MessagingV2;
.super Lcom/microsoft/xbox/smartglass/controls/Messaging;
.source "MessagingV2.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 0
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Messaging;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected sendMessage(ILjava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 19
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;-><init>(Ljava/lang/String;)V

    .line 20
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;
    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;->titleId:I

    if-lez v3, :cond_0

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;->titleId:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 21
    .local v2, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_0
    iget-object v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;->message:Ljava/lang/String;

    invoke-virtual {p0, p1, v3, v2}, Lcom/microsoft/xbox/smartglass/controls/MessagingV2;->sendMessage(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 25
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_1
    return-void

    .line 20
    .restart local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MessagingV2;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 22
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSendMessage;
    :catch_0
    move-exception v0

    .line 23
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MessagingV2;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid JSON. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_1
.end method
