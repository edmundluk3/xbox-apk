.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubBackgroundAchievementDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubNameViewHolder"
.end annotation


# instance fields
.field private final image:Landroid/widget/ImageView;

.field private final name:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Landroid/view/ViewGroup;)V
    .locals 3
    .param p2, "itemView"    # Landroid/view/ViewGroup;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    .line 109
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030066

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->image:Landroid/widget/ImageView;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->name:Landroid/widget/TextView;

    .line 112
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onAchievementSetAsClubImage(J)V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 3
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fa

    .line 115
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, v2, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void
.end method
