.class public Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "CanvasWebViewActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected computeBottomMargin()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string v0, "HTMLCompanion"

    return-object v0
.end method

.method public isManagingOwnAppBar()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public leaveScreen(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->leaveViewModel(Ljava/lang/Runnable;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 41
    const v0, 0x7f03004b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->setShowUtilityBar(Z)V

    .line 43
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->setShowRightPane(Z)V

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 46
    return-void
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->ensureDisplayLocale()V

    .line 66
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 67
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onResume()V

    .line 55
    return-void
.end method

.method public onTombstone()V
    .locals 2

    .prologue
    .line 78
    const-string v0, "CanvasWebViewActivity"

    const-string v1, "onTombstone called, but we are not removing views for canvas"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public resetBottomMargin()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method
