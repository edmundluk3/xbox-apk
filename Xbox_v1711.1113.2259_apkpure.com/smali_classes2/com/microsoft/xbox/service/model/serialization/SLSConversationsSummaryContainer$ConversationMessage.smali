.class public Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
.super Ljava/lang/Object;
.source "SLSConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConversationMessage"
.end annotation


# instance fields
.field public activityfeedItemLocator:Ljava/lang/String;

.field private attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

.field public hasAudio:Z

.field public hasPhoto:Z

.field public hasText:Z

.field public isRead:Z

.field public lastUpdateTime:Ljava/util/Date;

.field public messageFolder:Ljava/lang/String;

.field public messageId:J

.field public messageText:Ljava/lang/String;

.field public messageType:Ljava/lang/String;

.field public sender:Ljava/lang/String;

.field public senderGamerTag:Ljava/lang/String;

.field public senderTitleId:J

.field public senderXuid:Ljava/lang/String;

.field public sentTime:Ljava/util/Date;

.field public skypeMessageType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttachment()Lcom/microsoft/xbox/service/model/entity/Entity;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    return-object v0
.end method

.method public hasAttachment()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasXbox360Attachment()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasPhoto:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAudio:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasPhoto:Z

    if-nez v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAttachment()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasText:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAudio:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    .line 130
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    .line 130
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;)V
    .locals 0
    .param p1, "attachment"    # Lcom/microsoft/xbox/service/model/entity/Entity;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->attachment:Lcom/microsoft/xbox/service/model/entity/Entity;

    .line 114
    return-void
.end method
