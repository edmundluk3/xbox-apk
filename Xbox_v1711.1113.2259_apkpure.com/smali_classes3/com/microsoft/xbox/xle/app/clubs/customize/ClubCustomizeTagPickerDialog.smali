.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubCustomizeTagPickerDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;
.implements Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEManagedDialog;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;"
    }
.end annotation


# static fields
.field private static final MAX_TAGS:I = 0xa


# instance fields
.field private final progressBar:Landroid/widget/ProgressBar;

.field private final recyclerView:Landroid/support/v7/widget/RecyclerView;

.field private final selectedCountTextView:Landroid/widget/TextView;

.field private selectedTags:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

.field private final tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "tagsSelectedListener"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "selectedTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x1

    .line 47
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 37
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    .line 48
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->setContentView(I)V

    .line 51
    if-nez p2, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    .line 53
    const v0, 0x7f0e034f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v4, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 55
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0045

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(IILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 62
    const v0, 0x7f0e034d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->progressBar:Landroid/widget/ProgressBar;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsAsync(Z)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 67
    const v0, 0x7f0e034e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, p3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const v0, 0x7f0e034a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v0, 0x7f0e034c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedCountTextView:Landroid/widget/TextView;

    .line 71
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->updateCountText()V

    .line 72
    return-void

    .line 51
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "tagsSelectedListener"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;->onTagsSelected(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->dismiss()V

    return-void
.end method

.method private updateCountText()V
    .locals 6

    .prologue
    .line 130
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0701d1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "selectedText":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedCountTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 127
    return-void
.end method

.method public isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z
    .locals 2
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 6
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v5, 0xa

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 115
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->updateCountText()V

    .line 116
    return-void

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ge v1, v5, :cond_1

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0701d3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "errorText":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->progressBar:Landroid/widget/ProgressBar;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 79
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v5, v6, :cond_3

    .line 80
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getSystemTagGroups()Ljava/util/List;

    move-result-object v4

    .line 81
    .local v4, "tagGroups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 84
    .local v0, "filteredSelectedTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    .line 85
    .local v3, "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->groupName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    .line 87
    .local v2, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 92
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    .end local v2    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v3    # "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 98
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->selectedTags:Ljava/util/Set;

    .line 99
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;->updateCountText()V

    .line 101
    .end local v0    # "filteredSelectedTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v4    # "tagGroups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;>;"
    :cond_3
    return-void
.end method
