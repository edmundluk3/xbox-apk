.class Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "StorePivotScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserSubscriptionsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$1;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 120
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    .locals 6

    .prologue
    .line 148
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->getProductId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 149
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->getProductId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 148
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 150
    .local v2, "subscriptionProductIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 153
    .local v1, "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/microsoft/xbox/service/store/IStoreService;->getStoreCollectionList(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 158
    :goto_0
    return-object v1

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Exception occurred while loading subscriptions from collection"

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->loadDataInBackground()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->onError()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->onLoadUserSubscriptionsCompleted(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;)V

    .line 128
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->onLoadUserSubscriptionsCompleted(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;)V

    .line 139
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->onPostExecute(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 132
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->updateAdapter()V

    .line 134
    return-void
.end method
