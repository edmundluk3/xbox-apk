.class public Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer;
.super Ljava/lang/Object;
.source "ProfileRecentsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$Timeline;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardEntry;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardDateRange;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LeaderboardData;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$LfgAchievementTag;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;,
        Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
