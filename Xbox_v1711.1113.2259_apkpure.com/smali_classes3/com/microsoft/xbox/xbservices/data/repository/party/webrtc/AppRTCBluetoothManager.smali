.class public Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
.super Ljava/lang/Object;
.source "AppRTCBluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    }
.end annotation


# static fields
.field private static final BLUETOOTH_SCO_TIMEOUT_MS:I = 0xfa0

.field private static final MAX_SCO_CONNECTION_ATTEMPTS:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AppRTCBluetoothManager"


# instance fields
.field private final apprtcAudioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

.field private final apprtcContext:Landroid/content/Context;

.field private final audioManager:Landroid/media/AudioManager;

.field private bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private final bluetoothHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private final bluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field private final bluetoothTimeoutRunnable:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field scoConnectionAttempts:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "audioManager"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .prologue
    const/4 v2, 0x0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothTimeoutRunnable:Ljava/lang/Runnable;

    .line 198
    const-string v0, "AppRTCBluetoothManager"

    const-string v1, "ctor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    .line 203
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcAudioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .line 204
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->audioManager:Landroid/media/AudioManager;

    .line 205
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 206
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 207
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadsetReceiver:Landroid/content/BroadcastReceiver;

    .line 208
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->handler:Landroid/os/Handler;

    .line 209
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothTimeout()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-object p1
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothHeadset;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->updateAudioDeviceState()V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->cancelTimer()V

    return-void
.end method

.method private bluetoothTimeout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 486
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v2, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 489
    :cond_1
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bluetoothTimeout: BT state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", attempts: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", SCO is on: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 491
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->isScoOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v2, v3, :cond_0

    .line 496
    const/4 v1, 0x0

    .line 497
    .local v1, "scoConnected":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 498
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 499
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 500
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 501
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCO connected with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const/4 v1, 0x1

    .line 507
    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    .line 509
    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 510
    iput v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    .line 516
    :goto_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->updateAudioDeviceState()V

    .line 517
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bluetoothTimeout done: BT state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 504
    :cond_3
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCO is not connected with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 513
    :cond_4
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "BT failed to connect after timeout"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stopScoAudio()V

    goto :goto_2
.end method

.method private cancelTimer()V
    .locals 2

    .prologue
    .line 474
    const-string v0, "AppRTCBluetoothManager"

    const-string v1, "cancelTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 476
    return-void
.end method

.method static create(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "audioManager"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .prologue
    .line 194
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V

    return-object v0
.end method

.method private isScoOn()Z
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    return v0
.end method

.method private startTimer()V
    .locals 4

    .prologue
    .line 465
    const-string v0, "AppRTCBluetoothManager"

    const-string v1, "startTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 467
    return-void
.end method

.method private stateToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 527
    packed-switch p1, :pswitch_data_0

    .line 549
    :pswitch_0
    const-string v0, "INVALID"

    :goto_0
    return-object v0

    .line 529
    :pswitch_1
    const-string v0, "DISCONNECTED"

    goto :goto_0

    .line 531
    :pswitch_2
    const-string v0, "CONNECTED"

    goto :goto_0

    .line 533
    :pswitch_3
    const-string v0, "CONNECTING"

    goto :goto_0

    .line 535
    :pswitch_4
    const-string v0, "DISCONNECTING"

    goto :goto_0

    .line 537
    :pswitch_5
    const-string v0, "OFF"

    goto :goto_0

    .line 539
    :pswitch_6
    const-string v0, "ON"

    goto :goto_0

    .line 543
    :pswitch_7
    const-string v0, "TURNING_OFF"

    goto :goto_0

    .line 547
    :pswitch_8
    const-string v0, "TURNING_ON"

    goto :goto_0

    .line 527
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private updateAudioDeviceState()V
    .locals 2

    .prologue
    .line 456
    const-string v0, "AppRTCBluetoothManager"

    const-string v1, "updateAudioDeviceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcAudioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->updateAudioDeviceState()V

    .line 458
    return-void
.end method


# virtual methods
.method protected getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 410
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    return-object v0
.end method

.method protected getBluetoothProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .param p3, "profile"    # I

    .prologue
    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 399
    const/4 v0, 0x0

    .line 400
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 401
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 403
    :cond_0
    return-object v0
.end method

.method public getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-object v0
.end method

.method protected hasPermission(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "permission"    # Ljava/lang/String;

    .prologue
    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    .line 429
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 430
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    .line 427
    invoke-virtual {v0, p2, v1, v2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected logBluetoothAdapterInfo(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 6
    .param p1, "localAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 436
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BluetoothAdapter: enabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 437
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 438
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stateToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 439
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", address="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 440
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 436
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 443
    .local v1, "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 444
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "paired devices:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 446
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v3, "AppRTCBluetoothManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", address="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 449
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    return-void
.end method

.method protected registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    .locals 1
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;
    .param p2, "filter"    # Landroid/content/IntentFilter;

    .prologue
    .line 414
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 415
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 236
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {p0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->hasPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "Process (pid=) lacks BLUETOOTH permission"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v1, v2, :cond_1

    .line 244
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "Invalid BT state"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 247
    :cond_1
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 248
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 249
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    .line 251
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v1, :cond_2

    .line 253
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "Device does not support Bluetooth"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 257
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v1

    if-nez v1, :cond_3

    .line 258
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "Bluetooth SCO audio is not available off call"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 261
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->logBluetoothAdapterInfo(Landroid/bluetooth/BluetoothAdapter;)V

    .line 264
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {p0, v1, v2, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getBluetoothProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 266
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "BluetoothAdapter.getProfileProxy(HEADSET) failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 270
    :cond_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 272
    .local v0, "bluetoothHeadsetFilter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 274
    const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 276
    const-string v1, "AppRTCBluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HEADSET profile state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 277
    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 276
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "Bluetooth proxy for headset profile has started"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 280
    const-string v1, "AppRTCBluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start done: BT state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public startScoAudio()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 325
    const-string v1, "AppRTCBluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startSco: BT state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", attempts: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SCO is on: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 327
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->isScoOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 325
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 329
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "BT SCO connection fails - no more attempts"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :goto_0
    return v0

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v1, v2, :cond_1

    .line 333
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "BT SCO connection fails - no headset available"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 337
    :cond_1
    const-string v0, "AppRTCBluetoothManager"

    const-string v1, "Starting Bluetooth SCO and waits for ACTION_AUDIO_STATE_CHANGED..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 342
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 343
    iget v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    .line 344
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->startTimer()V

    .line 345
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startScoAudio done: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 289
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    .line 292
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stopScoAudio()V

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v0, v1, :cond_1

    .line 295
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->cancelTimer()V

    .line 296
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 298
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 300
    :cond_0
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 301
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 302
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 305
    :cond_1
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop done: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    return-void
.end method

.method public stopScoAudio()V
    .locals 3

    .prologue
    .line 354
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopScoAudio: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SCO is on: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 355
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->isScoOn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 354
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v0, v1, :cond_0

    .line 363
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->cancelTimer()V

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 361
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_DISCONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 362
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopScoAudio done: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 1
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->apprtcContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 419
    return-void
.end method

.method public updateDevice()V
    .locals 5

    .prologue
    .line 373
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "updateDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 381
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 382
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 383
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 384
    const-string v1, "AppRTCBluetoothManager"

    const-string v2, "No connected bluetooth headset"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :goto_1
    const-string v1, "AppRTCBluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateDevice done: BT state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 387
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 388
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 389
    const-string v1, "AppRTCBluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connected bluetooth headset: name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 390
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 391
    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SCO audio="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 392
    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 389
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
