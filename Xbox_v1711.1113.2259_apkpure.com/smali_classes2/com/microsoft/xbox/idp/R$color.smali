.class public final Lcom/microsoft/xbox/idp/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c0168

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c0169

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c016a

.field public static final abc_color_highlight_material:I = 0x7f0c016c

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0013

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c016f

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c0170

.field public static final abc_primary_text_material_dark:I = 0x7f0c0171

.field public static final abc_primary_text_material_light:I = 0x7f0c0172

.field public static final abc_search_url_text:I = 0x7f0c0173

.field public static final abc_search_url_text_normal:I = 0x7f0c0014

.field public static final abc_search_url_text_pressed:I = 0x7f0c0015

.field public static final abc_search_url_text_selected:I = 0x7f0c0016

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0174

.field public static final abc_secondary_text_material_light:I = 0x7f0c0175

.field public static final abc_tint_btn_checkable:I = 0x7f0c0176

.field public static final abc_tint_default:I = 0x7f0c0177

.field public static final abc_tint_edittext:I = 0x7f0c0178

.field public static final abc_tint_seek_thumb:I = 0x7f0c0179

.field public static final abc_tint_spinner:I = 0x7f0c017a

.field public static final abc_tint_switch_thumb:I = 0x7f0c017b

.field public static final abc_tint_switch_track:I = 0x7f0c017c

.field public static final accent_material_dark:I = 0x7f0c0017

.field public static final accent_material_light:I = 0x7f0c0018

.field public static final appBackground:I = 0x7f0c001c

.field public static final background_floating_material_dark:I = 0x7f0c001f

.field public static final background_floating_material_light:I = 0x7f0c0020

.field public static final background_material_dark:I = 0x7f0c0021

.field public static final background_material_light:I = 0x7f0c0022

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c002e

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c002f

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c0030

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c0031

.field public static final bright_foreground_material_dark:I = 0x7f0c0032

.field public static final bright_foreground_material_light:I = 0x7f0c0033

.field public static final buttonBack:I = 0x7f0c0036

.field public static final buttonBackPressed:I = 0x7f0c0037

.field public static final buttonBorder:I = 0x7f0c0038

.field public static final buttonBorderDisabled:I = 0x7f0c0039

.field public static final buttonTextPressed:I = 0x7f0c003a

.field public static final button_material_dark:I = 0x7f0c003e

.field public static final button_material_light:I = 0x7f0c003f

.field public static final button_text:I = 0x7f0c017e

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c0083

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c0084

.field public static final dim_foreground_material_dark:I = 0x7f0c0085

.field public static final dim_foreground_material_light:I = 0x7f0c0086

.field public static final footerDivider:I = 0x7f0c00b9

.field public static final foreground_material_dark:I = 0x7f0c00ba

.field public static final foreground_material_light:I = 0x7f0c00bb

.field public static final highlighted_text_material_dark:I = 0x7f0c00c8

.field public static final highlighted_text_material_light:I = 0x7f0c00c9

.field public static final material_blue_grey_800:I = 0x7f0c00cf

.field public static final material_blue_grey_900:I = 0x7f0c00d0

.field public static final material_blue_grey_950:I = 0x7f0c00d1

.field public static final material_deep_teal_200:I = 0x7f0c00d2

.field public static final material_deep_teal_500:I = 0x7f0c00d3

.field public static final material_grey_100:I = 0x7f0c00d4

.field public static final material_grey_300:I = 0x7f0c00d5

.field public static final material_grey_50:I = 0x7f0c00d6

.field public static final material_grey_600:I = 0x7f0c00d7

.field public static final material_grey_800:I = 0x7f0c00d8

.field public static final material_grey_850:I = 0x7f0c00d9

.field public static final material_grey_900:I = 0x7f0c00da

.field public static final overflowMenu:I = 0x7f0c00f1

.field public static final popupMessageBackground:I = 0x7f0c00fa

.field public static final popupMessageText:I = 0x7f0c00fb

.field public static final primary_dark_material_dark:I = 0x7f0c00fc

.field public static final primary_dark_material_light:I = 0x7f0c00fd

.field public static final primary_material_dark:I = 0x7f0c00fe

.field public static final primary_material_light:I = 0x7f0c00ff

.field public static final primary_text_default_material_dark:I = 0x7f0c0100

.field public static final primary_text_default_material_light:I = 0x7f0c0101

.field public static final primary_text_disabled_material_dark:I = 0x7f0c0102

.field public static final primary_text_disabled_material_light:I = 0x7f0c0103

.field public static final ripple_material_dark:I = 0x7f0c0119

.field public static final ripple_material_light:I = 0x7f0c011a

.field public static final secondary_text_default_material_dark:I = 0x7f0c0121

.field public static final secondary_text_default_material_light:I = 0x7f0c0122

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c0123

.field public static final secondary_text_disabled_material_light:I = 0x7f0c0124

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c0134

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c0135

.field public static final switch_thumb_material_dark:I = 0x7f0c018e

.field public static final switch_thumb_material_light:I = 0x7f0c018f

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c0136

.field public static final switch_thumb_normal_material_light:I = 0x7f0c0137

.field public static final userTileBackground:I = 0x7f0c0148

.field public static final xbid_activity_background:I = 0x7f0c0158

.field public static final xbid_body_dark:I = 0x7f0c0159

.field public static final xbid_body_grey:I = 0x7f0c015a

.field public static final xbid_body_light:I = 0x7f0c015b

.field public static final xbid_bottom_bar_shadow_end:I = 0x7f0c015c

.field public static final xbid_bottom_bar_shadow_start:I = 0x7f0c015d

.field public static final xbid_button_background:I = 0x7f0c015e

.field public static final xbid_button_background_pressed:I = 0x7f0c015f

.field public static final xbid_button_border:I = 0x7f0c0160

.field public static final xbid_clickable_view_background_pressed:I = 0x7f0c0161

.field public static final xbid_clickable_view_border:I = 0x7f0c0162

.field public static final xbid_colorPrimary:I = 0x7f0c0163

.field public static final xbid_header:I = 0x7f0c0164

.field public static final xbid_header_background:I = 0x7f0c0165

.field public static final xbid_white_primary:I = 0x7f0c0166

.field public static final xbid_white_secondary:I = 0x7f0c0167


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
