.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
.super Ljava/lang/Object;
.source "SkypeMessageDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SkypeMessage"
.end annotation


# instance fields
.field public final content:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final conversationLink:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final from:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final imdisplayname:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final messagetype:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final threadtopic:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "threadtopic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "messagetype"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "from"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p6, "conversationLink"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p7, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p8, "imdisplayname"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 64
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 65
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 66
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 67
    invoke-static {p7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 68
    invoke-static {p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->content:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->threadtopic:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->from:Ljava/lang/String;

    .line 75
    iput-object p6, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    .line 76
    iput-object p7, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->type:Ljava/lang/String;

    .line 77
    iput-object p8, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->imdisplayname:Ljava/lang/String;

    .line 78
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 82
    if-ne p1, p0, :cond_0

    .line 83
    const/4 v1, 0x1

    .line 88
    :goto_0
    return v1

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    if-nez v1, :cond_1

    .line 85
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 87
    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 88
    .local v0, "other":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 94
    const/16 v0, 0x11

    .line 95
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 97
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
