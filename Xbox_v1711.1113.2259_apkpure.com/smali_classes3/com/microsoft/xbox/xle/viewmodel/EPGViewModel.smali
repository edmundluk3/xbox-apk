.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;
.super Ljava/lang/Object;
.source "EPGViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;,
        Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;,
        Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Duration;
    }
.end annotation


# virtual methods
.method public abstract addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
.end method

.method public abstract getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
.end method

.method public abstract getChannelCount()I
.end method

.method public abstract getLiveTvChannelCount()I
.end method

.method public abstract getProgramEnumerator(IJZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;
.end method

.method public abstract getTimeSpan()I
.end method

.method public abstract removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
.end method

.method public abstract showDetailsView(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V
.end method
