.class public Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field forceAdd:Z

.field id:J

.field scrappedFromPosition:I

.field viewType:I


# direct methods
.method public constructor <init>(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 5442
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 5426
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->id:J

    .line 5444
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    if-ne v0, v3, :cond_0

    .line 5445
    const-string v0, "TwoWayView"

    const-string v1, "Constructing LayoutParams with width FILL_PARENT does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5446
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    .line 5449
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    .line 5450
    const-string v0, "TwoWayView"

    const-string v1, "Constructing LayoutParams with height FILL_PARENT does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5451
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    .line 5453
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, -0x1

    .line 5456
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5426
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->id:J

    .line 5458
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    if-ne v0, v2, :cond_0

    .line 5459
    const-string v0, "TwoWayView"

    const-string v1, "Inflation setting LayoutParams width to MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5460
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    .line 5463
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    if-ne v0, v2, :cond_1

    .line 5464
    const-string v0, "TwoWayView"

    const-string v1, "Inflation setting LayoutParams height to MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5465
    const/4 v0, -0x2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    .line 5467
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "other"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 5470
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5426
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->id:J

    .line 5472
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    if-ne v0, v3, :cond_0

    .line 5473
    const-string v0, "TwoWayView"

    const-string v1, "Constructing LayoutParams with width MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5474
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    .line 5477
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    .line 5478
    const-string v0, "TwoWayView"

    const-string v1, "Constructing LayoutParams with height MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5479
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    .line 5481
    :cond_1
    return-void
.end method
