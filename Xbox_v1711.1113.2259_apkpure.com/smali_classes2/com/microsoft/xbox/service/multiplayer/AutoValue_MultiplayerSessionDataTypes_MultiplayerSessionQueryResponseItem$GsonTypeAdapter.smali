.class public final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final acceptedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final clubIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAccepted:J

.field private defaultClubId:J

.field private defaultFirstMemberXuid:J

.field private defaultJoinRestriction:Ljava/lang/String;

.field private defaultKeywords:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultReadRestriction:Ljava/lang/String;

.field private defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private defaultStartTime:Ljava/util/Date;

.field private defaultVisibility:Ljava/lang/String;

.field private final firstMemberXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final joinRestrictionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final keywordsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readRestrictionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionRefAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;",
            ">;"
        }
    .end annotation
.end field

.field private final startTimeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final visibilityAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 36
    iput-wide v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultAccepted:J

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultVisibility:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultJoinRestriction:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultReadRestriction:Ljava/lang/String;

    .line 40
    iput-wide v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultClubId:J

    .line 41
    iput-wide v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultFirstMemberXuid:J

    .line 42
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultKeywords:Lcom/google/common/collect/ImmutableList;

    .line 44
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->acceptedAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->visibilityAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->joinRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->readRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->firstMemberXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->keywordsAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    .locals 17
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v16, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v16

    if-ne v2, v0, :cond_0

    .line 122
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 123
    const/4 v2, 0x0

    .line 184
    :goto_0
    return-object v2

    .line 125
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 127
    .local v3, "startTime":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 128
    .local v4, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultAccepted:J

    .line 129
    .local v5, "accepted":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultVisibility:Ljava/lang/String;

    .line 130
    .local v7, "visibility":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultJoinRestriction:Ljava/lang/String;

    .line 131
    .local v8, "joinRestriction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultReadRestriction:Ljava/lang/String;

    .line 132
    .local v9, "readRestriction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultClubId:J

    .line 133
    .local v10, "clubId":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultFirstMemberXuid:J

    .line 134
    .local v12, "firstMemberXuid":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultKeywords:Lcom/google/common/collect/ImmutableList;

    .line 135
    .local v14, "keywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v15

    .line 137
    .local v15, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v16, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v16

    if-ne v2, v0, :cond_1

    .line 138
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 141
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v15}, Ljava/lang/String;->hashCode()I

    move-result v16

    sparse-switch v16, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 179
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 141
    :sswitch_0
    const-string v16, "startTime"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string v16, "sessionRef"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string v16, "accepted"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v16, "visibility"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v16, "joinRestriction"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x4

    goto :goto_2

    :sswitch_5
    const-string v16, "readRestriction"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x5

    goto :goto_2

    :sswitch_6
    const-string v16, "clubId"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x6

    goto :goto_2

    :sswitch_7
    const-string v16, "firstMemberXuid"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/4 v2, 0x7

    goto :goto_2

    :sswitch_8
    const-string v16, "keywords"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/16 v2, 0x8

    goto :goto_2

    .line 143
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "startTime":Ljava/util/Date;
    check-cast v3, Ljava/util/Date;

    .line 144
    .restart local v3    # "startTime":Ljava/util/Date;
    goto/16 :goto_1

    .line 147
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    check-cast v4, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 148
    .restart local v4    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    goto/16 :goto_1

    .line 151
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->acceptedAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 152
    goto/16 :goto_1

    .line 155
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->visibilityAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "visibility":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 156
    .restart local v7    # "visibility":Ljava/lang/String;
    goto/16 :goto_1

    .line 159
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->joinRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "joinRestriction":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 160
    .restart local v8    # "joinRestriction":Ljava/lang/String;
    goto/16 :goto_1

    .line 163
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->readRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "readRestriction":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 164
    .restart local v9    # "readRestriction":Ljava/lang/String;
    goto/16 :goto_1

    .line 167
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 168
    goto/16 :goto_1

    .line 171
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->firstMemberXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 172
    goto/16 :goto_1

    .line 175
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->keywordsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "keywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    check-cast v14, Lcom/google/common/collect/ImmutableList;

    .line 176
    .restart local v14    # "keywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 183
    .end local v15    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 184
    new-instance v2, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;

    invoke-direct/range {v2 .. v14}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem;-><init>(Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 141
    :sswitch_data_0
    .sparse-switch
        -0x7ff16059 -> :sswitch_2
        -0x7eea75b1 -> :sswitch_0
        -0x786257aa -> :sswitch_5
        -0x50e7a78f -> :sswitch_6
        -0x44f17a9e -> :sswitch_7
        0x1f2e9faa -> :sswitch_8
        0x4c74e042 -> :sswitch_4
        0x630dae1d -> :sswitch_1
        0x73b66312 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAccepted(J)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultAccepted"    # J

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultAccepted:J

    .line 64
    return-object p0
.end method

.method public setDefaultClubId(J)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultClubId"    # J

    .prologue
    .line 79
    iput-wide p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultClubId:J

    .line 80
    return-object p0
.end method

.method public setDefaultFirstMemberXuid(J)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultFirstMemberXuid"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultFirstMemberXuid:J

    .line 84
    return-object p0
.end method

.method public setDefaultJoinRestriction(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultJoinRestriction"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultJoinRestriction:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public setDefaultKeywords(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "defaultKeywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultKeywords:Lcom/google/common/collect/ImmutableList;

    .line 88
    return-object p0
.end method

.method public setDefaultReadRestriction(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultReadRestriction"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultReadRestriction:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public setDefaultSessionRef(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 60
    return-object p0
.end method

.method public setDefaultStartTime(Ljava/util/Date;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultStartTime"    # Ljava/util/Date;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 56
    return-object p0
.end method

.method public setDefaultVisibility(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultVisibility"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->defaultVisibility:Ljava/lang/String;

    .line 68
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    if-nez p2, :cond_0

    .line 94
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 117
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 98
    const-string v0, "startTime"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->startTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 100
    const-string v0, "sessionRef"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 102
    const-string v0, "accepted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->acceptedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->accepted()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 104
    const-string v0, "visibility"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->visibilityAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->visibility()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 106
    const-string v0, "joinRestriction"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->joinRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->joinRestriction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 108
    const-string v0, "readRestriction"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->readRestrictionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->readRestriction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 110
    const-string v0, "clubId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->clubIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->clubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 112
    const-string v0, "firstMemberXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->firstMemberXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->firstMemberXuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 114
    const-string v0, "keywords"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->keywordsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->keywords()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 116
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;)V

    return-void
.end method
