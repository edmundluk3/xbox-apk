.class public final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MultiplayerSessionDataTypes_LfgNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultLfg_notification:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

.field private final lfg_notificationAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->defaultLfg_notification:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    .line 21
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->lfg_notificationAdapter:Lcom/google/gson/TypeAdapter;

    .line 22
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    .locals 4
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v2, v3, :cond_0

    .line 43
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 44
    const/4 v2, 0x0

    .line 65
    :goto_0
    return-object v2

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->defaultLfg_notification:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    .line 48
    .local v1, "lfg_notification":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 49
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v2, v3, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 54
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_1

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 54
    :pswitch_0
    const-string v3, "lfg_notification"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    .line 56
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->lfg_notificationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v2, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "lfg_notification":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;
    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    .line 57
    .restart local v1    # "lfg_notification":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;
    goto :goto_1

    .line 64
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 65
    new-instance v2, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x7d4c01bd
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultLfg_notification(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLfg_notification"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->defaultLfg_notification:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    .line 25
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    if-nez p2, :cond_0

    .line 31
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 38
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 35
    const-string v0, "lfg_notification"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->lfg_notificationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;->lfg_notification()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;)V

    return-void
.end method
