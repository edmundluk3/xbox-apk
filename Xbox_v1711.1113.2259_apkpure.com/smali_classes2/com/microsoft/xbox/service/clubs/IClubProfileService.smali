.class public interface abstract Lcom/microsoft/xbox/service/clubs/IClubProfileService;
.super Ljava/lang/Object;
.source "IClubProfileService.java"


# virtual methods
.method public abstract updateClubProfile(JLcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
