.class Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;
.super Lorg/json/JSONObject;
.source "JsonGyroscope.java"


# direct methods
.method public constructor <init>(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 12
    const-string v0, "x"

    invoke-virtual {p0, v0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 13
    const-string v0, "y"

    invoke-virtual {p0, v0, p3, p4}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 14
    const-string v0, "z"

    invoke-virtual {p0, v0, p5, p6}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 15
    return-void
.end method
