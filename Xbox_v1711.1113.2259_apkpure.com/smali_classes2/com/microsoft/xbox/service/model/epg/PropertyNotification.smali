.class public Lcom/microsoft/xbox/service/model/epg/PropertyNotification;
.super Ljava/lang/Object;
.source "PropertyNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;
    }
.end annotation


# instance fields
.field private final listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 14
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V
    .locals 1
    .param p1, "l"    # Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 20
    :cond_0
    return-void
.end method

.method public notify(Ljava/lang/String;)V
    .locals 3
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    .line 27
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;

    .line 28
    .local v0, "l":Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;->onPropertyChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 30
    .end local v0    # "l":Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V
    .locals 1
    .param p1, "l"    # Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 24
    return-void
.end method
