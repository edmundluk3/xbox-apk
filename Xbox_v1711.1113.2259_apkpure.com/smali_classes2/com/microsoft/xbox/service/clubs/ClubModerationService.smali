.class public final enum Lcom/microsoft/xbox/service/clubs/ClubModerationService;
.super Ljava/lang/Enum;
.source "ClubModerationService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubModerationService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubModerationService;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubModerationService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationService;

.field private static final BATCH_DELETE_ITEMS_ENDPOINT:Ljava/lang/String; = "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems/batch/delete"

.field private static final CONTRACT_VERSION:I = 0x1

.field private static final GET_REPORTS_ENDPOINT:Ljava/lang/String; = "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubModerationService;

.field private static final REPORT_ITEM_ENDPOINT:Ljava/lang/String; = "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    .line 29
    new-array v0, v3, [Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    .line 33
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->TAG:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->STATIC_HEADERS:Ljava/util/List;

    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getHeaders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 101
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 103
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept-Language"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubModerationService;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubModerationService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubModerationService;

    return-object v0
.end method


# virtual methods
.method public batchDeleteItemReports(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;)Z
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "batchRequest"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 85
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "batchDeleteItemReports(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " batchRequest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 88
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 91
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems/batch/delete"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 93
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 95
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public getReportedItems(J)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getReportedItems(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 59
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 61
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 60
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;

    .line 59
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;

    return-object v0
.end method

.method public reportItem(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;)Z
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "itemRequest"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportItem(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " itemRequest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 72
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 75
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubmoderation.xboxlive.com/clubs/%d/reporteditems"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 77
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubModerationService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 79
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    .line 75
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method
