.class public final Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;
.super Ljava/lang/Object;
.source "EditorialServiceModule_ProvideEditorialServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/service/editorial/EditorialService;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

.field private final retrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p2, "retrofitProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/Retrofit;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->module:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    .line 23
    sget-boolean v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->retrofitProvider:Ljavax/inject/Provider;

    .line 25
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/service/editorial/EditorialService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "retrofitProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/Retrofit;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;-><init>(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideEditorialService(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/editorial/EditorialService;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;
    .param p1, "retrofit"    # Lretrofit2/Retrofit;

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;->provideEditorialService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/service/editorial/EditorialService;
    .locals 2

    .prologue
    .line 29
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->module:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->retrofitProvider:Ljavax/inject/Provider;

    .line 30
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Retrofit;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;->provideEditorialService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 29
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->get()Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    move-result-object v0

    return-object v0
.end method
