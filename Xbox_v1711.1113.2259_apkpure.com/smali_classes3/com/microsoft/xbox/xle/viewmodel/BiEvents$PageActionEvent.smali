.class Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;
.super Ljava/lang/Object;
.source "BiEvents.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/BiEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PageActionEvent"
.end annotation


# instance fields
.field private final action:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V
    .locals 0
    .param p1, "action"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;->action:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 72
    return-void
.end method


# virtual methods
.method public post()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;->post(I)V

    .line 77
    return-void
.end method

.method public post(I)V
    .locals 3
    .param p1, "indexPos"    # I

    .prologue
    .line 81
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;->action:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 83
    return-void
.end method
