.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "ConversationsListAdapter$ConversationViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    .line 27
    const v0, 0x7f0e04a5

    const-string v1, "field \'title\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 28
    const v0, 0x7f0e04b3

    const-string v1, "field \'sender\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sender:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v0, 0x7f0e04b4

    const-string v1, "field \'senderRealname\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->senderRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e04b5

    const-string v1, "field \'sentTime\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f0e04af

    const-string v1, "field \'tile\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 32
    const v0, 0x7f0e0231

    const-string v1, "field \'presenceIcon\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->presenceIcon:Landroid/widget/ImageView;

    .line 33
    const v0, 0x7f0e0484

    const-string v1, "field \'attachmentIcon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e04b0

    const-string v1, "field \'favoriteIcon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 35
    const v0, 0x7f0e046d

    const-string v1, "field \'iconsContainerRow2\'"

    const-class v2, Landroid/widget/TableRow;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->iconsContainerRow2:Landroid/widget/TableRow;

    .line 36
    const v0, 0x7f0e046b

    const-string v1, "field \'groupTile1\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 37
    const v0, 0x7f0e046c

    const-string v1, "field \'groupTile2\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 38
    const v0, 0x7f0e046e

    const-string v1, "field \'groupTile3\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 39
    const v0, 0x7f0e046f

    const-string v1, "field \'groupTile4\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 40
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    .line 46
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sender:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->senderRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->presenceIcon:Landroid/widget/ImageView;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 56
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->iconsContainerRow2:Landroid/widget/TableRow;

    .line 58
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 59
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 60
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 61
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 62
    return-void
.end method
