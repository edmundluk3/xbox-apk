.class Lcom/microsoft/playready/HttpClient;
.super Ljava/lang/Object;
.source "HttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/HttpClient$ConnectionSpec;,
        Lcom/microsoft/playready/HttpClient$HeaderItem;
    }
.end annotation


# static fields
.field public static RECCOMENDED_RETRY_COUNT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x5

    sput v0, Lcom/microsoft/playready/HttpClient;->RECCOMENDED_RETRY_COUNT:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static doTransaction(Ljava/lang/String;Lcom/microsoft/playready/HttpClient$ConnectionSpec;Ljava/lang/String;[B)Lcom/microsoft/playready/HttpResponse;
    .locals 25
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "spec"    # Lcom/microsoft/playready/HttpClient$ConnectionSpec;
    .param p2, "headers"    # Ljava/lang/String;
    .param p3, "body"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/net/MalformedURLException;,
            Lcom/microsoft/playready/HttpRequestFailedException;,
            Lcom/microsoft/playready/HttpRedirectRequestedException;
        }
    .end annotation

    .prologue
    .line 136
    const/16 v17, 0x0

    .line 137
    .local v17, "u":Ljava/net/URL;
    const/4 v3, 0x0

    .line 138
    .local v3, "conn":Ljava/net/HttpURLConnection;
    const/4 v7, 0x0

    .line 139
    .local v7, "is":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 140
    .local v10, "os":Ljava/io/OutputStream;
    if-nez p1, :cond_0

    .line 141
    new-instance p1, Lcom/microsoft/playready/HttpClient$ConnectionSpec;

    .end local p1    # "spec":Lcom/microsoft/playready/HttpClient$ConnectionSpec;
    invoke-direct/range {p1 .. p1}, Lcom/microsoft/playready/HttpClient$ConnectionSpec;-><init>()V

    .line 144
    .restart local p1    # "spec":Lcom/microsoft/playready/HttpClient$ConnectionSpec;
    :cond_0
    const/4 v14, 0x0

    .line 145
    .local v14, "response":Lcom/microsoft/playready/HttpResponse;
    const/16 v16, 0x0

    .line 146
    .local v16, "retryCount":I
    const/4 v4, 0x0

    .line 147
    .local v4, "fResponseRecieved":Z
    move-object/from16 v19, p0

    .local v19, "urlToTry":Ljava/lang/String;
    move-object v8, v7

    .local v8, "is":Ljava/lang/Object;
    move-object/from16 v18, v17

    .line 151
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v17    # "u":Ljava/net/URL;
    .local v18, "u":Ljava/net/URL;
    :goto_0
    :try_start_0
    new-instance v17, Ljava/net/URL;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 158
    .end local v18    # "u":Ljava/net/URL;
    .restart local v17    # "u":Ljava/net/URL;
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v3, v0

    .line 159
    move-object/from16 v0, p1

    iget v0, v0, Lcom/microsoft/playready/HttpClient$ConnectionSpec;->mTimeOutMs:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 160
    move-object/from16 v0, p1

    iget v0, v0, Lcom/microsoft/playready/HttpClient$ConnectionSpec;->mTimeOutMs:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 161
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 163
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v20, v0

    if-nez v20, :cond_6

    .line 164
    :cond_1
    const-string v20, "GET"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 165
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 171
    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/microsoft/playready/HttpClient;->getHeaderItemFromHeaderString(Ljava/lang/String;)[Lcom/microsoft/playready/HttpClient$HeaderItem;

    move-result-object v5

    .line 172
    .local v5, "hs":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    if-eqz v5, :cond_2

    .line 173
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v0, v5

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v6, v0, :cond_8

    .line 177
    .end local v6    # "i":I
    :cond_2
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 178
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v20

    const-string v21, "POST"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 179
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    .line 180
    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/io/OutputStream;->write([B)V

    .line 183
    :cond_3
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v15

    .line 185
    .local v15, "responseCode":I
    const/16 v20, 0x12f

    move/from16 v0, v20

    if-eq v0, v15, :cond_4

    .line 186
    const/16 v20, 0x12e

    move/from16 v0, v20

    if-eq v0, v15, :cond_4

    .line 187
    const/16 v20, 0x133

    move/from16 v0, v20

    if-ne v0, v15, :cond_a

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v20

    const-string v21, "GET"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 196
    :cond_4
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v20

    const-string v21, "Location"

    invoke-interface/range {v20 .. v21}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/List;

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 197
    .local v13, "redirectUrl":Ljava/lang/String;
    sget v20, Lcom/microsoft/playready/HttpClient;->RECCOMENDED_RETRY_COUNT:I

    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_9

    .line 199
    const-string v20, "HttpClient"

    const-string v21, "Automaticaly following redirect ( %1$d ) from %2$s to %3$s"

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    aput-object v19, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    add-int/lit8 v16, v16, 0x1

    .line 201
    move-object/from16 v19, v13

    move-object v7, v8

    .line 272
    .end local v8    # "is":Ljava/lang/Object;
    .end local v13    # "redirectUrl":Ljava/lang/String;
    .restart local v7    # "is":Ljava/io/InputStream;
    :goto_3
    const/4 v7, 0x0

    .line 273
    const/4 v10, 0x0

    .line 275
    if-eqz v3, :cond_5

    .line 276
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 277
    :cond_5
    const/4 v3, 0x0

    .line 279
    if-eqz v4, :cond_f

    .line 281
    return-object v14

    .line 167
    .end local v5    # "hs":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v15    # "responseCode":I
    .restart local v8    # "is":Ljava/lang/Object;
    :cond_6
    :try_start_2
    const-string v20, "POST"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 168
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 271
    :catchall_0
    move-exception v20

    move-object v7, v8

    .line 272
    .end local v8    # "is":Ljava/lang/Object;
    .end local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    .restart local v7    # "is":Ljava/io/InputStream;
    :goto_4
    const/4 v7, 0x0

    .line 273
    const/4 v10, 0x0

    .line 275
    if-eqz v3, :cond_7

    .line 276
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 277
    :cond_7
    const/4 v3, 0x0

    .line 278
    throw v20

    .line 174
    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v5    # "hs":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .restart local v6    # "i":I
    .restart local v8    # "is":Ljava/lang/Object;
    .restart local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    :cond_8
    :try_start_3
    aget-object v20, v5, v6

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/playready/HttpClient$HeaderItem;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aget-object v21, v5, v6

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/microsoft/playready/HttpClient$HeaderItem;->mValue:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 205
    .end local v6    # "i":I
    .restart local v13    # "redirectUrl":Ljava/lang/String;
    .restart local v15    # "responseCode":I
    :cond_9
    new-instance v20, Lcom/microsoft/playready/HttpRequestFailedException;

    .line 207
    const-string v21, "Exceeded maximum number of redirects attempting to reach %s, giving up."

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object p0, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 205
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p0

    invoke-direct {v0, v15, v1, v2}, Lcom/microsoft/playready/HttpRequestFailedException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v20

    .line 212
    .end local v13    # "redirectUrl":Ljava/lang/String;
    :cond_a
    const/16 v20, 0x133

    move/from16 v0, v20

    if-ne v0, v15, :cond_b

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v20

    const-string v21, "POST"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 228
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v20

    const-string v21, "Location"

    invoke-interface/range {v20 .. v21}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/List;

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 229
    .restart local v13    # "redirectUrl":Ljava/lang/String;
    const-string v20, "HttpClient"

    const-string v21, "Unsupported redirect on POST from %1$s to %2$s"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v19, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    new-instance v20, Lcom/microsoft/playready/HttpRedirectRequestedException;

    .line 232
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v21

    .line 230
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p0

    invoke-direct {v0, v15, v1, v2, v13}, Lcom/microsoft/playready/HttpRedirectRequestedException;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v20

    .line 235
    .end local v13    # "redirectUrl":Ljava/lang/String;
    :cond_b
    div-int/lit8 v20, v15, 0x64

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_d

    .line 242
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    .line 243
    .restart local v7    # "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 244
    .local v9, "nReadSize":I
    :try_start_4
    new-instance v11, Lorg/apache/http/util/ByteArrayBuffer;

    .end local v8    # "is":Ljava/lang/Object;
    const/16 v20, 0x1000

    move/from16 v0, v20

    invoke-direct {v11, v0}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 245
    .local v11, "readBab":Lorg/apache/http/util/ByteArrayBuffer;
    const/16 v20, 0x1000

    move/from16 v0, v20

    new-array v12, v0, [B

    .line 246
    .local v12, "readBuf":[B
    :goto_5
    const/16 v20, 0x0

    const/16 v21, 0x1000

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v12, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v9

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v9, v0, :cond_c

    .line 250
    new-instance v14, Lcom/microsoft/playready/HttpResponse;

    .end local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v11}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v14, v15, v0, v1, v2}, Lcom/microsoft/playready/HttpResponse;-><init>(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 251
    .restart local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    const/4 v4, 0x1

    .line 252
    goto/16 :goto_3

    .line 247
    :cond_c
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v12, v0, v9}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 271
    .end local v11    # "readBab":Lorg/apache/http/util/ByteArrayBuffer;
    .end local v12    # "readBuf":[B
    .end local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    :catchall_1
    move-exception v20

    goto/16 :goto_4

    .line 259
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v9    # "nReadSize":I
    .restart local v8    # "is":Ljava/lang/Object;
    .restart local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    :cond_d
    :try_start_5
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 260
    .restart local v7    # "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 261
    .restart local v9    # "nReadSize":I
    :try_start_6
    new-instance v11, Lorg/apache/http/util/ByteArrayBuffer;

    .end local v8    # "is":Ljava/lang/Object;
    const/16 v20, 0x1000

    move/from16 v0, v20

    invoke-direct {v11, v0}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 262
    .restart local v11    # "readBab":Lorg/apache/http/util/ByteArrayBuffer;
    const/16 v20, 0x1000

    move/from16 v0, v20

    new-array v12, v0, [B

    .line 263
    .restart local v12    # "readBuf":[B
    :goto_6
    const/16 v20, 0x0

    const/16 v21, 0x1000

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v12, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v9

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v9, v0, :cond_e

    .line 267
    new-instance v14, Lcom/microsoft/playready/HttpResponse;

    .end local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v11}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v14, v15, v0, v1, v2}, Lcom/microsoft/playready/HttpResponse;-><init>(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 268
    .restart local v14    # "response":Lcom/microsoft/playready/HttpResponse;
    const/4 v4, 0x1

    .line 271
    goto/16 :goto_3

    .line 264
    :cond_e
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v12, v0, v9}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_6

    .line 271
    .end local v5    # "hs":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v9    # "nReadSize":I
    .end local v11    # "readBab":Lorg/apache/http/util/ByteArrayBuffer;
    .end local v12    # "readBuf":[B
    .end local v15    # "responseCode":I
    .end local v17    # "u":Ljava/net/URL;
    .restart local v8    # "is":Ljava/lang/Object;
    .restart local v18    # "u":Ljava/net/URL;
    :catchall_2
    move-exception v20

    move-object v7, v8

    .restart local v7    # "is":Ljava/io/InputStream;
    move-object/from16 v17, v18

    .end local v18    # "u":Ljava/net/URL;
    .restart local v17    # "u":Ljava/net/URL;
    goto/16 :goto_4

    .end local v8    # "is":Ljava/lang/Object;
    .restart local v5    # "hs":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .restart local v15    # "responseCode":I
    :cond_f
    move-object v8, v7

    .restart local v8    # "is":Ljava/lang/Object;
    move-object/from16 v18, v17

    .end local v17    # "u":Ljava/net/URL;
    .restart local v18    # "u":Ljava/net/URL;
    goto/16 :goto_0
.end method

.method private static getHeaderItemFromHeaderString(Ljava/lang/String;)[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .locals 11
    .param p0, "hstr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/StringReader;

    invoke-direct {v7, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 98
    .local v5, "sr":Ljava/io/BufferedReader;
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 101
    .local v1, "headers":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/microsoft/playready/HttpClient$HeaderItem;>;"
    :goto_0
    :try_start_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 102
    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_1

    .line 120
    .end local v3    # "line":Ljava/lang/String;
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v4, v7, [Lcom/microsoft/playready/HttpClient$HeaderItem;

    .line 121
    .local v4, "ret":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    invoke-virtual {v1, v4}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/microsoft/playready/HttpClient$HeaderItem;

    return-object v7

    .line 104
    .end local v4    # "ret":[Lcom/microsoft/playready/HttpClient$HeaderItem;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v7, ":"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 105
    .local v0, "colonPos":I
    if-gez v0, :cond_2

    .line 107
    new-instance v7, Ljava/lang/IllegalArgumentException;

    .line 108
    const-string v8, "Provided Header string does not appear to be properly formatted: /n %1$s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p0, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 107
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 115
    .end local v0    # "colonPos":I
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v7

    goto :goto_1

    .line 111
    .restart local v0    # "colonPos":I
    .restart local v3    # "line":Ljava/lang/String;
    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 112
    .local v2, "key":Ljava/lang/String;
    add-int/lit8 v7, v0, 0x2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "value":Ljava/lang/String;
    new-instance v7, Lcom/microsoft/playready/HttpClient$HeaderItem;

    invoke-direct {v7, v2, v6}, Lcom/microsoft/playready/HttpClient$HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
