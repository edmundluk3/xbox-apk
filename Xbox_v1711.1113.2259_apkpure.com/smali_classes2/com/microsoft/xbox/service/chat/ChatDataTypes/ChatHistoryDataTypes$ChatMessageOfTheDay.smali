.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
.super Ljava/lang/Object;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChatMessageOfTheDay"
.end annotation


# instance fields
.field public final message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V
    .locals 0
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    .line 195
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 199
    if-ne p1, p0, :cond_0

    .line 200
    const/4 v1, 0x1

    .line 205
    :goto_0
    return v1

    .line 201
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;

    if-nez v1, :cond_1

    .line 202
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 204
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;

    .line 205
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    iget-object v2, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 211
    const/16 v0, 0x11

    .line 212
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 214
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
