.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;
.super Landroid/content/BroadcastReceiver;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 150
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Processing locale change"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clearing endpoint and re-registering"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$100(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$300(Lcom/microsoft/xbox/service/model/gcm/GcmModel;IZ)V

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0, v2, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$400(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZZ)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Not registered, no need to re-register on locale change"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
