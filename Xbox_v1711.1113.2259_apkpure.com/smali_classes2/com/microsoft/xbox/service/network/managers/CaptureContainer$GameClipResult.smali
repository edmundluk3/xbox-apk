.class public Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
.super Ljava/lang/Object;
.source "CaptureContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/CaptureContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameClipResult"
.end annotation


# instance fields
.field public gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    return-object v0
.end method
