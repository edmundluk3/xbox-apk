.class public Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "DetailsPivotActivity.java"


# instance fields
.field private content:Ljava/lang/String;

.field private detailDisplayTitle:Ljava/lang/String;

.field private detailRootLayout:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

.field private headerRootView:Landroid/view/View;

.field private mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

.field private pivotHeaderData:Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

.field private screenName:Ljava/lang/String;

.field private screenType:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 34
    const-string v0, "DetailsPivotScreen"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenName:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->content:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailRootLayout:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    return-object v0
.end method

.method private getScreenNameFromMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Ljava/lang/String;
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    const-string v1, "DetailsScreen"

    .line 359
    :goto_0
    return-object v1

    .line 299
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 301
    .local v0, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$2;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV3MediaType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 359
    const-string v1, "DetailsScreen"

    goto :goto_0

    .line 306
    :pswitch_0
    const-string v1, "App Details"

    goto :goto_0

    .line 317
    :pswitch_1
    const-string v1, "Game Details"

    goto :goto_0

    .line 328
    :pswitch_2
    const-string v1, "DLC Details"

    goto :goto_0

    .line 335
    :pswitch_3
    const-string v1, "Companion Details"

    goto :goto_0

    .line 337
    :pswitch_4
    const-string v1, "Movie Details"

    goto :goto_0

    .line 339
    :pswitch_5
    const-string v1, "Show Details"

    goto :goto_0

    .line 342
    :pswitch_6
    const-string v1, "Episode Details"

    goto :goto_0

    .line 346
    :pswitch_7
    const-string v1, "Series Details"

    goto :goto_0

    .line 351
    :pswitch_8
    const-string v1, "Album Details"

    goto :goto_0

    .line 354
    :pswitch_9
    const-string v1, "Artist Details"

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private panesHaveChanged([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)Z
    .locals 4
    .param p1, "newPivotData"    # [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .param p2, "pivotData"    # [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    .prologue
    const/4 v1, 0x1

    .line 167
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v1

    .line 171
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_2

    .line 172
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addPivotPane(Ljava/lang/Class;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 182
    const-string v3, "DetailsPivotActivity"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Adding pivot pane class \'%s\'"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->getIsStarted()Z

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "index":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    array-length v6, v5

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_4

    aget-object v1, v5, v3

    .line 189
    .local v1, "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getIsDisplayed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 191
    add-int/lit8 v0, v0, 0x1

    .line 188
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 193
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 194
    .local v2, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_3

    .line 196
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    .line 197
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 198
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 201
    invoke-virtual {v1, v9}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->setIsDisplayed(Z)V

    .line 204
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v3

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 207
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v3

    if-lt v3, v0, :cond_2

    .line 208
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    .line 213
    :goto_1
    const-string v3, "DetailsPivotActivity"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Successfully added pivot pane class \'%s\'"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .end local v1    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .end local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_2
    return-void

    .line 210
    .restart local v1    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .restart local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    goto :goto_1

    .line 217
    :cond_3
    const-string v3, "DetailsPivotActivity"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Failed to add pivot pane class \'%s\'"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .end local v1    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .end local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_4
    const-string v3, "DetailsPivotActivity"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Pivot pane class \'%s\' is not added. Either it is already displayed or not defined correctly in DetailPageHelper."

    new-array v7, v9, [Ljava/lang/Object;

    .line 223
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 222
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public forceRefresh()V
    .locals 5

    .prologue
    .line 143
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenType:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getPaneConfigData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v1

    .line 144
    .local v1, "newPivotData":[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->panesHaveChanged([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    .line 146
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setDetailPivotData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v2

    .line 150
    .local v2, "total":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setPaneData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 155
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 156
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onStart()V

    .line 159
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 163
    .end local v0    # "i":I
    .end local v2    # "total":I
    :goto_1
    return-void

    .line 161
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->forceRefresh()V

    goto :goto_1
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method protected getChannelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    const-string v0, "Details"

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getContentKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    const-string v0, "mediaID"

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 44
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getDetailPivotData()[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getDetailScreenTypeFromMediaType(I)Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenType:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenType:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getDetailPivotHeaderData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;)Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotHeaderData:Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    .line 55
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->isActivityDetailsPivotPaneData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailDisplayTitle:Ljava/lang/String;

    .line 61
    :goto_1
    const v1, 0x7f0300db

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->setContentView(I)V

    .line 62
    const v1, 0x7f0e0500

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 63
    const v1, 0x7f0e04ff

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailRootLayout:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setPaneData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotHeaderData:Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    if-nez v1, :cond_2

    .line 71
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 76
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->getScreenNameFromMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->screenName:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-nez v1, :cond_3

    move-object v1, v2

    :goto_3
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->content:Ljava/lang/String;

    .line 78
    return-void

    .line 52
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotHeaderData:Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailDisplayTitle:Ljava/lang/String;

    goto :goto_1

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotHeaderData:Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->setPivotHeader(Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;)V

    goto :goto_2

    .line 77
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStart()V

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setDetailPivotData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    const v1, 0x7f02012e

    const v2, 0x7f020162

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setPageIndicatorDrawables(II)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0082

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setPageIndicatorBackground(I)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V

    .line 113
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStop()V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    const v1, 0x7f020068

    const v2, 0x7f02012e

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setPageIndicatorDrawables(II)V

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0145

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setPageIndicatorBackground(I)V

    .line 122
    return-void
.end method

.method public removePivotPane(Ljava/lang/Class;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 228
    const-string v4, "DetailsPivotActivity"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Removing pivot pane class \'%s\'"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->getIsStarted()Z

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "pivotIndex":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivotData:[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_2

    aget-object v0, v6, v4

    .line 235
    .local v0, "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getIsDisplayed()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 236
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 237
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 238
    .local v2, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_1

    .line 240
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 241
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 242
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    .line 245
    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->setIsDisplayed(Z)V

    .line 248
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v3

    .line 249
    .local v3, "totalPaneCount":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 252
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v4

    if-lt v4, v1, :cond_0

    .line 253
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    .line 258
    :goto_1
    const-string v4, "DetailsPivotActivity"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Successfully removed pivot pane class \'%s\'"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .end local v0    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .end local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v3    # "totalPaneCount":I
    :goto_2
    return-void

    .line 255
    .restart local v0    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .restart local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .restart local v3    # "totalPaneCount":I
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    goto :goto_1

    .line 262
    .end local v3    # "totalPaneCount":I
    :cond_1
    const-string v4, "DetailsPivotActivity"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Failed to remove pivot pane class \'%s\'"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .end local v0    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .end local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_2
    const-string v4, "DetailsPivotActivity"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Pivot pane class \'%s\' is not removed. Either it is already removed or not defined correctly in DetailPageHelper."

    new-array v8, v10, [Ljava/lang/Object;

    .line 270
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 269
    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 265
    .restart local v0    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 234
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method protected setPivotHeader(Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;)V
    .locals 7
    .param p1, "pivotHeaderData"    # Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    .prologue
    const/4 v6, 0x0

    .line 125
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailRootLayout:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    if-nez v1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->getPivotHeaderLayout()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->headerRootView:Landroid/view/View;

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->detailRootLayout:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->headerRootView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->addHeaderView(Landroid/view/View;)V

    .line 135
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->getViewModelClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DetailsPivotActivity"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "FIXME: Failed to create a header viewmodel of type \'%s\' with error: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;->getViewModelClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
