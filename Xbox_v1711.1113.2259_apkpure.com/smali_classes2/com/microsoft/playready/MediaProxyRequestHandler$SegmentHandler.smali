.class Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;
.super Ljava/lang/Object;
.source "MediaProxyRequestHandler.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxyRequestHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SegmentHandler"
.end annotation


# instance fields
.field private final mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private final mSegmentID:I

.field private final mStreamID:I


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;II)V
    .locals 0
    .param p1, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;
    .param p3, "streamID"    # I
    .param p4, "segmentID"    # I

    .prologue
    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 337
    iput p3, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mStreamID:I

    .line 338
    iput p4, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mSegmentID:I

    .line 339
    return-void
.end method


# virtual methods
.method public HandleElementRequest(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    iget-object v1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    iget v2, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mStreamID:I

    iget v3, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;->mSegmentID:I

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/playready/MediaProxy;->generateSegment(II)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 350
    .local v0, "body":Lorg/apache/http/entity/AbstractHttpEntity;
    const/16 v1, 0xc8

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 351
    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 352
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 353
    return-void
.end method

.method public Unregester()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method
