.class Lcom/appsflyer/Foreground$1;
.super Ljava/util/TimerTask;
.source "Foreground.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/appsflyer/Foreground;->onActivityPaused(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/appsflyer/Foreground;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/appsflyer/Foreground;Landroid/app/Activity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/appsflyer/Foreground;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/appsflyer/Foreground$1;->this$0:Lcom/appsflyer/Foreground;

    iput-object p2, p0, Lcom/appsflyer/Foreground$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 159
    iget-object v2, p0, Lcom/appsflyer/Foreground$1;->this$0:Lcom/appsflyer/Foreground;

    invoke-static {v2}, Lcom/appsflyer/Foreground;->access$000(Lcom/appsflyer/Foreground;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/appsflyer/Foreground$1;->this$0:Lcom/appsflyer/Foreground;

    invoke-static {v2}, Lcom/appsflyer/Foreground;->access$100(Lcom/appsflyer/Foreground;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/appsflyer/Foreground$1;->this$0:Lcom/appsflyer/Foreground;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/appsflyer/Foreground;->access$002(Lcom/appsflyer/Foreground;Z)Z

    .line 161
    iget-object v2, p0, Lcom/appsflyer/Foreground$1;->this$0:Lcom/appsflyer/Foreground;

    invoke-static {v2}, Lcom/appsflyer/Foreground;->access$200(Lcom/appsflyer/Foreground;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/appsflyer/Foreground$Listener;

    .line 163
    .local v1, "l":Lcom/appsflyer/Foreground$Listener;
    :try_start_0
    iget-object v3, p0, Lcom/appsflyer/Foreground$1;->val$activity:Landroid/app/Activity;

    invoke-interface {v1, v3}, Lcom/appsflyer/Foreground$Listener;->onBecameBackground(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "exc":Ljava/lang/Exception;
    const-string v3, "Listener threw exception! "

    invoke-static {v3, v0}, Lcom/appsflyer/AFLogger;->afLogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 169
    .end local v0    # "exc":Ljava/lang/Exception;
    .end local v1    # "l":Lcom/appsflyer/Foreground$Listener;
    :cond_0
    return-void
.end method
