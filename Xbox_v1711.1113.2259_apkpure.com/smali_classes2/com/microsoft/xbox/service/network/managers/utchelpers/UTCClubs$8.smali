.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionName:Ljava/lang/String;

.field final synthetic val$clubId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$actionName:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$clubId:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$actionName:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 245
    const-wide/16 v2, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$clubId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 247
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 248
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$clubId:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;->val$actionName:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 251
    return-void
.end method
