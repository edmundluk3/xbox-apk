.class public final Lcom/microsoft/xbox/toolkit/deeplink/TwitchDeepLinker;
.super Ljava/lang/Object;
.source "TwitchDeepLinker.java"


# static fields
.field private static final IN_APP_URI:Ljava/lang/String; = "twitch://stream/%s"

.field private static final WEB_URL:Ljava/lang/String; = "https://www.twitch.tv/%s/"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances allowed."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static getAppStreamIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "channelId"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "twitch://stream/%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method private static getWebStreamIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "channelId"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "https://www.twitch.tv/%s/"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public static launchStream(Ljava/lang/String;)Z
    .locals 4
    .param p0, "channelId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->newInstance()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v2

    .line 27
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/deeplink/TwitchDeepLinker;->getAppStreamIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v2

    .line 28
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/deeplink/TwitchDeepLinker;->getWebStreamIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v0

    .line 30
    .local v0, "deepLinkRequest":Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->intentOnlyExecutor()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->execute(Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;)Landroid/content/Intent;

    move-result-object v1

    .line 32
    .local v1, "launchedIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
