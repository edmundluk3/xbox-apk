.class Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;
.super Landroid/widget/Filter;
.source "ClubSearchScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

.field final synthetic val$items:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->val$items:Ljava/util/List;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 399
    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 401
    .local v3, "results":Landroid/widget/Filter$FilterResults;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 402
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 403
    .local v1, "lowerConstraint":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 405
    .local v2, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->val$items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 406
    .local v0, "i":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 407
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 411
    .end local v0    # "i":Ljava/lang/String;
    :cond_1
    iput-object v2, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 412
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    iput v4, v3, Landroid/widget/Filter$FilterResults;->count:I

    .line 415
    .end local v1    # "lowerConstraint":Ljava/lang/String;
    .end local v2    # "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    return-object v3
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 420
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->clear()V

    .line 422
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 424
    .local v0, "matches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->notifyDataSetInvalidated()V

    .line 430
    :goto_0
    return-void

    .line 427
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->addAll(Ljava/util/Collection;)V

    .line 428
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
