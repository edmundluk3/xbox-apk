.class public Lcom/microsoft/xbox/smartglass/PrimaryDevice;
.super Ljava/lang/Object;
.source "PrimaryDevice.java"


# instance fields
.field private final _pPrimaryDevice:Lcom/microsoft/xbox/smartglass/RefTPtr;

.field public final flags:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;",
            ">;"
        }
    .end annotation
.end field

.field public final hardwareId:Ljava/lang/String;

.field public final host:Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final isRemembered:Z

.field public final lastConnected:Ljava/util/Calendar;

.field public final lastUpdated:Ljava/util/Calendar;

.field public final name:Ljava/lang/String;

.field public final service:Ljava/lang/String;

.field public final status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

.field public final type:Lcom/microsoft/xbox/smartglass/DeviceType;


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/Calendar;Ljava/util/Calendar;Z)V
    .locals 1
    .param p1, "pPrimaryDevice"    # J
    .param p3, "hardwareId"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "host"    # Ljava/lang/String;
    .param p7, "service"    # Ljava/lang/String;
    .param p8, "status"    # I
    .param p9, "type"    # I
    .param p10, "flags"    # I
    .param p11, "lastUpdated"    # Ljava/util/Calendar;
    .param p12, "lastConnected"    # Ljava/util/Calendar;
    .param p13, "isRemembered"    # Z

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->_pPrimaryDevice:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 40
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->hardwareId:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->id:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->name:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->host:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->service:Ljava/lang/String;

    .line 45
    invoke-static {p8}, Lcom/microsoft/xbox/smartglass/DeviceStatus;->fromInt(I)Lcom/microsoft/xbox/smartglass/DeviceStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    .line 46
    invoke-static {p9}, Lcom/microsoft/xbox/smartglass/DeviceType;->fromInt(I)Lcom/microsoft/xbox/smartglass/DeviceType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->type:Lcom/microsoft/xbox/smartglass/DeviceType;

    .line 47
    invoke-static {p10}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->fromInt(I)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->flags:Ljava/util/EnumSet;

    .line 48
    iput-object p11, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->lastUpdated:Ljava/util/Calendar;

    .line 49
    iput-object p12, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->lastConnected:Ljava/util/Calendar;

    .line 50
    iput-boolean p13, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->isRemembered:Z

    .line 51
    return-void
.end method


# virtual methods
.method getPrimaryDevice()Lcom/microsoft/xbox/smartglass/RefTPtr;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->_pPrimaryDevice:Lcom/microsoft/xbox/smartglass/RefTPtr;

    return-object v0
.end method
