.class public interface abstract Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;
.super Ljava/lang/Object;
.source "RetrofitFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getRetrofit()Lretrofit2/Retrofit;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/mock/BehaviorDelegate",
            "<TT;>;)TT;"
        }
    .end annotation
.end method
