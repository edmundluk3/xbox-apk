.class public Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PopularWithFriendsScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
        ">;"
    }
.end annotation


# static fields
.field private static friendsPlayed:Ljava/lang/String;

.field private static friendsPlaying:Ljava/lang/String;

.field private static oneFriendPlayed:Ljava/lang/String;

.field private static oneFriendPlaying:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 24
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a64

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->friendsPlayed:Ljava/lang/String;

    .line 25
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a63

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->oneFriendPlayed:Ljava/lang/String;

    .line 26
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a62

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->friendsPlaying:Ljava/lang/String;

    .line 27
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a61

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->oneFriendPlaying:Ljava/lang/String;

    .line 28
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->friendsPlaying:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->friendsPlayed:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->oneFriendPlaying:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->oneFriendPlayed:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 34
    if-nez p2, :cond_1

    .line 35
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 36
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f0301d6

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 38
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;-><init>(Landroid/view/View;)V

    .line 39
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 44
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .line 45
    .local v0, "game":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->updateContent(Lcom/microsoft/xbox/service/network/managers/PopularGame;)V

    .line 49
    :cond_0
    return-object p2

    .line 41
    .end local v0    # "game":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    .end local v1    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;

    .restart local v1    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;
    goto :goto_0
.end method
