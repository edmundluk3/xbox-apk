.class Lcom/microsoft/xbox/smartglass/controls/JsonEnvironmentSettings;
.super Lorg/json/JSONObject;
.source "JsonEnvironmentSettings.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/EnvironmentSettings;)V
    .locals 2
    .param p1, "settings"    # Lcom/microsoft/xbox/smartglass/EnvironmentSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 14
    const-string v0, "Environment"

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->currentEnvironment:Lcom/microsoft/xbox/smartglass/Environment;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/Environment;->getValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonEnvironmentSettings;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 15
    const-string v0, "XliveAudienceUri"

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->xliveAudienceUri:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonEnvironmentSettings;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 16
    return-void
.end method
