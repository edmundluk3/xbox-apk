.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;
.source "AutoValue_ClubSearchDataTypes_ClubSearchResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "displayImageUrl"    # Ljava/lang/String;
    .param p5, "score"    # Ljava/lang/Float;
    .param p8, "preferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p6, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p7, "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p8}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    .line 21
    return-void
.end method
