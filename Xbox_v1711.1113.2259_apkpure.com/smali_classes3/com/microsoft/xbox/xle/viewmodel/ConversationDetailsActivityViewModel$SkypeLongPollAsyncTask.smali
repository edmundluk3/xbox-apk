.class Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkypeLongPollAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0

    .prologue
    .line 1135
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;

    .prologue
    .line 1135
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1139
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->skypeLongPoll()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1148
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1144
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1162
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 1167
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$900()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SkypeLongPollAsyncTask success"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 1174
    :goto_0
    return-void

    .line 1171
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$900()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SkypeLongPollAsyncTask failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1135
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1158
    return-void
.end method
