.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerSessionQueryResponseItem;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerSessionQueryResponseItem;
.source "AutoValue_MultiplayerDataTypes_MultiplayerSessionQueryResponseItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerSessionQueryResponseItem$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Date;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;)V
    .locals 0
    .param p1, "startTime"    # Ljava/util/Date;
    .param p2, "sessionRef"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;
    .param p3, "accepted"    # Ljava/lang/Long;
    .param p4, "visibility"    # Ljava/lang/String;
    .param p5, "joinRestriction"    # Ljava/lang/String;
    .param p6, "readRestriction"    # Ljava/lang/String;
    .param p7, "clubId"    # Ljava/lang/Long;
    .param p8, "firstMemberXuid"    # Ljava/lang/Long;
    .param p10, "status"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p9, "keywords":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p10}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerSessionQueryResponseItem;-><init>(Ljava/util/Date;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;)V

    .line 27
    return-void
.end method
