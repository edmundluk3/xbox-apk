.class public interface abstract Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
.super Ljava/lang/Object;
.source "EPGIteratorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IEPGFactoryListener"
.end annotation


# virtual methods
.method public abstract onDataChanged(Ljava/lang/String;II)V
.end method

.method public abstract onFavoritesError(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
.end method

.method public abstract onFetchingStatusChanged(Ljava/lang/String;)V
.end method
