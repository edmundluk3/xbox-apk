.class public Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
.super Ljava/lang/Object;
.source "TvHubTabletScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenCache"
.end annotation


# instance fields
.field private button:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private buttonUnderline:Landroid/view/View;

.field private screen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private screenClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Ljava/lang/Class;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->screenClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Ljava/lang/Class;)Ljava/lang/Class;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p1, "x1"    # Ljava/lang/Class;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->screenClass:Ljava/lang/Class;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->button:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->button:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->buttonUnderline:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->buttonUnderline:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->screen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->screen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object p1
.end method
