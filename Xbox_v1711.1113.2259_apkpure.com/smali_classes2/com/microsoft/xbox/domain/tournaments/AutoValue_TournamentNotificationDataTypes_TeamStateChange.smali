.class final Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TeamStateChange;
.super Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TeamStateChange;
.source "AutoValue_TournamentNotificationDataTypes_TeamStateChange.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TeamStateChange$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;J)V
    .locals 0
    .param p1, "newState"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;
    .param p2, "teamName"    # Ljava/lang/String;
    .param p3, "tournamentName"    # Ljava/lang/String;
    .param p4, "recipientXuid"    # Ljava/lang/String;
    .param p5, "teamId"    # Ljava/lang/String;
    .param p6, "tournamentImageUri"    # Ljava/lang/String;
    .param p7, "tournamentRef"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .param p8, "titleId"    # J

    .prologue
    .line 18
    invoke-direct/range {p0 .. p9}, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TeamStateChange;-><init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;J)V

    .line 19
    return-void
.end method
