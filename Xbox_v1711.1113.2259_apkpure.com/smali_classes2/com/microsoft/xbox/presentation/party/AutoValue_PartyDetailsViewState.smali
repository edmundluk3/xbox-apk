.class final Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;
.super Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
.source "AutoValue_PartyDetailsViewState.java"


# instance fields
.field private final error:Ljava/lang/Throwable;

.field private final isLoading:Z

.field private final memberStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field

.field private final partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

.field private final userAllowsBroadcasting:Z

.field private final userSummaries:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;ZLio/reactivex/Observable;ZLjava/lang/Throwable;)V
    .locals 0
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "userAllowsBroadcasting"    # Z
    .param p4    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "isLoading"    # Z
    .param p6, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;Z",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;Z",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    .local p4, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    .line 31
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userAllowsBroadcasting:Z

    .line 32
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    .line 33
    iput-boolean p5, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->isLoading:Z

    .line 34
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    if-ne p1, p0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v1

    .line 88
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 89
    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .line 90
    .local v0, "that":Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_4

    .line 91
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userAllowsBroadcasting:Z

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userAllowsBroadcasting()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    if-nez v3, :cond_5

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->isLoading:Z

    .line 94
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->isLoading()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    if-nez v3, :cond_6

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 90
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 91
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 93
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 95
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->error()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    :cond_7
    move v1, v2

    .line 97
    goto :goto_0
.end method

.method public error()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    const v5, 0xf4243

    .line 102
    const/4 v0, 0x1

    .line 103
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 105
    mul-int/2addr v0, v5

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v5

    .line 108
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userAllowsBroadcasting:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v5

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v5

    .line 112
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->isLoading:Z

    if-eqz v1, :cond_4

    :goto_4
    xor-int/2addr v0, v3

    .line 113
    mul-int/2addr v0, v5

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    if-nez v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 115
    return v0

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 108
    goto :goto_2

    .line 110
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v3, v4

    .line 112
    goto :goto_4

    .line 114
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->isLoading:Z

    return v0
.end method

.method public memberStream()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    return-object v0
.end method

.method public partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyDetailsViewState{partySession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userSummaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userAllowsBroadcasting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userAllowsBroadcasting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userAllowsBroadcasting()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userAllowsBroadcasting:Z

    return v0
.end method

.method public userSummaries()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
