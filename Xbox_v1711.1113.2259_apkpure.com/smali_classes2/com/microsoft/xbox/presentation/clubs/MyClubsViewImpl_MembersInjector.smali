.class public final Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;
.super Ljava/lang/Object;
.source "MyClubsViewImpl_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 17
    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "presenterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->presenter:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    .line 35
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    .prologue
    .line 26
    if-nez p1, :cond_0

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->presenter:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    .line 30
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->injectMembers(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V

    return-void
.end method
