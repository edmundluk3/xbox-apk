.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GcmNode"
.end annotation


# instance fields
.field private final path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;->path:Ljava/lang/String;

    .line 414
    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;->path:Ljava/lang/String;

    return-object v0
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 417
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;

    return-object v0
.end method
