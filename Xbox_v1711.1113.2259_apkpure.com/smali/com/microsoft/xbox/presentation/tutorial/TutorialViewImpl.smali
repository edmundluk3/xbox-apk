.class public Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "TutorialViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/tutorial/TutorialView;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field clubWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0afe
    .end annotation
.end field

.field facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0afc
    .end annotation
.end field

.field private onActivityResultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field redeemWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0afa
    .end annotation
.end field

.field setupWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0af9
    .end annotation
.end field

.field shopWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0afb
    .end annotation
.end field

.field suggestedFriendsWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0afd
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$1(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$2(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$3(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$4(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$5(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;

    return-object v0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    const-string v0, "Tutorial - Tutorial Page"

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 134
    const v0, 0x7f03023d

    return v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->presenter:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->onActivityResultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->with(IILandroid/content/Intent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 76
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;)V

    .line 77
    return-void
.end method

.method public onCreateContentView()V
    .locals 9

    .prologue
    .line 93
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 95
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->setupWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 96
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v3

    .line 98
    .local v3, "setupWelcomeCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->redeemWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 99
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 101
    .local v2, "redeemWelcomeCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->shopWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 102
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    .line 104
    .local v4, "storeWelcomeCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 105
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 107
    .local v1, "facebookWelcomeCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->suggestedFriendsWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 108
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v5

    .line 110
    .local v5, "suggestedFriendsCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->clubWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->getButtonTaps()Lio/reactivex/Observable;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v7

    .line 111
    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 113
    .local v0, "clubWelcomeCardClicked":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;>;"
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->onActivityResultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 116
    const/4 v6, 0x7

    new-array v6, v6, [Lio/reactivex/ObservableSource;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    const/4 v7, 0x2

    aput-object v4, v6, v7

    const/4 v7, 0x3

    aput-object v1, v6, v7

    const/4 v7, 0x4

    aput-object v5, v6, v7

    const/4 v7, 0x5

    aput-object v0, v6, v7

    const/4 v7, 0x6

    iget-object v8, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->onActivityResultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    aput-object v8, v6, v7

    invoke-static {v6}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 125
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;)V
    .locals 2
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isContent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isLinkedToFacebook()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->setupWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->setupWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->redeemWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->redeemWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->shopWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->shopWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->facebookWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->suggestedFriendsWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->clubWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->clubWelcomeCardCompleted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardCompleted(Z)V

    .line 158
    :goto_1
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_1

    .line 153
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_1

    .line 156
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown view state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->render(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<+",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
