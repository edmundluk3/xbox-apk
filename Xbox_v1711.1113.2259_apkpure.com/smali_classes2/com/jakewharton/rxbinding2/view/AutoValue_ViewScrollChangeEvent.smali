.class final Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;
.super Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;
.source "AutoValue_ViewScrollChangeEvent.java"


# instance fields
.field private final oldScrollX:I

.field private final oldScrollY:I

.field private final scrollX:I

.field private final scrollY:I

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;IIII)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scrollX"    # I
    .param p3, "scrollY"    # I
    .param p4, "oldScrollX"    # I
    .param p5, "oldScrollY"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;-><init>()V

    .line 21
    if-nez p1, :cond_0

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->view:Landroid/view/View;

    .line 25
    iput p2, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollX:I

    .line 26
    iput p3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollY:I

    .line 27
    iput p4, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollX:I

    .line 28
    iput p5, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollY:I

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 74
    check-cast v0, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;

    .line 75
    .local v0, "that":Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;
    iget-object v3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->view:Landroid/view/View;

    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->view()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollX:I

    .line 76
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->scrollX()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollY:I

    .line 77
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->scrollY()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollX:I

    .line 78
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->oldScrollX()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollY:I

    .line 79
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->oldScrollY()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;
    :cond_3
    move v1, v2

    .line 81
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 86
    const/4 v0, 0x1

    .line 87
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 88
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->view:Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 89
    mul-int/2addr v0, v2

    .line 90
    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollX:I

    xor-int/2addr v0, v1

    .line 91
    mul-int/2addr v0, v2

    .line 92
    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollY:I

    xor-int/2addr v0, v1

    .line 93
    mul-int/2addr v0, v2

    .line 94
    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollX:I

    xor-int/2addr v0, v1

    .line 95
    mul-int/2addr v0, v2

    .line 96
    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollY:I

    xor-int/2addr v0, v1

    .line 97
    return v0
.end method

.method public oldScrollX()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollX:I

    return v0
.end method

.method public oldScrollY()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollY:I

    return v0
.end method

.method public scrollX()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollX:I

    return v0
.end method

.method public scrollY()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollY:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewScrollChangeEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scrollX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scrollY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->scrollY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldScrollX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldScrollY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->oldScrollY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public view()Landroid/view/View;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewScrollChangeEvent;->view:Landroid/view/View;

    return-object v0
.end method
