.class public Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "XsapiSilentSignInViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private authStateDisposable:Lio/reactivex/disposables/Disposable;

.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private disableNavigationOnUpdate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->disableNavigationOnUpdate:Z

    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->ensureAdapter()V

    .line 33
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)V

    .line 34
    return-void
.end method

.method private ensureAdapter()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/XsapiSilentSignInAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/XsapiSilentSignInAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 47
    return-void
.end method

.method static synthetic lambda$onStartOverride$0(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;
    .param p1, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->disableNavigationOnUpdate:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onStartOverride$1(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;
    .param p1, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel$1;->$SwitchMap$com$microsoft$xbox$domain$auth$AuthState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/auth/AuthState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 62
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    .line 64
    .local v1, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->onSignInSuccess()V

    .line 68
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegIdOnLogin()V

    .line 70
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreenOrOOBETutorial()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to navigate to first screen after auth"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 79
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :pswitch_1
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->goToScreenWithPop(Ljava/lang/Class;)V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isBusy()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signInSilentlyXsapi()V

    .line 52
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->ensureAdapter()V

    .line 43
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->authStateDisposable:Lio/reactivex/disposables/Disposable;

    .line 83
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->authStateDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 88
    return-void
.end method

.method public setDisableNavigationOnUpdate(Z)V
    .locals 0
    .param p1, "shouldDisable"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;->disableNavigationOnUpdate:Z

    .line 38
    return-void
.end method
