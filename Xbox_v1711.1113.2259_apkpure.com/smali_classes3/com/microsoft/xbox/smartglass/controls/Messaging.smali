.class abstract Lcom/microsoft/xbox/smartglass/controls/Messaging;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Messaging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;
    }
.end annotation


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Messaging"

.field private static final SendMessageMethod:Ljava/lang/String; = "SendMessage"

.field private static s_messageCount:I


# instance fields
.field private final _listener:Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->s_messageCount:I

    return-void
.end method

.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 2
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 23
    const-string v0, "Messaging"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Messaging;Lcom/microsoft/xbox/smartglass/controls/Messaging$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_listener:Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_listener:Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 28
    const-string v0, "SendMessage"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->registerMethod(Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->dispose()V

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_listener:Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->removeListener(Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    const-string v0, "SendMessage"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->sendMessage(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract sendMessage(ILjava/lang/String;)V
.end method

.method protected sendMessage(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 52
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->failRequestValidation(I)V

    .line 68
    :goto_0
    return-void

    .line 58
    :cond_0
    sget v1, Lcom/microsoft/xbox/smartglass/controls/Messaging;->s_messageCount:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lcom/microsoft/xbox/smartglass/controls/Messaging;->s_messageCount:I

    rem-int/lit8 v1, v1, 0xa

    if-nez v1, :cond_1

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "SendMessage"

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendTitleMessage(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send title message. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
