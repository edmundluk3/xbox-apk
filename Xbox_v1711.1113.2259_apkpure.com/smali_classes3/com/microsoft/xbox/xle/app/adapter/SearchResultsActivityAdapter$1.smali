.class Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;
.super Ljava/lang/Object;
.source "SearchResultsActivityAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 50
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 51
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 52
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v3

    const-string v4, "Search"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    .line 56
    .local v1, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$3;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV3MediaType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 74
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 77
    .end local v1    # "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_0
    :goto_0
    return-void

    .line 60
    .restart local v1    # "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {v3, v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {v3, v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 70
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {v3, v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
