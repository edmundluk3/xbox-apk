.class public Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
.super Landroid/app/Service;
.source "HoverChatService.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_KEY:Ljava/lang/String; = "type"


# instance fields
.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isStoppingSelf:Z

.field private final keysToAddOnSignIn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation
.end field

.field private rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->onSignIn(Z)V

    return-void
.end method

.method public static addClubInstance(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "clubId"    # J

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "type"

    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->with(J)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 57
    return-void
.end method

.method private addKeyToRepository(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .prologue
    .line 142
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->addClubChatKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)V

    .line 149
    :goto_0
    return-void

    .line 144
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->addMessageKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V

    goto :goto_0

    .line 147
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "type"

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 63
    return-void
.end method

.method static synthetic lambda$onCreate$0(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreate$1(Lcom/microsoft/xbox/domain/auth/AuthState;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreate$2(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->stopSelf()V

    return-void
.end method

.method static synthetic lambda$onCreate$3(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreate$4(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->stopSelf()V

    return-void
.end method

.method private moveToForeground()V
    .locals 3

    .prologue
    .line 110
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v1, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x2

    .line 111
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 114
    .local v0, "notification":Landroid/app/Notification;
    const v1, 0x7fffffff

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->startForeground(ILandroid/app/Notification;)V

    .line 115
    return-void
.end method

.method private onSignIn(Z)V
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 152
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    monitor-enter v2

    .line 153
    if-eqz p1, :cond_0

    .line 154
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    const-string v3, "onSignIn: Success, adding keys"

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .line 156
    .local v0, "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addKeyToRepository(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    goto :goto_0

    .line 164
    .end local v0    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 159
    :cond_0
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    const-string v3, "onSignIn: Failed, stopping self"

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->stopSelf()V

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 164
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 67
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 69
    sget-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    const-string v1, "Creating service"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)V

    .line 74
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedOut()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    const-string v1, "Need to sign in first"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signInSilentlyXsapi()V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 81
    invoke-interface {v1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 82
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 83
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 84
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->onCreate()V

    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->moveToForeground()V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 92
    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getChatHeadManagerEvents()Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 93
    invoke-interface {v4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$AllChatHeadsRemovedEvent;

    .line 94
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 95
    invoke-interface {v4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 96
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 98
    invoke-interface {v2}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 99
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 100
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    aput-object v2, v1, v5

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    const-string v1, "Permission check failed, stopping self."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iput-boolean v5, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->isStoppingSelf:Z

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->stopSelf()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->onDestroy()V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 178
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->isStoppingSelf:Z

    .line 179
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 121
    .local v0, "extras":Landroid/os/Bundle;
    :goto_0
    iget-boolean v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->isStoppingSelf:Z

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    .line 122
    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .line 124
    .local v1, "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v2}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: adding key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addKeyToRepository(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    .line 138
    .end local v1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :goto_1
    const/4 v2, 0x2

    return v2

    .line 119
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 128
    .restart local v0    # "extras":Landroid/os/Bundle;
    .restart local v1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: waiting for sign in to add key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    monitor-enter v3

    .line 131
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->keysToAddOnSignIn:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 135
    .end local v1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: Ignoring intent. this.isStoppingSelf: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->isStoppingSelf:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " extras: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
