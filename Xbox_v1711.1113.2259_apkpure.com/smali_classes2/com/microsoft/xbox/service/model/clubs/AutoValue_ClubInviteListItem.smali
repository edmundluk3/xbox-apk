.class final Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;
.super Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
.source "AutoValue_ClubInviteListItem.java"


# instance fields
.field private final actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field private final club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

.field private final ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 2
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p2, "actorSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "ownerSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;-><init>()V

    .line 20
    if-nez p1, :cond_0

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null club"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 24
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 25
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 26
    return-void
.end method


# virtual methods
.method protected actorSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method protected club()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    .line 61
    .local v0, "that":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->club()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v3, :cond_3

    .line 62
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->actorSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v3, :cond_4

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->ownerSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 62
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->actorSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 63
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->ownerSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    :cond_5
    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 70
    const/4 v0, 0x1

    .line 71
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 73
    mul-int/2addr v0, v3

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 75
    mul-int/2addr v0, v3

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 77
    return v0

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->hashCode()I

    move-result v1

    goto :goto_0

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method protected ownerSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubInviteListItem{club="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actorSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ownerSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubInviteListItem;->ownerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
