.class public abstract Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
.super Ljava/lang/Object;
.source "ActivityFeedDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PinsInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract maxPins()I
.end method

.method public abstract numItems()I
.end method
