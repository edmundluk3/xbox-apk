.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;
.super Ljava/lang/Object;
.source "UTCFriends.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackFindFacebookFriendsClicked()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 88
    return-void
.end method

.method public static trackFindPhoneContactsClicked()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 76
    return-void
.end method

.method public static trackFriendsFilterAction(Lcom/microsoft/xbox/service/model/FollowersFilter;)V
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$1;-><init>(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 30
    return-void
.end method

.method public static trackNavigateToProfile(Ljava/lang/String;)V
    .locals 1
    .param p0, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$3;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 64
    return-void
.end method

.method public static trackSeeAllClicked()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 100
    return-void
.end method

.method public static trackSuggestionsFilterAction(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;)V
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 47
    return-void
.end method
