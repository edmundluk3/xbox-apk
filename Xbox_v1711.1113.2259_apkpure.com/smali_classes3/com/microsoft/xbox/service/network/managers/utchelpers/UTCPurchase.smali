.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;
.super Ljava/lang/Object;
.source "UTCPurchase.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 101
    return-void
.end method

.method public static trackAttemptToInstallView()V
    .locals 1

    .prologue
    .line 324
    const-string v0, "Purchase - Attempt to Install"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method public static trackFilter(Ljava/lang/String;)V
    .locals 1
    .param p0, "filter"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 104
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$2;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 116
    return-void
.end method

.method public static trackInstallToXbox(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 1
    .param p0, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 139
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$4;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 203
    return-void
.end method

.method public static trackRedeemCode()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 130
    return-void
.end method

.method public static trackWebBlend(Ljava/lang/String;JLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
    .param p1, "elapsedSeconds"    # J
    .param p3, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 214
    if-nez p3, :cond_0

    .line 250
    :goto_0
    return-void

    .line 219
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;

    invoke-direct {v0, p3, p1, p2, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$5;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    goto :goto_0
.end method

.method public static trackWebBlendLoadError(ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 1
    .param p0, "errorCode"    # I
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "failingUrl"    # Ljava/lang/String;
    .param p3, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 262
    if-nez p3, :cond_0

    .line 321
    :goto_0
    return-void

    .line 267
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;

    invoke-direct {v0, p3, p1, p2, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    goto :goto_0
.end method
