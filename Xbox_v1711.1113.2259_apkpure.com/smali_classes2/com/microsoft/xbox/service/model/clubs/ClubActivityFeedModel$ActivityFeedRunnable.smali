.class Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubActivityFeedModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityFeedRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final continuationToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 126
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    .line 127
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->access$100(Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getClubActivityFeed(JLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 148
    const-wide/16 v0, 0xc85

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 144
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->access$002(Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;Z)Z

    .line 134
    :cond_0
    return-void
.end method
