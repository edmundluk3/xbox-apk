.class public Lcom/microsoft/xbox/service/model/ActivityDetailModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "ActivityDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_ACTIVITY_DETAIL_MODELS:I = 0xa

.field private static identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/ActivityDetailModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 2
    .param p1, "preloadData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;-><init>(Lcom/microsoft/xbox/service/model/ActivityDetailModel;Lcom/microsoft/xbox/service/model/ActivityDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 38
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->lifetime:J

    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 43
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/ActivityDetailModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    return-object v0
.end method

.method public static getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    .locals 5
    .param p0, "preloadData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p1, "parentMediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 77
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 80
    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    .line 82
    .local v0, "model":Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 84
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 87
    :cond_0
    return-object v0

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    :cond_1
    move v1, v3

    .line 77
    goto :goto_0

    :cond_2
    move v2, v3

    .line 78
    goto :goto_1
.end method

.method public static final reset()V
    .locals 3

    .prologue
    .line 108
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 109
    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/ActivityDetailModel;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->clearObservers()V

    goto :goto_1

    .line 108
    .end local v0    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/ActivityDetailModel;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 113
    .restart local v0    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/ActivityDetailModel;>;"
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;-><init>(I)V

    sput-object v1, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;

    .line 114
    return-void
.end method


# virtual methods
.method public getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    return-object v0
.end method

.method public getDefaultParentalRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentalRatings()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentalRatings()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentalRatings()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    .line 99
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getRatingDescriptors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 53
    return-void
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 61
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 64
    .local v1, "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getAllowedTitleIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setAllowedTitleIds(Ljava/util/ArrayList;)V

    .line 67
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 68
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->addAllowedTitleId(I)V

    goto :goto_0

    .line 70
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 73
    .end local v1    # "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->ActivityDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-direct {v2, v3, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 74
    return-void
.end method
