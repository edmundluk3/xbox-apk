.class public Lcom/microsoft/xbox/toolkit/ui/PageIndicator;
.super Landroid/widget/RelativeLayout;
.source "PageIndicator.java"


# instance fields
.field private activePageDrawableID:I

.field private currentPageIndex:I

.field private images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private inactivePageDrawableID:I

.field private initialized:Z

.field private linearLayout:Landroid/widget/LinearLayout;

.field private totalPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 47
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 39
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    .line 40
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    .line 42
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialized:Z

    .line 43
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    .line 44
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialize()V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    .line 40
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    .line 42
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialized:Z

    .line 43
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    .line 44
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialize()V

    .line 56
    return-void
.end method

.method private initialize()V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 59
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->removeAllViews()V

    .line 64
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 65
    .local v0, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->resetPageDrawableIDs()V

    .line 70
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialized:Z

    .line 71
    return-void
.end method

.method private resetPageDrawableIDs()V
    .locals 1

    .prologue
    .line 74
    const v0, 0x7f020068

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    .line 75
    const v0, 0x7f02012e

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    .line 76
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 158
    const v0, 0x7f0e005b

    return v0
.end method

.method public isLayoutRequested()Z
    .locals 3

    .prologue
    .line 163
    invoke-super {p0}, Landroid/widget/RelativeLayout;->isLayoutRequested()Z

    move-result v0

    .line 165
    .local v0, "override":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    const-string v1, "PageIndicator"

    const-string v2, "App bar animating. Overriding isLayoutRequested to false"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    .line 170
    :cond_0
    return v0
.end method

.method public setActivePageDrawableID(I)V
    .locals 0
    .param p1, "drawableID"    # I

    .prologue
    .line 79
    if-ltz p1, :cond_0

    .line 80
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    .line 82
    :cond_0
    return-void
.end method

.method public setCurrentPage(I)V
    .locals 3
    .param p1, "currentPageIndex"    # I

    .prologue
    const/4 v1, 0x1

    .line 141
    const-string v2, "Page index should be positive."

    if-ltz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v2, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 144
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    .line 145
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    if-ltz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    .line 147
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    invoke-static {v0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setPageState(II)V

    .line 149
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    .line 152
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->activePageDrawableID:I

    invoke-static {v0, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setPageState(II)V

    .line 154
    :cond_1
    return-void

    .line 141
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInactivePageDrawableID(I)V
    .locals 0
    .param p1, "drawableID"    # I

    .prologue
    .line 85
    if-ltz p1, :cond_0

    .line 86
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    .line 88
    :cond_0
    return-void
.end method

.method public setIndicatorBackground(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 94
    :cond_0
    return-void
.end method

.method public setTotalPageCount(I)V
    .locals 11
    .param p1, "pageCount"    # I

    .prologue
    const v10, 0x7f0900f0

    const/4 v9, 0x0

    const/4 v8, -0x1

    const v7, 0x7f0900ef

    .line 97
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialized:Z

    if-nez v3, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->initialize()V

    .line 101
    :cond_0
    if-nez p1, :cond_1

    .line 102
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 103
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    .line 104
    iput v9, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    .line 105
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 106
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setVisibility(I)V

    .line 138
    :goto_0
    return-void

    .line 108
    :cond_1
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    if-eq v3, p1, :cond_3

    .line 109
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    .line 110
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    invoke-static {v3}, Lcom/microsoft/xle/test/interop/TestInterop;->setTotalPageCount(I)V

    .line 111
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 114
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 115
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->totalPageCount:I

    new-array v3, v3, [Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 117
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;)V

    .line 118
    .local v1, "image":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 119
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 120
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 121
    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 122
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 123
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 120
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 125
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    .line 126
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 128
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->inactivePageDrawableID:I

    invoke-static {v0, v3}, Lcom/microsoft/xle/test/interop/TestInterop;->setPageState(II)V

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->images:[Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    aput-object v1, v3, v0

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 134
    .end local v0    # "i":I
    .end local v1    # "image":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->currentPageIndex:I

    .line 136
    :cond_3
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->setVisibility(I)V

    goto :goto_0
.end method
