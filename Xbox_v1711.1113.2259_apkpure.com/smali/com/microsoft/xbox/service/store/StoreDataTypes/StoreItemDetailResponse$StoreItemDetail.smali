.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreItemDetail"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;
    }
.end annotation


# static fields
.field private static final FULL_SKU_TYPE:Ljava/lang/String; = "Full"

.field public static final LEGACY_XBOX_TITLEID_KEY:Ljava/lang/String; = "LegacyXboxProductId"

.field public static final XBOX_TITLEID_KEY:Ljava/lang/String; = "XboxTitleId"


# instance fields
.field private final alternateIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AlternateIds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;",
            ">;"
        }
    .end annotation
.end field

.field private applicationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final displaySkuAvailabilities:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DisplaySkuAvailabilities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;",
            ">;"
        }
    .end annotation
.end field

.field private final localizedProperties:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocalizedProperties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;",
            ">;"
        }
    .end annotation
.end field

.field private lowestRankDisplaySkuAvailability:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

.field private final marketProperties:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MarketProperties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;",
            ">;"
        }
    .end annotation
.end field

.field public final productFamily:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductFamily"
    .end annotation
.end field

.field public final productId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductId"
    .end annotation
.end field

.field public final productKind:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductKind"
    .end annotation
.end field

.field public final productType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductType"
    .end annotation
.end field

.field public final properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Properties"
    .end annotation
.end field

.field private templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "productFamily"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p7, "productKind"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "productType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "properties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "alternateIds":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;>;"
    .local p2, "displaySkuAvailabilities":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;>;"
    .local p3, "localizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;>;"
    .local p4, "marketProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 258
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 260
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    .line 261
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    .line 262
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->localizedProperties:Ljava/util/List;

    .line 263
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->marketProperties:Ljava/util/List;

    .line 264
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productFamily:Ljava/lang/String;

    .line 265
    iput-object p6, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    .line 266
    iput-object p7, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productKind:Ljava/lang/String;

    .line 267
    iput-object p8, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productType:Ljava/lang/String;

    .line 268
    iput-object p9, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    .line 269
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Browse:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    .line 270
    return-void
.end method

.method private findLowestRankSkuId()V
    .locals 5

    .prologue
    .line 444
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 445
    const v0, 0x7fffffff

    .line 446
    .local v0, "currentLowestRank":I
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 447
    .local v1, "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    iget v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->skuDisplayRank:I

    if-gt v3, v0, :cond_0

    const-string v3, "Full"

    iget-object v4, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuType:Ljava/lang/String;

    .line 451
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 452
    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->lowestRankDisplaySkuAvailability:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 453
    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    iget v0, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->skuDisplayRank:I

    goto :goto_0

    .line 457
    .end local v0    # "currentLowestRank":I
    .end local v1    # "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    :cond_1
    return-void
.end method

.method private hasSkuSupportsXbox()Z
    .locals 8

    .prologue
    .line 427
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 428
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 429
    .local v0, "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    if-eqz v4, :cond_0

    .line 430
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->getPackages()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;

    .line 431
    .local v2, "skuPropertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->getPlatformDependencies()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;

    .line 432
    .local v1, "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->platformName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 433
    const/4 v3, 0x1

    .line 440
    .end local v0    # "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    .end local v1    # "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    .end local v2    # "skuPropertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    :goto_0
    return v3

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 274
    if-ne p1, p0, :cond_0

    .line 275
    const/4 v1, 0x1

    .line 280
    :goto_0
    return v1

    .line 276
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    if-nez v1, :cond_1

    .line 277
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 279
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 280
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAlternateIds()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationIdForPackage()Ljava/lang/String;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 407
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->applicationId:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 408
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 409
    .local v0, "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    if-eqz v4, :cond_0

    .line 410
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->getPackages()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;

    .line 411
    .local v2, "skuPropertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->getPlatformDependencies()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;

    .line 412
    .local v1, "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->platformName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 413
    invoke-static {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->access$100(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 414
    invoke-static {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->access$100(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackageApplication;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackageApplication;->applicationId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->applicationId:Ljava/lang/String;

    .line 415
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->applicationId:Ljava/lang/String;

    .line 423
    .end local v0    # "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    .end local v1    # "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    .end local v2    # "skuPropertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    :goto_0
    return-object v3

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->applicationId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getBoxArt()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->getImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;

    .line 189
    .local v0, "image":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;
    const-string v2, "BoxArt"

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->imagePurpose:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->getValidImageUriString()Ljava/lang/String;

    move-result-object v1

    .line 195
    .end local v0    # "image":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisplaySkuAvailabilities()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->displaySkuAvailabilities:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLegacyXboxTitleIdKey()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 376
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 377
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;

    .line 378
    .local v0, "altId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    const-string v3, "LegacyXboxProductId"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 379
    iget-object v1, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    .line 383
    .end local v0    # "altId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLocalizedProperties()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->localizedProperties:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperties()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperties()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    .line 337
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLowestRankDisplaySkuAvailability()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->lowestRankDisplaySkuAvailability:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    if-nez v0, :cond_0

    .line 351
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->findLowestRankSkuId()V

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->lowestRankDisplaySkuAvailability:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    return-object v0
.end method

.method public getMarketProperties()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->marketProperties:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMarketProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getMarketProperties()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getMarketProperties()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;

    .line 345
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->productTitle:Ljava/lang/String;

    .line 180
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getProductTypeString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->fromBigCatTypeString(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v0

    return-object v0
.end method

.method public getProductTypeString()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productType:Ljava/lang/String;

    .line 323
    :goto_0
    return-object v0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productKind:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productKind:Ljava/lang/String;

    goto :goto_0

    .line 323
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getScid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTemplateType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    if-nez v0, :cond_0

    .line 389
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Browse:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    return-object v0
.end method

.method public getTitleId()J
    .locals 4

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getAlternateIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;

    .line 160
    .local v0, "alternateId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    if-eqz v0, :cond_0

    const-string v2, "XboxTitleId"

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v1, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 165
    .end local v0    # "alternateId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    :goto_0
    return-wide v2

    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getXboxTitleId()Ljava/lang/Long;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 358
    const/4 v2, 0x0

    .line 359
    .local v2, "titleId":Ljava/lang/Long;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 360
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->alternateIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;

    .line 361
    .local v0, "altId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    const-string v5, "XboxTitleId"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 363
    :try_start_0
    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 371
    .end local v0    # "altId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    :cond_1
    return-object v2

    .line 365
    .restart local v0    # "altId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    :catch_0
    move-exception v1

    .line 366
    .local v1, "ex":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to parse AlternateId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 286
    const/16 v0, 0x11

    .line 287
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 288
    return v0
.end method

.method public isXboxProduct()Z
    .locals 1

    .prologue
    .line 402
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->hasSkuSupportsXbox()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLegacyXboxTitleIdKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTemplateType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)V
    .locals 0
    .param p1, "templateType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->templateType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    .line 397
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
