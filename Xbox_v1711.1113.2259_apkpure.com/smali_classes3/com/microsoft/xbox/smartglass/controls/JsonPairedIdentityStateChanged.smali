.class Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;
.super Lorg/json/JSONObject;
.source "JsonPairedIdentityStateChanged.java"


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isIdentityPaired"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 12
    const-string v0, "isIdentityPaired"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 13
    return-void
.end method
