.class public final Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;
.super Ljava/lang/Object;
.source "UserPostsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;,
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;,
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;,
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;,
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;,
        Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static canUserPinPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z
    .locals 12
    .param p0, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 47
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    .line 50
    .local v4, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-nez p1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v5

    .line 54
    :cond_1
    sget-object v8, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$1;->$SwitchMap$com$microsoft$xbox$service$activityFeed$UserPostsDataTypes$TimelineType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    goto :goto_0

    .line 57
    :pswitch_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 58
    .local v0, "clubId":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_0

    .line 59
    sget-object v8, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v1

    .line 60
    .local v1, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsAdminOf()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 61
    const/4 v5, 0x1

    goto :goto_0

    .line 68
    .end local v0    # "clubId":Ljava/lang/Long;
    .end local v1    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :pswitch_1
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->isCommunityManager()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 69
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 70
    .local v6, "timelineTitleId":J
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getManagingTitleIds()Ljava/util/List;

    move-result-object v3

    .line 72
    .local v3, "managingTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 73
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_0

    .line 80
    .end local v3    # "managingTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v6    # "timelineTitleId":J
    :pswitch_2
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->isCommunityManager()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 81
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getManagingPageIds()Ljava/util/List;

    move-result-object v2

    .line 83
    .local v2, "managingPageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 84
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_0

    .line 90
    .end local v2    # "managingPageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_3
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v5

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
