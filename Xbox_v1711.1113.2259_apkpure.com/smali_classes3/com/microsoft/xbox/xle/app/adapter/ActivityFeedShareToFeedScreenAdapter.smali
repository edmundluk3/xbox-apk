.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ActivityFeedShareToFeedScreenAdapter.java"


# static fields
.field private static final LAYOUT_HEIGHT_DIFF_DP:F = 260.0f


# instance fields
.field private final closeButton:Landroid/widget/Button;

.field private contentLoaded:Z

.field private final counter:Landroid/widget/TextView;

.field private final discardButton:Landroid/widget/Button;

.field private final error:Landroid/widget/Button;

.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final itemContainer:Landroid/view/ViewGroup;

.field private final layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final maxCount:I

.field private final pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final scrollContainer:Landroid/widget/ScrollView;

.field private final shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final textEntry:Landroid/widget/EditText;

.field private final textWatcher:Landroid/text/TextWatcher;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

.field private final xleRootView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->contentLoaded:Z

    .line 166
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    .line 182
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    .line 48
    const v0, 0x7f0e0155

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->xleRootView:Landroid/view/View;

    .line 49
    const v0, 0x7f0e0159

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->scrollContainer:Landroid/widget/ScrollView;

    .line 50
    const v0, 0x7f0e015a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->itemContainer:Landroid/view/ViewGroup;

    .line 51
    const v0, 0x7f0e015b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textEntry:Landroid/widget/EditText;

    .line 52
    const v0, 0x7f0e015e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 53
    const v0, 0x7f0e0157

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->closeButton:Landroid/widget/Button;

    .line 54
    const v0, 0x7f0e0160

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 55
    const v0, 0x7f0e0161

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->discardButton:Landroid/widget/Button;

    .line 56
    const v0, 0x7f0e015d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->counter:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0e0162

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->error:Landroid/widget/Button;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->maxCount:I

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->contentLoaded:Z

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->submitShare()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->updateCountAndPostButton()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->xleRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->scrollContainer:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private submitShare()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "pin":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->isChecked()Z

    move-result v0

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->checkPinAndSubmitShare(Z)V

    .line 164
    return-void
.end method

.method private updateCountAndPostButton()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getCaption()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->counter:Landroid/widget/TextView;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%d/%d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->maxCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->counter:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070d71

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->maxCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->contentLoaded:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->canShare()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 155
    return-void

    :cond_0
    move v1, v2

    .line 154
    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getCaption()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->closeButton:Landroid/widget/Button;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->discardButton:Landroid/widget/Button;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->discardButton:Landroid/widget/Button;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->xleRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 138
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->discardButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    return-void
.end method

.method protected updateViewOverride()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 63
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isBusy()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->updateLoadingIndicator(Z)V

    .line 65
    const v4, 0x7f0e0158

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getActivityFeedHeaderStringId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 67
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 68
    .local v1, "newItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eq v1, v4, :cond_0

    .line 69
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 71
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->itemContainer:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v4, v7}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->createAndBindFeedItemViewForSharing(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 72
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->contentLoaded:Z

    .line 73
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 75
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getErrorStringResId()I

    move-result v0

    .line 76
    .local v0, "errorResId":I
    if-nez v0, :cond_2

    const/4 v2, 0x0

    .line 77
    .local v2, "strError":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->error:Landroid/widget/Button;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v5

    :goto_1
    invoke-static {v7, v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 79
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getLaunchContextTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v3

    .line 81
    .local v3, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getLaunchContextTimelineId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;->canUserPinPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 82
    sget-object v4, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter$6;->$SwitchMap$com$microsoft$xbox$service$activityFeed$UserPostsDataTypes$TimelineType:[I

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 96
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {v4, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 103
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->updateCountAndPostButton()V

    .line 104
    return-void

    .line 76
    .end local v2    # "strError":Ljava/lang/String;
    .end local v3    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->error:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .restart local v2    # "strError":Ljava/lang/String;
    :cond_3
    move v4, v6

    .line 77
    goto :goto_1

    .line 84
    .restart local v3    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v4, :cond_1

    .line 85
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700e2

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setVisibility(I)V

    goto :goto_2

    .line 90
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v4, :cond_1

    .line 91
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700e5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setVisibility(I)V

    goto :goto_2

    .line 100
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;->pinCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {v4, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
