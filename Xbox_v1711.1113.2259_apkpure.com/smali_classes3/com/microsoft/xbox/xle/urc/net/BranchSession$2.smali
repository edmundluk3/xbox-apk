.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 377
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 378
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$302(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .line 380
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$200(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .line 381
    .local v0, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    goto :goto_0

    .line 383
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->hasSettingDiff(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 384
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->val$_headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$302(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .line 386
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$200(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .line 387
    .restart local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    goto :goto_1

    .line 390
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    :cond_2
    return-void
.end method
