.class Lcom/microsoft/xbox/xle/ui/SearchBarView$1;
.super Ljava/lang/Object;
.source "SearchBarView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/SearchBarView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/SearchBarView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$000(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$000(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$100(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$100(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$100(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$100(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 83
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$200(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$200(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;->onQueryTextChange(Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    return-void
.end method
