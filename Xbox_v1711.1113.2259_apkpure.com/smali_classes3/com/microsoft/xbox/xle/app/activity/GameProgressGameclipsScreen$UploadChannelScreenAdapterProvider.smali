.class public Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen$UploadChannelScreenAdapterProvider;
.super Ljava/lang/Object;
.source "GameProgressGameclipsScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UploadChannelScreenAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen$UploadChannelScreenAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameProgressGameclipsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    .line 62
    .local v0, "uploadChannelViewModel":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProgressGameclipsAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
