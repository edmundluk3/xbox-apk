.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "HorizontalViewScroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyLayoutParams"
.end annotation


# instance fields
.field public start:I

.field public temporary:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 785
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 786
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 777
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 778
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0
    .param p1, "p"    # Landroid/view/ViewGroup$MarginLayoutParams;

    .prologue
    .line 781
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 782
    return-void
.end method


# virtual methods
.method public end()I
    .locals 2

    .prologue
    .line 792
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->start:I

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->width:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$MyLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method
