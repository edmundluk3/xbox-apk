.class public abstract Lcom/microsoft/xbox/toolkit/XLEObservable;
.super Ljava/lang/Object;
.source "XLEObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final data:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/toolkit/EquatableWeakRef",
            "<",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<TT;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/toolkit/XLEObservable;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/XLEObservable;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLEObservable;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add, There are "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " observers "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    new-instance v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized clearObservers()V
    .locals 3

    .prologue
    .line 62
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLEObservable;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clear, There are "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " observers."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized getObservers()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 72
    .local v1, "observers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    .line 73
    .local v2, "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 75
    .local v0, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    if-eqz v0, :cond_0

    .line 76
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    .end local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .end local v1    # "observers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    .end local v2    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 78
    .restart local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .restart local v1    # "observers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    .restart local v2    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82
    .end local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .end local v2    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    .line 48
    .local v1, "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 50
    .local v0, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    if-eqz v0, :cond_0

    .line 51
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/XLEObserver;->update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 47
    .end local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .end local v1    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 53
    .restart local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .restart local v1    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56
    .end local v0    # "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    .end local v1    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;>;"
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEObservable;, "Lcom/microsoft/xbox/toolkit/XLEObservable<TT;>;"
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<TT;>;"
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLEObservable;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remove, There are "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " observers "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEObservable;->data:Ljava/util/Set;

    new-instance v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
