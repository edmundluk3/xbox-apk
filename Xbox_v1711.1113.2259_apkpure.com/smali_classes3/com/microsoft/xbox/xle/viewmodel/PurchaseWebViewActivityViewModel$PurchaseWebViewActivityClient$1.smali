.class Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient$1;
.super Landroid/webkit/WebChromeClient;
.source "PurchaseWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->Start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient$1;->this$1:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 3
    .param p1, "cm"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 340
    const-string v0, "purchaseWebview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- From line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->sourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const/4 v0, 0x1

    return v0
.end method
