.class Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
.super Ljava/lang/Object;
.source "FeaturedList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Item"
.end annotation


# instance fields
.field private id:J

.field private position:I

.field private view:Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->view:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->view:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->position:I

    return v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .param p1, "x1"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->position:I

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->id:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .param p1, "x1"    # J

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->id:J

    return-wide p1
.end method
