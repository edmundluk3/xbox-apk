.class final Lcom/microsoft/xbox/xle/app/LaunchUtils$6;
.super Ljava/lang/Object;
.source "LaunchUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeCompanion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$deepLink:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$packageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$deepLink:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 327
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissAppBar()V

    .line 329
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$packageName:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$packageName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 330
    :cond_0
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$deepLink:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 331
    .local v3, "launchNativeIntent":Landroid/content/Intent;
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$200(Landroid/content/Intent;)V

    .line 353
    .end local v3    # "launchNativeIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 335
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 336
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 338
    .local v0, "app_installed":Z
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$packageName:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    const/4 v0, 0x1

    .line 344
    :goto_1
    if-nez v0, :cond_2

    .line 346
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "market://details?id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 347
    .local v2, "goToMarket":Landroid/content/Intent;
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$200(Landroid/content/Intent;)V

    goto :goto_0

    .line 340
    .end local v2    # "goToMarket":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 341
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    goto :goto_1

    .line 350
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;->val$deepLink:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 351
    .restart local v3    # "launchNativeIntent":Landroid/content/Intent;
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$200(Landroid/content/Intent;)V

    goto :goto_0
.end method
