.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "InitialLoadResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    .locals 1
    .param p0    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;"
        }
    .end annotation

    .prologue
    .line 261
    .local p0, "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    .local p1, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 262
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 263
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;-><init>(Lio/reactivex/Observable;Lio/reactivex/Observable;)V

    return-object v0
.end method


# virtual methods
.method public abstract memberStream()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end method

.method public abstract textStream()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    const-string v0, "InitialLoad"

    return-object v0
.end method
