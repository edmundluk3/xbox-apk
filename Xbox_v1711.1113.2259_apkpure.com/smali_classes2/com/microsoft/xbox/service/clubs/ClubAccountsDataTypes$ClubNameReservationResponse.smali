.class public abstract Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubNameReservationResponse"
.end annotation


# static fields
.field public static final NAME_NOT_AVAILABLE_ERROR_CODE:I = 0x3f2

.field public static final NAME_TOO_LONG_ERROR_CODE:I = 0x3ed

.field public static final NON_ENGLISH_CHAR_ERROR_CODE:I = 0x3ef

.field public static final NOT_LETTER_START_ERROR_CODE:I = 0x3ee

.field public static final SPACE_IN_FRONT_ERROR_CODE:I = 0x3f0

.field public static final TOO_MANY_SPACES_ERROR_CODE:I = 0x3f0

.field public static final VETTING_FAILURE_ERROR_CODE:I = 0x3ff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newErrorResponse(ILjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 6
    .param p0, "code"    # I
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 140
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;

    move v1, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newSuccessResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "owner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "expires"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 146
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 148
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubNameReservationResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract code()I
.end method

.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract expires()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->code()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract owner()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
