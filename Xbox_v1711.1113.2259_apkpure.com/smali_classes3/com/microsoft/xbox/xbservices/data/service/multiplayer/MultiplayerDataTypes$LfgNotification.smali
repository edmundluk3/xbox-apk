.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotification;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LfgNotification"
.end annotation


# static fields
.field public static final transient IMMINENT_EXPIRED_TYPE:Ljava/lang/String; = "imminentLfgExpired"

.field public static final transient SCHEDULED_EXPIRED_TYPE:Ljava/lang/String; = "scheduledLfgExpired"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1541
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_LfgNotification$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_LfgNotification$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public getSearchHandle()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotification;->lfg_notification()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotificationBody;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotificationBody;->searchHandle()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;

    move-result-object v0

    return-object v0
.end method

.method public abstract lfg_notification()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotificationBody;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
