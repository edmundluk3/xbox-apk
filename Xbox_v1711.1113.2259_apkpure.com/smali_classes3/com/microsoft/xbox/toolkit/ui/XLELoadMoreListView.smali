.class public Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEListView;
.source "XLELoadMoreListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;
    }
.end annotation


# instance fields
.field private footer:Landroid/view/View;

.field private footerLayout:Landroid/view/View;

.field private isLoadingMore:Z

.field private loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->init(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 32
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->isLoadingMore:Z

    .line 33
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 34
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030162

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footerLayout:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footerLayout:Landroid/view/View;

    const v2, 0x7f0e077e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footer:Landroid/view/View;

    .line 36
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footer:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footerLayout:Landroid/view/View;

    invoke-virtual {p0, v1, v4, v3}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 38
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 39
    return-void
.end method


# virtual methods
.method public onLoadMoreFinished()V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->isLoadingMore:Z

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 66
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 47
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->getLastVisiblePosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    if-nez p2, :cond_1

    .line 52
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->isLoadingMore:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;->isNeedLoadMore()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->footer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->isLoadingMore:Z

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;->loadMore()V

    .line 61
    :cond_1
    return-void
.end method

.method public setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;)V
    .locals 0
    .param p1, "loadMoreListener"    # Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLELoadMoreListView$LoadMoreListener;

    .line 43
    return-void
.end method
