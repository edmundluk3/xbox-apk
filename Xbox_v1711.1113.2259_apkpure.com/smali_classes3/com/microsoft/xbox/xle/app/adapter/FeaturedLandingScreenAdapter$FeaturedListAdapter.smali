.class Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FeaturedLandingScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FeaturedListAdapter"
.end annotation


# instance fields
.field itemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 92
    return-void
.end method

.method private bindImage(Landroid/view/View;II)V
    .locals 10
    .param p1, "cell"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "width"    # I

    .prologue
    const/4 v9, 0x0

    .line 135
    if-ltz p2, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt p2, v7, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 140
    .local v2, "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemRelatedAspectXY(I)[I

    move-result-object v1

    .line 141
    .local v1, "arBuf":[I
    aget v7, v1, v9

    int-to-float v7, v7

    const/4 v8, 0x1

    aget v8, v1, v8

    int-to-float v8, v8

    div-float v0, v7, v8

    .line 142
    .local v0, "ar":F
    int-to-float v7, p3

    mul-float/2addr v7, v0

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 144
    .local v4, "height":I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 145
    .local v6, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v6, :cond_2

    .line 146
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    .end local v6    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v6, p3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 151
    .restart local v6    # "params":Landroid/view/ViewGroup$LayoutParams;
    :goto_1
    invoke-virtual {p1, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v3

    .local v3, "defaultRid":I
    move-object v5, p1

    .line 157
    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 158
    .local v5, "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 159
    invoke-virtual {v5, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    goto :goto_0

    .line 148
    .end local v3    # "defaultRid":I
    .end local v5    # "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_2
    iput p3, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 149
    iput v4, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 116
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 121
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    .line 122
    .local v1, "parentWidth":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;->cellMargin:I

    sub-int v3, v1, v3

    div-int/lit8 v0, v3, 0x2

    .local v0, "imageWidth":I
    move-object v2, p2

    .line 124
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 125
    .local v2, "view":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    if-nez v2, :cond_0

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030101

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .end local v2    # "view":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 129
    .restart local v2    # "view":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_0
    invoke-direct {p0, v2, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->bindImage(Landroid/view/View;II)V

    .line 131
    return-object v2
.end method

.method public update(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "itemsList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    if-eq v0, p1, :cond_0

    .line 97
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->itemsList:Ljava/util/List;

    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter$FeaturedListAdapter;->notifyDataSetChanged()V

    .line 100
    :cond_0
    return-void
.end method
