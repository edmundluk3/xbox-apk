.class public final Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
.super Ljava/lang/Object;
.source "RxSharedPreferences.java"


# static fields
.field private static final DEFAULT_BOOLEAN:Ljava/lang/Boolean;

.field private static final DEFAULT_FLOAT:Ljava/lang/Float;

.field private static final DEFAULT_INTEGER:Ljava/lang/Integer;

.field private static final DEFAULT_LONG:Ljava/lang/Long;


# instance fields
.field private final keyChanges:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_FLOAT:Ljava/lang/Float;

    .line 22
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_INTEGER:Ljava/lang/Integer;

    .line 23
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_BOOLEAN:Ljava/lang/Boolean;

    .line 24
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_LONG:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    .line 38
    new-instance v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;

    invoke-direct {v0, p0, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;-><init>(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Landroid/content/SharedPreferences;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    .line 56
    return-void
.end method

.method public static create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 1
    .param p0, "preferences"    # Landroid/content/SharedPreferences;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    const-string v0, "preferences == null"

    invoke-static {p0, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-direct {v0, p0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_BOOLEAN:Ljava/lang/Boolean;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "defaultValue"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/BooleanAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/BooleanAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getEnum(Ljava/lang/String;Ljava/lang/Class;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 75
    .local p2, "enumClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getEnum(Ljava/lang/String;Ljava/lang/Enum;Ljava/lang/Class;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getEnum(Ljava/lang/String;Ljava/lang/Enum;Ljava/lang/Class;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Enum;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "TT;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    .local p2, "defaultValue":Ljava/lang/Enum;, "TT;"
    .local p3, "enumClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const-string v0, "enumClass == null"

    invoke-static {p3, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v4, Lcom/f2prateek/rx/preferences2/EnumAdapter;

    invoke-direct {v4, p3}, Lcom/f2prateek/rx/preferences2/EnumAdapter;-><init>(Ljava/lang/Class;)V

    .line 85
    .local v4, "adapter":Lcom/f2prateek/rx/preferences2/Preference$Adapter;, "Lcom/f2prateek/rx/preferences2/Preference$Adapter<TT;>;"
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_FLOAT:Ljava/lang/Float;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getFloat(Ljava/lang/String;Ljava/lang/Float;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;Ljava/lang/Float;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "defaultValue"    # Ljava/lang/Float;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/FloatAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/FloatAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getInteger(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_INTEGER:Ljava/lang/Integer;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getInteger(Ljava/lang/String;Ljava/lang/Integer;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getInteger(Ljava/lang/String;Ljava/lang/Integer;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "defaultValue"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/IntegerAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/IntegerAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getLong(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_LONG:Ljava/lang/Long;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "defaultValue"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/LongAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/LongAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getObject(Ljava/lang/String;Lcom/f2prateek/rx/preferences2/Preference$Adapter;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference$Adapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/f2prateek/rx/preferences2/Preference$Adapter",
            "<TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 132
    .local p2, "adapter":Lcom/f2prateek/rx/preferences2/Preference$Adapter;, "Lcom/f2prateek/rx/preferences2/Preference$Adapter<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference$Adapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/f2prateek/rx/preferences2/Preference$Adapter",
            "<TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 141
    .local p2, "defaultValue":Ljava/lang/Object;, "TT;"
    .local p3, "adapter":Lcom/f2prateek/rx/preferences2/Preference$Adapter;, "Lcom/f2prateek/rx/preferences2/Preference$Adapter<TT;>;"
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    const-string v0, "adapter == null"

    invoke-static {p3, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getString(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "defaultValue"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/StringAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/StringAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 163
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/f2prateek/rx/preferences2/Preference",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 171
    .local p2, "defaultValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v0, "key == null"

    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/f2prateek/rx/preferences2/StringSetAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/StringSetAdapter;

    iget-object v5, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method
