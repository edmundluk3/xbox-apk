.class public Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;
.super Landroid/support/v7/util/DiffUtil$Callback;
.source "ListOfImmutablesDiffCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v7/util/DiffUtil$Callback;"
    }
.end annotation


# instance fields
.field private newItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private oldItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+TT;>;",
            "Ljava/util/List",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;, "Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback<TT;>;"
    .local p1, "oldItems":Ljava/util/List;, "Ljava/util/List<+TT;>;"
    .local p2, "newItems":Ljava/util/List;, "Ljava/util/List<+TT;>;"
    invoke-direct {p0}, Landroid/support/v7/util/DiffUtil$Callback;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 17
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->oldItems:Ljava/util/List;

    .line 20
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->newItems:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1
    .param p1, "oldItemPosition"    # I
    .param p2, "newItemPosition"    # I

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;, "Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public areItemsTheSame(II)Z
    .locals 2
    .param p1, "oldItemPosition"    # I
    .param p2, "newItemPosition"    # I

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;, "Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->oldItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->newItems:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getNewListSize()I
    .locals 1

    .prologue
    .line 30
    .local p0, "this":Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;, "Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->newItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .prologue
    .line 25
    .local p0, "this":Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;, "Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;->oldItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
