.class Lcom/microsoft/xbox/service/model/SessionModel$8;
.super Ljava/lang/Object;
.source "SessionModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/SessionModel;->launchUriOnXboxOne(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/SessionModel;

.field final synthetic val$location:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

.field final synthetic val$postAction:Ljava/lang/Runnable;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/SessionModel;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/SessionModel;

    .prologue
    .line 392
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$uri:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$location:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    iput-object p4, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$postAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 396
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->access$300(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$uri:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$location:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->launchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Z

    move-result v0

    .line 397
    .local v0, "launchCommandSent":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$postAction:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel$8;->val$postAction:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 400
    :cond_0
    return-void
.end method
