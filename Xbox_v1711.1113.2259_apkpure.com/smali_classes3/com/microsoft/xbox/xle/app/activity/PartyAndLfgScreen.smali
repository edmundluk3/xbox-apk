.class public Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PartyAndLfgScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string v0, "LFG - Listing"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;->onCreateContentView()V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 15
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f0301a0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;->setContentView(I)V

    .line 25
    return-void
.end method
