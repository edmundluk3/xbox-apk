.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;-><init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$46;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    check-cast p1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$5(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)V

    return-void
.end method
