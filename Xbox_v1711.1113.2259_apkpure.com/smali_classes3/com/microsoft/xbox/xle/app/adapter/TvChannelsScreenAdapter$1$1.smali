.class Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;
.super Ljava/lang/Object;
.source "TvChannelsScreenAdapter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;

.field final synthetic val$provider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;->val$provider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;->val$provider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Provider Change"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;->val$provider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v0, 0x1

    return v0
.end method
