.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatManageNotifications(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 0

    .prologue
    .line 686
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 5

    .prologue
    .line 689
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 690
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v0

    .line 692
    .local v0, "clubId":J
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 693
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v3, "ClubId"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 695
    const-string v3, "Clubs - Chat Manage Notifications"

    invoke-static {v3, v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 696
    return-void
.end method
