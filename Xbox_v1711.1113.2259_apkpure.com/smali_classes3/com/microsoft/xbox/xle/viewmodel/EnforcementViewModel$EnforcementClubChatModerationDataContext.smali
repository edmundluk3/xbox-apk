.class public Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;
.super Ljava/lang/Object;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnforcementClubChatModerationDataContext"
.end annotation


# instance fields
.field public channelId:Ljava/lang/String;

.field public channelType:Ljava/lang/String;

.field public messageId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1, "channelType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "channelId"    # Ljava/lang/String;
    .param p3, "messageId"    # J

    .prologue
    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 472
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;->channelType:Ljava/lang/String;

    .line 473
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;->channelId:Ljava/lang/String;

    .line 474
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;->messageId:Ljava/lang/String;

    .line 475
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
