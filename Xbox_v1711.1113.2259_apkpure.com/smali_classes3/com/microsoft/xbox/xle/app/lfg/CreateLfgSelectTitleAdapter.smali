.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "CreateLfgSelectTitleAdapter.java"


# instance fields
.field private isTypingSearch:Z

.field private recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

.field private recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

.field private searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

.field private searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    .line 43
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0ae2

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0add

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 46
    .local v0, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    .line 53
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0ae3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 54
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 56
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/PopularNowListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0301d5

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/PopularNowListAdapter;-><init>(Landroid/app/Activity;I)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    .line 57
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0adf

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 72
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0ae1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    const v3, 0x7f0e0ade

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .line 80
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    if-eqz v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    const v3, 0x7f0e09c9

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 82
    .local v1, "searchInputEditView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 85
    .end local v1    # "searchInputEditView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 112
    const v2, 0x7f0e0ae0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->isTypingSearch:Z

    return p1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->onTapCloseButton()V

    .line 48
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
    .param p1, "title"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getScid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->selectTitle(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    .local v0, "searchSuggestion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackSearchGame(Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setSearchTag(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->loadSearchResultsAsync(Ljava/lang/String;)V

    .line 69
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->isTypingSearch:Z

    .line 70
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;
    .param p1, "title"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getScid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->selectTitle(JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 119
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchResults()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 121
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->clear()V

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchResultTitleInfoList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->addAll(Ljava/util/Collection;)V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->notifyDataSetChanged()V

    .line 136
    :goto_0
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->isTypingSearch:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    .line 137
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    .line 138
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 140
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 141
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 142
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    .line 144
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;

    .line 145
    .local v1, "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 146
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    .line 147
    .local v0, "product":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 128
    .end local v0    # "product":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    .end local v1    # "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchResultsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 129
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->clear()V

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getTitleInfoList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->addAll(Ljava/util/Collection;)V

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->recentlyPlayedAdapter:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 152
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->searchSuggestionsList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 154
    :cond_3
    return-void
.end method
