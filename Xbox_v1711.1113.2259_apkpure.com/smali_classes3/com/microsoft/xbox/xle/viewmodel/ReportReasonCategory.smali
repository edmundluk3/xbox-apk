.class public final enum Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;
.super Ljava/lang/Enum;
.source "ReportReasonCategory.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum BioLoc:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum Cheating:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum CommsTextMessageBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum HostInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum InappropriateComment:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum InappropriateFeedItem:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum InappropriateGameClip:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum InappropriateGamerTag:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum InappropriateScreenshot:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum Message:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum PlayerName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum PlayerPic:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum QuitEarly:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum Spam:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum Unsporting:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentActivityFeedBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentClubBackground:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentClubChat:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentClubDescription:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentClubLogo:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentClubName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserContentCommentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum UserInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field public static final enum VoiceComm:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;


# instance fields
.field private final feedbackType:Ljava/lang/String;

.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v1, "BioLoc"

    const v3, 0x7f070a6f

    const-string v4, "UserContentPersonalInfo"

    const-string v5, "UserContentPersonalInfo"

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->BioLoc:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 15
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "Cheating"

    const v6, 0x7f070a70

    const-string v7, "FairPlayCheater"

    const-string v8, "FairPlayCheater"

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Cheating:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 16
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "PlayerName"

    const v6, 0x7f070a7d

    const-string v7, "UserContentRealName"

    const-string v8, "UserContentRealName"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 17
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "PlayerPic"

    const v6, 0x7f070a7e

    const-string v7, "UserContentGamerpic"

    const-string v8, "UserContentGamerpic"

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerPic:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 18
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "QuitEarly"

    const v6, 0x7f070a7f

    const-string v7, "FairPlayQuitter"

    const-string v8, "FairPlayQuitter"

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->QuitEarly:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 19
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "Unsporting"

    const/4 v5, 0x5

    const v6, 0x7f070a88

    const-string v7, "FairplayUnsporting"

    const-string v8, "FairplayUnsporting"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Unsporting:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 20
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "VoiceComm"

    const/4 v5, 0x6

    const v6, 0x7f070a89

    const-string v7, "CommsAbusiveVoice"

    const-string v8, "CommsAbusiveVoice"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->VoiceComm:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 21
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "Message"

    const/4 v5, 0x7

    const v6, 0x7f070a7b

    const-string v7, "CommsTextMessage"

    const-string v8, "CommsTextMessage"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Message:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 22
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "Spam"

    const/16 v5, 0x8

    const v6, 0x7f070a83

    const-string v7, "CommsSpam"

    const-string v8, "CommsSpam"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Spam:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 23
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "InappropriateGamerTag"

    const/16 v5, 0x9

    const v6, 0x7f070a78

    const-string v7, "UserContentGamertag"

    const-string v8, "UserContentGamertag"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGamerTag:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 24
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "InappropriateFeedItem"

    const/16 v5, 0xa

    const v6, 0x7f070a76

    const-string v7, "UserContentActivityFeed"

    const-string v8, "UserContentActivityFeed"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateFeedItem:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 25
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "InappropriateComment"

    const/16 v5, 0xb

    const v6, 0x7f070a75

    const-string v7, "UserContentComment"

    const-string v8, "UserContentComment"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateComment:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 26
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "InappropriateGameClip"

    const/16 v5, 0xc

    const v6, 0x7f070a77

    const-string v7, "UserContentGameDVR"

    const-string v8, "UserContentGameDVR"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGameClip:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 27
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "InappropriateScreenshot"

    const/16 v5, 0xd

    const v6, 0x7f070a79

    const-string v7, "UserContentScreenshot"

    const-string v8, "UserContentScreenshot"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateScreenshot:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 28
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserInappropriateMessage"

    const/16 v5, 0xe

    const v6, 0x7f070a6c

    const-string v7, "UserContentLFGApplication"

    const-string v8, "UserContentLFGApplication"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 29
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "HostInappropriateMessage"

    const/16 v5, 0xf

    const v6, 0x7f070a6b

    const-string v7, "UserContentInappropriateLFG"

    const-string v8, "UserContentInappropriateLFG"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->HostInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 30
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentClubChat"

    const/16 v5, 0x10

    const v6, 0x7f070284

    const-string v7, "UserContentClubChat"

    const-string v8, "UserContentClubChat"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubChat:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 31
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentClubLogo"

    const/16 v5, 0x11

    const v6, 0x7f07033c

    const-string v7, "UserContentClubLogo"

    const-string v8, "UserContentClubLogo"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubLogo:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 32
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentClubBackground"

    const/16 v5, 0x12

    const v6, 0x7f070336

    const-string v7, "UserContentClubBackground"

    const-string v8, "UserContentClubBackground"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubBackground:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 33
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentClubName"

    const/16 v5, 0x13

    const v6, 0x7f07033a

    const-string v7, "UserContentClubName"

    const-string v8, "UserContentClubName"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 34
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentClubDescription"

    const/16 v5, 0x14

    const v6, 0x7f070338

    const-string v7, "UserContentClubDescription"

    const-string v8, "UserContentClubDescription"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubDescription:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 35
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "CommsTextMessageBreaksClubRules"

    const/16 v5, 0x15

    const v6, 0x7f070a7c

    const-string v7, "CommsTextMessageBreaksClubRules"

    const-string v8, "CommsTextMessageBreaksClubRules"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->CommsTextMessageBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 36
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentBreaksClubRules"

    const/16 v5, 0x16

    const v6, 0x7f070a72

    const-string v7, "UserContentBreaksClubRules"

    const-string v8, "UserContentBreaksClubRules"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 37
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentCommentBreaksClubRules"

    const/16 v5, 0x17

    const v6, 0x7f070a71

    const-string v7, "UserContentCommentBreaksClubRules"

    const-string v8, "UserContentCommentBreaksClubRules"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentCommentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 38
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    const-string v4, "UserContentActivityFeedBreaksClubRules"

    const/16 v5, 0x18

    const v6, 0x7f070a6e

    const-string v7, "UserContentActivityFeedBreaksClubRules"

    const-string v8, "UserContentActivityFeedBreaksClubRules"

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentActivityFeedBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    .line 13
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->BioLoc:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Cheating:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v1, v0, v9

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v1, v0, v10

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerPic:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v1, v0, v11

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->QuitEarly:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Unsporting:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->VoiceComm:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Message:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Spam:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGamerTag:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateFeedItem:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateComment:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGameClip:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateScreenshot:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->HostInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubChat:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubLogo:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubBackground:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubDescription:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->CommsTextMessageBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentCommentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentActivityFeedBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "feedbackType"    # Ljava/lang/String;
    .param p5, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 47
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 49
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->resId:I

    .line 50
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->feedbackType:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->telemetryName:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFeedbackType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->feedbackType:Ljava/lang/String;

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
