.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;
.super Ljava/lang/Object;
.source "UTCLauncher.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fillState:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

.field final synthetic val$titleId:J


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
    .locals 1

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;->val$titleId:J

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;->val$fillState:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 60
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;->val$titleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    const-string v1, "FillState"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;->val$fillState:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    const-string v1, "Launcher - Play to Xbox"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 64
    return-void
.end method
