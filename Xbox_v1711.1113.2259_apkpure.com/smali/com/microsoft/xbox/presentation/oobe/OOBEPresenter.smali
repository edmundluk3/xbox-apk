.class public Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "OOBEPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 6
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p5, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p6, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 71
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    .line 72
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p6

    move-object v4, p4

    move-object v5, p2

    .line 74
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 146
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object v0

    return-object v0
.end method

.method private codeEntryStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 4
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 184
    instance-of v2, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    if-eqz v2, :cond_1

    move-object v1, p2

    .line 185
    check-cast v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    .line 186
    .local v1, "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->settings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->withSettings(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    move-result-object p1

    .line 202
    .end local v1    # "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    :cond_0
    :goto_0
    return-object p1

    .line 187
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    :cond_1
    instance-of v2, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    if-eqz v2, :cond_0

    move-object v0, p2

    .line 188
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    .line 189
    .local v0, "enterCodeResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    sget-object v2, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$1;->$SwitchMap$com$microsoft$xbox$domain$oobe$OOBEInteractor$EnterCodeResult:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 191
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowTimeZonePage()V

    .line 192
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    move-result-object p1

    goto :goto_0

    .line 194
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackSaveSettingsFailure()V

    .line 195
    invoke-static {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->withFatalError(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    move-result-object p1

    goto :goto_0

    .line 197
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackInvalidSetupCode()V

    .line 198
    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->withBadCode(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    move-result-object p1

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private completionStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 0
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 257
    return-object p1
.end method

.method static synthetic lambda$initialSetup$26(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 157
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$25(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
    .param p2, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p5, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p6, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 75
    invoke-static/range {p0 .. p5}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p6, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 145
    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->defaultPrefs()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "initialLoadIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    .locals 5
    .param p0, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p1, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .param p2, "enterCodeIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    .line 84
    invoke-virtual {p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;->code()Ljava/lang/String;

    move-result-object v3

    .line 85
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyEmail()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 86
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyGamertag()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 87
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyGamerpicUrl()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 88
    invoke-interface {p1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isNewUser()Z

    move-result v4

    .line 83
    invoke-static {v3, v0, v1, v2, v4}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;
    .locals 1
    .param p0, "autoAppUpdatesChangedIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;->enabled()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;->with(Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "autoAppUpdatesChangedIntent"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoAppUpdatesChangedAction;->enabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackAppUpdateAction(Z)V

    return-void
.end method

.method static synthetic lambda$null$12(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;
    .locals 1
    .param p0, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;

    return-object v0
.end method

.method static synthetic lambda$null$13(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "ignored"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackBackButtonAction()V

    return-void
.end method

.method static synthetic lambda$null$14(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;
    .locals 1
    .param p0, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    return-object v0
.end method

.method static synthetic lambda$null$15(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "ignored"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackNextButtonAction()V

    return-void
.end method

.method static synthetic lambda$null$16(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    .locals 4
    .param p0, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p1, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .param p2, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    .line 126
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyEmail()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyGamertag()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 128
    invoke-interface {p0}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyGamerpicUrl()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 129
    invoke-interface {p1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isNewUser()Z

    move-result v3

    .line 125
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$17(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "ignored"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackNextButtonAction()V

    return-void
.end method

.method static synthetic lambda$null$18(Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;)V
    .locals 0
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p1, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;->navigateBack()V

    return-void
.end method

.method static synthetic lambda$null$19(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$OkResult;
    .locals 1
    .param p0, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$OkResult;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$OkResult;

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .locals 1
    .param p0, "timeZoneChangedIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;->timeZone()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$20(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;
    .locals 1
    .param p0, "ignored"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingAction;

    return-object v0
.end method

.method static synthetic lambda$null$21(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;)Z
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;->CONSOLE_COMPLETE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$22(Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;)V
    .locals 0
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p1, "ignored"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$PingResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;->navigateBack()V

    return-void
.end method

.method static synthetic lambda$null$23(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
    .param p2, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p5, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p6, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    const/16 v0, 0xc

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 78
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 79
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;

    .line 82
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2, p3}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 83
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getEnterCodeTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$TimeZoneChangedIntent;

    .line 91
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 92
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 93
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 94
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getTimeZoneChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;

    .line 96
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 97
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 98
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getDstOptionChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;

    .line 101
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 102
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 103
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 104
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getInstantOnChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;

    .line 106
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 108
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 109
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getAutoSystemUpdatesChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;

    .line 111
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$18;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 112
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 113
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 114
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getAutoAppUpdatesChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;

    .line 116
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$20;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 117
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 118
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;

    .line 120
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$22;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 121
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$23;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 122
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    .line 124
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2, p3}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 125
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$25;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 130
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getFinishTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;

    .line 133
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 134
    invoke-interface {p4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p5}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$26;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 135
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$27;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 136
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;

    .line 138
    invoke-virtual {p6, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$28;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 139
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 140
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->getPingTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$29;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 141
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 142
    invoke-interface {p4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p5}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$30;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 143
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 77
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$24(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
    .param p2, "profileProvider"    # Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p5, "navigator"    # Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;
    .param p6, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {p6}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 76
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static/range {p0 .. p5}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "ignored"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackTimeZoneAction()V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;
    .locals 1
    .param p0, "dstOptionChangedIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;->enabled()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;->with(Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "dstOptionChangedIntent"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$DSTOptionChangedAction;->enabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackDSTAction(Z)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;
    .locals 1
    .param p0, "instantOnChangedIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;->enabled()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;->with(Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "instantOnChangedIntent"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;->enabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackInstantOnAction(Z)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .locals 1
    .param p0, "autoSystemUpdatesChangedIntent"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;->enabled()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;->with(Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .param p1, "autoSystemUpdatesChangedIntent"    # Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;->enabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackConsoleUpdateAction(Z)V

    return-void
.end method

.method private powerSettingsStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 2
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 221
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 222
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    .line 223
    .local v0, "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->settings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    move-result-object p1

    .line 232
    .end local v0    # "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;
    :cond_0
    :goto_0
    return-object p1

    .line 224
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;
    :cond_1
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;

    if-eqz v1, :cond_2

    .line 225
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowTimeZonePage()V

    .line 226
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    move-result-object p1

    goto :goto_0

    .line 227
    :cond_2
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    if-eqz v1, :cond_0

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowUpdateSettingsPage()V

    .line 229
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    move-result-object p1

    goto :goto_0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 1
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 168
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    if-eqz v0, :cond_1

    .line 169
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->codeEntryStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object p1

    .line 180
    :cond_0
    :goto_0
    return-object p1

    .line 170
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    if-eqz v0, :cond_2

    .line 171
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->timeZoneStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object p1

    goto :goto_0

    .line 172
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_2
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    if-eqz v0, :cond_3

    .line 173
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->powerSettingsStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object p1

    goto :goto_0

    .line 174
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_3
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    if-eqz v0, :cond_4

    .line 175
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->updateSettingsStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object p1

    goto :goto_0

    .line 176
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_4
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    if-eqz v0, :cond_0

    .line 177
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->completionStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    move-result-object p1

    goto :goto_0
.end method

.method private timeZoneStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 2
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 206
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 207
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    .line 208
    .local v0, "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->settings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    move-result-object p1

    .line 217
    .end local v0    # "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;
    :cond_0
    :goto_0
    return-object p1

    .line 209
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;
    :cond_1
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;

    if-eqz v1, :cond_2

    .line 210
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowEnterCodePage()V

    .line 211
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    move-result-object p1

    goto :goto_0

    .line 212
    :cond_2
    instance-of v1, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowPowerSettingsPage()V

    .line 214
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    move-result-object p1

    goto :goto_0
.end method

.method private updateSettingsStateReducer(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    .locals 3
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 236
    instance-of v2, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    if-eqz v2, :cond_1

    move-object v1, p2

    .line 237
    check-cast v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    .line 238
    .local v1, "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;->settings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    move-result-object p1

    .line 253
    .end local v1    # "updateSettingsResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    :cond_0
    :goto_0
    return-object p1

    .line 239
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    :cond_1
    instance-of v2, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$BackResult;

    if-eqz v2, :cond_2

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowPowerSettingsPage()V

    .line 241
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    move-result-object p1

    goto :goto_0

    .line 242
    :cond_2
    instance-of v2, p2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;

    if-eqz v2, :cond_0

    move-object v0, p2

    .line 243
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;

    .line 244
    .local v0, "finishResult":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;
    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishResult;

    if-ne v0, v2, :cond_3

    .line 245
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowCompletionPage()V

    .line 246
    sget-object p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    goto :goto_0

    .line 248
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackSaveSettingsFailure()V

    .line 249
    invoke-static {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->withFatalError(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackShowEnterCodePage()V

    .line 152
    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 153
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 154
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 155
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 157
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 158
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 160
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 161
    return-void
.end method

.method public trackClose()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->telemetryService:Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;->trackCloseButtonAction()V

    .line 165
    return-void
.end method
