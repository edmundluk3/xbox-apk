.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupMember"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;
    }
.end annotation


# instance fields
.field public gamerPicUrl:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field protected isDummy:Z

.field protected itemDummyType:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

.field public realName:Ljava/lang/String;

.field private final role:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

.field public senderGamerTag:Ljava/lang/String;

.field public status:Lcom/microsoft/xbox/service/model/UserStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "role"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy:Z

    .line 73
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->id:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->role:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    .line 75
    return-void
.end method

.method public constructor <init>(ZLcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;)V
    .locals 2
    .param p1, "isDummy"    # Z
    .param p2, "type"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    .prologue
    .line 81
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V

    .line 82
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy:Z

    .line 83
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->itemDummyType:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    .line 85
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getItemDummyType()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->itemDummyType:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    return-object v0
.end method

.method public getType()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->role:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Member:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    .line 105
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;->Dummy:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$GroupMemberType;

    goto :goto_0
.end method

.method public isDummy()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->isDummy:Z

    return v0
.end method
