.class public Lcom/microsoft/xbox/toolkit/TimeSample;
.super Ljava/lang/Object;
.source "TimeSample.java"


# instance fields
.field public finish:J

.field public start:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->start:J

    .line 9
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    .line 10
    return-void
.end method


# virtual methods
.method public getElapsed()J
    .locals 4

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 14
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    .line 16
    :cond_0
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->start:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public setFinished()V
    .locals 10

    .prologue
    .line 20
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    .line 21
    const-string v0, "XLETEST"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Elapsed:%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->finish:J

    iget-wide v8, p0, Lcom/microsoft/xbox/toolkit/TimeSample;->start:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    return-void
.end method
