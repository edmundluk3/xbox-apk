.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubMemberPresence"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile transient lastSeenDate:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1222
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    .locals 2
    .param p0, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "lastSeenTimestamp"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "lastSeenState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1214
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1215
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1216
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1218
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence;-><init>(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;)I
    .locals 2
    .param p1, "another"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1240
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1241
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->getLastSeenDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->getLastSeenDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1199
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->compareTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;)I

    move-result v0

    return v0
.end method

.method public getLastSeenDate()Ljava/util/Date;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1227
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1228
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenDate:Ljava/util/Date;

    .line 1229
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1230
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse last seen timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenTimestamp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenDate:Ljava/util/Date;

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenDate:Ljava/util/Date;

    return-object v0
.end method

.method public abstract lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract lastSeenTimestamp()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract xuid()J
.end method
