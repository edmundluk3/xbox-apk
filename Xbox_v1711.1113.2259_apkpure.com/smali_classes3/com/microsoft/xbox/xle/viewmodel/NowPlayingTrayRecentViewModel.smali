.class public Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
.source "NowPlayingTrayRecentViewModel.java"


# instance fields
.field private recent:Lcom/microsoft/xbox/service/model/recents/Recent;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/service/model/recents/Recent;)V
    .locals 0
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
    .param p2, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p3, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    .line 20
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 21
    return-void
.end method


# virtual methods
.method public getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-object v0
.end method

.method protected getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/Recent;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getNowPlayingProviderOrArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNowPlayingSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->SubTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNowPlayingTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public launchHelp()V
    .locals 4

    .prologue
    .line 42
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissAppBar()V

    .line 43
    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayRecentViewModel;->getNowPlayingTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method protected shouldObserveNowPlayingGlobalModel()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method
