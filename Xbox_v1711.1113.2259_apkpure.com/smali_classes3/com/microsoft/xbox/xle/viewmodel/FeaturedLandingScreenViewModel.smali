.class public Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "FeaturedLandingScreenViewModel.java"


# instance fields
.field private dataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 39
    return-void
.end method

.method private createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/FeaturedLandingScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;)V

    return-object v0
.end method

.method private onLoadFeaturedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 155
    const-string v0, "FeaturedLandingScreen"

    const-string v1, "onLoad featured completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 159
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 164
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->updateAdapter()V

    .line 165
    return-void

    .line 161
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public gotoDetails(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 118
    const/16 v0, 0xa

    .line 119
    .local v0, "featuredStoreIndex":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, p1, :cond_0

    .line 120
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->dataList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 121
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "pageUri":Ljava/lang/String;
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 125
    .local v3, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 126
    .local v5, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    .line 127
    .local v1, "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    const/16 v6, 0xa

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putStoreFilterPosition(I)V

    .line 130
    const-string v6, "Store"

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 133
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .end local v3    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 134
    .restart local v3    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v6, "MediaId"

    iget-object v7, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 135
    const-string v6, "ListIndex"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 136
    const-string v6, "MediaType"

    const-string v7, "Featured"

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 137
    const-string v6, "Store - View Store Item"

    invoke-static {v6, v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 138
    invoke-virtual {p0, v2, v5}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 140
    .end local v1    # "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v4    # "pageUri":Ljava/lang/String;
    .end local v5    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public gotoRedeemCode()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 143
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 145
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackRedeemCode()V

    .line 149
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->increment()Ljava/lang/String;

    .line 151
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/PurchaseWebViewActivity;

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 152
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 93
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->loadDiscoverList(Z)V

    .line 95
    return-void
.end method

.method public navigateToSearchScreen()V
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/SearchScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 115
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 63
    return-void
.end method

.method public onSetActive()V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const-string v0, "Store - Featured"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 102
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetActive()V

    .line 103
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isFeaturedBlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 58
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_1

    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->createScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 51
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getFeaturedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 52
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 57
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    goto :goto_0

    .line 54
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 68
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 73
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v0, v1, :cond_1

    .line 74
    const-string v1, "FeaturedLandingScreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown update type ignore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;->onLoadFeaturedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method
