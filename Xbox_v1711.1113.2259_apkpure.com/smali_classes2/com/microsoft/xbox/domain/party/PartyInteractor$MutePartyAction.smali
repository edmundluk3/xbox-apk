.class public final enum Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
.super Ljava/lang/Enum;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MutePartyAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;",
        ">;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 512
    new-instance v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    .line 511
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    sget-object v1, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->INSTANCE:Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->$VALUES:[Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    .line 514
    const-class v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 511
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 511
    const-class v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
    .locals 1

    .prologue
    .line 511
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->$VALUES:[Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;->TAG:Ljava/lang/String;

    return-object v0
.end method
