.class public final enum Lcom/microsoft/xbox/smartglass/ControlKey;
.super Ljava/lang/Enum;
.source "ControlKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/ControlKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Asterisk:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Back:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum BrowserBack:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum BrowserRefresh:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum BrowserStop:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Display:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Down:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum DvdMenu:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Empty:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Escape:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum FastForward:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Guide:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Info:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Left:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum LeftTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum MediaCenter:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num0:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num1:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num2:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num3:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num4:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num5:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num6:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num7:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num8:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Num9:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum PadA:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum PadB:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum PadX:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum PadY:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Pause:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Play:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Pound:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Record:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Replay:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Return:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Rewind:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Right:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum RightTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Skip:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Start:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Stop:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Title:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum Up:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum VolumeDown:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum VolumeMute:Lcom/microsoft/xbox/smartglass/ControlKey;

.field public static final enum VolumeUp:Lcom/microsoft/xbox/smartglass/ControlKey;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/ControlKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0x1b

    const/16 v7, 0x13

    const/16 v6, 0xd

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Empty"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Empty:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Return"

    invoke-direct {v2, v3, v9, v6}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Return:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Pause"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4, v7}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Pause:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Escape"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v8}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Escape:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num0"

    const/4 v4, 0x4

    const/16 v5, 0x30

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num0:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num1"

    const/4 v4, 0x5

    const/16 v5, 0x31

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num1:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num2"

    const/4 v4, 0x6

    const/16 v5, 0x32

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num2:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num3"

    const/4 v4, 0x7

    const/16 v5, 0x33

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num3:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num4"

    const/16 v4, 0x8

    const/16 v5, 0x34

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num4:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num5"

    const/16 v4, 0x9

    const/16 v5, 0x35

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num5:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num6"

    const/16 v4, 0xa

    const/16 v5, 0x36

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num6:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num7"

    const/16 v4, 0xb

    const/16 v5, 0x37

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num7:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num8"

    const/16 v4, 0xc

    const/16 v5, 0x38

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num8:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Num9"

    const/16 v4, 0x39

    invoke-direct {v2, v3, v6, v4}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Num9:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "BrowserBack"

    const/16 v4, 0xe

    const/16 v5, 0xa6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserBack:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "BrowserRefresh"

    const/16 v4, 0xf

    const/16 v5, 0xa8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserRefresh:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "BrowserStop"

    const/16 v4, 0x10

    const/16 v5, 0xa9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserStop:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "VolumeMute"

    const/16 v4, 0x11

    const/16 v5, 0xad

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeMute:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 48
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "VolumeDown"

    const/16 v4, 0x12

    const/16 v5, 0xae

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeDown:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 50
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "VolumeUp"

    const/16 v4, 0xaf

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeUp:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 52
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Play"

    const/16 v4, 0x14

    const/16 v5, 0xfa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Play:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 54
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "PadA"

    const/16 v4, 0x15

    const/16 v5, 0x5800

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->PadA:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 56
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "PadB"

    const/16 v4, 0x16

    const/16 v5, 0x5801

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->PadB:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 58
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "PadX"

    const/16 v4, 0x17

    const/16 v5, 0x5802

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->PadX:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 60
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "PadY"

    const/16 v4, 0x18

    const/16 v5, 0x5803

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->PadY:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 62
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "LeftTrigger"

    const/16 v4, 0x19

    const/16 v5, 0x5806

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->LeftTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 64
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "RightTrigger"

    const/16 v4, 0x1a

    const/16 v5, 0x5807

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->RightTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 66
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Guide"

    const/16 v4, 0x5808

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Guide:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 68
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Up"

    const/16 v4, 0x1c

    const/16 v5, 0x5810

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Up:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 70
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Down"

    const/16 v4, 0x1d

    const/16 v5, 0x5811

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Down:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 72
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Left"

    const/16 v4, 0x1e

    const/16 v5, 0x5812

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Left:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Right"

    const/16 v4, 0x1f

    const/16 v5, 0x5813

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Right:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 76
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Start"

    const/16 v4, 0x20

    const/16 v5, 0x5814

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Start:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 78
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Back"

    const/16 v4, 0x21

    const/16 v5, 0x5815

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Back:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 80
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "DvdMenu"

    const/16 v4, 0x22

    const/16 v5, 0x5870

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->DvdMenu:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 82
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Display"

    const/16 v4, 0x23

    const/16 v5, 0x5875

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Display:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 84
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Stop"

    const/16 v4, 0x24

    const/16 v5, 0x5876

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Stop:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 86
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Record"

    const/16 v4, 0x25

    const/16 v5, 0x5877

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Record:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 88
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "FastForward"

    const/16 v4, 0x26

    const/16 v5, 0x5878

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->FastForward:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 90
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Rewind"

    const/16 v4, 0x27

    const/16 v5, 0x5879

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Rewind:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 92
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Skip"

    const/16 v4, 0x28

    const/16 v5, 0x587a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Skip:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 94
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Replay"

    const/16 v4, 0x29

    const/16 v5, 0x587b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Replay:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 96
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Info"

    const/16 v4, 0x2a

    const/16 v5, 0x587c

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Info:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 98
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Asterisk"

    const/16 v4, 0x2b

    const/16 v5, 0x5885

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Asterisk:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 100
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Pound"

    const/16 v4, 0x2c

    const/16 v5, 0x5886

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Pound:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 102
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "Title"

    const/16 v4, 0x2d

    const/16 v5, 0x5887

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->Title:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 104
    new-instance v2, Lcom/microsoft/xbox/smartglass/ControlKey;

    const-string v3, "MediaCenter"

    const/16 v4, 0x2e

    const/16 v5, 0x5888

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->MediaCenter:Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 10
    const/16 v2, 0x2f

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/ControlKey;

    sget-object v3, Lcom/microsoft/xbox/smartglass/ControlKey;->Empty:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/ControlKey;->Return:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v3, v2, v9

    const/4 v3, 0x2

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Pause:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Escape:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num0:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num1:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num2:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num3:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num4:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num5:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num6:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num7:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Num8:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/ControlKey;->Num9:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v3, v2, v6

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserBack:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserRefresh:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserStop:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeMute:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeDown:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeUp:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v3, v2, v7

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Play:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->PadA:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->PadB:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->PadX:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->PadY:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->LeftTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->RightTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/ControlKey;->Guide:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v3, v2, v8

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Up:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Down:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Left:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Right:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Start:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Back:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->DvdMenu:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Display:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x24

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Stop:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x25

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Record:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x26

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->FastForward:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x27

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Rewind:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x28

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Skip:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x29

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Replay:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Info:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Asterisk:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Pound:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->Title:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->MediaCenter:Lcom/microsoft/xbox/smartglass/ControlKey;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->$VALUES:[Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 107
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ControlKey;->_lookup:Landroid/util/SparseArray;

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/smartglass/ControlKey;->values()[Lcom/microsoft/xbox/smartglass/ControlKey;

    move-result-object v2

    array-length v3, v2

    .local v0, "key":Lcom/microsoft/xbox/smartglass/ControlKey;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 111
    sget-object v4, Lcom/microsoft/xbox/smartglass/ControlKey;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/ControlKey;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput p3, p0, Lcom/microsoft/xbox/smartglass/ControlKey;->_value:I

    .line 117
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/ControlKey;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 134
    sget-object v1, Lcom/microsoft/xbox/smartglass/ControlKey;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ControlKey;

    .line 135
    .local v0, "key":Lcom/microsoft/xbox/smartglass/ControlKey;
    if-nez v0, :cond_0

    .line 136
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 139
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/ControlKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/ControlKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ControlKey;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/ControlKey;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->$VALUES:[Lcom/microsoft/xbox/smartglass/ControlKey;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/ControlKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/ControlKey;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/microsoft/xbox/smartglass/ControlKey;->_value:I

    return v0
.end method
