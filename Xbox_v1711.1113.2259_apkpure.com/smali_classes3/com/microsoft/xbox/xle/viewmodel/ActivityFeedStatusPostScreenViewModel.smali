.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
.source "ActivityFeedStatusPostScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clubId:J

.field private getLinkPreviewAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

.field private loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private final meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private message:Ljava/lang/String;

.field private final originatingScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

.field private previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

.field private previewLinkText:Ljava/lang/String;

.field private showPreviewLayout:Z

.field private final userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;-><init>()V

    .line 43
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 53
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->originatingScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 54
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->clubId:J

    .line 58
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 59
    sget-object v1, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    .line 60
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->resetPreviewData()V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .param p2, "x2"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->onStatusUpdateComplete(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->clubId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->onGetLinkPreviewCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V

    return-void
.end method

.method private createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityFeedStatusPostScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$showWarning$0(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->goBack()V

    return-void
.end method

.method private onGetLinkPreviewCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "preview"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .prologue
    .line 272
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 286
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateAdapter()V

    .line 287
    return-void

    .line 276
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 281
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .line 282
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onStatusUpdateComplete(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .param p2, "pin"    # Z

    .prologue
    const/4 v3, 0x0

    .line 253
    if-eqz p1, :cond_2

    .line 254
    if-eqz p2, :cond_1

    .line 255
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->pinFeedItem(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to find timelineUri for pinning after posting"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0700dc

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 258
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->errorResId:I

    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->goBack()V

    .line 268
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateAdapter()V

    .line 269
    return-void

    .line 262
    :cond_1
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->errorResId:I

    .line 263
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->goBack()V

    goto :goto_0

    .line 266
    :cond_2
    const v0, 0x7f070b6c

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->errorResId:I

    goto :goto_0
.end method

.method private resetPreviewData()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewLinkText:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private showWarning()V
    .locals 4

    .prologue
    .line 131
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 133
    .local v0, "bldr":Landroid/app/AlertDialog$Builder;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700eb

    .line 134
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 135
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    .line 136
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070736

    .line 137
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 139
    return-void
.end method


# virtual methods
.method public addLinkToPost()V
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateMainLayout(Z)V

    .line 239
    return-void
.end method

.method public cancelPreview()V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->resetPreviewData()V

    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateMainLayout(Z)V

    .line 128
    return-void
.end method

.method public checkPinAndPostStatus(Z)V
    .locals 1
    .param p1, "pin"    # Z

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->checkPin()V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postStatus(Z)V

    goto :goto_0
.end method

.method public closeDialog()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->message:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->goBack()V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showWarning()V

    goto :goto_0
.end method

.method public getErrorStringResId()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->errorResId:I

    return v0
.end method

.method public getLoadPreviewDataState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->message:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->message:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPreviewErrorText()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "previewErrorResId":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 245
    const v0, 0x7f070d92

    .line 249
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    const-string v1, ""

    :goto_1
    return-object v1

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    iget-object v1, v1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->link:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_0

    .line 247
    :cond_3
    const v0, 0x7f070d93

    goto :goto_0

    .line 249
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getPreviewImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviewLinkText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewLinkText:Ljava/lang/String;

    return-object v0
.end method

.method public getShowPreviewLayout()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showPreviewLayout:Z

    return v0
.end method

.method public hasPreviewImage()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .line 223
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    .line 223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidLinkPreview()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    iget-object v0, v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->link:Ljava/lang/String;

    .line 218
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    .line 218
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->isPinning()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 105
    return-void
.end method

.method public loadPreview()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewLinkText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLinkPreviewAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLinkPreviewAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->cancel()V

    .line 211
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewLinkText:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLinkPreviewAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->getLinkPreviewAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$GetLinkPreviewAsyncTask;->load(Z)V

    .line 214
    :cond_1
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showPreviewLayout:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->cancelPreview()V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->closeDialog()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->onDestroy()V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->cancel()V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    .line 88
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 79
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 74
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method protected postStatus(Z)V
    .locals 7
    .param p1, "pin"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->cancel()V

    .line 161
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Activity Feed Status Post"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->originatingScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->errorResId:I

    if-nez v4, :cond_1

    const/4 v5, 0x0

    :goto_0
    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 162
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->postMessageTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel$PostMessageAsyncTask;->load(Z)V

    .line 164
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateAdapter()V

    .line 165
    return-void

    :cond_1
    move v5, v6

    .line 161
    goto :goto_0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->message:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setPreviewLinkText(Ljava/lang/String;)V
    .locals 0
    .param p1, "link"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->previewLinkText:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public shouldShowPreviewError()Z
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->loadPreviewDataState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasValidLinkPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->hasPreviewImage()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 232
    :goto_0
    return v0

    .line 234
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showAddLinkButton()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return v0
.end method

.method public updateMainLayout(Z)V
    .locals 3
    .param p1, "shouldShowPreviewLayout"    # Z

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showPreviewLayout:Z

    if-eq v0, p1, :cond_0

    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateMainLayout showPreviewLayout new value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->showPreviewLayout:Z

    .line 187
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;->updateAdapter()V

    .line 189
    :cond_0
    return-void
.end method
