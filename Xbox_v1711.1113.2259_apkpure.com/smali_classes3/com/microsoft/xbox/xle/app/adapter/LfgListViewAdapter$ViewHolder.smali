.class public Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LfgListViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field private clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private headerLayout:Landroid/widget/RelativeLayout;

.field private imageHeaderView:Landroid/widget/RelativeLayout;

.field private needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rowHeader:Landroid/widget/RelativeLayout;

.field private rowHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rowItem:Landroid/widget/RelativeLayout;

.field private startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

.field private tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

.field private tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

.field private titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private upcomingNoData:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .line 123
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 124
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->inflateViews()V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->bindViewHolderToItem(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    return-void
.end method

.method private bindTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 6
    .param p1, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 346
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 348
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergeTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;J)Z

    move-result v0

    .line 351
    .local v0, "shouldRefreshTags":Z
    if-eqz v0, :cond_0

    .line 352
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 353
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->getMergedTags()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 354
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 357
    .end local v0    # "shouldRefreshTags":Z
    :cond_0
    return-void
.end method

.method private bindViewHolderToItem(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 20
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;
    .param p2, "host"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 163
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v2, :cond_10

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->imageHeaderView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 177
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 179
    .local v13, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getStartTimeString()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 185
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v2

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v18

    .line 188
    .local v18, "selectedTitle":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v18, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    const v4, 0x7f020122

    const v5, 0x7f020122

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 193
    .end local v18    # "selectedTitle":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_0
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 194
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->sessionOwners()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 196
    if-eqz p2, :cond_1

    .line 197
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->primaryColor:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v17

    .line 198
    .local v17, "primaryColor":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setSelectedColor(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->secondaryColor:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundColor(I)V

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    const v4, 0x7f020125

    const v5, 0x7f020125

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 203
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->showClubBanner(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 246
    .end local v17    # "primaryColor":I
    :cond_1
    :goto_1
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v4, 0x7f0706e5

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setFocusable(Z)V

    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070628

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v19, v0

    .line 254
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    .line 253
    invoke-static/range {v2 .. v7}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    :cond_2
    :goto_2
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setFocusable(Z)V

    .line 273
    :goto_3
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getRemainingNeedCount()I

    move-result v16

    .line 274
    .local v16, "needValue":I
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getConfirmedCount()I

    move-result v8

    .line 276
    .local v8, "confirmedCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 279
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->bindTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%1$s %2$s %3$s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 284
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 285
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 286
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v5, v6

    .line 282
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 321
    .end local v8    # "confirmedCount":I
    .end local v13    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v16    # "needValue":I
    :goto_4
    return-void

    .line 182
    .restart local v13    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 205
    .restart local v17    # "primaryColor":I
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->itemType:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    if-ne v2, v3, :cond_8

    .line 206
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 207
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->membersCount()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->getConfirmedCount()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v14, v2, -0x1

    .line 208
    .local v14, "interestedCount":I
    if-lez v14, :cond_5

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070713

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 211
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->showClubBanner(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    goto/16 :goto_1

    .line 213
    .end local v14    # "interestedCount":I
    :cond_6
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->isXuidConfirmed(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706df

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 216
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 219
    :cond_8
    const/4 v9, 0x0

    .line 220
    .local v9, "firstFollowerFound":Lcom/microsoft/xbox/service/model/FollowersData;
    const/4 v11, 0x0

    .line 222
    .local v11, "friendIsHost":Z
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v15

    .line 223
    .local v15, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v15, :cond_a

    .line 224
    invoke-virtual {v15}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v12

    .line 225
    .local v12, "friendsList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 226
    .local v10, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v3, v10, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706da

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 228
    const/4 v11, 0x1

    .line 236
    .end local v10    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v12    # "friendsList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_a
    if-nez v11, :cond_c

    if-eqz v9, :cond_c

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706d8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 230
    .restart local v10    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .restart local v12    # "friendsList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_b
    if-nez v9, :cond_9

    iget-object v3, v10, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v13, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->isXuidConfirmed(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 231
    move-object v9, v10

    goto :goto_5

    .line 239
    .end local v10    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v12    # "friendsList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->showClubBanner(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    goto/16 :goto_1

    .line 259
    .end local v9    # "firstFollowerFound":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v11    # "friendIsHost":Z
    .end local v15    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v17    # "primaryColor":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    goto/16 :goto_2

    .line 266
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 269
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 292
    .end local v13    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->imageHeaderView:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 296
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 297
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 301
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$LfgListViewAdapter$LfgListViewAdapterItemType:[I

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->itemType:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_4

    .line 303
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->upcomingNoData:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070706

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 308
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->upcomingNoData:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706f5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 313
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->upcomingNoData:Landroid/view/View;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_4

    .line 301
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private inflateViews()V
    .locals 6

    .prologue
    .line 129
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0764

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    .line 130
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->imageHeaderView:Landroid/widget/RelativeLayout;

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0765

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 134
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0767

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 135
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0768

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 137
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0760

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0762

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0761

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 141
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0763

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    .line 142
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0769

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    .line 143
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e076a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->rowHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 144
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e076b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->upcomingNoData:Landroid/view/View;

    .line 146
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c0149

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 147
    .local v0, "backgroundColor":I
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 149
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v2

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    .line 155
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0766

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 156
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 157
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 159
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-direct {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    .line 160
    return-void

    :cond_0
    move v2, v0

    .line 152
    goto :goto_0
.end method

.method private showClubBanner(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 4
    .param p1, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 324
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 325
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 327
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->clubId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 328
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->clubId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 330
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 334
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 338
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 340
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    if-eqz v0, :cond_0

    .line 341
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$ViewHolder;->getAdapterPosition()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 343
    :cond_0
    return-void
.end method
