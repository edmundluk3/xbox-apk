.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCPageActionModel;
.super Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
.source "UTCPageActionModel.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "ctx"    # Ljava/lang/String;
    .param p2, "rId"    # Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 9
    const-string v0, "content"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCPageActionModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 10
    const-string v0, "relativeId"

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCPageActionModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 11
    const-string v0, "indexPos"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCPageActionModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 12
    return-void
.end method
