.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;
.super Ljava/lang/Object;
.source "UTCPeopleHub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackSelectFromFriendsListAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$item:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;->val$item:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 299
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;->val$item:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 300
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;->val$item:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getPersonXuid()Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "personXuid":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 303
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "TargetXuid"

    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 304
    const-string v2, "Filter"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$300()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->getTelemetryName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$000()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 307
    const-string v2, "ProfileType"

    const-string v3, "YouProfile"

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 312
    :goto_0
    const-string v2, "People Hub - Select from Friends List"

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 313
    return-void

    .line 309
    :cond_0
    const-string v2, "ProfileType"

    const-string v3, "MeProfile"

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
