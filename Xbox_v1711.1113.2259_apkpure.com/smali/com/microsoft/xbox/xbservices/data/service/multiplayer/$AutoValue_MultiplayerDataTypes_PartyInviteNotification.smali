.class abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
.source "$AutoValue_MultiplayerDataTypes_PartyInviteNotification.java"


# instance fields
.field private final inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;)V
    .locals 2
    .param p1, "inviteHandle"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null inviteHandle"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;->inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 37
    if-ne p1, p0, :cond_0

    .line 38
    const/4 v1, 0x1

    .line 44
    :goto_0
    return v1

    .line 40
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;

    .line 42
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;->inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;->inviteHandle()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 44
    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x1

    .line 50
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;->inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 52
    return v0
.end method

.method public inviteHandle()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "invite_handle"
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;->inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyInviteNotification{inviteHandle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_PartyInviteNotification;->inviteHandle:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
