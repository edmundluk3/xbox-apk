.class public Lcom/microsoft/xbox/idp/interop/Interop;
.super Ljava/lang/Object;
.source "Interop.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;,
        Lcom/microsoft/xbox/idp/interop/Interop$ErrorStatus;,
        Lcom/microsoft/xbox/idp/interop/Interop$ErrorType;,
        Lcom/microsoft/xbox/idp/interop/Interop$AuthFlowScreenStatus;,
        Lcom/microsoft/xbox/idp/interop/Interop$MSAError;,
        Lcom/microsoft/xbox/idp/interop/Interop$MSAPurpose;,
        Lcom/microsoft/xbox/idp/interop/Interop$XBLogoutCallback;,
        Lcom/microsoft/xbox/idp/interop/Interop$EventInitializationCallback;,
        Lcom/microsoft/xbox/idp/interop/Interop$XBLoginCallback;,
        Lcom/microsoft/xbox/idp/interop/Interop$Callback;,
        Lcom/microsoft/xbox/idp/interop/Interop$ErrorCallback;
    }
.end annotation


# static fields
.field private static final DNET_SCOPE:Ljava/lang/String; = "user.auth.dnet.xboxlive.com"

.field private static final PACKAGE_NAME_TO_REMOVE:Ljava/lang/String; = "com.microsoft.onlineid.sample"

.field private static final POLICY:Ljava/lang/String; = "mbi_ssl"

.field private static final PROD_SCOPE:Ljava/lang/String; = "user.auth.xboxlive.com"

.field private static final TAG:Ljava/lang/String;

.field private static s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

.field private static final silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/idp/interop/Interop;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    .line 295
    new-instance v0, Lcom/microsoft/xbox/idp/interop/Interop$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/interop/Interop$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetLiveXTokenCallback(Z)Ljava/lang/String;
    .locals 1
    .param p0, "forceRefresh"    # Z

    .prologue
    .line 89
    invoke-static {p0}, Lcom/microsoft/xbox/idp/interop/Interop;->get_uploader_x_token_callback(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static GetLocalStoragePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static GetXTokenCallback(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/microsoft/xbox/idp/interop/Interop;->get_supporting_x_token_callback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static InitCLL(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "iKey"    # Ljava/lang/String;

    .prologue
    .line 169
    const-string v2, "XSAPI.Android"

    const-string v3, "Init CLL"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    new-instance v2, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    new-instance v3, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    invoke-direct {v3, p1, p0}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;-><init>(Lcom/microsoft/cll/android/Internal/AndroidInternalCll;Landroid/content/Context;)V

    sput-object v2, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    .line 172
    new-instance v0, Lcom/microsoft/xbox/idp/interop/CLLCallback;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/idp/interop/CLLCallback;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 173
    .local v0, "callback":Lcom/microsoft/xbox/idp/interop/CLLCallback;
    sget-object v2, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    invoke-virtual {v2}, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v1

    .line 174
    .local v1, "cll":Lcom/microsoft/cll/android/Internal/AndroidInternalCll;
    invoke-virtual {v1, v0}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V

    .line 175
    sget-object v2, Lcom/microsoft/cll/android/Verbosity;->INFO:Lcom/microsoft/cll/android/Verbosity;

    invoke-virtual {v1, v2}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->setDebugVerbosity(Lcom/microsoft/cll/android/Verbosity;)V

    .line 176
    invoke-virtual {v1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->start()V

    .line 177
    return-void
.end method

.method public static InvokeAuthFlow(JLandroid/app/Activity;Z)V
    .locals 4
    .param p0, "userPtr"    # J
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "isProd"    # Z

    .prologue
    .line 234
    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v2, "InvokeAuthFlow"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    if-nez p3, :cond_0

    .line 237
    sget-object v1, Lcom/microsoft/xbox/idp/services/Endpoints$Type;->DNET:Lcom/microsoft/xbox/idp/services/Endpoints$Type;

    sput-object v1, Lcom/microsoft/xbox/idp/services/Config;->endpointType:Lcom/microsoft/xbox/idp/services/Endpoints$Type;

    .line 240
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/idp/interop/Interop$1;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/interop/Interop$1;-><init>()V

    invoke-static {v1}, Lcom/microsoft/xbox/idp/ui/AuthFlowActivity;->setStaticCallbacks(Lcom/microsoft/xbox/idp/ui/AuthFlowActivity$StaticCallbacks;)V

    .line 250
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/microsoft/xbox/idp/ui/AuthFlowActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "ARG_SECURITY_SCOPE"

    if-eqz p3, :cond_1

    const-string/jumbo v1, "user.auth.xboxlive.com"

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    const-string v1, "ARG_SECURITY_POLICY"

    const-string v2, "mbi_ssl"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    const-string v1, "ARG_USER_PTR"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 254
    invoke-virtual {p2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 255
    return-void

    .line 251
    :cond_1
    const-string/jumbo v1, "user.auth.dnet.xboxlive.com"

    goto :goto_0
.end method

.method public static InvokeEventInitialization(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$EventInitializationCallback;)V
    .locals 2
    .param p0, "userPtr"    # J
    .param p2, "rpsTicket"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/microsoft/xbox/idp/interop/Interop$EventInitializationCallback;

    .prologue
    .line 269
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "InvokeEventInitialization"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/interop/Interop;->invoke_event_initialization(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$EventInitializationCallback;)V

    .line 271
    return-void
.end method

.method public static InvokeMSA(Landroid/content/Context;IZLjava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "isProd"    # Z
    .param p3, "cid"    # Ljava/lang/String;

    .prologue
    .line 186
    const-string v0, "XSAPI.Android"

    const-string v1, "Invoking MSA"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-nez p2, :cond_0

    .line 189
    sget-object v0, Lcom/microsoft/xbox/idp/services/Endpoints$Type;->DNET:Lcom/microsoft/xbox/idp/services/Endpoints$Type;

    sput-object v0, Lcom/microsoft/xbox/idp/services/Config;->endpointType:Lcom/microsoft/xbox/idp/services/Endpoints$Type;

    .line 191
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/idp/interop/Interop$MSAPurpose;->fromId(I)Lcom/microsoft/xbox/idp/interop/Interop$MSAPurpose;

    move-result-object v7

    .line 192
    .local v7, "purpose":Lcom/microsoft/xbox/idp/interop/Interop$MSAPurpose;
    if-eqz v7, :cond_3

    .line 194
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop$3;->$SwitchMap$com$microsoft$xbox$idp$interop$Interop$MSAPurpose:[I

    invoke-virtual {v7}, Lcom/microsoft/xbox/idp/interop/Interop$MSAPurpose;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 211
    const-string v0, ""

    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->OTHER:Lcom/microsoft/xbox/idp/interop/Interop$MSAError;

    iget v1, v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->id:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid requestCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/idp/interop/Interop;->MSACallback(Ljava/lang/String;IILjava/lang/String;)V

    .line 219
    :goto_0
    return-void

    .line 197
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InvokeMSA OPPORTUNISTIC_SIGN_IN cid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    const-string v0, ""

    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->UI_INTERACTION_REQUIRED:Lcom/microsoft/xbox/idp/interop/Interop$MSAError;

    iget v1, v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->id:I

    const-string v2, "Must show UI to acquire an account."

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/idp/interop/Interop;->MSACallback(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/idp/interop/Interop;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    if-eqz p2, :cond_2

    const-string/jumbo v4, "user.auth.xboxlive.com"

    :goto_1
    const-string v5, "mbi_ssl"

    move-object v1, p0

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;->start()Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    goto :goto_0

    :cond_2
    const-string/jumbo v4, "user.auth.dnet.xboxlive.com"

    goto :goto_1

    .line 206
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "InvokeMSA SIGN_OUT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-static {}, Lcom/microsoft/xbox/idp/util/CacheUtil;->clearCaches()V

    .line 208
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->sign_out_callback()V

    goto :goto_0

    .line 217
    :cond_3
    const-string v0, ""

    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->OTHER:Lcom/microsoft/xbox/idp/interop/Interop$MSAError;

    iget v1, v1, Lcom/microsoft/xbox/idp/interop/Interop$MSAError;->id:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid requestCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/idp/interop/Interop;->MSACallback(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static InvokeXBLogin(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$XBLoginCallback;)V
    .locals 2
    .param p0, "userPtr"    # J
    .param p2, "rpsTicket"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/microsoft/xbox/idp/interop/Interop$XBLoginCallback;

    .prologue
    .line 259
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "InvokeXBLogin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/interop/Interop;->invoke_xb_login(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$XBLoginCallback;)V

    .line 261
    return-void
.end method

.method public static InvokeXBLogout(JLcom/microsoft/xbox/idp/interop/Interop$XBLogoutCallback;)V
    .locals 2
    .param p0, "userPtr"    # J
    .param p2, "callback"    # Lcom/microsoft/xbox/idp/interop/Interop$XBLogoutCallback;

    .prologue
    .line 280
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "InvokeSignOut"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/idp/interop/Interop;->invoke_xb_logout(JLcom/microsoft/xbox/idp/interop/Interop$XBLogoutCallback;)V

    .line 282
    return-void
.end method

.method public static InvokeXTokenCallback(JLcom/microsoft/xbox/idp/interop/Interop$Callback;)V
    .locals 2
    .param p0, "userPtr"    # J
    .param p2, "callback"    # Lcom/microsoft/xbox/idp/interop/Interop$Callback;

    .prologue
    .line 291
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "InvokeXTokenCallback"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/idp/interop/Interop;->invoke_x_token_acquisition(JLcom/microsoft/xbox/idp/interop/Interop$Callback;)V

    .line 293
    return-void
.end method

.method public static LogCLL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "eventData"    # Ljava/lang/String;

    .prologue
    .line 99
    const-string v1, "XSAPI.Android"

    const-string v2, "Log CLL"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v0, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    if-nez v1, :cond_0

    .line 105
    const-string v1, "XSAPI.Android"

    const-string v2, "Log CLL null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v1, p2, p1, v0, v2}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->log(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    goto :goto_0
.end method

.method public static LogTelemetrySignIn(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "api"    # Ljava/lang/String;
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string v2, "XSAPI.Android"

    const-string v3, "LogTelemetrySignIn"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    new-instance v0, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;-><init>()V

    .line 115
    .local v0, "model":Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "name":Ljava/lang/String;
    invoke-static {v1, v1, v0}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCAdditionalInfoModel;)V

    .line 117
    return-void
.end method

.method public static MSACallback(Ljava/lang/String;IILjava/lang/String;)V
    .locals 2
    .param p0, "rpsTicket"    # Ljava/lang/String;
    .param p1, "state"    # I
    .param p2, "errorCode"    # I
    .param p3, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 228
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    const-string v1, "MSA Callback"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/interop/Interop;->ticket_callback(Ljava/lang/String;IILjava/lang/String;)V

    .line 230
    return-void
.end method

.method public static ReadConfigFile(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 141
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string/jumbo v6, "xboxservices"

    const-string v7, "raw"

    .line 142
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 141
    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 140
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 143
    .local v1, "inputStream":Ljava/io/InputStream;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 145
    .local v3, "outputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x400

    new-array v0, v4, [B

    .line 149
    .local v0, "buf":[B
    :goto_0
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "len":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 151
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 156
    .end local v2    # "len":I
    :catch_0
    move-exception v4

    .line 159
    :goto_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 153
    .restart local v2    # "len":I
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 154
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static UpdateGamerTag(Ljava/lang/String;)V
    .locals 0
    .param p0, "newGamerTag"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-static {p0}, Lcom/microsoft/xbox/idp/interop/Interop;->gamertag_updated_callback(Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(JILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/interop/Interop;->auth_flow_callback(JILjava/lang/String;)V

    return-void
.end method

.method private static native auth_flow_callback(JILjava/lang/String;)V
.end method

.method public static native deinitializeInterop()Z
.end method

.method private static native gamertag_updated_callback(Ljava/lang/String;)V
.end method

.method public static getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;->getAppContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public static getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/idp/interop/Interop;->s_cll:Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/Interop$CllWrapper;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v0

    return-object v0
.end method

.method public static getLocale()Ljava/lang/String;
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "locale":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "locale is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    return-object v0
.end method

.method public static getSystemProxy()Ljava/lang/String;
    .locals 5

    .prologue
    .line 120
    const-string v3, "http.proxyHost"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "proxyAddress":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 122
    const-string v3, "http.proxyPort"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "proxyPort":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "fullProxy":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/idp/interop/Interop;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    .end local v0    # "fullProxy":Ljava/lang/String;
    .end local v2    # "proxyPort":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getTitleDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->get_title_telemetry_device_id()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTitleSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->get_title_telemetry_session_id()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native get_supporting_x_token_callback(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native get_title_telemetry_device_id()Ljava/lang/String;
.end method

.method private static native get_title_telemetry_session_id()Ljava/lang/String;
.end method

.method private static native get_uploader_x_token_callback(Z)Ljava/lang/String;
.end method

.method public static native initializeInterop(Landroid/content/Context;)Z
.end method

.method private static native invoke_event_initialization(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$EventInitializationCallback;)V
.end method

.method private static native invoke_x_token_acquisition(JLcom/microsoft/xbox/idp/interop/Interop$Callback;)V
.end method

.method private static native invoke_xb_login(JLjava/lang/String;Lcom/microsoft/xbox/idp/interop/Interop$XBLoginCallback;)V
.end method

.method private static native invoke_xb_logout(JLcom/microsoft/xbox/idp/interop/Interop$XBLogoutCallback;)V
.end method

.method private static native sign_out_callback()V
.end method

.method private static native ticket_callback(Ljava/lang/String;IILjava/lang/String;)V
.end method
