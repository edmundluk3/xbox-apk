.class public final Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;
.super Ljava/lang/Object;
.source "TutorialRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;


# static fields
.field private static final APP_FIRST_RUN_KEY:Ljava/lang/String; = "app_first_run"

.field private static final SYSTEM_SETTINGS_TIMEOUT_THRESHOLD:I = 0x1388

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isTutorialExperienceAllowedSingle:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isTutorialExperienceEnabledObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private temporarilyForceDisableTutorialExperienceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 5
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 40
    invoke-static {p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->getIsFirstRun()Z

    move-result v0

    .line 44
    .local v0, "isFirstRunOnAppLoad":Z
    if-nez v0, :cond_0

    .line 45
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceAllowedSingle:Lio/reactivex/Single;

    .line 81
    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->temporarilyForceDisableTutorialExperienceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string v2, "app_first_run"

    const/4 v3, 0x1

    .line 84
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v1

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->temporarilyForceDisableTutorialExperienceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    .line 83
    invoke-static {v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceEnabledObservable:Lio/reactivex/Observable;

    .line 88
    return-void

    .line 48
    :cond_0
    invoke-interface {p3}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 49
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 51
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 52
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 53
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p2}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 54
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 56
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p2}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 57
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 58
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 60
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 61
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 69
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceAllowedSingle:Lio/reactivex/Single;

    goto/16 :goto_0
.end method

.method static synthetic lambda$isTutorialExperienceEnabledObservable$12(Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;Ljava/lang/Boolean;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;
    .param p1, "allowed"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceEnabledObservable:Lio/reactivex/Observable;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 3
    .param p0, "authState"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "authState"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;Ljava/lang/Boolean;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;
    .param p1, "liveSettingEnabled"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    const-string v1, "It is the first run, but we are skipping the first run experience permanently because the live setting is not turned on."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->setAppToNormalState()V

    .line 75
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$11(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "shouldTutorialExperienceBeEnabled"    # Ljava/lang/Boolean;
    .param p1, "temporarilyForceDisableTutorialExperience"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 3
    .param p0, "authState"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authState success: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 2
    .param p0, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    const-string v1, "entering flatmap"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/domain/auth/AuthState;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p1, "authState"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getSettings()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v2, 0x1388

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 55
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/Observable;->timeout(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/SmartglassSettings;)V
    .locals 3
    .param p0, "ignore"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exiting flatmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/app/SmartglassSettings;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p1, "ignore"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->tutorialExperienceEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$7(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "tutorialExperienceEnabled"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tutorialExperienceEnabled?: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$8(Ljava/lang/Boolean;)V
    .locals 2
    .param p0, "ignore"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "timeout"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$9(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 3
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call to system settings exceeded timeout limit of 5000 ms. Assuming tutorial experience is disabled\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 65
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 66
    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getIsFirstRun()Z
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "app_first_run"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isTutorialExperienceAllowed()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lio/reactivex/annotations/NonNull;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceAllowedSingle:Lio/reactivex/Single;

    return-object v0
.end method

.method public isTutorialExperienceEnabled()Z
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->getIsFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->temporarilyForceDisableTutorialExperienceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTutorialExperienceEnabledObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->isTutorialExperienceAllowed()Lio/reactivex/Single;

    move-result-object v0

    .line 119
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method public setAppToNormalState()V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "app_first_run"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 156
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    const-string v1, "Disabling the tutorial experience"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public temporarilyForceDisableTutorialExperience()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "temporarilyForceDisableTutorialExperience"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;->temporarilyForceDisableTutorialExperienceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 147
    return-void
.end method
