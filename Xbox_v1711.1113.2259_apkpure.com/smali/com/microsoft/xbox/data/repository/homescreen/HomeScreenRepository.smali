.class public final Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
.super Ljava/lang/Object;
.source "HomeScreenRepository.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final DEFAULT_HOME_SCREEN:I

.field private static final PREFS_KEY:Ljava/lang/String; = "home_screen_pref"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activeHomeScreen:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field public telemetryService:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->TAG:Ljava/lang/String;

    .line 17
    sget-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v0

    sput v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->DEFAULT_HOME_SCREEN:I

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 28
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->telemetryService:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->activeHomeScreen:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 33
    return-void
.end method


# virtual methods
.method public getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->activeHomeScreen:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    return-object v0
.end method

.method public getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "home_screen_pref"

    sget v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->DEFAULT_HOME_SCREEN:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->fromInt(I)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    return-object v0
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "home_screen_pref"

    sget v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->DEFAULT_HOME_SCREEN:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 66
    return-void
.end method

.method public setActiveHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 0
    .param p1, "preference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 51
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->activeHomeScreen:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 54
    return-void
.end method

.method public setHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 4
    .param p1, "preference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->telemetryService:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "home_screen_pref"

    sget v3, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->DEFAULT_HOME_SCREEN:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;->changeDefault(ILcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "home_screen_pref"

    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 62
    return-void
.end method
