.class public Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "LfgVettingScreenAdapter.java"


# instance fields
.field private interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

.field private interestedCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private needCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    .line 26
    const v3, 0x7f0e0772

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 27
    const v3, 0x7f0e0773

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->needCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v3, 0x7f0e0774

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 30
    .local v2, "interestedList":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .line 40
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 42
    const v3, 0x7f0e0771

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 43
    .local v1, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const v3, 0x7f0e0776

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 49
    .local v0, "backButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
    .param p1, "approvedMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->approveMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V

    .line 33
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
    .param p1, "declinedMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->declineMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V

    .line 36
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
    .param p1, "reportMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->reportMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V

    .line 39
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 44
    const-string v0, "LFG - Close"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->onTapCloseButton()V

    .line 46
    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->onTapCloseButton()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->clear()V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getInterestedList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->addAll(Ljava/util/Collection;)V

    .line 56
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getNeedCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->setAllowApprovals(Z)V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->notifyDataSetChanged()V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->interestedCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070713

    .line 61
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getInterestedList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->needCountLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070717

    .line 64
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getNeedCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->isBlockingBusy()Z

    move-result v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07071a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 67
    return-void

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0
.end method
