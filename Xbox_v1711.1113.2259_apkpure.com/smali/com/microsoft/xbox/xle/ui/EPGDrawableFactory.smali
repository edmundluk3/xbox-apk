.class public Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;
.super Ljava/lang/Object;
.source "EPGDrawableFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;,
        Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;,
        Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;,
        Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;
    }
.end annotation


# static fields
.field private static sColorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;",
            ">;"
        }
    .end annotation
.end field

.field private static sTextColorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sColorMap:Ljava/util/HashMap;

    .line 231
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sTextColorMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static defaultPressedSelectedDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p0, "defaultState"    # Landroid/graphics/drawable/Drawable;
    .param p1, "pressedState"    # Landroid/graphics/drawable/Drawable;
    .param p2, "selectedState"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v4, 0x2

    .line 256
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 257
    .local v0, "sld":Landroid/graphics/drawable/StateListDrawable;
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x10100a7

    aput v3, v1, v2

    invoke-virtual {v0, v1, p1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 258
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1, p2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 259
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    invoke-virtual {v0, v1, p0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 261
    return-object v0

    .line 258
    nop

    :array_0
    .array-data 4
        0x10100a1
        -0x10100a7
    .end array-data

    .line 259
    :array_1
    .array-data 4
        -0x10100a1
        -0x10100a7
    .end array-data
.end method

.method public static defaultPressedSelectedText(III)Landroid/content/res/ColorStateList;
    .locals 8
    .param p0, "defaultState"    # I
    .param p1, "pressedState"    # I
    .param p2, "selectedState"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 265
    new-instance v0, Landroid/content/res/ColorStateList;

    new-array v1, v7, [[I

    new-array v2, v6, [I

    const v3, 0x10100a7

    aput v3, v2, v5

    aput-object v2, v1, v5

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v4

    new-array v2, v7, [I

    aput p1, v2, v5

    aput p2, v2, v6

    aput p0, v2, v4

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v0

    nop

    :array_0
    .array-data 4
        0x10100a1
        -0x10100a7
    .end array-data

    :array_1
    .array-data 4
        -0x10100a1
        -0x10100a7
    .end array-data
.end method

.method public static getProfileColor()I
    .locals 4

    .prologue
    .line 270
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 271
    .local v0, "currentUser":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 272
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    .line 273
    .local v1, "preferred":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-eqz v1, :cond_0

    .line 274
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v2

    .line 277
    .end local v1    # "preferred":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_0
    return v2

    :cond_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0
.end method

.method public static getSharedColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;
    .locals 2
    .param p0, "purpose"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .prologue
    .line 234
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sColorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sColorMap:Ljava/util/HashMap;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;-><init>()V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sColorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    return-object v0
.end method

.method public static getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "purpose"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .prologue
    .line 251
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColorDrawable;-><init>(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;)V

    return-object v0
.end method

.method public static getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;
    .locals 2
    .param p0, "purpose"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .prologue
    .line 241
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sTextColorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sTextColorMap:Ljava/util/HashMap;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;-><init>()V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->sTextColorMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    return-object v0
.end method
