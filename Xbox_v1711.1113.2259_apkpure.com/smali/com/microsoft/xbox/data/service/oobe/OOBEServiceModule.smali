.class public Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;
.super Ljava/lang/Object;
.source "OOBEServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule$Names;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideOOBESessionEndpoint()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "OOBE_ENDPOINT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 37
    const-string v0, "https://zto.dds.microsoft.com"

    return-object v0
.end method

.method provideOOBESessionRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;
    .locals 2
    .param p1, "baseUrl"    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "OOBE_ENDPOINT"
        .end annotation
    .end param
    .param p2, "client"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "OOBE_OK_HTTP"
        .end annotation
    .end param
    .param p3, "gsonConverterFactory"    # Lretrofit2/converter/gson/GsonConverterFactory;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "OOBE_RETROFIT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 70
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 72
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method provideOOBESessionService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/oobe/OOBEService;
    .locals 1
    .param p1, "retrofit"    # Lretrofit2/Retrofit;
        .annotation runtime Ljavax/inject/Named;
            value = "OOBE_RETROFIT"
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 80
    const-class v0, Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    return-object v0
.end method

.method providesOOBESessionClient(Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p1, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p2, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p3, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;
    .param p4, "xblCorrelationIdHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;
    .param p5, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p6, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p7, "contentRestrictionsHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "OOBE_OK_HTTP"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 52
    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p5}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p6}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p7}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 51
    return-object v0
.end method
