.class public final enum Lcom/microsoft/cll/android/Cll$EventPersistence;
.super Ljava/lang/Enum;
.source "Cll.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/cll/android/Cll;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventPersistence"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/cll/android/Cll$EventPersistence;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/cll/android/Cll$EventPersistence;

.field public static final enum CRITICAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

.field public static final enum NORMAL:Lcom/microsoft/cll/android/Cll$EventPersistence;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 125
    new-instance v0, Lcom/microsoft/cll/android/Cll$EventPersistence;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/cll/android/Cll$EventPersistence;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->NORMAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    .line 126
    new-instance v0, Lcom/microsoft/cll/android/Cll$EventPersistence;

    const-string v1, "CRITICAL"

    invoke-direct {v0, v1, v2, v4}, Lcom/microsoft/cll/android/Cll$EventPersistence;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->CRITICAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    .line 123
    new-array v0, v4, [Lcom/microsoft/cll/android/Cll$EventPersistence;

    sget-object v1, Lcom/microsoft/cll/android/Cll$EventPersistence;->NORMAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/cll/android/Cll$EventPersistence;->CRITICAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->$VALUES:[Lcom/microsoft/cll/android/Cll$EventPersistence;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    iput p3, p0, Lcom/microsoft/cll/android/Cll$EventPersistence;->value:I

    .line 132
    return-void
.end method

.method public static getPersistence(I)Lcom/microsoft/cll/android/Cll$EventPersistence;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 139
    packed-switch p0, :pswitch_data_0

    .line 146
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 141
    :pswitch_0
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->NORMAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    goto :goto_0

    .line 143
    :pswitch_1
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->CRITICAL:Lcom/microsoft/cll/android/Cll$EventPersistence;

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/Cll$EventPersistence;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    const-class v0, Lcom/microsoft/cll/android/Cll$EventPersistence;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/cll/android/Cll$EventPersistence;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/cll/android/Cll$EventPersistence;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventPersistence;->$VALUES:[Lcom/microsoft/cll/android/Cll$EventPersistence;

    invoke-virtual {v0}, [Lcom/microsoft/cll/android/Cll$EventPersistence;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/cll/android/Cll$EventPersistence;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/microsoft/cll/android/Cll$EventPersistence;->value:I

    return v0
.end method
