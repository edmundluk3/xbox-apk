.class public Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;
.super Landroid/widget/LinearLayout;
.source "MsaAuthenticatorView.java"


# instance fields
.field private _authView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

.field private _authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "packageName":Ljava/lang/String;
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 34
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const-string v3, "layout"

    const-string v4, "msaauthenticatorview"

    invoke-static {v1, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 36
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 37
    .local v2, "root":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .line 38
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 39
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "MsaAuthenticatorView.initialize must be called first."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->addListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V

    .line 52
    return-void
.end method

.method public initialize(Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .param p2, "policy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/smartglass/Environment;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "clientIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/microsoft/xbox/smartglass/Environment;Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;-><init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->setPolicy(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public login()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "MsaAuthenticatorView.initialize must be called first."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->login()V

    .line 67
    return-void
.end method

.method public logout()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "MsaAuthenticatorView.initialize must be called first."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->logout()V

    .line 74
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "MsaAuthenticatorView.initialize must be called first."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->removeListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V

    .line 60
    return-void
.end method

.method public setSandboxId(Ljava/lang/String;)V
    .locals 1
    .param p1, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorView;->_authenticator:Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->setSandboxId(Ljava/lang/String;)V

    .line 78
    return-void
.end method
