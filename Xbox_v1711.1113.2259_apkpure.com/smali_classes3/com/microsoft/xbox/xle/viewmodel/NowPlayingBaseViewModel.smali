.class public Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
.super Ljava/lang/Object;
.source "NowPlayingBaseViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# instance fields
.field protected activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field protected view:Lcom/microsoft/xbox/toolkit/IViewUpdate;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/IViewUpdate;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    .line 38
    return-void
.end method

.method private isAppRunning(I)Z
    .locals 4
    .param p1, "titleId"    # I

    .prologue
    const/4 v2, 0x0

    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 218
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v3, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v2

    .line 221
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 222
    .local v1, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    return-object v0
.end method

.method public getActiveTitleLocationSymbol()Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    const/4 v0, -0x1

    .line 195
    .local v0, "id":I
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel$1;->$SwitchMap$com$microsoft$xbox$smartglass$ActiveTitleLocation:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 209
    :goto_0
    if-ltz v0, :cond_0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    .line 197
    :pswitch_0
    const v0, 0x7f070ea4

    .line 198
    goto :goto_0

    .line 201
    :pswitch_1
    const v0, 0x7f070ea3

    .line 202
    goto :goto_0

    .line 205
    :pswitch_2
    const v0, 0x7f070eaa

    goto :goto_0

    .line 209
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getEnabledMediaCommands()Ljava/util/EnumSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 88
    .local v1, "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    if-eqz v1, :cond_0

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    iget v3, v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    .line 90
    .local v0, "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    if-eqz v0, :cond_0

    .line 91
    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    .line 95
    .end local v0    # "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    :goto_0
    return-object v2

    .line 94
    :cond_0
    const-string v2, "NowPlayingBaseViewModel"

    const-string v3, "Missing mediaState or media enabledComamnds"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getLastKnownIeUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getLastKnownIeUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isAppRunning([I)Z
    .locals 7
    .param p1, "titleIds"    # [I

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 228
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v4, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v4, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v3

    .line 231
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    .line 232
    .local v2, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 233
    array-length v5, p1

    move v4, v3

    :goto_1
    if-ge v4, v5, :cond_0

    aget v1, p1, v4

    .line 234
    .local v1, "id":I
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v6

    if-ne v6, v1, :cond_2

    .line 235
    const/4 v3, 0x1

    goto :goto_0

    .line 233
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public isBlueRayRunning()Z
    .locals 1

    .prologue
    .line 158
    const v0, 0x6a5297cd

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isAppRunning(I)Z

    move-result v0

    return v0
.end method

.method public isIeRunning()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public isInRemoteControl()Z
    .locals 1

    .prologue
    .line 53
    instance-of v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    return v0
.end method

.method public isMediaPaused()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 78
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v3, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v2

    .line 81
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 82
    .local v1, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaPaused()Z

    move-result v2

    goto :goto_0
.end method

.method public isTvRunning()Z
    .locals 1

    .prologue
    .line 163
    const v0, 0x162615ad

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isAppRunning(I)Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 180
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->load(Z)V

    .line 181
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->shouldObserveNowPlayingGlobalModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 176
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->shouldObserveNowPlayingGlobalModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 170
    :cond_0
    return-void
.end method

.method public sendMediaCommands(Lcom/microsoft/xbox/smartglass/MediaControlCommands;Ljava/lang/String;)V
    .locals 10
    .param p1, "controlCommand"    # Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    .param p2, "vortextActionName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isInRemoteControl()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Remote"

    invoke-virtual {v4, p2, v5, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 109
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v4, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v4, :cond_0

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/SessionModel;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 111
    .local v3, "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    if-eqz v3, :cond_0

    .line 113
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/MediaCommand;

    iget v4, v3, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-direct {v1, v4, p1}, Lcom/microsoft/xbox/smartglass/MediaCommand;-><init>(ILcom/microsoft/xbox/smartglass/MediaControlCommands;)V

    .line 114
    .local v1, "command":Lcom/microsoft/xbox/smartglass/MediaCommand;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v1    # "command":Lcom/microsoft/xbox/smartglass/MediaCommand;
    .end local v3    # "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_0
    :goto_1
    return-void

    .line 102
    .end local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    instance-of v4, v4, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    if-eqz v4, :cond_2

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Now Playing"

    invoke-virtual {v4, p2, v5, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    invoke-virtual {v4, p2, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    .restart local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .restart local v3    # "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :catch_0
    move-exception v2

    .line 116
    .local v2, "ex":Ljava/lang/Exception;
    const-string v4, "NowPlayingBaseViewModel"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "error in sending media command %s : %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public sendSeekCommand(J)V
    .locals 11
    .param p1, "seekPosition"    # J

    .prologue
    const/4 v7, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isInRemoteControl()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Transport Control - Seek"

    const-string v6, "Remote"

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 133
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v4, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v4, :cond_0

    .line 134
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/SessionModel;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 135
    .local v3, "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    if-eqz v3, :cond_0

    .line 137
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/MediaCommand;

    iget v4, v3, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-direct {v1, v4, p1, p2}, Lcom/microsoft/xbox/smartglass/MediaCommand;-><init>(IJ)V

    .line 138
    .local v1, "command":Lcom/microsoft/xbox/smartglass/MediaCommand;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v1    # "command":Lcom/microsoft/xbox/smartglass/MediaCommand;
    .end local v3    # "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_0
    :goto_1
    return-void

    .line 126
    .end local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    instance-of v4, v4, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    if-eqz v4, :cond_2

    .line 127
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Transport Control - Seek"

    const-string v6, "Now Playing"

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Transport Control - Seek"

    invoke-virtual {v4, v5, v7}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    .restart local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .restart local v3    # "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :catch_0
    move-exception v2

    .line 140
    .local v2, "ex":Ljava/lang/Exception;
    const-string v4, "NowPlayingBaseViewModel"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "error in sending media seek command : %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setActiveTitleLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 0
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->activeTitleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    return-object p1
.end method

.method protected shouldObserveNowPlayingGlobalModel()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public shouldShowIeStopButton()Z
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getIsIeUrlChanging()Z

    move-result v0

    return v0
.end method

.method public shouldShowMediaProgress()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 62
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v3, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v2

    .line 65
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 67
    .local v1, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->isMediaInProgress()Z

    move-result v2

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 186
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    const-string v1, "NowPlayingBaseVM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received update: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingQuickplay:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    invoke-interface {v1, p0}, Lcom/microsoft/xbox/toolkit/IViewUpdate;->updateView(Ljava/lang/Object;)V

    .line 190
    :cond_1
    return-void
.end method
