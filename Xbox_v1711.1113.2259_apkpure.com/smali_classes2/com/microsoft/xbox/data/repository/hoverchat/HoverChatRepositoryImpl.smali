.class public Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;
.super Ljava/lang/Object;
.source "HoverChatRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;


# static fields
.field private static final DISABLED_CLUBS:Ljava/lang/String; = "disabled_clubs_hover_chat"

.field private static final GLOBALLY_ENABLED:Ljava/lang/String; = "hover_chat_globally_enabled"

.field private static final MESSAGES_ENABLED:Ljava/lang/String; = "message_hover_chat_enabled"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activeChatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation
.end field

.field private activeManagerEventObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private activeManagerRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/google/common/base/Optional",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final chatHeadAttachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final chatHeadDetachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final chatHeadRemovedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final clubChatKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatIsOpen:Z

.field private final leftGroupMessageEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final messageKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private final telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 64
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 65
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 66
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->clubChatKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 67
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->messageKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 68
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadAttachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 69
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadDetachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 70
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadRemovedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 71
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->leftGroupMessageEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 73
    invoke-interface {p3}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 76
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->onChatHeadManagerEvent(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;)V

    return-void
.end method

.method private isClubEnabled(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Z
    .locals 4
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "disabled_clubs_hover_chat"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 207
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 206
    :goto_0
    return v0

    .line 207
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEnabledForMessage()Z
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "message_hover_chat_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$getChatHeadManagerEvents$2(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;Lcom/google/common/base/Optional;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;
    .param p1, "activeManager"    # Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 113
    invoke-virtual {p1}, Lcom/google/common/base/Optional;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 115
    .local v0, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    if-nez v0, :cond_0

    .line 116
    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeChatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 117
    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerEventObservable:Lio/reactivex/Observable;

    .line 118
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v1

    .line 125
    :goto_0
    return-object v1

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeChatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    if-ne v0, v2, :cond_1

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerEventObservable:Lio/reactivex/Observable;

    goto :goto_0

    .line 122
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/RxHoverChat;->managerEvents(Lcom/flipkart/chatheads/ui/ChatHeadManager;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v1

    .line 123
    .local v1, "events":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeChatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 124
    iput-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerEventObservable:Lio/reactivex/Observable;

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->onUserSignedOut()V

    return-void
.end method

.method private onChatHeadManagerEvent(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "event":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    instance-of v4, p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    if-eqz v4, :cond_0

    move-object v1, p1

    .line 133
    check-cast v1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    .line 134
    .local v1, "arrangementChangedEvent":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 135
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    .line 137
    .local v2, "expanded":Z
    :goto_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->hoverChatIsOpen:Z

    .line 138
    .local v3, "oldIsOpen":Z
    iput-boolean v2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->hoverChatIsOpen:Z

    .line 140
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->oldArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->hoverChatIsOpen:Z

    if-eq v3, v4, :cond_0

    .line 141
    iget-object v4, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeChatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 143
    .local v0, "activeCount":I
    if-eqz v2, :cond_2

    .line 144
    iget-object v4, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->expanded(I)V

    .line 150
    .end local v0    # "activeCount":I
    .end local v1    # "arrangementChangedEvent":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
    .end local v2    # "expanded":Z
    .end local v3    # "oldIsOpen":Z
    :cond_0
    :goto_1
    return-void

    .line 135
    .restart local v1    # "arrangementChangedEvent":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 146
    .restart local v0    # "activeCount":I
    .restart local v2    # "expanded":Z
    .restart local v3    # "oldIsOpen":Z
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->collapsed(I)V

    goto :goto_1
.end method

.method private onUserSignedOut()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hover_chat_globally_enabled"

    .line 292
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "message_hover_chat_enabled"

    .line 293
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "disabled_clubs_hover_chat"

    .line 294
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 295
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 296
    return-void
.end method

.method private setIsClubEnabled(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;Z)V
    .locals 7
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
    .param p2, "enabled"    # Z

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->isClubEnabled(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Z

    move-result v1

    .line 228
    .local v1, "currentValue":Z
    if-eq v1, p2, :cond_0

    .line 229
    sget-object v3, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setIsClubEnabled: key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " enabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubSettingChanged(JZ)V

    .line 232
    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "disabled_clubs_hover_chat"

    new-instance v5, Ljava/util/HashSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 233
    .local v2, "disabledClubs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "clubId":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 236
    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 241
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "disabled_clubs_hover_chat"

    .line 242
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 243
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 245
    .end local v0    # "clubId":Ljava/lang/String;
    .end local v2    # "disabledClubs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    return-void

    .line 238
    .restart local v0    # "clubId":Ljava/lang/String;
    .restart local v2    # "disabledClubs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setIsEnabledForMessage(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->isEnabledForMessage()Z

    move-result v0

    .line 250
    .local v0, "currentValue":Z
    if-eq v0, p1, :cond_0

    .line 251
    sget-object v1, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setIsEnabledForMessage: enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageSettingChanged(Z)V

    .line 253
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "message_hover_chat_enabled"

    .line 254
    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 255
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 257
    :cond_0
    return-void
.end method


# virtual methods
.method public addClubChatKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 91
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->clubChatKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public addMessageKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->messageKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public getChatHeadLifecycleEvents()Lio/reactivex/Observable;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadAttachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadDetachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadRemovedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getChatHeadManagerEvents()Lio/reactivex/Observable;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 111
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 110
    return-object v0
.end method

.method public getClubChatKeys()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->clubChatKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public getLeftMessageGroupEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->leftGroupMessageEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public getMessageKeys()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->messageKeyRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public hoverChatIsOpen()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->hoverChatIsOpen:Z

    return v0
.end method

.method public isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 195
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    .line 196
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->isClubEnabled(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Z

    move-result v0

    .line 201
    :goto_0
    return v0

    .line 197
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v0, :cond_1

    .line 198
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->isEnabledForMessage()Z

    move-result v0

    goto :goto_0

    .line 200
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabledGlobally()Z
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "hover_chat_globally_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isEnabledGloballyObs()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string v1, "hover_chat_globally_enabled"

    invoke-virtual {v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    .line 266
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->isEnabledGlobally()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 265
    return-object v0
.end method

.method public onChatHeadAttached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 4
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 165
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 167
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->inboxViewed()V

    .line 177
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadAttachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent;->with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 178
    return-void

    .line 169
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_1

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubChatViewed(J)V

    goto :goto_0

    .line 171
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v0, :cond_2

    .line 172
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageViewed(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected key type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onChatHeadDetached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 182
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadDetachedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;->with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 184
    return-void
.end method

.method public onChatHeadRemoved(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 188
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->instanceClosed(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->chatHeadRemovedEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent;->with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 191
    return-void
.end method

.method public onLeaveGroupMessage(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V
    .locals 1
    .param p1, "messageKey"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 286
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->leftGroupMessageEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 288
    return-void
.end method

.method public setActiveChatHeadManager(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 2
    .param p1    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->activeManagerRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Lcom/google/common/base/Optional;->fromNullable(Ljava/lang/Object;)Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method public setEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Z)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "enabled"    # Z

    .prologue
    .line 216
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    .line 217
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->setIsClubEnabled(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;Z)V

    .line 223
    :goto_0
    return-void

    .line 218
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v0, :cond_1

    .line 219
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->setIsEnabledForMessage(Z)V

    goto :goto_0

    .line 221
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEnabledGlobally(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 272
    sget-object v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabledGlobally: enabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hover_chat_globally_enabled"

    .line 274
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 275
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 276
    return-void
.end method
