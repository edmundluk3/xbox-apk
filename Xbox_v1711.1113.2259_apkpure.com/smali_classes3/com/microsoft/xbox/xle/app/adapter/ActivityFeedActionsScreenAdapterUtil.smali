.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static adjustFeedItemViewForActionList(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 6
    .param p0, "v"    # Landroid/view/View;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    const v5, 0x7f0200c8

    const/4 v2, 0x0

    .line 448
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 449
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v3, v4, :cond_3

    const/4 v1, 0x1

    .line 450
    .local v1, "isGameDvr":Z
    :goto_0
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-nez v3, :cond_4

    .line 451
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 458
    :goto_1
    instance-of v2, p0, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_0

    .line 459
    check-cast p0, Landroid/widget/FrameLayout;

    .end local p0    # "v":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 462
    :cond_0
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 463
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 466
    :cond_1
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    if-eqz v2, :cond_2

    .line 468
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 469
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 472
    :cond_2
    return-void

    .end local v1    # "isGameDvr":Z
    .restart local p0    # "v":Landroid/view/View;
    :cond_3
    move v1, v2

    .line 449
    goto :goto_0

    .line 453
    .restart local v1    # "isGameDvr":Z
    :cond_4
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 454
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto :goto_1
.end method

.method static attachCommentSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V
    .locals 3
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    .line 364
    if-nez p1, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;

    .line 370
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
    if-eqz v0, :cond_0

    .line 371
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v1, :cond_2

    .line 372
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 375
    :cond_2
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v1, :cond_3

    .line 376
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    :cond_3
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static attachGameprofileListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 4
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 226
    if-nez p1, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 232
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 234
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gameprofileOrLinkButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gameprofileOrLinkButton:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$7;

    invoke-direct {v3, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$7;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static attachSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 11
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v10, 0x8

    .line 76
    if-nez p1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v5

    .line 82
    .local v5, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 84
    .local v4, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v6, :cond_2

    .line 85
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$2;

    invoke-direct {v7, p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    :cond_2
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v6, :cond_3

    .line 94
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$3;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_3
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v6, :cond_5

    .line 103
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 105
    if-eqz v5, :cond_a

    iget-object v6, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 106
    iget-object v6, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v7, "Club"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 107
    iget-object v6, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v7, "Club/"

    const/4 v8, 0x1

    const-string v9, "0"

    invoke-static {v6, v7, v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 108
    .local v1, "clubId":Ljava/lang/Long;
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_9

    .line 109
    sget-object v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v2

    .line 110
    .local v2, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v6, v7, :cond_4

    .line 111
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 120
    .end local v1    # "clubId":Ljava/lang/Long;
    .end local v2    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_4
    :goto_1
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_5
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v6, :cond_6

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v6

    iget-object v7, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v7

    iget-object v8, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canReportFeedItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;Ljava/lang/String;)Z

    move-result v0

    .line 127
    .local v0, "canReport":Z
    move v3, v0

    .line 128
    .local v3, "hasMoreAction":Z
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 129
    if-eqz v3, :cond_6

    .line 130
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v5, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$2;->lambdaFactory$(ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    .end local v0    # "canReport":Z
    .end local v3    # "hasMoreAction":Z
    :cond_6
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v6, :cond_7

    .line 186
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    :cond_7
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v6, :cond_8

    .line 199
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$5;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    :cond_8
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v6, :cond_0

    .line 212
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$6;

    invoke-direct {v7, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$6;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 114
    .restart local v1    # "clubId":Ljava/lang/Long;
    :cond_9
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_1

    .line 118
    .end local v1    # "clubId":Ljava/lang/Long;
    :cond_a
    iget-object v6, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v6, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_1
.end method

.method static bindActionItem(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V
    .locals 11
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    const v10, 0x7f020125

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    .line 50
    .local v0, "actionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    const v7, 0x7f0e012c

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 51
    .local v6, "timeStamp":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->date:Ljava/util/Date;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 52
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->date:Ljava/util/Date;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateNarratorContentUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 54
    const v7, 0x7f0e0129

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 55
    .local v1, "gamerPic":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7, v10, v10}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 57
    const v7, 0x7f0e012a

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 58
    .local v2, "gamerTag":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->gamertag:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 60
    const v7, 0x7f0e012b

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 61
    .local v3, "realName":Landroid/widget/TextView;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getRealName()Ljava/lang/String;

    move-result-object v4

    .line 62
    .local v4, "strRealName":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    move v7, v8

    :goto_0
    invoke-static {v3, v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 64
    const v7, 0x7f0e012d

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 65
    .local v5, "text":Landroid/widget/TextView;
    sget-object v7, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v0, v7, :cond_1

    iget-object v7, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->text:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    :goto_1
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->text:Ljava/lang/String;

    invoke-static {v5, v8, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 67
    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$1;

    invoke-direct {v7, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    invoke-virtual {p1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void

    .end local v5    # "text":Landroid/widget/TextView;
    :cond_0
    move v7, v9

    .line 62
    goto :goto_0

    .restart local v5    # "text":Landroid/widget/TextView;
    :cond_1
    move v8, v9

    .line 65
    goto :goto_1
.end method

.method public static bindInfoRow(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 6
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 475
    const v3, 0x7f0e06d5

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 476
    .local v2, "tv":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 477
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    .line 478
    .local v0, "actionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$9;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 501
    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v3, v4, :cond_0

    .line 503
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0901a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 504
    .local v1, "padding":I
    invoke-virtual {v2, v5, v1, v5, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 507
    .end local v0    # "actionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .end local v1    # "padding":I
    :cond_0
    return-void

    .line 480
    .restart local v0    # "actionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :pswitch_0
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f07013a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 483
    :pswitch_1
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0704c4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 486
    :pswitch_2
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$9;->$SwitchMap$com$microsoft$xbox$service$model$feeditemactions$FeedItemActionType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 488
    :pswitch_3
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070342

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 491
    :pswitch_4
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070726

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 494
    :pswitch_5
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070c28

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 478
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 486
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static bindSocialBar(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 13
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0c0132

    const v7, 0x7f0c0131

    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 258
    if-nez p1, :cond_0

    .line 308
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 264
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 266
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v5, :cond_1

    .line 267
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v5, :cond_3

    .line 268
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/LikeControl;->enable()V

    .line 269
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    iget-object v9, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v9, v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    invoke-virtual {v5, v9}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setLikeState(Z)V

    .line 275
    :cond_1
    :goto_1
    iget-object v9, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v9, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 276
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v9

    if-eqz v9, :cond_5

    :goto_3
    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 278
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v5, :cond_6

    move v3, v8

    .line 279
    .local v3, "likeCount":I
    :goto_4
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v5, v3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 280
    iget-object v6, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-le v3, v11, :cond_7

    .line 281
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0700d9

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-static {v5, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 280
    :goto_5
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 283
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v5, :cond_8

    move v0, v8

    .line 284
    .local v0, "commentCount":I
    :goto_6
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v5, v0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 285
    iget-object v6, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-le v0, v11, :cond_9

    .line 286
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0700b8

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-static {v5, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 285
    :goto_7
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 288
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v5, :cond_a

    move v4, v8

    .line 289
    .local v4, "shareCount":I
    :goto_8
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 290
    iget-object v6, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-le v4, v11, :cond_b

    .line 291
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0700e9

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v8

    invoke-static {v5, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 290
    :goto_9
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 295
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v5, :cond_2

    .line 296
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v5, :cond_d

    .line 297
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v5, v5, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    if-lez v5, :cond_c

    .line 298
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    .line 307
    :cond_2
    :goto_a
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValuesContainer:Landroid/view/View;

    iget-object v6, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iget-object v7, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iget-object v8, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->updateSocialBarValueContainerVisibility(Landroid/view/View;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;)V

    goto/16 :goto_0

    .line 271
    .end local v0    # "commentCount":I
    .end local v3    # "likeCount":I
    .end local v4    # "shareCount":I
    :cond_3
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/LikeControl;->disable()V

    goto/16 :goto_1

    :cond_4
    move v5, v7

    .line 275
    goto/16 :goto_2

    :cond_5
    move v6, v7

    .line 276
    goto/16 :goto_3

    .line 278
    :cond_6
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v3, v5, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto/16 :goto_4

    .line 281
    .restart local v3    # "likeCount":I
    :cond_7
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700b6

    .line 282
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 283
    :cond_8
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v0, v5, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    goto/16 :goto_6

    .line 286
    .restart local v0    # "commentCount":I
    :cond_9
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700b5

    .line 287
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    .line 288
    :cond_a
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v5, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    goto :goto_8

    .line 291
    .restart local v4    # "shareCount":I
    :cond_b
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700b7

    .line 292
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_9

    .line 300
    :cond_c
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-virtual {v5, v12}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    goto :goto_a

    .line 303
    :cond_d
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-virtual {v5, v12}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    goto :goto_a
.end method

.method static bindSpinner(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/widget/Spinner;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 438
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->ensureSpinnerAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/widget/Spinner;)V

    .line 439
    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 440
    invoke-virtual {p1}, Landroid/widget/Spinner;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 441
    .local v0, "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 442
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 443
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 445
    .end local v0    # "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    :cond_0
    return-void
.end method

.method public static createAndBindFeedItemViewForSharing(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p0, "itemContainer"    # Landroid/view/ViewGroup;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 510
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->createAndBindFeedItemViewForSharing(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V

    .line 511
    return-void
.end method

.method public static createAndBindFeedItemViewForSharing(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V
    .locals 6
    .param p0, "itemContainer"    # Landroid/view/ViewGroup;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "hideTargetUserImage"    # Z

    .prologue
    const/4 v5, 0x0

    .line 514
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 517
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 519
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Shared:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    if-ne v3, v4, :cond_2

    .line 520
    const v3, 0x7f0301b0

    const/4 v4, 0x1

    invoke-static {v3, p0, v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createView(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Landroid/view/View;

    move-result-object v2

    .line 521
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 522
    const v3, 0x7f0e08ba

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 523
    .local v1, "shared":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 524
    const v3, 0x7f0c0019

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 531
    .end local v1    # "shared":Landroid/view/View;
    :cond_0
    :goto_0
    invoke-static {v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 532
    invoke-static {v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->adjustFeedItemViewForActionList(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 533
    const v3, 0x7f0e0899

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 535
    if-eqz p2, :cond_1

    .line 536
    const v3, 0x7f0e08b1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 539
    :cond_1
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 540
    return-void

    .line 527
    .end local v2    # "v":Landroid/view/View;
    :cond_2
    const v3, 0x7f0301ab

    invoke-static {v3, p0, v0, v5}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createView(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Landroid/view/View;

    move-result-object v2

    .line 528
    .restart local v2    # "v":Landroid/view/View;
    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method static detachSocialBarListeners(Landroid/view/View;)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 245
    if-eqz p0, :cond_0

    .line 246
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 248
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 249
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 250
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 251
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 252
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 253
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 255
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    :cond_0
    return-void
.end method

.method static ensureSpinnerAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/widget/Spinner;)V
    .locals 5
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "spinner"    # Landroid/widget/Spinner;

    .prologue
    .line 426
    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v3

    if-nez v3, :cond_0

    .line 427
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 428
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v2, :cond_0

    .line 429
    invoke-virtual {p1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 430
    .local v1, "ctx":Landroid/content/Context;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v3, 0x7f030202

    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->values()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v1, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 431
    .local v0, "adpSpinner":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;>;"
    const v3, 0x7f03020a

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 432
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 435
    .end local v0    # "adpSpinner":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;>;"
    .end local v1    # "ctx":Landroid/content/Context;
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_0
    return-void
.end method

.method static handleDeleteComment(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V
    .locals 7
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    .line 416
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700b9

    .line 417
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$8;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$8;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 422
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 416
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 423
    return-void
.end method

.method static synthetic lambda$attachCommentSocialBarListeners$3(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->likeComments(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    return-void
.end method

.method static synthetic lambda$attachCommentSocialBarListeners$5(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 9
    .param p0, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    const-string v1, "Club"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 378
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->rootPath:Ljava/lang/String;

    const-string v1, "Club/"

    const/4 v2, 0x1

    const-string v3, "0"

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 379
    .local v6, "clubId":Ljava/lang/Long;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 380
    new-instance v8, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-direct {v8, v0, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 381
    .local v8, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 382
    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0e0c23

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 384
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v7

    .line 385
    .local v7, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    if-nez v0, :cond_1

    .line 386
    :cond_0
    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0e0c26

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 389
    :cond_1
    invoke-static {p1, p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Ljava/lang/Long;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 400
    invoke-virtual {v8}, Landroid/widget/PopupMenu;->show()V

    .line 405
    .end local v6    # "clubId":Ljava/lang/Long;
    .end local v7    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .end local v8    # "popup":Landroid/widget/PopupMenu;
    :cond_2
    :goto_0
    return-void

    .line 403
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Comment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic lambda$attachCommentSocialBarListeners$6(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p2, "v12"    # Landroid/view/View;

    .prologue
    .line 410
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->handleDeleteComment(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$0(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->startShareFlow()V

    .line 122
    return-void
.end method

.method static synthetic lambda$attachSocialBarListeners$2(ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
    .locals 7
    .param p0, "canReport"    # Z
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p3, "v1"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0e0c1b

    const v5, 0x7f0e0c1a

    const v4, 0x7f0e0c19

    .line 131
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 132
    .local v0, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0f0001

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 133
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c16

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 134
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c17

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 135
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c18

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 136
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c1e

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 137
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c1c

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 138
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c1d

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 140
    if-eqz p0, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 141
    :cond_0
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 142
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/Menu;->removeItem(I)V

    .line 143
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/Menu;->removeItem(I)V

    .line 153
    :goto_0
    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 180
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 181
    return-void

    .line 145
    :cond_1
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v2, "Club"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/Menu;->removeItem(I)V

    .line 149
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Landroid/view/MenuItem;)Z
    .locals 7
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .param p2, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 154
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 178
    :goto_0
    return v6

    .line 156
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getIsCapture()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Gameclip:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 159
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Screenshot:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 162
    :cond_1
    const-string v0, "Could not find clipId or screenshotId for capture"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->SocialBar:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 169
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v1, "Club/"

    const-string v2, "0"

    invoke-static {v0, v1, v6, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "clubId":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x7f0e0c19
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Ljava/lang/Long;Landroid/view/MenuItem;)Z
    .locals 7
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p2, "clubId"    # Ljava/lang/Long;
    .param p3, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x1

    .line 390
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 398
    :goto_0
    return v6

    .line 392
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 395
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->path:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c25
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static updateCommentsSocialBar(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V
    .locals 9
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "action"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 311
    if-nez p1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;

    .line 317
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
    if-eqz v0, :cond_0

    .line 318
    if-eqz p0, :cond_7

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne v2, v5, :cond_7

    .line 319
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBar:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 321
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v2, :cond_2

    .line 322
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_4

    .line 323
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/LikeControl;->enable()V

    .line 324
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    iget-object v5, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v5, v5, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setLikeState(Z)V

    .line 333
    :cond_2
    :goto_1
    const/4 v1, 0x0

    .line 334
    .local v1, "likeCount":I
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_3

    .line 335
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v1, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 336
    if-nez v1, :cond_3

    iget-object v2, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v2, :cond_3

    .line 337
    const/4 v1, 0x1

    .line 340
    :cond_3
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 341
    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-le v1, v8, :cond_5

    .line 342
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0700d9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v2, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 341
    :goto_2
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 346
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 347
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletable(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Z

    move-result v5

    invoke-static {v2, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 348
    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v2, p2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    .line 349
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    .line 348
    :goto_3
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 350
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 351
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 352
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 353
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 355
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 326
    .end local v1    # "likeCount":I
    :cond_4
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/LikeControl;->disable()V

    goto :goto_1

    .line 342
    .restart local v1    # "likeCount":I
    :cond_5
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0700b6

    .line 343
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_6
    move v2, v4

    .line 349
    goto :goto_3

    .line 358
    .end local v1    # "likeCount":I
    :cond_7
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->socialBar:Landroid/widget/LinearLayout;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_0
.end method
