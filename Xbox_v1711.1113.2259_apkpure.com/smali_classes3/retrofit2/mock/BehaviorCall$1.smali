.class Lretrofit2/mock/BehaviorCall$1;
.super Ljava/lang/Object;
.source "BehaviorCall.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lretrofit2/mock/BehaviorCall;->enqueue(Lretrofit2/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lretrofit2/mock/BehaviorCall;

.field final synthetic val$callback:Lretrofit2/Callback;


# direct methods
.method constructor <init>(Lretrofit2/mock/BehaviorCall;Lretrofit2/Callback;)V
    .locals 0
    .param p1, "this$0"    # Lretrofit2/mock/BehaviorCall;

    .prologue
    .line 61
    .local p0, "this":Lretrofit2/mock/BehaviorCall$1;, "Lretrofit2/mock/BehaviorCall$1;"
    iput-object p1, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iput-object p2, p0, Lretrofit2/mock/BehaviorCall$1;->val$callback:Lretrofit2/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method delaySleep()Z
    .locals 7

    .prologue
    .line 63
    .local p0, "this":Lretrofit2/mock/BehaviorCall$1;, "Lretrofit2/mock/BehaviorCall$1;"
    iget-object v1, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v1, v1, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4}, Lretrofit2/mock/NetworkBehavior;->calculateDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 64
    .local v2, "sleepMs":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 66
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lretrofit2/mock/BehaviorCall$1;->val$callback:Lretrofit2/Callback;

    iget-object v4, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    new-instance v5, Ljava/io/IOException;

    const-string v6, "canceled"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v4, v5}, Lretrofit2/Callback;->onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V

    .line 69
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 76
    .local p0, "this":Lretrofit2/mock/BehaviorCall$1;, "Lretrofit2/mock/BehaviorCall$1;"
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-boolean v0, v0, Lretrofit2/mock/BehaviorCall;->canceled:Z

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->val$callback:Lretrofit2/Callback;

    iget-object v1, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "canceled"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lretrofit2/Callback;->onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v0, v0, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    invoke-virtual {v0}, Lretrofit2/mock/NetworkBehavior;->calculateIsFailure()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    invoke-virtual {p0}, Lretrofit2/mock/BehaviorCall$1;->delaySleep()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->val$callback:Lretrofit2/Callback;

    iget-object v1, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v2, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v2, v2, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    invoke-virtual {v2}, Lretrofit2/mock/NetworkBehavior;->failureException()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lretrofit2/Callback;->onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 82
    :cond_2
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v0, v0, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    invoke-virtual {v0}, Lretrofit2/mock/NetworkBehavior;->calculateIsError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    invoke-virtual {p0}, Lretrofit2/mock/BehaviorCall$1;->delaySleep()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->val$callback:Lretrofit2/Callback;

    iget-object v1, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v2, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v2, v2, Lretrofit2/mock/BehaviorCall;->behavior:Lretrofit2/mock/NetworkBehavior;

    invoke-virtual {v2}, Lretrofit2/mock/NetworkBehavior;->createErrorResponse()Lretrofit2/Response;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lretrofit2/Callback;->onResponse(Lretrofit2/Call;Lretrofit2/Response;)V

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lretrofit2/mock/BehaviorCall$1;->this$0:Lretrofit2/mock/BehaviorCall;

    iget-object v0, v0, Lretrofit2/mock/BehaviorCall;->delegate:Lretrofit2/Call;

    new-instance v1, Lretrofit2/mock/BehaviorCall$1$1;

    invoke-direct {v1, p0}, Lretrofit2/mock/BehaviorCall$1$1;-><init>(Lretrofit2/mock/BehaviorCall$1;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    goto :goto_0
.end method
