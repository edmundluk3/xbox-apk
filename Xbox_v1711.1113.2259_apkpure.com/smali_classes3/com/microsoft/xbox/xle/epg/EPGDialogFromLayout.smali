.class public Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "EPGDialogFromLayout.java"


# instance fields
.field private nondismissableView:Landroid/view/View;


# direct methods
.method public constructor <init>(IZZ)V
    .locals 6
    .param p1, "layoutResourceId"    # I
    .param p2, "center"    # Z
    .param p3, "softDismiss"    # Z

    .prologue
    const/4 v5, 0x1

    .line 16
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0802da

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->setContentView(I)V

    .line 20
    const v3, 0x7f0e0b9d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->nondismissableView:Landroid/view/View;

    .line 21
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->nondismissableView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 22
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->nondismissableView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setClickable(Z)V

    .line 23
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->nondismissableView:Landroid/view/View;

    new-instance v4, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$1;-><init>(Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    :cond_0
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->setCanceledOnTouchOutside(Z)V

    .line 33
    if-eqz p2, :cond_4

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/view/Window;->setGravity(I)V

    .line 39
    :goto_0
    if-eqz p3, :cond_1

    .line 40
    const v3, 0x7f0e058e

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 41
    .local v2, "view":Landroid/view/View;
    new-instance v3, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$2;

    invoke-direct {v3, p0, v2}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$2;-><init>(Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    const v3, 0x7f0e0b00

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "root":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 56
    new-instance v3, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$3;

    invoke-direct {v3, p0, v1}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$3;-><init>(Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    :cond_2
    const v3, 0x7f0e022a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 69
    .local v0, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    if-eqz v0, :cond_3

    .line 70
    new-instance v3, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$4;

    invoke-direct {v3, p0, v0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout$4;-><init>(Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    :cond_3
    return-void

    .line 36
    .end local v0    # "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v1    # "root":Landroid/view/View;
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x50

    invoke-virtual {v3, v4}, Landroid/view/Window;->setGravity(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->onClosePressed()V

    return-void
.end method

.method private onClosePressed()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->safeDismiss()V

    .line 97
    return-void
.end method

.method public static show(IZZ)Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;
    .locals 2
    .param p0, "layoutResourceId"    # I
    .param p1, "center"    # Z
    .param p2, "softDismiss"    # Z

    .prologue
    .line 85
    new-instance v0, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;-><init>(IZZ)V

    .line 86
    .local v0, "dlg":Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 87
    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->onClosePressed()V

    .line 93
    return-void
.end method
