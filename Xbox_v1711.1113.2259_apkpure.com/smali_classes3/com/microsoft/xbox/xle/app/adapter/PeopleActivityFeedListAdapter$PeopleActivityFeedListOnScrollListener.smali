.class Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "PeopleActivityFeedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeopleActivityFeedListOnScrollListener"
.end annotation


# instance fields
.field private final floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

.field impressionTimer:Landroid/os/CountDownTimer;

.field private shouldSendImpressionEvent:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;)V
    .locals 1
    .param p2, "floatingRowHelper"    # Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    .line 271
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->impressionTimer:Landroid/os/CountDownTimer;

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->shouldSendImpressionEvent:Z

    .line 272
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    .line 273
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->shouldSendImpressionEvent:Z

    return v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;
    .param p1, "x1"    # Z

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->shouldSendImpressionEvent:Z

    return p1
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 7
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->impressionTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->impressionTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 279
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->impressionTimer:Landroid/os/CountDownTimer;

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->shouldSendImpressionEvent:Z

    .line 282
    :cond_0
    if-nez p2, :cond_1

    .line 283
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;

    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;JJLandroid/support/v7/widget/RecyclerView;)V

    .line 304
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->impressionTimer:Landroid/os/CountDownTimer;

    .line 305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->shouldSendImpressionEvent:Z

    .line 307
    :cond_1
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 10
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 311
    if-eqz p1, :cond_1

    .line 312
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    if-eqz v6, :cond_0

    .line 313
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    invoke-virtual {v6, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->onScroll(Landroid/support/v7/widget/RecyclerView;II)V

    .line 315
    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/LinearLayoutManager;

    .line 316
    .local v2, "llm":Landroid/support/v7/widget/LinearLayoutManager;
    if-eqz v2, :cond_1

    .line 317
    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v3

    .line 318
    .local v3, "position":I
    invoke-virtual {v2, v3}, Landroid/support/v7/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    .line 319
    .local v0, "firstChild":Landroid/view/View;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    move-result-object v7

    new-instance v8, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    if-nez v0, :cond_2

    const/4 v6, 0x0

    :goto_0
    invoke-direct {v8, v3, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;-><init>(II)V

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setLastSelectedPosition(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;)V

    .line 321
    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v5

    .line 322
    .local v5, "totalItemCount":I
    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v1

    .line 323
    .local v1, "lastVisibleItem":I
    int-to-double v6, v5

    const-wide/high16 v8, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x402e000000000000L    # 15.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_3

    add-int/lit8 v4, v5, -0xf

    .line 324
    .local v4, "targetFetchItem":I
    :goto_1
    if-lez v4, :cond_1

    if-gt v4, v1, :cond_1

    .line 325
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->tryLoadMoreFeed()V

    .line 329
    .end local v0    # "firstChild":Landroid/view/View;
    .end local v1    # "lastVisibleItem":I
    .end local v2    # "llm":Landroid/support/v7/widget/LinearLayoutManager;
    .end local v3    # "position":I
    .end local v4    # "targetFetchItem":I
    .end local v5    # "totalItemCount":I
    :cond_1
    return-void

    .line 319
    .restart local v0    # "firstChild":Landroid/view/View;
    .restart local v2    # "llm":Landroid/support/v7/widget/LinearLayoutManager;
    .restart local v3    # "position":I
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    goto :goto_0

    .line 323
    .restart local v1    # "lastVisibleItem":I
    .restart local v5    # "totalItemCount":I
    :cond_3
    add-int/lit8 v4, v5, -0x1

    goto :goto_1
.end method
