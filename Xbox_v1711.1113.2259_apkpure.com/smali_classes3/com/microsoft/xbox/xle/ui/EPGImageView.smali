.class public Lcom/microsoft/xbox/xle/ui/EPGImageView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
.source "EPGImageView.java"


# instance fields
.field private mFadeInDuration:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/EPGImageView;->mFadeInDuration:J

    .line 31
    const/4 v0, 0x0

    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 32
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 37
    if-nez p1, :cond_0

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 47
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 65
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "animate":Z
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/EPGImageView;->mFadeInDuration:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 55
    const/4 v0, 0x1

    .line 58
    :cond_1
    if-eqz v0, :cond_2

    .line 59
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setAlpha(F)V

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/EPGImageView;->mFadeInDuration:J

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 63
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method public setImageURI3(Ljava/lang/String;J)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "fadeInDuration"    # J

    .prologue
    .line 25
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/ui/EPGImageView;->mFadeInDuration:J

    .line 26
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 27
    return-void
.end method
