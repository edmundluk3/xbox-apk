.class public Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
.super Ljava/lang/Object;
.source "ParentItemViewHolder.java"


# instance fields
.field private final icon:Landroid/widget/TextView;

.field private final name:Landroid/widget/TextView;

.field private final type:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const v0, 0x7f0e0831

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->name:Landroid/widget/TextView;

    .line 17
    const v0, 0x7f0e0832

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->type:Landroid/widget/TextView;

    .line 18
    const v0, 0x7f0e0830

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->icon:Landroid/widget/TextView;

    .line 19
    return-void
.end method


# virtual methods
.method public getIconView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->icon:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNameView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->name:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTypeView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->type:Landroid/widget/TextView;

    return-object v0
.end method
