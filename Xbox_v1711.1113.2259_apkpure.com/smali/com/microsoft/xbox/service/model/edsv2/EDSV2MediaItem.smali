.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.super Ljava/lang/Object;
.source "EDSV2MediaItem.java"


# annotations
.annotation runtime Lcom/google/gson/annotations/JsonAdapter;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;
.end annotation


# static fields
.field private static final MAX_RATING:I = 0x5

.field private static final PURCHASE_KEY:Ljava/lang/String; = "Purchase"

.field private static final RELATED_PRODUCT_ADD_ON_PARENT_KEY:Ljava/lang/String; = "addonparent"

.field private static final RELATED_PRODUCT_BUNDLE_KEY:Ljava/lang/String; = "bundle"


# instance fields
.field public Airings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation
.end field

.field public AllTimeAverageRating:F

.field public AllTimeRatingCount:I

.field public BundlePrimaryItemId:Ljava/lang/String;

.field public Contributors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation
.end field

.field public Description:Ljava/lang/String;

.field public Genres:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation
.end field

.field public HasActivities:Z

.field public ID:Ljava/lang/String;

.field public Images:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field public ImpressionGuid:Ljava/lang/String;

.field public IsBundle:Z

.field public IsPartOfAnyBundle:Z

.field public MediaItemType:Ljava/lang/String;

.field public Name:Ljava/lang/String;

.field private ReleaseDate:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public UserRatingCount:I

.field protected affirmationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected applicationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected availabilityId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected availableOnPlatforms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ">;"
        }
    .end annotation
.end field

.field protected backgroundImageUrl:Ljava/lang/String;

.field protected bigCatProductId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

.field protected bundleIdsIncludeThisItem:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected bundlePrimaryItemTitleId:J

.field protected bundledProductsIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected bundledSkus:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;",
            ">;"
        }
    .end annotation
.end field

.field protected contentRating:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected currencyCode:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private desktopArchitecture:Ljava/lang/String;

.field private desktopOSVersion:Ljava/lang/String;

.field protected desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected developerName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected foregroundImageUrl:Ljava/lang/String;

.field private hasValidAvailability:Z

.field protected iconUrl:Ljava/lang/String;

.field protected imageUrl:Ljava/lang/String;

.field protected isBackCompatProduct:Z

.field protected isXPA:Z

.field protected listPrice:Ljava/math/BigDecimal;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field protected minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected msrp:Ljava/math/BigDecimal;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected nowPlayingTitleId:J

.field protected packageFamilyName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected parentItemBigCatIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected partnerMediaId:Ljava/lang/String;

.field protected posterImageUrl:Ljava/lang/String;

.field protected productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected providers:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Providers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation
.end field

.field protected publisherName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected relatedProducts:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;",
            ">;"
        }
    .end annotation
.end field

.field protected skuId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected slideshows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field protected slideshowsForPlatforms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;>;"
        }
    .end annotation
.end field

.field private subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public titleId:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TitleId"
    .end annotation
.end field

.field protected wideImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 108
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 112
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 128
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 148
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 159
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 175
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 108
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 112
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 128
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 148
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 159
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 178
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 179
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 180
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Description:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Description:Ljava/lang/String;

    .line 181
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->HasActivities:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->HasActivities:Z

    .line 182
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 183
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ReleaseDate:Ljava/util/Date;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ReleaseDate:Ljava/util/Date;

    .line 184
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    .line 185
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    .line 186
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->partnerMediaId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->partnerMediaId:Ljava/lang/String;

    .line 187
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->nowPlayingTitleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->nowPlayingTitleId:J

    .line 188
    iget v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeAverageRating:F

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeAverageRating:F

    .line 189
    iget v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    .line 190
    iget v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->UserRatingCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->UserRatingCount:I

    .line 191
    iget v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    .line 192
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Genres:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Genres:Ljava/util/List;

    .line 193
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Contributors:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Contributors:Ljava/util/ArrayList;

    .line 194
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 197
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    .line 198
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 199
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availabilityId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availabilityId:Ljava/lang/String;

    .line 200
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->skuId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->skuId:Ljava/lang/String;

    .line 201
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->listPrice:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->listPrice:Ljava/math/BigDecimal;

    .line 202
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->msrp:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->msrp:Ljava/math/BigDecimal;

    .line 203
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->currencyCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->currencyCode:Ljava/lang/String;

    .line 204
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    .line 205
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 206
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    .line 207
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->affirmationId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->affirmationId:Ljava/lang/String;

    .line 208
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 209
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->developerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->developerName:Ljava/lang/String;

    .line 210
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->publisherName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->publisherName:Ljava/lang/String;

    .line 211
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 212
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parentItemBigCatIds:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parentItemBigCatIds:Ljava/util/List;

    .line 213
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    .line 214
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isBackCompatProduct:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isBackCompatProduct:Z

    .line 215
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 216
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 217
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 218
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    .line 219
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    .line 220
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    .line 221
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 222
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 223
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    .line 224
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopArchitecture:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopArchitecture:Ljava/lang/String;

    .line 225
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopOSVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopOSVersion:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)V
    .locals 3
    .param p1, "autoSuggestItem"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 108
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 112
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 116
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 128
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 148
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 159
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 243
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 245
    iget-object v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->productId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    .line 246
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 247
    iget-object v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 248
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->getValidIconString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;-><init>()V

    .line 250
    .local v0, "icon":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    iget v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->height:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setHeight(I)V

    .line 251
    iget v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->width:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setWidth(I)V

    .line 252
    iget-object v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->imageType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setPurpose(Ljava/lang/String;)V

    .line 253
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->getValidIconString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->setUrl(Ljava/lang/String;)V

    .line 254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->getValidIconString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 259
    .end local v0    # "icon":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 2
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 108
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 112
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 128
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 148
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 159
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 229
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 232
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 234
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateDisplaySkuAvailabilities(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 235
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateAlternatedIds(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 236
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateLocalizedProperty(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 237
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateMarketProperty(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 238
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 239
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateProductProperties(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 240
    return-void
.end method

.method protected static chooseAvailability(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;",
            ">;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;"
        }
    .end annotation

    .prologue
    .line 364
    .local p0, "availabilities":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 365
    const v1, 0x7fffffff

    .line 366
    .local v1, "currentLowestRank":I
    const/4 v2, 0x0

    .line 367
    .local v2, "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;

    .line 370
    .local v0, "availability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    if-eqz v0, :cond_0

    iget v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->displayRank:I

    if-gt v4, v1, :cond_0

    .line 372
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->getActions()Ljava/util/List;

    move-result-object v4

    const-string v5, "Purchase"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->remediationRequired:Z

    if-nez v4, :cond_0

    .line 374
    move-object v2, v0

    .line 376
    iget v1, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->displayRank:I

    goto :goto_0

    .line 383
    .end local v0    # "availability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    .end local v1    # "currentLowestRank":I
    .end local v2    # "result":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    :cond_1
    const/4 v2, 0x0

    :cond_2
    return-object v2
.end method

.method protected static chooseAvailabilityWithoutSku(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;",
            ">;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;"
        }
    .end annotation

    .prologue
    .line 348
    .local p0, "displaySkuAvailabilities":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 350
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    .line 351
    .local v1, "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->getAvailabilities()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 352
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->getAvailabilities()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;

    .line 353
    .local v0, "availability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->getActions()Ljava/util/List;

    move-result-object v4

    const-string v5, "Purchase"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 360
    .end local v0    # "availability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    .end local v1    # "displaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBoxArtImage()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 712
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    if-nez v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getBoxArtImage(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    return-object v0
.end method

.method static synthetic lambda$updateAvailableOnPlatforms$0(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;)I
    .locals 2
    .param p0, "pkg1"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    .param p1, "pkg2"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;

    .prologue
    .line 330
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->packageRank:I

    iget v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->packageRank:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private parseHardwareRequirements(Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "requirementsData"    # Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;
    .param p2, "processor"    # Ljava/lang/String;
    .param p3, "graphics"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 902
    .local p4, "otherHardwareRequirements":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopOSVersion:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 903
    new-instance v1, Landroid/support/v4/util/Pair;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c4f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopOSVersion:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->add(Landroid/support/v4/util/Pair;)V

    .line 905
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopArchitecture:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 906
    new-instance v1, Landroid/support/v4/util/Pair;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c49

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopArchitecture:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->add(Landroid/support/v4/util/Pair;)V

    .line 908
    :cond_1
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 909
    .local v0, "otherHW":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 910
    sget-object v2, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->getLocalizedSystemRequirementValue(Ljava/lang/String;)Landroid/support/v4/util/Pair;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->add(Landroid/support/v4/util/Pair;)V

    goto :goto_0

    .line 913
    .end local v0    # "otherHW":Ljava/lang/String;
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 914
    new-instance v1, Landroid/support/v4/util/Pair;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c52

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->add(Landroid/support/v4/util/Pair;)V

    .line 916
    :cond_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 917
    new-instance v1, Landroid/support/v4/util/Pair;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c4c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->add(Landroid/support/v4/util/Pair;)V

    .line 919
    :cond_5
    return-void
.end method

.method private setMediaItemType(Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;)V
    .locals 1
    .param p1, "mediaItemType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .prologue
    .line 752
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 753
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 754
    return-void
.end method

.method private updateAlternatedIds(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 6
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 262
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 264
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getAlternateIds()Ljava/util/List;

    move-result-object v1

    .line 265
    .local v1, "ids":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;

    .line 266
    .local v0, "id":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 267
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->LegacyXboxProductId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 268
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 270
    :cond_1
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->idType:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->XboxTitleId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 271
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;->value:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    .line 272
    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    goto :goto_0

    .line 276
    .end local v0    # "id":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;
    :cond_2
    return-void
.end method

.method private updateAvailableOnPlatforms(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem$$Lambda$1;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {p1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 331
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    .line 332
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;

    .line 333
    .local v2, "propertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->getPlatformDependencies()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;

    .line 334
    .local v0, "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->platformName:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->fromPlatformName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v1

    .line 335
    .local v1, "platformType":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 336
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    if-ne v1, v5, :cond_1

    .line 338
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;->getArchitecture()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopArchitecture:Ljava/lang/String;

    .line 339
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->getVersionString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopOSVersion:Ljava/lang/String;

    goto :goto_0

    .line 344
    .end local v0    # "platformDependency":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
    .end local v1    # "platformType":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .end local v2    # "propertyPackage":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;
    :cond_2
    return-void
.end method

.method private updateBundlesInfo(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;)V
    .locals 1
    .param p1, "properties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    .prologue
    .line 315
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->getBundledSkus()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    .line 318
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->isBundle:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledSkus:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsBundle:Z

    .line 319
    return-void

    .line 318
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 2
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 456
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 457
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getMarketProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;

    move-result-object v0

    .line 459
    .local v0, "marketProperty":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;
    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->getDefaultContentRating()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->contentRating:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;

    .line 462
    :cond_0
    return-void
.end method

.method private updateLocalizedProperty(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 6
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 387
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 390
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;

    move-result-object v2

    .line 391
    .local v2, "property":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;
    if-eqz v2, :cond_2

    .line 393
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->getImages()Ljava/util/List;

    move-result-object v0

    .line 394
    .local v0, "bigCatImages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    .line 395
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 396
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;

    .line 397
    .local v1, "image":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;
    if-eqz v1, :cond_0

    .line 398
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    new-instance v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-direct {v5, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 403
    .end local v1    # "image":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getBoxArtImage(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 405
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->developerName:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->developerName:Ljava/lang/String;

    .line 406
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->productTitle:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 407
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->productDescription:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Description:Ljava/lang/String;

    .line 408
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->publisherName:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->publisherName:Ljava/lang/String;

    .line 411
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->eligibilityProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;

    if-eqz v3, :cond_2

    .line 412
    iget-object v3, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->eligibilityProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->affirmationId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;->getSubscriptionType(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 415
    .end local v0    # "bigCatImages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;>;"
    :cond_2
    return-void
.end method

.method private updateMarketProperty(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 9
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 418
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 420
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getMarketProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;

    move-result-object v0

    .line 421
    .local v0, "marketProperty":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;
    if-eqz v0, :cond_4

    .line 422
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->getReleaseDate()Ljava/util/Date;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ReleaseDate:Ljava/util/Date;

    .line 424
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->getUsageData()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 425
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->getUsageData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;

    .line 426
    .local v1, "ratingData":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;
    if-eqz v1, :cond_0

    iget-object v6, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->aggregateTimeSpan:Ljava/lang/String;

    sget-object v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->AllTime:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 428
    iget v5, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->averageRating:F

    const/high16 v6, 0x40a00000    # 5.0f

    div-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeAverageRating:F

    .line 429
    iget v5, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->ratingCount:I

    iput v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    .line 435
    .end local v1    # "ratingData":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->relatedProducts()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    .line 436
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parentItemBigCatIds:Ljava/util/List;

    .line 437
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    .line 439
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->relatedProducts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;

    .line 440
    .local v2, "relatedProduct":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;
    iget-object v5, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;->relationshipType:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const/4 v5, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 442
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    iget-object v7, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;->relatedProductId:Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 440
    :sswitch_0
    const-string v8, "bundle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v5, v4

    goto :goto_1

    :sswitch_1
    const-string v8, "addonparent"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v5, v3

    goto :goto_1

    .line 446
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parentItemBigCatIds:Ljava/util/List;

    iget-object v7, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;->relatedProductId:Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 451
    .end local v2    # "relatedProduct":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_5

    :goto_2
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsPartOfAnyBundle:Z

    .line 453
    :cond_4
    return-void

    :cond_5
    move v3, v4

    .line 451
    goto :goto_2

    .line 440
    :sswitch_data_0
    .sparse-switch
        -0x5220cf7e -> :sswitch_0
        0x4c4e3aea -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateProductProperties(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 1
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 465
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 467
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    .line 468
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    iget-object v0, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->packageFamilyName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->packageFamilyName:Ljava/lang/String;

    .line 472
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getApplicationIdForPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->applicationId:Ljava/lang/String;

    .line 473
    return-void
.end method

.method private updateXPAInfo(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;)V
    .locals 1
    .param p1, "properties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    .prologue
    .line 322
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->xboxXPA:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    .line 323
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->hardwareProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    .line 325
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;->getPackages()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateAvailableOnPlatforms(Ljava/util/List;)V

    .line 327
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 923
    if-ne p1, p0, :cond_1

    .line 929
    :cond_0
    :goto_0
    return v1

    .line 925
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 926
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 928
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 929
    .local v0, "that":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 931
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getAffirmationId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 849
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->affirmationId:Ljava/lang/String;

    return-object v0
.end method

.method public getAirings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 655
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Airings:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAllTimeRatingCount()I
    .locals 1

    .prologue
    .line 744
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeRatingCount:I

    return v0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 477
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->applicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getBackgroundImageURIForArtists(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getAvailabilityId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 795
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availabilityId:Ljava/lang/String;

    return-object v0
.end method

.method public getAvailableOnPlatforms()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 868
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availableOnPlatforms:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 740
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->AllTimeAverageRating:F

    return v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getBackgroundImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->backgroundImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getBigCatProductId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 778
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    return-object v0
.end method

.method public getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 783
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    return-object v0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 699
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtImage()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 700
    const-string/jumbo v0, "transparent"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getBackgroundColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 701
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 707
    :goto_0
    return v0

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 707
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBundleIdsIncludeThisItem()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 844
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundleIdsIncludeThisItem:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBundlePrimaryItemTitleId()J
    .locals 2

    .prologue
    .line 950
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundlePrimaryItemTitleId:J

    return-wide v0
.end method

.method public getBundledProductsIds()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 824
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundledProductsIds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 819
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->currencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Description:Ljava/lang/String;

    return-object v0
.end method

.method public getDeveloper()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 829
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->developerName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGeneres()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation

    .prologue
    .line 769
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Genres:Ljava/util/List;

    return-object v0
.end method

.method public getHasSmartGlassActivity()Z
    .locals 1

    .prologue
    .line 623
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->HasActivities:Z

    return v0
.end method

.method public getIcon2x1Url()Ljava/lang/String;
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->wideImageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getWideURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->wideImageUrl:Ljava/lang/String;

    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->wideImageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 615
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->wideImageUrl:Ljava/lang/String;

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->wideImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 688
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 689
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtImage()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->boxArtImage:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getImages()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 647
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getImpressionGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ImpressionGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getIsProgrammingOverride()Z
    .locals 1

    .prologue
    .line 679
    const/4 v0, 0x0

    return v0
.end method

.method public getLegacyId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 790
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    return-object v0
.end method

.method public getListPrice()Ljava/math/BigDecimal;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 809
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->listPrice:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getMediaType()I
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v1, :cond_2

    .line 628
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629
    :cond_0
    const/4 v0, 0x0

    .line 639
    :goto_0
    return v0

    .line 631
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 632
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 639
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v0

    goto :goto_0

    .line 635
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    goto :goto_1
.end method

.method public getMinRequirements()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 872
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    if-eqz v0, :cond_0

    .line 873
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 874
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iget-object v1, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->minimumProcessor:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iget-object v2, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->minimumGraphics:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    .line 877
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->getMinimumHardware()Ljava/util/List;

    move-result-object v3

    .line 874
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parseHardwareRequirements(Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    if-nez v0, :cond_1

    .line 880
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 879
    :goto_0
    return-object v0

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->minRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 881
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->getSystemRequirements()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getMsrp()Ljava/math/BigDecimal;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 814
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->msrp:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getNowPlayingTitleId()J
    .locals 2

    .prologue
    .line 498
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->nowPlayingTitleId:J

    return-wide v0
.end method

.method public getPackageFamilyName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 859
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->packageFamilyName:Ljava/lang/String;

    return-object v0
.end method

.method public getParentCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 506
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentIDs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 764
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentItemBigCatIds()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 839
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parentItemBigCatIds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getParentItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 757
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentMediaType()I
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    return v0
.end method

.method public getParentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPartnerMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->partnerMediaId:Ljava/lang/String;

    return-object v0
.end method

.method public getPosterImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->posterImageUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getPosterImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->posterImageUrl:Ljava/lang/String;

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->posterImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    return-object v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 651
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->providers:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 834
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->publisherName:Ljava/lang/String;

    return-object v0
.end method

.method public getRecRequirements()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 885
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    if-eqz v0, :cond_0

    .line 886
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 888
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iget-object v1, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->recommendedProcessor:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    iget-object v2, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->recommendedGraphics:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    .line 891
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;->getRecommendedHardware()Ljava/util/List;

    move-result-object v3

    .line 888
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->parseHardwareRequirements(Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 893
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->desktopSystemRequirementsRaw:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;

    if-nez v0, :cond_1

    .line 894
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 893
    :goto_0
    return-object v0

    .line 894
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->recRequirements:Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;

    .line 895
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;->getSystemRequirements()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getReleaseDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ReleaseDate:Ljava/util/Date;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->convertToUTC(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getSkuId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 804
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->skuId:Ljava/lang/String;

    return-object v0
.end method

.method public getSlideShow()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 540
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 541
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    .line 542
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 543
    .local v0, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    const-string v2, "ImageGallery"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Screenshot"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 549
    .end local v0    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshows:Ljava/util/List;

    return-object v1
.end method

.method public getSlideshowsForPlatform(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)Ljava/util/List;
    .locals 6
    .param p1, "selectedPlatformType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554
    const/4 v1, 0x0

    .line 555
    .local v1, "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 556
    new-instance v3, Ljava/util/HashMap;

    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v4

    array-length v4, v4

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    .line 557
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 558
    .local v0, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    const-string v4, "ImageGallery"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "Screenshot"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 559
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getImagePositionInfo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 560
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getImagePositionInfo()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->fromPlatformTypeEnumName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v2

    .line 561
    .local v2, "platform":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    check-cast v1, Ljava/util/List;

    .line 562
    .restart local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    if-nez v1, :cond_2

    .line 563
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 564
    .restart local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    invoke-interface {v4, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 567
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 573
    .end local v0    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    .end local v2    # "platform":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    if-eqz v3, :cond_4

    .line 574
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->slideshowsForPlatforms:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    check-cast v1, Ljava/util/List;

    .line 576
    .restart local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    :cond_4
    if-eqz v1, :cond_5

    .end local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    :goto_1
    return-object v1

    .restart local v1    # "imageForCertainPlatform":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method

.method public getSquareIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->iconUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->iconUrl:Ljava/lang/String;

    .line 730
    :goto_0
    return-object v0

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 724
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getAppSquareIconURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->iconUrl:Ljava/lang/String;

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->iconUrl:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 728
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 730
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->iconUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubscriptionType()Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 854
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->subscriptionType:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 502
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->nowPlayingTitleId:J

    return-wide v0
.end method

.method public getUserRatingCount()I
    .locals 1

    .prologue
    .line 748
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->UserRatingCount:I

    return v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasValidAvailability()Z
    .locals 1

    .prologue
    .line 799
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 937
    const/16 v0, 0x11

    .line 938
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 939
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v2

    add-int v0, v1, v2

    .line 940
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 942
    return v0
.end method

.method public setBigCatProductId(Ljava/lang/String;)V
    .locals 0
    .param p1, "bigCatProductId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 773
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bigCatProductId:Ljava/lang/String;

    .line 774
    return-void
.end method

.method public setBundlePrimaryItemTitleId(J)V
    .locals 1
    .param p1, "bundlePrimaryItemTitleId"    # J

    .prologue
    .line 946
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->bundlePrimaryItemTitleId:J

    .line 947
    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 683
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->imageUrl:Ljava/lang/String;

    .line 684
    return-void
.end method

.method public setImpressionGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "guid"    # Ljava/lang/String;

    .prologue
    .line 667
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ImpressionGuid:Ljava/lang/String;

    .line 668
    return-void
.end method

.method public setMediaItemTypeFromInt(I)V
    .locals 1
    .param p1, "intValue"    # I

    .prologue
    .line 735
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 736
    .local v0, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemType(Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;)V

    .line 737
    return-void
.end method

.method public setNowPlayingTitleId(J)V
    .locals 1
    .param p1, "titleId"    # J

    .prologue
    .line 494
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->nowPlayingTitleId:J

    .line 495
    return-void
.end method

.method public setPackageFamilyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageFamilyName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 863
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->packageFamilyName:Ljava/lang/String;

    .line 864
    return-void
.end method

.method public setParentItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 761
    .local p1, "parents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    return-void
.end method

.method public setPartnerMediaId(Ljava/lang/String;)V
    .locals 0
    .param p1, "partnerMediaId"    # Ljava/lang/String;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->partnerMediaId:Ljava/lang/String;

    .line 483
    return-void
.end method

.method public setProviders(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    .local p1, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->providers:Ljava/util/ArrayList;

    .line 660
    return-void
.end method

.method public shouldLoadParent()Z
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    return v0
.end method

.method protected updateDisplaySkuAvailabilities(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 5
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    .line 279
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 280
    const/4 v0, 0x0

    .line 281
    .local v0, "chosenAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getLowestRankDisplaySkuAvailability()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;

    move-result-object v1

    .line 285
    .local v1, "properDisplaySkuAvailability":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
    if-nez v1, :cond_2

    .line 286
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getDisplaySkuAvailabilities()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->chooseAvailabilityWithoutSku(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;

    move-result-object v0

    .line 298
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 299
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability:Z

    .line 301
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->availabilityId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->availabilityId:Ljava/lang/String;

    .line 303
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->affirmationId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->affirmationId:Ljava/lang/String;

    .line 305
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    if-eqz v3, :cond_1

    .line 307
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->listPrice:F

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getBigDecimalFromFloat(FI)Ljava/math/BigDecimal;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->listPrice:Ljava/math/BigDecimal;

    .line 308
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->msrp:F

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getBigDecimalFromFloat(FI)Ljava/math/BigDecimal;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->msrp:Ljava/math/BigDecimal;

    .line 309
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;->orderManagementData:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->currencyCode:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->currencyCode:Ljava/lang/String;

    .line 312
    :cond_1
    return-void

    .line 287
    :cond_2
    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    if-eqz v3, :cond_0

    .line 288
    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v3, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->skuId:Ljava/lang/String;

    .line 289
    iget-object v3, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    iget-object v2, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    .line 291
    .local v2, "properties":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateBundlesInfo(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;)V

    .line 292
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->updateXPAInfo(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;)V

    .line 294
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->getAvailabilities()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->chooseAvailability(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;

    move-result-object v0

    goto :goto_0
.end method
