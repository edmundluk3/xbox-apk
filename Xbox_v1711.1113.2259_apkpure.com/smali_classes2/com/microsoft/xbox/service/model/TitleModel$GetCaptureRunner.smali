.class Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCaptureRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field private qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "titleId"    # Ljava/lang/String;
    .param p4, "qualifier"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .param p5, "xuid"    # Ljava/lang/String;

    .prologue
    .line 703
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 704
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->xuid:Ljava/lang/String;

    .line 705
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 706
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->titleId:Ljava/lang/String;

    .line 707
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 708
    return-void
.end method

.method private getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;
    .locals 4
    .param p1, "user"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .param p2, "settingId"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .prologue
    .line 822
    if-eqz p1, :cond_1

    .line 823
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;

    .line 824
    .local v0, "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 825
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 830
    .end local v0    # "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 712
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-nez v13, :cond_0

    .line 713
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Invalid game clips filter"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 716
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->titleId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->xuid:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-interface/range {v13 .. v16}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameClipsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    move-result-object v9

    .line 717
    .local v9, "titleCaptureResult":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 718
    .local v4, "profileGameClips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 719
    .local v5, "profileScreenshots":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 721
    .local v12, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v9, :cond_5

    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    if-eqz v13, :cond_5

    .line 724
    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 726
    .local v1, "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    sget-object v15, Lcom/microsoft/xbox/service/model/CaptureState;->Published:Lcom/microsoft/xbox/service/model/CaptureState;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/model/CaptureState;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 730
    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 731
    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 734
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 735
    .local v2, "gameClipUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;>;"
    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    if-eqz v14, :cond_1

    .line 736
    iget-object v14, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    .line 737
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    iget-object v15, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    sget-object v16, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/DVRVideoType;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 738
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 743
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_1

    .line 744
    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 745
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 751
    .end local v1    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .end local v2    # "gameClipUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;>;"
    :cond_5
    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    if-eqz v13, :cond_a

    .line 752
    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_6
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .line 753
    .local v7, "screenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    iget-object v14, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_7

    iget-object v14, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 754
    iget-object v14, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    :cond_7
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 758
    .local v8, "screenshotUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;>;"
    iget-object v14, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    if-eqz v14, :cond_6

    .line 759
    iget-object v14, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_8
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    .line 760
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    iget-object v15, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uriType:Ljava/lang/String;

    sget-object v16, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/DVRVideoType;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 761
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 766
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    :cond_9
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_6

    .line 767
    iput-object v8, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    .line 768
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 774
    .end local v7    # "screenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .end local v8    # "screenshotUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;>;"
    :cond_a
    new-instance v11, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    invoke-direct {v11, v12}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 775
    .local v11, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    const/4 v6, 0x0

    .line 776
    .local v6, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v11, :cond_b

    iget-object v13, v11, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    if-eqz v13, :cond_b

    iget-object v13, v11, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_b

    .line 777
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v13

    invoke-static {v11}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v6

    .line 780
    :cond_b
    if-eqz v6, :cond_10

    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    if-eqz v13, :cond_10

    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_10

    .line 781
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_c
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_10

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 782
    .local v10, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_d
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_e

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 785
    .restart local v1    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    iget-object v15, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_d

    iget-object v15, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    sget-object v16, Lcom/microsoft/xbox/service/model/CaptureState;->Published:Lcom/microsoft/xbox/service/model/CaptureState;

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/CaptureState;->name()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 789
    iget-object v15, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 790
    sget-object v15, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v15}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->setGamerTag(Ljava/lang/String;)V

    goto :goto_2

    .line 794
    .end local v1    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    :cond_e
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_f
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_c

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .line 795
    .restart local v7    # "screenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    iget-object v15, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 796
    sget-object v15, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v15}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->setGamerTag(Ljava/lang/String;)V

    goto :goto_3

    .line 802
    .end local v7    # "screenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .end local v10    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_10
    iput-object v4, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    .line 803
    iput-object v5, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    .line 804
    return-object v9
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 696
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 818
    const-wide/16 v0, 0xbda

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$100(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 814
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 809
    return-void
.end method
