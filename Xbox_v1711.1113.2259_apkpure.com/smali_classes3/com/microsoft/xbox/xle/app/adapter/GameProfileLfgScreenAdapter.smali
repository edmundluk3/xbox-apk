.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GameProfileLfgScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final createButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final editTagButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private hostXuids:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private languageAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private languageFilter:Landroid/widget/Spinner;

.field private lfgSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

.field private searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private selectedColor:I

.field private final selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

.field private selectedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private timeAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timeFilter:Landroid/widget/Spinner;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)V
    .locals 10
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    .prologue
    const v9, 0x7f03020a

    const v8, 0x7f030202

    const/4 v7, 0x0

    .line 64
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->lfgSessions:Ljava/util/List;

    .line 65
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    .line 66
    const v0, 0x7f0e0670

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->screenBody:Landroid/view/View;

    .line 67
    const v0, 0x7f0e0678

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 68
    const v0, 0x7f0e0679

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 69
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03015d

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    const/4 v4, 0x1

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)V

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;ZLcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 94
    const v0, 0x7f0e0675

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->createButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 95
    const v0, 0x7f0e0674

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->editTagButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 96
    const v0, 0x7f0e0673

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0151

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconFontColor(I)V

    .line 98
    const v0, 0x7f0e0672

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    .line 100
    .local v6, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedColor:I

    .line 101
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedColor:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(IILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setVerticalPadding(I)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, v7, v7}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 106
    const v0, 0x7f0e067a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 108
    const v0, 0x7f0e0676

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeFilter:Landroid/widget/Spinner;

    .line 109
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getDateFilters()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeAdapter:Landroid/widget/ArrayAdapter;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v9}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeFilter:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 113
    const v0, 0x7f0e0677

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageFilter:Landroid/widget/Spinner;

    .line 114
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getLanguageFilters()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageAdapter:Landroid/widget/ArrayAdapter;

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v9}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageFilter:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 117
    return-void

    :cond_0
    move v0, v7

    .line 100
    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 4
    .param p0, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .prologue
    .line 73
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 75
    .local v0, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandleId(Ljava/lang/String;)V

    .line 78
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackSelectLFG(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 81
    .end local v0    # "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    const-string v0, "LFG - Create"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->showCreateLfgDialog()V

    .line 186
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->showTagPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->showTagPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->showTagPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->setDateFilter(I)V

    .line 203
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 204
    return-void
.end method

.method static synthetic lambda$onStart$7(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->setLanguageFilter(I)V

    .line 208
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 209
    return-void
.end method

.method static synthetic lambda$updateViewOverride$1(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->navigateToAccountSettings()V

    return-void
.end method


# virtual methods
.method public getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    return-object v0
.end method

.method public bridge synthetic getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 180
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->createButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->createButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->editTagButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->editTagButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v0, :cond_2

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->timeFilter:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->languageFilter:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 211
    return-void
.end method

.method public updateViewOverride()V
    .locals 11

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 121
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    .line 122
    .local v5, "vmState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 123
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->isBusy()Z

    move-result v6

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->updateLoadingIndicator(Z)V

    .line 125
    const/4 v3, 0x0

    .line 127
    .local v3, "shouldForceListUpdate":Z
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v6, v9, :cond_0

    .line 128
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 130
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v6

    if-nez v6, :cond_3

    .line 131
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0706ba

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 132
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v9

    if-eq v6, v9, :cond_1

    .line 142
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    .line 143
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 144
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 145
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 146
    const/4 v3, 0x1

    .line 149
    :cond_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTagsListView:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v7

    :goto_1
    invoke-virtual {v9, v6}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 150
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->searchButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v6, v8

    :goto_2
    invoke-virtual {v9, v6}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 151
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->editTagButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->selectedTags:Ljava/util/List;

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v9

    if-eqz v9, :cond_7

    :goto_3
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 153
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummaries()Ljava/util/Map;

    move-result-object v4

    .line 155
    .local v4, "viewModelHostXuids":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v5, v6, :cond_9

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->lfgSessions:Ljava/util/List;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getData()Ljava/util/List;

    move-result-object v7

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->hostXuids:Ljava/util/Map;

    if-eq v4, v6, :cond_9

    .line 157
    :cond_2
    const/4 v3, 0x1

    .line 158
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->clear()V

    .line 160
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->hostXuids:Ljava/util/Map;

    .line 161
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getData()Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->lfgSessions:Ljava/util/List;

    .line 162
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->hostXuids:Ljava/util/Map;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->setUserMap(Ljava/util/Map;)V

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v1, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->lfgSessions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 166
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->GameProfile:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-direct {v7, v8, v0}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;-><init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 133
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v1    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    .end local v4    # "viewModelHostXuids":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    .restart local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateOrJoinLFG()Z

    move-result v6

    if-nez v6, :cond_4

    .line 134
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070110

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_4
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0706e8

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 137
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->errorPane:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_5
    move v6, v8

    .line 149
    goto/16 :goto_1

    :cond_6
    move v6, v7

    .line 150
    goto/16 :goto_2

    :cond_7
    move v7, v8

    .line 151
    goto/16 :goto_3

    .line 169
    .restart local v1    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    .restart local v4    # "viewModelHostXuids":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_8
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 172
    .end local v1    # "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;>;"
    :cond_9
    if-eqz v3, :cond_a

    .line 174
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 176
    :cond_a
    return-void
.end method
