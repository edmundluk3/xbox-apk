.class public Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "IncludedContentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final yearFormatter:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->yearFormatter:Ljava/text/SimpleDateFormat;

    .line 25
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 31
    if-nez p2, :cond_2

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 33
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030155

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$1;)V

    .line 36
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;
    const v4, 0x7f0e0714

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 37
    const v4, 0x7f0e0715

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 38
    const v4, 0x7f0e0716

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 39
    const v4, 0x7f0e0717

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$402(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/ui/FontStarRatingView;)Lcom/microsoft/xbox/xle/ui/FontStarRatingView;

    .line 41
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    .end local v3    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 47
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_1

    .line 48
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 49
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v4

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f020121

    const v7, 0x7f020123

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 53
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->yearFormatter:Ljava/text/SimpleDateFormat;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "releaseYear":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 58
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/FontStarRatingView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 59
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/FontStarRatingView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->setVisibility(I)V

    .line 60
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/FontStarRatingView;

    move-result-object v4

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAverageUserRating()F

    move-result v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAllTimeRatingCount()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->setAverageUserRatingAndUserCount(FJ)V

    .line 64
    .end local v2    # "releaseYear":Ljava/lang/String;
    :cond_1
    return-object p2

    .line 43
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter$ViewHolder;
    goto :goto_0
.end method
