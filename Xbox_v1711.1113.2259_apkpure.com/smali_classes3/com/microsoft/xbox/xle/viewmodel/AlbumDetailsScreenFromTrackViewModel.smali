.class public Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "AlbumDetailsScreenFromTrackViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;
    }
.end annotation


# instance fields
.field private albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

.field private detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

.field protected isLoading:Z

.field selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 29
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    .line 30
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 36
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 37
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 39
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAlbumDetailFromTrackAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    return-object v0
.end method


# virtual methods
.method public getAlbumYearAndStudio()Ljava/lang/String;
    .locals 5

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->getLabelOwner()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070e6e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getArtistName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDefaultImageRid()I
    .locals 1

    .prologue
    .line 74
    const/16 v0, 0x3ee

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    return v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 121
    const v0, 0x7f070683

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIsExplicit()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getIsExplicit()Z

    move-result v0

    goto :goto_0
.end method

.method public getLabelOwner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getLabelOwner()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getReleaseYear()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v1, :cond_0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTracks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->cancel()V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->load(Z)V

    .line 149
    :cond_0
    return-void
.end method

.method public navigateToArtistDetail()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 118
    :cond_0
    return-void
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "album"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    .prologue
    .line 152
    const-string v0, "AlbumDetailsScreen"

    const-string v1, "onLoadRelated Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->isLoading:Z

    .line 155
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 171
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->updateAdapter()V

    .line 172
    return-void

    .line 159
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 165
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 166
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAlbumDetailFromTrackAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 127
    return-void

    .line 126
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->cancel()V

    .line 135
    :cond_0
    return-void
.end method

.method public playAlbum()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    const-string v1, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "Album"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "uri":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v1

    invoke-static {v0, v1, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    goto :goto_0
.end method

.method public playTrack(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    if-nez v2, :cond_0

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->albumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 102
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x18ffc9f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ID:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "Track"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "uri":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v2

    invoke-static {v1, v2, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    goto :goto_0
.end method
