.class Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;
.super Ljava/lang/Object;
.source "SnappableAppsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/SnappableAppsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnappableAppsModelHolder"
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/service/model/SnappableAppsModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;-><init>(Lcom/microsoft/xbox/service/model/SnappableAppsModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;->instance:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$200()Lcom/microsoft/xbox/service/model/SnappableAppsModel;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;->instance:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    return-object v0
.end method

.method private static reset()V
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->access$100(Lcom/microsoft/xbox/service/model/SnappableAppsModel;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;-><init>(Lcom/microsoft/xbox/service/model/SnappableAppsModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;->instance:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    .line 45
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
