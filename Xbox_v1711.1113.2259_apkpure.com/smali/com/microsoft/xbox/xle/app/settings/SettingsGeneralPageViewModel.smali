.class public Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "SettingsGeneralPageViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
.implements Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;,
        Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;,
        Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;,
        Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;,
        Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final URI_GET_STARTED:Ljava/lang/String; = "http://www.xbox.com/%1$s/smartglass"

.field private static final URI_PRIVACY:Ljava/lang/String; = "http://go.microsoft.com/fwlink/?LinkId=248681"


# instance fields
.field private addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

.field private final ctx:Landroid/content/Context;

.field private editFirstNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

.field private editLastNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

.field private followingSummaries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;",
            ">;"
        }
    .end annotation
.end field

.field private friendsSelected:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private friendsUnSelected:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation
.end field

.field private getFollowingSummaryAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

.field private getUserPrivacySettingsAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

.field homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isAddingUsersToShareListStatus:Z

.field private isChangingUserShareIdentityStatus:Z

.field private isEditingFirstNameStatus:Z

.field private isEditingLastNameStatus:Z

.field private isLoadingUserFollowingSummaryStatus:Z

.field private isLoadingUserPrivacySettings:Z

.field private isRemovingUsersFromShareListStatus:Z

.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private removeUserFromShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

.field private secretEnableProgress:I

.field private sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

.field private signOutInProgress:Z

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private userShareIdentityStatusValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->secretEnableProgress:I

    .line 332
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 110
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    .line 111
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 112
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 113
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 114
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->ctx:Landroid/content/Context;

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->signOutInProgress:Z

    return v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->signOutInProgress:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->onChangeUserShareIdentityCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1102(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isChangingUserShareIdentityStatus:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserPrivacySettings:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->onLoadUserPrivacySettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->onGetFollowersSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserFollowingSummaryStatus:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->onEditFirstNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingFirstNameStatus:Z

    return p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->onEditLastNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$902(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingLastNameStatus:Z

    return p1
.end method

.method private onChangeUserShareIdentityCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v2, 0x0

    .line 423
    const-string v0, "SettingsActivityViewModel"

    const-string v1, "onChangeUserShareIdentityCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isChangingUserShareIdentityStatus:Z

    .line 425
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 436
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 439
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 440
    return-void

    .line 432
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070b1d

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0

    .line 425
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onEditFirstNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 443
    const-string v0, "SettingsActivityViewModel"

    const-string v1, "onEditFirstNameCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingFirstNameStatus:Z

    .line 445
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 456
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 457
    return-void

    .line 449
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyEditRealNameDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 453
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyEditRealNameDialogAsyncTaskFailed()V

    goto :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onEditLastNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 460
    const-string v0, "SettingsActivityViewModel"

    const-string v1, "onEditLastNameCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingLastNameStatus:Z

    .line 462
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 473
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 474
    return-void

    .line 466
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyEditRealNameDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 470
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyEditRealNameDialogAsyncTaskFailed()V

    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onGetFollowersSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 404
    const-string v0, "SettingsActivityViewModel"

    const-string v1, "onGetFollowersSummaryCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserFollowingSummaryStatus:Z

    .line 406
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 418
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 419
    return-void

    .line 410
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileFollowingSummaryData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    goto :goto_0

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadUserPrivacySettingsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "privacyResult"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    .prologue
    .line 477
    const-string v0, "SettingsActivityViewModel"

    const-string v1, "onLoadUserPrivacySettingsCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserPrivacySettings:Z

    .line 479
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 499
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 502
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 503
    return-void

    .line 481
    :pswitch_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 483
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setShareRealName(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 484
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getSharingRealNameTransitively()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setSharingRealNameTransitively(Z)V

    .line 485
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getCanShareRealName()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setCanShareRealName(Z)V

    goto :goto_0

    .line 493
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public NavigateToAboutScreen()V
    .locals 1

    .prologue
    .line 329
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/AboutActivity;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 330
    return-void
.end method

.method public addPersonToSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 2
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 772
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    return-void
.end method

.method public addPersonToUnSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 2
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 780
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    return-void
.end method

.method public changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 2
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;
    .param p2, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->cancel()V

    .line 359
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    invoke-direct {v0, p0, p2, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->load(Z)V

    .line 361
    return-void
.end method

.method public changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 2
    .param p1, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->cancel()V

    .line 350
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$ChangeUserShareIdentityAsyncTask;->load(Z)V

    .line 352
    return-void
.end method

.method public editUserRealName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "firstname"    # Ljava/lang/String;
    .param p2, "lastname"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 364
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editFirstNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editFirstNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->cancel()V

    .line 367
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editFirstNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

    .line 368
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editFirstNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditFirstNameAsyncTask;->load(Z)V

    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editLastNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editLastNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;->cancel()V

    .line 372
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editLastNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editLastNameAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$EditLastNameAsyncTask;->load(Z)V

    .line 374
    return-void
.end method

.method public fetchFollowingSummary()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    return-object v0
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->load(Z)V

    .line 185
    return-void
.end method

.method public getCanShareRealName()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanShareRealName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFirstName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 3

    .prologue
    .line 269
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getHomeScreenPreference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    return-object v0
.end method

.method public getIsChangingUserShareIdentityStatus()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isChangingUserShareIdentityStatus:Z

    return v0
.end method

.method public getIsEditingFirstNameStatus()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingFirstNameStatus:Z

    return v0
.end method

.method public getIsEditingLastNameStatus()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingLastNameStatus:Z

    return v0
.end method

.method public getIsLoadingProfile()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserPrivacySettings:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingUserFollowingSummaryStatus()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserFollowingSummaryStatus:Z

    return v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getLastName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getPeopleSelectedList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 751
    .local v0, "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserFollowingSummaryStatus:Z

    if-nez v3, :cond_0

    .line 752
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    .line 753
    .local v1, "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;-><init>(Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;)V

    .line 754
    .local v2, "personSelector":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    iget-boolean v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isIdentityShared:Z

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 755
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 758
    .end local v1    # "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    .end local v2    # "personSelector":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    :cond_0
    return-object v0
.end method

.method public getProfilePic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMyRealNamePreview()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSandboxId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedPerson()Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .locals 1

    .prologue
    .line 763
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->userShareIdentityStatusValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    return-object v0
.end method

.method public getShareText(Z)Ljava/lang/String;
    .locals 2
    .param p1, "toggleOn"    # Z

    .prologue
    .line 875
    if-eqz p1, :cond_0

    .line 876
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->ctx:Landroid/content/Context;

    const v1, 0x7f070b3f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 878
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->ctx:Landroid/content/Context;

    const v1, 0x7f070b3e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSharingTransitivelyStatus()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getSharingRealNameTransitively()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;
    .locals 1

    .prologue
    .line 224
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 6

    .prologue
    .line 285
    const-string v2, ""

    .line 287
    .local v2, "ret":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->ctx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 288
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    .end local v1    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v2

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->TAG:Ljava/lang/String;

    const-string v4, "failed to get version with exception "

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public incrementSecretEnable()V
    .locals 5

    .prologue
    const/4 v2, 0x5

    const/4 v4, 0x0

    .line 890
    iget v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->secretEnableProgress:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->secretEnableProgress:I

    if-ne v1, v2, :cond_0

    .line 891
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "Party on, Wayne!"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "IT\'S DANGEROUS TO GO ALONE! TAKE THIS."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "You gotta fight... for your right..."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Follow the white rabbit."

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Party = Very Yes."

    aput-object v2, v0, v1

    .line 899
    .local v0, "messages":[Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    aget-object v2, v0, v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 900
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->forceEnablePartyChat()V

    .line 901
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 903
    .end local v0    # "messages":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->signOutInProgress:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->isBlockingBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isChangingUserShareIdentityStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isLoadingUserFollowingSummaryStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isAddingUsersToShareListStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isRemovingUsersFromShareListStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingFirstNameStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isEditingLastNameStatus:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeveloperMode()Z
    .locals 2

    .prologue
    .line 315
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getDefaultSandboxId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecretAvailable()Z
    .locals 1

    .prologue
    .line 884
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v0, 0x1

    .line 175
    invoke-super {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->load(Z)V

    .line 176
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->loadPrivacySettings(Z)V

    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 178
    return-void
.end method

.method public loadFollowingSummary()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileFollowingSummaryData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getIsLoadingFollowingSummaries()Z

    move-result v0

    if-nez v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getFollowingSummaryAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getFollowingSummaryAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->cancel()V

    .line 388
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getFollowingSummaryAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

    .line 389
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getFollowingSummaryAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->load(Z)V

    .line 392
    :cond_1
    return-void
.end method

.method public loadPrivacySettings(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getUserPrivacySettingsAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getUserPrivacySettingsAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->cancel()V

    .line 398
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getUserPrivacySettingsAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getUserPrivacySettingsAsyncTask:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->load(Z)V

    .line 400
    return-void
.end method

.method public navigateToEditRealNameDialog()V
    .locals 1

    .prologue
    .line 826
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingEditName()V

    .line 828
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showEditRealNameDialog(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    .line 829
    return-void
.end method

.method public navigateToHelpScreen()V
    .locals 5

    .prologue
    .line 304
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "locale":Ljava/lang/String;
    const-string v2, "http://www.xbox.com/%1$s/smartglass"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, "uri":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateToUri(Landroid/net/Uri;)V

    .line 307
    return-void
.end method

.method public navigateToPeopleSelectorActivity()V
    .locals 3

    .prologue
    .line 811
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 812
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 815
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingEditFriends()V

    .line 817
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07063b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;Ljava/lang/String;)V

    .line 820
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingFriendsSelectorView()V

    .line 821
    return-void
.end method

.method public navigateToPrivacyScreen()V
    .locals 1

    .prologue
    .line 311
    const-string v0, "http://go.microsoft.com/fwlink/?LinkId=248681"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateToUri(Landroid/net/Uri;)V

    .line 312
    return-void
.end method

.method public navigateToThirdPartyNoticeScreen()V
    .locals 1

    .prologue
    .line 300
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 301
    return-void
.end method

.method public navigateToWhatsNewDetailsScreen()V
    .locals 1

    .prologue
    .line 296
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 297
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 145
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 150
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->loadFollowingSummary()V

    .line 155
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 163
    :cond_0
    return-void
.end method

.method public postAddUsersToShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 854
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isAddingUsersToShareListStatus:Z

    .line 855
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 866
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 867
    return-void

    .line 859
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyPeoplePickerDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 863
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f070b1d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyPeoplePickerDialogAsyncTaskFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 855
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public postRemoveUserFromShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 837
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isRemovingUsersFromShareListStatus:Z

    .line 838
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 849
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 850
    return-void

    .line 842
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyPeoplePickerDialogAsyncTaskCompleted()V

    goto :goto_0

    .line 846
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f070b1d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyPeoplePickerDialogAsyncTaskFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 838
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public preAddUsersToShareList()V
    .locals 1

    .prologue
    .line 832
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isAddingUsersToShareListStatus:Z

    .line 833
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 834
    return-void
.end method

.method public preRemoveUserFromShareList()V
    .locals 1

    .prologue
    .line 870
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isRemovingUsersFromShareListStatus:Z

    .line 871
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 872
    return-void
.end method

.method public removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 1
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 776
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 777
    return-void
.end method

.method public removePersonFromUnselected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 1
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 784
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 785
    return-void
.end method

.method public selectionActivityCompleted(Z)V
    .locals 4
    .param p1, "accepted"    # Z

    .prologue
    const/4 v3, 0x1

    .line 788
    if-eqz p1, :cond_3

    .line 790
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->followingSummaries:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingSaveFriends(Ljava/util/ArrayList;II)V

    .line 792
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 793
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->cancel()V

    .line 796
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsSelected:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;Ljava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    .line 797
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->load(Z)V

    .line 800
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->removeUserFromShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

    if-eqz v0, :cond_2

    .line 802
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->removeUserFromShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->cancel()V

    .line 804
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->friendsUnSelected:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->removeUserFromShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

    .line 805
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->removeUserFromShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->load(Z)V

    .line 808
    :cond_3
    return-void
.end method

.method public setHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 3
    .param p1, "preference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 274
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 275
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setHomeScreenPreference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->setHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    .line 277
    return-void
.end method

.method public setSelectedPerson(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 769
    return-void
.end method

.method public setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 0
    .param p1, "status"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->userShareIdentityStatusValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 233
    return-void
.end method

.method public setStayAwakeSetting(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V
    .locals 1
    .param p1, "setting"    # Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    .prologue
    .line 264
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setStayAwakeSettings(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V

    .line 265
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->Always:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 266
    return-void

    .line 265
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public signOut()V
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->signOutInProgress:Z

    .line 325
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->reset()V

    .line 326
    return-void
.end method

.method protected updateAdapter()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 219
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->updateAdapter()V

    .line 221
    :cond_0
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 213
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 214
    return-void

    .line 191
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v0

    .line 193
    .local v0, "sessionState":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    if-nez v0, :cond_0

    .line 194
    :cond_1
    const-string v1, "Stress"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "state changed, set ready "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->sessionReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    goto :goto_0

    .line 202
    .end local v0    # "sessionState":I
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v1, :cond_2

    .line 203
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 206
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setShareRealNameStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
