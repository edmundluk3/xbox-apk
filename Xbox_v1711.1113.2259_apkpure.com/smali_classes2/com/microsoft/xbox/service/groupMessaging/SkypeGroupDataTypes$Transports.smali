.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Transports"
.end annotation


# instance fields
.field private final gcm:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "gcm":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;->gcm:Ljava/util/List;

    .line 402
    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;->gcm:Ljava/util/List;

    return-object v0
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 405
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;

    return-object v0
.end method
