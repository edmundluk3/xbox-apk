.class Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedMeSettingsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetActivityFeedMeSettingsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 0

    .prologue
    .line 3222
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedMeSettingsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/ProfileModel$1;

    .prologue
    .line 3222
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedMeSettingsRunner;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;
    .locals 4

    .prologue
    .line 3226
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivityFeedService()Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;->getActivityFeedMeSettings()Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3230
    :goto_0
    return-object v1

    .line 3227
    :catch_0
    move-exception v0

    .line 3228
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Failed to get activity feed settings. Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3230
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3222
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedMeSettingsRunner;->buildData()Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3245
    const-wide/16 v0, 0xc85

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3240
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedMeSettingsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3300(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 3241
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3236
    return-void
.end method
