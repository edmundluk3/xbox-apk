.class public final enum Lcom/microsoft/xbox/smartglass/SoundLevel;
.super Ljava/lang/Enum;
.source "SoundLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/SoundLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/SoundLevel;

.field public static final enum Full:Lcom/microsoft/xbox/smartglass/SoundLevel;

.field public static final enum Low:Lcom/microsoft/xbox/smartglass/SoundLevel;

.field public static final enum Muted:Lcom/microsoft/xbox/smartglass/SoundLevel;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/SoundLevel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/SoundLevel;

    const-string v3, "Muted"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/SoundLevel;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SoundLevel;->Muted:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/SoundLevel;

    const-string v3, "Low"

    invoke-direct {v2, v3, v4, v4}, Lcom/microsoft/xbox/smartglass/SoundLevel;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SoundLevel;->Low:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/SoundLevel;

    const-string v3, "Full"

    invoke-direct {v2, v3, v5, v5}, Lcom/microsoft/xbox/smartglass/SoundLevel;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SoundLevel;->Full:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 10
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/SoundLevel;

    sget-object v3, Lcom/microsoft/xbox/smartglass/SoundLevel;->Muted:Lcom/microsoft/xbox/smartglass/SoundLevel;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/SoundLevel;->Low:Lcom/microsoft/xbox/smartglass/SoundLevel;

    aput-object v3, v2, v4

    sget-object v3, Lcom/microsoft/xbox/smartglass/SoundLevel;->Full:Lcom/microsoft/xbox/smartglass/SoundLevel;

    aput-object v3, v2, v5

    sput-object v2, Lcom/microsoft/xbox/smartglass/SoundLevel;->$VALUES:[Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 20
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SoundLevel;->_lookup:Landroid/util/SparseArray;

    .line 23
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SoundLevel;->values()[Lcom/microsoft/xbox/smartglass/SoundLevel;

    move-result-object v2

    array-length v3, v2

    .local v0, "buttons":Lcom/microsoft/xbox/smartglass/SoundLevel;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 24
    sget-object v4, Lcom/microsoft/xbox/smartglass/SoundLevel;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/SoundLevel;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 23
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/microsoft/xbox/smartglass/SoundLevel;->_value:I

    .line 30
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/SoundLevel;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 47
    sget-object v1, Lcom/microsoft/xbox/smartglass/SoundLevel;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 48
    .local v0, "val":Lcom/microsoft/xbox/smartglass/SoundLevel;
    if-nez v0, :cond_0

    .line 49
    sget-object v0, Lcom/microsoft/xbox/smartglass/SoundLevel;->Full:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 52
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/SoundLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/SoundLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SoundLevel;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/SoundLevel;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/SoundLevel;->$VALUES:[Lcom/microsoft/xbox/smartglass/SoundLevel;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/SoundLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/SoundLevel;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/smartglass/SoundLevel;->_value:I

    return v0
.end method
