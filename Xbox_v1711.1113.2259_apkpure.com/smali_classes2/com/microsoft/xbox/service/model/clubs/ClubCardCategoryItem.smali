.class public Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
.super Ljava/lang/Object;
.source "ClubCardCategoryItem.java"


# instance fields
.field public associatedTitles:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final cardModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation
.end field

.field public final criteria:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final headerText:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "criteria"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p3, "cardModels":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    .local p4, "associatedTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 36
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 37
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    .line 42
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->associatedTitles:Ljava/util/List;

    .line 43
    return-void
.end method


# virtual methods
.method public addCardModel(Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 1
    .param p1, "cardModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 51
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 61
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 63
    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 64
    .local v0, "other":Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    .line 65
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    .line 66
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getClubModels()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 72
    const/16 v0, 0x11

    .line 73
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 74
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 75
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->cardModels:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 76
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
