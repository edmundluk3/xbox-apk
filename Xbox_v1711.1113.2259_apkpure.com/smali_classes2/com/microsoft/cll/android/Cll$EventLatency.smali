.class public final enum Lcom/microsoft/cll/android/Cll$EventLatency;
.super Ljava/lang/Enum;
.source "Cll.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/cll/android/Cll;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventLatency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/cll/android/Cll$EventLatency;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/cll/android/Cll$EventLatency;

.field public static final enum NORMAL:Lcom/microsoft/cll/android/Cll$EventLatency;

.field public static final enum REALTIME:Lcom/microsoft/cll/android/Cll$EventLatency;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 155
    new-instance v0, Lcom/microsoft/cll/android/Cll$EventLatency;

    const-string v1, "NORMAL"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/cll/android/Cll$EventLatency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->NORMAL:Lcom/microsoft/cll/android/Cll$EventLatency;

    .line 156
    new-instance v0, Lcom/microsoft/cll/android/Cll$EventLatency;

    const-string v1, "REALTIME"

    const/16 v2, 0x200

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/cll/android/Cll$EventLatency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->REALTIME:Lcom/microsoft/cll/android/Cll$EventLatency;

    .line 153
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/cll/android/Cll$EventLatency;

    sget-object v1, Lcom/microsoft/cll/android/Cll$EventLatency;->NORMAL:Lcom/microsoft/cll/android/Cll$EventLatency;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/cll/android/Cll$EventLatency;->REALTIME:Lcom/microsoft/cll/android/Cll$EventLatency;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->$VALUES:[Lcom/microsoft/cll/android/Cll$EventLatency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    iput p3, p0, Lcom/microsoft/cll/android/Cll$EventLatency;->value:I

    .line 162
    return-void
.end method

.method public static getLatency(I)Lcom/microsoft/cll/android/Cll$EventLatency;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 169
    packed-switch p0, :pswitch_data_0

    .line 176
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 171
    :pswitch_0
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->NORMAL:Lcom/microsoft/cll/android/Cll$EventLatency;

    goto :goto_0

    .line 173
    :pswitch_1
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->REALTIME:Lcom/microsoft/cll/android/Cll$EventLatency;

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/Cll$EventLatency;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    const-class v0, Lcom/microsoft/cll/android/Cll$EventLatency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/cll/android/Cll$EventLatency;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/cll/android/Cll$EventLatency;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/microsoft/cll/android/Cll$EventLatency;->$VALUES:[Lcom/microsoft/cll/android/Cll$EventLatency;

    invoke-virtual {v0}, [Lcom/microsoft/cll/android/Cll$EventLatency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/cll/android/Cll$EventLatency;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/microsoft/cll/android/Cll$EventLatency;->value:I

    return v0
.end method
