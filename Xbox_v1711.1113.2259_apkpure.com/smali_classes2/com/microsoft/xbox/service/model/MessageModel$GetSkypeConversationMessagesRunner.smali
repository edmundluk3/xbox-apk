.class Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSkypeConversationMessagesRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final isGroupConversation:Z

.field private resetSyncState:Z

.field private syncState:Ljava/lang/String;

.field private final targetXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "targetXuid"    # Ljava/lang/String;
    .param p4, "isGroupConversation"    # Z
    .param p5, "syncState"    # Ljava/lang/String;

    .prologue
    .line 1719
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1715
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->resetSyncState:Z

    .line 1720
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 1721
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    .line 1722
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    .line 1723
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->isGroupConversation:Z

    .line 1724
    return-void
.end method

.method private updateNonMemberList(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p1, "conversationMessageResult"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "skypeId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1928
    .local p2, "nonMemberXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 1939
    :cond_0
    :goto_0
    return-void

    .line 1932
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1934
    .local v0, "xuid":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1935
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1936
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1932
    .end local v0    # "xuid":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .locals 37
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1730
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v16

    .line 1731
    .local v16, "meXuid":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_0

    .line 1735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 1736
    .local v25, "syncStateUri":Landroid/net/Uri;
    const-string v31, "syncState"

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    .line 1740
    .end local v25    # "syncStateUri":Landroid/net/Uri;
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeServiceMessage(Ljava/lang/String;)Z

    move-result v14

    .line 1741
    .local v14, "isServiceConversation":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->isGroupConversation:Z

    move/from16 v31, v0

    if-nez v31, :cond_1

    if-eqz v14, :cond_6

    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    .line 1743
    .local v11, "id":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-interface {v0, v11, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeConversationMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v5

    .line 1745
    .local v5, "conversationMessageResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v5, :cond_4

    .line 1746
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    if-eqz v31, :cond_9

    .line 1747
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    .line 1749
    .local v4, "backwardLink":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_8

    .line 1750
    const/4 v6, 0x0

    .line 1752
    .local v6, "conversationMessagesSyncState":Ljava/lang/String;
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_3

    .line 1753
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 1754
    .restart local v25    # "syncStateUri":Landroid/net/Uri;
    const-string v31, "syncState"

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1757
    .end local v25    # "syncStateUri":Landroid/net/Uri;
    :cond_3
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_7

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-interface {v0, v11, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeConversationMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v20

    .line 1759
    .local v20, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :goto_2
    const/4 v4, 0x0

    .line 1760
    if-eqz v20, :cond_2

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    if-eqz v31, :cond_2

    .line 1761
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    .line 1762
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    .line 1764
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_2

    .line 1765
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1893
    .end local v4    # "backwardLink":Ljava/lang/String;
    .end local v5    # "conversationMessageResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v6    # "conversationMessagesSyncState":Ljava/lang/String;
    .end local v11    # "id":Ljava/lang/String;
    .end local v14    # "isServiceConversation":Z
    .end local v20    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :catch_0
    move-exception v8

    .line 1895
    .local v8, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v32

    const-wide/16 v34, 0xf

    cmp-long v31, v32, v34

    if-nez v31, :cond_19

    .line 1896
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    const-class v32, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;

    invoke-static/range {v31 .. v32}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;

    .line 1899
    .local v7, "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    iget v0, v7, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->errorCode:I

    move/from16 v31, v0

    const/16 v32, 0xe6

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_18

    .line 1900
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->resetSyncState:Z

    .line 1909
    .end local v7    # "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    .end local v8    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_4
    const/4 v5, 0x0

    :cond_5
    return-object v5

    .line 1741
    .restart local v14    # "isServiceConversation":Z
    :cond_6
    :try_start_1
    sget-object v31, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v32, "8:xbox:%s"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    move-object/from16 v35, v0

    aput-object v35, v33, v34

    invoke-static/range {v31 .. v33}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    .line 1757
    .restart local v4    # "backwardLink":Ljava/lang/String;
    .restart local v5    # "conversationMessageResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v6    # "conversationMessagesSyncState":Ljava/lang/String;
    .restart local v11    # "id":Ljava/lang/String;
    :cond_7
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 1770
    .end local v6    # "conversationMessagesSyncState":Ljava/lang/String;
    :cond_8
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1773
    .end local v4    # "backwardLink":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->isGroupConversation:Z

    move/from16 v31, v0

    if-eqz v31, :cond_5

    .line 1775
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_b

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-interface {v0, v11}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->getGroupMembersForConversation(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;

    move-result-object v10

    .line 1777
    .local v10, "groupMembers":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;
    :goto_3
    if-eqz v10, :cond_13

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;->getGroupMembers()Ljava/util/List;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_13

    .line 1779
    new-instance v31, Ljava/util/HashMap;

    invoke-direct/range {v31 .. v31}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v31

    iput-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    .line 1781
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 1782
    .local v22, "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;->getGroupMembers()Ljava/util/List;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_4
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_c

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 1783
    .local v9, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 1785
    .local v21, "senderXuid":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_a

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v32

    if-nez v32, :cond_a

    const-string v32, "0"

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 1786
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1789
    :cond_a
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    move-object/from16 v32, v0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1775
    .end local v9    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v10    # "groupMembers":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;
    .end local v21    # "senderXuid":Ljava/lang/String;
    .end local v22    # "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    const/4 v10, 0x0

    goto :goto_3

    .line 1792
    .restart local v10    # "groupMembers":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;
    .restart local v22    # "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_c
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_13

    .line 1794
    new-instance v29, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1795
    .local v29, "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    const/16 v19, 0x0

    .line 1796
    .local v19, "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;->users:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_d

    .line 1797
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v31

    invoke-static/range {v29 .. v29}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;->getUserPresenceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;)Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserListPresenceInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;

    move-result-object v19

    .line 1800
    :cond_d
    new-instance v24, Ljava/util/Hashtable;

    invoke-direct/range {v24 .. v24}, Ljava/util/Hashtable;-><init>()V

    .line 1801
    .local v24, "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    if-eqz v19, :cond_f

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_f

    .line 1802
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_e
    :goto_5
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_f

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    .line 1803
    .local v27, "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_e

    .line 1804
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v33

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 1809
    .end local v27    # "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    :cond_f
    new-instance v30, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1810
    .local v30, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v31

    invoke-static/range {v30 .. v30}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v20

    .line 1812
    .local v20, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v20, :cond_13

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_13

    .line 1813
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v32

    :cond_10
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_13

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 1814
    .local v28, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    sget-object v31, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v12

    .line 1816
    .local v12, "imageUrl":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;->getGroupMembers()Ljava/util/List;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v33

    :cond_11
    :goto_6
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_10

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 1817
    .restart local v9    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 1818
    .restart local v21    # "senderXuid":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_11

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 1819
    iput-object v12, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    .line 1820
    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1100(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->realName:Ljava/lang/String;

    .line 1821
    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, v31

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 1822
    iget-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v31, v0

    if-nez v31, :cond_12

    .line 1823
    sget-object v31, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, v31

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 1826
    :cond_12
    sget-object v31, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    goto :goto_6

    .line 1834
    .end local v9    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v12    # "imageUrl":Ljava/lang/String;
    .end local v19    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .end local v20    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v21    # "senderXuid":Ljava/lang/String;
    .end local v22    # "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v24    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .end local v28    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .end local v29    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    .end local v30    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    :cond_13
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_5

    .line 1835
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1836
    .local v18, "nonMemberXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_14
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_17

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1839
    .local v17, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v26

    .line 1840
    .local v26, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v32, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-object/from16 v0, v26

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_14

    .line 1844
    sget-object v32, Lcom/microsoft/xbox/service/model/MessageModel$4;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v33

    aget v32, v32, v33

    packed-switch v32, :pswitch_data_0

    .line 1859
    :pswitch_0
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v23

    .line 1862
    .local v23, "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_15
    :goto_7
    invoke-static/range {v23 .. v23}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v32

    if-nez v32, :cond_14

    .line 1863
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v32

    :cond_16
    :goto_8
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v33

    if-eqz v33, :cond_14

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1864
    .local v15, "item":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v33

    if-nez v33, :cond_16

    .line 1865
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v5, v1, v15}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->updateNonMemberList(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_8

    .line 1846
    .end local v15    # "item":Ljava/lang/String;
    .end local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_1
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v32

    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {v32 .. v33}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/util/List;

    .line 1847
    .restart local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1848
    .local v13, "initiator":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_15

    .line 1849
    move-object/from16 v0, v23

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1853
    .end local v13    # "initiator":Ljava/lang/String;
    .end local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_2
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v23

    .line 1854
    .restart local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_7

    .line 1856
    .end local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_3
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v23

    .line 1857
    .restart local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_7

    .line 1871
    .end local v17    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v23    # "skypeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v26    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_17
    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_5

    .line 1872
    new-instance v30, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1873
    .restart local v30    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v31

    invoke-static/range {v30 .. v30}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v20

    .line 1875
    .restart local v20    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v20, :cond_5

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v31

    if-nez v31, :cond_5

    .line 1876
    new-instance v31, Ljava/util/HashMap;

    invoke-direct/range {v31 .. v31}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v31

    iput-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    .line 1877
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_9
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_5

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 1878
    .restart local v28    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    sget-object v32, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v12

    .line 1880
    .restart local v12    # "imageUrl":Ljava/lang/String;
    new-instance v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    const-string v32, "8:xbox:%s"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v35, v0

    aput-object v35, v33, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    sget-object v33, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;->User:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-direct {v9, v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V

    .line 1881
    .restart local v9    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iput-object v12, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    .line 1882
    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1100(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->realName:Ljava/lang/String;

    .line 1883
    sget-object v32, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    iput-object v0, v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    .line 1884
    iget-object v0, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    move-object/from16 v32, v0

    const-string v33, "8:xbox:%s"

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v36, v0

    aput-object v36, v34, v35

    invoke-static/range {v33 .. v34}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    .line 1902
    .end local v5    # "conversationMessageResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v9    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v10    # "groupMembers":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;
    .end local v11    # "id":Ljava/lang/String;
    .end local v12    # "imageUrl":Ljava/lang/String;
    .end local v14    # "isServiceConversation":Z
    .end local v18    # "nonMemberXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v20    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v28    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .end local v30    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    .restart local v7    # "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    .restart local v8    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_18
    throw v8

    .line 1905
    .end local v7    # "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    :cond_19
    throw v8

    .line 1844
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1711
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->buildData()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1924
    const-wide/16 v0, 0xbea

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1918
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->targetXuid:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->resetSyncState:Z

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;->syncState:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1600(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1919
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1914
    return-void
.end method
