.class public interface abstract Lcom/microsoft/playready/IMeteringReportingTask;
.super Ljava/lang/Object;
.source "IMeteringReportingTask.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract addMeteringReporterListener(Lcom/microsoft/playready/IMeteringReporterListener;)V
.end method

.method public abstract getResponseCustomData()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract removeMeteringReporterListener(Lcom/microsoft/playready/IMeteringReporterListener;)V
.end method
