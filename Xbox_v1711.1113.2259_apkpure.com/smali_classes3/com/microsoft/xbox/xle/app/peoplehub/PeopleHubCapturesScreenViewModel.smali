.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "PeopleHubCapturesScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;
.implements Lcom/microsoft/xbox/xle/viewmodel/ICaptureScreenControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

.field private gameClipsData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private gameClipsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

.field private isDeletingCapture:Z

.field private isLoadingGameClips:Z

.field private isLoadingScreenshots:Z

.field private lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

.field private likeClickTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

.field private loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

.field private loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

.field private model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private screenshotsData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private screenshotsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

.field private updateLikeControl:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    const/16 v3, 0x64

    .line 79
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    .line 72
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 73
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->Everything:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .line 74
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 80
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "profileXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    .line 84
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Gameclips:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v1, v2}, Lcom/microsoft/xbox/service/model/TrendingModel;->newUserInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 85
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Screenshots:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v1, v2}, Lcom/microsoft/xbox/service/model/TrendingModel;->newUserInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 87
    :cond_0
    return-void

    .line 82
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->onLikeCaptureCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->onLoadGameClipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isLoadingGameClips:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->onLoadScreenshotsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$902(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isLoadingScreenshots:Z

    return p1
.end method

.method private buildCombinedDataSet()V
    .locals 7

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isBusy()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 317
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 298
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 301
    const/4 v0, 0x0

    .local v0, "gameClipsIndex":I
    const/4 v2, 0x0

    .line 302
    .local v2, "screenshotsIndex":I
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_5

    .line 303
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_2

    .line 305
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "screenshotsIndex":I
    .local v3, "screenshotsIndex":I
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .end local v3    # "screenshotsIndex":I
    .restart local v2    # "screenshotsIndex":I
    goto :goto_1

    .line 306
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_3

    .line 308
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "gameClipsIndex":I
    .local v1, "gameClipsIndex":I
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .end local v1    # "gameClipsIndex":I
    .restart local v0    # "gameClipsIndex":I
    goto :goto_1

    .line 309
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/ICapture;

    invoke-interface {v4}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getDate()Ljava/util/Date;

    move-result-object v5

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/ICapture;

    invoke-interface {v4}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v4

    if-lez v4, :cond_4

    .line 310
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "gameClipsIndex":I
    .restart local v1    # "gameClipsIndex":I
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .end local v1    # "gameClipsIndex":I
    .restart local v0    # "gameClipsIndex":I
    goto :goto_1

    .line 312
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "screenshotsIndex":I
    .restart local v3    # "screenshotsIndex":I
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .end local v3    # "screenshotsIndex":I
    .restart local v2    # "screenshotsIndex":I
    goto :goto_1

    .line 316
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v5, v6

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto/16 :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method static synthetic lambda$deleteActivityFeedItem$2(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "feedItemId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isDeletingCapture:Z

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->removeItemFromCache(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->removeItemFromCache(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->removeItemFromCache(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    .line 251
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 252
    return-void
.end method

.method static synthetic lambda$deleteActivityFeedItem$3(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isDeletingCapture:Z

    .line 255
    const v0, 0x7f0703dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->showError(I)V

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 257
    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$0(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 0
    .param p0, "screenshot1"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 189
    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$1(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to increment screenshot count"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private onLikeCaptureCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "newLikeState"    # Z
    .param p3, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 541
    const-string v0, "ProfileShowcaseScreen ViewModel"

    const-string v1, "onLikeGameClipsCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 543
    if-eqz p2, :cond_1

    .line 544
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like XUID:%s CaptureID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike XUID:%s CaptureId: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadGameClipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 320
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoadGameClipsCompleted Completed with status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 342
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 343
    return-void

    .line 326
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 327
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 328
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->getItemsAsProfileRecentItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 329
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->toGameClip()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 331
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " game clips loaded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->buildCombinedDataSet()V

    .line 334
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 338
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadScreenshotsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 390
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoadScreenshotsCompleted Completed with status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 413
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 414
    return-void

    .line 397
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 399
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->getItemsAsProfileRecentItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 400
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->toScreenshot()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 402
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " screenshots loaded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->buildCombinedDataSet()V

    .line 405
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 409
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 393
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private removeItemFromCache(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 267
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 268
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    :cond_1
    return-object v1
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 138
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getCaptures()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 136
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "feedItemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 237
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 238
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isDeletingCapture:Z

    .line 241
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->deleteFeedItem(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v1

    .line 243
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 244
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Ljava/lang/String;)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 245
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 242
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    const v0, 0x7f0703dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->showError(I)V

    goto :goto_0
.end method

.method public getCaptures()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProfileCapturesFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 102
    const-string v0, "Unknown capture filter"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->allData:Ljava/util/ArrayList;

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->gameClipsData:Ljava/util/ArrayList;

    goto :goto_0

    .line 100
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->screenshotsData:Ljava/util/ArrayList;

    goto :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    return-object v0
.end method

.method public getLastSelectedCapture()Lcom/microsoft/xbox/service/network/managers/ICapture;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 109
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 110
    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 275
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProfileCapturesFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 285
    const-string v0, "Unknown filter type"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 286
    const-string v0, ""

    :goto_0
    return-object v0

    .line 279
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070a9e

    .line 280
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070aa1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 282
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070aa3

    .line 283
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070aa0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isLoadingGameClips:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isLoadingScreenshots:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isDeletingCapture:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public likeClick(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 5
    .param p1, "uncastItem"    # Ljava/lang/Object;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 474
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 475
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_1

    .line 478
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v4, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 479
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v2, :cond_3

    .line 480
    const/4 v1, 0x1

    .line 481
    .local v1, "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 486
    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateLikeControl:Z

    .line 487
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 489
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

    if-eqz v2, :cond_0

    .line 490
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;->cancel()V

    .line 493
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;ZLjava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

    .line 494
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LikeClickAsyncTask;->load(Z)V

    .line 496
    .end local v1    # "newLikeState":Z
    :cond_1
    return-void

    .line 478
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 483
    :cond_3
    const/4 v1, 0x0

    .line 484
    .restart local v1    # "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto :goto_1
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->cancel()V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;->cancel()V

    .line 165
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->load(Z)V

    .line 168
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;->load(Z)V

    .line 170
    return-void
.end method

.method public navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 201
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 202
    return-void
.end method

.method public navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 4
    .param p1, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 173
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToGameclip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 176
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->scid:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->loadAndPlayGameClip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Play Game DVR"

    const-string v2, "People Hub - Captures View"

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 179
    return-void
.end method

.method public navigateToPostCommentScreen(Ljava/lang/String;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 206
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-static {p0, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 218
    :goto_0
    return-void

    .line 209
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User has no communication privileges"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 211
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 213
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 214
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 212
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 215
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 210
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 4
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 182
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 184
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/IncrementScreenshotViewCount;->execute(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lio/reactivex/Single;

    move-result-object v1

    .line 185
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 186
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 187
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 193
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot View Count"

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot Display"

    const-string v2, "People Hub - Captures View"

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 197
    :cond_0
    return-void
.end method

.method public navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 222
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 233
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 226
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 228
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 229
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 227
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 230
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 225
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadGameClipsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->cancel()V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->loadScreenshotsAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadScreenshotsAsyncTask;->cancel()V

    .line 153
    :cond_1
    return-void
.end method

.method public resetLikeControlUpdate()V
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateLikeControl:Z

    .line 470
    return-void
.end method

.method public setFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    if-eq p1, v0, :cond_0

    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .line 120
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateViewModelState()V

    .line 121
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateAdapter()V

    .line 123
    :cond_0
    return-void
.end method

.method public shouldUpdateLikeControl()Z
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->updateLikeControl:Z

    return v0
.end method
