.class Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$4;
.super Ljava/lang/Object;
.source "VerticalScrollDockLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->initializeHeaderMomentumAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 636
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$4;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 640
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 641
    .local v1, "velocity":I
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    int-to-float v3, v1

    mul-float v0, v2, v3

    .line 642
    .local v0, "distance":F
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$4;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$800(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 644
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->end()V

    .line 646
    :cond_0
    return-void
.end method
