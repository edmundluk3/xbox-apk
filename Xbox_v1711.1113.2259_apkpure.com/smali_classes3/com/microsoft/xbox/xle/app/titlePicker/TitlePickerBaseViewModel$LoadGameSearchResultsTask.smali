.class Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TitlePickerBaseViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameSearchResultsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private pcResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final query:Ljava/lang/String;

.field private results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 276
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 277
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->query:Ljava/lang/String;

    .line 278
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 297
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$000()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->query:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/service/store/IStoreService;->getSearchResults(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 298
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->pcResults:Ljava/util/List;

    .line 300
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$200()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_0

    .line 301
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$300()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getMiniCatalog()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;

    move-result-object v1

    .line 303
    .local v1, "miniTitleHub":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    if-eqz v1, :cond_0

    .line 304
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$202(Ljava/util/List;)Ljava/util/List;

    .line 308
    .end local v1    # "miniTitleHub":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$200()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 309
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$200()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;

    .line 310
    .local v2, "win32Title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    if-eqz v2, :cond_1

    iget-object v4, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;->name:Ljava/lang/String;

    .line 311
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->query:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 312
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->pcResults:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 316
    .end local v2    # "win32Title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 320
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_1
    return-object v3

    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    if-nez v3, :cond_3

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 291
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->pcResults:Ljava/util/List;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$400(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Ljava/util/List;)V

    .line 330
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 270
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 325
    return-void
.end method
