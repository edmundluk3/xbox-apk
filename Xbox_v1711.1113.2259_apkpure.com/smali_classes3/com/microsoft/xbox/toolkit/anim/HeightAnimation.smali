.class public Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;
.super Landroid/view/animation/Animation;
.source "HeightAnimation.java"


# instance fields
.field private fromValue:I

.field private toValue:I

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 15
    iput p1, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->fromValue:I

    .line 16
    iput p2, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->toValue:I

    .line 17
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 26
    iget v1, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->toValue:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->fromValue:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 28
    .local v0, "newDelta":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->fromValue:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 29
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 31
    const-string v1, "HeightAnimation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public setTargetView(Landroid/view/View;)V
    .locals 1
    .param p1, "targetView"    # Landroid/view/View;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->view:Landroid/view/View;

    .line 21
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->fromValue:I

    .line 22
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method
