.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackDeleteMessage(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$conversationId:Ljava/lang/String;

.field final synthetic val$messageId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$conversationId:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$messageId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$conversationId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$messageId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 138
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 139
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ConversationId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 140
    const-string v1, "MessageId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$5;->val$messageId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 142
    const-string v1, "Messaging - Delete Message"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 143
    return-void
.end method
