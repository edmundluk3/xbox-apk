.class public Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "FriendFinderConfirmDialog.java"


# instance fields
.field private ConfirmMessageErrorSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final LOADING_STATE:I

.field private final VALID_CONTENT_STATE:I

.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

.field private confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private isTaskCompleted:Z

.field private nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

.field private overlayLoadingIndicator:Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .param p3, "completedTaskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;
    .param p4, "isTaskCompleted"    # Z

    .prologue
    .line 43
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->VALID_CONTENT_STATE:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->LOADING_STATE:I

    .line 44
    const v0, 0x7f030106

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setContentView(I)V

    .line 45
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .line 47
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    .line 49
    const v0, 0x7f0e05b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 50
    const v0, 0x7f0e05b6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    const v0, 0x7f0e05ba

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    const v0, 0x7f0e05b9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e05b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->ConfirmMessageErrorSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    const v0, 0x7f0e05bb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 55
    const v0, 0x7f0e05bc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 56
    const v0, 0x7f0e05b7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 57
    const v0, 0x7f0e023d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->overlayLoadingIndicator:Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;
    .param p1, "x1"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->dismissSelf()V

    return-void
.end method

.method private dismissSelf()V
    .locals 3

    .prologue
    .line 299
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->NONE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 300
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubFriendFinderState()V

    .line 301
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFriendFinderConfirmDialog()V

    .line 302
    return-void

    .line 299
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDialogState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 311
    packed-switch p1, :pswitch_data_0

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 313
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->overlayLoadingIndicator:Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->ConfirmMessageErrorSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 323
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->overlayLoadingIndicator:Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    goto :goto_0

    .line 311
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setShowRecommendationsView()V
    .locals 3

    .prologue
    .line 261
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->setShouldNavigateToSuggestionList(Z)V

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookUpsellTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookUpsellDescriptionLineOne()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07051b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070152

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 294
    :cond_1
    return-void
.end method

.method private setView()V
    .locals 7

    .prologue
    const v6, 0x7f07052a

    const/16 v2, 0x8

    const/4 v5, 0x1

    const v3, 0x7f07060f

    const/4 v4, 0x0

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->ConfirmMessageErrorSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 71
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$6;->$SwitchMap$com$microsoft$xbox$service$network$managers$friendfinder$FacebookManager$AsyncTaskType:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 73
    :pswitch_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-eqz v1, :cond_3

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v1, v2, :cond_1

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadGetPrivacyValueAsyncTask()V

    goto :goto_0

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v1, v2, :cond_0

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070556

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getDialogBodyTextId(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07053b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackUnlinkView()V

    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto :goto_0

    .line 101
    :cond_3
    invoke-direct {p0, v3, v6}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setViewWithErrorMessageOnly(II)V

    .line 102
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto :goto_0

    .line 106
    :pswitch_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-eqz v1, :cond_4

    .line 108
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->dismissSelf()V

    goto :goto_0

    .line 110
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getDialogErrorTextId(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)I

    move-result v0

    .line 111
    .local v0, "errorId":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->showErrorMessageInline(I)V

    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto/16 :goto_0

    .line 116
    .end local v0    # "errorId":I
    :pswitch_2
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-eqz v1, :cond_6

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070529

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getDialogBodyTextId(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_5

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07053a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    :cond_5
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackFriendFinderView()V

    .line 138
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto/16 :goto_0

    .line 132
    :cond_6
    invoke-direct {p0, v3, v6}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setViewWithErrorMessageOnly(II)V

    goto :goto_1

    .line 141
    :pswitch_3
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-eqz v1, :cond_7

    .line 142
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setShowRecommendationsView()V

    .line 146
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto/16 :goto_0

    .line 144
    :cond_7
    const v1, 0x7f070b6d

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->showErrorMessageInline(I)V

    goto :goto_2

    .line 151
    :pswitch_4
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-nez v1, :cond_0

    .line 152
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setShowRecommendationsView()V

    .line 153
    const v1, 0x7f070b6d

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->showErrorMessageInline(I)V

    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto/16 :goto_0

    .line 158
    :pswitch_5
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-nez v1, :cond_0

    .line 160
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 161
    const v1, 0x7f07054e

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setViewWithErrorMessageOnly(II)V

    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setDialogState(I)V

    goto/16 :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setViewWithErrorMessageOnly(II)V
    .locals 3
    .param p1, "ErrorTitleId"    # I
    .param p2, "ErrorBodyId"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageBodyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 258
    :cond_1
    return-void
.end method

.method private showErrorMessageInline(I)V
    .locals 3
    .param p1, "errorId"    # I

    .prologue
    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmMessageErrorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->ConfirmMessageErrorSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 308
    return-void
.end method


# virtual methods
.method public getAsyncTaskType()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    return-object v0
.end method

.method public getIsTaskCompleted()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    return v0
.end method

.method public getOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->dismissSelf()V

    .line 215
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setView()V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->NONE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    if-eq v0, v3, :cond_3

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 206
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelAsyncTasks()V

    .line 209
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->navigateToSuggestionList(Z)V

    .line 210
    return-void

    :cond_4
    move v0, v1

    .line 205
    goto :goto_0
.end method

.method public reportAsyncTaskCompleted(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V
    .locals 1
    .param p1, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setView()V

    .line 239
    return-void
.end method

.method public reportAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V
    .locals 1
    .param p1, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    .line 244
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->setView()V

    .line 245
    return-void
.end method

.method public reset(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V
    .locals 0
    .param p1, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .param p2, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;
    .param p3, "isTaskCompleted"    # Z

    .prologue
    .line 218
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->nextOptInStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 219
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->completedTaskType:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .line 220
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->isTaskCompleted:Z

    .line 221
    return-void
.end method
