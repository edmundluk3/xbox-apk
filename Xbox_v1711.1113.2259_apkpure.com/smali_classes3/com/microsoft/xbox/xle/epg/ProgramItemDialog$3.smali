.class Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;
.super Ljava/lang/Object;
.source "ProgramItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field final synthetic val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 139
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "providerId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "channelId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    .line 144
    .local v2, "showId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v3, v4, :cond_0

    .line 145
    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_0
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$3;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->dismiss()V

    .line 149
    return-void
.end method
