.class public Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;
.super Ljava/lang/Object;
.source "VortexServiceManager.java"


# static fields
.field private static final APPDEACTIVATEVERSION:I = 0x1

.field private static final CLIENTERRORVERSION:I = 0x1

.field private static final CRASHVERSION:I = 0x1

.field private static final DEVICEINFOVERSION:I = 0x1

.field private static final LaunchPageName:Ljava/lang/String; = "Launch"

.field private static final ONEGUIDEPAGEACTIONVERSION:I = 0x1

.field private static final PAGEACTIONVERSION:I = 0x1

.field private static final PAGEVIEWVERSION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VortexServiceManager"

.field private static final UNKNOWN_STRING:Ljava/lang/String; = "Unknown"

.field private static final URCPAGEACTIONVERSION:I = 0x1

.field private static final UrcButtonPressedPageAction:Ljava/lang/String; = "ButtonPressed"

.field private static final VESPERSESSIONEND:I = 0x1

.field private static final VESPERSESSIONSTART:I = 0x1

.field private static final VortexPoolDir:Ljava/lang/String; = "/XCE"

.field private static final VortexSettingUrl:Ljava/lang/String; = "https://settings.xboxlive.com/settings/feature/telemetry/settings"

.field private static appSessionId:Ljava/util/UUID; = null

.field private static context:Landroid/content/Context; = null

.field private static deviceModel:Ljava/lang/String; = null

.field private static emptyUUID:Ljava/util/UUID; = null

.field private static firstPageViewReported:Z = false

.field private static final iKey:Ljava/lang/String; = "A-SmtGlass"

.field private static initialized:Z

.field private static instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;


# instance fields
.field private attributionId:Ljava/lang/String;

.field private currentIndex:I

.field private filesInFolder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isEnabled:Z

.field private lang:Ljava/lang/String;

.field languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private legalLocale:Ljava/lang/String;

.field private osLocale:Ljava/lang/String;

.field private performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private region:Ljava/lang/String;

.field private settings:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 80
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    .line 89
    sput-boolean v1, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->initialized:Z

    .line 90
    sput-boolean v1, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->firstPageViewReported:Z

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->deviceModel:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/UUID;

    invoke-direct {v0, v2, v3, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->emptyUUID:Ljava/util/UUID;

    .line 93
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->appSessionId:Ljava/util/UUID;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->lang:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->legalLocale:Ljava/lang/String;

    .line 86
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->osLocale:Ljava/lang/String;

    .line 87
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->attributionId:Ljava/lang/String;

    .line 88
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->region:Ljava/lang/String;

    .line 113
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->clearPersistentValues()V

    .line 114
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V

    .line 115
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->settings:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 116
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->prefs()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 129
    return-void
.end method

.method private static UUIDtoByteArray(Ljava/util/UUID;)[B
    .locals 9
    .param p0, "uuid"    # Ljava/util/UUID;

    .prologue
    const/16 v8, 0x10

    .line 961
    invoke-virtual {p0}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v4

    .line 962
    .local v4, "msb":J
    invoke-virtual {p0}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v2

    .line 963
    .local v2, "lsb":J
    new-array v0, v8, [B

    .line 965
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v6, 0x8

    if-ge v1, v6, :cond_0

    .line 966
    rsub-int/lit8 v6, v1, 0x7

    mul-int/lit8 v6, v6, 0x8

    ushr-long v6, v4, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v0, v1

    .line 965
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 968
    :cond_0
    const/16 v1, 0x8

    :goto_1
    if-ge v1, v8, :cond_1

    .line 969
    rsub-int/lit8 v6, v1, 0x7

    mul-int/lit8 v6, v6, 0x8

    ushr-long v6, v2, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v0, v1

    .line 968
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 972
    :cond_1
    return-object v0
.end method

.method public static VortexInitialize(ZLandroid/content/Intent;)V
    .locals 6
    .param p0, "isNewLaunch"    # Z
    .param p1, "launchIntent"    # Landroid/content/Intent;

    .prologue
    .line 198
    if-eqz p0, :cond_0

    const-string v0, "Launch"

    .line 199
    .local v0, "launchType":Ljava/lang/String;
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->setEnabled(Z)V

    .line 200
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v3, v4, :cond_1

    .line 201
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;

    new-instance v3, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$1;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$1;-><init>()V

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;-><init>(Ljava/lang/Runnable;)V

    .line 215
    .local v1, "runnable":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 240
    .end local v1    # "runnable":Ljava/lang/Runnable;
    :goto_1
    const-string v3, "VortexServiceManager"

    const-string v4, "Vortex started"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void

    .line 198
    .end local v0    # "launchType":Ljava/lang/String;
    :cond_0
    const-string v0, "Activated"

    goto :goto_0

    .line 217
    .restart local v0    # "launchType":Ljava/lang/String;
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$3;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;

    new-instance v5, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;

    invoke-direct {v5, v0, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$3;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 238
    .local v2, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_1
.end method

.method public static VortexShutdown()V
    .locals 3

    .prologue
    .line 254
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v1, v2, :cond_0

    .line 255
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->stopCll()V

    .line 268
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :goto_0
    const-string v1, "VortexServiceManager"

    const-string v2, "Vortex shutdown"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void

    .line 257
    .end local v0    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$5;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$4;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$4;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$5;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 266
    .restart local v0    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    return-object v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 69
    sput-boolean p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->initialized:Z

    return p0
.end method

.method static synthetic access$200(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 69
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppLaunchInternal(Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppLaunchInternal(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppDeactivateInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/lang/String;JZILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J
    .param p4, "x3"    # Z
    .param p5, "x4"    # I
    .param p6, "x5"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPerformanceInternal(Ljava/lang/String;JZILjava/lang/String;)V

    return-void
.end method

.method private static assertIsNotUIThread()V
    .locals 2

    .prologue
    .line 176
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertTrue(Z)V

    .line 177
    return-void

    .line 176
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static assertNotNull(Ljava/lang/Object;)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 168
    const/4 v1, 0x0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertTrue(Ljava/lang/String;Z)V

    .line 169
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static assertTrue(Ljava/lang/String;Z)V
    .locals 0
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "condition"    # Z

    .prologue
    .line 185
    return-void
.end method

.method private static assertTrue(Z)V
    .locals 1
    .param p0, "condition"    # Z

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertTrue(Ljava/lang/String;Z)V

    .line 173
    return-void
.end method

.method public static createFile(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "createNew"    # I

    .prologue
    .line 426
    const-string v3, ""

    .line 427
    .local v3, "retValue":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getRootDir()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 428
    .local v1, "filePath":Ljava/lang/String;
    const-string v4, "VortexServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "trying to create file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 431
    .local v2, "newFile":Ljava/io/File;
    if-nez p1, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 447
    .end local v1    # "filePath":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 436
    .restart local v1    # "filePath":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 437
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 440
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_2

    .line 441
    move-object v3, v1

    :cond_2
    :goto_1
    move-object v1, v3

    .line 447
    goto :goto_0

    .line 443
    :catch_0
    move-exception v0

    .line 444
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "VortexServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to create file with exeception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static createSpool()V
    .locals 5

    .prologue
    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/XCE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "path":Ljava/lang/String;
    const-string v2, "VortexServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create spool at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/XCE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 281
    const-string v2, "VortexServiceManager"

    const-string/jumbo v3, "vortex spool directory already exist, ignore"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 284
    const-string v2, "VortexServiceManager"

    const-string/jumbo v3, "vortex spool directory created. "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deleteFile(Ljava/lang/String;)I
    .locals 5
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getRootDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 452
    .local v1, "filePath":Ljava/lang/String;
    const-string v2, "VortexServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "try to delete file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getRootDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 454
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 455
    const/4 v2, 0x1

    .line 457
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private findClose()I
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 859
    const/4 v0, 0x1

    .line 860
    .local v0, "allSuccess":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/XCE"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 861
    .local v3, "path":Ljava/lang/String;
    const-string v4, "VortexServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "reset spool at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/XCE"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 863
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 864
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_1

    aget-object v2, v6, v4

    .line 865
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v8

    if-nez v8, :cond_0

    .line 866
    const/4 v0, 0x0

    .line 864
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 870
    .end local v2    # "file":Ljava/io/File;
    :cond_1
    iput v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    .line 871
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    .line 874
    :cond_2
    return v0
.end method

.method public static getAppSessionId()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->appSessionId:Ljava/util/UUID;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertNotNull(Ljava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->appSessionId:Ljava/util/UUID;

    return-object v0
.end method

.method public static getConsoleId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 304
    const-string v1, "0"

    .line 305
    .local v1, "consoleId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 307
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 308
    iget-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    .line 312
    .end local v0    # "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :cond_0
    const-string v2, ""

    if-ne v1, v2, :cond_1

    .line 313
    const-string v1, "FIP"

    .line 316
    :cond_1
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->removePipes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getCurrentMemoryKB()I
    .locals 1

    .prologue
    .line 390
    invoke-static {}, Lcom/microsoft/xbox/toolkit/MemoryMonitor;->getTotalPss()I

    move-result v0

    return v0
.end method

.method public static getCurrentTime()[I
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 364
    const/16 v2, 0x8

    new-array v0, v2, [I

    .line 365
    .local v0, "response":[I
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 367
    .local v1, "rightNow":Ljava/util/Calendar;
    const/4 v2, 0x0

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, v0, v2

    .line 368
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v4

    .line 369
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aput v2, v0, v5

    .line 370
    const/4 v2, 0x3

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, v0, v2

    .line 371
    const/4 v2, 0x4

    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, v0, v2

    .line 372
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aput v2, v0, v6

    .line 373
    const/4 v2, 0x6

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, v0, v2

    .line 374
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aput v2, v0, v7

    .line 376
    return-object v0
.end method

.method public static getCurrentTimeUTC()Ljava/lang/String;
    .locals 6

    .prologue
    .line 381
    const-string/jumbo v0, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'z\'"

    .line 382
    .local v0, "ISO8601Format":Ljava/lang/String;
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 383
    .local v2, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 384
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 385
    .local v1, "date":Ljava/util/Date;
    const-string v3, "TESTDATA"

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getCurrentUser()Ljava/lang/String;
    .locals 3

    .prologue
    .line 347
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "xuid":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 349
    const-string v1, ""

    .line 351
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "x:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCurrentVersion()I
    .locals 4

    .prologue
    .line 356
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v0

    .line 358
    .local v0, "vercode":I
    const-string v1, "VortexServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get current version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return v0
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 289
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "deviceId":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertNotNull(Ljava/lang/Object;)V

    .line 291
    return-object v0
.end method

.method public static getDeviceModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->deviceModel:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AndroidPhone"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->deviceModel:Ljava/lang/String;

    .line 300
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->deviceModel:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->removePipes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFileSize(Ljava/lang/String;)J
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getRootDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 416
    .local v1, "filePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 417
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 418
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 420
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public static getFirstFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getFirstFileInternal()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFirstFileInternal()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 878
    const/4 v4, 0x0

    .line 879
    .local v4, "retVal":Ljava/lang/String;
    iput v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    .line 880
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    .line 882
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    if-nez v6, :cond_0

    const/4 v3, 0x0

    .line 883
    .local v3, "filesDir":Ljava/io/File;
    :goto_0
    if-eqz v3, :cond_2

    .line 884
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/XCE"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 885
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    invoke-static {v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertTrue(Z)V

    .line 886
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 887
    .local v2, "files":[Ljava/io/File;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 888
    array-length v6, v2

    :goto_1
    if-ge v5, v6, :cond_1

    aget-object v1, v2, v5

    .line 889
    .local v1, "file":Ljava/io/File;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 888
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 882
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    .end local v3    # "filesDir":Ljava/io/File;
    :cond_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v3

    goto :goto_0

    .line 893
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "files":[Ljava/io/File;
    .restart local v3    # "filesDir":Ljava/io/File;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 894
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    iget v6, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "retVal":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 895
    .restart local v4    # "retVal":Ljava/lang/String;
    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    .line 899
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    :cond_2
    return-object v4
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->instance:Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    return-object v0
.end method

.method public static getNextFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getNextFileInternal()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNextFileInternal()Ljava/lang/String;
    .locals 3

    .prologue
    .line 903
    const/4 v0, 0x0

    .line 904
    .local v0, "retVal":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    if-le v1, v2, :cond_0

    .line 905
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->filesInFolder:Ljava/util/ArrayList;

    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "retVal":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 906
    .restart local v0    # "retVal":Ljava/lang/String;
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->currentIndex:I

    .line 909
    :cond_0
    return-object v0
.end method

.method public static getRootDir()Ljava/lang/String;
    .locals 2

    .prologue
    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/XCE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSetting(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 913
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->settings:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 914
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->settings:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 917
    :goto_0
    return-object v0

    .line 916
    :cond_0
    const-string v0, "Vortex"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setting does not exist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getVortexSetting(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getSetting(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVortexSettings()V
    .locals 1

    .prologue
    .line 272
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->loadSettings()V

    .line 273
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->legalLocale:Ljava/lang/String;

    .line 127
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->osLocale:Ljava/lang/String;

    .line 128
    return-void
.end method

.method private loadSettings()V
    .locals 11

    .prologue
    .line 923
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertIsNotUIThread()V

    .line 925
    const-string v7, "https://settings.xboxlive.com/settings/feature/telemetry/settings"

    .line 926
    .local v7, "url":Ljava/lang/String;
    const-string v8, "VortexServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getting settings "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    const/4 v6, 0x0

    .line 929
    .local v6, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 931
    if-eqz v6, :cond_2

    .line 932
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 933
    .local v4, "response":Ljava/lang/String;
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 935
    .local v1, "gson":Lcom/google/gson/Gson;
    new-instance v8, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$12;

    invoke-direct {v8, p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$12;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$12;->getType()Ljava/lang/reflect/Type;

    move-result-object v8

    invoke-virtual {v1, v4, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Hashtable;

    .line 938
    .local v5, "result":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    .line 940
    .local v3, "newSetting":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 941
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v5, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v2, v9}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 948
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "newSetting":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "response":Ljava/lang/String;
    .end local v5    # "result":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 949
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v8, "VortexServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failed to retrieve the settings "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 951
    if-eqz v6, :cond_0

    .line 953
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 958
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-void

    .line 944
    .restart local v1    # "gson":Lcom/google/gson/Gson;
    .restart local v3    # "newSetting":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "response":Ljava/lang/String;
    .restart local v5    # "result":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    :try_start_3
    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->settings:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 951
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v3    # "newSetting":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "response":Ljava/lang/String;
    .end local v5    # "result":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_2
    if-eqz v6, :cond_0

    .line 953
    :try_start_4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 954
    :catch_1
    move-exception v8

    goto :goto_1

    .line 946
    :cond_2
    :try_start_5
    const-string v8, "VortexServiceManager"

    const-string v9, "Failed to retrieve the settings"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 951
    :catchall_0
    move-exception v8

    if-eqz v6, :cond_3

    .line 953
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 955
    :cond_3
    :goto_3
    throw v8

    .line 954
    .restart local v0    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v8

    goto :goto_1

    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v9

    goto :goto_3
.end method

.method private notNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 655
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->removePipes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private notNull(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 659
    if-nez p1, :cond_0

    invoke-static {p2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->removePipes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->removePipes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static postHttpData(Ljava/lang/String;[B)I
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "body"    # [B

    .prologue
    .line 463
    const-string v4, "VortexServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post data is called for url "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->assertIsNotUIThread()V

    .line 466
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-XBL-Contract-Version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-XBL-Build-Version"

    const-string v6, "current"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-Type"

    const-string/jumbo v6, "text/psv"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentLocale()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    const-string v4, "VortexServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Post data "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/16 v3, 0xc8

    .line 475
    .local v3, "statusCode":I
    const/4 v2, 0x0

    .line 477
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {p0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 478
    iget v3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    if-eqz v2, :cond_0

    .line 484
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 488
    :cond_0
    :goto_0
    const-string v4, "VortexServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post data is done "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    return v3

    .line 479
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "ServiceCommon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to post data "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481
    const/16 v3, 0x1f4

    .line 483
    if-eqz v2, :cond_0

    .line 484
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    goto :goto_0

    .line 483
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_1

    .line 484
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_1
    throw v4
.end method

.method private static removePipes(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "parameter"    # Ljava/lang/String;

    .prologue
    .line 188
    const-string/jumbo v0, "|"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static resetSpool()I
    .locals 1

    .prologue
    .line 394
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->findClose()I

    move-result v0

    return v0
.end method

.method private trackAppDeactivateInternal()V
    .locals 22

    .prologue
    .line 563
    const-string v16, "VortexServiceManager"

    const-string v17, "App deactivated"

    invoke-static/range {v16 .. v17}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->appLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->stopMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 565
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->appLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J

    move-result-wide v14

    .line 567
    .local v14, "totalTimeMs":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->connected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J

    move-result-wide v10

    .line 568
    .local v10, "connectedTimeMs":J
    sub-long v12, v14, v10

    .line 570
    .local v12, "disconnectedTimeMs":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J

    move-result-wide v8

    .line 571
    .local v8, "companionTime":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionConnected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J

    move-result-wide v4

    .line 572
    .local v4, "companionConnectedTimeMs":J
    sub-long v6, v8, v4

    .line 573
    .local v6, "companionDisconnectedTimeMs":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    .line 576
    .local v3, "device":Ljava/lang/String;
    const-wide/16 v16, 0x0

    cmp-long v16, v14, v16

    if-gtz v16, :cond_1

    .line 606
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    const-wide/16 v16, 0x0

    cmp-long v16, v10, v16

    if-ltz v16, :cond_0

    cmp-long v16, v10, v14

    if-gtz v16, :cond_0

    .line 580
    const-wide/16 v16, 0x0

    cmp-long v16, v12, v16

    if-ltz v16, :cond_0

    cmp-long v16, v12, v14

    if-gtz v16, :cond_0

    .line 582
    add-long v16, v10, v12

    cmp-long v16, v16, v14

    if-nez v16, :cond_0

    .line 587
    const-wide/16 v16, 0x0

    cmp-long v16, v8, v16

    if-ltz v16, :cond_0

    cmp-long v16, v8, v14

    if-gtz v16, :cond_0

    .line 589
    const-wide/16 v16, 0x0

    cmp-long v16, v4, v16

    if-ltz v16, :cond_0

    cmp-long v16, v4, v8

    if-gtz v16, :cond_0

    .line 591
    const-wide/16 v16, 0x0

    cmp-long v16, v6, v16

    if-ltz v16, :cond_0

    cmp-long v16, v6, v8

    if-gtz v16, :cond_0

    .line 593
    add-long v16, v4, v6

    cmp-long v16, v16, v8

    if-nez v16, :cond_0

    .line 596
    const-string v16, "VortexServiceManager"

    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v18, "App deactivated:%s|%s|%s|%s|%s|%s"

    const/16 v19, 0x6

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x2

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x5

    aput-object v3, v19, v20

    invoke-static/range {v17 .. v19}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    new-instance v2, Lxbox/smartglass/AppDeactivate;

    invoke-direct {v2}, Lxbox/smartglass/AppDeactivate;-><init>()V

    .line 599
    .local v2, "appDeactivate":Lxbox/smartglass/AppDeactivate;
    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lxbox/smartglass/AppDeactivate;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 600
    invoke-virtual {v2, v4, v5}, Lxbox/smartglass/AppDeactivate;->setCompanionConnectedTime(J)V

    .line 601
    invoke-virtual {v2, v6, v7}, Lxbox/smartglass/AppDeactivate;->setCompanionDisconnectedTime(J)V

    .line 602
    invoke-virtual {v2, v10, v11}, Lxbox/smartglass/AppDeactivate;->setConnectedTime(J)V

    .line 603
    invoke-virtual {v2, v12, v13}, Lxbox/smartglass/AppDeactivate;->setDisconnectedTime(J)V

    .line 605
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    goto/16 :goto_0
.end method

.method private static trackAppLaunchInternal(Ljava/lang/String;)V
    .locals 1
    .param p0, "launchType"    # Ljava/lang/String;

    .prologue
    .line 515
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppLaunchInternal(Ljava/lang/String;Landroid/content/Intent;)V

    .line 516
    return-void
.end method

.method private static trackAppLaunchInternal(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 5
    .param p0, "launchType"    # Ljava/lang/String;
    .param p1, "launchIntent"    # Landroid/content/Intent;

    .prologue
    .line 519
    const-string v3, "Launch"

    if-ne p0, v3, :cond_0

    .line 520
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;->LAUNCHED:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    invoke-static {v3, p1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V

    .line 525
    :goto_0
    new-instance v0, Lxbox/smartglass/DeviceInfo;

    invoke-direct {v0}, Lxbox/smartglass/DeviceInfo;-><init>()V

    .line 527
    .local v0, "deviceInfoEvent":Lxbox/smartglass/DeviceInfo;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v1

    .line 528
    .local v1, "x":I
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenHeight()I

    move-result v2

    .line 529
    .local v2, "y":I
    invoke-virtual {v0, v1}, Lxbox/smartglass/DeviceInfo;->setScreenResolutionX(I)V

    .line 530
    invoke-virtual {v0, v2}, Lxbox/smartglass/DeviceInfo;->setScreenResolutionY(I)V

    .line 531
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Lxbox/smartglass/DeviceInfo;->setColorDepth(I)V

    .line 533
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxbox/smartglass/DeviceInfo;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 535
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 537
    const-string v3, "Track App Launch Internal Complete"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    return-void

    .line 522
    .end local v0    # "deviceInfoEvent":Lxbox/smartglass/DeviceInfo;
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;->ACTIVATED:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    invoke-static {v3, p1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private trackPageActionInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "pageUri"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;
    .param p4, "relativeId"    # Ljava/lang/String;
    .param p5, "indexPos"    # I

    .prologue
    .line 642
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 652
    :goto_0
    return-void

    .line 646
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 647
    .local v0, "info":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 648
    const-string v1, "relativeId"

    invoke-virtual {v0, v1, p4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 649
    const-string v1, "indexPos"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 651
    invoke-static {p1, p2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method

.method private trackPageViewInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "fromPageName"    # Ljava/lang/String;
    .param p2, "toPageName"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "consoleId"    # Ljava/lang/String;
    .param p5, "relativeId"    # Ljava/lang/String;

    .prologue
    .line 688
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 698
    :goto_0
    return-void

    .line 692
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 693
    .local v0, "info":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 694
    const-string v1, "consoleId"

    invoke-virtual {v0, v1, p4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 695
    const-string v1, "relativeId"

    invoke-virtual {v0, v1, p5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 697
    invoke-static {p2, p1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method

.method private trackPerformanceInternal(Ljava/lang/String;JZILjava/lang/String;)V
    .locals 0
    .param p1, "measurementName"    # Ljava/lang/String;
    .param p2, "measurementTimeMS"    # J
    .param p4, "wasCached"    # Z
    .param p5, "retryCount"    # I
    .param p6, "contextString"    # Ljava/lang/String;

    .prologue
    .line 850
    return-void
.end method


# virtual methods
.method public beginTrackPerformance(Ljava/lang/String;)V
    .locals 2
    .param p1, "measurementName"    # Ljava/lang/String;

    .prologue
    .line 798
    new-instance v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    .line 799
    .local v0, "perfTimer":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->start()V

    .line 800
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 801
    return-void
.end method

.method public cancelTrackPerformance(Ljava/lang/String;)V
    .locals 1
    .param p1, "measurementName"    # Ljava/lang/String;

    .prologue
    .line 804
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 809
    :goto_0
    return-void

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public endTrackPerformance(Ljava/lang/String;)V
    .locals 1
    .param p1, "measurementName"    # Ljava/lang/String;

    .prologue
    .line 812
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    return-void
.end method

.method public endTrackPerformance(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "measurementName"    # Ljava/lang/String;
    .param p2, "context"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 816
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v0, :cond_1

    .line 817
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 828
    .local v11, "perfTimer":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    .line 830
    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v2

    .line 832
    .local v2, "measurementTimeMS":J
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_3

    .line 833
    if-nez p2, :cond_2

    const-string v6, ""

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPerformanceInternal(Ljava/lang/String;JZILjava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v6, p2

    goto :goto_1

    .line 835
    :cond_3
    new-instance v12, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$11;

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$10;

    move-object v6, p0

    move-object v7, p1

    move-wide v8, v2

    move-object v10, p2

    invoke-direct/range {v5 .. v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$10;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/lang/String;JLjava/lang/String;)V

    invoke-direct {v12, p0, v0, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$11;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 844
    .local v12, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method public getAttributionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->attributionId:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->legalLocale:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 329
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->legalLocale:Ljava/lang/String;

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->legalLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentMarket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->getLocationInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getMarket()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceLocale()Ljava/lang/String;
    .locals 6

    .prologue
    .line 339
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->osLocale:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 340
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 341
    .local v0, "deviceLocale":Ljava/util/Locale;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s-%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->osLocale:Ljava/lang/String;

    .line 343
    .end local v0    # "deviceLocale":Ljava/util/Locale;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->osLocale:Ljava/lang/String;

    return-object v1
.end method

.method public getIsEnabled()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    return v0
.end method

.method public initInteropCll(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 136
    const-string v1, "Initialize xbidp CLL"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    :try_start_0
    const-string v1, "A-SmtGlass"

    invoke-static {p1, v1}, Lcom/microsoft/xbox/idp/interop/Interop;->InitCLL(Landroid/content/Context;Ljava/lang/String;)V

    .line 139
    const-string/jumbo v1, "xbidp CLL initialized"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Could not initialize the xbidp Cll"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public isTrackingPerformance(Ljava/lang/String;)Z
    .locals 1
    .param p1, "measurementName"    # Ljava/lang/String;

    .prologue
    .line 855
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->performanceTimers:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setAttributionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->attributionId:Ljava/lang/String;

    .line 325
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 244
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    .line 245
    return-void
.end method

.method public startCll()V
    .locals 3

    .prologue
    .line 147
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Could not start the UTC client"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public stopCll()V
    .locals 3

    .prologue
    .line 156
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Could not stop the UTC client"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public trackAppDeactivate()V
    .locals 3

    .prologue
    .line 541
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 560
    :goto_0
    return-void

    .line 545
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v1, v2, :cond_1

    .line 546
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppDeactivateInternal()V

    goto :goto_0

    .line 548
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$9;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$8;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$8;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$9;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 557
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method public trackAppLaunch(Ljava/lang/String;)V
    .locals 3
    .param p1, "launchType"    # Ljava/lang/String;

    .prologue
    .line 494
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 512
    :goto_0
    return-void

    .line 498
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v1, v2, :cond_1

    .line 499
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppLaunchInternal(Ljava/lang/String;)V

    goto :goto_0

    .line 501
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$7;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$6;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$6;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$7;-><init>(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 510
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method public trackClientError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "errorName"    # Ljava/lang/String;
    .param p2, "errorCode"    # Ljava/lang/String;

    .prologue
    .line 701
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    return-void
.end method

.method public trackCompanionExit(Ljava/lang/String;ILjava/util/UUID;)V
    .locals 0
    .param p1, "companionId"    # Ljava/lang/String;
    .param p2, "durationMs"    # I
    .param p3, "sessionId"    # Ljava/util/UUID;

    .prologue
    .line 719
    return-void
.end method

.method public trackCompanionStart(Ljava/lang/String;Ljava/util/UUID;)V
    .locals 0
    .param p1, "companionId"    # Ljava/lang/String;
    .param p2, "sessionId"    # Ljava/util/UUID;

    .prologue
    .line 710
    return-void
.end method

.method public trackConsolePlayTo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "titleId"    # Ljava/lang/String;
    .param p2, "params"    # Ljava/lang/String;

    .prologue
    .line 723
    return-void
.end method

.method public trackOneGuideAppPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "providerId"    # Ljava/lang/String;
    .param p3, "channelId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    .line 757
    const-string v3, "App"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuidePageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    return-void
.end method

.method public trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "headendId"    # Ljava/lang/String;
    .param p3, "channelId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    .line 753
    const-string v3, "EPG"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuidePageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    return-void
.end method

.method public trackOneGuidePageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "headendId"    # Ljava/lang/String;
    .param p3, "channelType"    # Ljava/lang/String;
    .param p4, "channelId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;

    .prologue
    .line 730
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v3, :cond_0

    .line 750
    :goto_0
    return-void

    .line 734
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 736
    .local v2, "pageUri":Ljava/lang/String;
    const/4 v0, 0x0

    .line 737
    .local v0, "channelTypeValue":I
    const-string v3, "EPG"

    if-ne p3, v3, :cond_2

    .line 738
    const/4 v0, 0x1

    .line 743
    :cond_1
    :goto_1
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 744
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v3, "Show Id"

    invoke-virtual {v1, v3, p5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 745
    const-string v3, "Channel Id"

    invoke-virtual {v1, v3, p4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 746
    const-string v3, "Channel Type"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 747
    const-string v3, "Provider Id"

    invoke-virtual {v1, v3, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 749
    invoke-static {p1, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 739
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_2
    const-string v3, "App"

    if-ne p3, v3, :cond_1

    .line 740
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public trackPageAction(Ljava/lang/String;)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 609
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 610
    .local v2, "pageUri":Ljava/lang/String;
    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 611
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;I)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "indexPos"    # I

    .prologue
    .line 614
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 615
    .local v2, "pageUri":Ljava/lang/String;
    const-string v3, ""

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 616
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "context"    # Ljava/lang/String;

    .prologue
    .line 619
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 620
    .local v2, "pageUri":Ljava/lang/String;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 621
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "context"    # Ljava/lang/String;
    .param p3, "indexPos"    # I

    .prologue
    .line 629
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 630
    .local v2, "pageUri":Ljava/lang/String;
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 631
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "pageUri"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;

    .prologue
    .line 634
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 635
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "context"    # Ljava/lang/String;
    .param p3, "relativeId"    # Ljava/lang/String;
    .param p4, "indexPos"    # I

    .prologue
    .line 624
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .local v2, "pageUri":Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    .line 625
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 626
    return-void
.end method

.method public trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "pageUri"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;
    .param p4, "relativeId"    # Ljava/lang/String;
    .param p5, "indexPos"    # I

    .prologue
    .line 638
    invoke-direct/range {p0 .. p5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageActionInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 639
    return-void
.end method

.method public trackPageView(Ljava/lang/String;)V
    .locals 1
    .param p1, "toPageName"    # Ljava/lang/String;

    .prologue
    .line 663
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    return-void
.end method

.method public trackPageView(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "toPageName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 667
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v1

    .local v1, "fromPageName":Ljava/lang/String;
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    .line 668
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    return-void
.end method

.method public trackPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "fromPageName"    # Ljava/lang/String;
    .param p2, "toPageName"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 672
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    return-void
.end method

.method public trackPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "fromPageName"    # Ljava/lang/String;
    .param p2, "toPageName"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "consoleId"    # Ljava/lang/String;
    .param p5, "relativeId"    # Ljava/lang/String;

    .prologue
    .line 677
    sget-boolean v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->firstPageViewReported:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const-string v0, ""

    if-ne p1, v0, :cond_1

    .line 678
    :cond_0
    const-string v1, "Launch"

    .line 683
    .local v1, "fromPageNameOrLaunchName":Ljava/lang/String;
    :goto_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->firstPageViewReported:Z

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 684
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageViewInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    return-void

    .line 680
    .end local v1    # "fromPageNameOrLaunchName":Ljava/lang/String;
    :cond_1
    move-object v1, p1

    .restart local v1    # "fromPageNameOrLaunchName":Ljava/lang/String;
    goto :goto_0
.end method

.method public trackSignIn(Ljava/lang/String;JZZZ)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "durationMS"    # J
    .param p4, "isSuccess"    # Z
    .param p5, "isManual"    # Z
    .param p6, "isTokenCached"    # Z

    .prologue
    .line 727
    return-void
.end method

.method public trackURCPageAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "buttonId"    # Ljava/lang/String;

    .prologue
    .line 761
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v2, :cond_0

    .line 770
    :goto_0
    return-void

    .line 765
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v1

    .line 767
    .local v1, "pageUri":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 768
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "Button Id"

    invoke-virtual {v0, v2, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769
    const-string v2, "ButtonPressed"

    invoke-static {v2, v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method

.method public trackVesperSessionEnd(Ljava/util/UUID;I)V
    .locals 2
    .param p1, "sessionId"    # Ljava/util/UUID;
    .param p2, "sessionDuration"    # I

    .prologue
    .line 785
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 795
    :goto_0
    return-void

    .line 789
    :cond_0
    new-instance v0, Lxbox/smartglass/VesperSessionEnd;

    invoke-direct {v0}, Lxbox/smartglass/VesperSessionEnd;-><init>()V

    .line 790
    .local v0, "end":Lxbox/smartglass/VesperSessionEnd;
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/VesperSessionEnd;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 791
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/VesperSessionEnd;->setSessionId(Ljava/lang/String;)V

    .line 792
    invoke-virtual {v0, p2}, Lxbox/smartglass/VesperSessionEnd;->setDuration(I)V

    .line 794
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    goto :goto_0
.end method

.method public trackVesperSessionStart(Ljava/util/UUID;)V
    .locals 2
    .param p1, "sessionId"    # Ljava/util/UUID;

    .prologue
    .line 773
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->isEnabled:Z

    if-nez v1, :cond_0

    .line 782
    :goto_0
    return-void

    .line 776
    :cond_0
    new-instance v0, Lxbox/smartglass/VesperSessionStart;

    invoke-direct {v0}, Lxbox/smartglass/VesperSessionStart;-><init>()V

    .line 777
    .local v0, "start":Lxbox/smartglass/VesperSessionStart;
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/VesperSessionStart;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 778
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/VesperSessionStart;->setSessionId(Ljava/lang/String;)V

    .line 779
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentTimeUTC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/VesperSessionStart;->setStart(Ljava/lang/String;)V

    .line 781
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    goto :goto_0
.end method
