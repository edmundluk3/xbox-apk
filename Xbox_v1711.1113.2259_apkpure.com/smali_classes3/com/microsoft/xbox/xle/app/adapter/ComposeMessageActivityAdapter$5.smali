.class Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;
.super Ljava/lang/Object;
.source "ComposeMessageActivityAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getIsRecipientNonEmpty()Z

    move-result v0

    .line 154
    .local v0, "b":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    .line 162
    .end local v0    # "b":Z
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sget v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->CHARACTERLIMIT:I

    if-lt v1, v2, :cond_1

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;ZI)V

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    .line 171
    :goto_1
    return-void

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    goto :goto_0

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v1, v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;ZI)V

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    .line 175
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "messageText":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 181
    .local v0, "enableButton":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V

    .line 183
    return-void

    .line 179
    .end local v0    # "enableButton":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
