.class final Lcom/microsoft/xbox/smartglass/controls/CanvasKeyMapper;
.super Ljava/lang/Object;
.source "CanvasKeyMapper.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ToControlKey(Lcom/microsoft/xbox/smartglass/controls/CanvasKey;)Lcom/microsoft/xbox/smartglass/ControlKey;
    .locals 2
    .param p0, "canvasKey"    # Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/CanvasKeyMapper$1;->$SwitchMap$com$microsoft$xbox$smartglass$controls$CanvasKey:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 107
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Empty:Lcom/microsoft/xbox/smartglass/ControlKey;

    :goto_0
    return-object v0

    .line 13
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Return:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 15
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Pause:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 17
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Escape:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 19
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num0:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 21
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num1:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 23
    :pswitch_5
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num2:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 25
    :pswitch_6
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num3:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 27
    :pswitch_7
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num4:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 29
    :pswitch_8
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num5:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 31
    :pswitch_9
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num6:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 33
    :pswitch_a
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num7:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 35
    :pswitch_b
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num8:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 37
    :pswitch_c
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Num9:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 39
    :pswitch_d
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserBack:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 41
    :pswitch_e
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserRefresh:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 43
    :pswitch_f
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->BrowserStop:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 45
    :pswitch_10
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeMute:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 47
    :pswitch_11
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeDown:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 49
    :pswitch_12
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->VolumeUp:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 51
    :pswitch_13
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Play:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 53
    :pswitch_14
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->PadA:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 55
    :pswitch_15
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->PadB:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 57
    :pswitch_16
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->PadX:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 59
    :pswitch_17
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->PadY:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 61
    :pswitch_18
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->LeftTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 63
    :pswitch_19
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->RightTrigger:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 65
    :pswitch_1a
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Guide:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 67
    :pswitch_1b
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Up:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 69
    :pswitch_1c
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Down:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 71
    :pswitch_1d
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Left:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 73
    :pswitch_1e
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Right:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 75
    :pswitch_1f
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Start:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 77
    :pswitch_20
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Back:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 79
    :pswitch_21
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->DvdMenu:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 81
    :pswitch_22
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Display:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 83
    :pswitch_23
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Stop:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 85
    :pswitch_24
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Record:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 87
    :pswitch_25
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->FastForward:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 89
    :pswitch_26
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Rewind:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 91
    :pswitch_27
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Skip:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 93
    :pswitch_28
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Replay:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 95
    :pswitch_29
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Info:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto :goto_0

    .line 97
    :pswitch_2a
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Asterisk:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto/16 :goto_0

    .line 99
    :pswitch_2b
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Pound:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto/16 :goto_0

    .line 101
    :pswitch_2c
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->Title:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto/16 :goto_0

    .line 103
    :pswitch_2d
    sget-object v0, Lcom/microsoft/xbox/smartglass/ControlKey;->MediaCenter:Lcom/microsoft/xbox/smartglass/ControlKey;

    goto/16 :goto_0

    .line 11
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
    .end packed-switch
.end method
