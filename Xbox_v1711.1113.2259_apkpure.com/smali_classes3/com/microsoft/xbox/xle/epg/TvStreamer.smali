.class public final enum Lcom/microsoft/xbox/xle/epg/TvStreamer;
.super Ljava/lang/Enum;
.source "TvStreamer.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/epg/TvStreamer;",
        ">;",
        "Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/epg/TvStreamer;

.field private static final BEST_QUALITY:Ljava/lang/String; = "best"

.field private static final HIGH_QUALITY:Ljava/lang/String; = "high"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/xle/epg/TvStreamer;

.field private static final LOW_QUALITY:Ljava/lang/String; = "low"

.field private static final MEDIUM_QUALITY:Ljava/lang/String; = "medium"

.field private static final SKIP_BACK_RATE_IN_SECONDS:I = -0x7

.field private static final SKIP_FWD_RATE_IN_SECONDS:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "TvStreamer"


# instance fields
.field private mActivityContentView:Landroid/view/View;

.field private mActivityRightPaneView:Landroid/view/View;

.field private final mCloseButtonExecutor:Ljava/lang/Runnable;

.field private mCloseStreamBtn:Landroid/widget/Button;

.field private final mContentView:Landroid/view/View;

.field private final mControlsExecutor:Ljava/lang/Runnable;

.field private mControlsView:Landroid/view/ViewGroup;

.field private mErrorIconPip:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mErrorText:Landroid/widget/TextView;

.field private mErrorView:Landroid/view/ViewGroup;

.field private mIsArmDevice:Z

.field private mIsErrorState:Z

.field private mIsPauseBufferEnabled:Z

.field private mLiveBtn:Landroid/view/View;

.field private mLoadSpinner:Landroid/widget/ProgressBar;

.field private final mMainActivity:Landroid/support/v7/app/AppCompatActivity;

.field private mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

.field private mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

.field private mMediaRemote:Landroid/view/View;

.field private mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

.field private mOneGuideBtn:Landroid/view/View;

.field private mOriginalOrientation:I

.field private final mParentView:Landroid/view/ViewGroup;

.field private mPauseBtn:Landroid/view/View;

.field private final mPipHeight:I

.field private final mPipWidth:I

.field private mPlayBtn:Landroid/view/View;

.field private mQualityBtn:Landroid/view/View;

.field private mQualityMenu:Landroid/widget/PopupMenu;

.field private mRadioIndicator:Landroid/widget/LinearLayout;

.field private mRadioMessage:Landroid/widget/TextView;

.field private mResumeStreamingBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private mResumeStreamingLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mResumeStreamingView:Landroid/view/ViewGroup;

.field private final mScreenRatio:F

.field private mShouldShowTryAgain:Z

.field private mSkipBackBtn:Landroid/view/View;

.field private mSkipFwdBtn:Landroid/view/View;

.field private mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

.field private mVideoRatio:F

.field private mVideoView:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->INSTANCE:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/xle/epg/TvStreamer;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer;->INSTANCE:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->$VALUES:[Lcom/microsoft/xbox/xle/epg/TvStreamer;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f0e0787

    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    .line 872
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$18;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$18;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsExecutor:Ljava/lang/Runnable;

    .line 902
    new-instance v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$19;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$19;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseButtonExecutor:Ljava/lang/Runnable;

    .line 137
    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v5, "armeabi-v7a"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsArmDevice:Z

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    .line 139
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/support/v7/app/AppCompatActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 140
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030030

    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4, v7}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    .line 141
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4, v7}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    .line 142
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090585

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPipHeight:I

    .line 143
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090586

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPipWidth:I

    .line 144
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v4

    .line 145
    .local v2, "screenHeight":F
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v4

    .line 146
    .local v3, "screenWidth":F
    cmpl-float v4, v2, v3

    if-lez v4, :cond_0

    div-float v4, v3, v2

    :goto_0
    iput v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mScreenRatio:F

    .line 147
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getRequestedOrientation()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOriginalOrientation:I

    .line 149
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    const v5, 0x7f0e0786

    invoke-virtual {v4, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityContentView:Landroid/view/View;

    .line 150
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    const v5, 0x7f0e0788

    invoke-virtual {v4, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityRightPaneView:Landroid/view/View;

    .line 152
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0185

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    .line 154
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    .line 155
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorText:Landroid/widget/TextView;

    .line 156
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b8d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    .line 157
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0190

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLoadSpinner:Landroid/widget/ProgressBar;

    .line 158
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0186

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    .line 159
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0187

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioMessage:Landroid/widget/TextView;

    .line 160
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    .line 161
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$1;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 169
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0189

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 170
    new-instance v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$2;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    .line 185
    .local v1, "resumeBtnAction":Landroid/view/View$OnClickListener;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 188
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e018c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    .line 189
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0188

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorIconPip:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 191
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b8e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    .line 192
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b8f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOneGuideBtn:Landroid/view/View;

    .line 193
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOneGuideBtn:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$3;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$3;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b90

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipBackBtn:Landroid/view/View;

    .line 213
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipBackBtn:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$4;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$4;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b91

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPauseBtn:Landroid/view/View;

    .line 223
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPauseBtn:Landroid/view/View;

    const-string v5, "btn.pause"

    const-string v6, "Stream FullScreen Pause"

    invoke-direct {p0, v4, v5, v8, v6}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupControlButton(Landroid/view/View;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    .line 225
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b92

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPlayBtn:Landroid/view/View;

    .line 226
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPlayBtn:Landroid/view/View;

    const-string v5, "btn.play"

    const-string v6, "Stream FullScreen Play"

    invoke-direct {p0, v4, v5, v8, v6}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupControlButton(Landroid/view/View;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    .line 228
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b93

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipFwdBtn:Landroid/view/View;

    .line 229
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipFwdBtn:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$5;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$5;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b94

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaRemote:Landroid/view/View;

    .line 240
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaRemote:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$6;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$6;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b95

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLiveBtn:Landroid/view/View;

    .line 251
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLiveBtn:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$7;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$7;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const v5, 0x7f0e0b96

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityBtn:Landroid/view/View;

    .line 262
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityBtn:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$8;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    new-instance v4, Landroid/widget/PopupMenu;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityBtn:Landroid/view/View;

    invoke-direct {v4, v5, v6}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityMenu:Landroid/widget/PopupMenu;

    .line 291
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0f000b

    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 292
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityMenu:Landroid/widget/PopupMenu;

    new-instance v5, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v4, v5}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 329
    return-void

    .line 146
    .end local v1    # "resumeBtnAction":Landroid/view/View$OnClickListener;
    :cond_0
    div-float v4, v2, v3

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/epg/TvStreamer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateBufferControls(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupFullScreenUI()V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/epg/TvStreamer;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateProgressBarVisibility(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/xle/epg/TvStreamer;FII)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .param p1, "x1"    # F
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->adjustVideoViewRatio(FII)V

    return-void
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/xle/epg/TvStreamer;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showError(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateStreamState()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsExecutor:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseButtonExecutor:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityBtn:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsPauseBufferEnabled:Z

    return v0
.end method

.method private adjustVideoViewRatio(FII)V
    .locals 4
    .param p1, "ratio"    # F
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/16 v3, 0x11

    const/4 v2, 0x0

    .line 621
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 622
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mScreenRatio:F

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 623
    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 624
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    int-to-float v1, v1

    div-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 625
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 626
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 633
    :goto_0
    const-string v1, "TvStreamer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "adjusted to ratio = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 636
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoRatio:F

    .line 637
    return-void

    .line 628
    :cond_0
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 629
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 630
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 631
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private animateView(Landroid/view/View;IIII)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "toWidth"    # I
    .param p3, "toHeight"    # I
    .param p4, "toX"    # I
    .param p5, "toY"    # I

    .prologue
    .line 563
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 565
    .local v2, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p2

    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p3

    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p4

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p5

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;

    move-object v1, p0

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Landroid/widget/FrameLayout$LayoutParams;IIIILandroid/view/View;)V

    .line 566
    invoke-virtual {v8, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 594
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->INSTANCE:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    return-object v0
.end method

.method private initUI()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 419
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLoadSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorIconPip:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setVisibility(I)V

    .line 430
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaRemote:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 431
    return-void
.end method

.method private setActivityContentVisibility(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1065
    if-eqz p1, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityContentView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 1067
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityRightPaneView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 1072
    :goto_0
    return-void

    .line 1069
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityContentView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 1070
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mActivityRightPaneView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private setupControlButton(Landroid/view/View;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p1, "button"    # Landroid/view/View;
    .param p2, "command"    # Ljava/lang/String;
    .param p4, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 597
    .local p3, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 618
    :goto_0
    return-void

    .line 600
    :cond_0
    if-eqz p2, :cond_1

    .line 601
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;

    invoke-direct {v0, p0, p4, p2, p3}, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 616
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private setupFullScreenUI()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 434
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v2, v5}, Landroid/support/v7/app/AppCompatActivity;->setRequestedOrientation(I)V

    .line 435
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->initUI()V

    .line 437
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v2}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 440
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v2, v3, :cond_0

    .line 441
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 442
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 443
    .local v1, "visibleFrame":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v2}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 444
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 445
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 446
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 447
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 449
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 450
    iget v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoRatio:F

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-direct {p0, v2, v3, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->adjustVideoViewRatio(FII)V

    .line 456
    .end local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v1    # "visibleFrame":Landroid/graphics/Rect;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 459
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setActivityContentVisibility(Z)V

    .line 461
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 463
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    new-instance v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 508
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateStreamState()V

    .line 509
    return-void
.end method

.method private setupMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/microsoft/playready/PRMediaPlayer;

    .prologue
    .line 640
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-virtual {p1, v0}, Lcom/microsoft/playready/PRMediaPlayer;->addOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 672
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$15;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$15;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {p1, v0}, Lcom/microsoft/playready/PRMediaPlayer;->addOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 688
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {p1, v0}, Lcom/microsoft/playready/PRMediaPlayer;->addOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 699
    return-void
.end method

.method private showError(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "shouldShowTryAgain"    # Z

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 742
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v3, :cond_0

    .line 743
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setRequestedOrientation(I)V

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getSeekPerformed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v3, "Stream Seek"

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->cancelTrackPerformance(Ljava/lang/String;)V

    .line 748
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setSeekPerformed(Z)V

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v0, :cond_2

    .line 752
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 755
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v3, :cond_3

    .line 756
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 758
    :cond_3
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mShouldShowTryAgain:Z

    .line 759
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 760
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mQualityBtn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 761
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateProgressBarVisibility(Z)Z

    .line 762
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateBufferControls(Z)V

    .line 765
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setAlpha(F)V

    .line 766
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 767
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsExecutor:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 769
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 770
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 771
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseButtonExecutor:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 773
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 774
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 776
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 777
    const-string v0, "TvStreamer"

    const-string v3, "Showing resume screen"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v4, :cond_7

    move v0, v2

    :goto_3
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 779
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f070c84

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 780
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v3, v4, :cond_8

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 797
    :goto_5
    return-void

    :cond_4
    move v0, v2

    .line 766
    goto :goto_0

    :cond_5
    move v0, v2

    .line 770
    goto :goto_1

    :cond_6
    move v0, v2

    .line 774
    goto :goto_2

    :cond_7
    move v0, v1

    .line 778
    goto :goto_3

    :cond_8
    move v1, v2

    .line 780
    goto :goto_4

    .line 782
    :cond_9
    const-string v0, "TvStreamer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Showing error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsErrorState:Z

    .line 784
    if-eqz p2, :cond_a

    .line 785
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f0704c5

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 786
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 790
    :goto_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v3, :cond_b

    .line 791
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorIconPip:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 792
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_5

    .line 788
    :cond_a
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_6

    .line 794
    :cond_b
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorIconPip:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_5
.end method

.method private showStartupError(Ljava/lang/String;)V
    .locals 5
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 708
    const-string v0, "TvStreamer"

    const-string v1, "Showing startup error"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AppCompatActivity;->setRequestedOrientation(I)V

    .line 711
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 713
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v2}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v3}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 717
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 718
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 719
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$17;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$17;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 726
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 730
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setActivityContentVisibility(Z)V

    .line 732
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 733
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 734
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseButtonExecutor:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 736
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingView:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 739
    return-void
.end method

.method private updateBufferControls(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 835
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLiveBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 836
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPauseBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 837
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPlayBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 838
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipBackBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 839
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mSkipFwdBtn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 840
    return-void
.end method

.method private updateProgressBarVisibility(Z)Z
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v0, 0x0

    .line 825
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getCurrentSource()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tuner"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsLoading()Z

    move-result v1

    if-nez v1, :cond_0

    .line 826
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setVisibility(I)V

    .line 827
    const/4 v0, 0x1

    .line 830
    :goto_0
    return v0

    .line 829
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateStreamState()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLoadSpinner:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 802
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mLoadSpinner:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsLoading()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 807
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsRadioChannel()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 808
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 809
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioMessage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 810
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioMessage:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v3, v4, :cond_4

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 818
    :cond_1
    :goto_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsErrorState:Z

    if-eqz v0, :cond_2

    .line 819
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mShouldShowTryAgain:Z

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showError(Ljava/lang/String;Z)V

    .line 821
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 802
    goto :goto_0

    :cond_4
    move v2, v1

    .line 810
    goto :goto_1

    .line 813
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mRadioIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/epg/TvStreamer;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->$VALUES:[Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/epg/TvStreamer;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/epg/TvStreamer;

    return-object v0
.end method


# virtual methods
.method public backgroundStreamerUI()V
    .locals 2

    .prologue
    .line 552
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOriginalOrientation:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setRequestedOrientation(I)V

    .line 553
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 554
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 555
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->BACKGROUND:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 558
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setActivityContentVisibility(Z)V

    .line 559
    return-void
.end method

.method public dismiss(Z)V
    .locals 5
    .param p1, "byUser"    # Z

    .prologue
    const/4 v4, 0x0

    .line 843
    const-string v1, "TvStreamer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dismissing stream (user initiated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getSeekPerformed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 847
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Stream Seek"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->cancelTrackPerformance(Ljava/lang/String;)V

    .line 848
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setSeekPerformed(Z)V

    .line 851
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 852
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 853
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v4, v4, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupControlButton(Landroid/view/View;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    .line 852
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 856
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v1, :cond_2

    .line 857
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 860
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 861
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 865
    :cond_3
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setActivityContentVisibility(Z)V

    .line 867
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOriginalOrientation:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setRequestedOrientation(I)V

    .line 868
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->show()V

    .line 869
    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setStreamerState(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 870
    return-void
.end method

.method public getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    return-object v0
.end method

.method public onChannelChanged()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 993
    const-string v0, "TvStreamer"

    const-string v1, "onChannelChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 996
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 998
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 1000
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 1001
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1004
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 1005
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1007
    :cond_2
    return-void
.end method

.method public onChannelTypeChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "channelType"    # Ljava/lang/String;

    .prologue
    .line 1011
    const-string v0, "TvStreamer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChannelTypeChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 1015
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$21;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$21;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 1024
    :goto_0
    return-void

    .line 1022
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateStreamState()V

    goto :goto_0
.end method

.method public onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
    .locals 12
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1045
    const-string v1, "TvStreamer"

    const-string v2, "onLiveTvInfoReceived"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    if-eqz v1, :cond_0

    .line 1048
    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->enabled:Z

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsPauseBufferEnabled:Z

    .line 1049
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsPauseBufferEnabled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1051
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateProgressBarVisibility(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1052
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateBufferControls(Z)V

    .line 1053
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    .line 1054
    .local v0, "pauseBufferInfo":Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    iget-wide v2, v0, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferStart:J

    iget-wide v4, v0, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferEnd:J

    iget-wide v6, v0, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferCurrent:J

    iget-wide v8, v0, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->epoch:J

    iget-wide v10, v0, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->maxBufferSize:J

    invoke-virtual/range {v1 .. v11}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->initialize(JJJJJ)V

    .line 1055
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaProgressBar:Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->requestLayout()V

    .line 1062
    .end local v0    # "pauseBufferInfo":Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;
    :cond_0
    :goto_0
    return-void

    .line 1058
    :cond_1
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateProgressBarVisibility(Z)Z

    .line 1059
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateBufferControls(Z)V

    goto :goto_0
.end method

.method public onLoadingChanged(Z)V
    .locals 3
    .param p1, "isLoading"    # Z

    .prologue
    .line 1028
    const-string v0, "TvStreamer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadingChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 1032
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$22;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$22;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateStreamState()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->removeListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;)Z

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v0, v1, :cond_1

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_1
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->dismiss(Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->addListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;)V

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v0, v1, :cond_1

    .line 349
    const-string v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showError(Ljava/lang/String;Z)V

    .line 351
    :cond_1
    return-void
.end method

.method public onStreamError(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "shouldShowTryAgain"    # Z

    .prologue
    .line 973
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 974
    const-string v0, "TvStreamer"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :goto_0
    return-void

    .line 979
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_1

    .line 980
    new-instance v0, Lcom/microsoft/xbox/xle/epg/TvStreamer$20;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/TvStreamer$20;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 987
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showError(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onStreamFormatChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 946
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    .line 947
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-eqz v1, :cond_1

    .line 948
    const-string v1, "TvStreamer"

    const-string v2, "onStreamFormatChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsErrorState:Z

    .line 951
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 952
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateProgressBarVisibility(Z)Z

    .line 954
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 955
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mErrorView:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 959
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/playready/PRMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 968
    :cond_1
    :goto_0
    return-void

    .line 960
    :catch_0
    move-exception v0

    .line 961
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "TvStreamer"

    const-string v2, "IllegalStateException during setDisplay"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 962
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0

    .line 963
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 964
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "TvStreamer"

    const-string v2, "IllegalArgumentException during setDisplay"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 965
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0
.end method

.method public onTvStreamClosed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 940
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 941
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 942
    return-void
.end method

.method public onTvStreamClosing()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 934
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 935
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mResumeStreamingBtnPip:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 936
    return-void
.end method

.method protected setStreamerState(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    .line 134
    return-void
.end method

.method public setupPipUI()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 512
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mOriginalOrientation:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setRequestedOrientation(I)V

    .line 514
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->initUI()V

    .line 515
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->show()V

    .line 517
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    sget-object v2, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->PIP:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onStreamerStateChanged(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 519
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mCloseStreamBtn:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 520
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mControlsView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 522
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/epg/TvStreamer$11;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer$11;-><init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 536
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->updateStreamState()V

    .line 537
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 538
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPipWidth:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 539
    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mPipHeight:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 540
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0903f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 541
    const/16 v1, 0x55

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 542
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 544
    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoRatio:F

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-direct {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->adjustVideoViewRatio(FII)V

    .line 546
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setActivityContentVisibility(Z)V

    .line 549
    return-void
.end method

.method public streamFullScreen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "lineupId"    # Ljava/lang/String;

    .prologue
    .line 354
    const-string v2, "TvStreamer"

    const-string v3, "Streaming full screen"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsArmDevice:Z

    if-nez v2, :cond_1

    .line 357
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    const v3, 0x7f070c63

    invoke-virtual {v2, v3}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showStartupError(Ljava/lang/String;)V

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mState:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v2, v3, :cond_2

    .line 361
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupFullScreenUI()V

    .line 362
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 364
    const-string v2, "TvStreamer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Changing channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->setChannel(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 369
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    if-nez v2, :cond_3

    .line 370
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .line 371
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->addListener(Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;)V

    .line 373
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mIsErrorState:Z

    .line 374
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 376
    const-string v2, "TvStreamer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Changing channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->setChannel(Ljava/lang/String;Ljava/lang/String;)Z

    .line 383
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getMediaPlayerLock()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    .line 387
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    if-nez v2, :cond_4

    .line 388
    const-string v2, "TvStreamer"

    const-string v4, "Creating media player instance"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPlayReadyFactory()Lcom/microsoft/playready/IPlayReadyFactory;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/playready/IPlayReadyFactory;->createPRMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    .line 391
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V

    .line 393
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 395
    const-string v2, "TvStreamer"

    const-string v4, "requestStreamStarted: null"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestStreamStarted(Ljava/lang/String;)Z

    .line 402
    :goto_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMediaPlayer:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V

    .line 404
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    :try_start_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupFullScreenUI()V

    .line 406
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mVideoView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mModel:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 407
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 408
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 410
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mParentView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v4}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mMainActivity:Landroid/support/v7/app/AppCompatActivity;

    invoke-virtual {v5}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 411
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer;->mContentView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V
    :try_end_2
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Lcom/microsoft/xbox/xle/epg/TvStreamerException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->showError(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 380
    .end local v0    # "e":Lcom/microsoft/xbox/xle/epg/TvStreamerException;
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestLiveTvInfo()Z
    :try_end_3
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 398
    :cond_7
    :try_start_4
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v1

    .line 399
    .local v1, "source":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    const-string v2, "TvStreamer"

    const-string v4, "requestStreamStarted: tuner"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestStreamStarted(Ljava/lang/String;)Z

    goto :goto_2

    .line 404
    .end local v1    # "source":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v2
    :try_end_5
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_5 .. :try_end_5} :catch_0
.end method
