.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Connection;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Connection"
.end annotation


# static fields
.field public static final CheckAutoConnect:Ljava/lang/String; = "Check Auto Connect"

.field public static final Connect:Ljava/lang/String; = "Connection - Connect to Xbox"

.field public static final Disconnect:Ljava/lang/String; = "Disconnect"

.field public static final Forget:Ljava/lang/String; = "Connection - Forget Xbox"

.field public static final MRU:Ljava/lang/String; = "Connection - MRU Selected"

.field public static final PowerTrash:Ljava/lang/String; = "Power Trash"

.field public static final TurnOff:Ljava/lang/String; = "Power Turn Off"

.field public static final TurnOn:Ljava/lang/String; = "Power Turn On"

.field public static final TurnOnSuccess:Ljava/lang/String; = "Power On Success"

.field public static final UncheckAutoConnect:Ljava/lang/String; = "Uncheck Auto Connect"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
