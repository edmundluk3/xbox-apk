.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "ActivityAlertViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;
    }
.end annotation


# instance fields
.field private loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

.field private loadActivityAlerts:Z

.field private needRefresh:Z

.field private orderedActivityAlertList:[Ljava/lang/Object;

.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 28
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->needRefresh:Z

    .line 37
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlerts:Z

    .line 39
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlerts:Z

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->onLoadMeComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method private getActivityAlertRootPath(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 165
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 166
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    .line 175
    :goto_0
    return-object v0

    .line 168
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    goto :goto_0

    .line 170
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    goto :goto_0

    .line 175
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getActivityFeedItemType(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 179
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 180
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 189
    :goto_0
    return-object v0

    .line 182
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    goto :goto_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onLoadMeComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 151
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->updateAdapter()V

    .line 152
    return-void

    .line 140
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlerts:Z

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCommentActivityAlertData()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->orderedActivityAlertList:[Ljava/lang/Object;

    .line 142
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->updateViewModelState()V

    goto :goto_0

    .line 146
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlerts:Z

    .line 147
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->orderedActivityAlertList:[Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->orderedActivityAlertList:[Ljava/lang/Object;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 157
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getOrderedActivityAlertList()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->orderedActivityAlertList:[Ljava/lang/Object;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->cancel()V

    .line 55
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->load(Z)V

    .line 57
    return-void
.end method

.method public navigateToActivityFeedScreen(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    const/4 v6, 0x0

    .line 67
    if-eqz p1, :cond_2

    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getActivityAlertRootPath(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "rootPath":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getActivityFeedItemType(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    .line 70
    .local v0, "feedItemActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Activity Feed Notification Item"

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    invoke-static {p0, v1, v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 73
    iget-boolean v2, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    if-eqz v2, :cond_0

    .line 74
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->needRefresh:Z

    .line 82
    .end local v0    # "feedItemActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .end local v1    # "rootPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 77
    .restart local v0    # "feedItemActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .restart local v1    # "rootPath":Ljava/lang/String;
    :cond_1
    const-string v2, "ActivityAlertViewModel"

    const-string v3, "Invalid root path or feed action type - cannot navigate to activity feed screen"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    .end local v0    # "feedItemActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .end local v1    # "rootPath":Ljava/lang/String;
    :cond_2
    const-string v2, "ActivityAlertViewModel"

    const-string v3, "Invalid data - cannot navigate to activity feed screen"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public navigateToProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 85
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 86
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 87
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 89
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 90
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 91
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToViewProfile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v4, "Activity Feed Notification Item"

    const/4 v5, 0x0

    const-string v6, "Follower"

    invoke-virtual {v1, v4, v5, v6, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 99
    iget-boolean v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    if-eqz v1, :cond_0

    .line 100
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->needRefresh:Z

    .line 109
    :cond_0
    :goto_1
    return-void

    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    move v1, v3

    .line 86
    goto :goto_0

    .line 103
    .restart local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_2
    const v1, 0x7f07061c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->showError(I)V

    goto :goto_1

    .line 106
    :cond_3
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1
.end method

.method protected onStartOverride()V
    .locals 4

    .prologue
    .line 113
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 114
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->needRefresh:Z

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)V

    const-wide/16 v2, 0xc8

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->needRefresh:Z

    .line 125
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->loadActivityAlertDataTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel$LoadActivityAlertDataAsyncTask;->cancel()V

    .line 132
    :cond_0
    return-void
.end method
