.class public Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "ClubCreationScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;
    }
.end annotation


# instance fields
.field private final adapter:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

.field private final clubCreationViewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 27
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 28
    const v0, 0x7f0e030a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->clubCreationViewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 30
    const v0, 0x7f0e030b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 51
    const v0, 0x7f0e0309

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->clubCreationViewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->exit()V

    .line 53
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->clubCreationViewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    .line 59
    .local v0, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 60
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->clubCreationViewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getCurrentPage()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->adapter:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->updateCurrentPage()V

    .line 64
    :cond_0
    return-void
.end method
