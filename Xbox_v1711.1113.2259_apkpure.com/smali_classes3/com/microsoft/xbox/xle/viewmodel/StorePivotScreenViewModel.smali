.class public Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "StorePivotScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

.field private pivotHasGamepass:Z

.field private pivotHasGold:Z

.field private userHasGamepass:Z

.field private userHasGold:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGamepass:Z

    .line 26
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGold:Z

    .line 27
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGamepass:Z

    .line 28
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGold:Z

    .line 31
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getStorePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 32
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private shouldShowGamepass()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGamepass:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->shouldShowArches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowGold()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGold:Z

    return v0
.end method

.method private updatePivots()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    .line 83
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGold()Z

    move-result v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGold:Z

    if-eq v1, v2, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGold()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->addScreenToPivot(Ljava/lang/Class;II)V

    .line 90
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGold()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGold:Z

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGamepass()Z

    move-result v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGamepass:Z

    if-eq v1, v2, :cond_2

    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGamepass()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 95
    const/4 v0, 0x1

    .line 96
    .local v0, "gamePassPivotIndex":I
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGold:Z

    if-eqz v1, :cond_1

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 100
    :cond_1
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;

    invoke-virtual {p0, v1, v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->addScreenToPivot(Ljava/lang/Class;II)V

    .line 105
    .end local v0    # "gamePassPivotIndex":I
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->shouldShowGamepass()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->pivotHasGamepass:Z

    .line 107
    :cond_2
    return-void

    .line 87
    :cond_3
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    goto :goto_0

    .line 102
    :cond_4
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    goto :goto_1
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->cancel()V

    .line 40
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->loadUserSubscriptionsTask:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$LoadUserSubscriptionsTask;->load(Z)V

    .line 42
    return-void
.end method

.method protected onLoadUserSubscriptionsCompleted(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;)V
    .locals 5
    .param p1, "result"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    .prologue
    const/4 v4, 0x1

    .line 61
    if-eqz p1, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    .line 63
    .local v0, "subscription":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$SubscriptionType:[I

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->productId:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->getSubscriptionFromAffirmationProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 65
    :pswitch_0
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGold:Z

    goto :goto_0

    .line 69
    :pswitch_1
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->userHasGamepass:Z

    goto :goto_0

    .line 77
    .end local v0    # "subscription":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->updatePivots()V

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->updateAdapter()V

    .line 80
    :cond_1
    return-void

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getStorePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 47
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method
