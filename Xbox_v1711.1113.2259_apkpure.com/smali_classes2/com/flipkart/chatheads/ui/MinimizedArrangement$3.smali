.class Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;
.super Lcom/facebook/rebound/SimpleSpringListener;
.source "MinimizedArrangement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipkart/chatheads/ui/MinimizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

.field final synthetic val$chatHead:Lcom/flipkart/chatheads/ui/ChatHead;


# direct methods
.method constructor <init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 0
    .param p1, "this$0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 126
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    iput-object p2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->val$chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-direct {p0}, Lcom/facebook/rebound/SimpleSpringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onSpringUpdate(Lcom/facebook/rebound/Spring;)V
    .locals 8
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 129
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;"
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v2}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$300(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 130
    .local v1, "index":I
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v2}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$300(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v2, v1, v2

    add-int/lit8 v0, v2, 0x1

    .line 131
    .local v0, "diff":I
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->val$chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v4

    int-to-float v3, v0

    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v6}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$000(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)F

    move-result v6

    mul-float/2addr v3, v6

    float-to-double v6, v3

    add-double/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 132
    return-void
.end method
