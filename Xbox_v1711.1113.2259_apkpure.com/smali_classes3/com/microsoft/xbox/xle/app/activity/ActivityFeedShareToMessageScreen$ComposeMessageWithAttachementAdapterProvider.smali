.class Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;
.super Ljava/lang/Object;
.source "ActivityFeedShareToMessageScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposeMessageWithAttachementAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$1;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen$ComposeMessageWithAttachementAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ActivityFeedShareToMessageScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 95
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    .line 96
    .local v0, "composeMessageViewModel":Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getComposeMessageWithAttachementAdapter(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
