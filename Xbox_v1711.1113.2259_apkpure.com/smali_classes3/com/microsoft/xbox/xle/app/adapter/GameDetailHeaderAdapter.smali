.class public Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "GameDetailHeaderAdapter.java"


# instance fields
.field private backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final darkGreyColor:I

.field private gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private gameReleaseDataText:Landroid/widget/TextView;

.field private gameSmallTitleView:Landroid/widget/TextView;

.field private gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private gameTitleView:Landroid/widget/TextView;

.field private gameXPABadging:Landroid/view/View;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    .line 35
    const v0, 0x7f0e0604

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->screenBody:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0605

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 38
    const v0, 0x7f0e0606

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameTitleView:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0e0608

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 40
    const v0, 0x7f0e0607

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameReleaseDataText:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f0e04fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 42
    const v0, 0x7f0e060a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameSmallTitleView:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0e0609

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameXPABadging:Landroid/view/View;

    .line 45
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c00c3

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->darkGreyColor:I

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    :cond_1
    return-void
.end method

.method protected updateViewOverride()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 79
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameReleaseDataText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getGameReleaseData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameSmallTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameXPABadging:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->isXPA()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getAverageUserRating()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setVisibility(I)V

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->gameTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020121

    const v4, 0x7f020123

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->backgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->darkGreyColor:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setColorFilter(I)V

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getGameItemReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 97
    .local v0, "releaseDate":Ljava/util/Date;
    if-eqz v0, :cond_3

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameDetailsProgressPhoneScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 101
    :cond_3
    return-void

    .end local v0    # "releaseDate":Ljava/util/Date;
    :cond_4
    move v1, v3

    .line 79
    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 80
    goto/16 :goto_1
.end method
