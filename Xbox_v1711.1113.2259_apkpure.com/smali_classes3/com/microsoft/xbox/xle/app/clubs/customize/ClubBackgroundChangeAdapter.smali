.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubBackgroundChangeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$AchievementViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
        "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getItemType()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 84
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;

    .line 85
    .local v0, "dataItem":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;
    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;->onBind(Ljava/lang/Object;)V

    .line 88
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 69
    packed-switch p2, :pswitch_data_0

    .line 77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown backgroundImageView type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 71
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;Landroid/view/ViewGroup;)V

    .line 79
    .local v0, "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
    :goto_0
    return-object v0

    .line 74
    .end local v0    # "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$AchievementViewHolder;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$AchievementViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;Landroid/view/ViewGroup;)V

    .line 75
    .restart local v0    # "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
