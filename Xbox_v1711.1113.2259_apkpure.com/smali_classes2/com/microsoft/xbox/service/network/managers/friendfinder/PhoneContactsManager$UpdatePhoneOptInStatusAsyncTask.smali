.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PhoneContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UpdatePhoneOptInStatusAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
    .param p2, "status"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 630
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 631
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 632
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 652
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v2, v3, :cond_0

    .line 654
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$200(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 655
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 656
    .local v1, "newSettings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;>;"
    new-instance v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;

    sget-object v3, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    sget-object v4, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;-><init>(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-direct {v3, v1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;-><init>(Ljava/util/ArrayList;)V

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setPrivacySettings(Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 658
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 679
    .end local v1    # "newSettings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;>;"
    :goto_0
    return-object v2

    .line 663
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v2, v3, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_2

    .line 664
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Phone:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z

    .line 672
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Phone:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 673
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 665
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v2, v3, :cond_1

    .line 666
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->Phone:Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;

    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 667
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 670
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 676
    :catch_0
    move-exception v0

    .line 677
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 679
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 646
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 641
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->onUpdatePhoneOptInStatusCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 642
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 692
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq v0, v1, :cond_1

    .line 697
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->status:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->onUpdatePhoneOptInStatusCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 699
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 627
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 684
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    new-instance v1, Landroid/app/Dialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$302(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0301d9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 686
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0c0145

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 687
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->this$0:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 688
    return-void
.end method
