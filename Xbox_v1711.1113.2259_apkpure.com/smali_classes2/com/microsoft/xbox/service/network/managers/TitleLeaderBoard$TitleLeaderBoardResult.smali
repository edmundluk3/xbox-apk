.class public Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
.super Ljava/lang/Object;
.source "TitleLeaderBoard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TitleLeaderBoardResult"
.end annotation


# instance fields
.field public leaderboardInfo:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$LeaderBoardInfo;

.field private me:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

.field public pageInfo:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$PageInfo;

.field public statName:Ljava/lang/String;

.field public userList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 58
    const-class v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    return-object v0
.end method


# virtual methods
.method public getMe()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->me:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    return-object v0
.end method

.method public setMe(Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V
    .locals 0
    .param p1, "me"    # Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->me:Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 63
    return-void
.end method
