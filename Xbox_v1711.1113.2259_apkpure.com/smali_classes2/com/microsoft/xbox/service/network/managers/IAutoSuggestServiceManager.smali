.class public interface abstract Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager;
.super Ljava/lang/Object;
.source "IAutoSuggestServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;,
        Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;,
        Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Results;,
        Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;
    }
.end annotation


# virtual methods
.method public abstract getAutoSuggestData(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
