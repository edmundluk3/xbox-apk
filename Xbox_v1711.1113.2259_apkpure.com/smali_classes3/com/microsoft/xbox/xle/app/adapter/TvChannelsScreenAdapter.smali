.class public Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TvChannelsScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;
.implements Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;


# instance fields
.field protected diagTag:Ljava/lang/String;

.field protected epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

.field protected errorStateProviderBar:Landroid/widget/LinearLayout;

.field protected errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected inPageProgressBar:Landroid/widget/RelativeLayout;

.field protected inPageProgressBarExpandedHeight:I

.field protected providerBar:Landroid/widget/LinearLayout;

.field protected providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final providerClickListener:Landroid/view/View$OnClickListener;

.field protected providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field protected providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 53
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBarExpandedHeight:I

    .line 197
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerClickListener:Landroid/view/View$OnClickListener;

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    .line 59
    const-string v0, "TvChannelsScreenAdapter"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->diagTag:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public getEPGView()Lcom/microsoft/xbox/xle/ui/EPGView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    return-object v0
.end method

.method public onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "newProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->updateProviderName()V

    .line 304
    return-void
.end method

.method public onChannelTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
    .locals 2
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 283
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->show(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 286
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "startChannel"    # I
    .param p3, "endChannel"    # I

    .prologue
    .line 319
    return-void
.end method

.method public onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 324
    return-void
.end method

.method public onFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 314
    return-void
.end method

.method public onProgramTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z
    .locals 3
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p2, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    .line 291
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-static {v1, v0, p2, v2}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->show(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    .line 294
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onProviderListChanged()V
    .locals 0

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->updateProviderBarState()V

    .line 309
    return-void
.end method

.method public onSetInactive()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onSetInactive()V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 90
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 96
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 97
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 101
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 103
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 104
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->getScreenString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "refresh"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->refreshViewPort(Z)V

    .line 193
    return-void
.end method

.method protected setEPGView(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V
    .locals 2
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p2, "screen"    # Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/ui/EPGView;->setScreen(Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setOnTapListener(Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 112
    .local v0, "parent":Landroid/view/ViewGroup;
    const v1, 0x7f0e057f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    .line 113
    return-void
.end method

.method protected updateLoadingIndicator(Z)V
    .locals 2
    .param p1, "isLoading"    # Z

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->isActive:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 71
    if-eqz p1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBarExpandedHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 80
    :cond_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->inPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method protected updateProviderBarState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected updateProviderName()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 226
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    if-nez v2, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 231
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 232
    .local v0, "activeProvider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getLogo()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "logo":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 238
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;)V

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 242
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 244
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    goto :goto_0

    .line 250
    .end local v0    # "activeProvider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .end local v1    # "logo":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 252
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 253
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateViewLoadingIndicator()V
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isBusy()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 186
    .local v0, "show":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 187
    return-void

    .line 185
    .end local v0    # "show":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateViewOverride()V
    .locals 6

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->updateViewLoadingIndicator()V

    .line 120
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    if-eqz v3, :cond_0

    .line 123
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$2;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 128
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getHasData()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 129
    const/4 v1, 0x0

    .line 161
    .local v1, "panelIndex":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 163
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->diagTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Switch to view "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v3, 0x3

    if-eq v1, v3, :cond_0

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "EPGLoad"

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v4, v5, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .end local v1    # "panelIndex":I
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    if-eqz v3, :cond_2

    .line 174
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    .line 176
    .local v2, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getHasData()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 177
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->setModel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    .line 182
    .end local v2    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_2
    return-void

    .line 131
    :cond_3
    const/4 v1, 0x1

    .line 134
    .restart local v1    # "panelIndex":I
    goto :goto_0

    .line 137
    .end local v1    # "panelIndex":I
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v3, v4, :cond_4

    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v3, v4, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 138
    .local v0, "disconnect":Z
    :goto_2
    if-eqz v0, :cond_6

    .line 140
    const/4 v1, 0x2

    .restart local v1    # "panelIndex":I
    goto/16 :goto_0

    .line 137
    .end local v0    # "disconnect":Z
    .end local v1    # "panelIndex":I
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 141
    .restart local v0    # "disconnect":Z
    :cond_6
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 143
    const/4 v1, 0x1

    .restart local v1    # "panelIndex":I
    goto/16 :goto_0

    .line 146
    .end local v1    # "panelIndex":I
    :cond_7
    const/4 v1, 0x4

    .line 149
    .restart local v1    # "panelIndex":I
    goto/16 :goto_0

    .line 152
    .end local v0    # "disconnect":Z
    .end local v1    # "panelIndex":I
    :pswitch_1
    const/4 v1, 0x0

    .line 154
    .restart local v1    # "panelIndex":I
    goto/16 :goto_0

    .line 157
    .end local v1    # "panelIndex":I
    :pswitch_2
    const/4 v1, 0x3

    .restart local v1    # "panelIndex":I
    goto/16 :goto_0

    .line 168
    :cond_8
    const-string v3, ""

    goto :goto_1

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
