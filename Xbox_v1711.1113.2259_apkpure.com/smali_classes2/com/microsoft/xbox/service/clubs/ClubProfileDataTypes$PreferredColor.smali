.class public final Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreferredColor"
.end annotation


# instance fields
.field private final primaryColor:Ljava/lang/String;

.field private final secondaryColor:Ljava/lang/String;

.field private final tertiaryColor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "primary"    # Ljava/lang/String;
    .param p2, "secondary"    # Ljava/lang/String;
    .param p3, "tertiary"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;->primaryColor:Ljava/lang/String;

    .line 413
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;->secondaryColor:Ljava/lang/String;

    .line 414
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;->tertiaryColor:Ljava/lang/String;

    .line 415
    return-void
.end method
