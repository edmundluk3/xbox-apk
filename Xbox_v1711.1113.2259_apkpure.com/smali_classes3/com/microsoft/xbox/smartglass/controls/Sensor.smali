.class abstract Lcom/microsoft/xbox/smartglass/controls/Sensor;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Sensor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T",
        "Listener:Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;"
    }
.end annotation


# static fields
.field private static final GetStateMethod:Ljava/lang/String; = "GetState"

.field private static final StartMethod:Ljava/lang/String; = "Start"

.field private static final StopMethod:Ljava/lang/String; = "Stop"


# instance fields
.field protected _isActive:Z

.field protected final _listener:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT",
            "Listener;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 28
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_isActive:Z

    .line 29
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->createListener()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_listener:Ljava/lang/Object;

    .line 31
    const-string v0, "Start"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->registerMethod(Ljava/lang/String;)V

    .line 32
    const-string v0, "Stop"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->registerMethod(Ljava/lang/String;)V

    .line 33
    const-string v0, "GetState"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->registerMethod(Ljava/lang/String;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected abstract createListener()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT",
            "Listener;"
        }
    .end annotation
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 42
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->stop()V

    .line 43
    return-void
.end method

.method protected getMetricMethodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    const-string v0, ""

    return-object v0
.end method

.method protected abstract getSensor()Lcom/microsoft/xbox/smartglass/Sensor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/smartglass/Sensor",
            "<TT",
            "Listener;",
            ">;"
        }
    .end annotation
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "arguments"    # Ljava/lang/String;

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->validateMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v2, "Start"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 54
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;-><init>(Ljava/lang/String;)V

    .line 55
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;
    invoke-virtual {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->start(ILcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;
    :catch_0
    move-exception v0

    .line 57
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid JSON. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 59
    .end local v0    # "ex":Lorg/json/JSONException;
    :cond_2
    const-string v2, "Stop"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 60
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->stop(I)V

    goto :goto_0

    .line 61
    :cond_3
    const-string v2, "GetState"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getCurrentState()Lorg/json/JSONObject;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V

    goto :goto_0
.end method

.method protected abstract isSupported()Z
.end method

.method public start(ILcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "parameters"    # Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;

    .prologue
    .line 67
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->isSupported()Z

    move-result v3

    if-nez v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device does not support "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_componentName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " input."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 99
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getSensor()Lcom/microsoft/xbox/smartglass/Sensor;

    move-result-object v1

    .line 74
    .local v1, "sensor":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-boolean v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sendToConsole:Z

    if-eqz v3, :cond_3

    .line 75
    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->titleId:I

    if-lez v3, :cond_1

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->titleId:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 76
    .local v2, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_1
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 77
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->failRequestValidation(I)V

    goto :goto_0

    .line 75
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v2

    goto :goto_1

    .line 81
    .restart local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :cond_2
    iput-object v2, v1, Lcom/microsoft/xbox/smartglass/Sensor;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 84
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :cond_3
    iget-boolean v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sendToCanvas:Z

    if-eqz v3, :cond_4

    .line 85
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_listener:Ljava/lang/Object;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/smartglass/Sensor;->removeListener(Ljava/lang/Object;)V

    .line 86
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_listener:Ljava/lang/Object;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/smartglass/Sensor;->addListener(Ljava/lang/Object;)V

    .line 90
    :cond_4
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getMetricMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sampleRateMilliseconds:I

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/smartglass/Sensor;->start(I)V

    .line 93
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_isActive:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v3, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to start "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_componentName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 102
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->isSupported()Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    :goto_0
    return-void

    .line 107
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getSensor()Lcom/microsoft/xbox/smartglass/Sensor;

    move-result-object v1

    .line 108
    .local v1, "sensor":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_listener:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/Sensor;->removeListener(Ljava/lang/Object;)V

    .line 109
    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/Sensor;->stop()V

    .line 110
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_isActive:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 111
    .end local v1    # "sensor":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    :catch_0
    move-exception v0

    .line 112
    .local v0, "ex":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public stop(I)V
    .locals 5
    .param p1, "requestId"    # I

    .prologue
    .line 117
    .local p0, "this":Lcom/microsoft/xbox/smartglass/controls/Sensor;, "Lcom/microsoft/xbox/smartglass/controls/Sensor<TTListener;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->isSupported()Z

    move-result v2

    if-nez v2, :cond_0

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device does not support "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_componentName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " input."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 133
    :goto_0
    return-void

    .line 123
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Sensor;->getSensor()Lcom/microsoft/xbox/smartglass/Sensor;

    move-result-object v1

    .line 124
    .local v1, "sensor":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_listener:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/Sensor;->removeListener(Ljava/lang/Object;)V

    .line 125
    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/Sensor;->stop()V

    .line 126
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_isActive:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    goto :goto_0

    .line 127
    .end local v1    # "sensor":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    :catch_0
    move-exception v0

    .line 128
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to stop "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Sensor;->_componentName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
