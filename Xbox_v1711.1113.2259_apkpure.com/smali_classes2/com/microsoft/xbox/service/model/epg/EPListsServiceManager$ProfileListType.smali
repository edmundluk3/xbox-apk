.class public final enum Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
.super Ljava/lang/Enum;
.source "EPListsServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProfileListType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_AppRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_GameRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_MovieRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_MusicRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_Pins:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_TVRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_UnifiedRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

.field public static final enum PLT_Unknown:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_Unknown:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_Pins"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_Pins:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_UnifiedRecents"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_UnifiedRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_MovieRecents"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_MovieRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_MusicRecents"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_MusicRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_GameRecents"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_GameRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_AppRecents"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_TVRecents"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_TVRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v1, "PLT_AppChannelFavorites"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    .line 34
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_Unknown:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_Pins:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_UnifiedRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_MovieRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_MusicRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_GameRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_TVRecents:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    return-object v0
.end method
