.class public final Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;
.super Ljava/lang/Object;
.source "MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lretrofit2/Retrofit;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final baseUrlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonConverterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/converter/gson/GsonConverterFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;",
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/converter/gson/GsonConverterFactory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "baseUrlProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Ljava/lang/String;>;"
    .local p3, "clientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/OkHttpClient;>;"
    .local p4, "gsonConverterFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/converter/gson/GsonConverterFactory;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .line 32
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->baseUrlProvider:Ljavax/inject/Provider;

    .line 34
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->clientProvider:Ljavax/inject/Provider;

    .line 36
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->gsonConverterFactoryProvider:Ljavax/inject/Provider;

    .line 38
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;",
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/converter/gson/GsonConverterFactory;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "baseUrlProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Ljava/lang/String;>;"
    .local p2, "clientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/OkHttpClient;>;"
    .local p3, "gsonConverterFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/converter/gson/GsonConverterFactory;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideMultiplayerSessionRetrofit(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
    .param p1, "baseUrl"    # Ljava/lang/String;
    .param p2, "client"    # Lokhttp3/OkHttpClient;
    .param p3, "gsonConverterFactory"    # Lretrofit2/converter/gson/GsonConverterFactory;

    .prologue
    .line 66
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;->provideMultiplayerSessionRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->get()Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit2/Retrofit;
    .locals 4

    .prologue
    .line 42
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->baseUrlProvider:Ljavax/inject/Provider;

    .line 44
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/OkHttpClient;

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->gsonConverterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lretrofit2/converter/gson/GsonConverterFactory;

    .line 43
    invoke-virtual {v3, v0, v1, v2}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;->provideMultiplayerSessionRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 42
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Retrofit;

    return-object v0
.end method
