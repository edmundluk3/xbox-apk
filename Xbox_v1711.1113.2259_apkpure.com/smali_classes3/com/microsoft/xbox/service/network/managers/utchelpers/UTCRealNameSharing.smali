.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;
.super Ljava/lang/Object;
.source "UTCRealNameSharing.java"


# static fields
.field private static final EVERYONE:I = 0x1

.field private static final FRIENDSOFFRIENDSSHARING:I = 0x2

.field private static final FRIENDSONLY:I = 0x2

.field private static final ONELEVELSHARING:I = 0x1

.field private static final SHAREIDENTITY:Ljava/lang/String; = "ShareIdentity"

.field private static final SHAREIDENTITYTRANSITIVELY:Ljava/lang/String; = "ShareIdentityTransitively"

.field private static final SHARING_SCOPE_KEY:Ljava/lang/String; = "realNameShareScope"

.field private static final SHARING_SETTING_KEY:Ljava/lang/String; = "realNameSharingSetting"

.field private static final SPECIFICFRIENDS:I = 0x3

.field private static final UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackCancelEdit()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$7;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 152
    return-void
.end method

.method public static trackRealNameSharingEditFriends()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 124
    return-void
.end method

.method public static trackRealNameSharingEditName()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 115
    return-void
.end method

.method public static trackRealNameSharingEditNameView()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 134
    return-void
.end method

.method public static trackRealNameSharingFriendsSelectorView()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 143
    return-void
.end method

.method public static trackRealNameSharingSaveFriends(Ljava/util/ArrayList;II)V
    .locals 1
    .param p1, "selected"    # I
    .param p2, "unselected"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "following":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;-><init>(Ljava/util/ArrayList;II)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 66
    return-void
.end method

.method public static trackRealNameSharingToggle(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "actionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p1, "shareIdentityStatus"    # Ljava/lang/String;
    .param p2, "setting"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;

    invoke-direct {v0, p1, p0, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$2;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 106
    return-void
.end method

.method public static trackSaveName()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$8;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$8;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 161
    return-void
.end method
