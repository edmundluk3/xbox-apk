.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
.super Ljava/lang/Object;
.source "ChatMessage.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
        ">;"
    }
.end annotation


# instance fields
.field public final containsLinks:Z

.field public final gamerTag:Ljava/lang/String;

.field public final messageId:J

.field public final messageText:Ljava/lang/String;

.field private volatile transient parsedMessage:Ljava/lang/CharSequence;

.field public final timeStamp:Ljava/util/Date;

.field public final type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "messageText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "containsLinks"    # Z

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 62
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    .line 68
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    .line 69
    iput-object p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    .line 70
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->containsLinks:Z

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;)V
    .locals 1
    .param p1, "header"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V
    .locals 4
    .param p1, "header"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "messageText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageType()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 35
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getSenderXuid()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getSenderXuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    .line 41
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getSenderGamerTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    .line 43
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageTime()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->convertChatTimeToDate(J)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    .line 44
    iput-object p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->containsLinks()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->containsLinks:Z

    .line 46
    return-void

    .line 38
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V
    .locals 2
    .param p1, "historyMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    iget-object v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 52
    iget-wide v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->senderXuid:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    .line 53
    iget-object v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->senderGamertag:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    .line 54
    iget-wide v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    .line 55
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->getMessageDate()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    .line 56
    iget-object v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->message:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->containsLinks()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->containsLinks:Z

    .line 58
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)I
    .locals 4
    .param p1, "another"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    iget-wide v2, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 106
    const/4 v0, -0x1

    .line 110
    :goto_0
    return v0

    .line 107
    :cond_0
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    iget-wide v2, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 108
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->compareTo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    if-ne p1, p0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    if-nez v3, :cond_2

    move v1, v2

    .line 86
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 88
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 89
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getParsedMessage()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->parsedMessage:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 75
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->parseMessage(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->parsedMessage:Ljava/lang/CharSequence;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->parsedMessage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
