.class Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCompareStatisticsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;

    .prologue
    .line 2371
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2372
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2373
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->xuid:Ljava/lang/String;

    .line 2374
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->titleId:Ljava/lang/String;

    .line 2375
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2379
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2380
    .local v6, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2382
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2383
    .local v2, "groups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2385
    .local v5, "stats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    new-instance v8, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;

    sget-object v9, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->Hero:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->titleId:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2386
    new-instance v8, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;

    sget-object v9, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->titleId:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2389
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2390
    new-instance v3, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    invoke-direct {v3, v6, v2, v5}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 2398
    .local v3, "profileStatisticsRequest":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v8

    .line 2399
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->getProfileStatisticsRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v4

    .line 2401
    .local v4, "profileStatisticsResult":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    if-eqz v4, :cond_2

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    .line 2402
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 2403
    .local v1, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    if-eqz v1, :cond_0

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    if-eqz v9, :cond_0

    .line 2404
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 2405
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    new-instance v11, Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;

    iget-object v12, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lcom/microsoft/xbox/service/model/ProfileModel$HeroStatComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    .line 2392
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v1    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    .end local v3    # "profileStatisticsRequest":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    .end local v4    # "profileStatisticsResult":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2393
    .local v7, "xuidsWithMe":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2394
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2395
    new-instance v3, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    invoke-direct {v3, v7, v2, v5}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .restart local v3    # "profileStatisticsRequest":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    goto :goto_0

    .line 2411
    .end local v7    # "xuidsWithMe":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "profileStatisticsResult":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    :cond_2
    return-object v4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2365
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2426
    const-wide/16 v0, 0xbf6

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2420
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCompareStatisticsRunner;->titleId:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$1200(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 2421
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2416
    return-void
.end method
