.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar;
.super Ljava/lang/Object;
.source "UTCHomeActionBar.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackAlertsAction(I)V
    .locals 1
    .param p0, "alertCount"    # I

    .prologue
    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$3;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 57
    return-void
.end method

.method public static trackFriendsAction()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 39
    return-void
.end method

.method public static trackLFGAction()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 70
    return-void
.end method

.method public static trackMessageAction(I)V
    .locals 1
    .param p0, "unreadMessagesCount"    # I

    .prologue
    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar$1;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 27
    return-void
.end method
