.class public Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ChooseGamerpicDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private cellMargin:I

.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private rootView:Landroid/widget/RelativeLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 43
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->cellMargin:I

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030050

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0254

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0255

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0256

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$1;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->removeLabelLeftMargin()V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->setContentView(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 40
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->layoutInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->closeGamerpicPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->navigateToUploadPicScreen()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 3

    .prologue
    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->customPicUploadEnabled()Z

    move-result v0

    .line 68
    .local v0, "showCustomButtom":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 61
    return-void
.end method
