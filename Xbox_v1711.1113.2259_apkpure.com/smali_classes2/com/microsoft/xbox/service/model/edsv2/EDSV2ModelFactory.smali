.class public final Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;
.super Ljava/lang/Object;
.source "EDSV2ModelFactory.java"


# static fields
.field private static final BIGCAT_APP_KEY:Ljava/lang/String; = "application"

.field private static final BIGCAT_CONSUMABLE_KEY:Ljava/lang/String; = "consumable"

.field private static final BIGCAT_DURABLE_KEY:Ljava/lang/String; = "durable"

.field private static final BIGCAT_GAME_KEY:Ljava/lang/String; = "game"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type should not be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static appMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    .locals 1
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 115
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 116
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    return-object v0
.end method

.method public static gameContentMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;
    .locals 2
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 92
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 95
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 96
    .local v0, "gameContentMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->parentalRatingsFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->ParentalRatings:Ljava/util/List;

    .line 98
    return-object v0
.end method

.method public static gameMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .locals 2
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 80
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 83
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 84
    .local v0, "gameMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->parentalRatingsFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    .line 86
    return-object v0
.end method

.method public static genreFromBigCatCategoryString(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
    .locals 2
    .param p0, "category"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "genreCategoryTree"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 307
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 308
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 310
    invoke-virtual {p1, p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->getLocalizedGenre(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "localizedGenre":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static genresFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Ljava/util/List;
    .locals 6
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 278
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 280
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 282
    .local v2, "genres":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    if-eqz v4, :cond_1

    .line 283
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->getLocalizedGamePropertyResponse()Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    move-result-object v3

    .line 285
    .local v3, "localizedGamePropertyResponse":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    if-eqz v3, :cond_1

    .line 286
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->getCategories()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 287
    .local v0, "category":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 288
    iget-object v5, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;->genreCategoryTree:Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->genreFromBigCatCategoryString(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;

    move-result-object v1

    .line 292
    .local v1, "genre":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
    if-eqz v1, :cond_0

    .line 293
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "genre":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
    .end local v3    # "localizedGamePropertyResponse":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    :cond_1
    return-object v2
.end method

.method public static mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 4
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 47
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v1

    .line 53
    .local v1, "productType":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory$1;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$StoreItemDetailResponse$BigCatProductType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 65
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 69
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :goto_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->genresFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Genres:Ljava/util/List;

    .line 70
    sget-object v2, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getLegacyId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->isBackCompatItem(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isBackCompatProduct:Z

    .line 71
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->name()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isBackCompatProduct:Z

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemTypeFromBigCatType(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 72
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 74
    return-object v0

    .line 55
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->gameMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    move-result-object v0

    .line 56
    .restart local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 59
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_1
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->gameContentMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    move-result-object v0

    .line 60
    .restart local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 62
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->appMediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    move-result-object v0

    .line 63
    .restart local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static mediaItemFromStoreAutoSuggestItem(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 3
    .param p0, "autoSuggestItem"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 103
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 105
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)V

    .line 107
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemTypeFromBigCatType(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 108
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->mediaItemTypeEnumValue:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 110
    return-object v0
.end method

.method public static mediaItemTypeFromBigCatType(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "productType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "isLegacyXboxProduct"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 319
    if-nez p0, :cond_1

    const-string v0, ""

    .line 322
    .local v0, "type":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 336
    :goto_2
    return-object v0

    .end local v0    # "type":Ljava/lang/String;
    :cond_1
    move-object v0, p0

    .line 319
    goto :goto_0

    .line 322
    .restart local v0    # "type":Ljava/lang/String;
    :sswitch_0
    const-string v3, "game"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "durable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "consumable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "application"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    .line 324
    :pswitch_0
    if-eqz p1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360Game:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    .line 325
    :goto_3
    goto :goto_2

    .line 324
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 327
    :pswitch_1
    if-eqz p1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    .line 328
    :goto_4
    goto :goto_2

    .line 327
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 330
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    .line 331
    goto :goto_2

    .line 333
    :pswitch_3
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 322
    :sswitch_data_0
    .sparse-switch
        -0x9eaa19d -> :sswitch_2
        0x304bf2 -> :sswitch_0
        0x5ca40550 -> :sswitch_3
        0x780a32db -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static parentalRatingFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 7
    .param p0, "contentRating"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "ratingsResponse"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 160
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 161
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 163
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->getRatingBoards()Ljava/util/Map;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->ratingSystem:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;

    .line 165
    .local v2, "ratingBoard":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;
    const/4 v0, 0x0

    .line 166
    .local v0, "contentRatingLocalizedProperty":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;
    const/4 v3, 0x0

    .line 168
    .local v3, "ratingBoardLocalizedProperty":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;
    if-eqz v2, :cond_0

    .line 169
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->ratingId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->getDefaultContentRatingLocalizedProperty(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;

    move-result-object v0

    .line 170
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;->getDefaultRatingBoardLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;

    move-result-object v3

    .line 173
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    .line 174
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;-><init>()V

    .line 175
    .local v1, "parentalRating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->ratingId:Ljava/lang/String;

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingId:Ljava/lang/String;

    .line 176
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->ratingSystem:Ljava/lang/String;

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingSystem:Ljava/lang/String;

    .line 177
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->parentalRatingLocalizedDetailFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    .line 178
    invoke-static {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->ratingDescriptorsFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDescriptors:Ljava/util/List;

    .line 179
    invoke-static {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->ratingDisclaimerFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDisclaimers:Ljava/util/List;

    .line 185
    .end local v1    # "parentalRating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    :goto_0
    return-object v1

    .line 184
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to create EDSV2ParentalRating from ContentRating: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parentalRatingLocalizedDetailFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;
    .locals 3
    .param p0, "contentRating"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "localizedProperty"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 193
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 194
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 196
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;-><init>()V

    .line 197
    .local v0, "parentalRatingLocalizedDetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;
    iget-object v2, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;->description:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->ShortName:Ljava/lang/String;

    .line 198
    iget-object v2, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;->longName:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->LongName:Ljava/lang/String;

    .line 200
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;-><init>()V

    .line 201
    .local v1, "ratingImage":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;
    iget-object v2, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;->logoUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    .line 202
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    .line 204
    return-object v0
.end method

.method public static parentalRatingsFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Ljava/util/List;
    .locals 10
    .param p0, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 125
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getMarketProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;

    move-result-object v3

    .line 127
    .local v3, "marketProperty":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;
    if-nez v3, :cond_1

    .line 128
    sget-object v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Can\'t create ParentalRating without MarketProperty: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 155
    :cond_0
    :goto_0
    return-object v5

    .line 132
    :cond_1
    sget-object v7, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->getRatings()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    move-result-object v6

    .line 133
    .local v6, "ratingsResponse":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    sget-object v7, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->getLocalizedGamePropertyResponse()Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    move-result-object v2

    .line 135
    .local v2, "localizedGamePropertyResponse":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    if-eqz v6, :cond_4

    .line 136
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;->getContentRatings()Ljava/util/List;

    move-result-object v1

    .line 137
    .local v1, "contentRatings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .local v5, "parentalRatings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;

    .line 140
    .local v0, "contentRating":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
    invoke-static {v0, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->parentalRatingFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v4

    .line 142
    .local v4, "parentalRating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    if-eqz v4, :cond_2

    .line 143
    if-eqz v2, :cond_3

    .line 144
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;->getPrioritizedCurrentRatingSystem()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingSystem:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 145
    const/4 v8, 0x0

    invoke-interface {v5, v8, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 147
    :cond_3
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 155
    .end local v0    # "contentRating":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
    .end local v1    # "contentRatings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;>;"
    .end local v4    # "parentalRating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .end local v5    # "parentalRatings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;>;"
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    goto :goto_0
.end method

.method public static ratingDescriptorFromBigCatDescriptor(Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    .locals 3
    .param p0, "descriptor"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 229
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 231
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;-><init>()V

    .line 232
    .local v0, "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;->key:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->setId(Ljava/lang/String;)V

    .line 233
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;->descriptor:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->setNonLocalizedDescriptor(Ljava/lang/String;)V

    .line 235
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;-><init>()V

    .line 236
    .local v1, "ratingImage":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;->logoUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    .line 238
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->Image:Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    .line 240
    return-object v0
.end method

.method public static ratingDescriptorsFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;)Ljava/util/List;
    .locals 6
    .param p0, "contentRating"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "localizedProperty"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->getRatingDescriptors()Ljava/util/List;

    move-result-object v1

    .line 214
    .local v1, "descriptorKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 216
    .local v3, "ratingDescriptors":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 217
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;->getDescriptor(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;

    move-result-object v0

    .line 219
    .local v0, "descriptor":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;
    if-eqz v0, :cond_0

    .line 220
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->ratingDescriptorFromBigCatDescriptor(Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 224
    .end local v0    # "descriptor":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method public static ratingDisclaimerFromBigCatDesclaimer(Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;
    .locals 2
    .param p0, "disclaimer"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 266
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 268
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;-><init>()V

    .line 269
    .local v0, "ratingDisclaimer":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;->disclaimer:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;->Text:Ljava/lang/String;

    .line 271
    return-object v0
.end method

.method public static ratingDisclaimerFromContentRating(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;)Ljava/util/List;
    .locals 6
    .param p0, "contentRating"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "localizedProperty"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 248
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 250
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;->getRatingDisclaimers()Ljava/util/List;

    move-result-object v1

    .line 251
    .local v1, "disclaimerKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 253
    .local v3, "ratingDisclaimers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 254
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;->getDisclaimer(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;

    move-result-object v0

    .line 256
    .local v0, "disclaimer":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;
    if-eqz v0, :cond_0

    .line 257
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->ratingDisclaimerFromBigCatDesclaimer(Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    .end local v0    # "disclaimer":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    return-object v3
.end method
