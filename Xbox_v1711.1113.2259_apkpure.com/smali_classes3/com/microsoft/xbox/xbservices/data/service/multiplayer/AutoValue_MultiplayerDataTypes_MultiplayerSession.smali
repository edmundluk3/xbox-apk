.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerSession;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerSession;
.source "AutoValue_MultiplayerDataTypes_MultiplayerSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerSession$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;Ljava/util/Map;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;Ljava/util/Date;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;)V
    .locals 0
    .param p1, "branch"    # Ljava/lang/String;
    .param p2, "changeNumber"    # Ljava/lang/Integer;
    .param p3, "contractVersion"    # Ljava/lang/Integer;
    .param p4, "correlationId"    # Ljava/lang/String;
    .param p5, "searchHandle"    # Ljava/lang/String;
    .param p6, "properties"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;
    .param p7, "constants"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;
    .param p9, "membersInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;
    .param p10, "startTime"    # Ljava/util/Date;
    .param p11, "roleTypes"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
    .param p12, "servers"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;",
            ">;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;",
            "Ljava/util/Date;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p8, "members":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;>;"
    invoke-direct/range {p0 .. p12}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerSession;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;Ljava/util/Map;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;Ljava/util/Date;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;)V

    .line 30
    return-void
.end method
