.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;
.super Ljava/lang/Object;
.source "UTCLauncher.java"


# static fields
.field private static final CON_TITLEID_KEY:Ljava/lang/String; = "contitleid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackPinToggleAction(JZ)V
    .locals 2
    .param p0, "titleId"    # J
    .param p2, "pinToHome"    # Z

    .prologue
    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$3;-><init>(JZ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 53
    return-void
.end method

.method public static trackPlayOnXboxAction(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
    .locals 2
    .param p0, "titleId"    # J
    .param p2, "fillState"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$4;-><init>(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 66
    return-void
.end method

.method public static trackShowDetailsAction(J)V
    .locals 2
    .param p0, "titleId"    # J

    .prologue
    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$1;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 25
    return-void
.end method

.method public static trackShowGameProfileAction(J)V
    .locals 2
    .param p0, "titleId"    # J

    .prologue
    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher$2;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 37
    return-void
.end method
