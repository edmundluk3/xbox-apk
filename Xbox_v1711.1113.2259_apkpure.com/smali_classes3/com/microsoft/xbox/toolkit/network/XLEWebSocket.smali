.class public final Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
.super Ljava/lang/Object;
.source "XLEWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;,
        Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final client:Lcom/koushikdutta/async/http/AsyncHttpClient;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final endpoint:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field private state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field private webSocket:Lcom/koushikdutta/async/http/WebSocket;

.field private final webSocketConnectCallback:Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;)V
    .locals 1
    .param p1, "endpoint"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->None:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    .line 71
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->client:Lcom/koushikdutta/async/http/AsyncHttpClient;

    .line 72
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;)Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocketConnectCallback:Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;

    .line 110
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;Ljava/lang/Exception;Lcom/koushikdutta/async/http/WebSocket;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
    .param p1, "connectedException"    # Ljava/lang/Exception;
    .param p2, "webSocket"    # Lcom/koushikdutta/async/http/WebSocket;

    .prologue
    .line 73
    if-eqz p1, :cond_0

    .line 74
    monitor-enter p0

    .line 75
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Error:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 76
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;->onConnectError(Ljava/lang/Exception;)V

    .line 109
    :goto_0
    return-void

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 81
    :cond_0
    monitor-enter p0

    .line 82
    :try_start_2
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    .line 83
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 84
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;)Lcom/koushikdutta/async/http/WebSocket$StringCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->setStringCallback(Lcom/koushikdutta/async/http/WebSocket$StringCallback;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;)Lcom/koushikdutta/async/callback/DataCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->setDataCallback(Lcom/koushikdutta/async/callback/DataCallback;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;)Lcom/koushikdutta/async/http/WebSocket$PongCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->setPongCallback(Lcom/koushikdutta/async/http/WebSocket$PongCallback;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;)Lcom/koushikdutta/async/callback/CompletedCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;->onConnected()V

    goto :goto_0

    .line 84
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
    .param p1, "emitter"    # Lcom/koushikdutta/async/DataEmitter;
    .param p2, "bb"    # Lcom/koushikdutta/async/ByteBufferList;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-virtual {p2}, Lcom/koushikdutta/async/ByteBufferList;->getAll()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;->onDataReceived(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;Ljava/lang/Exception;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
    .param p1, "closedException"    # Ljava/lang/Exception;

    .prologue
    .line 90
    if-eqz p1, :cond_0

    .line 91
    monitor-enter p0

    .line 92
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Error:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 93
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;->onCloseError(Ljava/lang/Exception;)V

    .line 105
    :goto_0
    return-void

    .line 93
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 98
    :cond_0
    monitor-enter p0

    .line 99
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Closed:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 100
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->callback:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;->onClosed()V

    goto :goto_0

    .line 100
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "close status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endpoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-eq v0, v1, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "attempt to close a websocket that is not in Open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 191
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v0}, Lcom/koushikdutta/async/http/WebSocket;->close()V

    .line 192
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->CloseSent:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized closeIfOpen()V
    .locals 2

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-ne v0, v1, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_0
    monitor-exit p0

    return-void

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "auth"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "subProtocol"    # Ljava/lang/String;
    .param p4, "timeout"    # I

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connect status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " endpoint: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " auth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " locale: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sub protocol: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/koushikdutta/async/http/AsyncHttpGet;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/koushikdutta/async/http/AsyncHttpGet;-><init>(Ljava/lang/String;)V

    .line 125
    .local v0, "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    invoke-virtual {v0, p4}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setTimeout(I)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 126
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    const-string v1, "Authorization"

    invoke-virtual {v0, v1, p1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 130
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    const-string v1, "Accept-Language"

    invoke-virtual {v0, v1, p2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->client:Lcom/koushikdutta/async/http/AsyncHttpClient;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocketConnectCallback:Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;

    invoke-virtual {v1, v0, p3, v2}, Lcom/koushikdutta/async/http/AsyncHttpClient;->websocket(Lcom/koushikdutta/async/http/AsyncHttpRequest;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;)Lcom/koushikdutta/async/future/Future;

    .line 135
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Connecting:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    monitor-exit p0

    return-void

    .line 117
    .end local v0    # "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getState()Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    .locals 1

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized ping(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ping status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endpoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-eq v0, v1, :cond_0

    .line 176
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "attempt to ping on a websocket that is not in Open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 179
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v0, p1}, Lcom/koushikdutta/async/http/WebSocket;->ping(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized sendData(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 141
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send string data status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endpoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " string data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-eq v0, v1, :cond_0

    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "attempt to send data on a websocket that is not in Open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 150
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v0, p1}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized sendData(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "data"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 154
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 156
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send byte data status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endpoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->endpoint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->state:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-eq v0, v1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "attempt to send data on a websocket that is not in Open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->send([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    monitor-exit p0

    return-void
.end method
