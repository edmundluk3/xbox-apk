.class public Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameDetailsProgressPhoneScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadProgressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

.field private titleModel:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->shouldRefresh()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    .line 245
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 10

    .prologue
    .line 279
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v7

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 280
    .local v3, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 281
    const-wide/16 v4, -0x1

    .line 283
    .local v4, "titleId":J
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getIsBundle()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 284
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 286
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->setShouldHideScreen(Z)V

    .line 301
    :cond_0
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-ltz v7, :cond_6

    .line 303
    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 305
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 306
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressLimitedTimeChallenge(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 308
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 309
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClips(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v6

    .line 311
    .local v6, "userClipRequestStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 312
    const-string v7, "GameDetailsProgressPhoneScreenViewModel"

    const-string v8, "Error loading game details game clips for the user"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v8, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 316
    .local v0, "clips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 317
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClips(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 318
    const-string v7, "GameDetailsProgressPhoneScreenViewModel"

    const-string v8, "Error loading game details game clips for the community"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .end local v0    # "clips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    .end local v6    # "userClipRequestStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_3
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 324
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    .line 329
    .end local v4    # "titleId":J
    :goto_1
    return-object v7

    .line 288
    .restart local v4    # "titleId":J
    :cond_4
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 289
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setBigCatProductId(Ljava/lang/String;)V

    .line 290
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v7, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 291
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 292
    .local v2, "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->forceLoad:Z

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 293
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 294
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v8

    long-to-int v7, v8

    int-to-long v4, v7

    goto/16 :goto_0

    .line 298
    .end local v1    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v2    # "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v4

    goto/16 :goto_0

    .end local v4    # "titleId":J
    :cond_6
    move-object v7, v3

    .line 329
    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$002(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/service/model/TitleModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 270
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 237
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 264
    return-void
.end method
