.class Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;
.super Ljava/lang/Object;
.source "JsonLaunchUriParameters.java"


# instance fields
.field public final location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field public final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 19
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "uri"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->uri:Ljava/lang/String;

    .line 20
    const-string v3, "location"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 22
    .local v2, "rawLocation":I
    :try_start_0
    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->fromInt(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v3, Lorg/json/JSONException;

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
