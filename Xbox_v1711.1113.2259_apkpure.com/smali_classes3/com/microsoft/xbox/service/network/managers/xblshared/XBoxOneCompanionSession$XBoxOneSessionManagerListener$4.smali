.class Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;
.super Ljava/lang/Object;
.source "XBoxOneCompanionSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

.field final synthetic val$finalListener:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

.field final synthetic val$finalLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field final synthetic val$mediaState:Lcom/microsoft/xbox/smartglass/MediaState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/MediaState;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$mediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$finalListener:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$finalLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 324
    .local v0, "timeConsumed":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->currentMediaStates:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$mediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    iget v2, v2, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$mediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$finalListener:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$finalLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;->val$mediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;->onMediaStateUpdated(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/smartglass/MediaState;)V

    .line 326
    if-eqz v0, :cond_0

    .line 327
    const-string v1, "XBoxOneCompanionSession"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "notify media state changed done.  Time used=%dms"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 329
    const-string v1, "XBoxOneCompanionSession"

    const-string v2, "spent too much time in onMediaStateChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    return-void
.end method
