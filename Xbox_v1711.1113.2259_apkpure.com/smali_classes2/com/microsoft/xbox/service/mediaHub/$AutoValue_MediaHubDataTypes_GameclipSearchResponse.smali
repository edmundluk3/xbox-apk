.class abstract Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;
.super Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;
.source "$AutoValue_MediaHubDataTypes_GameclipSearchResponse.java"


# instance fields
.field private final continuationToken:Ljava/lang/String;

.field private final values:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "values":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    .line 19
    return-void
.end method


# virtual methods
.method public continuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    .line 48
    .local v0, "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->continuationToken()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_4

    .line 49
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 48
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->continuationToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 49
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;
    :cond_5
    move v1, v2

    .line 51
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 56
    const/4 v0, 0x1

    .line 57
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 59
    mul-int/2addr v0, v3

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 61
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GameclipSearchResponse{continuationToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->continuationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public values()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameclipSearchResponse;->values:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
