.class public Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "MovieDetailsHeaderAdapter.java"


# instance fields
.field private movieExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private movieReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private movieRottenTomatoesRatingView:Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

.field private movieTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private movieTitleSmallTextView:Landroid/widget/TextView;

.field private movieTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    .line 30
    const v0, 0x7f0e07af

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->screenBody:Landroid/view/View;

    .line 32
    const v0, 0x7f0e07b0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 33
    const v0, 0x7f0e07b1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e07b2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 35
    const v0, 0x7f0e07b4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRottenTomatoesRatingView:Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

    .line 36
    const v0, 0x7f0e07b3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 37
    const v0, 0x7f0e07b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 38
    const v0, 0x7f0e07b6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTitleSmallTextView:Landroid/widget/TextView;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;)Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    const v3, 0x7f020183

    const/4 v2, 0x0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTitleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getMovieReleaseData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getAverageUserRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRatingBar:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setVisibility(I)V

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getRottenTomatoesReviewScore()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRottenTomatoesRatingView:Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getRottenTomatoesReviewScore()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->setRating(I)V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRottenTomatoesRatingView:Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->setVisibility(I)V

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->getMoviewExtraData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 89
    return-void

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;->movieRottenTomatoesRatingView:Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->setVisibility(I)V

    goto :goto_0
.end method
