.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullNoStatsRecentHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 534
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 535
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 11
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    const/16 v10, 0x8

    const/4 v5, 0x0

    .line 539
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 541
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    .line 543
    .local v2, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 544
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v4, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 545
    .local v3, "uri":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 546
    .local v0, "defaultRid":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v6, 0x7f0e001d

    invoke-virtual {v4, v6, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 547
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v4, v3, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 548
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 551
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v3, :cond_2

    const/4 v4, 0x4

    :goto_1
    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 553
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 555
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v6

    const-wide/32 v8, 0x162615ad

    cmp-long v4, v6, v8

    if-nez v4, :cond_3

    .line 556
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 557
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 558
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 565
    :cond_0
    :goto_2
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 566
    return-void

    .line 545
    .end local v0    # "defaultRid":I
    :cond_1
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .restart local v0    # "defaultRid":I
    :cond_2
    move v4, v5

    .line 551
    goto :goto_1

    .line 561
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 562
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 570
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;->onRecentClick(I)V

    .line 571
    return-void
.end method
