.class public Lcom/rt2zz/reactnativecontacts/ContactsManager;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "ContactsManager.java"


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/rt2zz/reactnativecontacts/ContactsManager;)Lcom/facebook/react/bridge/ReactApplicationContext;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsManager;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/rt2zz/reactnativecontacts/ContactsManager;)Lcom/facebook/react/bridge/ReactApplicationContext;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsManager;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/rt2zz/reactnativecontacts/ContactsManager;)Lcom/facebook/react/bridge/ReactApplicationContext;
    .locals 1
    .param p0, "x0"    # Lcom/rt2zz/reactnativecontacts/ContactsManager;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v0

    return-object v0
.end method

.method private getAllContacts(Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;

    .prologue
    .line 55
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsManager$1;

    invoke-direct {v0, p0, p1}, Lcom/rt2zz/reactnativecontacts/ContactsManager$1;-><init>(Lcom/rt2zz/reactnativecontacts/ContactsManager;Lcom/facebook/react/bridge/Callback;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 67
    return-void
.end method

.method private getAllContactsMatchingString(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .param p1, "searchString"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/facebook/react/bridge/Callback;

    .prologue
    .line 83
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsManager$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/rt2zz/reactnativecontacts/ContactsManager$2;-><init>(Lcom/rt2zz/reactnativecontacts/ContactsManager;Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method

.method private isPermissionGranted()Ljava/lang/String;
    .locals 3

    .prologue
    .line 366
    const-string v0, "android.permission.READ_CONTACTS"

    .line 368
    .local v0, "permission":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/react/bridge/ReactApplicationContext;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    .line 369
    .local v1, "res":I
    if-nez v1, :cond_0

    const-string v2, "authorized"

    :goto_0
    return-object v2

    :cond_0
    const-string v2, "denied"

    goto :goto_0
.end method

.method private mapStringToEmailType(Ljava/lang/String;)I
    .locals 3
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 401
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 412
    const/4 v0, 0x3

    .line 415
    .local v0, "emailType":I
    :goto_1
    return v0

    .line 401
    .end local v0    # "emailType":I
    :sswitch_0
    const-string v2, "home"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "work"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "mobile"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 403
    :pswitch_0
    const/4 v0, 0x1

    .line 404
    .restart local v0    # "emailType":I
    goto :goto_1

    .line 406
    .end local v0    # "emailType":I
    :pswitch_1
    const/4 v0, 0x2

    .line 407
    .restart local v0    # "emailType":I
    goto :goto_1

    .line 409
    .end local v0    # "emailType":I
    :pswitch_2
    const/4 v0, 0x4

    .line 410
    .restart local v0    # "emailType":I
    goto :goto_1

    .line 401
    :sswitch_data_0
    .sparse-switch
        -0x3fb56f5e -> :sswitch_2
        0x30f4df -> :sswitch_0
        0x37c711 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private mapStringToPhoneType(Ljava/lang/String;)I
    .locals 3
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 378
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 389
    const/4 v0, 0x7

    .line 392
    .local v0, "phoneType":I
    :goto_1
    return v0

    .line 378
    .end local v0    # "phoneType":I
    :sswitch_0
    const-string v2, "home"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "work"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "mobile"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 380
    :pswitch_0
    const/4 v0, 0x1

    .line 381
    .restart local v0    # "phoneType":I
    goto :goto_1

    .line 383
    .end local v0    # "phoneType":I
    :pswitch_1
    const/4 v0, 0x3

    .line 384
    .restart local v0    # "phoneType":I
    goto :goto_1

    .line 386
    .end local v0    # "phoneType":I
    :pswitch_2
    const/4 v0, 0x2

    .line 387
    .restart local v0    # "phoneType":I
    goto :goto_1

    .line 378
    :sswitch_data_0
    .sparse-switch
        -0x3fb56f5e -> :sswitch_2
        0x30f4df -> :sswitch_0
        0x37c711 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private mapStringToPostalAddressType(Ljava/lang/String;)I
    .locals 3
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 420
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 428
    const/4 v0, 0x3

    .line 431
    .local v0, "postalAddressType":I
    :goto_1
    return v0

    .line 420
    .end local v0    # "postalAddressType":I
    :sswitch_0
    const-string v2, "home"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "work"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 422
    :pswitch_0
    const/4 v0, 0x1

    .line 423
    .restart local v0    # "postalAddressType":I
    goto :goto_1

    .line 425
    .end local v0    # "postalAddressType":I
    :pswitch_1
    const/4 v0, 0x2

    .line 426
    .restart local v0    # "postalAddressType":I
    goto :goto_1

    .line 420
    :sswitch_data_0
    .sparse-switch
        0x30f4df -> :sswitch_0
        0x37c711 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addContact(Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Callback;)V
    .locals 31
    .param p1, "contact"    # Lcom/facebook/react/bridge/ReadableMap;
    .param p2, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 122
    const-string v28, "givenName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_0

    const-string v28, "givenName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 123
    .local v13, "givenName":Ljava/lang/String;
    :goto_0
    const-string v28, "middleName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1

    const-string v28, "middleName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 124
    .local v17, "middleName":Ljava/lang/String;
    :goto_1
    const-string v28, "familyName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2

    const-string v28, "familyName"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 125
    .local v12, "familyName":Ljava/lang/String;
    :goto_2
    const-string v28, "prefix"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_3

    const-string v28, "prefix"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 126
    .local v26, "prefix":Ljava/lang/String;
    :goto_3
    const-string/jumbo v28, "suffix"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_4

    const-string/jumbo v28, "suffix"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 127
    .local v27, "suffix":Ljava/lang/String;
    :goto_4
    const-string v28, "company"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_5

    const-string v28, "company"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, "company":Ljava/lang/String;
    :goto_5
    const-string v28, "jobTitle"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_6

    const-string v28, "jobTitle"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 129
    .local v15, "jobTitle":Ljava/lang/String;
    :goto_6
    const-string v28, "department"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_7

    const-string v28, "department"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 135
    .local v7, "department":Ljava/lang/String;
    :goto_7
    const-string v28, "phoneNumbers"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_8

    const-string v28, "phoneNumbers"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v22

    .line 136
    .local v22, "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    :goto_8
    const/16 v19, 0x0

    .line 137
    .local v19, "numOfPhones":I
    const/16 v23, 0x0

    .line 138
    .local v23, "phones":[Ljava/lang/String;
    const/16 v24, 0x0

    .line 139
    .local v24, "phonesLabels":[Ljava/lang/Integer;
    if-eqz v22, :cond_9

    .line 140
    invoke-interface/range {v22 .. v22}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v19

    .line 141
    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    .line 142
    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v24, v0

    .line 143
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_9
    move/from16 v0, v19

    if-ge v14, v0, :cond_9

    .line 144
    move-object/from16 v0, v22

    invoke-interface {v0, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v28

    const-string v29, "number"

    invoke-interface/range {v28 .. v29}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v23, v14

    .line 145
    move-object/from16 v0, v22

    invoke-interface {v0, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v28

    const-string v29, "label"

    invoke-interface/range {v28 .. v29}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 146
    .local v16, "label":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->mapStringToPhoneType(Ljava/lang/String;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v24, v14

    .line 143
    add-int/lit8 v14, v14, 0x1

    goto :goto_9

    .line 122
    .end local v4    # "company":Ljava/lang/String;
    .end local v7    # "department":Ljava/lang/String;
    .end local v12    # "familyName":Ljava/lang/String;
    .end local v13    # "givenName":Ljava/lang/String;
    .end local v14    # "i":I
    .end local v15    # "jobTitle":Ljava/lang/String;
    .end local v16    # "label":Ljava/lang/String;
    .end local v17    # "middleName":Ljava/lang/String;
    .end local v19    # "numOfPhones":I
    .end local v22    # "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    .end local v23    # "phones":[Ljava/lang/String;
    .end local v24    # "phonesLabels":[Ljava/lang/Integer;
    .end local v26    # "prefix":Ljava/lang/String;
    .end local v27    # "suffix":Ljava/lang/String;
    :cond_0
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 123
    .restart local v13    # "givenName":Ljava/lang/String;
    :cond_1
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 124
    .restart local v17    # "middleName":Ljava/lang/String;
    :cond_2
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 125
    .restart local v12    # "familyName":Ljava/lang/String;
    :cond_3
    const/16 v26, 0x0

    goto/16 :goto_3

    .line 126
    .restart local v26    # "prefix":Ljava/lang/String;
    :cond_4
    const/16 v27, 0x0

    goto/16 :goto_4

    .line 127
    .restart local v27    # "suffix":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 128
    .restart local v4    # "company":Ljava/lang/String;
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_6

    .line 129
    .restart local v15    # "jobTitle":Ljava/lang/String;
    :cond_7
    const/4 v7, 0x0

    goto :goto_7

    .line 135
    .restart local v7    # "department":Ljava/lang/String;
    :cond_8
    const/16 v22, 0x0

    goto :goto_8

    .line 150
    .restart local v19    # "numOfPhones":I
    .restart local v22    # "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    .restart local v23    # "phones":[Ljava/lang/String;
    .restart local v24    # "phonesLabels":[Ljava/lang/Integer;
    :cond_9
    const-string v28, "emailAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_a

    const-string v28, "emailAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v9

    .line 151
    .local v9, "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    :goto_a
    const/16 v18, 0x0

    .line 152
    .local v18, "numOfEmails":I
    const/4 v10, 0x0

    .line 153
    .local v10, "emails":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 154
    .local v11, "emailsLabels":[Ljava/lang/Integer;
    if-eqz v9, :cond_b

    .line 155
    invoke-interface {v9}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v18

    .line 156
    move/from16 v0, v18

    new-array v10, v0, [Ljava/lang/String;

    .line 157
    move/from16 v0, v18

    new-array v11, v0, [Ljava/lang/Integer;

    .line 158
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_b
    move/from16 v0, v18

    if-ge v14, v0, :cond_b

    .line 159
    invoke-interface {v9, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v28

    const-string v29, "email"

    invoke-interface/range {v28 .. v29}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v10, v14

    .line 160
    invoke-interface {v9, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v28

    const-string v29, "label"

    invoke-interface/range {v28 .. v29}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 161
    .restart local v16    # "label":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->mapStringToEmailType(Ljava/lang/String;)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v11, v14

    .line 158
    add-int/lit8 v14, v14, 0x1

    goto :goto_b

    .line 150
    .end local v9    # "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    .end local v10    # "emails":[Ljava/lang/String;
    .end local v11    # "emailsLabels":[Ljava/lang/Integer;
    .end local v14    # "i":I
    .end local v16    # "label":Ljava/lang/String;
    .end local v18    # "numOfEmails":I
    :cond_a
    const/4 v9, 0x0

    goto :goto_a

    .line 165
    .restart local v9    # "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    .restart local v10    # "emails":[Ljava/lang/String;
    .restart local v11    # "emailsLabels":[Ljava/lang/Integer;
    .restart local v18    # "numOfEmails":I
    :cond_b
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v21, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v28, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "account_type"

    const/16 v30, 0x0

    .line 168
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "account_name"

    const/16 v30, 0x0

    .line 169
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 170
    .local v20, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v28, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "raw_contact_id"

    const/16 v30, 0x0

    .line 173
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "mimetype"

    const-string/jumbo v30, "vnd.android.cursor.item/name"

    .line 174
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data2"

    .line 176
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data5"

    .line 177
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data3"

    .line 178
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data4"

    .line 179
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data6"

    .line 180
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 181
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v28, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "raw_contact_id"

    const/16 v30, 0x0

    .line 184
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "mimetype"

    const-string/jumbo v30, "vnd.android.cursor.item/organization"

    .line 185
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data1"

    .line 186
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data4"

    .line 187
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data5"

    .line 188
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 189
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    const/16 v28, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    .line 194
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_c
    move/from16 v0, v19

    if-ge v14, v0, :cond_c

    .line 195
    sget-object v28, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "raw_contact_id"

    const/16 v30, 0x0

    .line 196
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "mimetype"

    const-string/jumbo v30, "vnd.android.cursor.item/phone_v2"

    .line 197
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data1"

    aget-object v30, v23, v14

    .line 198
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data2"

    aget-object v30, v24, v14

    .line 199
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 200
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v14, v14, 0x1

    goto :goto_c

    .line 203
    :cond_c
    const/4 v14, 0x0

    :goto_d
    move/from16 v0, v18

    if-ge v14, v0, :cond_d

    .line 204
    sget-object v28, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "raw_contact_id"

    const/16 v30, 0x0

    .line 205
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "mimetype"

    const-string/jumbo v30, "vnd.android.cursor.item/email_v2"

    .line 206
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data1"

    aget-object v30, v10, v14

    .line 207
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data2"

    aget-object v30, v11, v14

    .line 208
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 209
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    add-int/lit8 v14, v14, 0x1

    goto :goto_d

    .line 212
    :cond_d
    const-string v28, "postalAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_e

    const-string v28, "postalAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v25

    .line 213
    .local v25, "postalAddresses":Lcom/facebook/react/bridge/ReadableArray;
    :goto_e
    if-eqz v25, :cond_f

    .line 214
    const/4 v14, 0x0

    :goto_f
    invoke-interface/range {v25 .. v25}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v28

    move/from16 v0, v28

    if-ge v14, v0, :cond_f

    .line 215
    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v3

    .line 217
    .local v3, "address":Lcom/facebook/react/bridge/ReadableMap;
    sget-object v28, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v28 .. v28}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "raw_contact_id"

    const/16 v30, 0x0

    .line 218
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "mimetype"

    const-string/jumbo v30, "vnd.android.cursor.item/postal-address_v2"

    .line 219
    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data2"

    const-string v30, "label"

    .line 220
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->mapStringToPostalAddressType(Ljava/lang/String;)I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data4"

    const-string/jumbo v30, "street"

    .line 221
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data7"

    const-string v30, "city"

    .line 222
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data8"

    const-string/jumbo v30, "state"

    .line 223
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data9"

    const-string v30, "postCode"

    .line 224
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v28

    const-string v29, "data10"

    const-string v30, "country"

    .line 225
    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 227
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_f

    .line 212
    .end local v3    # "address":Lcom/facebook/react/bridge/ReadableMap;
    .end local v25    # "postalAddresses":Lcom/facebook/react/bridge/ReadableArray;
    :cond_e
    const/16 v25, 0x0

    goto/16 :goto_e

    .line 231
    .restart local v25    # "postalAddresses":Lcom/facebook/react/bridge/ReadableArray;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v6

    .line 233
    .local v6, "ctx":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 234
    .local v5, "cr":Landroid/content/ContentResolver;
    const-string v28, "com.android.contacts"

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 235
    const/16 v28, 0x0

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v5    # "cr":Landroid/content/ContentResolver;
    :goto_10
    return-void

    .line 236
    :catch_0
    move-exception v8

    .line 237
    .local v8, "e":Ljava/lang/Exception;
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    goto :goto_10
.end method

.method public checkPermission(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 351
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->isPermissionGranted()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    .line 352
    return-void
.end method

.method public getAll(Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getAllContacts(Lcom/facebook/react/bridge/Callback;)V

    .line 37
    return-void
.end method

.method public getAllWithoutPhotos(Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getAllContacts(Lcom/facebook/react/bridge/Callback;)V

    .line 47
    return-void
.end method

.method public getContactsMatchingString(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1, "searchString"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getAllContactsMatchingString(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V

    .line 75
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    const-string v0, "Contacts"

    return-object v0
.end method

.method public getPhotoForId(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .param p1, "contactId"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;-><init>(Lcom/rt2zz/reactnativecontacts/ContactsManager;Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method

.method public requestPermission(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 359
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->isPermissionGranted()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    .line 360
    return-void
.end method

.method public updateContact(Lcom/facebook/react/bridge/ReadableMap;Lcom/facebook/react/bridge/Callback;)V
    .locals 36
    .param p1, "contact"    # Lcom/facebook/react/bridge/ReadableMap;
    .param p2, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 247
    const-string v31, "recordID"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_0

    const-string v31, "recordID"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 249
    .local v29, "recordID":Ljava/lang/String;
    :goto_0
    const-string v31, "givenName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_1

    const-string v31, "givenName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 250
    .local v13, "givenName":Ljava/lang/String;
    :goto_1
    const-string v31, "middleName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_2

    const-string v31, "middleName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 251
    .local v17, "middleName":Ljava/lang/String;
    :goto_2
    const-string v31, "familyName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_3

    const-string v31, "familyName"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 252
    .local v12, "familyName":Ljava/lang/String;
    :goto_3
    const-string v31, "prefix"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_4

    const-string v31, "prefix"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 253
    .local v28, "prefix":Ljava/lang/String;
    :goto_4
    const-string/jumbo v31, "suffix"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_5

    const-string/jumbo v31, "suffix"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 254
    .local v30, "suffix":Ljava/lang/String;
    :goto_5
    const-string v31, "company"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_6

    const-string v31, "company"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 255
    .local v3, "company":Ljava/lang/String;
    :goto_6
    const-string v31, "jobTitle"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_7

    const-string v31, "jobTitle"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 256
    .local v15, "jobTitle":Ljava/lang/String;
    :goto_7
    const-string v31, "department"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_8

    const-string v31, "department"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 258
    .local v6, "department":Ljava/lang/String;
    :goto_8
    const-string v31, "phoneNumbers"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_9

    const-string v31, "phoneNumbers"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v25

    .line 259
    .local v25, "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    :goto_9
    const/16 v19, 0x0

    .line 260
    .local v19, "numOfPhones":I
    const/16 v26, 0x0

    .line 261
    .local v26, "phones":[Ljava/lang/String;
    const/16 v27, 0x0

    .line 262
    .local v27, "phonesLabels":[Ljava/lang/Integer;
    if-eqz v25, :cond_a

    .line 263
    invoke-interface/range {v25 .. v25}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v19

    .line 264
    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    .line 265
    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v27, v0

    .line 266
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_a
    move/from16 v0, v19

    if-ge v14, v0, :cond_a

    .line 267
    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v23

    .line 268
    .local v23, "phoneMap":Lcom/facebook/react/bridge/ReadableMap;
    const-string v31, "number"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 269
    .local v24, "phoneNumber":Ljava/lang/String;
    const-string v31, "label"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 270
    .local v22, "phoneLabel":Ljava/lang/String;
    aput-object v24, v26, v14

    .line 271
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->mapStringToPhoneType(Ljava/lang/String;)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v27, v14

    .line 266
    add-int/lit8 v14, v14, 0x1

    goto :goto_a

    .line 247
    .end local v3    # "company":Ljava/lang/String;
    .end local v6    # "department":Ljava/lang/String;
    .end local v12    # "familyName":Ljava/lang/String;
    .end local v13    # "givenName":Ljava/lang/String;
    .end local v14    # "i":I
    .end local v15    # "jobTitle":Ljava/lang/String;
    .end local v17    # "middleName":Ljava/lang/String;
    .end local v19    # "numOfPhones":I
    .end local v22    # "phoneLabel":Ljava/lang/String;
    .end local v23    # "phoneMap":Lcom/facebook/react/bridge/ReadableMap;
    .end local v24    # "phoneNumber":Ljava/lang/String;
    .end local v25    # "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    .end local v26    # "phones":[Ljava/lang/String;
    .end local v27    # "phonesLabels":[Ljava/lang/Integer;
    .end local v28    # "prefix":Ljava/lang/String;
    .end local v29    # "recordID":Ljava/lang/String;
    .end local v30    # "suffix":Ljava/lang/String;
    :cond_0
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 249
    .restart local v29    # "recordID":Ljava/lang/String;
    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 250
    .restart local v13    # "givenName":Ljava/lang/String;
    :cond_2
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 251
    .restart local v17    # "middleName":Ljava/lang/String;
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 252
    .restart local v12    # "familyName":Ljava/lang/String;
    :cond_4
    const/16 v28, 0x0

    goto/16 :goto_4

    .line 253
    .restart local v28    # "prefix":Ljava/lang/String;
    :cond_5
    const/16 v30, 0x0

    goto/16 :goto_5

    .line 254
    .restart local v30    # "suffix":Ljava/lang/String;
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 255
    .restart local v3    # "company":Ljava/lang/String;
    :cond_7
    const/4 v15, 0x0

    goto/16 :goto_7

    .line 256
    .restart local v15    # "jobTitle":Ljava/lang/String;
    :cond_8
    const/4 v6, 0x0

    goto :goto_8

    .line 258
    .restart local v6    # "department":Ljava/lang/String;
    :cond_9
    const/16 v25, 0x0

    goto :goto_9

    .line 275
    .restart local v19    # "numOfPhones":I
    .restart local v25    # "phoneNumbers":Lcom/facebook/react/bridge/ReadableArray;
    .restart local v26    # "phones":[Ljava/lang/String;
    .restart local v27    # "phonesLabels":[Ljava/lang/Integer;
    :cond_a
    const-string v31, "emailAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_b

    const-string v31, "emailAddresses"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/ReadableMap;->getArray(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableArray;

    move-result-object v8

    .line 276
    .local v8, "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    :goto_b
    const/16 v18, 0x0

    .line 277
    .local v18, "numOfEmails":I
    const/4 v10, 0x0

    .line 278
    .local v10, "emails":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 279
    .local v11, "emailsLabels":[Ljava/lang/Integer;
    if-eqz v8, :cond_c

    .line 280
    invoke-interface {v8}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v18

    .line 281
    move/from16 v0, v18

    new-array v10, v0, [Ljava/lang/String;

    .line 282
    move/from16 v0, v18

    new-array v11, v0, [Ljava/lang/Integer;

    .line 283
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_c
    move/from16 v0, v18

    if-ge v14, v0, :cond_c

    .line 284
    invoke-interface {v8, v14}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v9

    .line 285
    .local v9, "emailMap":Lcom/facebook/react/bridge/ReadableMap;
    const-string v31, "email"

    move-object/from16 v0, v31

    invoke-interface {v9, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    aput-object v31, v10, v14

    .line 286
    const-string v31, "label"

    move-object/from16 v0, v31

    invoke-interface {v9, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 287
    .local v16, "label":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->mapStringToEmailType(Ljava/lang/String;)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v11, v14

    .line 283
    add-int/lit8 v14, v14, 0x1

    goto :goto_c

    .line 275
    .end local v8    # "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    .end local v9    # "emailMap":Lcom/facebook/react/bridge/ReadableMap;
    .end local v10    # "emails":[Ljava/lang/String;
    .end local v11    # "emailsLabels":[Ljava/lang/Integer;
    .end local v14    # "i":I
    .end local v16    # "label":Ljava/lang/String;
    .end local v18    # "numOfEmails":I
    :cond_b
    const/4 v8, 0x0

    goto :goto_b

    .line 291
    .restart local v8    # "emailAddresses":Lcom/facebook/react/bridge/ReadableArray;
    .restart local v10    # "emails":[Ljava/lang/String;
    .restart local v11    # "emailsLabels":[Ljava/lang/Integer;
    .restart local v18    # "numOfEmails":I
    :cond_c
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v21, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v31, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "contact_id=?"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    .line 294
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "account_type"

    const/16 v33, 0x0

    .line 295
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "account_name"

    const/16 v33, 0x0

    .line 296
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 297
    .local v20, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v31

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    sget-object v31, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "contact_id=?"

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    .line 300
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "mimetype"

    const-string/jumbo v33, "vnd.android.cursor.item/name"

    .line 301
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data2"

    .line 302
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data5"

    .line 303
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data3"

    .line 304
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data4"

    .line 305
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data6"

    .line 306
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 307
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v31

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    sget-object v31, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "contact_id=? AND mimetype = ?"

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    .line 310
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    const/16 v34, 0x1

    const-string/jumbo v35, "vnd.android.cursor.item/organization"

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data1"

    .line 311
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data4"

    .line 312
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data5"

    .line 313
    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 314
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v31

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    const/16 v31, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    .line 318
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_d
    move/from16 v0, v19

    if-ge v14, v0, :cond_d

    .line 319
    sget-object v31, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "contact_id=? AND mimetype = ?"

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    .line 320
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    const/16 v34, 0x1

    const-string/jumbo v35, "vnd.android.cursor.item/phone_v2"

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "mimetype"

    const-string/jumbo v33, "vnd.android.cursor.item/phone_v2"

    .line 321
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data1"

    aget-object v33, v26, v14

    .line 322
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data2"

    aget-object v33, v27, v14

    .line 323
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 324
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v31

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    add-int/lit8 v14, v14, 0x1

    goto :goto_d

    .line 327
    :cond_d
    const/4 v14, 0x0

    :goto_e
    move/from16 v0, v18

    if-ge v14, v0, :cond_e

    .line 328
    sget-object v31, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v31 .. v31}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "raw_contact_id=? AND mimetype = ?"

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    .line 329
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    const/16 v34, 0x1

    const-string/jumbo v35, "vnd.android.cursor.item/email_v2"

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "mimetype"

    const-string/jumbo v33, "vnd.android.cursor.item/email_v2"

    .line 330
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data1"

    aget-object v33, v10, v14

    .line 331
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v31

    const-string v32, "data2"

    aget-object v33, v11, v14

    .line 332
    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v20

    .line 333
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v31

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    add-int/lit8 v14, v14, 0x1

    goto :goto_e

    .line 336
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->getReactApplicationContext()Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v5

    .line 338
    .local v5, "ctx":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 339
    .local v4, "cr":Landroid/content/ContentResolver;
    const-string v31, "com.android.contacts"

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 340
    const/16 v31, 0x0

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    .end local v4    # "cr":Landroid/content/ContentResolver;
    :goto_f
    return-void

    .line 341
    :catch_0
    move-exception v7

    .line 342
    .local v7, "e":Ljava/lang/Exception;
    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    goto :goto_f
.end method
