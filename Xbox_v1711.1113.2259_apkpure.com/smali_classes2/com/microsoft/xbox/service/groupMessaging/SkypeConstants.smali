.class public Lcom/microsoft/xbox/service/groupMessaging/SkypeConstants;
.super Ljava/lang/Object;
.source "SkypeConstants.java"


# static fields
.field public static final CONVERSATION_ID_FOR_NEW_GROUP:Ljava/lang/String; = "CONVERSATION_ID_FOR_NEW_GROUP"

.field public static final CONVERSATION_MESSAGE_PAGE_SIZE:I = 0x64

.field public static final CONVERSATION_PAGE_SIZE:I = 0x19

.field public static final MAX_CONVERSATIONS_SUPPORTED:I = 0x3e8

.field public static final MAX_PAGES_TO_FETCH:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
