.class public interface abstract Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;
.super Ljava/lang/Object;
.source "IProjectSpecificDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
    }
.end annotation


# virtual methods
.method public abstract addDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .param p1    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
.end method

.method public abstract dismissAppBar()V
.end method

.method public abstract dismissBlocking()V
.end method

.method public abstract dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
.end method

.method public abstract dismissToast()V
.end method

.method public abstract dismissTopNonFatalAlert()V
.end method

.method public abstract forceDismissAlerts()V
.end method

.method public abstract forceDismissAll()V
.end method

.method public abstract getAppBarMenu()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
.end method

.method public abstract getIsBlocking()Z
.end method

.method public abstract getVisibleDialog()Landroid/app/Dialog;
.end method

.method public abstract onAppBarDismissed()V
.end method

.method public abstract onApplicationPause()V
.end method

.method public abstract onApplicationResume()V
.end method

.method public abstract onDialogStopped(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
.end method

.method public abstract removeDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .param p1    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setBlocking(ZLjava/lang/String;)V
.end method

.method public abstract setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract showAppBarMenu(Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;)V
.end method

.method public abstract showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
.end method

.method public abstract showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
.end method

.method public abstract showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
.end method

.method public abstract showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
.end method

.method public abstract showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
.end method

.method public abstract showToast(I)V
.end method

.method public abstract showToast(II)V
.end method

.method public abstract showToast(Ljava/lang/String;)V
.end method

.method public abstract showToast(Ljava/lang/String;I)V
.end method
