.class public final Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.super Ljava/lang/Object;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ErrorCodes;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ErrorResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;,
        Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
