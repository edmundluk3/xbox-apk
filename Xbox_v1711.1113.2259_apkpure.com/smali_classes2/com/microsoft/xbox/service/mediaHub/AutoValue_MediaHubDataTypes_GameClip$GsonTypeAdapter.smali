.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_GameClip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;",
        ">;"
    }
.end annotation


# instance fields
.field private final clipNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final commentCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final contentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;>;"
        }
    .end annotation
.end field

.field private final contentSegmentsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final contentStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultClipName:Ljava/lang/String;

.field private defaultCommentCount:Ljava/lang/Integer;

.field private defaultContentId:Ljava/lang/String;

.field private defaultContentLocators:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContentSegments:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContentState:Ljava/lang/String;

.field private defaultDurationInSeconds:Ljava/lang/Integer;

.field private defaultEmbeddedText:Ljava/lang/String;

.field private defaultEnforcementState:Ljava/lang/String;

.field private defaultFrameRate:Ljava/lang/Integer;

.field private defaultGreatestMomentId:Ljava/lang/String;

.field private defaultLikeCount:Ljava/lang/Integer;

.field private defaultLocalId:Ljava/lang/String;

.field private defaultOwnerXuid:Ljava/lang/Long;

.field private defaultResolutionHeight:Ljava/lang/Integer;

.field private defaultResolutionWidth:Ljava/lang/Integer;

.field private defaultSandboxId:Ljava/lang/String;

.field private defaultSessions:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;"
        }
    .end annotation
.end field

.field private defaultShareCount:Ljava/lang/Integer;

.field private defaultStats:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTitleData:Ljava/lang/String;

.field private defaultTitleId:Ljava/lang/String;

.field private defaultTitleName:Ljava/lang/String;

.field private defaultTournaments:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;"
        }
    .end annotation
.end field

.field private defaultUploadDate:Ljava/util/Date;

.field private defaultUploadDeviceType:Ljava/lang/String;

.field private defaultUploadLanguage:Ljava/lang/String;

.field private defaultUploadRegion:Ljava/lang/String;

.field private defaultUploadTitleId:Ljava/lang/Long;

.field private defaultUserCaption:Ljava/lang/String;

.field private defaultViewCount:Ljava/lang/Integer;

.field private final durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final embeddedTextAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final enforcementStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final frameRateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final greatestMomentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final likeCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final localIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final sandboxIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;>;"
        }
    .end annotation
.end field

.field private final shareCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final statsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titleDataAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;>;"
        }
    .end annotation
.end field

.field private final uploadDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadRegionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final userCaptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 65
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultClipName:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 68
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentSegments:Lcom/google/common/collect/ImmutableList;

    .line 69
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 70
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEmbeddedText:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultFrameRate:Ljava/lang/Integer;

    .line 72
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultGreatestMomentId:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 75
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 76
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 77
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 78
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 79
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 80
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 81
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDate:Ljava/util/Date;

    .line 82
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    .line 85
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    .line 86
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    .line 87
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    .line 88
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    .line 89
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    .line 90
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    .line 91
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    .line 92
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    .line 94
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    .line 95
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    .line 97
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->clipNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 98
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 99
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    .line 100
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentSegmentsAdapter:Lcom/google/gson/TypeAdapter;

    .line 101
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    .line 102
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->embeddedTextAdapter:Lcom/google/gson/TypeAdapter;

    .line 103
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->frameRateAdapter:Lcom/google/gson/TypeAdapter;

    .line 104
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->greatestMomentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 105
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 106
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 107
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    .line 108
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    .line 109
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 110
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    .line 111
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 112
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 113
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 114
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    .line 115
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    .line 116
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 117
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 118
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 119
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 120
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 121
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 122
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 123
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 124
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 125
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    .line 126
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    .line 127
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    .line 128
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;
    .locals 35
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v34, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v34

    if-ne v1, v0, :cond_0

    .line 327
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 328
    const/4 v1, 0x0

    .line 499
    :goto_0
    return-object v1

    .line 330
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultClipName:Ljava/lang/String;

    .line 332
    .local v2, "clipName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 333
    .local v3, "contentId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 334
    .local v4, "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentSegments:Lcom/google/common/collect/ImmutableList;

    .line 335
    .local v5, "contentSegments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 336
    .local v6, "durationInSeconds":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEmbeddedText:Ljava/lang/String;

    .line 337
    .local v7, "embeddedText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultFrameRate:Ljava/lang/Integer;

    .line 338
    .local v8, "frameRate":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultGreatestMomentId:Ljava/lang/String;

    .line 339
    .local v9, "greatestMomentId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 340
    .local v10, "localId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 341
    .local v11, "ownerXuid":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 342
    .local v12, "resolutionHeight":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 343
    .local v13, "resolutionWidth":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 344
    .local v14, "sandboxId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 345
    .local v15, "titleData":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 346
    .local v16, "titleId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 347
    .local v17, "titleName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDate:Ljava/util/Date;

    move-object/from16 v18, v0

    .line 348
    .local v18, "uploadDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 349
    .local v19, "uploadLanguage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 350
    .local v20, "uploadRegion":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    move-object/from16 v21, v0

    .line 351
    .local v21, "uploadTitleId":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 352
    .local v22, "uploadDeviceType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 353
    .local v23, "userCaption":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    move-object/from16 v24, v0

    .line 354
    .local v24, "commentCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    move-object/from16 v25, v0

    .line 355
    .local v25, "likeCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    move-object/from16 v26, v0

    .line 356
    .local v26, "shareCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    move-object/from16 v27, v0

    .line 357
    .local v27, "viewCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 358
    .local v28, "contentState":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 359
    .local v29, "enforcementState":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v30, v0

    .line 360
    .local v30, "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v31, v0

    .line 361
    .local v31, "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v32, v0

    .line 362
    .local v32, "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 363
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v33

    .line 364
    .local v33, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v34, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v34

    if-ne v1, v0, :cond_1

    .line 365
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 368
    :cond_1
    const/4 v1, -0x1

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->hashCode()I

    move-result v34

    sparse-switch v34, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 494
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 368
    :sswitch_0
    const-string v34, "clipName"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v34, "contentId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v34, "contentLocators"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v34, "contentSegments"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v34, "durationInSeconds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v34, "embeddedText"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v34, "frameRate"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_7
    const-string v34, "greatestMomentId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/4 v1, 0x7

    goto :goto_2

    :sswitch_8
    const-string v34, "localId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x8

    goto :goto_2

    :sswitch_9
    const-string v34, "ownerXuid"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x9

    goto :goto_2

    :sswitch_a
    const-string v34, "resolutionHeight"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xa

    goto :goto_2

    :sswitch_b
    const-string v34, "resolutionWidth"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v34, "sandboxId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v34, "titleData"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v34, "titleId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v34, "titleName"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0xf

    goto/16 :goto_2

    :sswitch_10
    const-string v34, "uploadDate"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x10

    goto/16 :goto_2

    :sswitch_11
    const-string v34, "uploadLanguage"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x11

    goto/16 :goto_2

    :sswitch_12
    const-string v34, "uploadRegion"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x12

    goto/16 :goto_2

    :sswitch_13
    const-string v34, "uploadTitleId"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x13

    goto/16 :goto_2

    :sswitch_14
    const-string v34, "uploadDeviceType"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x14

    goto/16 :goto_2

    :sswitch_15
    const-string v34, "userCaption"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x15

    goto/16 :goto_2

    :sswitch_16
    const-string v34, "commentCount"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x16

    goto/16 :goto_2

    :sswitch_17
    const-string v34, "likeCount"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x17

    goto/16 :goto_2

    :sswitch_18
    const-string v34, "shareCount"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x18

    goto/16 :goto_2

    :sswitch_19
    const-string v34, "viewCount"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x19

    goto/16 :goto_2

    :sswitch_1a
    const-string v34, "contentState"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x1a

    goto/16 :goto_2

    :sswitch_1b
    const-string v34, "enforcementState"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x1b

    goto/16 :goto_2

    :sswitch_1c
    const-string v34, "sessions"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x1c

    goto/16 :goto_2

    :sswitch_1d
    const-string v34, "stats"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x1d

    goto/16 :goto_2

    :sswitch_1e
    const-string v34, "tournaments"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    const/16 v1, 0x1e

    goto/16 :goto_2

    .line 370
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->clipNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "clipName":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 371
    .restart local v2    # "clipName":Ljava/lang/String;
    goto/16 :goto_1

    .line 374
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "contentId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 375
    .restart local v3    # "contentId":Ljava/lang/String;
    goto/16 :goto_1

    .line 378
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    check-cast v4, Lcom/google/common/collect/ImmutableList;

    .line 379
    .restart local v4    # "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    goto/16 :goto_1

    .line 382
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentSegmentsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "contentSegments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    check-cast v5, Lcom/google/common/collect/ImmutableList;

    .line 383
    .restart local v5    # "contentSegments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    goto/16 :goto_1

    .line 386
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "durationInSeconds":Ljava/lang/Integer;
    check-cast v6, Ljava/lang/Integer;

    .line 387
    .restart local v6    # "durationInSeconds":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 390
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->embeddedTextAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "embeddedText":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 391
    .restart local v7    # "embeddedText":Ljava/lang/String;
    goto/16 :goto_1

    .line 394
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->frameRateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "frameRate":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 395
    .restart local v8    # "frameRate":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 398
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->greatestMomentIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "greatestMomentId":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 399
    .restart local v9    # "greatestMomentId":Ljava/lang/String;
    goto/16 :goto_1

    .line 402
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "localId":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 403
    .restart local v10    # "localId":Ljava/lang/String;
    goto/16 :goto_1

    .line 406
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "ownerXuid":Ljava/lang/Long;
    check-cast v11, Ljava/lang/Long;

    .line 407
    .restart local v11    # "ownerXuid":Ljava/lang/Long;
    goto/16 :goto_1

    .line 410
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "resolutionHeight":Ljava/lang/Integer;
    check-cast v12, Ljava/lang/Integer;

    .line 411
    .restart local v12    # "resolutionHeight":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 414
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "resolutionWidth":Ljava/lang/Integer;
    check-cast v13, Ljava/lang/Integer;

    .line 415
    .restart local v13    # "resolutionWidth":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 418
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "sandboxId":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 419
    .restart local v14    # "sandboxId":Ljava/lang/String;
    goto/16 :goto_1

    .line 422
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "titleData":Ljava/lang/String;
    check-cast v15, Ljava/lang/String;

    .line 423
    .restart local v15    # "titleData":Ljava/lang/String;
    goto/16 :goto_1

    .line 426
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "titleId":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 427
    .restart local v16    # "titleId":Ljava/lang/String;
    goto/16 :goto_1

    .line 430
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "titleName":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 431
    .restart local v17    # "titleName":Ljava/lang/String;
    goto/16 :goto_1

    .line 434
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "uploadDate":Ljava/util/Date;
    check-cast v18, Ljava/util/Date;

    .line 435
    .restart local v18    # "uploadDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 438
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "uploadLanguage":Ljava/lang/String;
    check-cast v19, Ljava/lang/String;

    .line 439
    .restart local v19    # "uploadLanguage":Ljava/lang/String;
    goto/16 :goto_1

    .line 442
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "uploadRegion":Ljava/lang/String;
    check-cast v20, Ljava/lang/String;

    .line 443
    .restart local v20    # "uploadRegion":Ljava/lang/String;
    goto/16 :goto_1

    .line 446
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "uploadTitleId":Ljava/lang/Long;
    check-cast v21, Ljava/lang/Long;

    .line 447
    .restart local v21    # "uploadTitleId":Ljava/lang/Long;
    goto/16 :goto_1

    .line 450
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "uploadDeviceType":Ljava/lang/String;
    check-cast v22, Ljava/lang/String;

    .line 451
    .restart local v22    # "uploadDeviceType":Ljava/lang/String;
    goto/16 :goto_1

    .line 454
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v23

    .end local v23    # "userCaption":Ljava/lang/String;
    check-cast v23, Ljava/lang/String;

    .line 455
    .restart local v23    # "userCaption":Ljava/lang/String;
    goto/16 :goto_1

    .line 458
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v24

    .end local v24    # "commentCount":Ljava/lang/Integer;
    check-cast v24, Ljava/lang/Integer;

    .line 459
    .restart local v24    # "commentCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 462
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v25

    .end local v25    # "likeCount":Ljava/lang/Integer;
    check-cast v25, Ljava/lang/Integer;

    .line 463
    .restart local v25    # "likeCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 466
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v26

    .end local v26    # "shareCount":Ljava/lang/Integer;
    check-cast v26, Ljava/lang/Integer;

    .line 467
    .restart local v26    # "shareCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 470
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "viewCount":Ljava/lang/Integer;
    check-cast v27, Ljava/lang/Integer;

    .line 471
    .restart local v27    # "viewCount":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 474
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v28

    .end local v28    # "contentState":Ljava/lang/String;
    check-cast v28, Ljava/lang/String;

    .line 475
    .restart local v28    # "contentState":Ljava/lang/String;
    goto/16 :goto_1

    .line 478
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v29

    .end local v29    # "enforcementState":Ljava/lang/String;
    check-cast v29, Ljava/lang/String;

    .line 479
    .restart local v29    # "enforcementState":Ljava/lang/String;
    goto/16 :goto_1

    .line 482
    :pswitch_1c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v30

    .end local v30    # "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    check-cast v30, Lcom/google/common/collect/ImmutableList;

    .line 483
    .restart local v30    # "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    goto/16 :goto_1

    .line 486
    :pswitch_1d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v31

    .end local v31    # "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    check-cast v31, Lcom/google/common/collect/ImmutableList;

    .line 487
    .restart local v31    # "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    goto/16 :goto_1

    .line 490
    :pswitch_1e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v32

    .end local v32    # "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    check-cast v32, Lcom/google/common/collect/ImmutableList;

    .line 491
    .restart local v32    # "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    goto/16 :goto_1

    .line 498
    .end local v33    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 499
    new-instance v1, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip;

    invoke-direct/range {v1 .. v32}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 368
    :sswitch_data_0
    .sparse-switch
        -0x7ff88b87 -> :sswitch_11
        -0x7f5465de -> :sswitch_d
        -0x7f4fdafd -> :sswitch_f
        -0x73f9af86 -> :sswitch_b
        -0x6c1edb90 -> :sswitch_18
        -0x672b23ab -> :sswitch_12
        -0x5f4efa96 -> :sswitch_19
        -0x4deb0a6d -> :sswitch_e
        -0x485d759a -> :sswitch_4
        -0x309aa8c7 -> :sswitch_3
        -0x30571c56 -> :sswitch_1e
        -0x2aeedc6f -> :sswitch_14
        -0x250b3b0d -> :sswitch_a
        -0x1843fc8c -> :sswitch_1
        -0xe8476b1 -> :sswitch_10
        -0xce113ae -> :sswitch_13
        -0xb4d2848 -> :sswitch_17
        0x10f262b -> :sswitch_9
        0x1c5c636 -> :sswitch_2
        0x68ac49f -> :sswitch_1d
        0x92a2831 -> :sswitch_1b
        0xef17b9b -> :sswitch_15
        0x142bb9e6 -> :sswitch_8
        0x207cebed -> :sswitch_6
        0x2ddf95f0 -> :sswitch_16
        0x30eb8398 -> :sswitch_1a
        0x36b169fb -> :sswitch_0
        0x53bfd09d -> :sswitch_1c
        0x56969a34 -> :sswitch_7
        0x617a7ed7 -> :sswitch_5
        0x623b3242 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultClipName(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClipName"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultClipName:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public setDefaultCommentCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCommentCount"    # Ljava/lang/Integer;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultCommentCount:Ljava/lang/Integer;

    .line 219
    return-object p0
.end method

.method public setDefaultContentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentId"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 135
    return-object p0
.end method

.method public setDefaultContentLocators(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "defaultContentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentLocators:Lcom/google/common/collect/ImmutableList;

    .line 139
    return-object p0
.end method

.method public setDefaultContentSegments(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "defaultContentSegments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentSegments:Lcom/google/common/collect/ImmutableList;

    .line 143
    return-object p0
.end method

.method public setDefaultContentState(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentState"    # Ljava/lang/String;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultContentState:Ljava/lang/String;

    .line 235
    return-object p0
.end method

.method public setDefaultDurationInSeconds(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDurationInSeconds"    # Ljava/lang/Integer;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultDurationInSeconds:Ljava/lang/Integer;

    .line 147
    return-object p0
.end method

.method public setDefaultEmbeddedText(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultEmbeddedText"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEmbeddedText:Ljava/lang/String;

    .line 151
    return-object p0
.end method

.method public setDefaultEnforcementState(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultEnforcementState"    # Ljava/lang/String;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultEnforcementState:Ljava/lang/String;

    .line 239
    return-object p0
.end method

.method public setDefaultFrameRate(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFrameRate"    # Ljava/lang/Integer;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultFrameRate:Ljava/lang/Integer;

    .line 155
    return-object p0
.end method

.method public setDefaultGreatestMomentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGreatestMomentId"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultGreatestMomentId:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public setDefaultLikeCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLikeCount"    # Ljava/lang/Integer;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLikeCount:Ljava/lang/Integer;

    .line 223
    return-object p0
.end method

.method public setDefaultLocalId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLocalId"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultLocalId:Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public setDefaultOwnerXuid(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwnerXuid"    # Ljava/lang/Long;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/Long;

    .line 167
    return-object p0
.end method

.method public setDefaultResolutionHeight(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResolutionHeight"    # Ljava/lang/Integer;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionHeight:Ljava/lang/Integer;

    .line 171
    return-object p0
.end method

.method public setDefaultResolutionWidth(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResolutionWidth"    # Ljava/lang/Integer;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultResolutionWidth:Ljava/lang/Integer;

    .line 175
    return-object p0
.end method

.method public setDefaultSandboxId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSandboxId"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSandboxId:Ljava/lang/String;

    .line 179
    return-object p0
.end method

.method public setDefaultSessions(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "defaultSessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultSessions:Lcom/google/common/collect/ImmutableList;

    .line 243
    return-object p0
.end method

.method public setDefaultShareCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShareCount"    # Ljava/lang/Integer;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultShareCount:Ljava/lang/Integer;

    .line 227
    return-object p0
.end method

.method public setDefaultStats(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "defaultStats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultStats:Lcom/google/common/collect/ImmutableList;

    .line 247
    return-object p0
.end method

.method public setDefaultTitleData(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleData"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleData:Ljava/lang/String;

    .line 183
    return-object p0
.end method

.method public setDefaultTitleId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleId"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method public setDefaultTitleName(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleName"    # Ljava/lang/String;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 191
    return-object p0
.end method

.method public setDefaultTournaments(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 250
    .local p1, "defaultTournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultTournaments:Lcom/google/common/collect/ImmutableList;

    .line 251
    return-object p0
.end method

.method public setDefaultUploadDate(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadDate"    # Ljava/util/Date;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDate:Ljava/util/Date;

    .line 195
    return-object p0
.end method

.method public setDefaultUploadDeviceType(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadDeviceType"    # Ljava/lang/String;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadDeviceType:Ljava/lang/String;

    .line 211
    return-object p0
.end method

.method public setDefaultUploadLanguage(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadLanguage"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadLanguage:Ljava/lang/String;

    .line 199
    return-object p0
.end method

.method public setDefaultUploadRegion(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadRegion"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadRegion:Ljava/lang/String;

    .line 203
    return-object p0
.end method

.method public setDefaultUploadTitleId(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUploadTitleId"    # Ljava/lang/Long;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUploadTitleId:Ljava/lang/Long;

    .line 207
    return-object p0
.end method

.method public setDefaultUserCaption(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserCaption"    # Ljava/lang/String;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultUserCaption:Ljava/lang/String;

    .line 215
    return-object p0
.end method

.method public setDefaultViewCount(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultViewCount"    # Ljava/lang/Integer;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->defaultViewCount:Ljava/lang/Integer;

    .line 231
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    if-nez p2, :cond_0

    .line 256
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 323
    :goto_0
    return-void

    .line 259
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 260
    const-string v0, "clipName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->clipNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->clipName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 262
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 264
    const-string v0, "contentLocators"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentLocatorsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 266
    const-string v0, "contentSegments"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentSegmentsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentSegments()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 268
    const-string v0, "durationInSeconds"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->durationInSecondsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->durationInSeconds()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 270
    const-string v0, "embeddedText"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->embeddedTextAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->embeddedText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 272
    const-string v0, "frameRate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->frameRateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->frameRate()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 274
    const-string v0, "greatestMomentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->greatestMomentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->greatestMomentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 276
    const-string v0, "localId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->localIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->localId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 278
    const-string v0, "ownerXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 279
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->ownerXuid()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 280
    const-string v0, "resolutionHeight"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionHeightAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->resolutionHeight()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 282
    const-string v0, "resolutionWidth"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->resolutionWidthAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->resolutionWidth()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 284
    const-string v0, "sandboxId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sandboxIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->sandboxId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 286
    const-string v0, "titleData"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->titleData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 288
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 290
    const-string v0, "titleName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->titleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 292
    const-string v0, "uploadDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 294
    const-string v0, "uploadLanguage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadLanguageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 296
    const-string v0, "uploadRegion"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadRegionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 298
    const-string v0, "uploadTitleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadTitleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadTitleId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 300
    const-string v0, "uploadDeviceType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 301
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->uploadDeviceTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 302
    const-string v0, "userCaption"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->userCaptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->userCaption()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 304
    const-string v0, "commentCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->commentCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->commentCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 306
    const-string v0, "likeCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->likeCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->likeCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 308
    const-string v0, "shareCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 309
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->shareCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->shareCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 310
    const-string v0, "viewCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->viewCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->viewCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 312
    const-string v0, "contentState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->contentStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 314
    const-string v0, "enforcementState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->enforcementStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->enforcementState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 316
    const-string v0, "sessions"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->sessionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->sessions()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 318
    const-string v0, "stats"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->statsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->stats()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 320
    const-string v0, "tournaments"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->tournamentsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->tournaments()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 322
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;)V

    return-void
.end method
