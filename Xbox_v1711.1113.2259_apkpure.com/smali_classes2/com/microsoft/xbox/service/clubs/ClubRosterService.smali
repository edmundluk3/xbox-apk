.class public final enum Lcom/microsoft/xbox/service/clubs/ClubRosterService;
.super Ljava/lang/Enum;
.source "ClubRosterService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubRosterService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubRosterService;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubRosterService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubRosterService;

.field private static final CLUB_ROSTER_ADD_ROLE_ENDPOINT:Ljava/lang/String; = "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)/roles"

.field private static final CLUB_ROSTER_BATCH_RELATIONSHIP_ENDPOINT:Ljava/lang/String; = "https://clubroster.xboxlive.com/clubs/%d/users"

.field private static final CLUB_ROSTER_REMOVE_ROLE_ENDPOINT:Ljava/lang/String; = "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)/roles/%s"

.field private static final CLUB_ROSTER_SINGLE_RELATIONSHIP_ENDPOINT:Ljava/lang/String; = "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)"

.field private static final CONTRACT_VERSION:I = 0x1

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubRosterService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubRosterService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    .line 30
    new-array v0, v3, [Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    .line 34
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    .line 47
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 50
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic lambda$addMemberToRoster$0(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    return-object v0
.end method

.method static synthetic lambda$addMemberToRoster$1(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    const-string v1, ""

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$addMembersToRoster$2(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$addRoleToMember$4(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$removeMembersFromRoster$3(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$removeRoleFromMember$5(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubRosterService;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubRosterService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubRosterService;

    return-object v0
.end method


# virtual methods
.method public addMemberToRoster(JJ)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 9
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "userId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x1

    .line 58
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    const-string v3, "addMemberToRoster"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   - (clubId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   - (userId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 63
    invoke-static {v6, v7, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 64
    invoke-static {v6, v7, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v0

    .line 67
    .local v0, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$2;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v2, v0, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    return-object v2

    :array_0
    .array-data 4
        0xc8
        0xc9
        0x190
    .end array-data
.end method

.method public addMembersToRoster(JLjava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    .locals 9
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x14L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p3, "userIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v0, 0x1

    .line 81
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    const-string v3, "addMembersToRoster"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 84
    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 85
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 86
    const-wide/16 v2, 0x14

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 88
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (clubId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (userIds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-static {v2, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubroster.xboxlive.com/clubs/%d/users"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 92
    .local v7, "url":Ljava/lang/String;
    invoke-static {p3}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;->put(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 94
    .local v6, "requestBody":Ljava/lang/String;
    invoke-static {v7, v6}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    return-object v0
.end method

.method public addRoleToMember(JJLcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 7
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "userId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5, "role"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 130
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    const-string v3, "addRoleToMember"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 133
    invoke-static {v4, v5, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 134
    invoke-static {v4, v5, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 135
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 137
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   - (clubId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   - (userId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   - (role: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)/roles"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "url":Ljava/lang/String;
    invoke-static {p5}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;->with(Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "requestBody":Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$5;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    return-object v2
.end method

.method public removeMembersFromRoster(JLjava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    .locals 9
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x14L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p3, "userIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v0, 0x1

    .line 105
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    const-string v3, "removeMembersFromRoster"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 108
    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 109
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 110
    const-wide/16 v2, 0x14

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 112
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (clubId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (userId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-static {v2, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubroster.xboxlive.com/clubs/%d/users"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 116
    .local v7, "url":Ljava/lang/String;
    invoke-static {p3}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;->delete(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 118
    .local v6, "requestBody":Ljava/lang/String;
    invoke-static {v7, v6}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    return-object v0
.end method

.method public removeRoleFromMember(JJLcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 7
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "userId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5, "role"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 156
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    const-string v2, "removeRoleFromMember"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 159
    invoke-static {v4, v5, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 160
    invoke-static {v4, v5, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 161
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 163
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - (clubId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - (userId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubRosterService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - (role: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://clubroster.xboxlive.com/clubs/%d/users/xuid(%d)/roles/%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p5}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterService$$Lambda$6;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    return-object v1
.end method
