.class public final Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;
.super Ljava/lang/Object;
.source "HoverChatModule_ProvideChatHeadViewAdapterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
        ">;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final circularDrawableFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

.field private final repositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p3, "circularDrawableFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;>;"
    .local p4, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .line 34
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->contextProvider:Ljavax/inject/Provider;

    .line 36
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->circularDrawableFactoryProvider:Ljavax/inject/Provider;

    .line 38
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->repositoryProvider:Ljavax/inject/Provider;

    .line 40
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p2, "circularDrawableFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;>;"
    .local p3, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideChatHeadViewAdapter(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "circularDrawableFactory"    # Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .param p3, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ")",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideChatHeadViewAdapter(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->contextProvider:Ljavax/inject/Provider;

    .line 46
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->circularDrawableFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->repositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 45
    invoke-virtual {v3, v0, v1, v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideChatHeadViewAdapter(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 44
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->get()Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    move-result-object v0

    return-object v0
.end method
