.class public final enum Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;
.super Ljava/lang/Enum;
.source "TitleHubServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/titleHub/ITitleHubService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/titleHub/ITitleHubService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

.field private static final MINI_CATALOG_FILE:Ljava/lang/String; = "stubdata/TitleHubMiniCatalog.json"

.field private static final RECENT_TITLES_FILE:Ljava/lang/String; = "stubdata/TitleHubRecentTitles.json"

.field private static final TAG:Ljava/lang/String;

.field private static final TITLE_HUB_BATCH_FILE:Ljava/lang/String; = "stubdata/TitleHubBatchTitles.json"

.field private static final TITLE_HUB_SESSION_FILE:Ljava/lang/String; = "stubdata/TitleHubSingleTitle.json"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    .line 26
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;

    return-object v0
.end method


# virtual methods
.method public getAllRecentlyPlayedTitles(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 80
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v4, "stubdata/TitleHubRecentTitles.json"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 82
    .local v1, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 89
    .end local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :cond_0
    :goto_0
    return-object v2

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "ex":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getMiniCatalog()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 117
    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubServiceStub;->TAG:Ljava/lang/String;

    const-string v2, "getMiniCatalog"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 121
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TitleHubMiniCatalog.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-object v1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRecentlyPlayedTitles(Ljava/lang/String;I)Ljava/util/List;
    .locals 8
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 97
    const-wide/16 v4, 0x1

    int-to-long v6, p2

    invoke-static {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 101
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v4, "stubdata/TitleHubRecentTitles.json"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 103
    .local v1, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 104
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 110
    .end local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :cond_0
    :goto_0
    return-object v2

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "ex":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getTitleSummaries(Ljava/util/Set;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .line 56
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 60
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v4, "stubdata/TitleHubBatchTitles.json"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 62
    .local v1, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 63
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 69
    .end local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :cond_0
    :goto_0
    return-object v2

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "ex":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getTitleSummary(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 7
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 36
    const-wide/16 v4, 0x1

    invoke-static {v4, v5, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 37
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 40
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v4, "stubdata/TitleHubSingleTitle.json"

    invoke-virtual {v2, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const-class v4, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 42
    .local v1, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .end local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :goto_0
    return-object v2

    .restart local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :cond_0
    move-object v2, v3

    .line 46
    goto :goto_0

    .line 48
    .end local v1    # "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    :catch_0
    move-exception v0

    .local v0, "ex":Ljava/io/IOException;
    move-object v2, v3

    .line 49
    goto :goto_0
.end method
