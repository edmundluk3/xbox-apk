.class public Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;
.super Ljava/lang/Object;
.source "EpgConnectorFragmentManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EpgConnectorFragmentManager"


# instance fields
.field private final FragmentTimeToLiveInSeconds:J

.field private messages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-wide/16 v0, 0x3c

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->FragmentTimeToLiveInSeconds:J

    .line 20
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    .line 21
    return-void
.end method

.method private RemoveDatagramIdAndStopTimer(I)V
    .locals 1
    .param p1, "datagramId"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->getDatagramTimer()Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;
    .param p1, "x1"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->RemoveDatagramIdAndStopTimer(I)V

    return-void
.end method


# virtual methods
.method public TryGetDatagram(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;)Ljava/lang/String;
    .locals 10
    .param p1, "fragmentMessage"    # Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;

    .prologue
    .line 30
    if-nez p1, :cond_0

    .line 31
    new-instance v5, Ljava/lang/NullPointerException;

    const-string v6, "fragmentMessage"

    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 36
    .local v0, "assembledDatagram":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getDatagramId()I

    move-result v1

    .line 37
    .local v1, "datagramId":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 40
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 41
    .local v3, "fragmentManagerWeak":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;>;"
    new-instance v4, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getDatagramId()I

    move-result v5

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getDatagramSize()I

    move-result v6

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;-><init>(II)V

    .line 42
    .local v4, "newHolder":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->getDatagramTimer()Ljava/util/Timer;

    move-result-object v5

    new-instance v6, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    invoke-direct {v6, p0, v3, v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;-><init>(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;Ljava/lang/ref/WeakReference;I)V

    const-wide/32 v8, 0xea60

    invoke-virtual {v5, v6, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 43
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v5, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 46
    .end local v3    # "fragmentManagerWeak":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;>;"
    .end local v4    # "newHolder":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->messages:Landroid/util/SparseArray;

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;

    .line 47
    .local v2, "fragmentHolder":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->getDatagramSize()I

    move-result v5

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getDatagramSize()I

    move-result v6

    if-ne v5, v6, :cond_3

    .line 48
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getFragmentOffset()I

    move-result v5

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getFragmentLength()I

    move-result v6

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getFragmentData()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v5, v6, v7}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->AddBytes(IILjava/lang/String;)V

    .line 49
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->GetDataIfReady()Ljava/lang/String;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_2

    .line 51
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->RemoveDatagramIdAndStopTimer(I)V

    .line 58
    :cond_2
    :goto_0
    return-object v0

    .line 54
    :cond_3
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 55
    const-string v5, "EpgConnectorFragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Datagram size mismatch: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getDatagramId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
