.class public final Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;
.super Ljava/lang/Object;
.source "TutorialInteractor.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)V
    .locals 1
    .param p1, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 41
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p3, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 25
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 26
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "facebookCallbackResult"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p3, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getUserAuthEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    invoke-interface {p2}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->getStates()Lio/reactivex/Observable;

    move-result-object v1

    .line 33
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 34
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 27
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 27
    return-object v0
.end method


# virtual methods
.method public initialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
