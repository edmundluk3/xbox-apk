.class Lcom/microsoft/xbox/smartglass/controls/HttpClient;
.super Ljava/lang/Object;
.source "HttpClient.java"


# static fields
.field private static final CONNECTION_PER_ROUTE:I = 0x10

.field private static final DEFAULT_TIMEOUT_IN_MILLISECONDS:I = 0x9c40

.field private static final MAX_TOTAL_CONNECTIONS:I = 0x20

.field private static _params:Lorg/apache/http/params/HttpParams;


# instance fields
.field private _client:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private _response:Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;


# direct methods
.method private constructor <init>(I)V
    .locals 2
    .param p1, "timeout"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 53
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 54
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    invoke-direct {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 55
    return-void
.end method

.method public static create()Lcom/microsoft/xbox/smartglass/controls/HttpClient;
    .locals 1

    .prologue
    .line 40
    const v0, 0x9c40

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->create(I)Lcom/microsoft/xbox/smartglass/controls/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method public static create(I)Lcom/microsoft/xbox/smartglass/controls/HttpClient;
    .locals 1
    .param p0, "timeout"    # I

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    if-nez v0, :cond_0

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->initializeFactory()V

    .line 48
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;-><init>(I)V

    return-object v0
.end method

.method public static getHttpMethod(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/StringEntity;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p0, "verb"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/entity/StringEntity;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 98
    const-string v2, "POST"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v0, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 113
    .end local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    :goto_0
    return-object v0

    .line 102
    :cond_0
    const-string v2, "GET"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_1
    const-string v2, "PUT"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105
    new-instance v1, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 106
    .local v1, "put":Lorg/apache/http/client/methods/HttpPut;
    invoke-virtual {v1, p2}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object v0, v1

    .line 107
    goto :goto_0

    .line 108
    .end local v1    # "put":Lorg/apache/http/client/methods/HttpPut;
    :cond_2
    const-string v2, "DELETE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_3
    const-string v2, "HEAD"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 111
    new-instance v0, Lorg/apache/http/client/methods/HttpHead;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initializeFactory()V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    .line 61
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    sget-object v1, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 62
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setUseExpectContinue(Lorg/apache/http/params/HttpParams;Z)V

    .line 64
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 70
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 71
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    new-instance v1, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 72
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_params:Lorg/apache/http/params/HttpParams;

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 73
    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;
    .locals 2
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v1, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;-><init>(Lorg/apache/http/HttpResponse;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_response:Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_response:Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;

    return-object v0
.end method

.method public getCookieStore()Lorg/apache/http/client/CookieStore;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getCookieStore()Lorg/apache/http/client/CookieStore;

    move-result-object v0

    return-object v0
.end method

.method public getLastResponse()Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_response:Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;

    return-object v0
.end method

.method public getParams()Lorg/apache/http/params/HttpParams;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    return-object v0
.end method

.method public setCookieStore(Lorg/apache/http/client/CookieStore;)V
    .locals 1
    .param p1, "cookieStore"    # Lorg/apache/http/client/CookieStore;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->_client:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    .line 94
    return-void
.end method
