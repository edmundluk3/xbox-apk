.class final enum Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;
.super Ljava/lang/Enum;
.source "TestInterop.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xle/test/interop/TestInterop;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TestInteropState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

.field public static final enum DISABLED:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

.field public static final enum READY:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

.field public static final enum WAITING:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->DISABLED:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    new-instance v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->WAITING:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    new-instance v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    const-string v1, "READY"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->READY:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->DISABLED:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->WAITING:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->READY:Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->$VALUES:[Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->$VALUES:[Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    invoke-virtual {v0}, [Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xle/test/interop/TestInterop$TestInteropState;

    return-object v0
.end method
