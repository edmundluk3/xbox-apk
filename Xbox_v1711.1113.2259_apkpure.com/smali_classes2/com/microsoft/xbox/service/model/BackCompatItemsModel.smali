.class public Lcom/microsoft/xbox/service/model/BackCompatItemsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "BackCompatItemsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;,
        Lcom/microsoft/xbox/service/model/BackCompatItemsModel$BackCompatItemsModelContainer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private backCompatItemMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private loaded:Z

.field private runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loaded:Z

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/BackCompatItemsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/BackCompatItemsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->onGetBackCompatItemsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private addItems(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 88
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/BackCompatItemsModel;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel$BackCompatItemsModelContainer;->access$000()Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    move-result-object v0

    return-object v0
.end method

.method private onGetBackCompatItemsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;>;"
    const/4 v3, 0x1

    .line 94
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->TAG:Ljava/lang/String;

    const-string v1, "onGetBackCompatItemsComplete"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->getBackCompatItems()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->addItems(Ljava/util/Map;)V

    .line 102
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loaded:Z

    .line 108
    :goto_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->BackCompatItems:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 109
    return-void

    .line 105
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->TAG:Ljava/lang/String;

    const-string v1, "Cannot get back compat data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getPurchaseLegacyId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "legacyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 59
    const-string v1, "BackCompatItemsModel is not loaded"

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loaded:Z

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "id":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 67
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public isBackCompatItem(Ljava/lang/String;)Z
    .locals 2
    .param p1, "legacyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 73
    const-string v0, "BackCompatItemsModel is not loaded"

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loaded:Z

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->backCompatItemMap:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loaded:Z

    return v0
.end method

.method public loadBackCompatItemsAsync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->isLoading:Z

    if-nez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;-><init>(Lcom/microsoft/xbox/service/model/BackCompatItemsModel;Lcom/microsoft/xbox/service/model/BackCompatItemsModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    .line 44
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->BackCompatItems:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 46
    :cond_1
    return-void
.end method

.method public loadBackCompatItemsSync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;-><init>(Lcom/microsoft/xbox/service/model/BackCompatItemsModel;Lcom/microsoft/xbox/service/model/BackCompatItemsModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->runner:Lcom/microsoft/xbox/service/model/BackCompatItemsModel$GetBackCompatItemsRunner;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 54
    return-void
.end method
