.class public Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ConsoleConnectionScreenAdapter.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private autoConnectCheckBox:Landroid/widget/CheckBox;

.field private connectButton:Landroid/widget/RelativeLayout;

.field private dialog:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

.field private disconnectButton:Landroid/widget/RelativeLayout;

.field private dropdownButtonClickable:Landroid/widget/RelativeLayout;

.field private dropdownButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private dropdownView:Landroid/view/View;

.field private forgetConsoleButton:Landroid/widget/RelativeLayout;

.field private oobeEntryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private screenTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private slideIn:Landroid/view/animation/Animation;

.field private slideOut:Landroid/view/animation/Animation;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private turnOffButton:Landroid/widget/RelativeLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 59
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)V

    .line 61
    const v0, 0x7f0e0442

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->screenBody:Landroid/view/View;

    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    .line 63
    const v0, 0x7f0e0443

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 64
    const v0, 0x7f0e043d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->connectButton:Landroid/widget/RelativeLayout;

    .line 65
    const v0, 0x7f0e0441

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 66
    const v0, 0x7f0e0440

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 67
    const v0, 0x7f0e043f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonClickable:Landroid/widget/RelativeLayout;

    .line 68
    const v0, 0x7f0e0447

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    .line 69
    const v0, 0x7f0e044a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    .line 70
    const v0, 0x7f0e0448

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->disconnectButton:Landroid/widget/RelativeLayout;

    .line 71
    const v0, 0x7f0e0449

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->turnOffButton:Landroid/widget/RelativeLayout;

    .line 72
    const v0, 0x7f0e044b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->forgetConsoleButton:Landroid/widget/RelativeLayout;

    .line 73
    const v0, 0x7f0e044d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->oobeEntryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 74
    const v0, 0x7f0e044c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->screenTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f04000e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideIn:Landroid/view/animation/Animation;

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f04000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideOut:Landroid/view/animation/Animation;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    .line 82
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v1

    add-int/lit8 v1, v1, 0x12

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    .line 83
    invoke-virtual {v2}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    .line 84
    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    .line 85
    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v4

    .line 81
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 86
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Lcom/microsoft/xbox/xle/app/SmartglassSettings;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "settings"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->tutorialExperienceEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Ljava/lang/Boolean;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "tutorialScreenAllowed"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->screenTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070379

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->oobeEntryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackConnectToXbox()V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connect()V

    .line 109
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070ef3

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070ef6

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 122
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 124
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->disconnectConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 128
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 129
    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 133
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 134
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackTurnOff()V

    .line 137
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->powerOff(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 145
    :cond_0
    return-void

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    const v3, 0x7f070362

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 140
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->TAG:Ljava/lang/String;

    const-string v3, "Could not power off console"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 148
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 149
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 150
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackForgetConsole()V

    .line 151
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->disconnectConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 154
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->forgetDevice(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 161
    :cond_0
    return-void

    .line 155
    :catch_0
    move-exception v1

    .line 156
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->TAG:Ljava/lang/String;

    const-string v3, "Could not forget device"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$7(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 164
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 165
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setAutoConnect(Z)V

    .line 168
    invoke-static {p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackAutomaticallyConnect(Z)V

    .line 169
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateDropdown(Z)V

    .line 171
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$8(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 174
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->TAG:Ljava/lang/String;

    const-string v1, "OOBE entry button pressed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackNavigateToConsoleManager()V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dialog:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->show()V

    .line 177
    return-void
.end method

.method private updateDropdown(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 233
    if-nez p1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideIn:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 246
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 238
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 240
    .local v0, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    if-eqz v0, :cond_1

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideOut:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideIn:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->slideOut:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 226
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 230
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 91
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dialog:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getSettings()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 95
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 96
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->connectButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonClickable:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->disconnectButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->turnOffButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->forgetConsoleButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->autoConnectCheckBox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->oobeEntryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    return-void
.end method

.method public updateViewOverride()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 181
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->updateLoadingIndicator(Z)V

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    .line 185
    .local v1, "consoleData":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->getConnectionState()I

    move-result v0

    .line 187
    .local v0, "connectionState":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonClickable:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 189
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->connectButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 190
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f070376

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 191
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 192
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :goto_0
    return-void

    .line 194
    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    if-eqz v1, :cond_2

    .line 195
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getXboxName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 198
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f070ef6

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 203
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonClickable:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 204
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->connectButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 200
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownSymbol:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f070ef3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_1

    .line 208
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->connectButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 209
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownButtonClickable:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 210
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;->dropdownView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
