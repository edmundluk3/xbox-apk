.class abstract Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;
.super Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
.source "$AutoValue_MediaHubDataTypes_Locator.java"


# instance fields
.field private final expiration:Ljava/util/Date;

.field private final fileSize:Ljava/lang/Integer;

.field private final locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

.field private final uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/Date;Ljava/lang/Integer;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;Ljava/lang/String;)V
    .locals 0
    .param p1, "expiration"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "fileSize"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "locatorType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "uri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    .line 22
    iput-object p2, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    .line 23
    iput-object p3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 24
    iput-object p4, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 67
    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    .line 68
    .local v0, "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->expiration()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    if-nez v3, :cond_4

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->fileSize()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    if-nez v3, :cond_5

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 71
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 68
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->expiration()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 69
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->fileSize()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 70
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 71
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    :cond_7
    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public expiration()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    return-object v0
.end method

.method public fileSize()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 78
    const/4 v0, 0x1

    .line 79
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v3

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 83
    mul-int/2addr v0, v3

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 85
    mul-int/2addr v0, v3

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 87
    return v0

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    goto :goto_0

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->hashCode()I

    move-result v1

    goto :goto_2

    .line 86
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Locator{expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->expiration:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->fileSize:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locatorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->locatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Locator;->uri:Ljava/lang/String;

    return-object v0
.end method
