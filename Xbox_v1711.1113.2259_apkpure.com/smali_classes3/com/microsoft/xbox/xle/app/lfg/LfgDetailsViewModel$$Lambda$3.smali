.class final synthetic Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function5;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;->arg$1:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Lio/reactivex/functions/Function5;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$$Lambda$3;->arg$1:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object v1, p1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-object v2, p2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-object v3, p3

    check-cast v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    move-object v4, p4

    check-cast v4, Lcom/google/common/base/Optional;

    move-object v5, p5

    check-cast v5, Lcom/google/common/base/Optional;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->lambda$load$1(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/google/common/base/Optional;Lcom/google/common/base/Optional;)Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel$LoadCompletionBundle;

    move-result-object v0

    return-object v0
.end method
