.class public Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TVEpisodeDetailsHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private episodeImageUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 21
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 23
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 26
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->episodeImageUri:Ljava/lang/String;

    .line 29
    .end local v1    # "url":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f070683

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "uri":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->episodeImageUri:Ljava/lang/String;

    .line 47
    :cond_0
    return-object v0
.end method

.method public getReleaseData()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 64
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f071084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "S"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getSeasonNumber()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", Ep"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getEpisodeNumber()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 64
    invoke-static {v1, v4, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getSeriesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTVEpisodeExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "TODO: Hookup to live data"

    .line 59
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 39
    return-void
.end method
