.class final Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;
.super Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
.source "AutoValue_TutorialViewState.java"


# instance fields
.field private final error:Ljava/lang/Throwable;

.field private final isLinkedToFacebook:Z

.field private final isLoading:Z

.field private final welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;


# direct methods
.method constructor <init>(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;Ljava/lang/Throwable;Z)V
    .locals 0
    .param p1, "isLinkedToFacebook"    # Z
    .param p2, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "isLoading"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;-><init>()V

    .line 21
    iput-boolean p1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLinkedToFacebook:Z

    .line 22
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 23
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    .line 24
    iput-boolean p4, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLoading:Z

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    .line 66
    .local v0, "that":Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLinkedToFacebook:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isLinkedToFacebook()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    if-nez v3, :cond_3

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    if-nez v3, :cond_4

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLoading:Z

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->isLoading()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 67
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 68
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->error()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    :cond_5
    move v1, v2

    .line 71
    goto :goto_0
.end method

.method public error()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v4, 0x0

    const v5, 0xf4243

    .line 76
    const/4 v0, 0x1

    .line 77
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 78
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLinkedToFacebook:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 79
    mul-int/2addr v0, v5

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    if-nez v1, :cond_1

    move v1, v4

    :goto_1
    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v5

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v4

    .line 83
    mul-int/2addr v0, v5

    .line 84
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLoading:Z

    if-eqz v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 85
    return v0

    :cond_0
    move v1, v3

    .line 78
    goto :goto_0

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    goto :goto_2

    :cond_3
    move v2, v3

    .line 84
    goto :goto_3
.end method

.method public isLinkedToFacebook()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLinkedToFacebook:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLoading:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TutorialViewState{isLinkedToFacebook="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLinkedToFacebook:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", welcomeCardCompletionStates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public welcomeCardCompletionStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/AutoValue_TutorialViewState;->welcomeCardCompletionStates:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    return-object v0
.end method
