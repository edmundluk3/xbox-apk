.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "HomeScreenPopupScreenViewModelContent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMergedModelTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)V
    .locals 0

    .prologue
    .line 654
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$1;

    .prologue
    .line 654
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 657
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 672
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 654
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 667
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 654
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 662
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 663
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 689
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 690
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 654
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 8

    .prologue
    .line 677
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent<TT;>.LoadMergedModelTask;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    .line 678
    .local v0, "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v0, :cond_1

    .line 679
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v1

    .line 680
    .local v1, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-nez v1, :cond_0

    const-wide/16 v2, -0x1

    .line 681
    .local v2, "titleId":J
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$LoadMergedModelTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v6

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v7

    invoke-static {v5, v6, v7, v2, v3}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v5

    iput-object v5, v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->mergedModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 685
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .end local v2    # "titleId":J
    :goto_1
    return-void

    .line 680
    .restart local v1    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    goto :goto_0

    .line 683
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    const-string v5, "No media model found found, won\'t load merged model"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
