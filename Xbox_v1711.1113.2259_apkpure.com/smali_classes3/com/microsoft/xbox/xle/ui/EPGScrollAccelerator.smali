.class public Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
.super Landroid/widget/RelativeLayout;
.source "EPGScrollAccelerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;,
        Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;,
        Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;
    }
.end annotation


# static fields
.field private static final TWEEN_IN_DURATION_MS:J = 0x1f4L

.field private static final TWEEN_OUT_DURATION_MS:J = 0x1f4L

.field public static final TWEEN_OUT_POST_DELAY:J = 0x3e8L


# instance fields
.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mBackground:Landroid/view/View;

.field private mLargestHeight:F

.field private mListener:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

.field private mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

.field private mReleaseDelayHandler:Landroid/os/Handler;

.field private mSelectedIdx:F

.field private mSelectedPos:F

.field private mSlider:Landroid/view/View;

.field private mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 75
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 64
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedPos:F

    .line 65
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedIdx:F

    .line 72
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedPos:F

    .line 65
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedIdx:F

    .line 72
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->onHandlePost_tweenOut()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mBackground:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method private calculateIndex(F)I
    .locals 3
    .param p1, "position"    # F

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getWidth()I

    move-result v0

    .line 331
    .local v0, "width":I
    int-to-float v1, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 332
    const/4 v1, -0x1

    .line 334
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLargestHeight()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
    .locals 4
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 85
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v2, 0x7f0300f7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .line 86
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
    const/4 v2, 0x0

    iput v2, v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mLargestHeight:F

    .line 87
    const v2, 0x7f0e058c

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mBackground:Landroid/view/View;

    .line 88
    const v2, 0x7f0e058d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    .line 89
    new-instance v2, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;-><init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V

    iput-object v2, v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    .line 91
    return-object v1
.end method

.method private onHandlePost_tweenOut()V
    .locals 6

    .prologue
    .line 246
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_0

    .line 247
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 249
    :cond_0
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 251
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v0, v2

    .line 252
    .local v0, "desiredWidth":F
    const/high16 v1, 0x40e00000    # 7.0f

    .line 253
    .local v1, "easingExponent":F
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 254
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 255
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$2;

    invoke-direct {v3, p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;F)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 267
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;-><init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 292
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    const/high16 v4, 0x40e00000    # 7.0f

    sget-object v5, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseIn:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 294
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 295
    return-void

    .line 254
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private repositionSlider(FF)V
    .locals 3
    .param p1, "position"    # F
    .param p2, "height"    # F

    .prologue
    .line 319
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    if-nez v1, :cond_0

    .line 326
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, p2, v1

    .line 325
    .local v0, "sliderRange":F
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    mul-float v2, v0, p1

    invoke-virtual {v1, v2}, Landroid/view/View;->setY(F)V

    goto :goto_0
.end method

.method private touchToPositionEvent(F)V
    .locals 5
    .param p1, "y"    # F

    .prologue
    const/4 v4, 0x0

    .line 302
    const/4 v0, 0x0

    .line 303
    .local v0, "endzone":F
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLargestHeight()F

    move-result v1

    .line 305
    .local v1, "height":F
    cmpg-float v3, p1, v4

    if-gez v3, :cond_0

    .line 306
    const/4 v2, 0x0

    .line 313
    .local v2, "position":F
    :goto_0
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->repositionSlider(FF)V

    .line 315
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mListener:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

    invoke-interface {v3, v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;->onScrollAcceleratorEvent(F)V

    .line 316
    return-void

    .line 307
    .end local v2    # "position":F
    :cond_0
    cmpl-float v3, p1, v1

    if-lez v3, :cond_1

    .line 308
    const/high16 v2, 0x3f800000    # 1.0f

    .restart local v2    # "position":F
    goto :goto_0

    .line 310
    .end local v2    # "position":F
    :cond_1
    sub-float v3, p1, v4

    div-float v2, v3, v1

    .restart local v2    # "position":F
    goto :goto_0
.end method


# virtual methods
.method public getLargestHeight()F
    .locals 3

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getHeight()I

    move-result v1

    int-to-float v0, v1

    .line 97
    .local v0, "currentHeight":F
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mLargestHeight:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 98
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mLargestHeight:F

    .line 100
    :cond_0
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mLargestHeight:F

    return v1
.end method

.method public getListener()Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mListener:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

    return-object v0
.end method

.method public getSelectedPosition()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedPos:F

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mListener:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

    if-nez v1, :cond_0

    .line 145
    const/4 v1, 0x0

    .line 164
    :goto_0
    return v1

    .line 147
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 148
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 164
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 151
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mBackground:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 152
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->tweenIn(Ljava/lang/Object;)V

    .line 154
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->touchToPositionEvent(F)V

    goto :goto_1

    .line 158
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mBackground:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSlider:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 159
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->tweenOutOnDelay(Ljava/lang/Object;J)V

    goto :goto_1

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setListener(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mListener:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;

    .line 105
    return-void
.end method

.method public setSelectedPosition(F)V
    .locals 5
    .param p1, "position"    # F

    .prologue
    .line 113
    const/4 v3, 0x0

    cmpg-float v3, p1, v3

    if-gez v3, :cond_2

    .line 114
    const/4 p1, 0x0

    .line 118
    :cond_0
    :goto_0
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedPos:F

    cmpl-float v3, v3, p1

    if-nez v3, :cond_3

    .line 135
    :cond_1
    :goto_1
    return-void

    .line 115
    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, p1, v3

    if-lez v3, :cond_0

    .line 116
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 121
    :cond_3
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedPos:F

    .line 123
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->calculateIndex(F)I

    move-result v2

    .line 124
    .local v2, "index":I
    int-to-float v3, v2

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedIdx:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    .line 128
    const/4 v0, 0x0

    .line 129
    .local v0, "endzone":F
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLargestHeight()F

    move-result v1

    .line 131
    .local v1, "height":F
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->repositionSlider(FF)V

    .line 133
    int-to-float v3, v2

    iput v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mSelectedIdx:F

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->invalidate()V

    goto :goto_1
.end method

.method public tweenIn(Ljava/lang/Object;)V
    .locals 6
    .param p1, "caller"    # Ljava/lang/Object;

    .prologue
    .line 195
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    if-eqz v3, :cond_0

    .line 196
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->cancel()V

    .line 199
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 200
    .local v1, "newTweenInitiator":Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 202
    .local v2, "oldTweenInitiator":Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;
    if-ne p1, p0, :cond_1

    .line 203
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->SELF_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 208
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    if-eq v2, v3, :cond_2

    .line 209
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 239
    :goto_1
    return-void

    .line 205
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->OTHER_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    goto :goto_0

    .line 213
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_3

    .line 214
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 217
    :cond_3
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 219
    new-instance v3, Landroid/animation/ValueAnimator;

    invoke-direct {v3}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    .line 221
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v0, v3

    .line 222
    .local v0, "desiredWidth":F
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 223
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 224
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;

    invoke-direct {v4, p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;F)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 237
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    .line 223
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public tweenOutOnDelay(Ljava/lang/Object;J)V
    .locals 2
    .param p1, "caller"    # Ljava/lang/Object;
    .param p2, "delay"    # J

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    if-ne v0, v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    if-ne p1, p0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->OTHER_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    if-eq v0, v1, :cond_0

    .line 174
    :cond_2
    if-eq p1, p0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mTweenInitiator:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->SELF_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    if-eq v0, v1, :cond_0

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mReleaseDelayHandler:Landroid/os/Handler;

    if-nez v0, :cond_4

    .line 180
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mReleaseDelayHandler:Landroid/os/Handler;

    .line 182
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    if-eqz v0, :cond_5

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->cancel()V

    .line 186
    :cond_5
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;-><init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mReleaseDelayHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->mOnReleaseDelayEvent:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
