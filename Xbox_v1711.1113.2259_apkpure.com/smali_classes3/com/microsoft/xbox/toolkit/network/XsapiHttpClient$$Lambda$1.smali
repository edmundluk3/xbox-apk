.class final synthetic Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/microsoft/xbox/idp/util/HttpCall$Callback;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;

.field private final arg$3:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$1:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;

    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$2:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;

    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$3:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;)Lcom/microsoft/xbox/idp/util/HttpCall$Callback;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;-><init>(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;)V

    return-object v0
.end method


# virtual methods
.method public processResponse(ILjava/io/InputStream;Lcom/microsoft/xbox/idp/util/HttpHeaders;)V
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$1:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$2:Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->arg$3:Ljava/util/concurrent/CountDownLatch;

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->lambda$getHttpStatusAndStreamInternal$0(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;ILjava/io/InputStream;Lcom/microsoft/xbox/idp/util/HttpHeaders;)V

    return-void
.end method
