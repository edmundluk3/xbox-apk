.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "GameProfileInfoScreenAdapter.java"


# instance fields
.field private final descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private followButton:Landroid/widget/RelativeLayout;

.field private final followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private playButton:Landroid/widget/RelativeLayout;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewInStoreButton:Landroid/widget/RelativeLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    .line 35
    const v0, 0x7f0e0668

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->screenBody:Landroid/view/View;

    .line 36
    const v0, 0x7f0e0669

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 38
    const v0, 0x7f0e066a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 39
    const v0, 0x7f0e066e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 40
    const v0, 0x7f0e066f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 41
    const v0, 0x7f0e0665

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    const v0, 0x7f0e0664

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 43
    const v0, 0x7f0e066b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    .line 44
    const v0, 0x7f0e066d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    .line 45
    const v0, 0x7f0e066c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    .line 46
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->launchGame()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowState()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewInStore()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 4

    .prologue
    .line 50
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->getGameTitleId()J

    move-result-wide v0

    const-wide/32 v2, 0x6bf082d7

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 66
    :cond_2
    :goto_0
    return-void

    .line 63
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    :cond_2
    return-void
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->updateLoadingIndicator(Z)V

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->getData()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    .line 89
    .local v0, "data":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v0, :cond_1

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->getDisplayImageUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 92
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    iget-object v1, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 99
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->developerTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->developerName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->showPlayButton()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewInStoreButton:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->canViewInStore()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->isFollowedOnTitle()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070fe0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1
    :goto_1
    return-void

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->descriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->detail:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->description:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070e9a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;->followButtonLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
