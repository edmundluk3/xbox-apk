.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleHubSocialListAdapter$ClubViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;

    .line 23
    const v0, 0x7f0e08cf

    const-string v1, "field \'clubImage\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f0e08d6

    const-string v1, "field \'clubManagementIndicator\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    .line 25
    const v0, 0x7f0e08d1

    const-string v1, "field \'clubName\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 26
    const v0, 0x7f0e08d2

    const-string v1, "field \'clubGlyph\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f0e08d3

    const-string v1, "field \'clubType\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e08d5

    const-string v1, "field \'clubActivityText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;

    .line 35
    .local v0, "target":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;

    .line 38
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubManagementIndicator:Landroid/view/View;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$ClubViewHolder;->clubActivityText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    return-void
.end method
