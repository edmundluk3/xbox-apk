.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;
.super Ljava/lang/Object;
.source "ClubBackgroundChangeScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AchievementBackgroundItem"
.end annotation


# instance fields
.field private final achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V
    .locals 0
    .param p2, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 277
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    .param p3, "x2"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;

    .prologue
    .line 271
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getGamerScore()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getGamerscore()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getImageURI()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemType()I
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailImageURI()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;->achievementsItem:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->name:Ljava/lang/String;

    return-object v0
.end method
