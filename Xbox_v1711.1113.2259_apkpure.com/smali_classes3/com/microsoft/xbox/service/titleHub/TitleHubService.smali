.class public final enum Lcom/microsoft/xbox/service/titleHub/TitleHubService;
.super Ljava/lang/Enum;
.source "TitleHubService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/titleHub/ITitleHubService;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/titleHub/TitleHubService$BatchTitleRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubService;",
        ">;",
        "Lcom/microsoft/xbox/service/titleHub/ITitleHubService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubService;

.field private static final CONTRACT_VERSION:I = 0x2

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubService;

.field private static final MAX_ITEMS_PARAMETER:Ljava/lang/String; = "?maxItems=%s"

.field private static final MINI_CATALOG_FORMAT:Ljava/lang/String; = "https://titlehub.xboxlive.com/titles/win32/decoration/min,scid"

.field private static final RECENTLY_PLAYED_FORMAT:Ljava/lang/String; = "https://titlehub.xboxlive.com/users/xuid(%s)/titles/titlehistory/decoration/achievement,image,scid"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final TITLE_SUMMARY_BATCH_FORMAT:Ljava/lang/String; = "https://titlehub.xboxlive.com/titles/batch/decoration/detail,image,achievement,scid"

.field private static final TITLE_SUMMARY_FORMAT:Ljava/lang/String; = "https://titlehub.xboxlive.com/users/xuid(%s)/titles/titles(%s)/decoration/detail,image,achievement,friendsWhoPlayed,scid"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->INSTANCE:Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    .line 37
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->STATIC_HEADERS:Ljava/util/List;

    .line 51
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "x-xbl-contract-version"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 54
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getHeaders()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    new-instance v1, Ljava/util/ArrayList;

    sget-object v3, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 157
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getContentRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "contentRestrictions":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 159
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "x-xbl-contentrestrictions"

    invoke-direct {v3, v4, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "legalLocale":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 164
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept-Language"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    :cond_1
    return-object v1
.end method

.method private getRecentlyPlayedTitlesInternal(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 128
    .line 129
    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getHeaders()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService$$Lambda$3;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v2

    .line 131
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getHttp200Codes()[I

    move-result-object v3

    .line 128
    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 133
    .local v0, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-nez v0, :cond_0

    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_0
    invoke-static {p1, v1}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 135
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v1

    .line 138
    :goto_1
    return-object v1

    .line 133
    :cond_0
    sget-object v1, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_0

    .line 138
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static synthetic lambda$getMiniCatalog$3(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;

    return-object v0
.end method

.method static synthetic lambda$getRecentlyPlayedTitlesInternal$2(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    return-object v0
.end method

.method static synthetic lambda$getTitleSummaries$1(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    return-object v0
.end method

.method static synthetic lambda$getTitleSummary$0(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/TitleHubService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/titleHub/TitleHubService;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/titleHub/TitleHubService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubService;

    return-object v0
.end method


# virtual methods
.method public getAllRecentlyPlayedTitles(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 105
    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAllRecentlyPlayedTitles(xuid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 107
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 109
    const-string v1, "https://titlehub.xboxlive.com/users/xuid(%s)/titles/titlehistory/decoration/achievement,image,scid"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "url":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getRecentlyPlayedTitlesInternal(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getMiniCatalog()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    const-string v1, "getMiniCatalog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 148
    const-string v0, "https://titlehub.xboxlive.com/titles/win32/decoration/min,scid"

    .line 149
    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getHeaders()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v1

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getHttp200Codes()[I

    move-result-object v2

    .line 148
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;

    return-object v0
.end method

.method public getRecentlyPlayedTitles(Ljava/lang/String;I)Ljava/util/List;
    .locals 6
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 116
    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRecentlyPlayedTitles(xuid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", maxItems: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 118
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 119
    const-wide/16 v2, 0x1

    int-to-long v4, p2

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "myXuid":Ljava/lang/String;
    const-string v2, "https://titlehub.xboxlive.com/users/xuid(%s)/titles/titlehistory/decoration/achievement,image,scid?maxItems=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "url":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getRecentlyPlayedTitlesInternal(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method public getTitleSummaries(Ljava/util/Set;)Ljava/util/List;
    .locals 6
    .param p1    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 84
    sget-object v3, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    const-string v4, "getTitleSummaries"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 87
    const-string v2, "https://titlehub.xboxlive.com/titles/batch/decoration/detail,image,achievement,scid"

    .line 88
    .local v2, "url":Ljava/lang/String;
    new-instance v3, Lcom/microsoft/xbox/service/titleHub/TitleHubService$BatchTitleRequest;

    invoke-direct {v3, p1}, Lcom/microsoft/xbox/service/titleHub/TitleHubService$BatchTitleRequest;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "titleIdListJson":Ljava/lang/String;
    const-string v3, "https://titlehub.xboxlive.com/titles/batch/decoration/detail,image,achievement,scid"

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getHeaders()Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v4

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getHttp200Codes()[I

    move-result-object v5

    .line 90
    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 95
    .local v0, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    .line 98
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getTitleSummary(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 9
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 59
    sget-object v3, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTitleSummary("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 61
    const-wide/16 v4, 0x1

    invoke-static {v4, v5, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "myXuid":Ljava/lang/String;
    const-string v3, "https://titlehub.xboxlive.com/users/xuid(%s)/titles/titles(%s)/decoration/detail,image,achievement,friendsWhoPlayed,scid"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v7

    const/4 v5, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService;->getHeaders()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/service/titleHub/TitleHubService$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v4

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getHttp200Codes()[I

    move-result-object v5

    .line 66
    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 71
    .local v1, "response":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    if-nez v1, :cond_0

    sget-object v3, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_0
    invoke-static {v2, v3}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 73
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 74
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 76
    :goto_1
    return-object v3

    .line 71
    :cond_0
    sget-object v3, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_0

    .line 76
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
