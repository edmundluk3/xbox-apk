.class public Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;
.super Ljava/lang/Object;
.source "SimpleGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private dataObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private dataObjectsPreLength:I

.field private emptyResourceId:I

.field private gridLayoutListStateOrdinal:I

.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private viewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private viewResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resourceId"    # I
    .param p3, "emptyResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->gridLayoutListStateOrdinal:I

    .line 18
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->context:Landroid/content/Context;

    .line 29
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewResourceId:I

    .line 30
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->emptyResourceId:I

    .line 31
    iput-object p4, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjectsPreLength:I

    .line 33
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected createGridView(I)Landroid/view/View;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 108
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 110
    .local v0, "dataItem":Ljava/lang/Object;, "TT;"
    const/4 v1, 0x0

    .line 111
    .local v1, "v":Landroid/view/View;
    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->inflateEmptyView()Landroid/view/View;

    move-result-object v1

    .line 117
    :goto_0
    return-object v1

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->inflateView()Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getEmptyResourceId()I
    .locals 1

    .prologue
    .line 44
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->emptyResourceId:I

    return v0
.end method

.method public getGridView(I)Landroid/view/View;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 80
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 85
    :goto_0
    return-object v1

    .line 83
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->createGridView(I)Landroid/view/View;

    move-result-object v0

    .line 84
    .local v0, "item":Landroid/view/View;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    const/4 v0, 0x0

    .line 125
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->gridLayoutListStateOrdinal:I

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-object v0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getResourceId()I
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewResourceId:I

    return v0
.end method

.method protected inflateEmptyView()Landroid/view/View;
    .locals 4

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    const/4 v1, 0x0

    .line 90
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getEmptyResourceId()I

    move-result v2

    if-lez v2, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 92
    .local v0, "vi":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getEmptyResourceId()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 94
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    return-object v1
.end method

.method protected inflateView()Landroid/view/View;
    .locals 4

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    const/4 v1, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getResourceId()I

    move-result v2

    if-lez v2, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 101
    .local v0, "vi":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getResourceId()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 103
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    return-object v1
.end method

.method public notifyDataChanged()V
    .locals 2

    .prologue
    .line 66
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 67
    .local v0, "dataObjectsLength":I
    :goto_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjectsPreLength:I

    if-eq v1, v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->onDestory()V

    .line 69
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjectsPreLength:I

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v1}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 73
    return-void

    .line 66
    .end local v0    # "dataObjectsLength":I
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public notifyDataInvalidated()V
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    .line 77
    return-void
.end method

.method public onDestory()V
    .locals 3

    .prologue
    .line 136
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 137
    .local v1, "num":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->onItemDestory(Landroid/view/View;)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->viewCache:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 141
    return-void
.end method

.method public onItemDestory(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 145
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 48
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public setGridLayoutModelState(I)V
    .locals 0
    .param p1, "listStateOrdinal"    # I

    .prologue
    .line 121
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->gridLayoutListStateOrdinal:I

    .line 122
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 52
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->onDestory()V

    .line 55
    return-void
.end method

.method public updateDataObjects(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;, "Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter<TT;>;"
    .local p1, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjectsPreLength:I

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->notifyDataChanged()V

    .line 63
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->dataObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
