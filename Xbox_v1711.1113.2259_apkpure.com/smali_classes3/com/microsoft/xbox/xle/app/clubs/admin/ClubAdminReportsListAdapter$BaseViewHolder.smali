.class public abstract Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "BaseViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final ago:Landroid/widget/TextView;

.field private final contentView:Landroid/view/View;

.field private controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

.field private final count:Landroid/widget/TextView;

.field protected final delete:Landroid/view/View;

.field private final errorView:Landroid/view/View;

.field private final more:Landroid/view/View;

.field private final reason:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .line 129
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 131
    const v1, 0x7f0e0279

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->ago:Landroid/widget/TextView;

    .line 132
    const v1, 0x7f0e027a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->title:Landroid/widget/TextView;

    .line 133
    const v1, 0x7f0e027b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->reason:Landroid/widget/TextView;

    .line 134
    const v1, 0x7f0e0280

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->count:Landroid/widget/TextView;

    .line 135
    const v1, 0x7f0e027e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->errorView:Landroid/view/View;

    .line 136
    const v1, 0x7f0e0282

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->delete:Landroid/view/View;

    .line 137
    const v1, 0x7f0e0283

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->more:Landroid/view/View;

    .line 139
    const v1, 0x7f0e027d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 140
    .local v0, "innerStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->getInnerViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 141
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->contentView:Landroid/view/View;

    .line 142
    return-void
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->ignoreReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 195
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->removeAndCloseDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
    .param p1, "creatorXuid"    # J
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->banUser(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
    .param p1, "creatorXuid"    # J
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p4, "v1"    # Landroid/view/View;

    .prologue
    .line 196
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "creatorXuid"    # J

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporterXuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportsCreatorMessage(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreator()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 201
    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;JLandroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "creatorXuid"    # J
    .param p4, "v1"    # Landroid/view/View;

    .prologue
    .line 198
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporterXuid()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporterGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v3, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->banUser(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 203
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "creatorXuid"    # J

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporterXuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportsReporterMessage(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporter()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 208
    return-void
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;JLandroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "creatorXuid"    # J
    .param p4, "v1"    # Landroid/view/View;

    .prologue
    .line 205
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$onBind$10(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Landroid/view/View;)V
    .locals 14
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 158
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    const v12, 0x7f03005d

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 159
    .local v3, "controls":Landroid/view/View;
    new-instance v11, Landroid/support/design/widget/BottomSheetDialog;

    iget-object v12, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    iput-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    .line 160
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v11, v3}, Landroid/support/design/widget/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 161
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v11}, Landroid/support/design/widget/BottomSheetDialog;->getWindow()Landroid/view/Window;

    move-result-object v11

    const/16 v12, 0x7d3

    invoke-virtual {v11, v12}, Landroid/view/Window;->setType(I)V

    .line 163
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v12, 0x7f0e0288

    invoke-virtual {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 164
    .local v6, "ignore":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v12, 0x7f0e0289

    invoke-virtual {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 165
    .local v0, "banCreator":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v12, 0x7f0e028a

    invoke-virtual {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 166
    .local v7, "messageCreator":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v12, 0x7f0e028b

    invoke-virtual {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 167
    .local v1, "banReporter":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    const v12, 0x7f0e028c

    invoke-virtual {v11, v12}, Landroid/support/design/widget/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 169
    .local v8, "messageReporter":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    invoke-static {v11}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v2

    .line 170
    .local v2, "bottomSheetBehavior":Landroid/support/design/widget/BottomSheetBehavior;
    const/4 v11, 0x3

    invoke-virtual {v2, v11}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 172
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 173
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 174
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 175
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 176
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 178
    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    if-eqz v7, :cond_2

    if-eqz v1, :cond_2

    if-eqz v8, :cond_2

    .line 179
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->hasValidContent()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsCreatorMe()Z

    move-result v11

    if-nez v11, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsCreatorOwner()Z

    move-result v11

    if-nez v11, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsCreatorModerator()Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Z

    move-result v11

    if-eqz v11, :cond_3

    :cond_0
    const/4 v9, 0x1

    .line 180
    .local v9, "showBanCreator":Z
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsLastReporterMe()Z

    move-result v11

    if-nez v11, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsLastReportedOwner()Z

    move-result v11

    if-nez v11, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsLastReporterModerator()Z

    move-result v11

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_1
    const/4 v10, 0x1

    .line 182
    .local v10, "showBanReporter":Z
    :goto_1
    invoke-static {v0, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 183
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->hasValidContent()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsCreatorMe()Z

    move-result v11

    if-nez v11, :cond_5

    const/4 v11, 0x1

    :goto_2
    invoke-static {v7, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 184
    invoke-static {v1, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 185
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getIsLastReporterMe()Z

    move-result v11

    if-nez v11, :cond_6

    const/4 v11, 0x1

    :goto_3
    invoke-static {v8, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 187
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getBanCreatorText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 188
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getMessageCreatorText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 190
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getBanReporterText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getMessageReporterText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 193
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v11

    if-nez v11, :cond_7

    const-wide/16 v4, 0x0

    .line 195
    .local v4, "creatorXuid":J
    :goto_4
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    invoke-static {p0, v4, v5, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    invoke-static {p0, p1, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    invoke-static {p0, p1, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;J)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v11}, Landroid/support/design/widget/BottomSheetDialog;->show()V

    .line 212
    .end local v4    # "creatorXuid":J
    .end local v9    # "showBanCreator":Z
    .end local v10    # "showBanReporter":Z
    :cond_2
    return-void

    .line 179
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 180
    .restart local v9    # "showBanCreator":Z
    :cond_4
    const/4 v10, 0x0

    goto :goto_1

    .line 183
    .restart local v10    # "showBanReporter":Z
    :cond_5
    const/4 v11, 0x0

    goto :goto_2

    .line 185
    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    .line 193
    :cond_7
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_4
.end method


# virtual methods
.method protected closeDialogDecorator(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->controlsDialog:Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomSheetDialog;->hide()V

    .line 239
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 240
    return-void
.end method

.method protected abstract getInnerViewId()I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end method

.method protected hideErrorView()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->contentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->errorView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 250
    return-void
.end method

.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 150
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->ago:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getAgoText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07027c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->reason:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->count:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getFormattedCount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->more:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 115
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method protected removeAndCloseDecorator(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->getAdapterPosition()I

    move-result v0

    .line 218
    .local v0, "adapterPosition":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->remove(I)V

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->notifyItemRemoved(I)V

    .line 223
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->closeDialogDecorator(Ljava/lang/Runnable;)V

    .line 224
    return-void
.end method

.method protected removeDecorator(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->getAdapterPosition()I

    move-result v0

    .line 229
    .local v0, "adapterPosition":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->remove(I)V

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->notifyItemRemoved(I)V

    .line 234
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 235
    return-void
.end method

.method protected showErrorView()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->contentView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->errorView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 245
    return-void
.end method
