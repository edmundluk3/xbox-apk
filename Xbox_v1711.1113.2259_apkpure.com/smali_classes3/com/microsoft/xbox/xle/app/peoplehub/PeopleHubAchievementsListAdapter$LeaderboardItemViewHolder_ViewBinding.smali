.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 26
    const v0, 0x7f0e06a3

    const-string v1, "field \'gamerpic\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 27
    const v0, 0x7f0e06a4

    const-string v1, "field \'ribbonLayout\'"

    const-class v2, Landroid/widget/RelativeLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    .line 28
    const v0, 0x7f0e06a5

    const-string v1, "field \'ribbon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    .line 29
    const v0, 0x7f0e06a6

    const-string v1, "field \'gamertag\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e06a7

    const-string v1, "field \'subtext\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v0, 0x7f0e06a8

    const-string v1, "field \'monthlyGamerscore\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e06a9

    const-string v1, "field \'progressBar\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 33
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 39
    .local v0, "target":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 40
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 49
    return-void
.end method
