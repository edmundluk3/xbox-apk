.class public final Lcom/microsoft/xboxone/smartglass/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetEndWithActions:I = 0x19

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x18

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x1a

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1c

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x1b

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x1

.field public static final AlertDialog_listItemLayout:I = 0x5

.field public static final AlertDialog_listLayout:I = 0x2

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x3

.field public static final AlertDialog_showTitle:I = 0x6

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x4

.field public static final AppBarLayout:[I

.field public static final AppBarLayoutStates:[I

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1

.field public static final AppBarLayout_Layout:[I

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1

.field public static final AppBarLayout_android_background:I = 0x0

.field public static final AppBarLayout_elevation:I = 0x1

.field public static final AppBarLayout_expanded:I = 0x2

.field public static final AppCompatImageView:[I

.field public static final AppCompatImageView_android_src:I = 0x0

.field public static final AppCompatImageView_srcCompat:I = 0x1

.field public static final AppCompatSeekBar:[I

.field public static final AppCompatSeekBar_android_thumb:I = 0x0

.field public static final AppCompatSeekBar_tickMark:I = 0x1

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3

.field public static final AppCompatTextHelper:[I

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x2

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x6

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x3

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x4

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x5

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x1

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x0

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_textAllCaps:I = 0x1

.field public static final AppCompatTheme:[I

.field public static final AppCompatTheme_actionBarDivider:I = 0x17

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x18

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x11

.field public static final AppCompatTheme_actionBarSize:I = 0x16

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x13

.field public static final AppCompatTheme_actionBarStyle:I = 0x12

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0xd

.field public static final AppCompatTheme_actionBarTabStyle:I = 0xc

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0xe

.field public static final AppCompatTheme_actionBarTheme:I = 0x14

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0x15

.field public static final AppCompatTheme_actionButtonStyle:I = 0x32

.field public static final AppCompatTheme_actionDropDownStyle:I = 0x2e

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0x19

.field public static final AppCompatTheme_actionMenuTextColor:I = 0x1a

.field public static final AppCompatTheme_actionModeBackground:I = 0x1d

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x1c

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x1f

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x21

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x20

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x25

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x22

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x27

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x23

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x24

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x1e

.field public static final AppCompatTheme_actionModeStyle:I = 0x1b

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x26

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0xf

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x10

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x3a

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x5e

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x5f

.field public static final AppCompatTheme_alertDialogStyle:I = 0x5d

.field public static final AppCompatTheme_alertDialogTheme:I = 0x60

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x1

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x0

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x65

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x37

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x34

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x63

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x64

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x62

.field public static final AppCompatTheme_buttonBarStyle:I = 0x33

.field public static final AppCompatTheme_buttonStyle:I = 0x66

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x67

.field public static final AppCompatTheme_checkboxStyle:I = 0x68

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x69

.field public static final AppCompatTheme_colorAccent:I = 0x55

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x5c

.field public static final AppCompatTheme_colorButtonNormal:I = 0x59

.field public static final AppCompatTheme_colorControlActivated:I = 0x57

.field public static final AppCompatTheme_colorControlHighlight:I = 0x58

.field public static final AppCompatTheme_colorControlNormal:I = 0x56

.field public static final AppCompatTheme_colorPrimary:I = 0x53

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x54

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x5a

.field public static final AppCompatTheme_controlBackground:I = 0x5b

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x2c

.field public static final AppCompatTheme_dialogTheme:I = 0x2b

.field public static final AppCompatTheme_dividerHorizontal:I = 0x39

.field public static final AppCompatTheme_dividerVertical:I = 0x38

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x4b

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x2f

.field public static final AppCompatTheme_editTextBackground:I = 0x40

.field public static final AppCompatTheme_editTextColor:I = 0x3f

.field public static final AppCompatTheme_editTextStyle:I = 0x6a

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x31

.field public static final AppCompatTheme_imageButtonStyle:I = 0x41

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x52

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x2d

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x72

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x4c

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x46

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x48

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x47

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x49

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x4a

.field public static final AppCompatTheme_panelBackground:I = 0x4f

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x51

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x50

.field public static final AppCompatTheme_popupMenuStyle:I = 0x3d

.field public static final AppCompatTheme_popupWindowStyle:I = 0x3e

.field public static final AppCompatTheme_radioButtonStyle:I = 0x6b

.field public static final AppCompatTheme_ratingBarStyle:I = 0x6c

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x6d

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x6e

.field public static final AppCompatTheme_searchViewStyle:I = 0x45

.field public static final AppCompatTheme_seekBarStyle:I = 0x6f

.field public static final AppCompatTheme_selectableItemBackground:I = 0x35

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x36

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x30

.field public static final AppCompatTheme_spinnerStyle:I = 0x70

.field public static final AppCompatTheme_switchStyle:I = 0x71

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x28

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x4d

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x4e

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x2a

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x43

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x42

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x29

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x61

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x44

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x3c

.field public static final AppCompatTheme_toolbarStyle:I = 0x3b

.field public static final AppCompatTheme_windowActionBar:I = 0x2

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x4

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x5

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x9

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x7

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x6

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x8

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0xa

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0xb

.field public static final AppCompatTheme_windowNoTitle:I = 0x3

.field public static final ApplicationBar:[I

.field public static final ApplicationBar_appBarLayout:I = 0x0

.field public static final AspectRatioFrameLayout:[I

.field public static final AspectRatioFrameLayout_resize_mode:I = 0x0

.field public static final Banner:[I

.field public static final Banner_ratio:I = 0x0

.field public static final BottomSheetBehavior_Layout:[I

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x1

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x0

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x2

.field public static final BranchButton:[I

.field public static final BranchButton_command:I = 0x0

.field public static final BranchImageButton:[I

.field public static final BranchImageButton_command:I = 0x0

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final CardView:[I

.field public static final CardView_android_minHeight:I = 0x1

.field public static final CardView_android_minWidth:I = 0x0

.field public static final CardView_cardBackgroundColor:I = 0x2

.field public static final CardView_cardCornerRadius:I = 0x3

.field public static final CardView_cardElevation:I = 0x4

.field public static final CardView_cardMaxElevation:I = 0x5

.field public static final CardView_cardPreventCornerOverlap:I = 0x7

.field public static final CardView_cardUseCompatPadding:I = 0x6

.field public static final CardView_contentPadding:I = 0x8

.field public static final CardView_contentPaddingBottom:I = 0xc

.field public static final CardView_contentPaddingLeft:I = 0x9

.field public static final CardView_contentPaddingRight:I = 0xa

.field public static final CardView_contentPaddingTop:I = 0xb

.field public static final CirclePageIndicator:[I

.field public static final CirclePageIndicator_primaryColor:I = 0x0

.field public static final CirclePageIndicator_radius:I = 0x2

.field public static final CirclePageIndicator_secondaryColor:I = 0x1

.field public static final CirclePageIndicator_viewpager:I = 0x3

.field public static final ClubTypeDescriptionView:[I

.field public static final ClubTypeDescriptionView_clubType:I = 0x0

.field public static final CollapsingToolbarLayout:[I

.field public static final CollapsingToolbarLayout_Layout:[I

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0xd

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x7

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x8

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0xe

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x1

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x4

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x2

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x3

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x6

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xc

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0x9

.field public static final CollapsingToolbarLayout_title:I = 0x0

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xf

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xa

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final ComparisonBar:[I

.field public static final ComparisonBar_isSpace:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonTint:I = 0x1

.field public static final CompoundButton_buttonTintMode:I = 0x2

.field public static final ConnectDialogState:[I

.field public static final ConnectDialogState_state_connection_connect_failed:I = 0x2

.field public static final ConnectDialogState_state_connection_connected:I = 0x0

.field public static final ConnectDialogState_state_connection_connecting:I = 0x1

.field public static final ConnectDialogState_state_connection_disconnected:I = 0x4

.field public static final ConnectDialogState_state_connection_disconnecting:I = 0x3

.field public static final ConnectDialogState_state_not_listed:I = 0x5

.field public static final ConstraintLayout_Layout:[I

.field public static final ConstraintLayout_Layout_android_maxHeight:I = 0x2

.field public static final ConstraintLayout_Layout_android_maxWidth:I = 0x1

.field public static final ConstraintLayout_Layout_android_minHeight:I = 0x4

.field public static final ConstraintLayout_Layout_android_minWidth:I = 0x3

.field public static final ConstraintLayout_Layout_android_orientation:I = 0x0

.field public static final ConstraintLayout_Layout_constraintSet:I = 0x5

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_creator:I = 0x6

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf:I = 0x7

.field public static final ConstraintLayout_Layout_layout_constraintBottom_creator:I = 0x8

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toBottomOf:I = 0x9

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toTopOf:I = 0xa

.field public static final ConstraintLayout_Layout_layout_constraintDimensionRatio:I = 0xb

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toEndOf:I = 0xc

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toStartOf:I = 0xd

.field public static final ConstraintLayout_Layout_layout_constraintGuide_begin:I = 0xe

.field public static final ConstraintLayout_Layout_layout_constraintGuide_end:I = 0xf

.field public static final ConstraintLayout_Layout_layout_constraintGuide_percent:I = 0x10

.field public static final ConstraintLayout_Layout_layout_constraintHeight_default:I = 0x11

.field public static final ConstraintLayout_Layout_layout_constraintHeight_max:I = 0x12

.field public static final ConstraintLayout_Layout_layout_constraintHeight_min:I = 0x13

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_bias:I = 0x14

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle:I = 0x15

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_weight:I = 0x16

.field public static final ConstraintLayout_Layout_layout_constraintLeft_creator:I = 0x17

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toLeftOf:I = 0x18

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toRightOf:I = 0x19

.field public static final ConstraintLayout_Layout_layout_constraintRight_creator:I = 0x1a

.field public static final ConstraintLayout_Layout_layout_constraintRight_toLeftOf:I = 0x1b

.field public static final ConstraintLayout_Layout_layout_constraintRight_toRightOf:I = 0x1c

.field public static final ConstraintLayout_Layout_layout_constraintStart_toEndOf:I = 0x1d

.field public static final ConstraintLayout_Layout_layout_constraintStart_toStartOf:I = 0x1e

.field public static final ConstraintLayout_Layout_layout_constraintTop_creator:I = 0x1f

.field public static final ConstraintLayout_Layout_layout_constraintTop_toBottomOf:I = 0x20

.field public static final ConstraintLayout_Layout_layout_constraintTop_toTopOf:I = 0x21

.field public static final ConstraintLayout_Layout_layout_constraintVertical_bias:I = 0x22

.field public static final ConstraintLayout_Layout_layout_constraintVertical_chainStyle:I = 0x23

.field public static final ConstraintLayout_Layout_layout_constraintVertical_weight:I = 0x24

.field public static final ConstraintLayout_Layout_layout_constraintWidth_default:I = 0x25

.field public static final ConstraintLayout_Layout_layout_constraintWidth_max:I = 0x26

.field public static final ConstraintLayout_Layout_layout_constraintWidth_min:I = 0x27

.field public static final ConstraintLayout_Layout_layout_editor_absoluteX:I = 0x28

.field public static final ConstraintLayout_Layout_layout_editor_absoluteY:I = 0x29

.field public static final ConstraintLayout_Layout_layout_goneMarginBottom:I = 0x2a

.field public static final ConstraintLayout_Layout_layout_goneMarginEnd:I = 0x2b

.field public static final ConstraintLayout_Layout_layout_goneMarginLeft:I = 0x2c

.field public static final ConstraintLayout_Layout_layout_goneMarginRight:I = 0x2d

.field public static final ConstraintLayout_Layout_layout_goneMarginStart:I = 0x2e

.field public static final ConstraintLayout_Layout_layout_goneMarginTop:I = 0x2f

.field public static final ConstraintLayout_Layout_layout_optimizationLevel:I = 0x30

.field public static final ConstraintSet:[I

.field public static final ConstraintSet_android_alpha:I = 0x9

.field public static final ConstraintSet_android_elevation:I = 0x15

.field public static final ConstraintSet_android_id:I = 0x1

.field public static final ConstraintSet_android_layout_height:I = 0x4

.field public static final ConstraintSet_android_layout_marginBottom:I = 0x8

.field public static final ConstraintSet_android_layout_marginEnd:I = 0x13

.field public static final ConstraintSet_android_layout_marginLeft:I = 0x5

.field public static final ConstraintSet_android_layout_marginRight:I = 0x7

.field public static final ConstraintSet_android_layout_marginStart:I = 0x12

.field public static final ConstraintSet_android_layout_marginTop:I = 0x6

.field public static final ConstraintSet_android_layout_width:I = 0x3

.field public static final ConstraintSet_android_orientation:I = 0x0

.field public static final ConstraintSet_android_rotationX:I = 0x10

.field public static final ConstraintSet_android_rotationY:I = 0x11

.field public static final ConstraintSet_android_scaleX:I = 0xe

.field public static final ConstraintSet_android_scaleY:I = 0xf

.field public static final ConstraintSet_android_transformPivotX:I = 0xa

.field public static final ConstraintSet_android_transformPivotY:I = 0xb

.field public static final ConstraintSet_android_translationX:I = 0xc

.field public static final ConstraintSet_android_translationY:I = 0xd

.field public static final ConstraintSet_android_translationZ:I = 0x14

.field public static final ConstraintSet_android_visibility:I = 0x2

.field public static final ConstraintSet_layout_constraintBaseline_creator:I = 0x16

.field public static final ConstraintSet_layout_constraintBaseline_toBaselineOf:I = 0x17

.field public static final ConstraintSet_layout_constraintBottom_creator:I = 0x18

.field public static final ConstraintSet_layout_constraintBottom_toBottomOf:I = 0x19

.field public static final ConstraintSet_layout_constraintBottom_toTopOf:I = 0x1a

.field public static final ConstraintSet_layout_constraintDimensionRatio:I = 0x1b

.field public static final ConstraintSet_layout_constraintEnd_toEndOf:I = 0x1c

.field public static final ConstraintSet_layout_constraintEnd_toStartOf:I = 0x1d

.field public static final ConstraintSet_layout_constraintGuide_begin:I = 0x1e

.field public static final ConstraintSet_layout_constraintGuide_end:I = 0x1f

.field public static final ConstraintSet_layout_constraintGuide_percent:I = 0x20

.field public static final ConstraintSet_layout_constraintHeight_default:I = 0x21

.field public static final ConstraintSet_layout_constraintHeight_max:I = 0x22

.field public static final ConstraintSet_layout_constraintHeight_min:I = 0x23

.field public static final ConstraintSet_layout_constraintHorizontal_bias:I = 0x24

.field public static final ConstraintSet_layout_constraintHorizontal_chainStyle:I = 0x25

.field public static final ConstraintSet_layout_constraintHorizontal_weight:I = 0x26

.field public static final ConstraintSet_layout_constraintLeft_creator:I = 0x27

.field public static final ConstraintSet_layout_constraintLeft_toLeftOf:I = 0x28

.field public static final ConstraintSet_layout_constraintLeft_toRightOf:I = 0x29

.field public static final ConstraintSet_layout_constraintRight_creator:I = 0x2a

.field public static final ConstraintSet_layout_constraintRight_toLeftOf:I = 0x2b

.field public static final ConstraintSet_layout_constraintRight_toRightOf:I = 0x2c

.field public static final ConstraintSet_layout_constraintStart_toEndOf:I = 0x2d

.field public static final ConstraintSet_layout_constraintStart_toStartOf:I = 0x2e

.field public static final ConstraintSet_layout_constraintTop_creator:I = 0x2f

.field public static final ConstraintSet_layout_constraintTop_toBottomOf:I = 0x30

.field public static final ConstraintSet_layout_constraintTop_toTopOf:I = 0x31

.field public static final ConstraintSet_layout_constraintVertical_bias:I = 0x32

.field public static final ConstraintSet_layout_constraintVertical_chainStyle:I = 0x33

.field public static final ConstraintSet_layout_constraintVertical_weight:I = 0x34

.field public static final ConstraintSet_layout_constraintWidth_default:I = 0x35

.field public static final ConstraintSet_layout_constraintWidth_max:I = 0x36

.field public static final ConstraintSet_layout_constraintWidth_min:I = 0x37

.field public static final ConstraintSet_layout_editor_absoluteX:I = 0x38

.field public static final ConstraintSet_layout_editor_absoluteY:I = 0x39

.field public static final ConstraintSet_layout_goneMarginBottom:I = 0x3a

.field public static final ConstraintSet_layout_goneMarginEnd:I = 0x3b

.field public static final ConstraintSet_layout_goneMarginLeft:I = 0x3c

.field public static final ConstraintSet_layout_goneMarginRight:I = 0x3d

.field public static final ConstraintSet_layout_goneMarginStart:I = 0x3e

.field public static final ConstraintSet_layout_goneMarginTop:I = 0x3f

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x6

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x3

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final CropImageView:[I

.field public static final CropImageView_cropAspectRatioX:I = 0x7

.field public static final CropImageView_cropAspectRatioY:I = 0x8

.field public static final CropImageView_cropAutoZoomEnabled:I = 0x3

.field public static final CropImageView_cropBackgroundColor:I = 0x12

.field public static final CropImageView_cropBorderCornerColor:I = 0xf

.field public static final CropImageView_cropBorderCornerLength:I = 0xe

.field public static final CropImageView_cropBorderCornerOffset:I = 0xd

.field public static final CropImageView_cropBorderCornerThickness:I = 0xc

.field public static final CropImageView_cropBorderLineColor:I = 0xb

.field public static final CropImageView_cropBorderLineThickness:I = 0xa

.field public static final CropImageView_cropFixAspectRatio:I = 0x6

.field public static final CropImageView_cropGuidelines:I = 0x0

.field public static final CropImageView_cropGuidelinesColor:I = 0x11

.field public static final CropImageView_cropGuidelinesThickness:I = 0x10

.field public static final CropImageView_cropInitialCropWindowPaddingRatio:I = 0x9

.field public static final CropImageView_cropMaxCropResultHeightPX:I = 0x1c

.field public static final CropImageView_cropMaxCropResultWidthPX:I = 0x1b

.field public static final CropImageView_cropMaxZoom:I = 0x4

.field public static final CropImageView_cropMinCropResultHeightPX:I = 0x1a

.field public static final CropImageView_cropMinCropResultWidthPX:I = 0x19

.field public static final CropImageView_cropMinCropWindowHeight:I = 0x18

.field public static final CropImageView_cropMinCropWindowWidth:I = 0x17

.field public static final CropImageView_cropMultiTouchEnabled:I = 0x5

.field public static final CropImageView_cropScaleType:I = 0x1

.field public static final CropImageView_cropShape:I = 0x2

.field public static final CropImageView_cropShowCropOverlay:I = 0x15

.field public static final CropImageView_cropShowProgressBar:I = 0x16

.field public static final CropImageView_cropSnapRadius:I = 0x13

.field public static final CropImageView_cropTouchRadius:I = 0x14

.field public static final CroppedImageView:[I

.field public static final CroppedImageView_endX:I = 0x2

.field public static final CroppedImageView_endY:I = 0x3

.field public static final CroppedImageView_startX:I = 0x0

.field public static final CroppedImageView_startY:I = 0x1

.field public static final CustomStyledTextView:[I

.field public static final CustomStyledTextView_typefaceSource:I = 0x0

.field public static final CustomTypeface:[I

.field public static final CustomTypeface_deleteOnActivityDestroyOrTombstone:I = 0x2

.field public static final CustomTypeface_typefaceSource:I = 0x0

.field public static final CustomTypeface_uppercaseText:I = 0x1

.field public static final DesignTheme:[I

.field public static final DesignTheme_bottomSheetDialogTheme:I = 0x0

.field public static final DesignTheme_bottomSheetStyle:I = 0x1

.field public static final DesignTheme_textColorError:I = 0x2

.field public static final DetailsMoreOrLessView:[I

.field public static final DetailsMoreOrLessView_alwaysShowAllText:I = 0x1

.field public static final DetailsMoreOrLessView_android_textColor:I = 0x0

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x4

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x5

.field public static final DrawerArrowToggle_barLength:I = 0x6

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final EPGRecentChannelsView:[I

.field public static final EPGRecentChannelsView_widthToHeightRatio:I = 0x0

.field public static final EPGRecentChannelsView_widthToMarginRatio:I = 0x1

.field public static final EditViewFixedLength:[I

.field public static final EditViewFixedLength_editTextId:I = 0x7

.field public static final EditViewFixedLength_inputType:I = 0x3

.field public static final EditViewFixedLength_layoutId:I = 0x5

.field public static final EditViewFixedLength_maxCharacterCount:I = 0x1

.field public static final EditViewFixedLength_maxLines:I = 0x4

.field public static final EditViewFixedLength_singleLine:I = 0x2

.field public static final EditViewFixedLength_textCountId:I = 0x8

.field public static final EditViewFixedLength_titleId:I = 0x6

.field public static final EditViewFixedLength_viewTitle:I = 0x0

.field public static final FeaturedList:[I

.field public static final FeaturedList_columnPadding:I = 0x0

.field public static final FeaturedList_flingDamping:I = 0x2

.field public static final FeaturedList_numberOfColumns:I = 0x1

.field public static final FixedAspectLayout:[I

.field public static final FixedAspectLayout_aspectX:I = 0x0

.field public static final FixedAspectLayout_aspectY:I = 0x1

.field public static final FixedAspectLayout_fixDimension:I = 0x2

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_Behavior_Layout:[I

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final FloatingActionButton_backgroundTint:I = 0x6

.field public static final FloatingActionButton_backgroundTintMode:I = 0x7

.field public static final FloatingActionButton_borderWidth:I = 0x4

.field public static final FloatingActionButton_elevation:I = 0x0

.field public static final FloatingActionButton_fabSize:I = 0x2

.field public static final FloatingActionButton_pressedTranslationZ:I = 0x3

.field public static final FloatingActionButton_rippleColor:I = 0x1

.field public static final FloatingActionButton_useCompatPadding:I = 0x5

.field public static final FontStarRatingView:[I

.field public static final FontStarRatingView_activeStarColor:I = 0x0

.field public static final FontStarRatingView_hideRatingText:I = 0x3

.field public static final FontStarRatingView_inactiveStarColor:I = 0x1

.field public static final FontStarRatingView_ratingTextColor:I = 0x2

.field public static final FontStarRatingView_ratingTextSize:I = 0x5

.field public static final FontStarRatingView_starSize:I = 0x4

.field public static final ForegroundLinearLayout:[I

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2

.field public static final GenericDraweeHierarchy:[I

.field public static final GenericDraweeHierarchy_actualImageScaleType:I = 0xb

.field public static final GenericDraweeHierarchy_backgroundImage:I = 0xc

.field public static final GenericDraweeHierarchy_fadeDuration:I = 0x0

.field public static final GenericDraweeHierarchy_failureImage:I = 0x6

.field public static final GenericDraweeHierarchy_failureImageScaleType:I = 0x7

.field public static final GenericDraweeHierarchy_overlayImage:I = 0xd

.field public static final GenericDraweeHierarchy_placeholderImage:I = 0x2

.field public static final GenericDraweeHierarchy_placeholderImageScaleType:I = 0x3

.field public static final GenericDraweeHierarchy_pressedStateOverlayImage:I = 0xe

.field public static final GenericDraweeHierarchy_progressBarAutoRotateInterval:I = 0xa

.field public static final GenericDraweeHierarchy_progressBarImage:I = 0x8

.field public static final GenericDraweeHierarchy_progressBarImageScaleType:I = 0x9

.field public static final GenericDraweeHierarchy_retryImage:I = 0x4

.field public static final GenericDraweeHierarchy_retryImageScaleType:I = 0x5

.field public static final GenericDraweeHierarchy_roundAsCircle:I = 0xf

.field public static final GenericDraweeHierarchy_roundBottomLeft:I = 0x14

.field public static final GenericDraweeHierarchy_roundBottomRight:I = 0x13

.field public static final GenericDraweeHierarchy_roundTopLeft:I = 0x11

.field public static final GenericDraweeHierarchy_roundTopRight:I = 0x12

.field public static final GenericDraweeHierarchy_roundWithOverlayColor:I = 0x15

.field public static final GenericDraweeHierarchy_roundedCornerRadius:I = 0x10

.field public static final GenericDraweeHierarchy_roundingBorderColor:I = 0x17

.field public static final GenericDraweeHierarchy_roundingBorderPadding:I = 0x18

.field public static final GenericDraweeHierarchy_roundingBorderWidth:I = 0x16

.field public static final GenericDraweeHierarchy_viewAspectRatio:I = 0x1

.field public static final IconFontButton:[I

.field public static final IconFontButton_button_icon:I = 0x1

.field public static final IconFontButton_button_text:I = 0x0

.field public static final IconFontButton_button_text_size:I = 0x2

.field public static final IconFontRingButton:[I

.field public static final IconFontRingButton_btn_count:I = 0x1

.field public static final IconFontRingButton_btn_icon:I = 0x0

.field public static final IconFontRingButton_btn_sub_text:I = 0x3

.field public static final IconFontRingButton_btn_text:I = 0x2

.field public static final IconFontToggleButton:[I

.field public static final IconFontToggleButton_icon_checked:I = 0x2

.field public static final IconFontToggleButton_icon_unchecked:I = 0x3

.field public static final IconFontToggleButton_text_checked:I = 0x0

.field public static final IconFontToggleButton_text_unchecked:I = 0x1

.field public static final LabeledArcView:[I

.field public static final LabeledArcView_arcColor:I = 0x1

.field public static final LabeledArcView_arcStrokeSize:I = 0x0

.field public static final LeadingIconTextView:[I

.field public static final LeadingIconTextView_android_text:I = 0x1

.field public static final LeadingIconTextView_android_textSize:I = 0x0

.field public static final LeadingIconTextView_leadingIcon:I = 0x3

.field public static final LeadingIconTextView_leadingIconSize:I = 0x4

.field public static final LeadingIconTextView_spacesBeforeIcon:I = 0x6

.field public static final LeadingIconTextView_spacesBeforeText:I = 0x5

.field public static final LeadingIconTextView_typefaceSource:I = 0x2

.field public static final LikeControl:[I

.field public static final LikeControl_android_textColor:I = 0x1

.field public static final LikeControl_android_textSize:I = 0x0

.field public static final LikeControl_hideCount:I = 0x3

.field public static final LikeControl_iconSize:I = 0x2

.field public static final LinearConstraintLayout:[I

.field public static final LinearConstraintLayout_android_orientation:I = 0x0

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x2

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x0

.field public static final MaxWidthLinearLayout:[I

.field public static final MaxWidthLinearLayout_maxWidth:I = 0x0

.field public static final MediaButtons:[I

.field public static final MediaButtons_hidePrevNextTrackButtons:I = 0x0

.field public static final MediaProgressBar:[I

.field public static final MediaProgressBar_showProgressText:I = 0x0

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_subMenuArrow:I = 0x8

.field public static final MixedFontFaceTextView:[I

.field public static final MixedFontFaceTextView_formatFontFace:I = 0x3

.field public static final MixedFontFaceTextView_formatFontSize:I = 0x5

.field public static final MixedFontFaceTextView_formatFontString:I = 0x4

.field public static final MixedFontFaceTextView_mainFontFace:I = 0x0

.field public static final MixedFontFaceTextView_mainFontSize:I = 0x2

.field public static final MixedFontFaceTextView_mainTextString:I = 0x1

.field public static final MixedFontFaceTextView_willPadFormatStringWithNBSP:I = 0x6

.field public static final NavigationView:[I

.field public static final NavigationView_android_background:I = 0x0

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1

.field public static final NavigationView_android_maxWidth:I = 0x2

.field public static final NavigationView_elevation:I = 0x3

.field public static final NavigationView_headerLayout:I = 0x9

.field public static final NavigationView_itemBackground:I = 0x7

.field public static final NavigationView_itemIconTint:I = 0x5

.field public static final NavigationView_itemTextAppearance:I = 0x8

.field public static final NavigationView_itemTextColor:I = 0x6

.field public static final NavigationView_menu:I = 0x4

.field public static final Panorama:[I

.field public static final Panorama_scrollPaddingLeft:I = 0x0

.field public static final Panorama_scrollPaddingRight:I = 0x1

.field public static final PercentLayout_Layout:[I

.field public static final PercentLayout_Layout_layout_aspectRatio:I = 0x9

.field public static final PercentLayout_Layout_layout_heightPercent:I = 0x1

.field public static final PercentLayout_Layout_layout_marginBottomPercent:I = 0x6

.field public static final PercentLayout_Layout_layout_marginEndPercent:I = 0x8

.field public static final PercentLayout_Layout_layout_marginLeftPercent:I = 0x3

.field public static final PercentLayout_Layout_layout_marginPercent:I = 0x2

.field public static final PercentLayout_Layout_layout_marginRightPercent:I = 0x5

.field public static final PercentLayout_Layout_layout_marginStartPercent:I = 0x7

.field public static final PercentLayout_Layout_layout_marginTopPercent:I = 0x4

.field public static final PercentLayout_Layout_layout_widthPercent:I = 0x0

.field public static final PieChartView:[I

.field public static final PieChartView_strokeWidth:I = 0x0

.field public static final Pivot:[I

.field public static final PivotWithTabs:[I

.field public static final PivotWithTabs_headerText:I = 0x0

.field public static final PivotWithTabs_subHeaderText:I = 0x1

.field public static final Pivot_selected_color:I = 0x0

.field public static final Pivot_unselected_color:I = 0x1

.field public static final PlaybackControlView:[I

.field public static final PlaybackControlView_controller_layout_id:I = 0x0

.field public static final PlaybackControlView_fastforward_increment:I = 0x1

.field public static final PlaybackControlView_rewind_increment:I = 0x2

.field public static final PlaybackControlView_show_timeout:I = 0x3

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final PrivacySettingView:[I

.field public static final PrivacySettingView_description:I = 0x1

.field public static final PrivacySettingView_header:I = 0x0

.field public static final RecycleListView:[I

.field public static final RecycleListView_paddingBottomNoButtons:I = 0x0

.field public static final RecycleListView_paddingTopNoTitle:I = 0x1

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_layoutManager:I = 0x2

.field public static final RecyclerView_reverseLayout:I = 0x4

.field public static final RecyclerView_spanCount:I = 0x3

.field public static final RecyclerView_stackFromEnd:I = 0x5

.field public static final RelativeLayoutWithForegroundSelector:[I

.field public static final RelativeLayoutWithForegroundSelector_android_foreground:I = 0x0

.field public static final RelativeLayoutWithForegroundSelector_xleForegroundInsidePadding:I = 0x1

.field public static final ReputationArcView:[I

.field public static final ReputationArcView_arcStrokeWidth:I = 0x0

.field public static final ScreenLayout:[I

.field public static final ScreenLayout_screenDIPs:I = 0x1

.field public static final ScreenLayout_screenPercent:I = 0x0

.field public static final ScrimInsetsFrameLayout:[I

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0

.field public static final ScrollingViewBehavior_Layout:[I

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x8

.field public static final SearchView_commitIcon:I = 0xd

.field public static final SearchView_defaultQueryHint:I = 0x7

.field public static final SearchView_goIcon:I = 0x9

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xf

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchHintIcon:I = 0xb

.field public static final SearchView_searchIcon:I = 0xa

.field public static final SearchView_submitBackground:I = 0x10

.field public static final SearchView_suggestionRowLayout:I = 0xe

.field public static final SearchView_voiceIcon:I = 0xc

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2

.field public static final SimpleDraweeView:[I

.field public static final SimpleDraweeView_actualImageUri:I = 0x0

.field public static final SimpleExoPlayerView:[I

.field public static final SimpleExoPlayerView_controller_layout_id:I = 0x0

.field public static final SimpleExoPlayerView_default_artwork:I = 0x8

.field public static final SimpleExoPlayerView_fastforward_increment:I = 0x1

.field public static final SimpleExoPlayerView_player_layout_id:I = 0x2

.field public static final SimpleExoPlayerView_resize_mode:I = 0x3

.field public static final SimpleExoPlayerView_rewind_increment:I = 0x4

.field public static final SimpleExoPlayerView_show_timeout:I = 0x5

.field public static final SimpleExoPlayerView_surface_type:I = 0x6

.field public static final SimpleExoPlayerView_use_artwork:I = 0x7

.field public static final SimpleExoPlayerView_use_controller:I = 0x9

.field public static final SimpleGridLayout:[I

.field public static final SimpleGridLayout_gridDividerSize:I = 0x0

.field public static final SimpleGridLayout_notLongColumnNumber:I = 0x1

.field public static final SmartGlassControlPickerButton:[I

.field public static final SmartGlassControlPickerButton_activeimage:I = 0x1

.field public static final SmartGlassControlPickerButton_disabledimage:I = 0x2

.field public static final SmartGlassControlPickerButton_enabledimage:I = 0x3

.field public static final SmartGlassControlPickerButton_text:I = 0x0

.field public static final SnackbarLayout:[I

.field public static final SnackbarLayout_android_maxWidth:I = 0x0

.field public static final SnackbarLayout_elevation:I = 0x1

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x2

.field public static final SocialBarValue:[I

.field public static final SocialBarValue_socialBarValue:I = 0x3

.field public static final SocialBarValue_textOne:I = 0x1

.field public static final SocialBarValue_textTwoOrMore:I = 0x2

.field public static final SocialBarValue_textZero:I = 0x0

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_entries:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final StackingBehavior_Params:[I

.field public static final StackingBehavior_Params_behavior_previousSibling:I = 0x0

.field public static final StarRatingView:[I

.field public static final StarRatingView_maskColorType:I = 0x0

.field public static final StyledTextView:[I

.field public static final StyledTextView_font:I = 0x0

.field public static final StyledTextView_isUnderlined:I = 0x1

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0xd

.field public static final SwitchCompat_splitTrack:I = 0xc

.field public static final SwitchCompat_switchMinWidth:I = 0xa

.field public static final SwitchCompat_switchPadding:I = 0xb

.field public static final SwitchCompat_switchTextAppearance:I = 0x9

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_thumbTint:I = 0x3

.field public static final SwitchCompat_thumbTintMode:I = 0x4

.field public static final SwitchCompat_track:I = 0x5

.field public static final SwitchCompat_trackTint:I = 0x6

.field public static final SwitchCompat_trackTintMode:I = 0x7

.field public static final SwitchPanel:[I

.field public static final SwitchPanelItem:[I

.field public static final SwitchPanelItem_state:I = 0x0

.field public static final SwitchPanel_selectedState:I = 0x0

.field public static final TabItem:[I

.field public static final TabItem_android_icon:I = 0x0

.field public static final TabItem_android_layout:I = 0x1

.field public static final TabItem_android_text:I = 0x2

.field public static final TabLayout:[I

.field public static final TabLayout_tabBackground:I = 0x3

.field public static final TabLayout_tabContentStart:I = 0x2

.field public static final TabLayout_tabGravity:I = 0x5

.field public static final TabLayout_tabIndicatorColor:I = 0x0

.field public static final TabLayout_tabIndicatorHeight:I = 0x1

.field public static final TabLayout_tabMaxWidth:I = 0x7

.field public static final TabLayout_tabMinWidth:I = 0x6

.field public static final TabLayout_tabMode:I = 0x4

.field public static final TabLayout_tabPadding:I = 0xf

.field public static final TabLayout_tabPaddingBottom:I = 0xe

.field public static final TabLayout_tabPaddingEnd:I = 0xd

.field public static final TabLayout_tabPaddingStart:I = 0xb

.field public static final TabLayout_tabPaddingTop:I = 0xc

.field public static final TabLayout_tabSelectedTextColor:I = 0xa

.field public static final TabLayout_tabTextAppearance:I = 0x8

.field public static final TabLayout_tabTextColor:I = 0x9

.field public static final TextAppearance:[I

.field public static final TextAppearance_android_shadowColor:I = 0x5

.field public static final TextAppearance_android_shadowDx:I = 0x6

.field public static final TextAppearance_android_shadowDy:I = 0x7

.field public static final TextAppearance_android_shadowRadius:I = 0x8

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textColorHint:I = 0x4

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_textAllCaps:I = 0x9

.field public static final TextHintView:[I

.field public static final TextHintView_hints:I = 0x0

.field public static final TextInputLayout:[I

.field public static final TextInputLayout_android_hint:I = 0x1

.field public static final TextInputLayout_android_textColorHint:I = 0x0

.field public static final TextInputLayout_counterEnabled:I = 0x6

.field public static final TextInputLayout_counterMaxLength:I = 0x7

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0x9

.field public static final TextInputLayout_counterTextAppearance:I = 0x8

.field public static final TextInputLayout_errorEnabled:I = 0x4

.field public static final TextInputLayout_errorTextAppearance:I = 0x5

.field public static final TextInputLayout_hintAnimationEnabled:I = 0xa

.field public static final TextInputLayout_hintEnabled:I = 0x3

.field public static final TextInputLayout_hintTextAppearance:I = 0x2

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0xd

.field public static final TextInputLayout_passwordToggleDrawable:I = 0xc

.field public static final TextInputLayout_passwordToggleEnabled:I = 0xb

.field public static final TextInputLayout_passwordToggleTint:I = 0xe

.field public static final TextInputLayout_passwordToggleTintMode:I = 0xf

.field public static final TextStyle:[I

.field public static final TextStyle_android_ellipsize:I = 0x4

.field public static final TextStyle_android_maxLines:I = 0x5

.field public static final TextStyle_android_shadowColor:I = 0x7

.field public static final TextStyle_android_shadowDx:I = 0x8

.field public static final TextStyle_android_shadowDy:I = 0x9

.field public static final TextStyle_android_shadowRadius:I = 0xa

.field public static final TextStyle_android_singleLine:I = 0x6

.field public static final TextStyle_android_textAppearance:I = 0x0

.field public static final TextStyle_android_textColor:I = 0x3

.field public static final TextStyle_android_textSize:I = 0x1

.field public static final TextStyle_android_textStyle:I = 0x2

.field public static final TitleLeaderboardScreen:[I

.field public static final TitleLeaderboardScreen_textColor:I = 0x0

.field public static final ToggleTypeface:[I

.field public static final ToggleTypeface_initialState:I = 0x2

.field public static final ToggleTypeface_negativeTypeface:I = 0x1

.field public static final ToggleTypeface_positiveTypeface:I = 0x0

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_buttonGravity:I = 0x15

.field public static final Toolbar_collapseContentDescription:I = 0x17

.field public static final Toolbar_collapseIcon:I = 0x16

.field public static final Toolbar_contentInsetEnd:I = 0x6

.field public static final Toolbar_contentInsetEndWithActions:I = 0xa

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x5

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0x9

.field public static final Toolbar_logo:I = 0x4

.field public static final Toolbar_logoDescription:I = 0x1a

.field public static final Toolbar_maxButtonHeight:I = 0x14

.field public static final Toolbar_navigationContentDescription:I = 0x19

.field public static final Toolbar_navigationIcon:I = 0x18

.field public static final Toolbar_popupTheme:I = 0xb

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xd

.field public static final Toolbar_subtitleTextColor:I = 0x1c

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMargin:I = 0xe

.field public static final Toolbar_titleMarginBottom:I = 0x12

.field public static final Toolbar_titleMarginEnd:I = 0x10

.field public static final Toolbar_titleMarginStart:I = 0xf

.field public static final Toolbar_titleMarginTop:I = 0x11

.field public static final Toolbar_titleMargins:I = 0x13

.field public static final Toolbar_titleTextAppearance:I = 0xc

.field public static final Toolbar_titleTextColor:I = 0x1b

.field public static final TwoLineHeaderView:[I

.field public static final TwoLineHeaderView_Behavior_Params:[I

.field public static final TwoLineHeaderView_Behavior_Params_behavior_movingSibling:I = 0x0

.field public static final TwoLineHeaderView_headerText:I = 0x0

.field public static final TwoLineHeaderView_subHeaderText:I = 0x1

.field public static final TwoWayView:[I

.field public static final TwoWayView_android_choiceMode:I = 0x3

.field public static final TwoWayView_android_drawSelectorOnTop:I = 0x2

.field public static final TwoWayView_android_listSelector:I = 0x1

.field public static final TwoWayView_android_orientation:I = 0x0

.field public static final TwoWayView_itemMargin:I = 0x4

.field public static final URCGroupView:[I

.field public static final URCGroupView_urc_group:I = 0x0

.field public static final URCPercentView:[I

.field public static final URCPercentView_Layout:[I

.field public static final URCPercentView_Layout_bottom:I = 0x3

.field public static final URCPercentView_Layout_left:I = 0x0

.field public static final URCPercentView_Layout_right:I = 0x1

.field public static final URCPercentView_Layout_top:I = 0x2

.field public static final URCPercentView_command:I = 0x0

.field public static final URCPowerLayout:[I

.field public static final URCPowerLayout_powerStripLayoutId:I = 0x0

.field public static final URCPowerLayout_willShowSystemOnOff:I = 0x1

.field public static final URCViewControl:[I

.field public static final URCViewControl_urcStatusDisconnectStringId:I = 0x1

.field public static final URCViewControl_urcStatusErrorStringId:I = 0x2

.field public static final URCViewControl_urcStatusFirstRunStringId:I = 0x4

.field public static final URCViewControl_urcStatusFirstRunSubTextStringId:I = 0x5

.field public static final URCViewControl_urcStatusLoadingStringId:I = 0x3

.field public static final URCViewControl_urcViewControlId:I = 0x0

.field public static final VerticalPanScrollContainer:[I

.field public static final VerticalPanScrollContainer_lastVisibleId:I = 0x0

.field public static final VerticalScrollDockLayout:[I

.field public static final VerticalScrollDockLayout_scrollContainer:I = 0x0

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewPagerWithTabs:[I

.field public static final ViewPagerWithTabs_headerText:I = 0x0

.field public static final ViewPagerWithTabs_subHeaderText:I = 0x1

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x3

.field public static final View_paddingStart:I = 0x2

.field public static final View_theme:I = 0x4

.field public static final VirtualGrid:[I

.field public static final VirtualGrid_hasScrollableHeader:I = 0x0

.field public static final WelcomeCard:[I

.field public static final WelcomeCard_buttonText:I = 0x3

.field public static final WelcomeCard_descriptionText:I = 0x2

.field public static final WelcomeCard_iconText:I = 0x0

.field public static final WelcomeCard_titleText:I = 0x1

.field public static final XLECheckBox:[I

.field public static final XLECheckBox_subText:I = 0x3

.field public static final XLECheckBox_subTextStyle:I = 0x4

.field public static final XLECheckBox_subTextTypefaceSource:I = 0x5

.field public static final XLECheckBox_text:I = 0x0

.field public static final XLECheckBox_textStyle:I = 0x1

.field public static final XLECheckBox_textTypefaceSource:I = 0x2

.field public static final XLEConstrainedImageView:[I

.field public static final XLEConstrainedImageView_aspectX:I = 0x0

.field public static final XLEConstrainedImageView_aspectY:I = 0x1

.field public static final XLEConstrainedImageView_fixDimension:I = 0x2

.field public static final XLEConstrainedImageView_maxHeight:I = 0x3

.field public static final XLEConstrainedImageView_maxWidth:I = 0x4

.field public static final XLEIconButton:[I

.field public static final XLEIconButton_android_text:I = 0x0

.field public static final XLEIconButton_buttonIcon:I = 0x1

.field public static final XLEIconButton_iconPressed:I = 0x2

.field public static final XLEIconButton_iconSymbolAdjustForImageSize:I = 0xa

.field public static final XLEIconButton_iconSymbolAspectRatio:I = 0x9

.field public static final XLEIconButton_iconSymbolBackground:I = 0xb

.field public static final XLEIconButton_iconSymbolColor:I = 0x5

.field public static final XLEIconButton_iconSymbolEraseColor:I = 0xc

.field public static final XLEIconButton_iconSymbolSize:I = 0x4

.field public static final XLEIconButton_iconSymbolStyle:I = 0x8

.field public static final XLEIconButton_iconSymbolText:I = 0x3

.field public static final XLEIconButton_iconSymbolTypeface:I = 0x6

.field public static final XLEIconButton_iconSymbolTypefaceSource:I = 0x7

.field public static final XLEIconFontButton:[I

.field public static final XLEIconFontButton_android_text:I = 0x1

.field public static final XLEIconFontButton_android_textSize:I = 0x0

.field public static final XLEIconFontButton_iconTextSize:I = 0x3

.field public static final XLEIconFontButton_typefaceSource:I = 0x2

.field public static final XLEImageViewFast:[I

.field public static final XLEImageViewFast_src:I = 0x0

.field public static final XLENewFuncView:[I

.field public static final XLENewFuncView_content:I = 0x2

.field public static final XLENewFuncView_tile:I = 0x0

.field public static final XLENewFuncView_tittle:I = 0x1

.field public static final XLERibbonView:[I

.field public static final XLERibbonView_depth:I = 0x0

.field public static final XLERibbonView_imageColor:I = 0x1

.field public static final XLERootView:[I

.field public static final XLERootView_activityBody:I = 0x0

.field public static final XLERootView_headerName:I = 0x3

.field public static final XLERootView_isTopLevel:I = 0x2

.field public static final XLERootView_minScreenPercent:I = 0x4

.field public static final XLERootView_showTitleBar:I = 0x1

.field public static final XLEUniformImageView:[I

.field public static final XLEUniformImageView_aspectRatio:I = 0x3

.field public static final XLEUniformImageView_fixDimension:I = 0x0

.field public static final XLEUniformImageView_maxHeight:I = 0x1

.field public static final XLEUniformImageView_maxWidth:I = 0x2

.field public static final XLEUniversalImageView:[I

.field public static final XLEUniversalImageView_adjustForImageSize:I = 0x9

.field public static final XLEUniversalImageView_android_src:I = 0x4

.field public static final XLEUniversalImageView_android_text:I = 0x5

.field public static final XLEUniversalImageView_android_textColor:I = 0x3

.field public static final XLEUniversalImageView_android_textSize:I = 0x0

.field public static final XLEUniversalImageView_android_textStyle:I = 0x2

.field public static final XLEUniversalImageView_android_typeface:I = 0x1

.field public static final XLEUniversalImageView_eraseColor:I = 0xa

.field public static final XLEUniversalImageView_textAspectRatio:I = 0x8

.field public static final XLEUniversalImageView_typefaceSource:I = 0x6

.field public static final XLEUniversalImageView_uri:I = 0x7

.field public static final com_facebook_like_view:[I

.field public static final com_facebook_like_view_com_facebook_auxiliary_view_position:I = 0x4

.field public static final com_facebook_like_view_com_facebook_foreground_color:I = 0x0

.field public static final com_facebook_like_view_com_facebook_horizontal_alignment:I = 0x5

.field public static final com_facebook_like_view_com_facebook_object_id:I = 0x1

.field public static final com_facebook_like_view_com_facebook_object_type:I = 0x2

.field public static final com_facebook_like_view_com_facebook_style:I = 0x3

.field public static final com_facebook_login_view:[I

.field public static final com_facebook_login_view_com_facebook_confirm_logout:I = 0x0

.field public static final com_facebook_login_view_com_facebook_login_text:I = 0x1

.field public static final com_facebook_login_view_com_facebook_logout_text:I = 0x2

.field public static final com_facebook_login_view_com_facebook_tooltip_mode:I = 0x3

.field public static final com_facebook_profile_picture_view:[I

.field public static final com_facebook_profile_picture_view_com_facebook_is_cropped:I = 0x1

.field public static final com_facebook_profile_picture_view_com_facebook_preset_size:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16267
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActionBar:[I

    .line 16673
    new-array v0, v3, [I

    const v1, 0x10100b3

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActionBarLayout:[I

    .line 16692
    new-array v0, v3, [I

    const v1, 0x101013f

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActionMenuItemView:[I

    .line 16703
    new-array v0, v2, [I

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActionMenuView:[I

    .line 16726
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActionMode:[I

    .line 16810
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ActivityChooserView:[I

    .line 16851
    new-array v0, v5, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AdsAttrs:[I

    .line 16918
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AlertDialog:[I

    .line 17006
    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppBarLayout:[I

    .line 17057
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppBarLayoutStates:[I

    .line 17100
    new-array v0, v4, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppBarLayout_Layout:[I

    .line 17145
    new-array v0, v4, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppCompatImageView:[I

    .line 17180
    new-array v0, v6, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppCompatSeekBar:[I

    .line 17257
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppCompatTextHelper:[I

    .line 17315
    new-array v0, v4, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppCompatTextView:[I

    .line 17573
    const/16 v0, 0x73

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AppCompatTheme:[I

    .line 18945
    new-array v0, v3, [I

    const v1, 0x7f0100ec

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ApplicationBar:[I

    .line 18968
    new-array v0, v3, [I

    const v1, 0x7f01003b

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->AspectRatioFrameLayout:[I

    .line 19000
    new-array v0, v3, [I

    const v1, 0x7f0100ed

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->Banner:[I

    .line 19031
    new-array v0, v5, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->BottomSheetBehavior_Layout:[I

    .line 19096
    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->BranchButton:[I

    .line 19123
    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->BranchImageButton:[I

    .line 19150
    new-array v0, v3, [I

    const v1, 0x7f0100f1

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ButtonBarLayout:[I

    .line 19201
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CardView:[I

    .line 19406
    new-array v0, v6, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CirclePageIndicator:[I

    .line 19475
    new-array v0, v3, [I

    const v1, 0x7f010101

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ClubTypeDescriptionView:[I

    .line 19536
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CollapsingToolbarLayout:[I

    .line 19802
    new-array v0, v4, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CollapsingToolbarLayout_Layout:[I

    .line 19851
    new-array v0, v5, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ColorStateListItem:[I

    .line 19890
    new-array v0, v3, [I

    const v1, 0x7f010114

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ComparisonBar:[I

    .line 19921
    new-array v0, v5, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CompoundButton:[I

    .line 19985
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ConnectDialogState:[I

    .line 20179
    const/16 v0, 0x31

    new-array v0, v0, [I

    fill-array-data v0, :array_15

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ConstraintLayout_Layout:[I

    .line 21071
    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_16

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ConstraintSet:[I

    .line 21915
    new-array v0, v4, [I

    fill-array-data v0, :array_17

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CoordinatorLayout:[I

    .line 21960
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CoordinatorLayout_Layout:[I

    .line 22148
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CropImageView:[I

    .line 22614
    new-array v0, v6, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CroppedImageView:[I

    .line 22683
    new-array v0, v3, [I

    const v1, 0x7f010046

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomStyledTextView:[I

    .line 22714
    new-array v0, v5, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    .line 22773
    new-array v0, v5, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->DesignTheme:[I

    .line 22823
    new-array v0, v4, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->DetailsMoreOrLessView:[I

    .line 22870
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->DrawerArrowToggle:[I

    .line 23011
    new-array v0, v4, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->EPGRecentChannelsView:[I

    .line 23068
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->EditViewFixedLength:[I

    .line 23200
    new-array v0, v5, [I

    fill-array-data v0, :array_21

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->FeaturedList:[I

    .line 23252
    new-array v0, v5, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->FixedAspectLayout:[I

    .line 23324
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_23

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->FloatingActionButton:[I

    .line 23468
    new-array v0, v3, [I

    const v1, 0x7f010167

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->FloatingActionButton_Behavior_Layout:[I

    .line 23505
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_24

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->FontStarRatingView:[I

    .line 23596
    new-array v0, v5, [I

    fill-array-data v0, :array_25

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ForegroundLinearLayout:[I

    .line 23683
    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_26

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->GenericDraweeHierarchy:[I

    .line 24086
    new-array v0, v5, [I

    fill-array-data v0, :array_27

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontButton:[I

    .line 24140
    new-array v0, v6, [I

    fill-array-data v0, :array_28

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontRingButton:[I

    .line 24203
    new-array v0, v6, [I

    fill-array-data v0, :array_29

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontToggleButton:[I

    .line 24262
    new-array v0, v4, [I

    fill-array-data v0, :array_2a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LabeledArcView:[I

    .line 24312
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_2b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LeadingIconTextView:[I

    .line 24410
    new-array v0, v6, [I

    fill-array-data v0, :array_2c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LikeControl:[I

    .line 24459
    new-array v0, v3, [I

    const v1, 0x10100c4

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LinearConstraintLayout:[I

    .line 24494
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LinearLayoutCompat:[I

    .line 24604
    new-array v0, v6, [I

    fill-array-data v0, :array_2e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 24643
    new-array v0, v4, [I

    fill-array-data v0, :array_2f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ListPopupWindow:[I

    .line 24672
    new-array v0, v5, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->LoadingImageView:[I

    .line 24731
    new-array v0, v3, [I

    const v1, 0x7f010039

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MaxWidthLinearLayout:[I

    .line 24757
    new-array v0, v3, [I

    const v1, 0x7f0101a1

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MediaButtons:[I

    .line 24784
    new-array v0, v3, [I

    const v1, 0x7f0101a2

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MediaProgressBar:[I

    .line 24821
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_31

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MenuGroup:[I

    .line 24903
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_32

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MenuItem:[I

    .line 25072
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_33

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MenuView:[I

    .line 25165
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_34

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->MixedFontFaceTextView:[I

    .line 25278
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_35

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->NavigationView:[I

    .line 25399
    new-array v0, v4, [I

    fill-array-data v0, :array_36

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->Panorama:[I

    .line 25456
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_37

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PercentLayout_Layout:[I

    .line 25631
    new-array v0, v3, [I

    const v1, 0x7f0101c2

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PieChartView:[I

    .line 25659
    new-array v0, v4, [I

    fill-array-data v0, :array_38

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->Pivot:[I

    .line 25704
    new-array v0, v4, [I

    fill-array-data v0, :array_39

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PivotWithTabs:[I

    .line 25751
    new-array v0, v6, [I

    fill-array-data v0, :array_3a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PlaybackControlView:[I

    .line 25820
    new-array v0, v5, [I

    fill-array-data v0, :array_3b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PopupWindow:[I

    .line 25859
    new-array v0, v3, [I

    const v1, 0x7f0101c6

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PopupWindowBackgroundState:[I

    .line 25888
    new-array v0, v4, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->PrivacySettingView:[I

    .line 25931
    new-array v0, v4, [I

    fill-array-data v0, :array_3d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->RecycleListView:[I

    .line 25986
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->RecyclerView:[I

    .line 26070
    new-array v0, v4, [I

    fill-array-data v0, :array_3f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->RelativeLayoutWithForegroundSelector:[I

    .line 26100
    new-array v0, v3, [I

    const v1, 0x7f0101d0

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ReputationArcView:[I

    .line 26128
    new-array v0, v4, [I

    fill-array-data v0, :array_40

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ScreenLayout:[I

    .line 26168
    new-array v0, v3, [I

    const v1, 0x7f0101d3

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ScrimInsetsFrameLayout:[I

    .line 26193
    new-array v0, v3, [I

    const v1, 0x7f0101d4

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ScrollingViewBehavior_Layout:[I

    .line 26254
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_41

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SearchView:[I

    .line 26441
    new-array v0, v5, [I

    fill-array-data v0, :array_42

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SignInButton:[I

    .line 26505
    new-array v0, v3, [I

    const v1, 0x7f0101e5

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SimpleDraweeView:[I

    .line 26550
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_43

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SimpleExoPlayerView:[I

    .line 26704
    new-array v0, v4, [I

    fill-array-data v0, :array_44

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SimpleGridLayout:[I

    .line 26747
    new-array v0, v6, [I

    fill-array-data v0, :array_45

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SmartGlassControlPickerButton:[I

    .line 26808
    new-array v0, v5, [I

    fill-array-data v0, :array_46

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SnackbarLayout:[I

    .line 26865
    new-array v0, v6, [I

    fill-array-data v0, :array_47

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SocialBarValue:[I

    .line 26930
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->Spinner:[I

    .line 26978
    new-array v0, v3, [I

    const v1, 0x7f0101f3

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->StackingBehavior_Params:[I

    .line 27001
    new-array v0, v3, [I

    const v1, 0x7f0101f4

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->StarRatingView:[I

    .line 27033
    new-array v0, v4, [I

    fill-array-data v0, :array_49

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->StyledTextView:[I

    .line 27100
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_4a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SwitchCompat:[I

    .line 27302
    new-array v0, v3, [I

    const v1, 0x7f010202

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SwitchPanel:[I

    .line 27329
    new-array v0, v3, [I

    const v1, 0x7f010203

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->SwitchPanelItem:[I

    .line 27360
    new-array v0, v5, [I

    fill-array-data v0, :array_4b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TabItem:[I

    .line 27421
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_4c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TabLayout:[I

    .line 27698
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_4d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TextAppearance:[I

    .line 27778
    new-array v0, v3, [I

    const v1, 0x7f010214

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TextHintView:[I

    .line 27832
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_4e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TextInputLayout:[I

    .line 28063
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_4f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TextStyle:[I

    .line 28144
    new-array v0, v3, [I

    const v1, 0x7f010223

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TitleLeaderboardScreen:[I

    .line 28173
    new-array v0, v5, [I

    fill-array-data v0, :array_50

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ToggleTypeface:[I

    .line 28284
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_51

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->Toolbar:[I

    .line 28703
    new-array v0, v4, [I

    fill-array-data v0, :array_52

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoLineHeaderView:[I

    .line 28744
    new-array v0, v3, [I

    const v1, 0x7f010238

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoLineHeaderView_Behavior_Params:[I

    .line 28775
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_53

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoWayView:[I

    .line 28826
    new-array v0, v3, [I

    const v1, 0x7f01023a

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->URCGroupView:[I

    .line 28862
    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->URCPercentView:[I

    .line 28895
    new-array v0, v6, [I

    fill-array-data v0, :array_54

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->URCPercentView_Layout:[I

    .line 28966
    new-array v0, v4, [I

    fill-array-data v0, :array_55

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->URCPowerLayout:[I

    .line 29010
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_56

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->URCViewControl:[I

    .line 29084
    new-array v0, v3, [I

    const v1, 0x7f010247

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->VerticalPanScrollContainer:[I

    .line 29107
    new-array v0, v3, [I

    const v1, 0x7f010248

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->VerticalScrollDockLayout:[I

    .line 29138
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_57

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->View:[I

    .line 29210
    new-array v0, v5, [I

    fill-array-data v0, :array_58

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ViewBackgroundHelper:[I

    .line 29266
    new-array v0, v4, [I

    fill-array-data v0, :array_59

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ViewPagerWithTabs:[I

    .line 29311
    new-array v0, v5, [I

    fill-array-data v0, :array_5a

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->ViewStubCompat:[I

    .line 29342
    new-array v0, v3, [I

    const v1, 0x7f01024e

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->VirtualGrid:[I

    .line 29372
    new-array v0, v6, [I

    fill-array-data v0, :array_5b

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->WelcomeCard:[I

    .line 29451
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_5c

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLECheckBox:[I

    .line 29549
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_5d

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEConstrainedImageView:[I

    .line 29658
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_5e

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEIconButton:[I

    .line 29846
    new-array v0, v6, [I

    fill-array-data v0, :array_5f

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEIconFontButton:[I

    .line 29901
    new-array v0, v3, [I

    const v1, 0x7f010265

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEImageViewFast:[I

    .line 29928
    new-array v0, v5, [I

    fill-array-data v0, :array_60

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLENewFuncView:[I

    .line 29973
    new-array v0, v4, [I

    fill-array-data v0, :array_61

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLERibbonView:[I

    .line 30023
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_62

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLERootView:[I

    .line 30109
    new-array v0, v6, [I

    fill-array-data v0, :array_63

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEUniformImageView:[I

    .line 30199
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_64

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEUniversalImageView:[I

    .line 30331
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_65

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->com_facebook_like_view:[I

    .line 30452
    new-array v0, v6, [I

    fill-array-data v0, :array_66

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->com_facebook_login_view:[I

    .line 30527
    new-array v0, v4, [I

    fill-array-data v0, :array_67

    sput-object v0, Lcom/microsoft/xboxone/smartglass/R$styleable;->com_facebook_profile_picture_view:[I

    return-void

    .line 16267
    nop

    :array_0
    .array-data 4
        0x7f01000b
        0x7f010043
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f0100aa
    .end array-data

    .line 16726
    :array_1
    .array-data 4
        0x7f01000b
        0x7f01004e
        0x7f01004f
        0x7f010053
        0x7f010055
        0x7f010065
    .end array-data

    .line 16810
    :array_2
    .array-data 4
        0x7f010066
        0x7f010067
    .end array-data

    .line 16851
    :array_3
    .array-data 4
        0x7f010068
        0x7f010069
        0x7f01006a
    .end array-data

    .line 16918
    :array_4
    .array-data 4
        0x10100f2
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
        0x7f010070
    .end array-data

    .line 17006
    :array_5
    .array-data 4
        0x10100d4
        0x7f010063
        0x7f010071
    .end array-data

    .line 17057
    :array_6
    .array-data 4
        0x7f010072
        0x7f010073
    .end array-data

    .line 17100
    :array_7
    .array-data 4
        0x7f010074
        0x7f010075
    .end array-data

    .line 17145
    :array_8
    .array-data 4
        0x1010119
        0x7f010076
    .end array-data

    .line 17180
    :array_9
    .array-data 4
        0x1010142
        0x7f010077
        0x7f010078
        0x7f010079
    .end array-data

    .line 17257
    :array_a
    .array-data 4
        0x1010034
        0x101016d
        0x101016e
        0x101016f
        0x1010170
        0x1010392
        0x1010393
    .end array-data

    .line 17315
    :array_b
    .array-data 4
        0x1010034
        0x7f01007a
    .end array-data

    .line 17573
    :array_c
    .array-data 4
        0x1010057
        0x10100ae
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
        0x7f0100b1
        0x7f0100b2
        0x7f0100b3
        0x7f0100b4
        0x7f0100b5
        0x7f0100b6
        0x7f0100b7
        0x7f0100b8
        0x7f0100b9
        0x7f0100ba
        0x7f0100bb
        0x7f0100bc
        0x7f0100bd
        0x7f0100be
        0x7f0100bf
        0x7f0100c0
        0x7f0100c1
        0x7f0100c2
        0x7f0100c3
        0x7f0100c4
        0x7f0100c5
        0x7f0100c6
        0x7f0100c7
        0x7f0100c8
        0x7f0100c9
        0x7f0100ca
        0x7f0100cb
        0x7f0100cc
        0x7f0100cd
        0x7f0100ce
        0x7f0100cf
        0x7f0100d0
        0x7f0100d1
        0x7f0100d2
        0x7f0100d3
        0x7f0100d4
        0x7f0100d5
        0x7f0100d6
        0x7f0100d7
        0x7f0100d8
        0x7f0100d9
        0x7f0100da
        0x7f0100db
        0x7f0100dc
        0x7f0100dd
        0x7f0100de
        0x7f0100df
        0x7f0100e0
        0x7f0100e1
        0x7f0100e2
        0x7f0100e3
        0x7f0100e4
        0x7f0100e5
        0x7f0100e6
        0x7f0100e7
        0x7f0100e8
        0x7f0100e9
        0x7f0100ea
        0x7f0100eb
    .end array-data

    .line 19031
    :array_d
    .array-data 4
        0x7f0100ee
        0x7f0100ef
        0x7f0100f0
    .end array-data

    .line 19201
    :array_e
    .array-data 4
        0x101013f
        0x1010140
        0x7f0100f2
        0x7f0100f3
        0x7f0100f4
        0x7f0100f5
        0x7f0100f6
        0x7f0100f7
        0x7f0100f8
        0x7f0100f9
        0x7f0100fa
        0x7f0100fb
        0x7f0100fc
    .end array-data

    .line 19406
    :array_f
    .array-data 4
        0x7f0100fd
        0x7f0100fe
        0x7f0100ff
        0x7f010100
    .end array-data

    .line 19536
    :array_10
    .array-data 4
        0x7f010043
        0x7f010102
        0x7f010103
        0x7f010104
        0x7f010105
        0x7f010106
        0x7f010107
        0x7f010108
        0x7f010109
        0x7f01010a
        0x7f01010b
        0x7f01010c
        0x7f01010d
        0x7f01010e
        0x7f01010f
        0x7f010110
    .end array-data

    .line 19802
    :array_11
    .array-data 4
        0x7f010111
        0x7f010112
    .end array-data

    .line 19851
    :array_12
    .array-data 4
        0x10101a5
        0x101031f
        0x7f010113
    .end array-data

    .line 19921
    :array_13
    .array-data 4
        0x1010107
        0x7f010115
        0x7f010116
    .end array-data

    .line 19985
    :array_14
    .array-data 4
        0x7f010117
        0x7f010118
        0x7f010119
        0x7f01011a
        0x7f01011b
        0x7f01011c
    .end array-data

    .line 20179
    :array_15
    .array-data 4
        0x10100c4
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x7f010003
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
    .end array-data

    .line 21071
    :array_16
    .array-data 4
        0x10100c4
        0x10100d0
        0x10100dc
        0x10100f4
        0x10100f5
        0x10100f7
        0x10100f8
        0x10100f9
        0x10100fa
        0x101031f
        0x1010320
        0x1010321
        0x1010322
        0x1010323
        0x1010324
        0x1010325
        0x1010327
        0x1010328
        0x10103b5
        0x10103b6
        0x10103fa
        0x1010440
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
    .end array-data

    .line 21915
    :array_17
    .array-data 4
        0x7f01011d
        0x7f01011e
    .end array-data

    .line 21960
    :array_18
    .array-data 4
        0x10100b3
        0x7f01011f
        0x7f010120
        0x7f010121
        0x7f010122
        0x7f010123
        0x7f010124
    .end array-data

    .line 22148
    :array_19
    .array-data 4
        0x7f010125
        0x7f010126
        0x7f010127
        0x7f010128
        0x7f010129
        0x7f01012a
        0x7f01012b
        0x7f01012c
        0x7f01012d
        0x7f01012e
        0x7f01012f
        0x7f010130
        0x7f010131
        0x7f010132
        0x7f010133
        0x7f010134
        0x7f010135
        0x7f010136
        0x7f010137
        0x7f010138
        0x7f010139
        0x7f01013a
        0x7f01013b
        0x7f01013c
        0x7f01013d
        0x7f01013e
        0x7f01013f
        0x7f010140
        0x7f010141
    .end array-data

    .line 22614
    :array_1a
    .array-data 4
        0x7f010142
        0x7f010143
        0x7f010144
        0x7f010145
    .end array-data

    .line 22714
    :array_1b
    .array-data 4
        0x7f010046
        0x7f010146
        0x7f010147
    .end array-data

    .line 22773
    :array_1c
    .array-data 4
        0x7f010148
        0x7f010149
        0x7f01014a
    .end array-data

    .line 22823
    :array_1d
    .array-data 4
        0x1010098
        0x7f01014b
    .end array-data

    .line 22870
    :array_1e
    .array-data 4
        0x7f01014c
        0x7f01014d
        0x7f01014e
        0x7f01014f
        0x7f010150
        0x7f010151
        0x7f010152
        0x7f010153
    .end array-data

    .line 23011
    :array_1f
    .array-data 4
        0x7f010154
        0x7f010155
    .end array-data

    .line 23068
    :array_20
    .array-data 4
        0x7f010156
        0x7f010157
        0x7f010158
        0x7f010159
        0x7f01015a
        0x7f01015b
        0x7f01015c
        0x7f01015d
        0x7f01015e
    .end array-data

    .line 23200
    :array_21
    .array-data 4
        0x7f01015f
        0x7f010160
        0x7f010161
    .end array-data

    .line 23252
    :array_22
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010007
    .end array-data

    .line 23324
    :array_23
    .array-data 4
        0x7f010063
        0x7f010162
        0x7f010163
        0x7f010164
        0x7f010165
        0x7f010166
        0x7f01024c
        0x7f01024d
    .end array-data

    .line 23505
    :array_24
    .array-data 4
        0x7f010168
        0x7f010169
        0x7f01016a
        0x7f01016b
        0x7f01016c
        0x7f01016d
    .end array-data

    .line 23596
    :array_25
    .array-data 4
        0x1010109
        0x1010200
        0x7f01016e
    .end array-data

    .line 23683
    :array_26
    .array-data 4
        0x7f01016f
        0x7f010170
        0x7f010171
        0x7f010172
        0x7f010173
        0x7f010174
        0x7f010175
        0x7f010176
        0x7f010177
        0x7f010178
        0x7f010179
        0x7f01017a
        0x7f01017b
        0x7f01017c
        0x7f01017d
        0x7f01017e
        0x7f01017f
        0x7f010180
        0x7f010181
        0x7f010182
        0x7f010183
        0x7f010184
        0x7f010185
        0x7f010186
        0x7f010187
    .end array-data

    .line 24086
    :array_27
    .array-data 4
        0x7f010188
        0x7f010189
        0x7f01018a
    .end array-data

    .line 24140
    :array_28
    .array-data 4
        0x7f01018b
        0x7f01018c
        0x7f01018d
        0x7f01018e
    .end array-data

    .line 24203
    :array_29
    .array-data 4
        0x7f01018f
        0x7f010190
        0x7f010191
        0x7f010192
    .end array-data

    .line 24262
    :array_2a
    .array-data 4
        0x7f010193
        0x7f010194
    .end array-data

    .line 24312
    :array_2b
    .array-data 4
        0x1010095
        0x101014f
        0x7f010046
        0x7f010195
        0x7f010196
        0x7f010197
        0x7f010198
    .end array-data

    .line 24410
    :array_2c
    .array-data 4
        0x1010095
        0x1010098
        0x7f010199
        0x7f01019a
    .end array-data

    .line 24494
    :array_2d
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f010052
        0x7f01019b
        0x7f01019c
        0x7f01019d
    .end array-data

    .line 24604
    :array_2e
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    .line 24643
    :array_2f
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    .line 24672
    :array_30
    .array-data 4
        0x7f01019e
        0x7f01019f
        0x7f0101a0
    .end array-data

    .line 24821
    :array_31
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    .line 24903
    :array_32
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f0101a3
        0x7f0101a4
        0x7f0101a5
        0x7f0101a6
    .end array-data

    .line 25072
    :array_33
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f0101a7
        0x7f0101a8
    .end array-data

    .line 25165
    :array_34
    .array-data 4
        0x7f0101a9
        0x7f0101aa
        0x7f0101ab
        0x7f0101ac
        0x7f0101ad
        0x7f0101ae
        0x7f0101af
    .end array-data

    .line 25278
    :array_35
    .array-data 4
        0x10100d4
        0x10100dd
        0x101011f
        0x7f010063
        0x7f0101b0
        0x7f0101b1
        0x7f0101b2
        0x7f0101b3
        0x7f0101b4
        0x7f0101b5
    .end array-data

    .line 25399
    :array_36
    .array-data 4
        0x7f0101b6
        0x7f0101b7
    .end array-data

    .line 25456
    :array_37
    .array-data 4
        0x7f0101b8
        0x7f0101b9
        0x7f0101ba
        0x7f0101bb
        0x7f0101bc
        0x7f0101bd
        0x7f0101be
        0x7f0101bf
        0x7f0101c0
        0x7f0101c1
    .end array-data

    .line 25659
    :array_38
    .array-data 4
        0x7f0101c3
        0x7f0101c4
    .end array-data

    .line 25704
    :array_39
    .array-data 4
        0x7f010008
        0x7f01003e
    .end array-data

    .line 25751
    :array_3a
    .array-data 4
        0x7f010004
        0x7f010006
        0x7f01003c
        0x7f01003d
    .end array-data

    .line 25820
    :array_3b
    .array-data 4
        0x1010176
        0x10102c9
        0x7f0101c5
    .end array-data

    .line 25888
    :array_3c
    .array-data 4
        0x7f0101c7
        0x7f0101c8
    .end array-data

    .line 25931
    :array_3d
    .array-data 4
        0x7f0101c9
        0x7f0101ca
    .end array-data

    .line 25986
    :array_3e
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f0101cb
        0x7f0101cc
        0x7f0101cd
        0x7f0101ce
    .end array-data

    .line 26070
    :array_3f
    .array-data 4
        0x1010109
        0x7f0101cf
    .end array-data

    .line 26128
    :array_40
    .array-data 4
        0x7f0101d1
        0x7f0101d2
    .end array-data

    .line 26254
    :array_41
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0101d5
        0x7f0101d6
        0x7f0101d7
        0x7f0101d8
        0x7f0101d9
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
    .end array-data

    .line 26441
    :array_42
    .array-data 4
        0x7f0101e2
        0x7f0101e3
        0x7f0101e4
    .end array-data

    .line 26550
    :array_43
    .array-data 4
        0x7f010004
        0x7f010006
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f010041
        0x7f0101e6
        0x7f0101e7
        0x7f0101e8
    .end array-data

    .line 26704
    :array_44
    .array-data 4
        0x7f0101e9
        0x7f0101ea
    .end array-data

    .line 26747
    :array_45
    .array-data 4
        0x7f010042
        0x7f0101eb
        0x7f0101ec
        0x7f0101ed
    .end array-data

    .line 26808
    :array_46
    .array-data 4
        0x101011f
        0x7f010063
        0x7f0101ee
    .end array-data

    .line 26865
    :array_47
    .array-data 4
        0x7f0101ef
        0x7f0101f0
        0x7f0101f1
        0x7f0101f2
    .end array-data

    .line 26930
    :array_48
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f010064
    .end array-data

    .line 27033
    :array_49
    .array-data 4
        0x7f0101f5
        0x7f0101f6
    .end array-data

    .line 27100
    :array_4a
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f0101f7
        0x7f0101f8
        0x7f0101f9
        0x7f0101fa
        0x7f0101fb
        0x7f0101fc
        0x7f0101fd
        0x7f0101fe
        0x7f0101ff
        0x7f010200
        0x7f010201
    .end array-data

    .line 27360
    :array_4b
    .array-data 4
        0x1010002
        0x10100f2
        0x101014f
    .end array-data

    .line 27421
    :array_4c
    .array-data 4
        0x7f010204
        0x7f010205
        0x7f010206
        0x7f010207
        0x7f010208
        0x7f010209
        0x7f01020a
        0x7f01020b
        0x7f01020c
        0x7f01020d
        0x7f01020e
        0x7f01020f
        0x7f010210
        0x7f010211
        0x7f010212
        0x7f010213
    .end array-data

    .line 27698
    :array_4d
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f01007a
    .end array-data

    .line 27832
    :array_4e
    .array-data 4
        0x101009a
        0x1010150
        0x7f010215
        0x7f010216
        0x7f010217
        0x7f010218
        0x7f010219
        0x7f01021a
        0x7f01021b
        0x7f01021c
        0x7f01021d
        0x7f01021e
        0x7f01021f
        0x7f010220
        0x7f010221
        0x7f010222
    .end array-data

    .line 28063
    :array_4f
    .array-data 4
        0x1010034
        0x1010095
        0x1010097
        0x1010098
        0x10100ab
        0x1010153
        0x101015d
        0x1010161
        0x1010162
        0x1010163
        0x1010164
    .end array-data

    .line 28173
    :array_50
    .array-data 4
        0x7f010224
        0x7f010225
        0x7f010226
    .end array-data

    .line 28284
    :array_51
    .array-data 4
        0x10100af
        0x1010140
        0x7f010043
        0x7f01004d
        0x7f010051
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010064
        0x7f010227
        0x7f010228
        0x7f010229
        0x7f01022a
        0x7f01022b
        0x7f01022c
        0x7f01022d
        0x7f01022e
        0x7f01022f
        0x7f010230
        0x7f010231
        0x7f010232
        0x7f010233
        0x7f010234
        0x7f010235
        0x7f010236
        0x7f010237
    .end array-data

    .line 28703
    :array_52
    .array-data 4
        0x7f010008
        0x7f01003e
    .end array-data

    .line 28775
    :array_53
    .array-data 4
        0x10100c4
        0x10100fb
        0x10100fc
        0x101012b
        0x7f010239
    .end array-data

    .line 28895
    :array_54
    .array-data 4
        0x7f01023b
        0x7f01023c
        0x7f01023d
        0x7f01023e
    .end array-data

    .line 28966
    :array_55
    .array-data 4
        0x7f01023f
        0x7f010240
    .end array-data

    .line 29010
    :array_56
    .array-data 4
        0x7f010241
        0x7f010242
        0x7f010243
        0x7f010244
        0x7f010245
        0x7f010246
    .end array-data

    .line 29138
    :array_57
    .array-data 4
        0x1010000
        0x10100da
        0x7f010249
        0x7f01024a
        0x7f01024b
    .end array-data

    .line 29210
    :array_58
    .array-data 4
        0x10100d4
        0x7f01024c
        0x7f01024d
    .end array-data

    .line 29266
    :array_59
    .array-data 4
        0x7f010008
        0x7f01003e
    .end array-data

    .line 29311
    :array_5a
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data

    .line 29372
    :array_5b
    .array-data 4
        0x7f01024f
        0x7f010250
        0x7f010251
        0x7f010252
    .end array-data

    .line 29451
    :array_5c
    .array-data 4
        0x7f010042
        0x7f010253
        0x7f010254
        0x7f010255
        0x7f010256
        0x7f010257
    .end array-data

    .line 29549
    :array_5d
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010007
        0x7f010038
        0x7f010039
    .end array-data

    .line 29658
    :array_5e
    .array-data 4
        0x101014f
        0x7f010258
        0x7f010259
        0x7f01025a
        0x7f01025b
        0x7f01025c
        0x7f01025d
        0x7f01025e
        0x7f01025f
        0x7f010260
        0x7f010261
        0x7f010262
        0x7f010263
    .end array-data

    .line 29846
    :array_5f
    .array-data 4
        0x1010095
        0x101014f
        0x7f010046
        0x7f010264
    .end array-data

    .line 29928
    :array_60
    .array-data 4
        0x7f010266
        0x7f010267
        0x7f010268
    .end array-data

    .line 29973
    :array_61
    .array-data 4
        0x7f010269
        0x7f01026a
    .end array-data

    .line 30023
    :array_62
    .array-data 4
        0x7f01026b
        0x7f01026c
        0x7f01026d
        0x7f01026e
        0x7f01026f
    .end array-data

    .line 30109
    :array_63
    .array-data 4
        0x7f010007
        0x7f010038
        0x7f010039
        0x7f010270
    .end array-data

    .line 30199
    :array_64
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x1010119
        0x101014f
        0x7f010046
        0x7f010271
        0x7f010272
        0x7f010273
        0x7f010274
    .end array-data

    .line 30331
    :array_65
    .array-data 4
        0x7f010275
        0x7f010276
        0x7f010277
        0x7f010278
        0x7f010279
        0x7f01027a
    .end array-data

    .line 30452
    :array_66
    .array-data 4
        0x7f01027b
        0x7f01027c
        0x7f01027d
        0x7f01027e
    .end array-data

    .line 30527
    :array_67
    .array-data 4
        0x7f01027f
        0x7f010280
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
