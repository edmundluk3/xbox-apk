.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubPivotScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile isShowingAdminTab:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 28
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 30
    return-void
.end method

.method private updateAdminTabIfNecessary(Z)V
    .locals 1
    .param p1, "shouldShow"    # Z

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->isShowingAdminTab:Z

    if-eq p1, v0, :cond_0

    .line 88
    if-eqz p1, :cond_1

    .line 89
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->addScreenToPivot(Ljava/lang/Class;)V

    .line 94
    :goto_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->isShowingAdminTab:Z

    .line 96
    :cond_0
    return-void

    .line 91
    :cond_1
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    goto :goto_0
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public loadOverride(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 53
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 57
    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 84
    :goto_0
    return-void

    .line 61
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 62
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 64
    .local v1, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v4, v5}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 66
    .local v2, "shouldShowAdminTab":Z
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->updateAdminTabIfNecessary(Z)V

    .line 71
    .end local v2    # "shouldShowAdminTab":Z
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->parameters:Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;->getAndResetStartPivot()Ljava/lang/Class;

    move-result-object v3

    .line 72
    .local v3, "startPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    if-eqz v3, :cond_1

    .line 73
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->switchToPivot(Ljava/lang/Class;)V

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 62
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .end local v3    # "startPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 81
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "Club model changed to invalid state. Using old club data."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 35
    return-void
.end method

.method public onSetInactive()V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->getIsActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetInactive()V

    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 43
    :cond_0
    return-void
.end method
