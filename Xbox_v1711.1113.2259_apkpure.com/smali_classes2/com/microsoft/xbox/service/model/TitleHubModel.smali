.class public Lcom/microsoft/xbox/service/model/TitleHubModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;,
        Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;,
        Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_MODEL_COUNT:I = 0xff

.field private static final TAG:Ljava/lang/String;

.field private static titleHubModel:Lcom/microsoft/xbox/service/model/TitleHubModel;


# instance fields
.field private batchLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private final titles:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;",
            ">;"
        }
    .end annotation
.end field

.field private userTitleFollowingModel:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleHubModel;->TAG:Ljava/lang/String;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titleHubModel:Lcom/microsoft/xbox/service/model/TitleHubModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->userTitleFollowingModel:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->batchLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 44
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titles:Landroid/util/LruCache;

    .line 45
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/model/TitleHubModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    .locals 5
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 48
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 50
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titles:Landroid/util/LruCache;

    monitor-enter v3

    .line 51
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titles:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    .line 52
    .local v0, "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    .end local v0    # "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;-><init>(J)V

    .line 54
    .restart local v0    # "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titles:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    monitor-exit v3

    move-object v1, v0

    .line 58
    .end local v0    # "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    .local v1, "titleHub":Ljava/lang/Object;
    :goto_0
    return-object v1

    .end local v1    # "titleHub":Ljava/lang/Object;
    .restart local v0    # "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    :cond_0
    monitor-exit v3

    move-object v1, v0

    .restart local v1    # "titleHub":Ljava/lang/Object;
    goto :goto_0

    .line 59
    .end local v0    # "titleHub":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    .end local v1    # "titleHub":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static instance()Lcom/microsoft/xbox/service/model/TitleHubModel;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/service/model/TitleHubModel;->titleHubModel:Lcom/microsoft/xbox/service/model/TitleHubModel;

    return-object v0
.end method


# virtual methods
.method public getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 3
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->getResult()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    return-object v0
.end method

.method public getUserTitleFollowingModel()Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->userTitleFollowingModel:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    return-object v0
.end method

.method public isLoading(J)Z
    .locals 3
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 78
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 94
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public load(ZLjava/util/Set;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 11
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 98
    .local p2, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz p1, :cond_1

    .line 100
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleHubModel;->batchLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;

    invoke-direct {v6, p0, p2}, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleHubModel;Ljava/util/Set;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 122
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :cond_1
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 104
    .local v7, "staleTitleIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v10, "upToDateTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    .line 106
    .local v9, "titleId":Ljava/lang/Long;
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v8

    .line 107
    .local v8, "title":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->shouldRefresh()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    invoke-virtual {v7, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 110
    :cond_2
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->getResult()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    .end local v8    # "title":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    .end local v9    # "titleId":Ljava/lang/Long;
    :cond_3
    invoke-virtual {v7}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 115
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;

    invoke-direct {v6, p0, v7}, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleHubModel;Ljava/util/Set;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 116
    .local v0, "loadResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 122
    .end local v0    # "loadResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, v10, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method public loadAsync(ZJ)V
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 83
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 84
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->loadAsync(Z)V

    .line 85
    return-void
.end method

.method protected onTitleHubUpdate(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 133
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->TitleDataLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    .line 134
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    .line 133
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 135
    return-void
.end method

.method public rxLoad(ZJ)Lio/reactivex/Single;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 89
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->rxLoad(Z)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public shouldRefresh(J)Z
    .locals 3
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 73
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method public updateTitleHubBatch(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 328
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 329
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 330
    .local v0, "data":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    iget-wide v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 331
    iget-wide v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getTitleHub(J)Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    move-result-object v1

    .line 332
    .local v1, "title":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    new-instance v3, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v4, 0x0

    invoke-direct {v3, v0, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 336
    .end local v0    # "data":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .end local v1    # "title":Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
    :cond_1
    return-void
.end method
