.class public Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
.super Ljava/lang/Object;
.source "SocialActionsSummariesContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SocialActionsSummaries"
.end annotation


# instance fields
.field public summaries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    return-object v0
.end method
