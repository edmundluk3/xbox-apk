.class public abstract Lcom/microsoft/xbox/presentation/base/MviActivityBase;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "MviActivityBase.java"


# instance fields
.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected abstract getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->onCreateContentView()V

    .line 51
    return-void
.end method

.method public onCreateContentView()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->setContentView()V

    .line 57
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->unbinder:Lbutterknife/Unbinder;

    .line 58
    return-void
.end method

.method public onDestroy()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 80
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 81
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;->onDestroy(Ljava/lang/Object;)V

    .line 82
    return-void
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onPause()V

    .line 101
    return-void
.end method

.method public final onRehydrate()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onRehydrate()V

    .line 106
    return-void
.end method

.method public final onRehydrateOverride()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onRehydrateOverride()V

    .line 111
    return-void
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onResume()V

    .line 96
    return-void
.end method

.method public onStart()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 64
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;->onStart(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public onStop()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStop()V

    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;->onStop(Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public final onTombstone()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onTombstone()V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 91
    return-void
.end method

.method protected abstract setContentView()V
.end method
