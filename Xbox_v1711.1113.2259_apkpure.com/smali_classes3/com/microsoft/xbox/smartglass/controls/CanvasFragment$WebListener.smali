.class Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;
.super Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
.source "CanvasFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$1;

    .prologue
    .line 346
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V

    return-void
.end method


# virtual methods
.method public onLoadCompleted()V
    .locals 3

    .prologue
    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$200(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 364
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onLoadCompleted()V

    goto :goto_0

    .line 366
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    return-void
.end method

.method public onNavigating(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$200(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 350
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onNavigating(Ljava/lang/String;)V

    goto :goto_0

    .line 352
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    return-void
.end method

.method public onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "failingUrl"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 356
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$200(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 357
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 359
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    return-void
.end method

.method public onReady()V
    .locals 3

    .prologue
    .line 370
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$300(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 371
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$400(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)V

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment$WebListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->access$200(Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 375
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onReady()V

    goto :goto_0

    .line 377
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_1
    return-void
.end method
