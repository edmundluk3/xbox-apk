.class Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;
.super Ljava/lang/Object;
.source "DetailsMoreOrLessView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->handleMoreAndLessClickEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getCollapsedLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 145
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 139
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 135
    return-void
.end method
