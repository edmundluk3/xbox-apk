.class public Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TvRecentChannelsScreenAdapter.java"


# instance fields
.field protected mDiagTag:Ljava/lang/String;

.field protected mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

.field protected mInPageProgressBar:Landroid/widget/RelativeLayout;

.field protected final mInPageProgressBarExpandedHeight:I

.field protected mSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 30
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBarExpandedHeight:I

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    .line 36
    const v0, 0x7f0e0b3c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->screenBody:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0b3d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 38
    const-string v0, "TvRecentChannelsScreenAdapter"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mDiagTag:Ljava/lang/String;

    .line 40
    const v0, 0x7f0e058b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->setEPGRecentChannelsView(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;)V

    .line 41
    return-void
.end method


# virtual methods
.method public get(I)Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 120
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    .line 121
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/epg/EPGChannel;>;"
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 122
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 124
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEPGRecentChannelsView(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setModel(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 47
    .local v0, "parent":Landroid/view/ViewGroup;
    const v1, 0x7f0e057f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    .line 48
    return-void
.end method

.method public size()I
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    .line 112
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 115
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected updateLoadingIndicator(Z)V
    .locals 2
    .param p1, "isLoading"    # Z

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->isActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 61
    if-eqz p1, :cond_1

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBarExpandedHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 67
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 69
    :cond_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mInPageProgressBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public updateViewLoadingIndicator()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->isBusy()Z

    move-result v0

    .line 52
    .local v0, "isBusy":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v4, v5, :cond_0

    move v1, v2

    .line 53
    .local v1, "isLoadingState":Z
    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 54
    .local v2, "show":Z
    :goto_1
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 55
    return-void

    .end local v1    # "isLoadingState":Z
    .end local v2    # "show":Z
    :cond_0
    move v1, v3

    .line 52
    goto :goto_0

    .restart local v1    # "isLoadingState":Z
    :cond_1
    move v2, v3

    .line 53
    goto :goto_1
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->updateViewLoadingIndicator()V

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    if-eqz v2, :cond_0

    .line 78
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 81
    const/4 v1, 0x1

    .line 101
    .local v1, "panelIndex":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 102
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mDiagTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Switch to view "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .end local v1    # "panelIndex":I
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mEpgView:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->setModel(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V

    .line 108
    :cond_1
    return-void

    .line 84
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v2, v3, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v2, v3, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 85
    .local v0, "disconnect":Z
    :goto_1
    if-eqz v0, :cond_4

    .line 87
    const/4 v1, 0x2

    .restart local v1    # "panelIndex":I
    goto :goto_0

    .line 84
    .end local v0    # "disconnect":Z
    .end local v1    # "panelIndex":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 90
    .restart local v0    # "disconnect":Z
    :cond_4
    const/4 v1, 0x4

    .line 92
    .restart local v1    # "panelIndex":I
    goto :goto_0

    .line 94
    .end local v0    # "disconnect":Z
    .end local v1    # "panelIndex":I
    :pswitch_1
    const/4 v1, 0x0

    .line 95
    .restart local v1    # "panelIndex":I
    goto :goto_0

    .line 97
    .end local v1    # "panelIndex":I
    :pswitch_2
    const/4 v1, 0x3

    .restart local v1    # "panelIndex":I
    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
