.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PeopleHubAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AchievementsItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        ">;"
    }
.end annotation


# instance fields
.field achievementsStatusIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0906
    .end annotation
.end field

.field achievementsStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0907
    .end annotation
.end field

.field comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e090a
    .end annotation
.end field

.field comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e090b
    .end annotation
.end field

.field gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0902
    .end annotation
.end field

.field gamerscoreIconView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0904
    .end annotation
.end field

.field gamerscorePercentProgressTextViewMe:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0908
    .end annotation
.end field

.field gamerscorePercentProgressTextViewProfile:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0909
    .end annotation
.end field

.field gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0905
    .end annotation
.end field

.field imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0901
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .line 513
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 514
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 515
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;Landroid/util/Pair;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;
    .param p1, "item"    # Landroid/util/Pair;
    .param p2, "ignore"    # Landroid/view/View;

    .prologue
    .line 642
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->navigateToGameProgressPivotOrToComparison(Landroid/util/Pair;)V

    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V
    .locals 22
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 519
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 520
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementItem()Landroid/util/Pair;

    move-result-object v8

    .line 521
    .local v8, "item":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 523
    iget-object v9, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 524
    .local v9, "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    iget-object v11, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 526
    .local v11, "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    const-string v16, "Both items cannot be null"

    if-nez v9, :cond_c

    if-nez v11, :cond_c

    const/4 v15, 0x1

    :goto_0
    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Ljava/lang/String;Z)V

    .line 528
    if-nez v9, :cond_0

    if-eqz v11, :cond_b

    .line 529
    :cond_0
    if-eqz v9, :cond_1

    if-eqz v11, :cond_1

    iget-object v15, v9, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    iget-object v0, v11, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_d

    :cond_1
    const/4 v15, 0x1

    :goto_1
    invoke-static {v15}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 531
    if-eqz v9, :cond_e

    move-object v7, v9

    .line 536
    .local v7, "infoItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :goto_2
    const/4 v3, 0x0

    .local v3, "completionPercentageMe":I
    const/4 v4, 0x0

    .line 539
    .local v4, "completionPercentageProfile":I
    iget-object v15, v7, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    sget-object v16, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->Xbox360:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_11

    move-object v6, v7

    .line 540
    check-cast v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .line 541
    .local v6, "info":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    iget-object v12, v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    .line 542
    .local v12, "title":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v5

    .line 543
    .local v5, "currentGamerscore":I
    iget v13, v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalGamerscore:I

    .line 544
    .local v13, "totalGamerscore":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v15

    if-eqz v15, :cond_f

    .line 545
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v4

    .line 554
    .end local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .end local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_2
    :goto_3
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->currentAchievements:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalAchievements:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 556
    .local v2, "achievementString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    if-eqz v15, :cond_3

    .line 557
    const-string v15, "http://tiles.xbox.com/consoleAssets/%s/%s/largeboxart.jpg"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget v0, v6, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleId:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 558
    .local v14, "url":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    const v16, 0x7f020121

    const v17, 0x7f020123

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 591
    .end local v6    # "info":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    .end local v14    # "url":Ljava/lang/String;
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v15, :cond_4

    .line 592
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v15, v12}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 594
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v15, :cond_5

    .line 595
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 596
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v16

    sget-object v17, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v18, 0x7f070d71

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v16 .. v18}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 598
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewMe:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v15

    if-nez v15, :cond_6

    if-ltz v3, :cond_6

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewMe:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v18

    const-string v19, "%d"

    const/4 v15, 0x1

    new-array v0, v15, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v15, 0x64

    if-le v3, v15, :cond_17

    const/16 v15, 0x64

    :goto_5
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v20, v21

    invoke-static/range {v18 .. v20}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, "%"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewProfile:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v15, :cond_7

    if-ltz v4, :cond_7

    .line 602
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewProfile:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v18

    const-string v19, "%d"

    const/4 v15, 0x1

    new-array v0, v15, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v15, 0x64

    if-le v4, v15, :cond_18

    const/16 v15, 0x64

    :goto_6
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v20, v21

    invoke-static/range {v18 .. v20}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, "%"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v15, :cond_8

    .line 605
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v15, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 609
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v15

    if-nez v15, :cond_19

    .line 610
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 611
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 612
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 613
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 614
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewProfile:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 615
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewMe:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 616
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 627
    :goto_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v15

    if-nez v15, :cond_9

    .line 628
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    int-to-long v0, v3

    move-wide/from16 v16, v0

    rsub-int/lit8 v18, v3, 0x64

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-virtual/range {v15 .. v19}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 629
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 630
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 631
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v16, 0x3f000000    # 0.5f

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 634
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v15, :cond_a

    .line 635
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    int-to-long v0, v4

    move-wide/from16 v16, v0

    rsub-int/lit8 v18, v4, 0x64

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-virtual/range {v15 .. v19}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 636
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getProfileColor()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 637
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getProfileColor()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 638
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarProfile:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v16, 0x3f000000    # 0.5f

    invoke-virtual/range {v15 .. v16}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 641
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;Landroid/util/Pair;)Landroid/view/View$OnClickListener;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    .end local v2    # "achievementString":Ljava/lang/String;
    .end local v3    # "completionPercentageMe":I
    .end local v4    # "completionPercentageProfile":I
    .end local v5    # "currentGamerscore":I
    .end local v7    # "infoItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .end local v12    # "title":Ljava/lang/String;
    .end local v13    # "totalGamerscore":I
    :cond_b
    return-void

    .line 526
    .restart local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .restart local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 529
    :cond_d
    const/4 v15, 0x0

    goto/16 :goto_1

    :cond_e
    move-object v7, v11

    .line 531
    goto/16 :goto_2

    .line 547
    .restart local v3    # "completionPercentageMe":I
    .restart local v4    # "completionPercentageProfile":I
    .restart local v5    # "currentGamerscore":I
    .restart local v6    # "info":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    .restart local v7    # "infoItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .restart local v12    # "title":Ljava/lang/String;
    .restart local v13    # "totalGamerscore":I
    :cond_f
    if-eqz v9, :cond_10

    .line 548
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .end local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v3

    .line 550
    :cond_10
    if-eqz v11, :cond_2

    .line 551
    check-cast v11, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .end local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v4

    goto/16 :goto_3

    .end local v5    # "currentGamerscore":I
    .end local v6    # "info":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    .end local v12    # "title":Ljava/lang/String;
    .end local v13    # "totalGamerscore":I
    .restart local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .restart local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_11
    move-object v6, v7

    .line 561
    check-cast v6, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 562
    .local v6, "info":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    iget-object v12, v6, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    .line 563
    .restart local v12    # "title":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v5

    .line 564
    .restart local v5    # "currentGamerscore":I
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v13

    .line 565
    .restart local v13    # "totalGamerscore":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v15

    if-eqz v15, :cond_13

    .line 566
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v4

    .line 575
    .end local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .end local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_12
    :goto_8
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 577
    .restart local v2    # "achievementString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    if-eqz v15, :cond_3

    .line 578
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEDSV2MediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v10

    .line 579
    .local v10, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v10, :cond_16

    .line 580
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v15

    const/16 v16, 0x2329

    move/from16 v0, v16

    if-ne v15, v0, :cond_15

    .line 581
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSquareIconUrl()Ljava/lang/String;

    move-result-object v16

    const v17, 0x7f020122

    const v18, 0x7f020124

    invoke-virtual/range {v15 .. v18}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_4

    .line 568
    .end local v2    # "achievementString":Ljava/lang/String;
    .end local v10    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .restart local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_13
    if-eqz v9, :cond_14

    .line 569
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .end local v9    # "leftItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v3

    .line 571
    :cond_14
    if-eqz v11, :cond_12

    .line 572
    check-cast v11, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .end local v11    # "rightItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCompletionPercentage()I

    move-result v4

    goto :goto_8

    .line 583
    .restart local v2    # "achievementString":Ljava/lang/String;
    .restart local v10    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSquareIconUrl()Ljava/lang/String;

    move-result-object v16

    const v17, 0x7f020064

    const v18, 0x7f020064

    invoke-virtual/range {v15 .. v18}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_4

    .line 585
    :cond_16
    const-string v15, "Game"

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleType()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 586
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getDisplayImage()Ljava/lang/String;

    move-result-object v16

    const v17, 0x7f020122

    const v18, 0x7f020124

    invoke-virtual/range {v15 .. v18}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_4

    .end local v6    # "info":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    .end local v10    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_17
    move v15, v3

    .line 599
    goto/16 :goto_5

    :cond_18
    move v15, v4

    .line 602
    goto/16 :goto_6

    .line 618
    :cond_19
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 619
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->achievementsStatusIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 620
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 621
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscoreStatusTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 622
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewProfile:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 623
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->gamerscorePercentProgressTextViewMe:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->comparisonBarMe:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/16 v16, 0x8

    invoke-static/range {v15 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_7
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 480
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsItemViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V

    return-void
.end method
