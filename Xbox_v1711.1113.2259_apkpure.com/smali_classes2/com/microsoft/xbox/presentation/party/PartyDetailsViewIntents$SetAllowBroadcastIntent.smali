.class public abstract Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;
.super Ljava/lang/Object;
.source "PartyDetailsViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SetAllowBroadcastIntent"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Z)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;
    .locals 1
    .param p0, "allow"    # Z

    .prologue
    .line 83
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewIntents_SetAllowBroadcastIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewIntents_SetAllowBroadcastIntent;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public abstract allow()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;->allow()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
