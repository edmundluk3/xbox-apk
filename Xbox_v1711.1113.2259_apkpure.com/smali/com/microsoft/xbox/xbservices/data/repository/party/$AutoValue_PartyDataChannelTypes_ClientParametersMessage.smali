.class abstract Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;
.source "$AutoValue_PartyDataChannelTypes_ClientParametersMessage.java"


# instance fields
.field private final payloadType:I

.field private final ssrc:J

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;JI)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "ssrc"    # J
    .param p4, "payloadType"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->type:Ljava/lang/String;

    .line 23
    iput-wide p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    .line 24
    iput p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->payloadType:I

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 59
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;

    .line 60
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    .line 61
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;->ssrc()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->payloadType:I

    .line 62
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;->payloadType()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;
    :cond_3
    move v1, v2

    .line 64
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 69
    const/4 v0, 0x1

    .line 70
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 72
    mul-int/2addr v0, v8

    .line 73
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 74
    mul-int/2addr v0, v8

    .line 75
    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->payloadType:I

    xor-int/2addr v0, v1

    .line 76
    return v0
.end method

.method public payloadType()I
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "payload_type"
    .end annotation

    .prologue
    .line 41
    iget v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->payloadType:I

    return v0
.end method

.method public ssrc()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClientParametersMessage{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->ssrc:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payloadType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->payloadType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ClientParametersMessage;->type:Ljava/lang/String;

    return-object v0
.end method
