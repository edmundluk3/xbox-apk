.class public Lcom/microsoft/xbox/xle/app/activity/ArtistDetailBiographyScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ArtistDetailBiographyScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string v0, "ArtistDetailBiography"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailBiographyScreen;->onCreateContentView()V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailBiographyScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 15
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 19
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailBiographyScreen;->setContentView(I)V

    .line 20
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method
