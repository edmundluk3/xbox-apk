.class public final enum Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
.super Ljava/lang/Enum;
.source "UserPostsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PostType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field public static final enum Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field public static final enum Link:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field public static final enum Text:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field public static final enum XboxLink:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    const-string v1, "Text"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Text:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    const-string v1, "Link"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Link:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    const-string v1, "XboxLink"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->XboxLink:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    const-string v1, "Lfg"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Text:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Link:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->XboxLink:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    return-object v0
.end method
