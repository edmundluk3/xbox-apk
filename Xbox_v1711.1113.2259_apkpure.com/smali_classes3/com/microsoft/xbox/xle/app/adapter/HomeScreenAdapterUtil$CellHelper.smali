.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;
.super Ljava/lang/Object;
.source "HomeScreenAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CellHelper"
.end annotation


# instance fields
.field private barSize:I

.field private final grid:Landroid/widget/GridLayout;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final root:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/widget/GridLayout;)V
    .locals 0
    .param p1, "grid"    # Landroid/widget/GridLayout;

    .prologue
    .line 112
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;-><init>(Landroid/widget/GridLayout;Landroid/view/View;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/widget/GridLayout;Landroid/view/View;)V
    .locals 2
    .param p1, "grid"    # Landroid/widget/GridLayout;
    .param p2, "root"    # Landroid/view/View;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    .line 117
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->root:Landroid/view/View;

    .line 118
    invoke-virtual {p1}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 119
    .local v0, "ctx":Landroid/content/Context;
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->inflater:Landroid/view/LayoutInflater;

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->root:Landroid/view/View;

    return-object v0
.end method

.method private getBarCount()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v0}, Landroid/widget/GridLayout;->getColumnCount()I

    move-result v0

    return v0
.end method

.method private setDimensions(Landroid/view/View;)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/GridLayout$LayoutParams;

    .line 231
    .local v2, "lp":Landroid/widget/GridLayout$LayoutParams;
    const v5, 0x7f0e0007

    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 232
    .local v3, "rowSpan":I
    iget v5, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->barSize:I

    mul-int/2addr v5, v3

    iget v6, v2, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    sub-int/2addr v5, v6

    iget v6, v2, Landroid/widget/GridLayout$LayoutParams;->bottomMargin:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 234
    const v5, 0x7f0e0005

    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    .local v4, "tag":Ljava/lang/Object;
    if-eqz v4, :cond_1

    move-object v5, v4

    .line 237
    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 238
    .local v0, "aspectRatio":F
    iget v5, v2, Landroid/widget/GridLayout$LayoutParams;->height:I

    int-to-float v5, v5

    div-float/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v2, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 244
    .end local v0    # "aspectRatio":F
    :cond_0
    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 245
    return-void

    .line 239
    :cond_1
    const v5, 0x7f0e0006

    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    move-object v5, v4

    .line 241
    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 242
    .local v1, "colSpan":I
    iget v5, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->barSize:I

    mul-int/2addr v5, v1

    iget v6, v2, Landroid/widget/GridLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v5, v6

    iget v6, v2, Landroid/widget/GridLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/widget/GridLayout$LayoutParams;->width:I

    goto :goto_0
.end method


# virtual methods
.method protected computeBarSize(II)I
    .locals 8
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 214
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v4}, Landroid/widget/GridLayout;->getColumnCount()I

    move-result v1

    .line 215
    .local v1, "columnCount":I
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    int-to-double v6, v1

    mul-double/2addr v4, v6

    int-to-double v6, p1

    mul-double/2addr v4, v6

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->getBarCount()I

    move-result v6

    int-to-double v6, v6

    div-double v2, v4, v6

    .line 216
    .local v2, "gridWidth":D
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v4}, Landroid/widget/GridLayout;->getPaddingLeft()I

    move-result v4

    int-to-double v4, v4

    sub-double v4, v2, v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v6}, Landroid/widget/GridLayout;->getPaddingRight()I

    move-result v6

    int-to-double v6, v6

    sub-double/2addr v4, v6

    int-to-double v6, v1

    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 217
    .local v0, "barSize":I
    return v0
.end method

.method public newCell(II)Landroid/view/View;
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    const/4 v0, 0x1

    .line 130
    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->newCell(IIII)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public newCell(III)Landroid/view/View;
    .locals 6
    .param p1, "cellResId"    # I
    .param p2, "row"    # I
    .param p3, "col"    # I

    .prologue
    const/4 v3, 0x1

    .line 142
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->newCell(IIIII)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public newCell(IIII)Landroid/view/View;
    .locals 6
    .param p1, "row"    # I
    .param p2, "rowSz"    # I
    .param p3, "col"    # I
    .param p4, "colSz"    # I

    .prologue
    .line 155
    const v1, 0x7f03013d

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->newCell(IIIII)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public newCell(IIIII)Landroid/view/View;
    .locals 7
    .param p1, "cellResId"    # I
    .param p2, "row"    # I
    .param p3, "rowSz"    # I
    .param p4, "col"    # I
    .param p5, "colSz"    # I

    .prologue
    .line 159
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->newCell(IIIIILjava/lang/Float;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public newCell(IIIIILjava/lang/Float;)Landroid/view/View;
    .locals 5
    .param p1, "cellResId"    # I
    .param p2, "row"    # I
    .param p3, "rowSz"    # I
    .param p4, "col"    # I
    .param p5, "colSz"    # I
    .param p6, "aspectRatio"    # Ljava/lang/Float;

    .prologue
    .line 187
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->inflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 188
    .local v0, "cell":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/GridLayout$LayoutParams;

    .line 189
    .local v1, "lp":Landroid/widget/GridLayout$LayoutParams;
    invoke-static {p4, p5}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v2

    iput-object v2, v1, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    .line 190
    invoke-static {p2, p3}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v2

    iput-object v2, v1, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    .line 191
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    const v2, 0x7f0e0006

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 193
    const v2, 0x7f0e0007

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 194
    if-eqz p6, :cond_0

    .line 195
    const v2, 0x7f0e0005

    invoke-virtual {v0, v2, p6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 197
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->setDimensions(Landroid/view/View;)V

    .line 198
    return-object v0
.end method

.method public newCell(IIILjava/lang/Float;)Landroid/view/View;
    .locals 7
    .param p1, "cellResId"    # I
    .param p2, "row"    # I
    .param p3, "col"    # I
    .param p4, "aspectRatio"    # Ljava/lang/Float;

    .prologue
    const/4 v3, 0x1

    .line 172
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move v5, v3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->newCell(IIIIILjava/lang/Float;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public positionCells()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->root:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->root:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->positionCells(II)V

    .line 210
    return-void
.end method

.method public positionCells(II)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 202
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->computeBarSize(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->barSize:I

    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v1}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->grid:Landroid/widget/GridLayout;

    invoke-virtual {v1, v0}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->setDimensions(Landroid/view/View;)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method public postPositionCells()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;->root:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellHelper;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 227
    return-void
.end method
