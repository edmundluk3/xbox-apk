.class public Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "LfgDetailsScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    const-string v0, "LFG - Session Details"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;->onCreateContentView()V

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 18
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f030159

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;->setContentView(I)V

    .line 23
    return-void
.end method
