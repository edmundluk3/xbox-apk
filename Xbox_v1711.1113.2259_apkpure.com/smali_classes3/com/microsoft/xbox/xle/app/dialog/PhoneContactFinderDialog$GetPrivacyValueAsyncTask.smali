.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PhoneContactFinderDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "GetPrivacyValueAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 396
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 398
    .local v1, "result":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPrivacySetting(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;

    move-result-object v2

    .line 399
    .local v2, "setting":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    if-eqz v2, :cond_0

    .line 400
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;->getPrivacySettingValue()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->access$102(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 401
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 406
    .end local v2    # "setting":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    :cond_0
    :goto_0
    return-object v1

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->onGetPrivacyValueAsyncTaskCompleted()V

    .line 416
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 379
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method
