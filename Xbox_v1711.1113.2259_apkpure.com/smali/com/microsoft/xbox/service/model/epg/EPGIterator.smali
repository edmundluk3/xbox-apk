.class public Lcom/microsoft/xbox/service/model/epg/EPGIterator;
.super Ljava/lang/Object;
.source "EPGIterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    }
.end annotation


# static fields
.field public static CREATE_HOLES:Z

.field public static CREATE_OVERLAPS:Z

.field private static final USE_MOCK_ITERATOR:Z


# instance fields
.field private final channelOrdinal:I

.field private final isForwardIterator:Z

.field private last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

.field private nativePointer:I

.field private runningOffset:I

.field private startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-boolean v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_HOLES:Z

    .line 14
    sput-boolean v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_OVERLAPS:Z

    return-void
.end method

.method private constructor <init>(IIZ)V
    .locals 2
    .param p1, "nativePointer"    # I
    .param p2, "channelOrdinal"    # I
    .param p3, "forwardIterator"    # Z

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 215
    iput v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->runningOffset:I

    .line 216
    iput v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->startTime:I

    .line 219
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->isForwardIterator:Z

    .line 220
    iput p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    .line 221
    iput p2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->channelOrdinal:I

    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->readProgramData()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 226
    return-void
.end method

.method public static createIteratorAt(Ljava/lang/String;IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    .locals 1
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "channelOrdinal"    # I
    .param p2, "startTime"    # I
    .param p3, "forward"    # Z

    .prologue
    .line 208
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativeCreateIterator(Ljava/lang/String;IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeCreateIterator(Ljava/lang/String;IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;
.end method

.method private native nativeReadNext(I)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
.end method

.method private native nativeRelease(I)V
.end method

.method private readProgramData()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 5

    .prologue
    const/16 v4, 0x5f

    .line 243
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    if-nez v2, :cond_1

    .line 244
    const/4 v0, 0x0

    .line 266
    :cond_0
    :goto_0
    return-object v0

    .line 246
    :cond_1
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativeReadNext(I)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v0

    .line 248
    .local v0, "data":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    sget-boolean v2, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_HOLES:Z

    if-eqz v2, :cond_2

    .line 249
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    rem-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_2

    .line 250
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativeReadNext(I)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v0

    goto :goto_1

    .line 254
    :cond_2
    sget-boolean v2, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_OVERLAPS:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 255
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->access$000(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)I

    move-result v2

    add-int/lit16 v2, v2, 0x12c

    invoke-static {v0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->access$002(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;I)I

    .line 258
    :cond_3
    if-eqz v0, :cond_0

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 261
    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->channelOrdinal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->setId(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 277
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    const-string v1, "EPGIterator does not support clone."

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    if-eqz v0, :cond_0

    .line 195
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativeRelease(I)V

    .line 196
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    .line 198
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->destroy()V

    .line 272
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 273
    return-void
.end method

.method public get()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    return-object v0
.end method

.method public isForwardIterator()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->isForwardIterator:Z

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->nativePointer:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readNext()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 2

    .prologue
    .line 182
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    if-nez v1, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 187
    .local v0, "ret":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->readProgramData()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->last:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    goto :goto_0
.end method
