.class public Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;
.super Ljava/lang/Object;
.source "ContactsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PostalAddressItem"
.end annotation


# instance fields
.field public final map:Lcom/facebook/react/bridge/WritableMap;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v0

    iput-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->map:Lcom/facebook/react/bridge/WritableMap;

    .line 358
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->map:Lcom/facebook/react/bridge/WritableMap;

    const-string v1, "label"

    invoke-static {p1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->getLabel(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v0, "formattedAddress"

    const-string v1, "data1"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v0, "street"

    const-string v1, "data4"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v0, "pobox"

    const-string v1, "data5"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v0, "neighborhood"

    const-string v1, "data6"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v0, "city"

    const-string v1, "data7"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v0, "region"

    const-string v1, "data8"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v0, "state"

    const-string v1, "data8"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v0, "postCode"

    const-string v1, "data9"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v0, "country"

    const-string v1, "data10"

    invoke-direct {p0, p1, v0, v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    return-void
.end method

.method static getLabel(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 377
    const-string v1, "data2"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 386
    const-string v0, "other"

    :cond_0
    :goto_0
    return-object v0

    .line 379
    :pswitch_0
    const-string v0, "home"

    goto :goto_0

    .line 381
    :pswitch_1
    const-string v0, "work"

    goto :goto_0

    .line 383
    :pswitch_2
    const-string v1, "data3"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "label":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 377
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private putString(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "androidKey"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;->map:Lcom/facebook/react/bridge/WritableMap;

    invoke-interface {v1, p2, v0}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_0
    return-void
.end method
