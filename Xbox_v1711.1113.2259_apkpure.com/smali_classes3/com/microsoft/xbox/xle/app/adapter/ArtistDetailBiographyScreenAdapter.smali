.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ArtistDetailBiographyScreenAdapter.java"


# instance fields
.field private biographyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 19
    const v0, 0x7f0e01e0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->screenBody:Landroid/view/View;

    .line 20
    const v0, 0x7f0e01e1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    .line 23
    const v0, 0x7f0e01e2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->biographyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 24
    return-void
.end method


# virtual methods
.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->updateLoadingIndicator(Z)V

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->biographyTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 33
    return-void
.end method
