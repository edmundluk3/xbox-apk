.class public Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;
.super Landroid/widget/RelativeLayout;
.source "EditViewFixedLength.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;
    }
.end annotation


# instance fields
.field private characterCountView:Landroid/widget/TextView;

.field private characterCountViewRid:I

.field private container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

.field private editTextView:Landroid/widget/EditText;

.field private editTextViewRid:I

.field private inputType:I

.field private maxCharacterCount:I

.field private maxLines:I

.field private onMeasureListener:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

.field private singleLine:Z

.field private title:Ljava/lang/String;

.field private titleView:Landroid/widget/TextView;

.field private titleViewRid:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleView:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    .line 63
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleView:Landroid/widget/TextView;

    .line 32
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    .line 33
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    .line 70
    sget-object v4, Lcom/microsoft/xboxone/smartglass/R$styleable;->EditViewFixedLength:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 71
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->title:Ljava/lang/String;

    .line 72
    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxCharacterCount:I

    .line 73
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->singleLine:Z

    .line 74
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->inputType:I

    .line 75
    const/4 v4, 0x4

    const v5, 0x7fffffff

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxLines:I

    .line 80
    const/4 v4, 0x6

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleViewRid:I

    .line 81
    const/4 v4, 0x7

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextViewRid:I

    .line 82
    const/16 v4, 0x8

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountViewRid:I

    .line 83
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 84
    .local v1, "layout":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 86
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 88
    .local v3, "vi":Landroid/view/LayoutInflater;
    if-ne v1, v6, :cond_0

    const v1, 0x7f0300e8

    .end local v1    # "layout":I
    :cond_0
    invoke-virtual {v3, v1, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 90
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v2, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 91
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->updateCharacterCountView(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;)Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    return-object v0
.end method

.method private updateCharacterCountView(I)V
    .locals 6
    .param p1, "newLength"    # I

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxCharacterCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070d71

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 222
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxCharacterCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 221
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 223
    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "watcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 213
    return-void
.end method

.method public getEditTextView()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method public getEditTextViewMeasuredHeight()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getEditTextViewMeasuredWidth()F
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getOnMeasureListener()Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->onMeasureListener:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTotalHeightOfCharacterCountIncludingMargins()I
    .locals 4

    .prologue
    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v1

    .line 172
    .local v1, "result":I
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 173
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 174
    return v1
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 96
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 98
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleViewRid:I

    if-ne v2, v3, :cond_0

    const v2, 0x7f0e040b

    :goto_0
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleView:Landroid/widget/TextView;

    .line 99
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextViewRid:I

    if-ne v2, v3, :cond_1

    const v2, 0x7f0e0410

    :goto_1
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    .line 100
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountViewRid:I

    if-ne v2, v3, :cond_2

    const v2, 0x7f0e0412

    :goto_2
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    .line 103
    new-array v1, v5, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxCharacterCount:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    .line 104
    .local v1, "filters":[Landroid/text/InputFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :goto_3
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->updateCharacterCountView(I)V

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->singleLine:Z

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 140
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->maxLines:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 142
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setFocusable(Z)V

    .line 143
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setFocusableInTouchMode(Z)V

    .line 145
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 146
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 148
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getInputType()I

    move-result v0

    .line 149
    .local v0, "existingInputType":I
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->inputType:I

    or-int/2addr v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 150
    return-void

    .line 98
    .end local v0    # "existingInputType":I
    .end local v1    # "filters":[Landroid/text/InputFilter;
    :cond_0
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleViewRid:I

    goto/16 :goto_0

    .line 99
    :cond_1
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextViewRid:I

    goto/16 :goto_1

    .line 100
    :cond_2
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountViewRid:I

    goto :goto_2

    .line 135
    .restart local v1    # "filters":[Landroid/text/InputFilter;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->titleView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 227
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->onMeasureListener:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->onMeasureListener:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;->afterOnMeasure(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;)V

    .line 231
    :cond_0
    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "watcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 217
    return-void
.end method

.method public setCharacterCountVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->characterCountView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    return-void
.end method

.method public setContainer(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V
    .locals 2
    .param p1, "parent"    # Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "EditViewFixedLength"

    const-string v1, "container is set multiple times"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->addChild(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setOnMeasureListener(Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->onMeasureListener:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength$OnMeasureListener;

    .line 54
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 209
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 153
    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->editTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 156
    :cond_0
    return-void
.end method
