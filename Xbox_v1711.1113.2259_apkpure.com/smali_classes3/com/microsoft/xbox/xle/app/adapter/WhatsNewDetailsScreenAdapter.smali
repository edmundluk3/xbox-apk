.class public Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "WhatsNewDetailsScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030278

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;-><init>(Landroid/content/Context;I)V

    .line 29
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;->buildFeatureList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 30
    const v1, 0x7f0e0bdb

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 31
    const v1, 0x7f0e0bdc

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    return-void
.end method

.method private buildFeatureList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v0, "featureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v1, :cond_0

    .line 44
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070da7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070da8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070392

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;->onTapCloseButton()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
