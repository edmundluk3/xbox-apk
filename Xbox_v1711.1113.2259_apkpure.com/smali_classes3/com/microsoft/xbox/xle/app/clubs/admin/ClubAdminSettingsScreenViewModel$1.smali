.class synthetic Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;
.super Ljava/lang/Object;
.source "ClubAdminSettingsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 643
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->values()[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    .line 609
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->values()[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_2
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_4
    :try_start_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_5
    :try_start_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    .line 132
    :goto_6
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    :try_start_7
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_7
    :try_start_8
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_8
    :try_start_9
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    .line 62
    :goto_9
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    :try_start_a
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_a
    :try_start_b
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_b
    :try_start_c
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_c
    :try_start_d
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_d
    return-void

    :catch_0
    move-exception v0

    goto :goto_d

    :catch_1
    move-exception v0

    goto :goto_c

    :catch_2
    move-exception v0

    goto :goto_b

    :catch_3
    move-exception v0

    goto :goto_a

    .line 132
    :catch_4
    move-exception v0

    goto :goto_9

    :catch_5
    move-exception v0

    goto :goto_8

    :catch_6
    move-exception v0

    goto :goto_7

    .line 609
    :catch_7
    move-exception v0

    goto :goto_6

    :catch_8
    move-exception v0

    goto :goto_5

    :catch_9
    move-exception v0

    goto/16 :goto_4

    :catch_a
    move-exception v0

    goto/16 :goto_3

    :catch_b
    move-exception v0

    goto/16 :goto_2

    .line 643
    :catch_c
    move-exception v0

    goto/16 :goto_1

    :catch_d
    move-exception v0

    goto/16 :goto_0
.end method
