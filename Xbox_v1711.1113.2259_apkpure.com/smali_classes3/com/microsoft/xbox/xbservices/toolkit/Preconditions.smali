.class public final Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;
.super Ljava/lang/Object;
.source "Preconditions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This class shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static error(Ljava/lang/String;)V
    .locals 0
    .param p0, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public static floatRange(DDD)V
    .locals 2
    .param p0, "from"    # D
    .param p2, "to"    # D
    .param p4, "value"    # D

    .prologue
    .line 73
    cmpg-double v0, p4, p0

    if-ltz v0, :cond_0

    cmpl-double v0, p4, p2

    if-lez v0, :cond_1

    .line 74
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "floatRange - from:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 76
    :cond_1
    return-void
.end method

.method public static floatRangeFrom(DD)V
    .locals 6
    .param p0, "from"    # D
    .param p2, "value"    # D

    .prologue
    .line 50
    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    move-wide v0, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->floatRange(DDD)V

    .line 51
    return-void
.end method

.method public static floatRangeTo(DD)V
    .locals 6
    .param p0, "to"    # D
    .param p2, "value"    # D

    .prologue
    .line 61
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    move-wide v2, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->floatRange(DDD)V

    .line 62
    return-void
.end method

.method private static getClassCallerStackTrace()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/StackTraceElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 185
    .local v3, "stackTrace":Ljava/util/List;, "Ljava/util/List<Ljava/lang/StackTraceElement;>;"
    const/4 v1, 0x0

    .line 187
    .local v1, "foundFirstClassFrame":Z
    const/4 v0, 0x0

    .line 189
    .local v0, "depth":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 190
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/StackTraceElement;

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 192
    .local v2, "isClassFrame":Z
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 193
    const/4 v1, 0x1

    .line 189
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    if-eqz v1, :cond_0

    if-nez v2, :cond_0

    .line 199
    .end local v2    # "isClassFrame":Z
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .end local v3    # "stackTrace":Ljava/util/List;, "Ljava/util/List<Ljava/lang/StackTraceElement;>;"
    :cond_3
    return-object v3
.end method

.method public static intRange(JJJ)V
    .locals 2
    .param p0, "from"    # J
    .param p2, "to"    # J
    .param p4, "value"    # J

    .prologue
    .line 109
    cmp-long v0, p4, p0

    if-ltz v0, :cond_0

    cmp-long v0, p4, p2

    if-lez v0, :cond_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "intRange - from:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 112
    :cond_1
    return-void
.end method

.method public static intRangeFrom(JJ)V
    .locals 6
    .param p0, "from"    # J
    .param p2, "value"    # J

    .prologue
    .line 86
    const-wide v2, 0x7fffffffffffffffL

    move-wide v0, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->intRange(JJJ)V

    .line 87
    return-void
.end method

.method public static intRangeTo(JJ)V
    .locals 6
    .param p0, "to"    # J
    .param p2, "value"    # J

    .prologue
    .line 97
    const-wide/high16 v0, -0x8000000000000000L

    move-wide v2, p0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->intRange(JJJ)V

    .line 98
    return-void
.end method

.method public static isOnUIThread()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 27
    const-string v0, "Operation must occur on UI thread"

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 29
    :cond_0
    return-void
.end method

.method public static isTrue(Z)V
    .locals 1
    .param p0, "condition"    # Z

    .prologue
    .line 157
    if-nez p0, :cond_0

    .line 158
    const-string v0, "isTrue"

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 160
    :cond_0
    return-void
.end method

.method public static nonEmpty(Ljava/lang/CharSequence;)V
    .locals 1
    .param p0, "chars"    # Ljava/lang/CharSequence;

    .prologue
    .line 145
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "nonEmpty"

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 148
    :cond_0
    return-void
.end method

.method public static nonEmpty(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "coll":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    const-string v0, "nonEmpty"

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 136
    :cond_1
    return-void
.end method

.method public static nonNull(Ljava/lang/Object;)V
    .locals 1
    .param p0, "arg"    # Ljava/lang/Object;

    .prologue
    .line 121
    if-nez p0, :cond_0

    .line 122
    const-string v0, "nonNull"

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->throwCallerPreconditionException(Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method private static throwCallerPreconditionException(Ljava/lang/String;)V
    .locals 4
    .param p0, "detailMessage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;
        }
    .end annotation

    .prologue
    .line 170
    new-instance v1, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$1;)V

    .line 172
    .local v1, "preconditionException":Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->getClassCallerStackTrace()Ljava/util/List;

    move-result-object v0

    .line 173
    .local v0, "callerStackTrace":Ljava/util/List;, "Ljava/util/List<Ljava/lang/StackTraceElement;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/StackTraceElement;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/StackTraceElement;

    .line 174
    .local v2, "stackTrace":[Ljava/lang/StackTraceElement;
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions$PreconditionException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 176
    throw v1
.end method
