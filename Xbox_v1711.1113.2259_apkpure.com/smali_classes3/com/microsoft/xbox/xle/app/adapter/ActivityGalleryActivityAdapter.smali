.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;
.source "ActivityGalleryActivityAdapter.java"


# instance fields
.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

.field private slideShow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;-><init>()V

    .line 45
    const v0, 0x7f0e0618

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->screenBody:Landroid/view/View;

    .line 46
    const v0, 0x7f0e0619

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    .line 49
    const v0, 0x7f0e061a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030114

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->updateLoadingIndicator(Z)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->slideShow:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->getSlideShows()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->getSlideShows()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->slideShow:Ljava/util/ArrayList;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->clear()V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->slideShow:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->slideShow:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->addAll(Ljava/util/Collection;)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 75
    :cond_1
    return-void
.end method
