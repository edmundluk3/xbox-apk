.class public abstract Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
.super Ljava/lang/Object;
.source "LanguageSettingsInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LocationChangedAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
    .locals 1
    .param p0, "newLocation"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsInteractor_LocationChangedAction;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/settings/language/AutoValue_LanguageSettingsInteractor_LocationChangedAction;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V

    return-object v0
.end method


# virtual methods
.method public abstract newLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
