.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBESettings;
.super Ljava/lang/Object;
.source "OOBESettings.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULTS:Lcom/microsoft/xbox/domain/oobe/OOBESettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 29
    const/4 v0, 0x0

    const-string v1, ""

    invoke-static {v2, v2, v0, v2, v1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->with(ZZLjava/lang/Boolean;ZLjava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->DEFAULTS:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;-><init>()V

    return-object v0
.end method

.method public static defaultPrefs()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->DEFAULTS:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(ZZLjava/lang/Boolean;ZLjava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 6
    .param p0, "allowAutoUpdateApps"    # Z
    .param p1, "allowAutoUpdateSystem"    # Z
    .param p2, "allowInstantOn"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "autoDSTEnabled"    # Z
    .param p4, "currentTimeZone"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;

    move v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;-><init>(ZZLjava/lang/Boolean;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract autoDST()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AutoDSTEnabled"
    .end annotation
.end method

.method public abstract autoUpdateApps()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowAutoUpdateApps"
    .end annotation
.end method

.method public abstract autoUpdateSystem()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowAutoUpdateSystem"
    .end annotation
.end method

.method public abstract instantOn()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowInstantOn"
    .end annotation
.end method

.method public abstract timeZone()Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currentTimeZone"
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
