.class Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;
.super Ljava/lang/Object;
.source "CreateLfgSelectTitleAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$102(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Z)Z

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clearSearchResults()V

    .line 109
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 88
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clearSearchResults()V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$102(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Z)Z

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->loadAutoSuggestAsync(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 98
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->loadSearchResultsAsync(Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;->access$102(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;Z)Z

    .line 103
    return-void
.end method
