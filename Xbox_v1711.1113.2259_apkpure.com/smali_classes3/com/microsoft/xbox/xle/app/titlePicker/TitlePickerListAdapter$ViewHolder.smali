.class Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TitlePickerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# static fields
.field private static SELECTION_COLOR:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# instance fields
.field private final checked:Landroid/view/View;

.field private final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final rootView:Landroid/view/View;

.field private final text:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 3
    .param p1, "rootView"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->rootView:Landroid/view/View;

    .line 83
    const v1, 0x7f0e0ad7

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 85
    const v1, 0x7f0e0ad8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->text:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 86
    const v1, 0x7f0e0ad6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->checked:Landroid/view/View;

    .line 88
    sget-object v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->SELECTION_COLOR:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->SELECTION_COLOR:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 89
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 90
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->SELECTION_COLOR:Ljava/lang/Integer;

    .line 92
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    return-void

    .line 90
    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$1;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V

    return-void
.end method

.method private bindTo(Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V
    .locals 3
    .param p1, "titleInfo"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f020122

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 98
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getBoxArt()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->text:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->checked:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->SELECTION_COLOR:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 106
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
