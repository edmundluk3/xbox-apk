.class public Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
.super Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
.source "PeopleSelectorItem.java"


# instance fields
.field private selected:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V
    .locals 4
    .param p1, "userSummary"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;-><init>()V

    .line 35
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isFavorite:Z

    .line 36
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isIdentityShared:Z

    .line 37
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayPicRaw:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamertag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayName:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->realName:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamertag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->gamertag:Ljava/lang/String;

    .line 42
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    .line 43
    return-void

    :cond_0
    move v0, v1

    .line 36
    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;)V
    .locals 1
    .param p1, "p"    # Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;-><init>()V

    .line 13
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isFavorite:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isFavorite:Z

    .line 14
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isIdentityShared:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isIdentityShared:Z

    .line 15
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayPicRaw:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayPicRaw:Ljava/lang/String;

    .line 16
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayName:Ljava/lang/String;

    .line 17
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    .line 18
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->realName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->realName:Ljava/lang/String;

    .line 19
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->gamertag:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->gamertag:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "p"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;-><init>()V

    .line 24
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isFavorite:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isFavorite:Z

    .line 25
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isIdentityShared:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->isIdentityShared:Z

    .line 26
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayPicRaw:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayName:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->realName:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->gamertag:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    .line 32
    return-void
.end method


# virtual methods
.method public getGamerPicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->displayPicRaw:Ljava/lang/String;

    return-object v0
.end method

.method public getGamerRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->realName:Ljava/lang/String;

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public getIsOnline()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public getIsSelected()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    return v0
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    .line 55
    return-void
.end method

.method public toggleSelection()V
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->selected:Z

    .line 47
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
