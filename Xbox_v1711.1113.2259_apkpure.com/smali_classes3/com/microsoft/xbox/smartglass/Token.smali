.class public Lcom/microsoft/xbox/smartglass/Token;
.super Ljava/lang/Object;
.source "Token.java"


# static fields
.field private static final XblAuthHeaderFormat:Ljava/lang/String; = "XBL3.0 x=%s;%s"


# instance fields
.field public final ageGroup:Lcom/microsoft/xbox/smartglass/AgeGroup;

.field public final audienceUri:Ljava/lang/String;

.field public final authorization:Ljava/lang/String;

.field public final gamertag:Ljava/lang/String;

.field public final privileges:[I

.field public final type:Lcom/microsoft/xbox/smartglass/TokenType;

.field public final userHash:Ljava/lang/String;

.field public final xuid:J


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[IJLjava/lang/String;I)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "audienceUri"    # Ljava/lang/String;
    .param p3, "authorization"    # Ljava/lang/String;
    .param p4, "userHash"    # Ljava/lang/String;
    .param p5, "privileges"    # [I
    .param p6, "xuid"    # J
    .param p8, "gamertag"    # Ljava/lang/String;
    .param p9, "ageGroup"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/TokenType;->fromInt(I)Lcom/microsoft/xbox/smartglass/TokenType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Token;->type:Lcom/microsoft/xbox/smartglass/TokenType;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/Token;->audienceUri:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/Token;->authorization:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/Token;->userHash:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/microsoft/xbox/smartglass/Token;->privileges:[I

    .line 34
    iput-wide p6, p0, Lcom/microsoft/xbox/smartglass/Token;->xuid:J

    .line 35
    iput-object p8, p0, Lcom/microsoft/xbox/smartglass/Token;->gamertag:Ljava/lang/String;

    .line 36
    invoke-static {p9}, Lcom/microsoft/xbox/smartglass/AgeGroup;->fromInt(I)Lcom/microsoft/xbox/smartglass/AgeGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Token;->ageGroup:Lcom/microsoft/xbox/smartglass/AgeGroup;

    .line 37
    return-void
.end method


# virtual methods
.method public getAuthorizationHeaderForRequest()Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    const-string v0, "XBL3.0 x=%s;%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/Token;->userHash:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/Token;->authorization:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
