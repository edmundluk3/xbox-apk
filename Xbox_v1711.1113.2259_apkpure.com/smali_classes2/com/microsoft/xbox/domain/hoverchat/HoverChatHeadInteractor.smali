.class public Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;
.super Ljava/lang/Object;
.source "HoverChatHeadInteractor.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final contentDescriptionDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

.field private final imageDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;)V
    .locals 0
    .param p1, "imageDownloader"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
    .param p2, "contentDescriptionDownloader"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->imageDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;

    .line 24
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->contentDescriptionDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    .line 25
    return-void
.end method

.method static synthetic lambda$getParticipants$1(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->TAG:Ljava/lang/String;

    const-string v1, "getParticipants failed."

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic lambda$loadImageUrls$0(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->TAG:Ljava/lang/String;

    const-string v1, "loadImageUrls failed."

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public getParticipants()Lio/reactivex/Single;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->contentDescriptionDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;->getParticipants()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 40
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;->with(Ljava/util/List;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturnItem(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public loadImageUrls()Lio/reactivex/Single;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->imageDownloader:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;->getImagesUrls()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;->with(Ljava/util/List;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturnItem(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 29
    return-object v0
.end method
