.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;
.super Ljava/lang/Object;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecoListPagingInfo"
.end annotation


# instance fields
.field public final totalItems:I
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TotalItems"
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "totalItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 171
    iput p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;->totalItems:I

    .line 172
    return-void
.end method
