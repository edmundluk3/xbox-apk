.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullNoStatsHolder"
.end annotation


# instance fields
.field protected final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field protected final mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected final smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    const v2, 0x7f0b0020

    .line 486
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 487
    const v0, 0x7f0e00df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 488
    const v0, 0x7f0e07cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 489
    const v0, 0x7f0e07d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 490
    const v0, 0x7f0e07d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 491
    const v0, 0x7f0e07d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 492
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->setRowHeight(II)V

    .line 493
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->setImageWidth(Landroid/widget/ImageView;II)V

    .line 494
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 11
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    const/16 v10, 0x8

    const/4 v4, 0x0

    .line 498
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 500
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 502
    .local v1, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getImageUri(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v2

    .line 503
    .local v2, "uri":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v0

    .line 504
    .local v0, "defaultRid":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v5, 0x7f0e001d

    invoke-virtual {v3, v5, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 505
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v2, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 506
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 509
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v2, :cond_1

    const/4 v3, 0x4

    :goto_0
    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 511
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 513
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v6

    const-wide/32 v8, 0x162615ad

    cmp-long v3, v6, v8

    if-nez v3, :cond_2

    .line 514
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 515
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 516
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 523
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 524
    return-void

    :cond_1
    move v3, v4

    .line 509
    goto :goto_0

    .line 519
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->oneguideIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 520
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->newFeature:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    .line 529
    return-void
.end method
