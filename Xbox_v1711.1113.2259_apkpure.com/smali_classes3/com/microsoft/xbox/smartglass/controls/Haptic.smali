.class Lcom/microsoft/xbox/smartglass/controls/Haptic;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Haptic.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Haptic"

.field private static final GetStateMethod:Ljava/lang/String; = "GetState"

.field private static final StartMethod:Ljava/lang/String; = "Start"

.field private static final StopMethod:Ljava/lang/String; = "Stop"


# instance fields
.field private _context:Landroid/content/Context;

.field private _vibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;Landroid/content/Context;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const-string v0, "Haptic"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_context:Landroid/content/Context;

    .line 28
    const-string v0, "vibrator"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_vibrator:Landroid/os/Vibrator;

    .line 30
    const-string v0, "Start"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->registerMethod(Ljava/lang/String;)V

    .line 31
    const-string v0, "Stop"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->registerMethod(Ljava/lang/String;)V

    .line 32
    const-string v0, "GetState"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->registerMethod(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method private isAvailable()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "haptic_feedback_enabled"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 93
    .local v0, "hapticSetting":I
    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private start(I)V
    .locals 3
    .param p1, "requestId"    # I

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_vibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v1, "Device does not support haptic."

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_vibrator:Landroid/os/Vibrator;

    const/4 v1, 0x4

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    goto :goto_0

    .line 79
    :array_0
    .array-data 8
        0x0
        0x190
        0x64
        0x0
    .end array-data
.end method

.method private stop(I)V
    .locals 1
    .param p1, "requestId"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_vibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_vibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    .line 89
    return-void
.end method


# virtual methods
.method public getCurrentState()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 46
    .local v0, "state":Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->isAvailable()Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;-><init>(ZZ)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "state":Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
    .local v1, "state":Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
    move-object v0, v1

    .line 50
    .end local v1    # "state":Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
    .restart local v0    # "state":Lcom/microsoft/xbox/smartglass/controls/JsonHapticCurrentState;
    :goto_0
    return-object v0

    .line 47
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string v0, "Start"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->start(I)V

    goto :goto_0

    .line 62
    :cond_2
    const-string v0, "Stop"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->stop(I)V

    goto :goto_0

    .line 64
    :cond_3
    const-string v0, "GetState"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Haptic;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Haptic;->getCurrentState()Lorg/json/JSONObject;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V

    goto :goto_0
.end method
