.class public interface abstract Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;
.super Ljava/lang/Object;
.source "BranchSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IAppChannelsListener"
.end annotation


# virtual methods
.method public abstract onAppChannelDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppChannelLineupsReceived(Ljava/util/List;Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onAppChannelProgramDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
.end method
