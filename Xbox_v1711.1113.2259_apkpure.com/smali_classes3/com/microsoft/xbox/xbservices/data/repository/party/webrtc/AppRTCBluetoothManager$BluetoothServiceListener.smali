.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;
.super Ljava/lang/Object;
.source "AppRTCBluetoothManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 97
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v0, v1, :cond_1

    .line 105
    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :cond_0
    :goto_0
    return-void

    .line 100
    .restart local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :cond_1
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BluetoothServiceListener.onServiceConnected: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    invoke-static {v0, p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$202(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    .line 104
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected done: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 4
    .param p1, "profile"    # I

    .prologue
    const/4 v3, 0x0

    .line 110
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v0, v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BluetoothServiceListener.onServiceDisconnected: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stopScoAudio()V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$202(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$402(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$102(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    .line 119
    const-string v0, "AppRTCBluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceDisconnected done: BT state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothServiceListener;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
