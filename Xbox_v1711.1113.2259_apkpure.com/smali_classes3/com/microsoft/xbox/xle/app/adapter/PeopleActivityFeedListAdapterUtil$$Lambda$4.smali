.class final synthetic Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final arg$2:Z

.field private final arg$3:Z

.field private final arg$4:Z

.field private final arg$5:Z

.field private final arg$6:Z

.field private final arg$7:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field private final arg$8:Z


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$1:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$2:Z

    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$3:Z

    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$4:Z

    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$5:Z

    iput-boolean p6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$6:Z

    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$7:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iput-boolean p8, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$8:Z

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Landroid/view/View$OnClickListener;
    .locals 9

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$1:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$2:Z

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$3:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$4:Z

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$5:Z

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$6:Z

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$7:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$4;->arg$8:Z

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->lambda$attachSocialBarListeners$5(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;ZZZZZLcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;ZLandroid/view/View;)V

    return-void
.end method
