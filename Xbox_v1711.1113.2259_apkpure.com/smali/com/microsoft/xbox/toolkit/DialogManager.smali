.class public Lcom/microsoft/xbox/toolkit/DialogManager;
.super Ljava/lang/Object;
.source "DialogManager.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;


# static fields
.field private static final instance:Lcom/microsoft/xbox/toolkit/DialogManager;


# instance fields
.field private manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/microsoft/xbox/toolkit/DialogManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/DialogManager;->instance:Lcom/microsoft/xbox/toolkit/DialogManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private checkProvider()V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/toolkit/DialogManager;->instance:Lcom/microsoft/xbox/toolkit/DialogManager;

    return-object v0
.end method


# virtual methods
.method public addDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->addDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V

    .line 271
    :cond_0
    return-void
.end method

.method public addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 250
    :cond_0
    return-void
.end method

.method public dismissAppBar()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissAppBar()V

    .line 236
    :cond_0
    return-void
.end method

.method public dismissBlocking()V
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissBlocking()V

    .line 227
    :cond_0
    return-void
.end method

.method public dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 68
    :cond_0
    return-void
.end method

.method public dismissToast()V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissToast()V

    .line 203
    :cond_0
    return-void
.end method

.method public dismissTopNonFatalAlert()V
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissTopNonFatalAlert()V

    .line 219
    :cond_0
    return-void
.end method

.method public forceDismissAlerts()V
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->forceDismissAlerts()V

    .line 211
    :cond_0
    return-void
.end method

.method public forceDismissAll()V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->forceDismissAll()V

    .line 195
    :cond_0
    return-void
.end method

.method public getAppBarMenu()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->getAppBarMenu()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsBlocking()Z
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->getIsBlocking()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    return-object v0
.end method

.method public getVisibleDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAppBarDismissed()V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->onAppBarDismissed()V

    .line 187
    :cond_0
    return-void
.end method

.method public onApplicationPause()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->onApplicationPause()V

    .line 257
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->onApplicationResume()V

    .line 264
    :cond_0
    return-void
.end method

.method public onDialogStopped(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->onDialogStopped(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 78
    :cond_0
    return-void
.end method

.method public removeDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->removeDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V

    .line 278
    :cond_0
    return-void
.end method

.method public setBlocking(ZLjava/lang/String;)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "statusText"    # Ljava/lang/String;

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->setBlocking(ZLjava/lang/String;)V

    .line 150
    :cond_0
    return-void
.end method

.method public setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "statusText"    # Ljava/lang/String;
    .param p3, "cancelRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V

    .line 158
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->setEnabled(Z)V

    .line 52
    :cond_0
    return-void
.end method

.method public setManager(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    .line 24
    return-void
.end method

.method public showAppBarMenu(Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;)V
    .locals 1
    .param p1, "appBar"    # Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showAppBarMenu(Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;)V

    .line 166
    :cond_0
    return-void
.end method

.method public showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 86
    :cond_0
    return-void
.end method

.method public showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;
    .param p5, "cancelText"    # Ljava/lang/String;
    .param p6, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 94
    :cond_0
    return-void
.end method

.method public showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 60
    :cond_0
    return-void
.end method

.method public showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 102
    :cond_0
    return-void
.end method

.method public showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;
    .param p5, "cancelText"    # Ljava/lang/String;
    .param p6, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 110
    :cond_0
    return-void
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "contentResId"    # I

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showToast(I)V

    .line 126
    :cond_0
    return-void
.end method

.method public showToast(II)V
    .locals 1
    .param p1, "contentResId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showToast(II)V

    .line 118
    :cond_0
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showToast(Ljava/lang/String;)V

    .line 134
    :cond_0
    return-void
.end method

.method public showToast(Ljava/lang/String;I)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManager;->checkProvider()V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManager;->manager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showToast(Ljava/lang/String;I)V

    .line 142
    :cond_0
    return-void
.end method
