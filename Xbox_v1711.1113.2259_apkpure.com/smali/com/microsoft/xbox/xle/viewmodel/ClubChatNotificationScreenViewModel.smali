.class public Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubChatNotificationScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private final clubId:J

.field private final model:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

.field private params:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

.field repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 37
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->model:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->clubId:J

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubChatNotificationScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 50
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate back from notification setting page"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getChatNotificationSetting()Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->model:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatNotificationSetting(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    move-result-object v0

    return-object v0
.end method

.method public hoverChatEnabled()Z
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->clubId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->with(J)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z

    move-result v0

    return v0
.end method

.method public hoverChatGloballyEnabled()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledGlobally()Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 99
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public setChatNotificationSetting(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V
    .locals 2
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->model:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->setChatNotificationSetting(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 63
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    if-ne p1, v0, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->setHoverChatEnabled(Z)V

    .line 66
    :cond_0
    return-void
.end method

.method public setHoverChatEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->clubId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->with(J)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->setEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Z)V

    .line 78
    return-void
.end method
