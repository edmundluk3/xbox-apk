.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2GameMediaItem.java"


# instance fields
.field public capabilities:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Capabilities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Capability;",
            ">;"
        }
    .end annotation
.end field

.field public developerName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DeveloperName"
    .end annotation
.end field

.field public parentalRatings:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParentalRatings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation
.end field

.field public publisherName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PublisherName"
    .end annotation
.end field

.field public ratingId:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "RatingId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 45
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->developerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->developerName:Ljava/lang/String;

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Genres:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Genres:Ljava/util/List;

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->publisherName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->publisherName:Ljava/lang/String;

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->ratingId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->ratingId:I

    move-object v0, p1

    .line 51
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 52
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->MediaItemType:Ljava/lang/String;

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getMediaType()I

    move-result v0

    if-nez v0, :cond_1

    .line 56
    const/16 v0, 0x2329

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 58
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/serialization/GameInfo;)V
    .locals 2
    .param p1, "gameInfo"    # Lcom/microsoft/xbox/service/model/serialization/GameInfo;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 80
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/serialization/GameInfo;->Id:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 81
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/GameInfo;->Name:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Name:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/GameInfo;->ImageUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    .line 84
    iget v0, p1, Lcom/microsoft/xbox/service/model/serialization/GameInfo;->Type:I

    sparse-switch v0, :sswitch_data_0

    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 95
    :goto_0
    return-void

    .line 89
    :sswitch_0
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/Title;)V
    .locals 2
    .param p1, "title"    # Lcom/microsoft/xbox/service/model/sls/Title;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 98
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->IsGame()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 101
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->getTitleId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 102
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Name:Ljava/lang/String;

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/service/model/sls/Title;->getImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 105
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 1
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 63
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->IsBundle:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->updateBundledProducts()V

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;
    .locals 1
    .param p1, "attributeName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v0

    goto :goto_0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    .line 154
    :goto_0
    return-object v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getPosterImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 151
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->imageUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Util;->getLocalizedParentalRating(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    return-object v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->providers:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->providers:Ljava/util/ArrayList;

    .line 188
    :goto_0
    return-object v1

    .line 177
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->providers:Ljava/util/ArrayList;

    .line 179
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 180
    .local v0, "gameProvider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 184
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%08X://default/"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getTitleId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->DeepLinkInfo:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setImages(Ljava/util/ArrayList;)V

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->providers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->providers:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->parentalRatings:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDescriptors:Ljava/util/List;

    .line 168
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRatingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->ratingId:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 124
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "temp":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getTitleId()J
    .locals 4

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 194
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 196
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v0

    goto :goto_0
.end method

.method public hasAttribute()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->productProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->getAttributes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBigCatProductId(Ljava/lang/String;)V
    .locals 1
    .param p1, "bigCatProductId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->bigCatProductId:Ljava/lang/String;

    .line 114
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Game:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->bigCatProductType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    .line 117
    :cond_0
    return-void
.end method

.method public updateBundledProducts()V
    .locals 4

    .prologue
    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->bundledSkus:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->bundledSkus:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;

    .line 71
    .local v0, "sku":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->bundledProductsIds:Ljava/util/List;

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;->productId:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;->isPrimary:Z

    if-eqz v2, :cond_0

    .line 73
    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;->productId:Ljava/lang/String;

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->BundlePrimaryItemId:Ljava/lang/String;

    goto :goto_0

    .line 77
    .end local v0    # "sku":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;
    :cond_1
    return-void
.end method
