.class public final enum Lcom/microsoft/cll/android/EventSensitivity;
.super Ljava/lang/Enum;
.source "EventSensitivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/cll/android/EventSensitivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/cll/android/EventSensitivity;

.field public static final enum Drop:Lcom/microsoft/cll/android/EventSensitivity;

.field public static final enum Hash:Lcom/microsoft/cll/android/EventSensitivity;

.field public static final enum Mark:Lcom/microsoft/cll/android/EventSensitivity;

.field public static final enum None:Lcom/microsoft/cll/android/EventSensitivity;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/cll/android/EventSensitivity;

    const-string v1, "None"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/cll/android/EventSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/EventSensitivity;->None:Lcom/microsoft/cll/android/EventSensitivity;

    .line 5
    new-instance v0, Lcom/microsoft/cll/android/EventSensitivity;

    const-string v1, "Mark"

    const/high16 v2, 0x80000

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/cll/android/EventSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/EventSensitivity;->Mark:Lcom/microsoft/cll/android/EventSensitivity;

    .line 6
    new-instance v0, Lcom/microsoft/cll/android/EventSensitivity;

    const-string v1, "Hash"

    const/high16 v2, 0x100000

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/cll/android/EventSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/EventSensitivity;->Hash:Lcom/microsoft/cll/android/EventSensitivity;

    .line 7
    new-instance v0, Lcom/microsoft/cll/android/EventSensitivity;

    const-string v1, "Drop"

    const/high16 v2, 0x200000

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/cll/android/EventSensitivity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/cll/android/EventSensitivity;

    sget-object v1, Lcom/microsoft/cll/android/EventSensitivity;->None:Lcom/microsoft/cll/android/EventSensitivity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/cll/android/EventSensitivity;->Mark:Lcom/microsoft/cll/android/EventSensitivity;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/cll/android/EventSensitivity;->Hash:Lcom/microsoft/cll/android/EventSensitivity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/cll/android/EventSensitivity;->$VALUES:[Lcom/microsoft/cll/android/EventSensitivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12
    iput p3, p0, Lcom/microsoft/cll/android/EventSensitivity;->value:I

    .line 13
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/EventSensitivity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/cll/android/EventSensitivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/cll/android/EventSensitivity;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/cll/android/EventSensitivity;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/cll/android/EventSensitivity;->$VALUES:[Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v0}, [Lcom/microsoft/cll/android/EventSensitivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/cll/android/EventSensitivity;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/microsoft/cll/android/EventSensitivity;->value:I

    return v0
.end method
