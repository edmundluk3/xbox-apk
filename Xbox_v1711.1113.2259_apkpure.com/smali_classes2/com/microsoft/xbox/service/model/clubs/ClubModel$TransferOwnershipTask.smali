.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransferOwnershipTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubAccountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

.field private final resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private final xuid:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1556
    .local p3, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1557
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1558
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1559
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->xuid:Ljava/lang/String;

    .line 1560
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 1561
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsTransferOwnershipSubmit(JLjava/lang/String;)V

    .line 1562
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->clubAccountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    .line 1563
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;

    .prologue
    .line 1550
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1567
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7

    .prologue
    .line 1581
    const/4 v2, 0x0

    .line 1584
    .local v2, "success":Z
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->clubAccountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->xuid:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v6}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->transferOwnership(JLjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v1

    .line 1585
    .local v1, "response":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->owner()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->owner()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->xuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 1590
    .end local v1    # "response":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_1
    return-object v3

    .line 1585
    .restart local v1    # "response":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1586
    .end local v1    # "response":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :catch_0
    move-exception v0

    .line 1587
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to transfer club ownership to xuid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1590
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1550
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1576
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1550
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1572
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1599
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1606
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 1607
    return-void

    .line 1602
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->xuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$900(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/lang/String;)V

    goto :goto_0

    .line 1599
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1550
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$TransferOwnershipTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1595
    return-void
.end method
