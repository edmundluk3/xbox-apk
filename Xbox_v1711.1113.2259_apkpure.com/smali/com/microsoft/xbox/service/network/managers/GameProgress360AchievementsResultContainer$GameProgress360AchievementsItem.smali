.class public Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
.super Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
.source "GameProgress360AchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProgress360AchievementsItem"
.end annotation


# static fields
.field private static final ImageStoragePathFormat:Ljava/lang/String; = "//global/t:%s/ach/0/%s"

.field private static transient dateFormat:Ljava/text/DateFormat;

.field private static defaultBackgroundColor:[B

.field private static predimmedBackgroundColor:[B


# instance fields
.field public gamerscore:I

.field public imageId:I

.field private tileImageUri:Ljava/lang/String;

.field public timeUnlocked:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;
    .end annotation
.end field

.field public titleId:I

.field public unlocked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 28
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->defaultBackgroundColor:[B

    .line 29
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->predimmedBackgroundColor:[B

    .line 31
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->dateFormat:Ljava/text/DateFormat;

    return-void

    .line 28
    nop

    :array_0
    .array-data 1
        -0x19t
        -0x19t
        -0x19t
    .end array-data

    .line 29
    :array_1
    .array-data 1
        0x50t
        0x50t
        0x50t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;-><init>()V

    return-void
.end method

.method private getTileSubPath(Ljava/lang/String;SZZ)Ljava/lang/String;
    .locals 25
    .param p1, "storagePath"    # Ljava/lang/String;
    .param p2, "dimensions"    # S
    .param p3, "achieved"    # Z
    .param p4, "hidden"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v22

    const v23, 0xffff

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_0

    .line 52
    new-instance v22, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v23, "storagePath length should fit in short"

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 55
    :cond_0
    const/4 v13, 0x0

    .line 56
    .local v13, "fallback":B
    const/16 v22, 0x20

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 57
    const/4 v13, 0x1

    .line 60
    :cond_1
    const/4 v14, 0x0

    .line 61
    .local v14, "flag":B
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->defaultBackgroundColor:[B

    .line 62
    .local v4, "backgroundColor":[B
    if-nez p3, :cond_4

    .line 63
    const/16 v22, 0x1

    move/from16 v0, v22

    int-to-byte v14, v0

    .line 64
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->predimmedBackgroundColor:[B

    .line 66
    if-eqz p4, :cond_2

    .line 67
    or-int/lit8 v22, v14, 0x10

    move/from16 v0, v22

    int-to-byte v14, v0

    .line 74
    :cond_2
    :goto_0
    if-nez p3, :cond_3

    if-eqz p4, :cond_3

    .line 75
    const-string p1, ""

    .line 78
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v22

    int-to-char v0, v0

    move/from16 v19, v0

    .line 82
    .local v19, "storagePathLength":C
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 85
    .local v6, "byteOutputStream":Ljava/io/ByteArrayOutputStream;
    const/16 v22, 0x2

    invoke-static/range {v22 .. v22}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 86
    .local v5, "buffer":Ljava/nio/ByteBuffer;
    sget-object v22, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 87
    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 88
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 89
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x2

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 92
    const-string v22, "UTF-8"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v19

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 95
    const/16 v22, 0x2

    invoke-static/range {v22 .. v22}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 96
    .local v9, "dimensionBuffer":Ljava/nio/ByteBuffer;
    sget-object v22, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 97
    move/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 98
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 99
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x2

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 102
    invoke-virtual {v6, v13}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 105
    invoke-virtual {v6, v14}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 108
    const/16 v22, 0x0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v4, v0, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 110
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v17

    .line 113
    .local v17, "output":[B
    invoke-static/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/CRC32;->ComputeCrc32Hash([B)J

    move-result-wide v10

    .line 114
    .local v10, "crcValue":J
    const/16 v22, 0x8

    invoke-static/range {v22 .. v22}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 115
    .local v8, "crcBuffer":Ljava/nio/ByteBuffer;
    sget-object v22, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 116
    invoke-virtual {v8, v10, v11}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 117
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 118
    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    .line 121
    .local v7, "bytes":[B
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    array-length v0, v7

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x4

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    .line 122
    aget-byte v21, v7, v16

    .line 123
    .local v21, "temp":B
    array-length v0, v7

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    sub-int v22, v22, v16

    add-int/lit8 v22, v22, -0x1

    aget-byte v22, v7, v22

    aput-byte v22, v7, v16

    .line 124
    array-length v0, v7

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    sub-int v22, v22, v16

    add-int/lit8 v22, v22, -0x1

    aput-byte v21, v7, v22

    .line 121
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 70
    .end local v5    # "buffer":Ljava/nio/ByteBuffer;
    .end local v6    # "byteOutputStream":Ljava/io/ByteArrayOutputStream;
    .end local v7    # "bytes":[B
    .end local v8    # "crcBuffer":Ljava/nio/ByteBuffer;
    .end local v9    # "dimensionBuffer":Ljava/nio/ByteBuffer;
    .end local v10    # "crcValue":J
    .end local v16    # "i":I
    .end local v17    # "output":[B
    .end local v19    # "storagePathLength":C
    .end local v21    # "temp":B
    :cond_4
    sget-object v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->defaultBackgroundColor:[B

    goto/16 :goto_0

    .line 127
    .restart local v5    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v6    # "byteOutputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v7    # "bytes":[B
    .restart local v8    # "crcBuffer":Ljava/nio/ByteBuffer;
    .restart local v9    # "dimensionBuffer":Ljava/nio/ByteBuffer;
    .restart local v10    # "crcValue":J
    .restart local v16    # "i":I
    .restart local v17    # "output":[B
    .restart local v19    # "storagePathLength":C
    :cond_5
    const/16 v22, 0x0

    array-length v0, v7

    move/from16 v23, v0

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v7, v0, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 129
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v17

    .line 135
    if-eqz v17, :cond_7

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    const v23, 0xffff

    move/from16 v0, v22

    move/from16 v1, v23

    if-gt v0, v1, :cond_7

    .line 136
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    add-int/lit8 v15, v22, -0x1

    .line 137
    .local v15, "half":I
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    add-int/lit8 v12, v22, -0x1

    .line 139
    .local v12, "end":I
    const/16 v16, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v0, v15, :cond_6

    .line 140
    aget-byte v22, v17, v16

    sub-int v23, v12, v16

    aget-byte v23, v17, v23

    xor-int v22, v22, v23

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, v17, v16

    .line 139
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 143
    :cond_6
    const/16 v22, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v18

    .line 144
    .local v18, "rawPath":Ljava/lang/String;
    const/16 v22, 0x2f

    const/16 v23, 0x2d

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v18

    .line 146
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v20, "subPathBuilder":Ljava/lang/StringBuilder;
    const-string v22, "/"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const/16 v22, 0x0

    const/16 v23, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string v22, "/"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const/16 v22, 0x2

    const/16 v23, 0x4

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string v22, "/"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const/16 v22, 0x4

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v22, ".jpg"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 158
    .end local v12    # "end":I
    .end local v15    # "half":I
    .end local v18    # "rawPath":Ljava/lang/String;
    .end local v20    # "subPathBuilder":Ljava/lang/StringBuilder;
    :goto_3
    return-object v22

    :cond_7
    const/16 v22, 0x0

    goto :goto_3
.end method

.method private getTileUrl()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 43
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "//global/t:%s/ach/0/%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->titleId:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->imageId:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "storagePath":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->unlocked:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->isSecret:Z

    invoke-direct {p0, v0, v6, v2, v3}, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->getTileSubPath(Ljava/lang/String;SZZ)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "subPath":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getTileImageUrlFormat()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getAchievementIconUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->tileImageUri:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 190
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->getTileUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->tileImageUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->tileImageUri:Ljava/lang/String;

    return-object v1

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "GameProgress360AchievementsItem"

    const-string v2, "Unexpected error while fetching achievement tile image for 360"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getGamerscore()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->gamerscore:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->unlocked:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->NotStarted:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    goto :goto_0
.end method

.method public getRemainingTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->Xbox360:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    return-object v0
.end method

.method public getTimeUnlocked()Ljava/util/Date;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->timeUnlocked:Ljava/util/Date;

    return-object v0
.end method

.method public hasRewards()Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method public isExpired()Z
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method protected setPercentageComplete()V
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->unlocked:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->percentageComplete:I

    .line 213
    return-void

    .line 212
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
