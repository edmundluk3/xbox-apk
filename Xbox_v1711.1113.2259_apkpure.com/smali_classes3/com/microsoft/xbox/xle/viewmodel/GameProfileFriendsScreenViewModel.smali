.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "GameProfileFriendsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;
    }
.end annotation


# instance fields
.field private final clubMembershipVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final clubRecommendationsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private friendsWhoPlayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private final friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private gameTitleId:J

.field private lastSelectedPosition:I

.field private peopleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private vipsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private final vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 4
    .param p1, "viewModels"    # [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 44
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->gameTitleId:J

    .line 45
    const/4 v2, -0x1

    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->lastSelectedPosition:I

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileFriendsScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 50
    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    .line 51
    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    .line 52
    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubMembershipVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    .line 53
    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubRecommendationsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    .line 55
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 56
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 57
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubMembershipVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubRecommendationsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->createNewPeopleList(Z)V

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 63
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 64
    .local v1, "selectedItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->gameTitleId:J

    .line 65
    return-void

    .line 64
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method private addItems(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;I)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x1

    .line 163
    if-eqz p1, :cond_0

    .line 164
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 166
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;->getData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    goto :goto_0

    .line 169
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/service/model/FollowersData;

    instance-of v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_NO_DATA_VIPS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    :goto_1
    invoke-direct {v2, v3, v0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_NO_DATA:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    goto :goto_1

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_ERROR:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private createNewPeopleList(Z)V
    .locals 4
    .param p1, "showLoadingText"    # Z

    .prologue
    const/4 v3, 0x1

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_WHO_PLAY:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_VIPS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    if-eqz p1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LOADING:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LOADING:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_0
    return-void
.end method

.method private updateList()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 146
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getData()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v0, v2

    .line 147
    .local v0, "friendsWhoPlay":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_3

    move-object v1, v2

    .line 148
    .local v1, "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayList:Ljava/util/ArrayList;

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsList:Ljava/util/ArrayList;

    if-eq v2, v1, :cond_1

    .line 150
    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayList:Ljava/util/ArrayList;

    .line 151
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsList:Ljava/util/ArrayList;

    .line 152
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->createNewPeopleList(Z)V

    .line 153
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->addItems(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;I)V

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->addItems(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;I)V

    .line 156
    :cond_1
    return-void

    .line 146
    .end local v0    # "friendsWhoPlay":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v1    # "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getData()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 147
    .restart local v0    # "friendsWhoPlay":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method


# virtual methods
.method public areBothListEmpty()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->vipsList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 88
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->hasSpotlight()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 89
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getSpotlightData()Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubMembershipVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getClubMembershipList()Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    move-result-object v2

    .line 93
    .local v2, "gameProfileClubMembershipListItem":Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;
    if-eqz v2, :cond_1

    .line 94
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubRecommendationsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->isBusy()Z

    move-result v4

    if-nez v4, :cond_2

    .line 98
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->clubRecommendationsVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getRecommendationCategory()Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    move-result-object v3

    .line 99
    .local v3, "gameProfileClubRecommendationsListItem":Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
    if-eqz v3, :cond_2

    .line 100
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    .end local v3    # "gameProfileClubRecommendationsListItem":Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 105
    .local v1, "followersData":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    invoke-direct {v5, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;Lcom/microsoft/xbox/service/model/FollowersData;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    .end local v1    # "followersData":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_3
    return-object v0
.end method

.method public getFriendsWhoPlayCount()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->getPeopleCount()I

    move-result v0

    return v0
.end method

.method public getGameTitleId()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method public getLastSelectedPosition()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->lastSelectedPosition:I

    return v0
.end method

.method public gotoGamerProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 2
    .param p1, "gamer"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 124
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 126
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsDummy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 128
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 131
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 132
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowFriendAction(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 134
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 136
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public launchRecommendations(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "criteria"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 139
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 140
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    invoke-direct {v0, p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .local v0, "params":Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 143
    return-void
.end method

.method protected onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "child"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 76
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->updateList()V

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->updateAdapter()V

    .line 78
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onRehydrate()V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileFriendsScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 71
    return-void
.end method

.method public setLastSelectedPosition(I)V
    .locals 0
    .param p1, "lastSelectedPosition"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->lastSelectedPosition:I

    .line 121
    return-void
.end method
