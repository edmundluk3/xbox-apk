.class Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PageUserInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleFollowUnfollowAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;

    .prologue
    .line 289
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 293
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 294
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 321
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v3, v4, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFollowingPageState(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_1
    return-object v1

    .line 321
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 299
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 311
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 289
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 303
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Z)Z

    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateAdapter()V

    .line 306
    return-void
.end method
