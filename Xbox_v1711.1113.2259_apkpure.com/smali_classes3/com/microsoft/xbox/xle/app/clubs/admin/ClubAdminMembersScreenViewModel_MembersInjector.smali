.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;
.super Ljava/lang/Object;
.source "ClubAdminMembersScreenViewModel_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final userSummaryRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "userSummaryRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p2, "userSummaryFromUserSearchResultDataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->userSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 26
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;

    .line 29
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "userSummaryRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p1, "userSummaryFromUserSearchResultDataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectUserSummaryFromUserSearchResultDataMapper(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .line 60
    .local p1, "userSummaryFromUserSearchResultDataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryFromUserSearchResultDataMapper:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;

    .line 61
    return-void
.end method

.method public static injectUserSummaryRepository(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "userSummaryRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 53
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->userSummaryRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;

    .line 46
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryFromUserSearchResultDataMapper:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;

    .line 47
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->injectMembers(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V

    return-void
.end method
