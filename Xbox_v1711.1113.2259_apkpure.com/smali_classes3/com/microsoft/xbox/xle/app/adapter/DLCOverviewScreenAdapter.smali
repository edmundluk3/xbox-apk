.class public Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "DLCOverviewScreenAdapter.java"


# instance fields
.field private consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

.field private descriptionTextView:Landroid/widget/TextView;

.field private downloadButton:Landroid/widget/RelativeLayout;

.field private durablePurchaseButton:Landroid/widget/RelativeLayout;

.field private durablePurchaseButtonIconView:Landroid/widget/TextView;

.field private durablePurchaseButtonLabel:Landroid/widget/TextView;

.field private durablePurchaseButtonText:Landroid/widget/TextView;

.field private genreTextView:Landroid/widget/TextView;

.field private languagesTextView:Landroid/widget/TextView;

.field private publisherTextView:Landroid/widget/TextView;

.field private purchaseButtonStrikethroughText:Landroid/widget/TextView;

.field private purchasedDateTextView:Landroid/widget/TextView;

.field private purchasedTextLayout:Landroid/widget/LinearLayout;

.field private ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

.field private releaseDateTextView:Landroid/widget/TextView;

.field private subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private subscriptionText:Landroid/widget/TextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 56
    const v0, 0x7f0e0510

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 57
    const v0, 0x7f0e0511

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    .line 59
    const v0, 0x7f0e0518

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    .line 60
    const v0, 0x7f0e051d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->descriptionTextView:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0e051e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->publisherTextView:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0e0521

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->genreTextView:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0e0522

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->releaseDateTextView:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0e0523

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->languagesTextView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0e0515

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->downloadButton:Landroid/widget/RelativeLayout;

    .line 66
    const v0, 0x7f0e0512

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    .line 67
    const v0, 0x7f0e096b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonText:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0e0968

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonLabel:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0e096a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0e0514

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    .line 71
    const v0, 0x7f0e051a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    .line 72
    const v0, 0x7f0e051c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0e096c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 74
    const v0, 0x7f0e096d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0e0967

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonIconView:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 80
    :cond_0
    return-void
.end method

.method private addPurchaseButton(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)V
    .locals 8
    .param p1, "purchaseItem"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    .prologue
    const/4 v7, 0x0

    .line 201
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_0

    if-eqz p1, :cond_0

    .line 204
    iget-object v5, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuTitle:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->price:Ljava/math/BigDecimal;

    if-nez v5, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 209
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f0301dd

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 211
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 212
    const v5, 0x7f0e0990

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 213
    .local v2, "titleView":Landroid/widget/TextView;
    const v5, 0x7f0e098f

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 214
    .local v1, "priceView":Landroid/widget/TextView;
    iget-object v5, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuTitle:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->getConsumableDisplayPriceText(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    invoke-virtual {v3, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 218
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v0, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 225
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v5, 0x41000000    # 8.0f

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v5

    invoke-virtual {v0, v7, v7, v7, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 227
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getConsumableDisplayPriceText(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)Ljava/lang/String;
    .locals 2
    .param p1, "purchaseItem"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    .prologue
    .line 237
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->price:Ljava/math/BigDecimal;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->currencyCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    :cond_0
    const-string v0, ""

    .line 241
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->price:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070afd

    .line 242
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->price:Ljava/math/BigDecimal;

    iget-object v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->currencyCode:Ljava/lang/String;

    .line 243
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDisplayPriceText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getIsFree()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070afd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$addPurchaseButton$2(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;
    .param p1, "v1"    # Landroid/view/View;

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->purchaseConsumableDLC(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)V

    .line 222
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->launchDLC()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->purchaseDurable()V

    return-void
.end method

.method private updateSubscriptionDetails()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 247
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getSubscriptionText()Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "subscriptionName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getSubscriptionImageResourceId()I

    move-result v0

    .line 250
    .local v0, "subscriptionImageResourceId":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 251
    if-lez v0, :cond_1

    .line 252
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 253
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 254
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 256
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 258
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 260
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 261
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 262
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->downloadButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->downloadButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->downloadButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->downloadButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 106
    return-void
.end method

.method public updateViewOverride()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->isViewModelBroken()Z

    move-result v3

    if-nez v3, :cond_2

    .line 111
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 113
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 115
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v3

    if-nez v3, :cond_9

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonLabel:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getPurchaseButtonLabelText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 117
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 118
    .local v0, "itemType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getShowPurchaseButton()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 119
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v3, :cond_5

    .line 120
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 121
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 122
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 123
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonLabel:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getIsFree()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_0
    invoke-static {v7, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 124
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->getDisplayPriceText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 126
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 128
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 134
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->updateSubscriptionDetails()V

    .line 175
    .end local v0    # "itemType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->descriptionTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 177
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->publisherTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    move v3, v6

    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703cc

    .line 178
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 177
    invoke-static {v4, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 180
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->genreTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    move v3, v6

    :goto_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703c8

    .line 181
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 180
    invoke-static {v4, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 183
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->releaseDateTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    move v3, v6

    :goto_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0703ce

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    .line 184
    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 183
    invoke-static {v4, v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 186
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->languagesTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getLanguages()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_d

    :goto_6
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getLanguages()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 190
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 192
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V

    .line 193
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->ratingLevelView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setVisibility(I)V

    .line 198
    :cond_2
    return-void

    .restart local v0    # "itemType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_3
    move v3, v5

    .line 123
    goto/16 :goto_0

    .line 131
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_1

    .line 135
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v3, :cond_1

    .line 136
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 140
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getConsumablePriceList()Ljava/util/List;

    move-result-object v2

    .line 141
    .local v2, "purchaseItemList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;>;"
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 142
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 143
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_1

    .line 145
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 146
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    .line 147
    .local v1, "purchaseItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->addPurchaseButton(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)V

    goto :goto_7

    .line 151
    .end local v1    # "purchaseItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 155
    .end local v2    # "purchaseItemList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;>;"
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 156
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 157
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 158
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->subscriptionText:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 160
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v3, :cond_8

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 161
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 162
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 164
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 168
    .end local v0    # "itemType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 169
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->consumablePurchaseButtonsList:Landroid/widget/LinearLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 170
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 171
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->durablePurchaseButtonLabel:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 172
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    :cond_a
    move v3, v5

    .line 177
    goto/16 :goto_3

    :cond_b
    move v3, v5

    .line 180
    goto/16 :goto_4

    :cond_c
    move v3, v5

    .line 183
    goto/16 :goto_5

    :cond_d
    move v6, v5

    .line 186
    goto/16 :goto_6
.end method
