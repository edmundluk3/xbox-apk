.class public Lcom/microsoft/xbox/xle/app/activity/PinsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PinsScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "Pins"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PinsScreen;->onCreateContentView()V

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModelPhone;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModelPhone;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PinsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 14
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    const v0, 0x7f0301d3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/PinsScreen;->setContentView(I)V

    .line 24
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1, v1}, Lcom/microsoft/xbox/xle/app/activity/PinsScreen;->setAppBarLayout(IZZ)V

    .line 25
    return-void
.end method
