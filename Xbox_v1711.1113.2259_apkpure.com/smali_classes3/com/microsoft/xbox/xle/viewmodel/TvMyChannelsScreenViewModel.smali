.class public Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;
.source "TvMyChannelsScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->ALL_FAVORITE_CHANNELS:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;-><init>(I)V

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvMyChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 23
    const-string v0, "EPGTvMyChannelsScreenViewModel"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getHasData()Z
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->getTVChannelsCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHasTVChannelErrors()Z
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTVChannelsCount()I
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvMyChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 42
    return-void
.end method

.method protected refresh(Z)V
    .locals 1
    .param p1, "isExplicit"    # Z

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->ResetFavoritesState()V

    .line 36
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->refresh(Z)V

    .line 37
    return-void
.end method

.method protected setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->isActive:Z

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method protected updateViewModelState()V
    .locals 4

    .prologue
    .line 48
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->modelChanged:Z

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->updateLoadingStatus()V

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->isLoading:Z

    if-eqz v2, :cond_0

    .line 82
    :goto_0
    return-void

    .line 59
    :cond_0
    sget v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->hasFlag(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->modelChanged:Z

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->updateTvChannels()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->modelChanged:Z

    .line 64
    :cond_1
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->modelChanged:Z

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .line 66
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;->modelChanged()V

    goto :goto_1

    .line 73
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->getHasTVChannelErrors()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 81
    .local v1, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :goto_2
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 75
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;->getChannelCount()I

    move-result v2

    if-nez v2, :cond_4

    .line 76
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2

    .line 78
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2
.end method
