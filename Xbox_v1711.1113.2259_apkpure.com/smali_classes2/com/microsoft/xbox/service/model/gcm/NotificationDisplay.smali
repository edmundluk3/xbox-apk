.class public Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
.super Ljava/lang/Object;
.source "NotificationDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;
    }
.end annotation


# static fields
.field public static final EXTRA_CLUB_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.CLUB_ID"

.field public static final EXTRA_HANDLE_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.HANDLE_ID"

.field public static final EXTRA_IN_APP:Ljava/lang/String; = "com.microsoft.xbox.extra.IN_APP"

.field public static final EXTRA_NOTIFICATION_SUBTYPE:Ljava/lang/String; = "com.microsoft.xbox.extra.NOTIFICATION_SUBTYPE"

.field public static final EXTRA_NOTIFICATION_TYPE:Ljava/lang/String; = "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

.field public static final EXTRA_SESSION_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.SESSION_ID"

.field public static final EXTRA_THREAD_TOPIC:Ljava/lang/String; = "com.microsoft.xbox.extra.THREAD_TOPIC"

.field public static final EXTRA_TITLE_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.TITLE_ID"

.field public static final EXTRA_TOURNAMENT_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.TOURNAMENT_ID"

.field public static final EXTRA_TOURNAMENT_ORGANIZER:Ljava/lang/String; = "com.microsoft.xbox.extra.TOURNAMENT_ORGANIZER"

.field public static final EXTRA_XUID:Ljava/lang/String; = "com.microsoft.xbox.extra.XUID"

.field private static final IN_APP_NOTIFICATION_TIMEOUT:J = 0xbb8L

.field private static final LIGHTS_COLOR:I = -0xff0100

.field private static final LIGHTS_MS_OFF:I = 0x7d0

.field private static final LIGHTS_MS_ON:I = 0x1f4

.field private static final MAX_NOTIFICATIONS:I = 0xa

.field protected static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

.field private static final lock:Ljava/lang/Object;


# instance fields
.field private final ctx:Landroid/content/Context;

.field private curNotificationId:I

.field private inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

.field private final messageIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final nm:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->messageIds:Ljava/util/HashSet;

    .line 76
    const/16 v0, 0x9

    iput v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    .line 80
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    .line 81
    return-void
.end method

.method private createAchievementIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "titleId"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 484
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 485
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 486
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 487
    const-string v1, "com.microsoft.xbox.extra.TITLE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 489
    return-object v0
.end method

.method private createAchievementPendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "titleId"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 536
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createAchievementIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createClubIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "notificationType"    # Ljava/lang/String;
    .param p2, "clubId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z

    .prologue
    .line 524
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 525
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 526
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 527
    const-string v1, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 528
    return-object v0
.end method

.method private createClubPendingIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "notificationType"    # Ljava/lang/String;
    .param p2, "clubId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z

    .prologue
    .line 458
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createClubIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createFavBroadcastIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 475
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 476
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    const-string v1, "com.microsoft.xbox.extra.XUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 478
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 479
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 480
    return-object v0
.end method

.method private createFavBroadcastPendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 532
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavBroadcastIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createFavOnlineIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 433
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 434
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    const-string v1, "com.microsoft.xbox.extra.XUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 437
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 438
    return-object v0
.end method

.method private createFavOnlinePendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "inApp"    # Z

    .prologue
    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavOnlineIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createLfgHasApplicantIntent(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "handleId"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z
    .param p4, "lfgSubtype"    # Ljava/lang/String;

    .prologue
    .line 493
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 494
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_SUBTYPE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 497
    const-string v1, "com.microsoft.xbox.extra.TITLE_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    const-string v1, "com.microsoft.xbox.extra.HANDLE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 501
    return-object v0
.end method

.method private createLfgHasApplicantPendingIntent(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "handleId"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z
    .param p4, "lfgSubtype"    # Ljava/lang/String;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createLfgHasApplicantIntent(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createMessageIntent(ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "inApp"    # Z
    .param p2, "senderXuid"    # Ljava/lang/String;
    .param p3, "threadtopic"    # Ljava/lang/String;

    .prologue
    .line 540
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 541
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    const-string v1, "com.microsoft.xbox.extra.XUID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 543
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 545
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    const-string v1, "com.microsoft.xbox.extra.THREAD_TOPIC"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    :cond_0
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 550
    return-object v0
.end method

.method private createMessagePendingIntent(ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "inApp"    # Z
    .param p2, "senderXuid"    # Ljava/lang/String;
    .param p3, "topic"    # Ljava/lang/String;

    .prologue
    .line 554
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createMessageIntent(ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "pendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->getApplicationLabel()Ljava/lang/CharSequence;

    move-result-object v1

    .line 390
    .local v1, "title":Ljava/lang/CharSequence;
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 392
    .local v0, "bld":Landroid/app/Notification$Builder;
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 393
    invoke-virtual {v2, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 394
    invoke-virtual {v3, p1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f02013b

    .line 395
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 396
    invoke-virtual {v2, p1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 397
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, -0xff0100

    const/16 v4, 0x1f4

    const/16 v5, 0x7d0

    .line 398
    invoke-virtual {v2, v3, v4, v5}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x2

    .line 399
    invoke-static {v3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 400
    invoke-virtual {v2, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 402
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method private createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "pendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 406
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 407
    .local v0, "bld":Landroid/app/Notification$Builder;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s, %s"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    aput-object p2, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 409
    .local v1, "ticker":Ljava/lang/String;
    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 410
    invoke-virtual {v2, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 411
    invoke-virtual {v3, p2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f02013b

    .line 412
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 413
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 414
    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, -0xff0100

    const/16 v4, 0x1f4

    const/16 v5, 0x7d0

    .line 415
    invoke-virtual {v2, v3, v4, v5}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 416
    invoke-static {v7}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 417
    invoke-virtual {v2, p3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 419
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method private createPartyInviteIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "handleId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z

    .prologue
    .line 505
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 506
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 507
    const-string v1, "com.microsoft.xbox.extra.SESSION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    const-string v1, "com.microsoft.xbox.extra.HANDLE_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 509
    const-string v1, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 510
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 512
    return-object v0
.end method

.method private createPartyInvitePendingIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "handleId"    # Ljava/lang/String;
    .param p3, "inApp"    # Z

    .prologue
    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyInviteIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createPartyShowIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 516
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 517
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 518
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 520
    return-object v0
.end method

.method private createPartyShowPendingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 454
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyShowIntent()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createTournamentIntent(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .param p2, "organizer"    # Ljava/lang/String;
    .param p3, "tournamentId"    # Ljava/lang/String;

    .prologue
    .line 466
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const-class v2, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 467
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    const-string v1, "com.microsoft.xbox.extra.TOURNAMENT_ORGANIZER"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 469
    const-string v1, "com.microsoft.xbox.extra.TOURNAMENT_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 471
    return-object v0
.end method

.method private createTournamentPendingIntent(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .param p2, "organizer"    # Ljava/lang/String;
    .param p3, "tournamentID"    # Ljava/lang/String;

    .prologue
    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createTournamentIntent(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V
    .locals 2
    .param p1, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 336
    if-eqz p1, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-eq p1, v0, :cond_0

    .line 339
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nextNotificationId(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 342
    :cond_1
    return-void
.end method

.method private displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 348
    if-eqz p3, :cond_2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-eq p3, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-eq p3, v3, :cond_0

    .line 351
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v3

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v4

    and-int/2addr v3, v4

    if-eqz v3, :cond_2

    .line 352
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 354
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 355
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 356
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->dismiss()V

    .line 357
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    .line 360
    :cond_1
    new-instance v3, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-direct {v3, v0, p1, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    .line 362
    iget v2, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    .line 363
    .local v2, "curNotifId":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    const v4, 0x7f0e07dd

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 364
    .local v1, "body":Landroid/view/View;
    invoke-static {p0, v0, p2, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/content/Intent;I)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xle/app/MainActivity;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 383
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->show()V

    .line 386
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .end local v1    # "body":Landroid/view/View;
    .end local v2    # "curNotifId":I
    :cond_2
    return-void
.end method

.method public static ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    if-nez v0, :cond_1

    .line 89
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    .line 93
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static generateTemplatedNotificationBody(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "messageFormat"    # Ljava/lang/String;
    .param p1, "messageArgs"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/util/MissingFormatArgumentException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "argsArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    new-array v1, v3, [Ljava/lang/String;

    .line 122
    .local v1, "argsStrings":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 123
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 126
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "argsStrings":[Ljava/lang/String;
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .end local p0    # "messageFormat":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method private getApplicationLabel()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 423
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 425
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 426
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 428
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return-object v3

    .line 427
    :catch_0
    move-exception v1

    .line 428
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const v4, 0x7f070dac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->instance:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    return-object v0
.end method

.method static synthetic lambda$displayOptionalInAppNotification$0(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/content/Intent;ILandroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p1, "activity"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "curNotifId"    # I
    .param p4, "v"    # Landroid/view/View;

    .prologue
    .line 365
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->dismiss()V

    .line 367
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    .line 370
    :cond_0
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    invoke-virtual {v1, p3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 375
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v2, "Could not find destination activity"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$displayOptionalInAppNotification$1(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Lcom/microsoft/xbox/xle/app/MainActivity;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p1, "activity"    # Lcom/microsoft/xbox/xle/app/MainActivity;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->dismiss()V

    .line 379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    .line 381
    :cond_0
    return-void
.end method

.method private nextNotificationId(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)I
    .locals 2
    .param p1, "nt"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 324
    iget v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->messageIds:Ljava/util/HashSet;

    iget v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 326
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne p1, v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->messageIds:Ljava/util/HashSet;

    iget v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 329
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->curNotificationId:I

    return v0
.end method


# virtual methods
.method public displayAchievementNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "achievementTitle"    # Ljava/lang/String;
    .param p2, "achievementBody"    # Ljava/lang/String;
    .param p3, "titleId"    # Ljava/lang/String;

    .prologue
    .line 113
    const/4 v1, 0x0

    invoke-direct {p0, p3, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createAchievementPendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    .line 114
    .local v0, "n":Landroid/app/Notification;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, p3, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createAchievementIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 116
    return-void
.end method

.method public displayChatNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;)V
    .locals 12
    .param p1, "titleFormat"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "titleArgs"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "notificationDetails"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 218
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 220
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "displayChatNotification: titleFormat:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " titleArgs:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " notificationDetails:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 225
    .local v0, "argsArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    new-array v1, v8, [Ljava/lang/String;

    .line 227
    .local v1, "argsStrings":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 228
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v4

    .line 227
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 231
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "argsStrings":[Ljava/lang/String;
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 233
    .local v7, "title":Ljava/lang/String;
    :goto_1
    if-eqz p3, :cond_2

    iget-object v8, p3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    if-eqz v8, :cond_2

    iget-object v5, p3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    .line 236
    .local v5, "message":Ljava/lang/String;
    :goto_2
    const-string v8, " "

    const-string v9, "&nbsp;"

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 238
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->getChatChannel()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v2

    .line 239
    .local v2, "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :goto_3
    if-eqz v2, :cond_5

    sget-object v8, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->getCurrentDisplayedChannel()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 240
    instance-of v8, v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    if-eqz v8, :cond_4

    .line 241
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 242
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v8

    .line 243
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    .line 241
    invoke-direct {p0, v8, v9, v10}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createClubPendingIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-direct {p0, v7, v5, v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v6

    .line 245
    .local v6, "n":Landroid/app/Notification;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v8, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 246
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 247
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v9

    .line 248
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    .line 246
    invoke-direct {p0, v9, v10, v11}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createClubIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v8, v9, v10}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 262
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v2    # "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .end local v4    # "i":I
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "n":Landroid/app/Notification;
    .end local v7    # "title":Ljava/lang/String;
    :goto_4
    return-void

    .restart local v0    # "argsArray":Lorg/json/JSONArray;
    .restart local v1    # "argsStrings":[Ljava/lang/String;
    .restart local v4    # "i":I
    :cond_1
    move-object v7, p1

    .line 231
    goto :goto_1

    .line 233
    .end local v1    # "argsStrings":[Ljava/lang/String;
    .restart local v7    # "title":Ljava/lang/String;
    :cond_2
    const-string v5, ""

    goto :goto_2

    .line 238
    .restart local v5    # "message":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 251
    .restart local v2    # "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :cond_4
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "displayChatNotification - unsupported channelType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_4

    .line 257
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v2    # "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .end local v4    # "i":I
    .end local v5    # "message":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 258
    .local v3, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v9, "Could not parse message title"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 254
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v0    # "argsArray":Lorg/json/JSONArray;
    .restart local v2    # "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .restart local v4    # "i":I
    .restart local v5    # "message":Ljava/lang/String;
    .restart local v7    # "title":Ljava/lang/String;
    :cond_5
    :try_start_1
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v9, "displayChatNotification - channelId is null"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 259
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v2    # "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .end local v4    # "i":I
    .end local v5    # "message":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 260
    .local v3, "e":Ljava/util/MissingFormatArgumentException;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not format notification title with ID with format: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and arguments: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public displayClubsNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;)V
    .locals 9
    .param p1, "messageFormat"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "messageArgs"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "notificationType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "notificationDetails"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 177
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 178
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 182
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "argsArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    new-array v1, v6, [Ljava/lang/String;

    .line 185
    .local v1, "argsStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 186
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    .line 185
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "argsStrings":[Ljava/lang/String;
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 190
    .local v4, "messageBody":Ljava/lang/String;
    :goto_1
    if-eqz p4, :cond_2

    .line 192
    invoke-virtual {p4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubId()Ljava/lang/String;

    move-result-object v6

    :goto_2
    const/4 v7, 0x0

    .line 190
    invoke-direct {p0, p3, v6, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createClubPendingIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-direct {p0, v4, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v5

    .line 194
    .local v5, "n":Landroid/app/Notification;
    invoke-static {p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v6

    invoke-direct {p0, v6, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 195
    if-eqz p4, :cond_3

    .line 197
    invoke-virtual {p4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubId()Ljava/lang/String;

    move-result-object v6

    :goto_3
    const/4 v7, 0x1

    .line 195
    invoke-direct {p0, p3, v6, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createClubIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v6

    .line 198
    invoke-static {p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v7

    .line 195
    invoke-direct {p0, v4, v6, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 204
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v4    # "messageBody":Ljava/lang/String;
    .end local v5    # "n":Landroid/app/Notification;
    :goto_4
    return-void

    .restart local v0    # "argsArray":Lorg/json/JSONArray;
    .restart local v1    # "argsStrings":[Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_1
    move-object v4, p1

    .line 189
    goto :goto_1

    .line 192
    .end local v1    # "argsStrings":[Ljava/lang/String;
    .restart local v4    # "messageBody":Ljava/lang/String;
    :cond_2
    const-string v6, ""

    goto :goto_2

    .line 197
    .restart local v5    # "n":Landroid/app/Notification;
    :cond_3
    const-string v6, ""
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_3

    .line 199
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v4    # "messageBody":Ljava/lang/String;
    .end local v5    # "n":Landroid/app/Notification;
    :catch_0
    move-exception v2

    .line 200
    .local v2, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v7, "Could not parse message body"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 201
    .end local v2    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v2

    .line 202
    .local v2, "e":Ljava/util/MissingFormatArgumentException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not format notification with ID with format: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and arguments: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public displayFavBroadcast(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const v3, 0x7f070b07

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "message":Ljava/lang/String;
    invoke-direct {p0, p2, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavBroadcastPendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v1

    .line 108
    .local v1, "n":Landroid/app/Notification;
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 109
    invoke-direct {p0, p2, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavBroadcastIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 110
    return-void
.end method

.method public displayFavOnline(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    const v3, 0x7f070b0b

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "message":Ljava/lang/String;
    invoke-direct {p0, p2, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavOnlinePendingIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v1

    .line 101
    .local v1, "n":Landroid/app/Notification;
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 102
    invoke-direct {p0, p2, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createFavOnlineIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 103
    return-void
.end method

.method public displayLfgNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Ljava/lang/String;)V
    .locals 6
    .param p1, "messageFormat"    # Ljava/lang/String;
    .param p2, "messageArgs"    # Ljava/lang/String;
    .param p3, "searchHandle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .param p4, "lfgSubtype"    # Ljava/lang/String;

    .prologue
    .line 156
    :try_start_0
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->generateTemplatedNotificationBody(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "messageBody":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v4

    .line 159
    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v3

    :goto_0
    const/4 v5, 0x0

    .line 157
    invoke-direct {p0, v4, v3, v5, p4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createLfgHasApplicantPendingIntent(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    .line 162
    .local v2, "n":Landroid/app/Notification;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 164
    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v4

    .line 165
    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v3

    :goto_1
    const/4 v5, 0x1

    .line 163
    invoke-direct {p0, v4, v3, v5, p4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createLfgHasApplicantIntent(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 173
    .end local v1    # "messageBody":Ljava/lang/String;
    .end local v2    # "n":Landroid/app/Notification;
    :goto_2
    return-void

    .line 159
    .restart local v1    # "messageBody":Ljava/lang/String;
    :cond_0
    const-string v3, ""

    goto :goto_0

    .line 165
    .restart local v2    # "n":Landroid/app/Notification;
    :cond_1
    const-string v3, ""
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 168
    .end local v1    # "messageBody":Ljava/lang/String;
    .end local v2    # "n":Landroid/app/Notification;
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v4, "Could not parse message body"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 170
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 171
    .local v0, "e":Ljava/util/MissingFormatArgumentException;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not format notification with ID with format: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public varargs displayLocalPartyNotification(I[Ljava/lang/Object;)V
    .locals 4
    .param p1, "stringId"    # I
    .param p2, "stringArgs"    # [Ljava/lang/Object;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyShowPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    .line 150
    .local v0, "n":Landroid/app/Notification;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ctx:Landroid/content/Context;

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyShowIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 152
    return-void
.end method

.method public displayMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public displayMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "senderXuid"    # Ljava/lang/String;
    .param p3, "threadTopic"    # Ljava/lang/String;

    .prologue
    .line 211
    const/4 v1, 0x0

    invoke-direct {p0, v1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createMessagePendingIntent(ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    .line 212
    .local v0, "n":Landroid/app/Notification;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 213
    const/4 v1, 0x1

    invoke-direct {p0, v1, p2, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createMessageIntent(ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 214
    return-void
.end method

.method public displayPartyInviteNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;)V
    .locals 6
    .param p1, "messageFormat"    # Ljava/lang/String;
    .param p2, "messageArgs"    # Ljava/lang/String;
    .param p3, "inviteHandle"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    .prologue
    .line 131
    :try_start_0
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->generateTemplatedNotificationBody(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "messageBody":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v3

    .line 134
    invoke-virtual {p3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;->id()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 132
    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyInvitePendingIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    .line 136
    .local v2, "n":Landroid/app/Notification;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 138
    invoke-virtual {p3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-virtual {p3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;->id()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 137
    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createPartyInviteIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v1, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    .end local v1    # "messageBody":Ljava/lang/String;
    .end local v2    # "n":Landroid/app/Notification;
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v4, "Could not parse message body"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 144
    .local v0, "e":Ljava/util/MissingFormatArgumentException;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not format notification with ID with format: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and arguments: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "messageFormat"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "args"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "organizer"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "tournamentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 270
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 271
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 272
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 273
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 274
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 277
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "argsArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    new-array v1, v6, [Ljava/lang/String;

    .line 280
    .local v1, "argsStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 281
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    .line 280
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 284
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "argsStrings":[Ljava/lang/String;
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 286
    .local v4, "messageBody":Ljava/lang/String;
    :goto_1
    invoke-direct {p0, p3, p4, p5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createTournamentPendingIntent(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-direct {p0, v4, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v5

    .line 287
    .local v5, "n":Landroid/app/Notification;
    invoke-direct {p0, p3, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayDeviceNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/app/Notification;)V

    .line 288
    invoke-direct {p0, p3, p4, p5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createTournamentIntent(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v4, v6, p3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/MissingFormatArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 295
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v4    # "messageBody":Ljava/lang/String;
    .end local v5    # "n":Landroid/app/Notification;
    :goto_2
    return-void

    .restart local v0    # "argsArray":Lorg/json/JSONArray;
    .restart local v1    # "argsStrings":[Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_1
    move-object v4, p1

    .line 284
    goto :goto_1

    .line 290
    .end local v0    # "argsArray":Lorg/json/JSONArray;
    .end local v1    # "argsStrings":[Ljava/lang/String;
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 291
    .local v2, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    const-string v7, "Could not parse message body"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 292
    .end local v2    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v2

    .line 293
    .local v2, "e":Ljava/util/MissingFormatArgumentException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not format notification with ID with format: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and arguments: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public removeAllNotifications()V
    .locals 2

    .prologue
    .line 298
    const/4 v0, 0x0

    .local v0, "id":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_0
    return-void
.end method

.method public removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 3
    .param p1, "nt"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 304
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->getNotificationType()Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->dismiss()V

    .line 306
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->inAppNotification:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;

    .line 308
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne p1, v1, :cond_2

    .line 310
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->messageIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 311
    .local v0, "id":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 313
    .end local v0    # "id":I
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->messageIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 315
    :cond_2
    return-void
.end method

.method public showNotificationWithMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 318
    invoke-direct {p0, p1, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v0

    .line 319
    .local v0, "n":Landroid/app/Notification;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nm:Landroid/app/NotificationManager;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->nextNotificationId(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 320
    invoke-direct {p0, p1, v3, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayOptionalInAppNotification(Ljava/lang/String;Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 321
    return-void
.end method
