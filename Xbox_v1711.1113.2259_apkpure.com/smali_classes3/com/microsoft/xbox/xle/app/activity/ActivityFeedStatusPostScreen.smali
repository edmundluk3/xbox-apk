.class public Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "ActivityFeedStatusPostScreen.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private oldInputMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method

.method private restoreSoftInputAdjustMode()V
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 60
    .local v0, "wnd":Landroid/view/Window;
    iget v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->oldInputMode:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 61
    return-void
.end method

.method private saveAndAdjustSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 53
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 54
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->oldInputMode:I

    .line 55
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 56
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackPage()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 18
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->onCreateContentView()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 20
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->setContentView(I)V

    .line 37
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 24
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onStart()V

    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->saveAndAdjustSoftInputAdjustMode()V

    .line 26
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onStop()V

    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;->restoreSoftInputAdjustMode()V

    .line 32
    return-void
.end method
