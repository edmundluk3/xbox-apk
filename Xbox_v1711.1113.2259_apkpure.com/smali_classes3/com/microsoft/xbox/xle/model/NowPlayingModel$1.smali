.class Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;
.super Ljava/lang/Object;
.source "NowPlayingModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/model/NowPlayingModel;->updateNowPlayingModels()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

.field final synthetic val$cachedMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

.field final synthetic val$cachedTitleId:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;ILcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 701
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    iput p2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->val$cachedTitleId:I

    iput-object p3, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->val$cachedMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 704
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "3 seconds is up!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$100(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    if-eq v1, v2, :cond_1

    .line 708
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "No longer playing music."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$200(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$200(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 710
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Clearing current media state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1, v6}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$202(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Lcom/microsoft/xbox/smartglass/MediaState;)Lcom/microsoft/xbox/smartglass/MediaState;

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->val$cachedTitleId:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$300(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 718
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Title has changed."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$200(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$200(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 720
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Clearing current media state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1, v6}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$202(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Lcom/microsoft/xbox/smartglass/MediaState;)Lcom/microsoft/xbox/smartglass/MediaState;

    goto :goto_0

    .line 728
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->val$cachedTitleId:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    .line 729
    .local v0, "temp":Lcom/microsoft/xbox/smartglass/MediaState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->val$cachedMediaState:Lcom/microsoft/xbox/smartglass/MediaState;

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 730
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$000(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Media state hasn\'t updated to a valid asset id. Updating state to now playing app."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    sget-object v2, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$102(Lcom/microsoft/xbox/xle/model/NowPlayingModel;Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;)Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    .line 733
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$400(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 734
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->access$500(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 735
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$1;->this$0:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v2, v3, p0, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto/16 :goto_0
.end method
