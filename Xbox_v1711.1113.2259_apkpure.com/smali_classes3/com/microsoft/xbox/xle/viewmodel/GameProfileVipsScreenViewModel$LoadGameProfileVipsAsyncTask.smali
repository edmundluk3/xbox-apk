.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileVipsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameProfileVipsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$1;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 77
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->gameTitleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 80
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileVipsModel()Lcom/microsoft/xbox/service/model/GameProfileVipsModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iget-wide v2, v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel;->shouldRefresh(J)Z

    move-result v0

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 110
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 112
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iget-wide v2, v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->gameTitleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileVipsModel()Lcom/microsoft/xbox/service/model/GameProfileVipsModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    iget-wide v4, v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->gameTitleId:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 118
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->onLoadGameProfileVipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 90
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->onLoadGameProfileVipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 101
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->updateAdapter()V

    .line 96
    return-void
.end method
