.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 588
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->val$state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 591
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->val$state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_1

    .line 592
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$900(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestConfig()Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$1000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$900(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestHeadend()Z

    .line 597
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$900(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestLiveTvInfo()Z

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;->val$state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$1000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    .line 601
    return-void
.end method
