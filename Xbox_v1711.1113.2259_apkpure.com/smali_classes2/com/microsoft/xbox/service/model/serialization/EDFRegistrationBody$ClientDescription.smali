.class public Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;
.super Ljava/lang/Object;
.source "EDFRegistrationBody.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClientDescription"
.end annotation


# instance fields
.field public final appId:Ljava/lang/String;

.field public final languageId:Ljava/lang/String;

.field public final templateKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;->this$0:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "com.microsoft.xboxone.smartglass.beta.android"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;->appId:Ljava/lang/String;

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;->languageId:Ljava/lang/String;

    .line 29
    const-string v0, "com.microsoft.xboxone.smartglass.beta.android:1.0"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;->templateKey:Ljava/lang/String;

    return-void
.end method
