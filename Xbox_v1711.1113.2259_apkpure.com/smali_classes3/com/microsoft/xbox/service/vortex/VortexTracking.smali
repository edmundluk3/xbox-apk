.class public Lcom/microsoft/xbox/service/vortex/VortexTracking;
.super Ljava/lang/Object;
.source "VortexTracking.java"


# static fields
.field private static final DateFormatString:Ljava/lang/String; = "yyyy/MM/dd HH:mm:ss.SSS"

.field private static instance:Lcom/microsoft/xbox/service/vortex/VortexTracking;


# instance fields
.field private appId:Ljava/lang/String;

.field private appName:Ljava/lang/String;

.field private clientAppId:Ljava/lang/String;

.field private clientAppVersion:Ljava/lang/String;

.field private clientDeviceId:Ljava/lang/String;

.field private clientDeviceType:Ljava/lang/String;

.field private dateFormat:Ljava/text/SimpleDateFormat;

.field private experimentName:Ljava/lang/String;

.field private isEnabled:Z

.field private pid:I

.field private resolution:Ljava/lang/String;

.field private sequenceNumber:J

.field private serviceEndpoint:Ljava/lang/String;

.field private sessionGuid:Ljava/lang/String;

.field private syncObj:Ljava/lang/Object;

.field private timeZoneOffset:Ljava/lang/String;

.field private userSubscriptionTier:Ljava/lang/String;

.field private userSubscriptionType:Ljava/lang/String;

.field private userXuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/vortex/VortexTracking;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->instance:Lcom/microsoft/xbox/service/vortex/VortexTracking;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->syncObj:Ljava/lang/Object;

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sequenceNumber:J

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/vortex/VortexTracking;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/vortex/VortexTracking;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->trackAppLaunchInternal(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/vortex/VortexTracking;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/vortex/VortexTracking;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->trackMediaPlaybackInternal(Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V

    return-void
.end method

.method private getCommonSchema(Ljava/lang/StringBuilder;J)Ljava/lang/StringBuilder;
    .locals 4
    .param p1, "builder"    # Ljava/lang/StringBuilder;
    .param p2, "sequenceNumber"    # J

    .prologue
    const/16 v3, 0x7c

    .line 199
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->experimentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    const-string v0, "|0|"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    return-object p1
.end method

.method private getExperiment()Ljava/lang/String;
    .locals 2

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".Release"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/vortex/VortexTracking;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->instance:Lcom/microsoft/xbox/service/vortex/VortexTracking;

    return-object v0
.end method

.method private getPartA_c(Ljava/lang/StringBuilder;Ljava/lang/String;J)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "builder"    # Ljava/lang/StringBuilder;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p3, "sequenceNumber"    # J

    .prologue
    const/16 v2, 0x7c

    .line 208
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string v0, "||"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientAppId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientAppVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v0, "|||100"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->pid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 216
    return-object p1
.end method

.method private getPartA_s(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v2, 0x7c

    .line 221
    const-string v0, "|1.0"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string v0, "||"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientDeviceType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 225
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    return-object p1
.end method

.method private getPartB_trackEvent_deviceAttributes(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 2
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v0, 0x7c

    .line 251
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->resolution:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    const-string v0, "|TRUE"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string v0, "|FALSE"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    return-object p1
.end method

.method private getPartB_trackEvent_location(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 5
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v4, 0x7c

    .line 240
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "locale":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x5

    if-lt v2, v3, :cond_0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "region":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->timeZoneOffset:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    return-object p1

    .line 241
    .end local v1    # "region":Ljava/lang/String;
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method private getPartB_trackEvent_mediaContent(Ljava/lang/StringBuilder;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "builder"    # Ljava/lang/StringBuilder;
    .param p2, "mediaTracking"    # Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;

    .prologue
    const/16 v2, 0x7c

    .line 259
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->mediaType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->providerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->providerMediaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->providerMediaInstanceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->mediaLength:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    return-object p1
.end method

.method private getPartB_trackEvent_mediaPlayback(Ljava/lang/StringBuilder;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "builder"    # Ljava/lang/StringBuilder;
    .param p2, "mediaTracking"    # Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;

    .prologue
    const/16 v2, 0x7c

    .line 270
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->action:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 271
    const-string v0, "|1.0"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->positionInMS:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->playbackDurationInSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 274
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->acquisitionType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string v0, "|||"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->isStreaming:Z

    if-eqz v0, :cond_0

    const-string v0, "TRUE"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p2, Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;->isThethered:Z

    if-eqz v0, :cond_1

    const-string v0, "TRUE"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    return-object p1

    .line 276
    :cond_0
    const-string v0, "FALSE"

    goto :goto_0

    .line 277
    :cond_1
    const-string v0, "FALSE"

    goto :goto_1
.end method

.method private getPartB_trackEvent_user(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v2, 0x7c

    .line 231
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sessionGuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 233
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userSubscriptionType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userSubscriptionTier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    return-object p1
.end method

.method private getSequenceNumber()J
    .locals 8

    .prologue
    .line 187
    iget-object v3, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->syncObj:Ljava/lang/Object;

    monitor-enter v3

    .line 188
    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sequenceNumber:J

    .line 189
    .local v0, "seq":J
    iget-wide v4, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sequenceNumber:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sequenceNumber:J

    .line 190
    monitor-exit v3

    return-wide v0

    .line 191
    .end local v0    # "seq":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getVortexEndpoint()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 325
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->PROD:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    if-eq v0, v1, :cond_0

    .line 326
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://data.vint.xboxlive.com/event/auth0/%s/0/data"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->appId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 328
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://data.xboxlive.com/event/auth1/%s/0/data"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->appId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private reportToService(Ljava/lang/StringBuilder;)V
    .locals 10
    .param p1, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    .line 282
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 284
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "postBody":Ljava/lang/String;
    const-string v5, "VortexTracking"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tracing body "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v4, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->serviceEndpoint:Ljava/lang/String;

    .line 288
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "text/psv"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept"

    const-string v7, "Application/Json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    const/4 v3, 0x0

    .line 294
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 295
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 296
    const-string v5, "VortexTracking"

    const-string v6, "Successful"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :cond_0
    if-eqz v3, :cond_1

    .line 317
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 320
    :cond_1
    :goto_0
    return-void

    .line 298
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v6

    const-wide/16 v8, 0x12

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 302
    new-instance v5, Lcom/microsoft/xbox/service/vortex/VortexTracking$5;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/service/vortex/VortexTracking$5;-><init>(Lcom/microsoft/xbox/service/vortex/VortexTracking;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :goto_1
    if-eqz v3, :cond_1

    .line 317
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    goto :goto_0

    .line 310
    :cond_2
    :try_start_2
    const-string v5, "VortexTracking"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "report to service get exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 315
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_3

    .line 317
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v5

    .line 312
    :catch_1
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "VortexTracking"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "report to service get exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 315
    if-eqz v3, :cond_1

    .line 317
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    goto :goto_0
.end method

.method private trackAppLaunchInternal(Z)V
    .locals 7
    .param p1, "isActivation"    # Z

    .prologue
    .line 141
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 143
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getSequenceNumber()J

    move-result-wide v2

    .line 145
    .local v2, "sequenceNumber":J
    const-string v4, "0|Track"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-direct {p0, v0, v2, v3}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getCommonSchema(Ljava/lang/StringBuilder;J)Ljava/lang/StringBuilder;

    .line 147
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartA_s(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 148
    const-string v4, "AppLaunch"

    invoke-direct {p0, v0, v4, v2, v3}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartA_c(Ljava/lang/StringBuilder;Ljava/lang/String;J)Ljava/lang/StringBuilder;

    .line 151
    const/16 v4, 0x7c

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p1, :cond_0

    const-string v4, "1"

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->reportToService(Ljava/lang/StringBuilder;)V

    .line 157
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "sequenceNumber":J
    :goto_1
    return-void

    .line 151
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "sequenceNumber":J
    :cond_0
    const-string v4, "0"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "sequenceNumber":J
    :catch_0
    move-exception v1

    .line 154
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "VortexTracking"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed tracking with exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private trackMediaPlaybackInternal(Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V
    .locals 7
    .param p1, "mediaTracking"    # Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 164
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getSequenceNumber()J

    move-result-wide v2

    .line 167
    .local v2, "sequenceNumber":J
    const-string v4, "0|Track"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    invoke-direct {p0, v0, v2, v3}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getCommonSchema(Ljava/lang/StringBuilder;J)Ljava/lang/StringBuilder;

    .line 169
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartA_s(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 170
    const-string v4, "MediaUsageEvent"

    invoke-direct {p0, v0, v4, v2, v3}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartA_c(Ljava/lang/StringBuilder;Ljava/lang/String;J)Ljava/lang/StringBuilder;

    .line 173
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartB_trackEvent_user(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 174
    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartB_trackEvent_mediaContent(Ljava/lang/StringBuilder;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)Ljava/lang/StringBuilder;

    .line 175
    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartB_trackEvent_mediaPlayback(Ljava/lang/StringBuilder;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)Ljava/lang/StringBuilder;

    .line 176
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartB_trackEvent_location(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 177
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getPartB_trackEvent_deviceAttributes(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 179
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->reportToService(Ljava/lang/StringBuilder;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "sequenceNumber":J
    :goto_0
    return-void

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "VortexTracking"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed tracking with exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public initialize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "clientDeviceType"    # Ljava/lang/String;
    .param p4, "clientDeviceId"    # Ljava/lang/String;
    .param p5, "clientAppId"    # Ljava/lang/String;
    .param p6, "clientAppVersion"    # Ljava/lang/String;
    .param p7, "xuid"    # Ljava/lang/String;
    .param p8, "subscriptionType"    # Ljava/lang/String;
    .param p9, "subscriptionTier"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 68
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy/MM/dd HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->dateFormat:Ljava/text/SimpleDateFormat;

    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 70
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->pid:I

    .line 71
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->sessionGuid:Ljava/lang/String;

    .line 72
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v6

    int-to-double v6, v6

    const-wide v8, 0x414b774000000000L    # 3600000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->timeZoneOffset:Ljava/lang/String;

    .line 73
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%dx%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenHeight()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->resolution:Ljava/lang/String;

    .line 74
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->isEnabled:Z

    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->appName:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->appId:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientDeviceType:Ljava/lang/String;

    .line 79
    iput-object p4, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientDeviceId:Ljava/lang/String;

    .line 80
    iput-object p5, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientAppId:Ljava/lang/String;

    .line 81
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->clientAppVersion:Ljava/lang/String;

    .line 82
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userXuid:Ljava/lang/String;

    .line 83
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userSubscriptionType:Ljava/lang/String;

    .line 84
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->userSubscriptionTier:Ljava/lang/String;

    .line 86
    invoke-direct {p0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getExperiment()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->experimentName:Ljava/lang/String;

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->getVortexEndpoint()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->serviceEndpoint:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setEnableTracking(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->isEnabled:Z

    .line 138
    return-void
.end method

.method public trackAppLaunch(Z)V
    .locals 3
    .param p1, "isActivation"    # Z

    .prologue
    .line 92
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->isEnabled:Z

    if-nez v1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v1, v2, :cond_1

    .line 97
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->trackAppLaunchInternal(Z)V

    goto :goto_0

    .line 99
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/vortex/VortexTracking$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/vortex/VortexTracking$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking$1;-><init>(Lcom/microsoft/xbox/service/vortex/VortexTracking;Z)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/vortex/VortexTracking$2;-><init>(Lcom/microsoft/xbox/service/vortex/VortexTracking;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 108
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method public trackMediaPlayback(Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V
    .locals 3
    .param p1, "mediaTracking"    # Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;

    .prologue
    .line 115
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/vortex/VortexTracking;->isEnabled:Z

    if-nez v1, :cond_0

    .line 134
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v1, v2, :cond_1

    .line 120
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking;->trackMediaPlaybackInternal(Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V

    goto :goto_0

    .line 122
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/vortex/VortexTracking$4;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/vortex/VortexTracking$3;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/vortex/VortexTracking$3;-><init>(Lcom/microsoft/xbox/service/vortex/VortexTracking;Lcom/microsoft/xbox/service/vortex/VortexMediaTracking;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/vortex/VortexTracking$4;-><init>(Lcom/microsoft/xbox/service/vortex/VortexTracking;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 131
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method
