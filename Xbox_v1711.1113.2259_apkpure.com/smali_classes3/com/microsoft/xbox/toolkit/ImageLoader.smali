.class public final Lcom/microsoft/xbox/toolkit/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;,
        Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;
    }
.end annotation


# static fields
.field private static final LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

.field public static NO_RES:I

.field private static final STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const/4 v0, -0x1

    sput v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    .line 37
    const-class v0, Lcom/microsoft/xbox/toolkit/ImageLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->TAG:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;-><init>(Lcom/microsoft/xbox/toolkit/ImageLoader$1;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;-><init>(Lcom/microsoft/xbox/toolkit/ImageLoader$1;)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Utility class - no instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static load(Landroid/widget/ImageView;I)V
    .locals 2
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "resourceId"    # I

    .prologue
    .line 133
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-static {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;III)V

    .line 134
    return-void
.end method

.method public static load(Landroid/widget/ImageView;III)V
    .locals 2
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "resourceId"    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p2, "placeholderImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3, "errorImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 141
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 143
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 146
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v0

    .line 147
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/Integer;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v0

    .line 148
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/DrawableTypeRequest;->placeholder(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    .line 149
    invoke-virtual {v0, p3}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    .line 150
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 151
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    .line 152
    invoke-virtual {v0, p0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 153
    return-void
.end method

.method public static load(Landroid/widget/ImageView;Ljava/lang/String;II)V
    .locals 3
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "placeholderImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3, "errorImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 53
    if-eqz p1, :cond_0

    const-string v1, "file"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, "model":Ljava/lang/Object;
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v1

    .line 57
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/Object;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v1

    .line 58
    invoke-virtual {v1, p2}, Lcom/bumptech/glide/DrawableTypeRequest;->placeholder(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 59
    invoke-virtual {v1, p3}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    .line 60
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 61
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 62
    invoke-virtual {v1, p0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 63
    return-void

    .end local v0    # "model":Ljava/lang/Object;
    :cond_0
    move-object v0, p1

    .line 53
    goto :goto_0
.end method

.method public static load1080p(Landroid/widget/ImageView;Ljava/lang/String;II)V
    .locals 4
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "placeholderImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3, "errorImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 70
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 72
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 76
    if-eqz p1, :cond_0

    const-string v1, "file"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 79
    .local v0, "model":Ljava/lang/Object;
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/Object;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v1

    const/16 v2, 0x780

    const/16 v3, 0x438

    .line 81
    invoke-virtual {v1, v2, v3}, Lcom/bumptech/glide/DrawableTypeRequest;->override(II)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 82
    invoke-virtual {v1, p2}, Lcom/bumptech/glide/DrawableRequestBuilder;->placeholder(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 83
    invoke-virtual {v1, p3}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    .line 84
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 85
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 86
    invoke-virtual {v1, p0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 87
    return-void

    .end local v0    # "model":Ljava/lang/Object;
    :cond_0
    move-object v0, p1

    .line 76
    goto :goto_0
.end method

.method public static loadCircular(Landroid/widget/ImageView;Ljava/lang/String;II)V
    .locals 6
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "placeholderImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3, "errorImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 113
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 115
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 119
    if-eqz p1, :cond_0

    const-string v1, "file"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "model":Ljava/lang/Object;
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/Object;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/bumptech/glide/load/Transformation;

    const/4 v3, 0x0

    new-instance v4, Ljp/wasabeef/glide/transformations/CropCircleTransformation;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 124
    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Ljp/wasabeef/glide/transformations/CropCircleTransformation;-><init>(Landroid/content/Context;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableTypeRequest;->bitmapTransform([Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 125
    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 126
    invoke-virtual {v2, p3}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    .line 127
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 128
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 129
    invoke-virtual {v1, p0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 130
    return-void

    .end local v0    # "model":Ljava/lang/Object;
    :cond_0
    move-object v0, p1

    .line 119
    goto :goto_0
.end method

.method public static loadRaw(Landroid/widget/ImageView;Landroid/net/Uri;II)V
    .locals 2
    .param p0, "target"    # Landroid/widget/ImageView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "placeholderImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3, "errorImageId"    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 94
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 96
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 99
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/RequestManager;->load(Landroid/net/Uri;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v0

    .line 101
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/DrawableTypeRequest;->placeholder(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    .line 102
    invoke-virtual {v0, p3}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->STOP_NETWORK_SAMPLING:Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;

    .line 103
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->LOGGER:Lcom/microsoft/xbox/toolkit/ImageLoader$Logger;

    .line 104
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 106
    return-void
.end method
