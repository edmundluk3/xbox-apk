.class public Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SkypeConversationDetailsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;,
        Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final MY_MESSAGE_VIEW_TYPE:I = 0x0

.field private static final OTHER_MESSAGE_VIEW_TYPE:I = 0x1

.field private static final OTHER_USER_BACKGROUND_COLOR:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private static final STATUS_MESSAGE_VIEW_TYPE:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static final UNSUPPORTED_MESSAGE:Ljava/lang/String;

.field private static final VIEW_TYPE_COUNT:I = 0x3


# instance fields
.field private final allowClicks:Z

.field private final myBackgroundColor:I

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->TAG:Ljava/lang/String;

    .line 56
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07075d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->UNSUPPORTED_MESSAGE:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->OTHER_USER_BACKGROUND_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "rowViewResourceId"    # I
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "allowClicks"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 69
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 72
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    .line 73
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->allowClicks:Z

    .line 75
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 76
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->myBackgroundColor:I

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->notifyDataSetChanged()V

    .line 79
    return-void

    .line 76
    :cond_0
    sget v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->OTHER_USER_BACKGROUND_COLOR:I

    goto :goto_0
.end method

.method private convertSkypeToConversationMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
    .locals 4
    .param p1, "skypeMessage"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    const/4 v2, 0x0

    .line 405
    if-nez p1, :cond_0

    move-object v0, v2

    .line 419
    :goto_0
    return-object v0

    .line 409
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;-><init>()V

    .line 410
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getActivityFeedItemLocatorFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;

    move-result-object v1

    .line 412
    .local v1, "skypeAttachmentContent":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    if-eqz v1, :cond_1

    .line 413
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->message:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    .line 414
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;->locator:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    .line 415
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->setAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;)V

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 419
    goto :goto_0
.end method

.method private convertViewTypeToLayoutResourceId(I)I
    .locals 2
    .param p1, "viewType"    # I

    .prologue
    .line 137
    packed-switch p1, :pswitch_data_0

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid view type found: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 146
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 139
    :pswitch_0
    const v0, 0x7f0300bf

    goto :goto_0

    .line 141
    :pswitch_1
    const v0, 0x7f0300be

    goto :goto_0

    .line 143
    :pswitch_2
    const v0, 0x7f0300c0

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private createAttachmentHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 429
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;-><init>(Landroid/view/View;)V

    .line 431
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    const v2, 0x7f0e048b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 432
    const v2, 0x7f0e048d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    .line 433
    const v2, 0x7f0e0485

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    .line 434
    const v2, 0x7f0e048e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 435
    const v2, 0x7f0e048f

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 436
    const v2, 0x7f0e0490

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    .line 437
    const v2, 0x7f0e0491

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    .line 438
    const v2, 0x7f0e0486

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 439
    const v2, 0x7f0e0488

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    .line 440
    const v2, 0x7f0e0487

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    .line 441
    const v2, 0x7f0e0492

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    .line 442
    const v2, 0x7f0e048c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    .line 443
    const v2, 0x7f0e048a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    .line 446
    const v2, 0x7f0e0872

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    .line 447
    const v2, 0x7f0e0874

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    .line 448
    const v2, 0x7f0e0875

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 449
    const v2, 0x7f0e0876

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 450
    const v2, 0x7f0e0877

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 451
    const v2, 0x7f0e0878

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    .line 452
    const v2, 0x7f0e0879

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 453
    const v2, 0x7f0e087a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 454
    const v2, 0x7f0e087b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 455
    const v2, 0x7f0e087c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 456
    const v2, 0x7f0e087d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 459
    const v2, 0x7f0e087e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    .line 460
    const v2, 0x7f0e0880

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 461
    const v2, 0x7f0e0881

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 462
    const v2, 0x7f0e0882

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 463
    const v2, 0x7f0e0884

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 465
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 466
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 467
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->tagBackgroundColor:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 469
    const v2, 0x7f0e0883

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 471
    .local v1, "tagsList":Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    if-eqz v1, :cond_0

    .line 472
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 473
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 478
    :goto_0
    return-object v0

    .line 475
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->TAG:Ljava/lang/String;

    const-string v3, "Could not find tag list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createAttachmentView(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 423
    const v1, 0x7f0300bc

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 424
    .local v0, "v":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->createAttachmentHolder(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 425
    return-object v0
.end method

.method private displaySkypeAttachment(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;I)V
    .locals 11
    .param p1, "attachmentContainer"    # Landroid/view/ViewGroup;
    .param p2, "skypeMessage"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .param p3, "color"    # I

    .prologue
    const v10, 0x7f070752

    const/4 v9, 0x1

    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 337
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 338
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->convertSkypeToConversationMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    move-result-object v8

    .line 340
    .local v8, "message":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 342
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v4, v0, v4, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->hasAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 345
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 346
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 347
    invoke-virtual {p1, v5, v5, v5, v5}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030040

    invoke-virtual {v0, v3, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 402
    .end local v8    # "message":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
    :cond_0
    :goto_0
    return-void

    .line 352
    .restart local v8    # "message":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->hasAttachmentLoadingError(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getAttachmentErrorResId(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)I

    move-result v0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setAttachmentError(Landroid/view/ViewGroup;I)V

    goto :goto_0

    .line 356
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v6

    .line 358
    .local v6, "attachment":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 359
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 360
    .local v7, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 362
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v2, :cond_4

    .line 378
    invoke-direct {p0, p1, v7}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->createAttachmentView(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    .line 380
    .local v1, "view":Landroid/view/View;
    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 381
    const v0, 0x7f0e0899

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 382
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->allowClicks:Z

    if-eqz v0, :cond_3

    .line 383
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;

    invoke-direct {v3, v6, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCMessagingRunnable;-><init>(Lcom/microsoft/xbox/service/model/entity/Entity;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->CONVERSATION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_OLD:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    .line 387
    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 389
    .end local v1    # "view":Landroid/view/View;
    :cond_4
    invoke-static {p1, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setAttachmentError(Landroid/view/ViewGroup;I)V

    goto :goto_0

    .line 392
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v7    # "inflater":Landroid/view/LayoutInflater;
    :cond_5
    invoke-static {p1, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setAttachmentError(Landroid/view/ViewGroup;I)V

    goto :goto_0

    .line 395
    .end local v6    # "attachment":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->hasXbox360Attachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 396
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0300c1

    invoke-virtual {v0, v3, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 397
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 399
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method static synthetic lambda$populateMessageFromOther$0(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;Ljava/lang/String;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;
    .param p1, "senderXuid"    # Ljava/lang/String;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->navigateToSpecificFriendProfile(Ljava/lang/String;)V

    return-void
.end method

.method private populateMessageFromOther(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;I)V
    .locals 17
    .param p1, "v"    # Landroid/view/View;
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .param p3, "position"    # I

    .prologue
    .line 183
    if-eqz p2, :cond_4

    if-eqz p1, :cond_4

    .line 185
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    instance-of v14, v14, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    if-nez v14, :cond_0

    .line 186
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$000(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 189
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;

    .line 190
    .local v13, "views":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    move-object/from16 v0, p2

    iput-object v0, v13, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->conversationMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 192
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isSkypeServiceConversation()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 193
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->UNSUPPORTED_MESSAGE:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 197
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getSkypeMessageSentTimeString(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Ljava/lang/String;

    move-result-object v11

    .line 198
    .local v11, "sentTimeString":Ljava/lang/String;
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v14, v11, v15}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 199
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;

    move-result-object v14

    const/4 v15, 0x4

    invoke-static {v14, v11, v15}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 201
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$400(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/view/View;

    move-result-object v14

    sget v15, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->OTHER_USER_BACKGROUND_COLOR:I

    invoke-virtual {v14, v15}, Landroid/view/View;->setBackgroundColor(I)V

    .line 203
    const/4 v14, 0x1

    move/from16 v0, p3

    if-lt v0, v14, :cond_6

    const/4 v6, 0x1

    .line 205
    .local v6, "positionGreaterThanOrEqualTo1":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isSkypeServiceConversation()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderSkypeId()Ljava/lang/String;

    move-result-object v10

    .line 206
    .local v10, "senderXuid":Ljava/lang/String;
    :goto_2
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_8

    const/4 v5, 0x1

    .line 207
    .local v5, "messageFolderIsNotEmpty":Z
    :goto_3
    const/4 v2, 0x1

    .line 208
    .local v2, "currentMessageFolderIsSameAsPreviousMessageFolder":Z
    if-lez p3, :cond_1

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 209
    add-int/lit8 v14, p3, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 210
    .local v7, "previousMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    if-eqz v7, :cond_9

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    const/4 v2, 0x1

    .line 213
    .end local v7    # "previousMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_1
    :goto_4
    if-eqz v6, :cond_a

    if-eqz v5, :cond_a

    if-eqz v2, :cond_a

    const/4 v3, 0x1

    .line 214
    .local v3, "isNotFirstConsecutiveMessage":Z
    :goto_5
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderSkypeId()Ljava/lang/String;

    move-result-object v12

    .line 215
    .local v12, "skypeId":Ljava/lang/String;
    const-string v8, ""

    .line 216
    .local v8, "senderGamerTag":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 217
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    .line 218
    .local v4, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v4, :cond_2

    .line 219
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v8

    .line 225
    .end local v4    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    :goto_6
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 226
    if-eqz v3, :cond_d

    .line 227
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/LinearLayout;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/TextView;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    :cond_3
    :goto_7
    if-eqz v3, :cond_f

    const/4 v9, 0x4

    .line 243
    .local v9, "senderVisibility":I
    :goto_8
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v14

    invoke-static {v14, v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 244
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$700(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v14

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 246
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$800(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/view/ViewGroup;

    move-result-object v14

    sget v15, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->OTHER_USER_BACKGROUND_COLOR:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v14, v1, v15}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->displaySkypeAttachment(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;I)V

    .line 248
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$900(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/ImageView;

    move-result-object v14

    if-eqz v14, :cond_4

    .line 249
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$900(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/ImageView;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 250
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$900(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/ImageView;

    move-result-object v14

    sget v15, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->OTHER_USER_BACKGROUND_COLOR:I

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 251
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$900(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/ImageView;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/ImageView;->invalidate()V

    .line 254
    .end local v2    # "currentMessageFolderIsSameAsPreviousMessageFolder":Z
    .end local v3    # "isNotFirstConsecutiveMessage":Z
    .end local v5    # "messageFolderIsNotEmpty":Z
    .end local v6    # "positionGreaterThanOrEqualTo1":Z
    .end local v8    # "senderGamerTag":Ljava/lang/String;
    .end local v9    # "senderVisibility":I
    .end local v10    # "senderXuid":Ljava/lang/String;
    .end local v11    # "sentTimeString":Ljava/lang/String;
    .end local v12    # "skypeId":Ljava/lang/String;
    .end local v13    # "views":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    :cond_4
    return-void

    .line 195
    .restart local v13    # "views":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;
    :cond_5
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_0

    .line 203
    .restart local v11    # "sentTimeString":Ljava/lang/String;
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 205
    .restart local v6    # "positionGreaterThanOrEqualTo1":Z
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    .line 206
    .restart local v10    # "senderXuid":Ljava/lang/String;
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 210
    .restart local v2    # "currentMessageFolderIsSameAsPreviousMessageFolder":Z
    .restart local v5    # "messageFolderIsNotEmpty":Z
    .restart local v7    # "previousMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 213
    .end local v7    # "previousMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 222
    .restart local v3    # "isNotFirstConsecutiveMessage":Z
    .restart local v8    # "senderGamerTag":Ljava/lang/String;
    .restart local v12    # "skypeId":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isSkypeGroupConversation()Z

    move-result v14

    if-eqz v14, :cond_c

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getConversationSkypeId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15, v12}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_9
    goto/16 :goto_6

    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getSenderGamertag()Ljava/lang/String;

    move-result-object v8

    goto :goto_9

    .line 231
    :cond_d
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/LinearLayout;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 232
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isSkypeServiceConversation()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 233
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v14

    invoke-static {v14, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 236
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->allowClicks:Z

    if-eqz v14, :cond_3

    .line 237
    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$OtherMessageViews;)Landroid/widget/LinearLayout;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_7

    .line 242
    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_8
.end method

.method private populateMessageFromSelf(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    const/4 v3, 0x0

    .line 257
    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    .line 260
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    if-nez v1, :cond_0

    .line 261
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1000(Landroid/view/View;)Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 263
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;

    .line 264
    .local v0, "views":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
    iput-object p2, v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->conversationMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 266
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 268
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 269
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1200(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->myBackgroundColor:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 272
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 274
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 275
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/widget/ImageView;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->myBackgroundColor:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 276
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1300(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 279
    :cond_2
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;->access$1400(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;)Landroid/view/ViewGroup;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->myBackgroundColor:I

    invoke-direct {p0, v1, p2, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->displaySkypeAttachment(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;I)V

    .line 281
    .end local v0    # "views":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$MyMessageViews;
    :cond_3
    return-void
.end method

.method private populateStatusInfo(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    .line 151
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 152
    const v6, 0x7f0e04a6

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 154
    .local v2, "statusLine":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v6, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v5

    .line 155
    .local v5, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :goto_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getConversationSkypeId()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "conversationId":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 177
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 180
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v2    # "statusLine":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v5    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_0
    :goto_1
    return-void

    .line 154
    .restart local v2    # "statusLine":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    goto :goto_0

    .line 159
    .restart local v0    # "conversationId":Ljava/lang/String;
    .restart local v5    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v7, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v8, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "initiator":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v7, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v8, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v0, v7, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 161
    .local v4, "target":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070639

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 165
    .end local v1    # "initiator":Ljava/lang/String;
    .end local v4    # "target":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07065a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v11, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v12, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v0, v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 169
    :pswitch_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07063d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    iget-object v11, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v12, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v0, v11}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v11, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;

    invoke-direct {v3}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;-><init>()V

    .line 172
    .local v3, "tagData":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;
    iput-object p2, v3, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;->conversationMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 173
    invoke-virtual {p1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 88
    const/4 v1, -0x1

    .line 90
    .local v1, "result":I
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 91
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    if-eqz v0, :cond_2

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "senderXuid":Ljava/lang/String;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 95
    const-string v3, "Unexpected message type"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 96
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Message type was null: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 100
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 101
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 102
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    :cond_1
    const/4 v1, 0x2

    .line 109
    .end local v2    # "senderXuid":Ljava/lang/String;
    :cond_2
    :goto_0
    return v1

    .line 105
    .restart local v2    # "senderXuid":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x0

    :goto_1
    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 115
    move-object v3, p2

    .line 116
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 118
    .local v2, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getItemViewType(I)I

    move-result v0

    .line 119
    .local v0, "itemViewType":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->convertViewTypeToLayoutResourceId(I)I

    move-result v1

    .line 121
    .local v1, "layoutId":I
    if-nez v3, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 123
    .local v4, "vi":Landroid/view/LayoutInflater;
    const/4 v5, 0x0

    invoke-virtual {v4, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 126
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    if-nez v0, :cond_1

    .line 127
    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->populateMessageFromSelf(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    .line 133
    :goto_0
    return-object v3

    .line 128
    :cond_1
    const/4 v5, 0x1

    if-ne v0, v5, :cond_2

    .line 129
    invoke-direct {p0, v3, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->populateMessageFromOther(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;I)V

    goto :goto_0

    .line 131
    :cond_2
    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsListAdapter;->populateStatusInfo(Landroid/view/View;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x3

    return v0
.end method
