.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;
.super Ljava/lang/Object;
.source "SkypeMessageDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SkypeEventMessage"
.end annotation


# instance fields
.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final resourceLink:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final resourceType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final time:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resourceType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "time"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "resourceLink"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p6, "resource"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 141
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 142
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 143
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 144
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 145
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->id:Ljava/lang/String;

    .line 148
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->type:Ljava/lang/String;

    .line 149
    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceType:Ljava/lang/String;

    .line 150
    iput-object p4, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->time:Ljava/lang/String;

    .line 151
    iput-object p5, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceLink:Ljava/lang/String;

    .line 152
    iput-object p6, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 153
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    if-ne p0, p1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 159
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;

    if-nez v3, :cond_2

    move v1, v2

    .line 160
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 162
    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;

    .line 163
    .local v0, "other":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceType:Ljava/lang/String;

    .line 164
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v4, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 165
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 171
    const/16 v0, 0x11

    .line 172
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 173
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 174
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 176
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
