.class public final Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "ClubModeratorPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ModeratorViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
        ">;"
    }
.end annotation


# instance fields
.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final presence:Landroid/widget/TextView;

.field private final realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    .line 106
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 108
    const v0, 0x7f0e03a9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 109
    const v0, 0x7f0e03aa

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 110
    const v0, 0x7f0e03ab

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 111
    const v0, 0x7f0e03ac

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->presence:Landroid/widget/TextView;

    .line 112
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "dataObject"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsTransferOwnershipSelectOwner()V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 122
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 3
    .param p1, "dataObject"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fa

    .line 116
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->itemView:Landroid/view/View;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->presence:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->presenceText:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 129
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 99
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->onBind(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    return-void
.end method
