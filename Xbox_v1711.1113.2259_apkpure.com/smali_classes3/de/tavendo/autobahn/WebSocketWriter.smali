.class public Lde/tavendo/autobahn/WebSocketWriter;
.super Ljava/lang/Thread;
.source "WebSocketWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;
    }
.end annotation


# static fields
.field private static final CRLF:Ljava/lang/String; = "\r\n"

.field private static final TAG:Ljava/lang/String;

.field private static final WEB_SOCKETS_VERSION:I = 0xd


# instance fields
.field private final mApplicationBuffer:Ljava/nio/ByteBuffer;

.field private mHandler:Landroid/os/Handler;

.field private mOutputStream:Ljava/io/OutputStream;

.field private final mRandom:Ljava/util/Random;

.field private final mSocket:Ljava/net/Socket;

.field private final mWebSocketConnectionHandler:Landroid/os/Handler;

.field private final mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/net/Socket;Lde/tavendo/autobahn/WebSocketOptions;Ljava/lang/String;)V
    .locals 2
    .param p1, "master"    # Landroid/os/Handler;
    .param p2, "socket"    # Ljava/net/Socket;
    .param p3, "options"    # Lde/tavendo/autobahn/WebSocketOptions;
    .param p4, "threadName"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0, p4}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 49
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketWriter;->mRandom:Ljava/util/Random;

    .line 72
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketConnectionHandler:Landroid/os/Handler;

    .line 73
    iput-object p3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    .line 74
    iput-object p2, p0, Lde/tavendo/autobahn/WebSocketWriter;->mSocket:Ljava/net/Socket;

    .line 76
    invoke-virtual {p3}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxFramePayloadSize()I

    move-result v0

    add-int/lit8 v0, v0, 0xe

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    .line 78
    sget-object v0, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket writer created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method private newFrameMask()[B
    .locals 2

    .prologue
    .line 129
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 130
    .local v0, "ba":[B
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mRandom:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 131
    return-object v0
.end method

.method private newHandshakeKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 118
    .local v0, "ba":[B
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mRandom:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 119
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private notify(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 105
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketConnectionHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 106
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 107
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketConnectionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 108
    return-void
.end method

.method private sendBinaryMessage(Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;)V
    .locals 3
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;->mPayload:[B

    array-length v0, v0

    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxMessagePayloadSize()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 233
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "message payload exceeds payload limit"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x1

    iget-object v2, p1, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;->mPayload:[B

    invoke-virtual {p0, v0, v1, v2}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 236
    return-void
.end method

.method private sendClientHandshake(Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;)V
    .locals 6
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 141
    :cond_0
    const-string v1, "/"

    .line 144
    :cond_1
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "query":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 146
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    :cond_2
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GET "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " HTTP/1.1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 150
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Host: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 151
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "Upgrade: WebSocket\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 152
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "Connection: Upgrade\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 153
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sec-WebSocket-Key: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketWriter;->newHandshakeKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 158
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "Origin: https://www.google.com\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 160
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getSubprotocols()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getSubprotocols()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_3

    .line 161
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "Sec-WebSocket-Protocol: "

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 162
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getSubprotocols()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lt v0, v3, :cond_4

    .line 166
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 169
    .end local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "Sec-WebSocket-Version: 13\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 170
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, "\r\n"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 171
    return-void

    .line 163
    .restart local v0    # "i":I
    :cond_4
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->getSubprotocols()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 164
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const-string v4, ", "

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private sendClose(Lde/tavendo/autobahn/WebSocketMessage$Close;)V
    .locals 7
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$Close;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    .line 178
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getCode()I

    move-result v3

    if-lez v3, :cond_3

    .line 179
    const/4 v2, 0x0

    .line 181
    .local v2, "payload":[B
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getReason()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getReason()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 182
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getReason()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 183
    .local v1, "pReason":[B
    array-length v3, v1

    add-int/lit8 v3, v3, 0x2

    new-array v2, v3, [B

    .line 184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_0

    .line 191
    .end local v0    # "i":I
    .end local v1    # "pReason":[B
    :goto_1
    if-eqz v2, :cond_2

    array-length v3, v2

    const/16 v4, 0x7d

    if-le v3, v4, :cond_2

    .line 192
    new-instance v3, Lde/tavendo/autobahn/WebSocketException;

    const-string v4, "close payload exceeds 125 octets"

    invoke-direct {v3, v4}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 185
    .restart local v0    # "i":I
    .restart local v1    # "pReason":[B
    :cond_0
    add-int/lit8 v3, v0, 0x2

    aget-byte v4, v1, v0

    aput-byte v4, v2, v3

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "i":I
    .end local v1    # "pReason":[B
    :cond_1
    const/4 v3, 0x2

    new-array v2, v3, [B

    goto :goto_1

    .line 195
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getCode()I

    move-result v4

    shr-int/lit8 v4, v4, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 196
    invoke-virtual {p1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getCode()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v2, v5

    .line 198
    invoke-virtual {p0, v6, v5, v2}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 202
    .end local v2    # "payload":[B
    :goto_2
    return-void

    .line 200
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p0, v6, v5, v3}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    goto :goto_2
.end method

.method private sendPing(Lde/tavendo/autobahn/WebSocketMessage$Ping;)V
    .locals 3
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$Ping;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Ping;->mPayload:[B

    if-eqz v0, :cond_0

    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Ping;->mPayload:[B

    array-length v0, v0

    const/16 v1, 0x7d

    if-le v0, v1, :cond_0

    .line 210
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "ping payload exceeds 125 octets"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_0
    const/16 v0, 0x9

    const/4 v1, 0x1

    iget-object v2, p1, Lde/tavendo/autobahn/WebSocketMessage$Ping;->mPayload:[B

    invoke-virtual {p0, v0, v1, v2}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 213
    return-void
.end method

.method private sendPong(Lde/tavendo/autobahn/WebSocketMessage$Pong;)V
    .locals 3
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$Pong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    if-eqz v0, :cond_0

    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    array-length v0, v0

    const/16 v1, 0x7d

    if-le v0, v1, :cond_0

    .line 222
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "pong payload exceeds 125 octets"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    const/16 v0, 0xa

    const/4 v1, 0x1

    iget-object v2, p1, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    invoke-virtual {p0, v0, v1, v2}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 225
    return-void
.end method

.method private sendRawTextMessage(Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;)V
    .locals 3
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 255
    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;->mPayload:[B

    array-length v0, v0

    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxMessagePayloadSize()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 256
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "message payload exceeds payload limit"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_0
    iget-object v0, p1, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;->mPayload:[B

    invoke-virtual {p0, v2, v2, v0}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 259
    return-void
.end method

.method private sendTextMessage(Lde/tavendo/autobahn/WebSocketMessage$TextMessage;)V
    .locals 4
    .param p1, "message"    # Lde/tavendo/autobahn/WebSocketMessage$TextMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 243
    iget-object v1, p1, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;->mPayload:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 244
    .local v0, "payload":[B
    array-length v1, v0

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxMessagePayloadSize()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 245
    new-instance v1, Lde/tavendo/autobahn/WebSocketException;

    const-string v2, "message payload exceeds payload limit"

    invoke-direct {v1, v2}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 247
    :cond_0
    invoke-virtual {p0, v3, v3, v0}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[B)V

    .line 248
    return-void
.end method


# virtual methods
.method public forward(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 93
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 94
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 95
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 96
    return-void
.end method

.method protected processAppMessage(Ljava/lang/Object;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lde/tavendo/autobahn/WebSocketException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 408
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "unknown message received by WebSocketWriter"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected processMessage(Ljava/lang/Object;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 359
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    if-eqz v0, :cond_0

    .line 360
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendTextMessage(Lde/tavendo/autobahn/WebSocketMessage$TextMessage;)V

    .line 380
    :goto_0
    return-void

    .line 361
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    if-eqz v0, :cond_1

    .line 362
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendRawTextMessage(Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;)V

    goto :goto_0

    .line 363
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    if-eqz v0, :cond_2

    .line 364
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendBinaryMessage(Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;)V

    goto :goto_0

    .line 365
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Ping;

    if-eqz v0, :cond_3

    .line 366
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$Ping;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendPing(Lde/tavendo/autobahn/WebSocketMessage$Ping;)V

    goto :goto_0

    .line 367
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    if-eqz v0, :cond_4

    .line 368
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendPong(Lde/tavendo/autobahn/WebSocketMessage$Pong;)V

    goto :goto_0

    .line 369
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Close;

    if-eqz v0, :cond_5

    .line 370
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$Close;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendClose(Lde/tavendo/autobahn/WebSocketMessage$Close;)V

    goto :goto_0

    .line 371
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_5
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;

    if-eqz v0, :cond_6

    .line 372
    check-cast p1, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;

    .end local p1    # "msg":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->sendClientHandshake(Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;)V

    goto :goto_0

    .line 373
    .restart local p1    # "msg":Ljava/lang/Object;
    :cond_6
    instance-of v0, p1, Lde/tavendo/autobahn/WebSocketMessage$Quit;

    if-eqz v0, :cond_7

    .line 374
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 376
    sget-object v0, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket writer ended."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 378
    :cond_7
    invoke-virtual {p0, p1}, Lde/tavendo/autobahn/WebSocketWriter;->processAppMessage(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 416
    const/4 v1, 0x0

    .line 418
    .local v1, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketWriter;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 423
    :goto_0
    iput-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mOutputStream:Ljava/io/OutputStream;

    .line 425
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 427
    new-instance v2, Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;

    invoke-direct {v2, p0}, Lde/tavendo/autobahn/WebSocketWriter$ThreadHandler;-><init>(Lde/tavendo/autobahn/WebSocketWriter;)V

    iput-object v2, p0, Lde/tavendo/autobahn/WebSocketWriter;->mHandler:Landroid/os/Handler;

    .line 429
    monitor-enter p0

    .line 430
    :try_start_1
    sget-object v2, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    const-string v3, "WebSocker writer running."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 429
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 435
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 436
    return-void

    .line 419
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 429
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method protected sendFrame(IZ[B)V
    .locals 6
    .param p1, "opcode"    # I
    .param p2, "fin"    # Z
    .param p3, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 271
    if-eqz p3, :cond_0

    .line 272
    array-length v5, p3

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[BII)V

    .line 276
    :goto_0
    return-void

    .line 274
    :cond_0
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lde/tavendo/autobahn/WebSocketWriter;->sendFrame(IZ[BII)V

    goto :goto_0
.end method

.method protected sendFrame(IZ[BII)V
    .locals 18
    .param p1, "opcode"    # I
    .param p2, "fin"    # Z
    .param p3, "payload"    # [B
    .param p4, "offset"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    const/4 v4, 0x0

    .line 292
    .local v4, "b0":B
    if-eqz p2, :cond_0

    .line 293
    const/16 v10, -0x80

    int-to-byte v4, v10

    .line 295
    :cond_0
    move/from16 v0, p1

    int-to-byte v10, v0

    or-int/2addr v10, v4

    int-to-byte v4, v10

    .line 296
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 299
    const/4 v5, 0x0

    .line 300
    .local v5, "b1":B
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v10}, Lde/tavendo/autobahn/WebSocketOptions;->getMaskClientFrames()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 301
    const/16 v5, -0x80

    .line 304
    :cond_1
    move/from16 v0, p5

    int-to-long v8, v0

    .line 307
    .local v8, "len":J
    const-wide/16 v10, 0x7d

    cmp-long v10, v8, v10

    if-gtz v10, :cond_5

    .line 308
    long-to-int v10, v8

    int-to-byte v10, v10

    or-int/2addr v10, v5

    int-to-byte v5, v10

    .line 309
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 327
    :goto_0
    const/4 v7, 0x0

    .line 328
    .local v7, "mask":[B
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v10}, Lde/tavendo/autobahn/WebSocketOptions;->getMaskClientFrames()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 330
    invoke-direct/range {p0 .. p0}, Lde/tavendo/autobahn/WebSocketWriter;->newFrameMask()[B

    move-result-object v7

    .line 331
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v11, 0x0

    aget-byte v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 332
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v11, 0x1

    aget-byte v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 333
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v11, 0x2

    aget-byte v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 334
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v11, 0x3

    aget-byte v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 337
    :cond_2
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_4

    .line 338
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v10}, Lde/tavendo/autobahn/WebSocketOptions;->getMaskClientFrames()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 341
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    int-to-long v10, v6

    cmp-long v10, v10, v8

    if-ltz v10, :cond_7

    .line 345
    .end local v6    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v10, v0, v1, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 347
    :cond_4
    return-void

    .line 310
    .end local v7    # "mask":[B
    :cond_5
    const-wide/32 v10, 0xffff

    cmp-long v10, v8, v10

    if-gtz v10, :cond_6

    .line 311
    or-int/lit8 v10, v5, 0x7e

    int-to-byte v5, v10

    .line 312
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 313
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v11, 0x2

    new-array v11, v11, [B

    const/4 v12, 0x0

    const/16 v13, 0x8

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x1

    const-wide/16 v14, 0xff

    and-long/2addr v14, v8

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 315
    :cond_6
    or-int/lit8 v10, v5, 0x7f

    int-to-byte v5, v10

    .line 316
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 317
    move-object/from16 v0, p0

    iget-object v10, v0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/16 v11, 0x8

    new-array v11, v11, [B

    const/4 v12, 0x0

    const/16 v13, 0x38

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x1

    .line 318
    const/16 v13, 0x30

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x2

    .line 319
    const/16 v13, 0x28

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x3

    .line 320
    const/16 v13, 0x20

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x4

    .line 321
    const/16 v13, 0x18

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x5

    .line 322
    const/16 v13, 0x10

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x6

    .line 323
    const/16 v13, 0x8

    shr-long v14, v8, v13

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    const/4 v12, 0x7

    .line 324
    const-wide/16 v14, 0xff

    and-long/2addr v14, v8

    long-to-int v13, v14

    int-to-byte v13, v13

    aput-byte v13, v11, v12

    .line 317
    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 342
    .restart local v6    # "i":I
    .restart local v7    # "mask":[B
    :cond_7
    add-int v10, v6, p4

    aget-byte v11, p3, v10

    rem-int/lit8 v12, v6, 0x4

    aget-byte v12, v7, v12

    xor-int/2addr v11, v12

    int-to-byte v11, v11

    aput-byte v11, p3, v10

    .line 341
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1
.end method

.method public writeMessageToBuffer(Landroid/os/Message;)V
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 384
    :try_start_0
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 385
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->processMessage(Ljava/lang/Object;)V

    .line 386
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 388
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketWriter;->mOutputStream:Ljava/io/OutputStream;

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketWriter;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 399
    :goto_0
    return-void

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Ljava/net/SocketException;
    sget-object v1, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "run() : SocketException ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;

    invoke-direct {v1}, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;-><init>()V

    invoke-direct {p0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->notify(Ljava/lang/Object;)V

    goto :goto_0

    .line 393
    .end local v0    # "e":Ljava/net/SocketException;
    :catch_1
    move-exception v0

    .line 394
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lde/tavendo/autobahn/WebSocketWriter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "run() : IOException ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 396
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 397
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$Error;

    invoke-direct {v1, v0}, Lde/tavendo/autobahn/WebSocketMessage$Error;-><init>(Ljava/lang/Exception;)V

    invoke-direct {p0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->notify(Ljava/lang/Object;)V

    goto :goto_0
.end method
