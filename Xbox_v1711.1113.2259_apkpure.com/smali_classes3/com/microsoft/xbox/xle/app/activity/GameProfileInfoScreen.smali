.class public Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameProfileInfoScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;

    .line 20
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "GameProfileInfo"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0705cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->onCreateContentView()V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 33
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onCreate()V

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 35
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f030124

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;->setContentView(I)V

    .line 40
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method
