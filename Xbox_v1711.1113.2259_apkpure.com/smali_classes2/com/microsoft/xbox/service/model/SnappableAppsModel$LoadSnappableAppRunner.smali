.class Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "SnappableAppsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/SnappableAppsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSnappableAppRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

.field private final ids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

.field private final titleIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/SnappableAppsModel;Lcom/microsoft/xbox/service/model/SnappableAppsModel;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->this$0:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->titleIds:Ljava/util/ArrayList;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->ids:Ljava/util/ArrayList;

    .line 120
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->caller:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    .line 121
    return-void
.end method


# virtual methods
.method public buildData()Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->browseSnappableAppList()Ljava/util/ArrayList;

    move-result-object v1

    .line 127
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 128
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    instance-of v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    if-eqz v4, :cond_0

    move-object v4, v0

    .line 129
    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getTitleId()J

    move-result-wide v2

    .line 130
    .local v2, "titleId":J
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->titleIds:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->ids:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 135
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v2    # "titleId":J
    :cond_1
    new-instance v4, Landroid/util/Pair;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->titleIds:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->ids:Ljava/util/ArrayList;

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->buildData()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 149
    const-wide/16 v0, 0x106a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Landroid/util/Pair<Ljava/util/ArrayList<Ljava/lang/Long;>;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;->caller:Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 145
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method
