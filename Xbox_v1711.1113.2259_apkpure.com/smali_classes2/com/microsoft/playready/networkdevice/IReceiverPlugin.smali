.class public interface abstract Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
.super Ljava/lang/Object;
.source "IReceiverPlugin.java"


# virtual methods
.method public abstract getDeviceRegistrationCustomData()Lcom/microsoft/playready/networkdevice/CustomData;
.end method

.method public abstract getLicenseFetchCustomData(Lcom/microsoft/playready/networkdevice/ContentIDType;[B)Lcom/microsoft/playready/networkdevice/CustomData;
.end method

.method public abstract onAuthenticateTransmitter(Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;)Z
.end method

.method public abstract onProcessLicenseCustomResponseData(Lcom/microsoft/playready/networkdevice/CustomData;)Z
.end method
