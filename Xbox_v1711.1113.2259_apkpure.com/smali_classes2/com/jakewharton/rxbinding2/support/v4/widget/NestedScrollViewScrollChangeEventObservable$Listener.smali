.class final Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "NestedScrollViewScrollChangeEventObservable.java"

# interfaces
.implements Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/support/v4/widget/NestedScrollView;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/NestedScrollView;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "view"    # Landroid/support/v4/widget/NestedScrollView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/widget/NestedScrollView;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p2, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->view:Landroid/support/v4/widget/NestedScrollView;

    .line 34
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 35
    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 2

    .prologue
    .line 47
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->view:Landroid/support/v4/widget/NestedScrollView;

    const/4 v0, 0x0

    check-cast v0, Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/NestedScrollView;->setOnScrollChangeListener(Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;)V

    .line 48
    return-void
.end method

.method public onScrollChange(Landroid/support/v4/widget/NestedScrollView;IIII)V
    .locals 2
    .param p1, "v"    # Landroid/support/v4/widget/NestedScrollView;
    .param p2, "scrollX"    # I
    .param p3, "scrollY"    # I
    .param p4, "oldScrollX"    # I
    .param p5, "oldScrollY"    # I

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->observer:Lio/reactivex/Observer;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v4/widget/NestedScrollViewScrollChangeEventObservable$Listener;->view:Landroid/support/v4/widget/NestedScrollView;

    .line 42
    invoke-static {v1, p2, p3, p4, p5}, Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;->create(Landroid/view/View;IIII)Lcom/jakewharton/rxbinding2/view/ViewScrollChangeEvent;

    move-result-object v1

    .line 41
    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 44
    :cond_0
    return-void
.end method
