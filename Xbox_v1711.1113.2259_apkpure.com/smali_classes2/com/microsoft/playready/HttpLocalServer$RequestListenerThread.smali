.class Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;
.super Ljava/lang/Thread;
.source "HttpLocalServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/HttpLocalServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RequestListenerThread"
.end annotation


# instance fields
.field private final httpRegistry:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

.field private final httpService:Lorg/apache/http/protocol/HttpService;

.field private isPaused:Z

.field private final params:Lorg/apache/http/params/HttpParams;

.field private final serversocket:Ljava/net/ServerSocket;

.field private final workerThreadMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Thread;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final workerThreadMapLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .locals 5
    .param p1, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 86
    iput-boolean v4, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->isPaused:Z

    .line 91
    new-instance v1, Ljava/net/ServerSocket;

    invoke-direct {v1, p1}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    .line 92
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    .line 93
    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    const-string v2, "http.socket.timeout"

    const/16 v3, 0x1388

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.socket.buffer-size"

    const/16 v3, 0x2000

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 94
    const-string v2, "http.connection.stalecheck"

    invoke-interface {v1, v2, v4}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.tcp.nodelay"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 95
    const-string v2, "http.origin-server"

    const-string v3, "HttpComponents/1.1"

    invoke-interface {v1, v2, v3}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 98
    new-instance v0, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 99
    .local v0, "httpproc":Lorg/apache/http/protocol/BasicHttpProcessor;
    new-instance v1, Lorg/apache/http/protocol/ResponseDate;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseDate;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 100
    new-instance v1, Lorg/apache/http/protocol/ResponseServer;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseServer;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 101
    new-instance v1, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 102
    new-instance v1, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 104
    new-instance v1, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v1}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpRegistry:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    .line 107
    new-instance v1, Lorg/apache/http/protocol/HttpService;

    new-instance v2, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v2}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v3, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v3}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v1, v0, v2, v3}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    .line 108
    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v1, v2}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 110
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMap:Ljava/util/WeakHashMap;

    .line 111
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMapLock:Ljava/lang/Object;

    .line 112
    return-void
.end method


# virtual methods
.method kill()V
    .locals 3

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->interrupt()V

    .line 127
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "HttpLocalServer"

    const-string v2, "Exception occured when killing a HTTP response Handler"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 174
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->isPaused:Z

    .line 177
    iget-object v2, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMap:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 183
    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMap:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->clear()V

    .line 177
    monitor-exit v2

    .line 185
    return-void

    .line 178
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 179
    .local v0, "t":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 177
    .end local v0    # "t":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method registerMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 3
    .param p1, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpRegistry:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 117
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    iget-object v1, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpRegistry:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 118
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    .line 134
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    :goto_1
    return-void

    .line 137
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->serversocket:Ljava/net/ServerSocket;

    invoke-virtual {v5}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 145
    .local v3, "socket":Ljava/net/Socket;
    :goto_2
    iget-boolean v5, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->isPaused:Z

    if-nez v5, :cond_1

    .line 153
    new-instance v0, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 155
    .local v0, "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    iget-object v5, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->params:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v0, v3, v5}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 157
    new-instance v4, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;

    iget-object v5, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpService:Lorg/apache/http/protocol/HttpService;

    invoke-direct {v4, v5, v0}, Lcom/microsoft/playready/HttpLocalServer$WorkerThread;-><init>(Lorg/apache/http/protocol/HttpService;Lorg/apache/http/HttpServerConnection;)V

    .line 158
    .local v4, "t":Ljava/lang/Thread;
    iget-object v6, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMapLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 159
    :try_start_1
    iget-object v5, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->workerThreadMap:Ljava/util/WeakHashMap;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v4, v7}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 162
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 163
    .end local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v3    # "socket":Ljava/net/Socket;
    .end local v4    # "t":Ljava/lang/Thread;
    :catch_0
    move-exception v2

    .line 164
    .local v2, "ex":Ljava/io/InterruptedIOException;
    goto :goto_1

    .line 147
    .end local v2    # "ex":Ljava/io/InterruptedIOException;
    .restart local v3    # "socket":Ljava/net/Socket;
    :cond_1
    const-wide/16 v6, 0x64

    :try_start_3
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 148
    :catch_1
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 165
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "socket":Ljava/net/Socket;
    :catch_2
    move-exception v1

    .line 166
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "HttpLocalServer"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 158
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "conn":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v3    # "socket":Ljava/net/Socket;
    .restart local v4    # "t":Ljava/lang/Thread;
    :catchall_0
    move-exception v5

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v5
    :try_end_6
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
.end method

.method public unpause()V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->isPaused:Z

    .line 189
    return-void
.end method

.method unregisterMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 3
    .param p1, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->httpRegistry:Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->unregister(Ljava/lang/String;)V

    .line 122
    return-void
.end method
