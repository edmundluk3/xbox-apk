.class public abstract Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;
.super Landroid/widget/RelativeLayout;
.source "MultiPaneScreen.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method


# virtual methods
.method public addPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 11
    .param p1, "paneData"    # Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .param p2, "index"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 79
    const-string v3, "MultiPaneScreen"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Adding  pane \'%s\' at index %d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v2, 0x0

    .line 83
    .local v2, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-object v2, v0

    .line 84
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getScreenRatio()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenPercent(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 85
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getResourceId()I

    move-result v3

    if-lez v3, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setContentView(I)V

    .line 87
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setHeaderName(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 95
    const/4 v2, 0x0

    .line 99
    .end local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_1
    return-object v2

    .line 90
    .restart local v2    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "MultiPaneScreen"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "FIXME: Failed to create a screen of type \'%s\' with error: %s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0, v2, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    goto :goto_1
.end method

.method public abstract addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
.end method

.method public abstract adjustBottomMargin(I)V
.end method

.method public abstract adjustPaneSize(Ljava/lang/Class;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract animateToCurrentPaneIndex(I)V
.end method

.method public abstract forceRefresh()V
.end method

.method public abstract getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
.end method

.method public abstract getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
.end method

.method public abstract getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.end method

.method public abstract getCurrentPaneIndex()I
.end method

.method public abstract getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
.end method

.method public abstract getIndexOfScreen(Ljava/lang/Class;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation
.end method

.method protected abstract getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.end method

.method public abstract getIsScrolling()Z
.end method

.method public abstract getTotalPaneCount()I
.end method

.method protected abstract initialize()V
.end method

.method public isPageIndicatorVisible()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public abstract onAnimateInCompleted()V
.end method

.method public abstract onAnimateInStarted()V
.end method

.method public abstract onContextItemSelected(Landroid/view/MenuItem;)Z
.end method

.method public abstract onCreate()V
.end method

.method public abstract onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
.end method

.method public abstract onDestroy()V
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->initialize()V

    .line 64
    return-void
.end method

.method public abstract onPause()V
.end method

.method public abstract onRehydrate()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onSetActive()V
.end method

.method public abstract onSetActive(I)V
.end method

.method public abstract onSetInactive()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method

.method public abstract onTombstone()V
.end method

.method public abstract removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.end method

.method public abstract resetBottomMargin()V
.end method

.method public abstract setActiveOnAdd(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
.end method

.method public abstract setCurrentPaneIndex(I)V
.end method

.method public abstract setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V
.end method

.method public abstract setOnScrollingChangedRunnable(Ljava/lang/Runnable;)V
.end method

.method public setPaneData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V
    .locals 5
    .param p1, "panesData"    # [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "index":I
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, p1, v2

    .line 72
    .local v1, "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getIsDisplayed()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 73
    add-int/lit8 v0, v0, 0x1

    .line 71
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 76
    .end local v1    # "paneData":Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    :cond_1
    return-void
.end method

.method public setScreenState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 45
    return-void
.end method

.method public abstract setStartPaneIndex(I)V
.end method

.method public abstract switchToPane(I)V
.end method

.method public xleFindViewId(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method
