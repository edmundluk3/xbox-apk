.class synthetic Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;
.super Ljava/lang/Object;
.source "ClubHomeScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 798
    invoke-static {}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->values()[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_2
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    .line 603
    :goto_4
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->values()[Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    :try_start_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->InvitationPending:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_5
    :try_start_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_6
    :try_start_7
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->CancelRequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    :try_start_8
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RemoveSelfFromClub:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_8
    :try_start_9
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    .line 291
    :goto_9
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

    :try_start_a
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Social:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_a
    :try_start_b
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    .line 293
    :goto_b
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    :try_start_c
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_c
    :try_start_d
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_d
    :try_start_e
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_e
    return-void

    :catch_0
    move-exception v0

    goto :goto_e

    :catch_1
    move-exception v0

    goto :goto_d

    :catch_2
    move-exception v0

    goto :goto_c

    .line 291
    :catch_3
    move-exception v0

    goto :goto_b

    :catch_4
    move-exception v0

    goto :goto_a

    .line 603
    :catch_5
    move-exception v0

    goto :goto_9

    :catch_6
    move-exception v0

    goto :goto_8

    :catch_7
    move-exception v0

    goto :goto_7

    :catch_8
    move-exception v0

    goto :goto_6

    :catch_9
    move-exception v0

    goto/16 :goto_5

    .line 798
    :catch_a
    move-exception v0

    goto/16 :goto_4

    :catch_b
    move-exception v0

    goto/16 :goto_3

    :catch_c
    move-exception v0

    goto/16 :goto_2

    :catch_d
    move-exception v0

    goto/16 :goto_1

    :catch_e
    move-exception v0

    goto/16 :goto_0
.end method
