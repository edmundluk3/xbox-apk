.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;
.super Ljava/lang/Object;
.source "EDSV2SearchDataModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EDSV2SearchDataModelHolder"
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->instance:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->instance:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    return-object v0
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 42
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->reset()V

    return-void
.end method

.method private static reset()V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->instance:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .line 48
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
