.class public Lcom/microsoft/xbox/smartglass/Accelerometer;
.super Lcom/microsoft/xbox/smartglass/Sensor;
.source "Accelerometer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Sensor",
        "<",
        "Lcom/microsoft/xbox/smartglass/AccelerometerListener;",
        ">;"
    }
.end annotation


# static fields
.field private static final SENSOR_TYPE:Ljava/lang/String; = "accelerometer"

.field static isSupported:Z


# direct methods
.method constructor <init>(Landroid/hardware/SensorManager;)V
    .locals 2
    .param p1, "manager"    # Landroid/hardware/SensorManager;

    .prologue
    .line 25
    const-string v0, "accelerometer"

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/microsoft/xbox/smartglass/Sensor;-><init>(Ljava/lang/String;Landroid/hardware/SensorManager;Landroid/hardware/Sensor;)V

    .line 26
    return-void
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 21
    sget-boolean v0, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported:Z

    return v0
.end method


# virtual methods
.method createListener()Landroid/hardware/SensorEventListener;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;-><init>(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/Accelerometer$1;)V

    return-object v0
.end method

.method public handleCommand(Lcom/microsoft/xbox/smartglass/JavaScriptCommand;Lcom/microsoft/xbox/smartglass/SensorArguments;)Lcom/microsoft/xbox/smartglass/SensorResult;
    .locals 4
    .param p1, "command"    # Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
    .param p2, "arguments"    # Lcom/microsoft/xbox/smartglass/SensorArguments;

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/smartglass/SensorResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/SensorResult;-><init>()V

    .line 31
    .local v0, "result":Lcom/microsoft/xbox/smartglass/SensorResult;
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->methodName:Ljava/lang/String;

    const-string v2, "IsSupported"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    .line 33
    const-string v1, "isSupported"

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldName:Ljava/lang/String;

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/smartglass/Accelerometer;->isSupported()Z

    move-result v1

    iput-boolean v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldValue:Z

    .line 52
    :goto_0
    return-object v0

    .line 35
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->methodName:Ljava/lang/String;

    const-string v2, "Start"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    iget v1, p2, Lcom/microsoft/xbox/smartglass/SensorArguments;->sampleRate:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/smartglass/Accelerometer;->start(I)V

    .line 37
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    goto :goto_0

    .line 38
    :cond_1
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->methodName:Ljava/lang/String;

    const-string v2, "IsActive"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    .line 40
    const-string v1, "isActive"

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldName:Ljava/lang/String;

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/Accelerometer;->isActive()Z

    move-result v1

    iput-boolean v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldValue:Z

    goto :goto_0

    .line 42
    :cond_2
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->methodName:Ljava/lang/String;

    const-string v2, "Stop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/Accelerometer;->stop()V

    .line 44
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    goto :goto_0

    .line 45
    :cond_3
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->methodName:Ljava/lang/String;

    const-string v2, "SetTarget"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 46
    new-instance v1, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v2, p2, Lcom/microsoft/xbox/smartglass/SensorArguments;->titleId:I

    iget v3, p2, Lcom/microsoft/xbox/smartglass/SensorArguments;->service:I

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(II)V

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/Accelerometer;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 47
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    goto :goto_0

    .line 49
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->NotFound:Lcom/microsoft/xbox/smartglass/SGError;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    goto :goto_0
.end method
