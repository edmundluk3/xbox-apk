.class Lcom/microsoft/xbox/smartglass/SensorManager$1;
.super Lcom/microsoft/xbox/smartglass/AccelerometerListener;
.source "SensorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/SensorManager;->getAccelerometer()Lcom/microsoft/xbox/smartglass/Accelerometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/SensorManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/SensorManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/SensorManager;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/SensorManager$1;->this$0:Lcom/microsoft/xbox/smartglass/SensorManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/AccelerometerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReadingChanged(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/AccelerometerReading;)V
    .locals 2
    .param p1, "accelerometer"    # Lcom/microsoft/xbox/smartglass/Accelerometer;
    .param p2, "reading"    # Lcom/microsoft/xbox/smartglass/AccelerometerReading;

    .prologue
    .line 45
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/Accelerometer;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    if-eqz v0, :cond_0

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SensorManager$1;->this$0:Lcom/microsoft/xbox/smartglass/SensorManager;

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/Accelerometer;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v0, p2, v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendAccelerometerReading(Lcom/microsoft/xbox/smartglass/AccelerometerReading;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0
.end method
