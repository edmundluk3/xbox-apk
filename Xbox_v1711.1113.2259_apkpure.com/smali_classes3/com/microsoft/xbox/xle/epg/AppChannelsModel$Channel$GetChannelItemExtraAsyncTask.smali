.class Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "AppChannelsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetChannelItemExtraAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final mShowId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Ljava/lang/String;)V
    .locals 1
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 628
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 629
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 630
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->mShowId:Ljava/lang/String;

    .line 631
    return-void
.end method


# virtual methods
.method protected doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 635
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->mShowId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestAppChannelProgramData(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 636
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 624
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x0

    .line 645
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->mShowId:Ljava/lang/String;

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->onAppChannelProgramDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 624
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 641
    return-void
.end method
