.class public Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubCardListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_TAG_COLOR:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation
.end field

.field private tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->TAG:Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c014e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->DEFAULT_TAG_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 41
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "listener"    # Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    .line 47
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->DEFAULT_TAG_COLOR:I

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 56
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V

    .line 57
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030068

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;Landroid/view/View;)V

    return-object v0
.end method
