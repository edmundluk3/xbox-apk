.class public abstract Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;
.super Ljava/lang/Object;
.source "SkypeMessagingDB.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MessageEntry"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;

.field public static final COL_CLIENTMESSAGEID:Ljava/lang/String; = "clientmessageid"

.field public static final COL_CONTENT:Ljava/lang/String; = "content"

.field public static final COL_CONVERSATIONID:Ljava/lang/String; = "conversationid"

.field public static final COL_CONVERSATIONLINK:Ljava/lang/String; = "conversationlink"

.field public static final COL_FROM:Ljava/lang/String; = "fromuser"

.field public static final COL_ID:Ljava/lang/String; = "id"

.field public static final COL_MESSAGETYPE:Ljava/lang/String; = "messagetype"

.field public static final COL_ORIGINALARRIVALTIME:Ljava/lang/String; = "originalarrivaltime"

.field public static final COL_SKYPEEDITEDID:Ljava/lang/String; = "skypeeditedid"

.field public static final COL_SYNC_STATE:Ljava/lang/String; = "syncstate"

.field public static final COL_TYPE:Ljava/lang/String; = "type"

.field public static final CREATE_MESSAGES_TABLE:Ljava/lang/String;

.field public static final DELETE_MESSAGES_TABLE:Ljava/lang/String;

.field public static final TABLE_NAME:Ljava/lang/String; = "messages"


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1568
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s %s (%s %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s)"

    const/16 v2, 0x1b

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "CREATE TABLE IF NOT EXISTS"

    aput-object v3, v2, v5

    const-string v3, "messages"

    aput-object v3, v2, v6

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "INTEGER"

    aput-object v3, v2, v8

    const-string v3, "PRIMARY KEY AUTOINCREMENT"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "type"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "messagetype"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "content"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "originalarrivaltime"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "INTEGER"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "fromuser"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "conversationlink"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "skypeeditedid"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string v4, "conversationid"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string v4, "clientmessageid"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "syncstate"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->CREATE_MESSAGES_TABLE:Ljava/lang/String;

    .line 1585
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s %s"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "DROP TABLE IF EXISTS"

    aput-object v3, v2, v5

    const-string v3, "messages"

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->DELETE_MESSAGES_TABLE:Ljava/lang/String;

    .line 1590
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "id"

    aput-object v1, v0, v6

    const-string v1, "type"

    aput-object v1, v0, v7

    const-string v1, "messagetype"

    aput-object v1, v0, v8

    const-string v1, "content"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "originalarrivaltime"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "fromuser"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "conversationlink"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "skypeeditedid"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "conversationid"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "clientmessageid"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "syncstate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
