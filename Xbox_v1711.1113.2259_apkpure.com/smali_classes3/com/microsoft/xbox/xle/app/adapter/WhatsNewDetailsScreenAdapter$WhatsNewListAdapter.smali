.class Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "WhatsNewDetailsScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WhatsNewListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 54
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "recycleView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 59
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 61
    move-object v2, p2

    .line 62
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    .local v0, "data":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$WhatsNewListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 65
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030278

    const/4 v5, 0x0

    invoke-virtual {v1, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 66
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;

    invoke-direct {v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 69
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;

    .line 71
    .local v3, "views":Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;
    iget-object v4, v3, Lcom/microsoft/xbox/xle/app/adapter/WhatsNewDetailsScreenAdapter$ViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-object v2
.end method
