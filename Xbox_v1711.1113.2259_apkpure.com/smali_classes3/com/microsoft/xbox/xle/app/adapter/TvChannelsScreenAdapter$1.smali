.class Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;
.super Ljava/lang/Object;
.source "TvChannelsScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 201
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    .line 221
    :goto_0
    return-void

    .line 205
    :cond_0
    new-instance v2, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 206
    .local v2, "providerMenu":Landroid/widget/PopupMenu;
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 208
    .local v0, "menu":Landroid/view/Menu;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getSortedProviders()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 209
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;

    invoke-direct {v5, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter$1;Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 220
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method
