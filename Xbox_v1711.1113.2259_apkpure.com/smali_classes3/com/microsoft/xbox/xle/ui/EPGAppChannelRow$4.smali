.class Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;
.super Ljava/lang/Object;
.source "EPGAppChannelRow.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->startAnimateBottomFrame(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

.field final synthetic val$finishAlpha:F

.field final synthetic val$finishHeight:F

.field final synthetic val$startAlpha:F

.field final synthetic val$startX:F


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;FFFF)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$startX:F

    iput p3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$startAlpha:F

    iput p4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$finishAlpha:F

    iput p5, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$finishHeight:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 233
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$finishAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$300(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$finishHeight:F

    float-to-double v2, v0

    const-wide v4, 0x3f947ae147ae147bL    # 0.02

    cmpg-double v0, v2, v4

    if-gez v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$finishHeight:F

    :goto_0
    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$400(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$502(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 229
    return-void

    .line 226
    :cond_1
    const/high16 v0, -0x40000000    # -2.0f

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 217
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$startX:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->this$0:Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;->access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelRow$4;->val$startAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 213
    :cond_0
    return-void
.end method
