.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaderboardEntry"
.end annotation


# instance fields
.field private final columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final rank:J

.field public final xuid:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;)V
    .locals 1
    .param p1, "rank"    # J
    .param p3, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "columns"    # Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iput-wide p1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    .line 259
    iput-object p3, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    .line 260
    iput-object p4, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    .line 261
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273
    if-ne p1, p0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return v1

    .line 275
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    if-nez v3, :cond_2

    move v1, v2

    .line 276
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 278
    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    .line 279
    .local v0, "other":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    .line 280
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    iget-object v4, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    .line 281
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getGamerscore()J
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    invoke-static {v0}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;->access$100(Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;)J

    move-result-wide v0

    .line 268
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 287
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    if-nez v0, :cond_0

    .line 288
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    .line 289
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    .line 290
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    .line 291
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->columns:Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$GamerscoreColumn;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    .line 294
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
