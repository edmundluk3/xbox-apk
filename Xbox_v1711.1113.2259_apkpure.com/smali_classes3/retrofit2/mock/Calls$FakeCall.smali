.class final Lretrofit2/mock/Calls$FakeCall;
.super Ljava/lang/Object;
.source "Calls.java"

# interfaces
.implements Lretrofit2/Call;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/mock/Calls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FakeCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/Call",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final error:Ljava/io/IOException;

.field private final executed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final response:Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/Response",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lretrofit2/Response;Ljava/io/IOException;)V
    .locals 3
    .param p2, "error"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Response",
            "<TT;>;",
            "Ljava/io/IOException;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    .local p1, "response":Lretrofit2/Response;, "Lretrofit2/Response<TT;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v2, p0, Lretrofit2/mock/Calls$FakeCall;->canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v2, p0, Lretrofit2/mock/Calls$FakeCall;->executed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 59
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-ne v2, v0, :cond_2

    .line 60
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Only one of response or error can be set."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    move v2, v1

    .line 59
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 62
    :cond_2
    iput-object p1, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    .line 63
    iput-object p2, p0, Lretrofit2/mock/Calls$FakeCall;->error:Ljava/io/IOException;

    .line 64
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 100
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 101
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    invoke-virtual {p0}, Lretrofit2/mock/Calls$FakeCall;->clone()Lretrofit2/Call;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lretrofit2/Call;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    new-instance v0, Lretrofit2/mock/Calls$FakeCall;

    iget-object v1, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    iget-object v2, p0, Lretrofit2/mock/Calls$FakeCall;->error:Ljava/io/IOException;

    invoke-direct {v0, v1, v2}, Lretrofit2/mock/Calls$FakeCall;-><init>(Lretrofit2/Response;Ljava/io/IOException;)V

    return-object v0
.end method

.method public enqueue(Lretrofit2/Callback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    .local p1, "callback":Lretrofit2/Callback;, "Lretrofit2/Callback<TT;>;"
    if-nez p1, :cond_0

    .line 81
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->executed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    new-instance v0, Ljava/io/IOException;

    const-string v1, "canceled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p0, v0}, Lretrofit2/Callback;->onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V

    .line 93
    :goto_0
    return-void

    .line 88
    :cond_2
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    if-eqz v0, :cond_3

    .line 89
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    invoke-interface {p1, p0, v0}, Lretrofit2/Callback;->onResponse(Lretrofit2/Call;Lretrofit2/Response;)V

    goto :goto_0

    .line 91
    :cond_3
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->error:Ljava/io/IOException;

    invoke-interface {p1, p0, v0}, Lretrofit2/Callback;->onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public execute()Lretrofit2/Response;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Response",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->executed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    new-instance v0, Ljava/io/IOException;

    const-string v1, "canceled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    return-object v0

    .line 76
    :cond_2
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->error:Ljava/io/IOException;

    throw v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->canceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isExecuted()Z
    .locals 1

    .prologue
    .line 96
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->executed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public request()Lokhttp3/Request;
    .locals 2

    .prologue
    .line 112
    .local p0, "this":Lretrofit2/mock/Calls$FakeCall;, "Lretrofit2/mock/Calls$FakeCall<TT;>;"
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lretrofit2/mock/Calls$FakeCall;->response:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->raw()Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    const-string v1, "http://localhost"

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    goto :goto_0
.end method
