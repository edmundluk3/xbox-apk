.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Club"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    }
.end annotation


# instance fields
.field private transient isLoaded:Ljava/lang/Boolean;

.field private transient preferredColorCache:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1

    .prologue
    .line 445
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;-><init>()V

    return-object v0
.end method

.method private getClubPresenceString()Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const v4, 0x7f07024e

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 392
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-ne v0, v1, :cond_0

    .line 394
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 395
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070265

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 399
    :goto_0
    return-object v0

    .line 396
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    .line 397
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 240
    if-nez p0, :cond_0

    .line 250
    :goto_0
    return v0

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 245
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->creationDateUtc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 246
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 245
    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded:Ljava/lang/Boolean;

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public static merge(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 6
    .param p0, "newClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "oldClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 407
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 408
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 409
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 411
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 412
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->id(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 413
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 414
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubType(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 415
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->shortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->shortName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->ownerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 417
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->founderXuid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->founderXuid(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 418
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->creationDateUtc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->creationDateUtc(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 419
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->glyphImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->bannerImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 421
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 422
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v0

    :goto_3
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->followersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 423
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v0

    :goto_4
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->membersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 424
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v0

    :goto_5
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->moderatorsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 425
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v0

    :goto_6
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->recommendedCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 426
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v0

    :goto_7
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->requestedToJoinCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 427
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    :goto_8
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubPresenceCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 428
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v0

    :goto_9
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubPresenceTodayCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v2

    .line 429
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v0

    :goto_a
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubPresenceInGameCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 430
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    :goto_b
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 431
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v0

    :goto_c
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->targetRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 432
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->recommendation(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 433
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_d
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubPresence(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 434
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->state(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 435
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->titleDeeplinks(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 436
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->suspendedUntilUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 437
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportCount()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->reportCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 438
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportedItemsCount()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->reportedItemsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 439
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersPerClub()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->maxMembersPerClub(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 440
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersInGame()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->maxMembersInGame(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 411
    return-object v0

    .line 409
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 413
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    goto/16 :goto_1

    .line 421
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    goto/16 :goto_2

    .line 422
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v0

    goto/16 :goto_3

    .line 423
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v0

    goto/16 :goto_4

    .line 424
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v0

    goto/16 :goto_5

    .line 425
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v0

    goto/16 :goto_6

    .line 426
    :cond_7
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v0

    goto/16 :goto_7

    .line 427
    :cond_8
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    goto/16 :goto_8

    .line 428
    :cond_9
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v0

    goto/16 :goto_9

    .line 429
    :cond_a
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v0

    goto/16 :goto_a

    .line 430
    :cond_b
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    goto/16 :goto_b

    .line 431
    :cond_c
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v0

    goto/16 :goto_c

    .line 433
    :cond_d
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto/16 :goto_d
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public associatedTitles()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract bannerImageUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract clubPresence()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation
.end method

.method public abstract clubPresenceCount()J
.end method

.method public abstract clubPresenceInGameCount()J
.end method

.method public abstract clubPresenceTodayCount()J
.end method

.method public abstract clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract creationDateUtc()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract followersCount()J
.end method

.method public abstract founderXuid()J
.end method

.method public getPresenceStringWithMembership()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 361
    .local v2, "subTextBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 362
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 363
    .local v1, "roles":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 365
    .local v3, "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    if-eqz v3, :cond_0

    .line 366
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    .line 367
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 371
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 372
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07030b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    .end local v1    # "roles":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    .end local v3    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getClubPresenceString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 374
    .restart local v1    # "roles":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    .restart local v3    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :cond_2
    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 375
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070309

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 377
    :cond_3
    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 378
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07030a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 380
    :cond_4
    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Follower:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 381
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0701ed

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public getTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .locals 4
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 297
    .local v1, "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    if-eqz v1, :cond_1

    .line 298
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    .line 299
    .local v0, "roleItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 306
    .end local v0    # "roleItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .end local v1    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract glyphImageUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public hasManagementActions()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 352
    :goto_0
    return v0

    .line 354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Z
    .locals 1
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract id()J
.end method

.method public abstract maxMembersInGame()J
.end method

.method public abstract maxMembersPerClub()J
.end method

.method public abstract membersCount()J
.end method

.method public abstract moderatorsCount()J
.end method

.method public abstract ownerXuid()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColorCache:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v0, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 271
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 272
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 269
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColorCache:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColorCache:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public abstract profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract recommendedCount()J
.end method

.method public abstract reportCount()J
.end method

.method public abstract reportedItemsCount()J
.end method

.method public abstract requestedToJoinCount()J
.end method

.method public abstract roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract shortName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract suspendedUntilUtc()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.end method

.method public type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    .line 258
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    goto :goto_0
.end method

.method public userIsAdminOf()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 310
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 312
    .local v1, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    if-eqz v1, :cond_1

    .line 313
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 315
    .local v0, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-eqz v0, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 316
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 317
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 320
    .end local v0    # "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_1
    return v2
.end method

.method public userIsFollowerOf()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 339
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 341
    .local v1, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    if-eqz v1, :cond_0

    .line 342
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 344
    .local v0, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-eqz v0, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Follower:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 345
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 348
    .end local v0    # "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_0
    return v2
.end method

.method public userIsMemberOf()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 324
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 326
    .local v1, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    if-eqz v1, :cond_1

    .line 327
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 329
    .local v0, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-eqz v0, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 330
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 331
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 332
    invoke-virtual {v0, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 335
    .end local v0    # "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_1
    return v2
.end method
