.class public Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;
.super Ljava/lang/Object;
.source "XboxLiveServiceManager.java"


# instance fields
.field private final _pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 1
    .param p1, "pXboxLiveServiceManager"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->_pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 13
    return-void
.end method

.method private native getCurrentUserProfile(JLcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getUserProfileByGamertag(JLjava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getUserProfileByXuid(JJLcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native setCulture(JLjava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public getCurrentUserProfile(Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/ServiceRequestListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->_pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->getCurrentUserProfile(JLcom/microsoft/xbox/smartglass/ServiceRequestListener;)V

    .line 29
    return-void
.end method

.method public getUserProfile(JLcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .locals 7
    .param p1, "xuid"    # J
    .param p3, "listener"    # Lcom/microsoft/xbox/smartglass/ServiceRequestListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->_pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->getUserProfileByXuid(JJLcom/microsoft/xbox/smartglass/ServiceRequestListener;)V

    .line 47
    return-void
.end method

.method public getUserProfile(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .locals 2
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/microsoft/xbox/smartglass/ServiceRequestListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->_pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->getUserProfileByGamertag(JLjava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V

    .line 38
    return-void
.end method

.method public setCulture(Ljava/lang/String;)V
    .locals 2
    .param p1, "culture"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->_pXboxLiveServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/XboxLiveServiceManager;->setCulture(JLjava/lang/String;)J

    .line 21
    return-void
.end method
