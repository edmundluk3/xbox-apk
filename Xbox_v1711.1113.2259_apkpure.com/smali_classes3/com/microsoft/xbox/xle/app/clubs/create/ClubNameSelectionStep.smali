.class Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;
.super Ljava/lang/Object;
.source "ClubNameSelectionStep.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;


# instance fields
.field private backBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private clubCreateCheck:Landroid/widget/TextView;

.field private inputNameDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

.field private nameTextView:Landroid/widget/TextView;

.field private nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private progressBar:Landroid/widget/ProgressBar;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 39
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->onClubNameSelected(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->previousPage()V

    return-void
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateNext()V

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->nextPage()V

    .line 55
    return-void
.end method

.method static synthetic lambda$onCreateContentView$2(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 59
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->inputNameDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    return-void
.end method

.method private onClubNameSelected(Ljava/lang/String;)V
    .locals 3
    .param p1, "clubName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->setClubName(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v0, v1, :cond_0

    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->setLoading(Z)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->backBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 91
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateClubCheckAvailability(Ljava/lang/String;)V

    .line 96
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->inputNameDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 97
    return-void

    .line 93
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->update()V

    goto :goto_0
.end method

.method private setLoading(Z)V
    .locals 2
    .param p1, "loading"    # Z

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->progressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 102
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    const v1, 0x7f030075

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "page":Landroid/view/View;
    const v1, 0x7f0e0310

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->progressBar:Landroid/widget/ProgressBar;

    .line 46
    const v1, 0x7f0e030f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    .line 48
    const v1, 0x7f0e0307

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->backBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->backBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const v1, 0x7f0e030d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getClubName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->inputNameDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    .line 58
    const v1, 0x7f0e030e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nameTextView:Landroid/widget/TextView;

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nameTextView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-object v0
.end method

.method public update()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->isClubNameValid()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->backBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nameTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getClubName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->setLoading(Z)V

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->isClubNameValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    const v2, 0x7f070e94

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->nextBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    const v2, 0x7f070f79

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->clubCreateCheck:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0142

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getNameErrorReason()I

    move-result v0

    .line 78
    .local v0, "errorReason":I
    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method
