.class Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTitleFollowingStateRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/Set",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 271
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;->buildData()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getTitleFollowingState(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 289
    const-wide/16 v0, 0x247c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/Set<Ljava/lang/Long;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 285
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 280
    return-void
.end method
