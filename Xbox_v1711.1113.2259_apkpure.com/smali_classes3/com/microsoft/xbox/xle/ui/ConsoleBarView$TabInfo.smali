.class Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
.super Ljava/lang/Object;
.source "ConsoleBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabInfo"
.end annotation


# instance fields
.field private contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field private location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field private tab:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

.field private tabSpecHolder:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

.field private view:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;)V
    .locals 0
    .param p2, "view"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .param p3, "tabSpecHolder"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;
    .param p4, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p5, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p6, "contentType"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 653
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->view:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    .line 654
    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->tabSpecHolder:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    .line 655
    iput-object p4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->tab:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    .line 656
    iput-object p5, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 657
    iput-object p6, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 658
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 645
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->view:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->tab:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->tabSpecHolder:Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabSpecHolder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->contentType:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-object v0
.end method
