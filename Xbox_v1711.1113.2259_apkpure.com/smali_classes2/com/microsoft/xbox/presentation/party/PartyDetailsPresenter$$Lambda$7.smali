.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;

.field private final arg$3:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$3:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;-><init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$7;->arg$3:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    check-cast p1, Lio/reactivex/Observable;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->lambda$null$11(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
