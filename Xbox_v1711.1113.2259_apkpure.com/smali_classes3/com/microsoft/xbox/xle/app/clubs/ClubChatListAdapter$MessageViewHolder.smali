.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;
.source "ClubChatListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MessageViewHolder"
.end annotation


# instance fields
.field private final gamerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gamerTagContainer:Landroid/view/View;

.field private final image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final onMenuItemClickListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

.field private final text:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

.field private final timeStamp:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .line 138
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02ed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerTagContainer:Landroid/view/View;

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->timeStamp:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 149
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->onMenuItemClickListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 189
    return-void
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;
    .param p1, "personSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->getXuidLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->navigateToUserProfile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$bindTo$2(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Landroid/view/View;)Z
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0e0c26

    .line 244
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 245
    .local v0, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0f0004

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c25

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 249
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->canDeleteMessage()Z

    move-result v1

    if-nez v1, :cond_1

    .line 253
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0c23

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 256
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->getUserIsMemberOf()Z

    move-result v1

    if-nez v1, :cond_2

    .line 257
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 260
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->onMenuItemClickListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 262
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 264
    const/4 v1, 0x1

    return v1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;Landroid/view/MenuItem;)Z
    .locals 12
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->getAdapterPosition()I

    move-result v3

    .line 151
    .local v3, "position":I
    if-ltz v3, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataCount()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 152
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 153
    .local v0, "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 185
    .end local v0    # "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :cond_0
    :goto_0
    return v11

    .line 155
    .restart local v0    # "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :pswitch_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatReport(J)V

    .line 156
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v0, v10, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->navigateToEnforcement(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;ZLjava/lang/Long;)V

    goto :goto_0

    .line 160
    :pswitch_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatReport(J)V

    .line 161
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v0, v11, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->navigateToEnforcement(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;ZLjava/lang/Long;)V

    goto :goto_0

    .line 165
    :pswitch_2
    iget-wide v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    .line 166
    .local v4, "messageId":J
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->remove(I)V

    .line 167
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->notifyItemRemoved(I)V

    .line 168
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataCount()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 169
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->notifyItemChanged(I)V

    .line 172
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    .line 173
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatDeleteMessage(JJ)V

    goto :goto_0

    .line 177
    .end local v4    # "messageId":J
    :pswitch_3
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v7, "clipboard"

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ClipboardManager;

    .line 178
    .local v2, "clipboardManager":Landroid/content/ClipboardManager;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 179
    .local v1, "clipData":Landroid/content/ClipData;
    invoke-virtual {v2, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 180
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v7, 0x7f070c9e

    invoke-static {v6, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c23
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 13
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 192
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 194
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v8, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 196
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v7

    iget-object v8, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getPersonSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    .line 197
    .local v4, "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v4, :cond_0

    .line 198
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    const v9, 0x7f020125

    const v10, 0x7f020125

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 199
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p0, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 207
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->timeStamp:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v8, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 209
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->getAdapterPosition()I

    move-result v5

    .line 210
    .local v5, "position":I
    if-nez v5, :cond_1

    const/4 v1, 0x1

    .line 211
    .local v1, "isFirst":Z
    :goto_1
    if-eqz v1, :cond_2

    const/4 v6, 0x0

    .line 212
    .local v6, "previousMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :goto_2
    if-eqz v6, :cond_3

    iget-object v7, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    iget-object v8, v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v3, 0x1

    .line 213
    .local v3, "isXuidSameWithPrevious":Z
    :goto_3
    if-eqz v6, :cond_4

    iget-object v7, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    iget-object v7, v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$200()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-gez v7, :cond_4

    const/4 v2, 0x1

    .line 215
    .local v2, "isWithinTimeRange":Z
    :goto_4
    if-nez v1, :cond_5

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    .line 216
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 217
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerTagContainer:Landroid/view/View;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 218
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->timeStamp:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 225
    :goto_5
    const-string v0, ""

    .line 226
    .local v0, "displayedMessage":Ljava/lang/CharSequence;
    sget-object v7, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    iget-object v8, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 235
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cannot bind message to MessageViewHolder "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 238
    :goto_6
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    iget-boolean v7, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->containsLinks:Z

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    :goto_7
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 239
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 240
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->text:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 241
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s,%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v0, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 243
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Landroid/view/View$OnLongClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 266
    return-void

    .line 202
    .end local v0    # "displayedMessage":Ljava/lang/CharSequence;
    .end local v1    # "isFirst":Z
    .end local v2    # "isWithinTimeRange":Z
    .end local v3    # "isXuidSameWithPrevious":Z
    .end local v5    # "position":I
    .end local v6    # "previousMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    move-result-object v7

    iget-object v8, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;->lazyQueryPersonSummary(Ljava/lang/String;)V

    .line 203
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const v8, 0x7f020125

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageResource(I)V

    .line 204
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 210
    .restart local v5    # "position":I
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 211
    .restart local v1    # "isFirst":Z
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    add-int/lit8 v8, v5, -0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    move-object v6, v7

    goto/16 :goto_2

    .line 212
    .restart local v6    # "previousMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 213
    .restart local v3    # "isXuidSameWithPrevious":Z
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 220
    .restart local v2    # "isWithinTimeRange":Z
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 221
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->gamerTagContainer:Landroid/view/View;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 222
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;->timeStamp:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_5

    .line 228
    .restart local v0    # "displayedMessage":Ljava/lang/CharSequence;
    :pswitch_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    .line 229
    goto :goto_6

    .line 232
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->getParsedMessage()Ljava/lang/CharSequence;

    move-result-object v0

    .line 233
    goto/16 :goto_6

    .line 238
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
