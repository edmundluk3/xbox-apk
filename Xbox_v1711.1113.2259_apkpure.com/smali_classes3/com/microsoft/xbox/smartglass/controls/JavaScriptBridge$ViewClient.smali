.class Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;
.super Landroid/webkit/WebViewClient;
.source "JavaScriptBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewClient"
.end annotation


# static fields
.field private static final OnLoadScript:Ljava/lang/String; = "javascript:setTimeout(function() { if (typeof(SmartGlass) !== \'undefined\') { smartglass.onLoad(); } else { setTimeout(function () { smartglass.onLoad(); }, 200); } }, 0)"


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->access$600(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)Landroid/webkit/WebView;

    move-result-object v1

    const-string v2, "javascript:setTimeout(function() { if (typeof(SmartGlass) !== \'undefined\') { smartglass.onLoad(); } else { setTimeout(function () { smartglass.onLoad(); }, 200); } }, 0)"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;-><init>(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 143
    .local v0, "loadScript":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;->this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->access$700(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->run(Lcom/microsoft/xbox/smartglass/controls/RunnableScript;)V

    .line 144
    return-void
.end method
