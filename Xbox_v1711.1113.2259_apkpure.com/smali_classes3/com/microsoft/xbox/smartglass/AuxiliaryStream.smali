.class public Lcom/microsoft/xbox/smartglass/AuxiliaryStream;
.super Ljava/lang/Object;
.source "AuxiliaryStream.java"


# instance fields
.field private _listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(JI)V
    .locals 5
    .param p1, "pSessionManagerProxy"    # J
    .param p3, "titleId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->initialize(JI)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 17
    return-void
.end method

.method private native close(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native initialize(JI)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private onConnect()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;->onConnect()V

    .line 54
    return-void
.end method

.method private onError(II)V
    .locals 2
    .param p1, "independentError"    # I
    .param p2, "platformError"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    new-instance v1, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;->onError(Lcom/microsoft/xbox/smartglass/SGResult;)V

    .line 66
    return-void
.end method

.method private onReceive(JJ)V
    .locals 3
    .param p1, "receivedBytes"    # J
    .param p3, "availableBytes"    # J

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    new-instance v1, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;-><init>(JJ)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;->onReceive(Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;)V

    .line 58
    return-void
.end method

.method private onSend(JJ)V
    .locals 3
    .param p1, "sentBytes"    # J
    .param p3, "queuedBytes"    # J

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    new-instance v1, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;-><init>(JJ)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;->onSend(Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;)V

    .line 62
    return-void
.end method

.method private native open(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native read(JJ)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native write(J[B)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->close(J)V

    .line 31
    return-void
.end method

.method public open(Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    .prologue
    .line 21
    if-nez p1, :cond_0

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "AuxiliaryStreamListener is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_listener:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;

    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->open(J)V

    .line 26
    return-void
.end method

.method public read(J)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;
    .locals 3
    .param p1, "dataSize"    # J

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->read(JJ)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;

    move-result-object v0

    return-object v0
.end method

.method public write([B)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;->write(J[B)Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;

    move-result-object v0

    return-object v0
.end method
