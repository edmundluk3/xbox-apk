.class public Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConversationsSelectorScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustBottomMargin(I)V
    .locals 0
    .param p1, "bottomMargin"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;->forceAdjustBottomMargin(I)V

    .line 30
    return-void
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "Conversations Selector"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;->onCreateContentView()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 13
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 17
    const v0, 0x7f0300c5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;->setContentView(I)V

    .line 18
    return-void
.end method
