.class public final enum Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
.super Ljava/lang/Enum;
.source "VirtualGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScrollState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field public static final enum DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field public static final enum FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field public static final enum IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

.field public static final enum JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    const-string v1, "DRAG"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    const-string v1, "FLING"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    const-string v1, "JUMP"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 83
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->$VALUES:[Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->$VALUES:[Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    return-object v0
.end method


# virtual methods
.method public isFast()Z
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrolling()Z
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
