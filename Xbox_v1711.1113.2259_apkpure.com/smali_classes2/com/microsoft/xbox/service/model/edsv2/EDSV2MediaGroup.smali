.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaGroup;
.super Ljava/lang/Object;
.source "EDSV2MediaGroup.java"


# static fields
.field public static final MEDIAGROUP_APP:I = 0x2

.field public static final MEDIAGROUP_APP_STRING:Ljava/lang/String; = "AppType"

.field public static final MEDIAGROUP_ENHANCEDCONTENTTYPE:I = 0x8

.field public static final MEDIAGROUP_ENHANCEDCONTENTTYPE_STRING:Ljava/lang/String; = "EnhancedContentType"

.field public static final MEDIAGROUP_GAME:I = 0x1

.field public static final MEDIAGROUP_GAME_STRING:Ljava/lang/String; = "GameType"

.field public static final MEDIAGROUP_MOVIE:I = 0x3

.field public static final MEDIAGROUP_MOVIE_STRING:Ljava/lang/String; = "MovieType"

.field public static final MEDIAGROUP_MUSIC:I = 0x5

.field public static final MEDIAGROUP_MUSICALBUM_STRING:Ljava/lang/String; = "MusicAlbumType"

.field public static final MEDIAGROUP_MUSICARTIST:I = 0x6

.field public static final MEDIAGROUP_MUSICARTIST_STRING:Ljava/lang/String; = "MusicArtistType"

.field public static final MEDIAGROUP_MUSICTRACK_STRING:Ljava/lang/String; = "MusicTrackType"

.field public static final MEDIAGROUP_MUSIC_STRING:Ljava/lang/String; = "MusicType"

.field public static final MEDIAGROUP_TV:I = 0x4

.field public static final MEDIAGROUP_TV_STRING:Ljava/lang/String; = "TVType"

.field public static final MEDIAGROUP_UNKNOWN:I = 0x0

.field public static final MEDIAGROUP_UNKNOWN_STRING:Ljava/lang/String; = ""

.field public static final MEDIAGROUP_WEBVIDEO:I = 0x7

.field public static final MEDIAGROUP_WEBVIDEO_STRING:Ljava/lang/String; = "WebVideoType"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGroupStringName(I)Ljava/lang/String;
    .locals 1
    .param p0, "mediaGroup"    # I

    .prologue
    .line 28
    packed-switch p0, :pswitch_data_0

    .line 50
    const-string v0, ""

    :goto_0
    return-object v0

    .line 30
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 32
    :pswitch_1
    const-string v0, "GameType"

    goto :goto_0

    .line 34
    :pswitch_2
    const-string v0, "AppType"

    goto :goto_0

    .line 36
    :pswitch_3
    const-string v0, "MovieType"

    goto :goto_0

    .line 38
    :pswitch_4
    const-string v0, "TVType"

    goto :goto_0

    .line 40
    :pswitch_5
    const-string v0, "MusicType"

    goto :goto_0

    .line 42
    :pswitch_6
    const-string v0, "MusicArtistType"

    goto :goto_0

    .line 44
    :pswitch_7
    const-string v0, "WebVideoType"

    goto :goto_0

    .line 46
    :pswitch_8
    const-string v0, "EnhancedContentType"

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getMediaGroupFromMediaType(I)I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    .line 54
    sparse-switch p0, :sswitch_data_0

    .line 111
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 58
    :sswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 88
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 92
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 99
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 105
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 108
    :sswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x24 -> :sswitch_1
        0x25 -> :sswitch_1
        0x2e -> :sswitch_1
        0x2f -> :sswitch_1
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1
        0x3c -> :sswitch_1
        0x3d -> :sswitch_0
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x41 -> :sswitch_1
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_5
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch
.end method
