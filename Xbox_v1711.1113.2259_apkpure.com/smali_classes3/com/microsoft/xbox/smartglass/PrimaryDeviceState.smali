.class public Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;
.super Ljava/lang/Object;
.source "PrimaryDeviceState.java"


# instance fields
.field public final buildNumber:I

.field public final locale:Ljava/lang/String;

.field public final majorVersion:I

.field public final minorVersion:I

.field public final tvProvider:I


# direct methods
.method constructor <init>(IIILjava/lang/String;I)V
    .locals 0
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I
    .param p3, "buildNumber"    # I
    .param p4, "locale"    # Ljava/lang/String;
    .param p5, "tvProvider"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->majorVersion:I

    .line 22
    iput p2, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->minorVersion:I

    .line 23
    iput p3, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->buildNumber:I

    .line 24
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->locale:Ljava/lang/String;

    .line 25
    iput p5, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->tvProvider:I

    .line 26
    return-void
.end method
