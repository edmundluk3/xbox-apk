.class final synthetic Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private final arg$2:Ljava/lang/String;

.field private final arg$3:Ljava/lang/String;

.field private final arg$4:Ljava/util/List;

.field private final arg$5:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$2:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$3:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$4:Ljava/util/List;

    iput-object p5, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$5:Ljava/lang/String;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/functions/Function;
    .locals 6

    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$2:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$3:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$4:Ljava/util/List;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->arg$5:Ljava/lang/String;

    move-object v5, p1

    check-cast v5, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->lambda$createLfgSession$0(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)Lio/reactivex/SingleSource;

    move-result-object v0

    return-object v0
.end method
