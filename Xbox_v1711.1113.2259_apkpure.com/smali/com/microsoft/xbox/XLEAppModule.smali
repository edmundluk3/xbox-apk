.class public Lcom/microsoft/xbox/XLEAppModule;
.super Ljava/lang/Object;
.source "XLEAppModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;,
        Lcom/microsoft/xbox/toolkit/ToolkitModule;,
        Lcom/microsoft/xbox/domain/ModelManagerModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/XLEAppModule$Names;
    }
.end annotation


# instance fields
.field private final app:Landroid/app/Application;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 0
    .param p1, "app"    # Landroid/app/Application;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/XLEAppModule;->app:Landroid/app/Application;

    .line 46
    return-void
.end method


# virtual methods
.method projectSpecificDialogManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 75
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    return-object v0
.end method

.method provideApplicationContext()Landroid/content/Context;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "app_context"
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/XLEAppModule;->app:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method provideMyProfileProvider()Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lcom/microsoft/xbox/toolkit/MyProfileProviderImpl;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/MyProfileProviderImpl;-><init>()V

    return-object v0
.end method

.method provideMyXuidProvider()Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lcom/microsoft/xbox/toolkit/MyXuidProviderImpl;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/MyXuidProviderImpl;-><init>()V

    return-object v0
.end method

.method provideNavigationManager()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 69
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    return-object v0
.end method

.method provideNotificationDisplay()Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/XLEAppModule;->app:Landroid/app/Application;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v0

    return-object v0
.end method

.method provideReactInstanceManager()Lcom/facebook/react/ReactInstanceManager;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 105
    invoke-static {}, Lcom/facebook/react/ReactInstanceManager;->builder()Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/XLEAppModule;->app:Landroid/app/Application;

    .line 106
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setApplication(Landroid/app/Application;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    const-string v1, "index.android.bundle"

    .line 107
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setBundleAssetName(Ljava/lang/String;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    const-string v1, "index.android"

    .line 108
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setJSMainModuleName(Ljava/lang/String;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    new-instance v1, Lcom/facebook/react/shell/MainReactPackage;

    invoke-direct {v1}, Lcom/facebook/react/shell/MainReactPackage;-><init>()V

    .line 109
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->addPackage(Lcom/facebook/react/ReactPackage;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/toolkit/rn/XLEReactPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/rn/XLEReactPackage;-><init>()V

    .line 110
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->addPackage(Lcom/facebook/react/ReactPackage;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    new-instance v1, Lcom/rt2zz/reactnativecontacts/ReactNativeContacts;

    invoke-direct {v1}, Lcom/rt2zz/reactnativecontacts/ReactNativeContacts;-><init>()V

    .line 111
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->addPackage(Lcom/facebook/react/ReactPackage;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 112
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setUseDeveloperSupport(Z)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/react/common/LifecycleState;->RESUMED:Lcom/facebook/react/common/LifecycleState;

    .line 113
    invoke-virtual {v0, v1}, Lcom/facebook/react/ReactInstanceManagerBuilder;->setInitialLifecycleState(Lcom/facebook/react/common/LifecycleState;)Lcom/facebook/react/ReactInstanceManagerBuilder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/facebook/react/ReactInstanceManagerBuilder;->build()Lcom/facebook/react/ReactInstanceManager;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method provideSchedulerProvider()Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLESchedulerProvider;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/XLESchedulerProvider;-><init>()V

    return-object v0
.end method

.method provideSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/XLEAppModule;->app:Landroid/app/Application;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method provideTitleBarView()Lcom/microsoft/xbox/xle/ui/TitleBarView;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 63
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getTitleBarView()Lcom/microsoft/xbox/xle/ui/TitleBarView;

    move-result-object v0

    return-object v0
.end method
