.class public Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;
.super Landroid/view/ViewGroup;
.source "VerticalPanScrollContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;,
        Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;,
        Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    }
.end annotation


# instance fields
.field private body:Landroid/widget/AbsListView;

.field private headerSize:I

.field private lastVisible:Landroid/view/View;

.field private final locBody:[I

.field private final locLastVisible:[I

.field private pointerId:I

.field private ptDown:Landroid/graphics/PointF;

.field private ptPrev:Landroid/graphics/PointF;

.field private resIdLastVisible:I

.field private translation:F

.field private vc:Landroid/view/ViewConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x2

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resIdLastVisible:I

    .line 36
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locBody:[I

    .line 37
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locLastVisible:[I

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x2

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resIdLastVisible:I

    .line 36
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locBody:[I

    .line 37
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locLastVisible:[I

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method private cancelAndDown(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 324
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 326
    .local v0, "eCancel":Landroid/view/MotionEvent;
    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 327
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 328
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 330
    .local v1, "eDown":Landroid/view/MotionEvent;
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 331
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    :try_start_2
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 338
    return-void

    .line 333
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 336
    .end local v1    # "eDown":Landroid/view/MotionEvent;
    :catchall_1
    move-exception v2

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    throw v2
.end method

.method private computeNewState(FF)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    .locals 4
    .param p1, "yCur"    # F
    .param p2, "yPrev"    # F

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getTouchTarget(F)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    if-ne v2, v3, :cond_0

    .line 224
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    .line 233
    .local v0, "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    :goto_0
    return-object v0

    .line 226
    .end local v0    # "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    :cond_0
    cmpg-float v2, p1, p2

    if-gez v2, :cond_1

    const/4 v1, 0x1

    .line 227
    .local v1, "scrollingUp":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 228
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getMovableHeaderSize()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    .restart local v0    # "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    :goto_2
    goto :goto_0

    .line 226
    .end local v0    # "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    .end local v1    # "scrollingUp":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 228
    .restart local v1    # "scrollingUp":Z
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    goto :goto_2

    .line 230
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->isBodyTopEdgeVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    .restart local v0    # "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    :goto_3
    goto :goto_0

    .end local v0    # "ret":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    :cond_4
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    goto :goto_3
.end method

.method private static distance(Landroid/graphics/PointF;Landroid/graphics/PointF;)J
    .locals 4
    .param p0, "ptCurr"    # Landroid/graphics/PointF;
    .param p1, "ptPrev"    # Landroid/graphics/PointF;

    .prologue
    .line 282
    iget v2, p0, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->x:F

    sub-float v0, v2, v3

    .local v0, "dx":F
    iget v2, p0, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    sub-float v1, v2, v3

    .line 283
    .local v1, "dy":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    return-wide v2
.end method

.method private getMovableHeaderSize()I
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 183
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->headerSize:I

    .line 184
    .local v4, "size":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->lastVisible:Landroid/view/View;

    if-eqz v5, :cond_1

    .line 185
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->lastVisible:Landroid/view/View;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locLastVisible:[I

    invoke-virtual {v5, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 186
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locBody:[I

    invoke-virtual {v5, v6}, Landroid/widget/AbsListView;->getLocationOnScreen([I)V

    .line 188
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locLastVisible:[I

    aget v1, v5, v7

    .line 189
    .local v1, "lastVisibleY":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->lastVisible:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 190
    .local v3, "lpLastVisible":Landroid/view/ViewGroup$LayoutParams;
    instance-of v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v5, :cond_0

    .line 191
    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local v3    # "lpLastVisible":Landroid/view/ViewGroup$LayoutParams;
    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v1, v5

    .line 194
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->locBody:[I

    aget v0, v5, v7

    .line 195
    .local v0, "bodyY":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v5}, Landroid/widget/AbsListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 196
    .local v2, "lpBody":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    iget v5, v2, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    sub-int/2addr v0, v5

    .line 198
    sub-int v5, v0, v1

    sub-int/2addr v4, v5

    .line 200
    .end local v0    # "bodyY":I
    .end local v1    # "lastVisibleY":I
    .end local v2    # "lpBody":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    :cond_1
    return v4
.end method

.method private getTouchTarget(F)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;
    .locals 3
    .param p1, "y"    # F

    .prologue
    .line 204
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getTop()I

    move-result v1

    .line 205
    .local v1, "top":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 206
    .local v0, "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    iget v2, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    sub-int/2addr v1, v2

    .line 207
    int-to-float v2, v1

    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->BODY:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->VerticalPanScrollContainer:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 74
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resIdLastVisible:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    return-void

    .line 76
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private isBodyTopEdgeVisible()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 211
    const/4 v1, 0x0

    .line 212
    .local v1, "ret":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 213
    const/4 v1, 0x1

    .line 218
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    if-nez v3, :cond_0

    .line 215
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v3, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 216
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    if-nez v3, :cond_2

    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private makeMove(Landroid/graphics/PointF;)V
    .locals 6
    .param p1, "ptCurr"    # Landroid/graphics/PointF;

    .prologue
    .line 313
    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v1, v2, v3

    .line 314
    .local v1, "offset":F
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    .line 315
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    sub-float/2addr v2, v1

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getMovableHeaderSize()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 316
    .local v0, "newTranslation":F
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    .line 317
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    .line 318
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getBottom()I

    move-result v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->relayout(IIII)V

    .line 320
    :cond_0
    return-void
.end method

.method private relaxMeasureSpec(I)I
    .locals 2
    .param p1, "spec"    # I

    .prologue
    .line 178
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 179
    .local v0, "sz":I
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    return v1
.end method

.method private relayout(IIII)V
    .locals 14
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 155
    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 156
    .local v1, "dy":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildCount()I

    move-result v9

    add-int/lit8 v3, v9, -0x1

    .line 157
    .local v3, "headerCount":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingLeft()I

    move-result v9

    add-int v5, p1, v9

    .line 158
    .local v5, "left":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingRight()I

    move-result v9

    sub-int v7, p3, v9

    .line 159
    .local v7, "right":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingBottom()I

    move-result v9

    sub-int v0, p4, v9

    .line 160
    .local v0, "bottom":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingTop()I

    move-result v9

    add-int v8, p2, v9

    .line 161
    .local v8, "top":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 162
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 163
    .local v2, "header":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_0

    .line 164
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 165
    .local v6, "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    iget v9, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    add-int/2addr v8, v9

    .line 166
    iget v9, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    add-int/2addr v9, v5

    sub-int v10, v8, v1

    iget v11, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    sub-int v11, v7, v11

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v8

    sub-int/2addr v12, v1

    invoke-virtual {v2, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 167
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    iget v10, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    add-int/2addr v9, v10

    add-int/2addr v8, v9

    .line 161
    .end local v6    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 171
    .end local v2    # "header":Landroid/view/View;
    :cond_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_2

    .line 172
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 173
    .restart local v6    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    iget v10, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    add-int/2addr v10, v5

    iget v11, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    add-int/2addr v11, v8

    sub-int/2addr v11, v1

    iget v12, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    sub-int v12, v7, v12

    iget v13, v6, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    sub-int v13, v0, v13

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/AbsListView;->layout(IIII)V

    .line 175
    .end local v6    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    :cond_2
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 342
    instance-of v0, p1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 347
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 357
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    check-cast p1, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .end local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    :goto_0
    return-object v0

    .restart local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getBody()Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 55
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 56
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resIdLastVisible:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 57
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resIdLastVisible:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->lastVisible:Landroid/view/View;

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 60
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " requires at least one child of AbsListView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 63
    .local v0, "lastView":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/AbsListView;

    if-nez v1, :cond_2

    .line 64
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last child of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be AbsListView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_2
    check-cast v0, Landroid/widget/AbsListView;

    .end local v0    # "lastView":Landroid/view/View;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    .line 67
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->vc:Landroid/view/ViewConfiguration;

    .line 69
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    .line 246
    const/4 v3, 0x0

    .line 247
    .local v3, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 278
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 249
    :pswitch_1
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->pointerId:I

    .line 250
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptDown:Landroid/graphics/PointF;

    goto :goto_0

    .line 253
    :pswitch_2
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->pointerId:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 254
    .local v0, "actionIndex":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 255
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 256
    .local v2, "ptCurr":Landroid/graphics/PointF;
    iget v4, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getTouchTarget(F)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    if-ne v4, v5, :cond_1

    .line 257
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->makeMove(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 259
    :cond_1
    iget v4, v2, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->computeNewState(FF)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    move-result-object v1

    .line 260
    .local v1, "newState":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    sget-object v4, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->PANNING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    if-ne v1, v4, :cond_3

    .line 261
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptDown:Landroid/graphics/PointF;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->distance(Landroid/graphics/PointF;Landroid/graphics/PointF;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->vc:Landroid/view/ViewConfiguration;

    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v6

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 262
    const/4 v3, 0x1

    goto :goto_0

    .line 264
    :cond_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->makeMove(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 267
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getTouchTarget(F)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;->HEADER:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$TouchTarget;

    if-ne v4, v5, :cond_4

    .line 268
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->cancelAndDown(Landroid/view/MotionEvent;)V

    .line 270
    :cond_4
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 132
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->relayout(IIII)V

    .line 133
    return-void
.end method

.method protected onMeasure(II)V
    .locals 21
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 86
    const/16 v18, 0x0

    .line 87
    .local v18, "width":I
    const/4 v12, 0x0

    .line 88
    .local v12, "height":I
    const/4 v6, 0x0

    .line 90
    .local v6, "childState":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildCount()I

    move-result v19

    add-int/lit8 v9, v19, -0x1

    .line 91
    .local v9, "headerCount":I
    if-lez v9, :cond_1

    .line 92
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->relaxMeasureSpec(I)I

    move-result v17

    .line 93
    .local v17, "relaxedHeightMeasureSpec":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    if-ge v14, v9, :cond_1

    .line 94
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 95
    .local v8, "header":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 96
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 97
    .local v15, "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingLeft()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingRight()I

    move-result v20

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->width:I

    move/from16 v20, v0

    move/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildMeasureSpec(III)I

    move-result v11

    .line 98
    .local v11, "headerWSpec":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingTop()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingBottom()I

    move-result v20

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->height:I

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildMeasureSpec(III)I

    move-result v10

    .line 99
    .local v10, "headerHSpec":I
    invoke-virtual {v8, v11, v10}, Landroid/view/View;->measure(II)V

    .line 100
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v19

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 101
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    add-int v12, v12, v19

    .line 102
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredState()I

    move-result v19

    move/from16 v0, v19

    invoke-static {v6, v0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->combineMeasuredStates(II)I

    move-result v6

    .line 93
    .end local v10    # "headerHSpec":I
    .end local v11    # "headerWSpec":I
    .end local v15    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    :cond_0
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 107
    .end local v8    # "header":Landroid/view/View;
    .end local v14    # "i":I
    .end local v17    # "relaxedHeightMeasureSpec":I
    :cond_1
    move-object/from16 v0, p0

    iput v12, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->headerSize:I

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    .line 110
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 111
    .local v7, "h":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    .line 112
    .local v16, "mh":I
    sub-int v19, v7, v12

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    move/from16 v20, v0

    add-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 113
    .local v3, "bodyH":I
    move/from16 v0, v16

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 115
    .local v13, "heightBodyMeasureSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;

    .line 116
    .restart local v15    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingLeft()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingRight()I

    move-result v20

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->width:I

    move/from16 v20, v0

    move/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildMeasureSpec(III)I

    move-result v5

    .line 117
    .local v5, "bodyWSpec":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingTop()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingBottom()I

    move-result v20

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->height:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v13, v0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getChildMeasureSpec(III)I

    move-result v4

    .line 118
    .local v4, "bodyHSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v4}, Landroid/widget/AbsListView;->measure(II)V

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getMeasuredWidth()I

    move-result v19

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->leftMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->rightMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getMeasuredHeight()I

    move-result v19

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    iget v0, v15, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    add-int v12, v12, v19

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->body:Landroid/widget/AbsListView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getMeasuredState()I

    move-result v19

    move/from16 v0, v19

    invoke-static {v6, v0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->combineMeasuredStates(II)I

    move-result v6

    .line 124
    .end local v3    # "bodyH":I
    .end local v4    # "bodyHSpec":I
    .end local v5    # "bodyWSpec":I
    .end local v7    # "h":I
    .end local v13    # "heightBodyMeasureSpec":I
    .end local v15    # "lp":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$LayoutParams;
    .end local v16    # "mh":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingLeft()I

    move-result v19

    add-int v19, v19, v18

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingRight()I

    move-result v20

    add-int v19, v19, v20

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getSuggestedMinimumWidth()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingTop()I

    move-result v19

    add-int v19, v19, v12

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getPaddingBottom()I

    move-result v20

    add-int v19, v19, v20

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getSuggestedMinimumHeight()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 127
    move/from16 v0, v18

    move/from16 v1, p1

    invoke-static {v0, v1, v6}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resolveSizeAndState(III)I

    move-result v19

    shl-int/lit8 v20, v6, 0x10

    move/from16 v0, p2

    move/from16 v1, v20

    invoke-static {v12, v0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->resolveSizeAndState(III)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->setMeasuredDimension(II)V

    .line 128
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 145
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 146
    check-cast v0, Landroid/os/Bundle;

    .line 147
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "parentState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 148
    const-string v1, "translation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    .line 152
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 138
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    const-string v1, "translation"

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->translation:F

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 140
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 289
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 306
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 309
    .local v3, "ret":Z
    :goto_0
    return v3

    .line 291
    .end local v3    # "ret":Z
    :pswitch_1
    const/4 v3, 0x1

    .line 292
    .restart local v3    # "ret":Z
    goto :goto_0

    .line 294
    .end local v3    # "ret":Z
    :pswitch_2
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->pointerId:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 295
    .local v0, "actionIndex":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 296
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 297
    .local v2, "ptCurr":Landroid/graphics/PointF;
    iget v4, v2, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->ptPrev:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->computeNewState(FF)Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    move-result-object v1

    .line 298
    .local v1, "newState":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->makeMove(Landroid/graphics/PointF;)V

    .line 299
    sget-object v4, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;->SCROLLING:Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;

    if-ne v1, v4, :cond_0

    .line 300
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->cancelAndDown(Landroid/view/MotionEvent;)V

    .line 303
    .end local v1    # "newState":Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer$State;
    .end local v2    # "ptCurr":Landroid/graphics/PointF;
    :cond_0
    const/4 v3, 0x1

    .line 304
    .restart local v3    # "ret":Z
    goto :goto_0

    .line 289
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalPanScrollContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 242
    return-void
.end method
