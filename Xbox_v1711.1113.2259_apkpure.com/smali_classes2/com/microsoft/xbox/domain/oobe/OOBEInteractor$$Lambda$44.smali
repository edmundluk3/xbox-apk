.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

.field private final arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;->arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;-><init>(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;->arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$44;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    check-cast p1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$3(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)Lio/reactivex/SingleSource;

    move-result-object v0

    return-object v0
.end method
