.class public Lcom/microsoft/xbox/toolkit/ui/XLEListView;
.super Landroid/widget/ListView;
.source "XLEListView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;


# static fields
.field private static final LAYOUT_BLOCK_TIMEOUT_MS:I = 0x1f4

.field private static final LIST_VIEW_ANIMATION_NAME:Ljava/lang/String; = "ListView"

.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x7530

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private blocking:Z

.field private desiredLayoutAnimation:Landroid/view/animation/LayoutAnimationController;

.field private firstTouchAfterNonBlocking:Z

.field private listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private needLayoutAnimation:Z

.field private pendingNotifyDataSetChanged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    .line 28
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->firstTouchAfterNonBlocking:Z

    .line 29
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->pendingNotifyDataSetChanged:Z

    .line 30
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->needLayoutAnimation:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->desiredLayoutAnimation:Landroid/view/animation/LayoutAnimationController;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 52
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setSoundEffectsEnabled(Z)V

    .line 53
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 54
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setSelector(I)V

    .line 55
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/OverScrollUtil;->removeOverScrollFooter(Landroid/view/View;)V

    .line 56
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onLayoutAnimationStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onLayoutAnimationEnd()V

    return-void
.end method

.method private onLayoutAnimationEnd()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 180
    const-string v0, "XLEListView"

    const-string v1, "LayoutAnimation end"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setBlocking(Z)V

    .line 182
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 183
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->pendingNotifyDataSetChanged:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->notifyDataSetChanged()V

    .line 185
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->pendingNotifyDataSetChanged:Z

    .line 187
    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 188
    return-void
.end method

.method private onLayoutAnimationStart()V
    .locals 2

    .prologue
    .line 174
    const-string v0, "XLEListView"

    const-string v1, "LayoutAnimation start"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 176
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setBlocking(Z)V

    .line 177
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 216
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 225
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAnimationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    const-string v0, "ListView"

    return-object v0
.end method

.method public getBlocking()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    return v0
.end method

.method public getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getFirstVisiblePosition()I

    move-result v0

    .line 198
    .local v0, "index":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 199
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_0

    .line 200
    .local v1, "offset":I
    :goto_0
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    return-object v3

    .line 199
    .end local v1    # "offset":I
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 210
    return-object p0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 109
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-nez v1, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 111
    .local v0, "adapter":Landroid/widget/ListAdapter;
    instance-of v1, v0, Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_0

    .line 112
    check-cast v0, Landroid/widget/ArrayAdapter;

    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 114
    :cond_0
    const-string v1, "XLEListView"

    const-string v2, "notifyDataSetChanged"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->startLayoutAnimationIfReady()V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->pendingNotifyDataSetChanged:Z

    goto :goto_0
.end method

.method public onDataUpdated()V
    .locals 2

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 103
    const-string v0, "XLEListView"

    const-string v1, "onDataUpdate"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->needLayoutAnimation:Z

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->startLayoutAnimationIfReady()V

    .line 106
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    .line 81
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 82
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    .line 163
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->firstTouchAfterNonBlocking:Z

    if-eqz v0, :cond_2

    .line 164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 165
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 167
    :cond_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->firstTouchAfterNonBlocking:Z

    .line 169
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public restoreScrollState(II)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    .line 205
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setSelectionFromTop(II)V

    .line 206
    return-void
.end method

.method public setBlocking(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->firstTouchAfterNonBlocking:Z

    .line 87
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-eq v0, p1, :cond_0

    .line 88
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    .line 89
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->blocking:Z

    if-eqz v0, :cond_2

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListLayout:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 95
    :cond_0
    :goto_1
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListLayout:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    goto :goto_1
.end method

.method public setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V
    .locals 2
    .param p1, "controller"    # Landroid/view/animation/LayoutAnimationController;

    .prologue
    .line 142
    const-string v0, "XLEListView"

    const-string v1, "setLayoutAnimation"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 145
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->desiredLayoutAnimation:Landroid/view/animation/LayoutAnimationController;

    .line 147
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->startLayoutAnimationIfReady()V

    .line 148
    return-void
.end method

.method public startLayoutAnimationIfReady()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 122
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->needLayoutAnimation:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->desiredLayoutAnimation:Landroid/view/animation/LayoutAnimationController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    if-eqz v1, :cond_1

    .line 123
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->needLayoutAnimation:Z

    .line 124
    const-string v1, "We should try to set layout animation only once"

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getLayoutAnimation()Landroid/view/animation/LayoutAnimationController;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->desiredLayoutAnimation:Landroid/view/animation/LayoutAnimationController;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setLayoutAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 130
    :cond_1
    return-void
.end method
