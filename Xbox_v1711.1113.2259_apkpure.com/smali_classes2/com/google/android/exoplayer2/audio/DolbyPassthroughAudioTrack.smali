.class public final Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
.super Landroid/media/AudioTrack;
.source "DolbyPassthroughAudioTrack.java"


# static fields
.field private static final BUFFER_COUNT:I = 0x2

.field private static final MSG_FLUSH_TRACK:I = 0x4

.field private static final MSG_PAUSE_TRACK:I = 0x2

.field private static final MSG_PLAY_TRACK:I = 0x3

.field private static final MSG_RELEASE_TRACK:I = 0x6

.field private static final MSG_STOP_TRACK:I = 0x5

.field private static final MSG_WRITE_TO_TRACK:I = 0x1

.field private static final TRACK_HANDLER_THREAD_NAME:Ljava/lang/String; = "dolbyTrackHandlerThread"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private audioBuffer:[[B

.field private final log:Lcom/google/android/exoplayer2/util/Logger;

.field private nextBufferIndex:I

.field private pendingWriteSem:Ljava/util/concurrent/Semaphore;

.field private trackHandler:Landroid/os/Handler;

.field private trackHandlerGate:Landroid/os/ConditionVariable;

.field private trackHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 8
    .param p1, "streamType"    # I
    .param p2, "sampleRateInHz"    # I
    .param p3, "channelConfig"    # I
    .param p4, "audioFormat"    # I
    .param p5, "bufferSizeInBytes"    # I
    .param p6, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 68
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;-><init>(IIIIIII)V

    .line 70
    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .locals 4
    .param p1, "streamType"    # I
    .param p2, "sampleRateInHz"    # I
    .param p3, "channelConfig"    # I
    .param p4, "audioFormat"    # I
    .param p5, "bufferSizeInBytes"    # I
    .param p6, "mode"    # I
    .param p7, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 76
    invoke-direct/range {p0 .. p7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    .line 37
    const-class v1, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->TAG:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    .line 42
    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    .line 43
    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    .line 57
    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->pendingWriteSem:Ljava/util/concurrent/Semaphore;

    .line 58
    check-cast v0, [[B

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    .line 62
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger;

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger;-><init>(Lcom/google/android/exoplayer2/util/Logger$Module;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    .line 78
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v1, "DolbyPassthroughAudioTrack constructor"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 79
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    .line 80
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "dolbyTrackHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    .line 81
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->pendingWriteSem:Ljava/util/concurrent/Semaphore;

    .line 82
    new-array v0, v3, [[B

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    .line 84
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 88
    new-instance v0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;-><init>(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    .line 146
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)[[B
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    return-object v0
.end method

.method static synthetic access$1001(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->stop()V

    return-void
.end method

.method static synthetic access$1101(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->release()V

    return-void
.end method

.method static synthetic access$201(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;[BII)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .param p1, "x1"    # [B
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->pendingWriteSem:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$401(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->pause()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic access$601(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->play()V

    return-void
.end method

.method static synthetic access$701(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->flush()V

    return-void
.end method

.method static synthetic access$801(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->stop()V

    return-void
.end method

.method static synthetic access$901(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    .prologue
    .line 35
    invoke-super {p0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    return v0
.end method


# virtual methods
.method public flush()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 197
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "flush"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 199
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 200
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Sending flush Directtrack handler thread"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 204
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 205
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Flushing Direct Track Done"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 208
    :cond_1
    return-void
.end method

.method public pause()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "pause"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 179
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 180
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Sending pause directtrack handler thread"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 184
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 185
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 186
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Pausing Direct Track Done"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 188
    :cond_1
    return-void
.end method

.method public play()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "play"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 158
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 159
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Sending play to DirectTrack handler thread"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 163
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 164
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "DirectTrack Play done"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 167
    :cond_1
    return-void
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 281
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v3, "release"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 282
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->close()V

    .line 283
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 284
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v3, "Sending release directtrack handler thread"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 288
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->block()V

    .line 290
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    .line 291
    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerThread:Landroid/os/HandlerThread;

    .line 292
    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    .line 293
    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    .line 294
    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->pendingWriteSem:Ljava/util/concurrent/Semaphore;

    .line 295
    check-cast v1, [[B

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    .line 296
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 297
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Release track done"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 299
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 217
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "stop"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->getPlayState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 219
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "already in stopped state"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 223
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 224
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Sending stop Directtrack handler thread"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 228
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandlerGate:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    .line 229
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Stopping Direct Track Done"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public write([BII)I
    .locals 5
    .param p1, "audioData"    # [B
    .param p2, "offsetInBytes"    # I
    .param p3, "sizeInBytes"    # I

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->getPlayState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 243
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v3, "not in play state..not writing buffer now..."

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->w(Ljava/lang/String;)V

    move p3, v1

    .line 270
    .end local p3    # "sizeInBytes":I
    :goto_0
    return p3

    .line 246
    .restart local p3    # "sizeInBytes":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->pendingWriteSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v2

    if-nez v2, :cond_2

    .line 247
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v3, "pending writes... not writing buffer now"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    :cond_1
    move p3, v1

    .line 250
    goto :goto_0

    .line 252
    :cond_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    iget v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    iget v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    aget-object v2, v2, v3

    array-length v2, v2

    if-ge v2, p3, :cond_5

    .line 254
    :cond_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 255
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Allocating buffer index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    .line 258
    :cond_4
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    iget v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    new-array v4, p3, [B

    aput-object v4, v2, v3

    .line 260
    :cond_5
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->audioBuffer:[[B

    iget v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    aget-object v2, v2, v3

    invoke-static {p1, p2, v2, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 261
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 264
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 265
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v2, "Sending buffer to directtrack handler thread"

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    .line 267
    :cond_6
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->trackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 268
    iget v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->nextBufferIndex:I

    goto/16 :goto_0
.end method
