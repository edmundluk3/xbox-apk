.class Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PhoneContactFinderTicketModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MsaTicketRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;)V
    .locals 1

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->this$0:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 90
    new-instance v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable$1;-><init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;-><init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 9

    .prologue
    .line 70
    new-instance v7, Lcom/microsoft/xbox/idp/interop/LocalConfig;

    invoke-direct {v7}, Lcom/microsoft/xbox/idp/interop/LocalConfig;-><init>()V

    .line 71
    .local v7, "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    new-instance v0, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->access$100()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    const-string v4, "ssl.live.com"

    const-string v5, "mbi_ssl"

    invoke-virtual {v7}, Lcom/microsoft/xbox/idp/interop/LocalConfig;->getCid()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .local v0, "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    :try_start_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;->start()Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 75
    :catch_0
    move-exception v8

    .line 76
    .local v8, "exception":Ljava/lang/Exception;
    const-string v1, "XSAPI.Android"

    const-string v2, "exception on getting  MSA Ticket"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 87
    const-wide/16 v0, 0xbf2

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method
