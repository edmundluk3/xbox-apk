.class public Lcom/microsoft/xbox/smartglass/MediaCommand;
.super Ljava/lang/Object;
.source "MediaCommand.java"


# instance fields
.field public command:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public seekPosition:J

.field public titleId:I


# direct methods
.method public constructor <init>(IJ)V
    .locals 2
    .param p1, "titleId"    # I
    .param p2, "seekPosition"    # J

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MediaCommand;->titleId:I

    .line 33
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Seek:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/MediaCommand;->command:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 34
    iput-wide p2, p0, Lcom/microsoft/xbox/smartglass/MediaCommand;->seekPosition:J

    .line 35
    return-void
.end method

.method public constructor <init>(ILcom/microsoft/xbox/smartglass/MediaControlCommands;)V
    .locals 0
    .param p1, "titleId"    # I
    .param p2, "command"    # Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MediaCommand;->titleId:I

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/MediaCommand;->command:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 24
    return-void
.end method
