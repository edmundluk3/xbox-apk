.class public Lcom/microsoft/xbox/service/model/serialization/PushNotificationConstants;
.super Ljava/lang/Object;
.source "PushNotificationConstants.java"


# static fields
.field public static final APPID:Ljava/lang/String; = "com.microsoft.xboxone.smartglass.beta.android"

.field public static final CHANNEL_TYPE:Ljava/lang/String; = "PushNotification"

.field public static final EVENT_SERVICE_NAME:Ljava/lang/String; = "pnh"

.field public static final INTERESTED_RESOURCE_ALL:Ljava/lang/String; = "/v1/users/ME/conversations/ALL/properties"

.field public static final INTERESTED_RESOURCE_CONSUMPTION_HORIZON:Ljava/lang/String; = "/v1/users/ME/conversations/ALL/properties?view=consumptionHorizon"

.field public static final INTERESTED_RESOURCE_CONTACTS:Ljava/lang/String; = "/v1/users/ME/contacts/ALL"

.field public static final INTERESTED_RESOURCE_MESSAGES:Ljava/lang/String; = "/v1/users/ME/conversations/ALL/messages"

.field public static final INTERESTED_RESOURCE_THREADS:Ljava/lang/String; = "/v1/threads/ALL"

.field public static final LONG_POLL_TYPE:Ljava/lang/String; = "httpLongPoll"

.field public static final SKYPE_CHAT_LONG_POLL_TEMPLATE:Ljava/lang/String; = "raw"

.field public static final SKYPE_CHAT_SERVICE_TEMPLATE:Ljava/lang/String; = "AndroidSmartglassChat"

.field public static final TEMPLATE_KEY:Ljava/lang/String; = "com.microsoft.xboxone.smartglass.beta.android:1.0"

.field public static final TTL:J = 0xed4e00L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
