.class public Lcom/microsoft/xbox/xle/app/LiveTVSettings;
.super Ljava/lang/Object;
.source "LiveTVSettings.java"


# instance fields
.field public SMARTGLASSCONSOLEPOWERENABLED:I

.field public SMARTGLASSCONSOLEPOWERENABLED_BETA:I

.field public SMARTGLASSGUIDEENABLED:I

.field public SMARTGLASSGUIDEENABLED_BETA:I

.field public SMARTGLASSNETWORKTESTINGENABLED:I

.field public SMARTGLASSNETWORKTESTINGENABLED_BETA:I

.field public SMARTGLASSNONADULTCONTENTRATINGS:Ljava/lang/String;

.field public SMARTGLASSSHOWSPECIFICCONTENTRATINGS:I

.field public SMARTGLASSTREATMISSINGRATINGASADULT:I

.field public SMARTGLASSURCENABLED:I

.field public SMARTGLASSURCENABLED_BETA:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromUnknownLocale()Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LiveTVSettings;-><init>()V

    .line 23
    .local v0, "settings":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED:I

    .line 24
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED:I

    .line 25
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED_BETA:I

    .line 26
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED_BETA:I

    .line 27
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSSHOWSPECIFICCONTENTRATINGS:I

    .line 28
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSTREATMISSINGRATINGASADULT:I

    .line 29
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSNONADULTCONTENTRATINGS:Ljava/lang/String;

    .line 31
    return-object v0
.end method
