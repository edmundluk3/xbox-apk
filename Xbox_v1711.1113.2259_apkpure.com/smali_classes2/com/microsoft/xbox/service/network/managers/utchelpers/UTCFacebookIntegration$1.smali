.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;
.super Ljava/lang/Object;
.source "UTCFacebookIntegration.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackAddFacebookUserToFriends(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$isFacebookFriend:Z

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;->val$isFacebookFriend:Z

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;->val$xuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 27
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;->val$isFacebookFriend:Z

    if-nez v1, :cond_0

    .line 36
    :goto_0
    return-void

    .line 32
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 33
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "facebookxuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    const-string v1, "Facebook - Add Facebook Friend"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method
