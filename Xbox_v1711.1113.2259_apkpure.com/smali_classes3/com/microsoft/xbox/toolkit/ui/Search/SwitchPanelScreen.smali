.class public abstract Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "SwitchPanelScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected abstract getContentScreenId()I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end method

.method protected getErrorScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 78
    const v0, 0x7f030132

    return v0
.end method

.method protected getInvalidScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 108
    const v0, 0x7f030134

    return v0
.end method

.method protected getLoadingScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 98
    const v0, 0x7f030133

    return v0
.end method

.method protected getNoContentScreenId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 88
    const v0, 0x7f030134

    return v0
.end method

.method protected getRootViewId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 62
    const v0, 0x7f030223

    return v0
.end method

.method public onCreate()V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 25
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->onCreateContentView()V

    .line 27
    return-void
.end method

.method public onCreateContentView()V
    .locals 6
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getRootViewId()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->setContentView(I)V

    .line 34
    const v5, 0x7f0e0a96

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 35
    .local v4, "validStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getContentScreenId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 36
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 38
    const v5, 0x7f0e0a97

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 39
    .local v0, "errorStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getErrorScreenId()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 40
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 42
    const v5, 0x7f0e0a98

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 43
    .local v3, "noContentStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getNoContentScreenId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 44
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 46
    const v5, 0x7f0e0a99

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 47
    .local v2, "loadingStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getLoadingScreenId()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 48
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 50
    const v5, 0x7f0e0a9a

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 51
    .local v1, "invalidStub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->getInvalidScreenId()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 52
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 53
    return-void
.end method
