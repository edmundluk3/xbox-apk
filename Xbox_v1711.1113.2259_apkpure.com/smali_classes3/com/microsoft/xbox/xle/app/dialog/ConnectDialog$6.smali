.class Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;
.super Landroid/widget/BaseAdapter;
.source "ConnectDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .prologue
    .line 351
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->reportVortexAutoConnectCheck(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;)V

    return-void
.end method

.method private bindConsoleRow(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 29
    .param p1, "row"    # Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;
    .param p2, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 428
    const v3, 0x7f0e0427

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/LinearLayout;

    .line 429
    .local v27, "mainView":Landroid/widget/LinearLayout;
    const v3, 0x7f0e042a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/RelativeLayout;

    .line 430
    .local v23, "expandedView":Landroid/widget/RelativeLayout;
    const v3, 0x7f0e0428

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 431
    .local v21, "consoleStateIcon":Landroid/widget/ImageView;
    const v3, 0x7f0e042c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 432
    .local v20, "consoleSleepingIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v3, 0x7f0e042e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 433
    .local v28, "removeConsoleButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v3, 0x7f0e042d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 434
    .local v19, "consoleNameView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v3, 0x7f0e042f

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 435
    .local v5, "autoConnectCheckbox":Landroid/widget/CheckBox;
    const v3, 0x7f0e0430

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 436
    .local v6, "autoConnectLabel":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v3, 0x7f0e0438

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 437
    .local v18, "connectButton":Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
    const v3, 0x7f0e0439

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 438
    .local v22, "disconnectButton":Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
    const v3, 0x7f0e0436

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 439
    .local v8, "turnOnButton":Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
    const v3, 0x7f0e0437

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 440
    .local v14, "turnOffButton":Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
    const v3, 0x7f0e0431

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 441
    .local v9, "powerOnOffConnectView":Landroid/widget/LinearLayout;
    const v3, 0x7f0e0433

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 442
    .local v7, "powerOnOffConnectMessage":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getConsoleConnected(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v24

    .line 443
    .local v24, "isConnected":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    if-ne v3, v4, :cond_2

    const/16 v25, 0x1

    .line 444
    .local v25, "isConnecting":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getIsPoweringOnOff()Z

    move-result v26

    .line 446
    .local v26, "isPoweringOnOff":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->setConsoleState(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;)V

    .line 448
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v25, :cond_3

    :cond_0
    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 449
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v25, :cond_4

    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 451
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPowerEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    .line 453
    if-nez v26, :cond_6

    if-nez v25, :cond_6

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 455
    if-eqz v24, :cond_7

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPowerEnabled()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v14, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    .line 456
    if-nez v26, :cond_8

    if-nez v25, :cond_8

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v14, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 458
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 459
    if-eqz v26, :cond_9

    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v9, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 461
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getXboxName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 464
    if-nez v24, :cond_a

    if-nez v26, :cond_a

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-nez v3, :cond_a

    const/4 v3, 0x1

    :goto_8
    invoke-static {v5, v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setVisible(Landroid/view/View;Z)V

    .line 465
    if-nez v24, :cond_b

    if-nez v26, :cond_b

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-nez v3, :cond_b

    const/4 v3, 0x1

    :goto_9
    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setVisible(Landroid/view/View;Z)V

    .line 467
    if-nez v24, :cond_c

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-nez v3, :cond_c

    const/4 v3, 0x0

    :goto_a
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    .line 468
    if-nez v26, :cond_d

    if-nez v25, :cond_d

    const/4 v3, 0x1

    :goto_b
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 470
    if-eqz v24, :cond_e

    const/4 v3, 0x0

    :goto_c
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    .line 471
    if-nez v26, :cond_f

    if-nez v25, :cond_f

    const/4 v3, 0x1

    :goto_d
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 473
    if-nez v24, :cond_10

    const/4 v3, 0x1

    :goto_e
    invoke-virtual {v5, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 474
    if-nez v24, :cond_11

    const/4 v3, 0x1

    :goto_f
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 476
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isExpanded(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v3, 0x0

    :goto_10
    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 477
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->isExpanded(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, 0x0

    :goto_11
    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 479
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v3, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;)V

    invoke-virtual {v5, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 502
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1, v5}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 513
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1, v5}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$6;

    move-object/from16 v4, p0

    move-object/from16 v10, p2

    invoke-direct/range {v3 .. v10}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Landroid/widget/CheckBox;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;Landroid/widget/LinearLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 540
    new-instance v10, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;

    move-object/from16 v11, p0

    move-object v12, v5

    move-object v13, v6

    move-object v15, v7

    move-object/from16 v16, v9

    move-object/from16 v17, p2

    invoke-direct/range {v10 .. v17}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Landroid/widget/CheckBox;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Landroid/widget/LinearLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    invoke-virtual {v14, v10}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 555
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 564
    return-void

    .line 443
    .end local v25    # "isConnecting":Z
    .end local v26    # "isPoweringOnOff":Z
    :cond_2
    const/16 v25, 0x0

    goto/16 :goto_0

    .line 448
    .restart local v25    # "isConnecting":Z
    .restart local v26    # "isPoweringOnOff":Z
    :cond_3
    const/16 v3, 0x8

    goto/16 :goto_1

    .line 449
    :cond_4
    const/16 v3, 0x8

    goto/16 :goto_2

    .line 451
    :cond_5
    const/16 v3, 0x8

    goto/16 :goto_3

    .line 453
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 455
    :cond_7
    const/16 v3, 0x8

    goto/16 :goto_5

    .line 456
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 459
    :cond_9
    const/16 v3, 0x8

    goto/16 :goto_7

    .line 464
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_8

    .line 465
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 467
    :cond_c
    const/16 v3, 0x8

    goto/16 :goto_a

    .line 468
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 470
    :cond_e
    const/16 v3, 0x8

    goto/16 :goto_c

    .line 471
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_d

    .line 473
    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_e

    .line 474
    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_f

    .line 476
    :cond_12
    const/16 v3, 0x8

    goto/16 :goto_10

    .line 477
    :cond_13
    const/16 v3, 0x8

    goto/16 :goto_11
.end method

.method private bindFooterRow(Landroid/view/View;)V
    .locals 5
    .param p1, "footer"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 414
    const v2, 0x7f0e043b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 415
    .local v1, "view":Landroid/widget/Button;
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {v1}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070357

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 416
    .local v0, "text":Landroid/text/SpannableString;
    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v0, v2, v4, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 417
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 418
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v2

    if-nez v2, :cond_0

    .line 421
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    const v2, 0x7f0e043c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$502(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 422
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 423
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$600(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    .line 425
    :cond_0
    return-void
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 568
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    .line 569
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->FOOTER:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    .line 575
    .local v0, "type":Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    :goto_0
    return-object v0

    .line 570
    .end local v0    # "type":Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 571
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->NO_CONSOLES:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    .restart local v0    # "type":Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    goto :goto_0

    .line 573
    .end local v0    # "type":Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->CONSOLE:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    .restart local v0    # "type":Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    goto :goto_0
.end method

.method private reportVortexAutoConnectCheck(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;)V
    .locals 5
    .param p1, "row"    # Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    .prologue
    const/4 v4, 0x0

    .line 579
    const v2, 0x7f0e042a

    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 580
    .local v1, "expandedView":Landroid/widget/RelativeLayout;
    const v2, 0x7f0e042f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 581
    .local v0, "autoConnectCheckbox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 582
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Check Auto Connect"

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :goto_0
    return-void

    .line 584
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Uncheck Auto Connect"

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 369
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$7;->$SwitchMap$com$microsoft$xbox$xle$app$dialog$ConnectDialog$ViewType:[I

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 373
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 371
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    goto :goto_0

    .line 369
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 351
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getItem(I)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 379
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 384
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$7;->$SwitchMap$com$microsoft$xbox$xle$app$dialog$ConnectDialog$ViewType:[I

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 399
    if-nez p2, :cond_0

    .line 400
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300b1

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 402
    :cond_0
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->bindFooterRow(Landroid/view/View;)V

    .line 405
    :goto_0
    return-object p2

    .line 386
    :pswitch_0
    if-nez p2, :cond_1

    .line 387
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300af

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_1
    move-object v0, p2

    .line 389
    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->getItem(I)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->bindConsoleRow(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    goto :goto_0

    .line 392
    :pswitch_1
    if-nez p2, :cond_2

    .line 393
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300b2

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$302(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;Z)Z

    goto :goto_0

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 359
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->values()[Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 410
    const/4 v0, 0x0

    return v0
.end method
