.class public Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;
.super Ljava/lang/Object;
.source "SkypeUtil.java"


# static fields
.field private static final FIX_MESSAGING_URL:Ljava/lang/String; = "https://support.xbox.com/fix-messaging"

.field private static final MAX_GAMERTAG_COUNT_ALLOWED:I = 0x2

.field private static final RELYING_PARTY:Ljava/lang/String; = "https://skypexbox.skype.com"

.field private static final TAG:Ljava/lang/String;

.field private static final TOPIC_MEMBER_PREFIX:Ljava/lang/String; = "xbox"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;)V
    .locals 1
    .param p0, "registrationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 321
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method public static cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p0, "registrationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "skypeRegistrationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 325
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v7, "cleaning up unused Skype endpoints"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 328
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v0

    .line 329
    .local v0, "ES_SERVICE_MANAGER":Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->getEDFRegistrationForSkypeNotifications()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$EdfRegistrationResponse;

    move-result-object v4

    .line 331
    .local v4, "edfEndpoints":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$EdfRegistrationResponse;
    if-eqz v4, :cond_2

    .line 332
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$EdfRegistrationResponse;->getBindings()Ljava/util/List;

    move-result-object v2

    .line 333
    .local v2, "bindings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 334
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;

    .line 335
    .local v1, "binding":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;
    if-eqz v1, :cond_0

    .line 336
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "com.microsoft.xboxone.smartglass.beta.android"

    .line 337
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->getAppId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 338
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;->getRegistrationId()Ljava/lang/String;

    move-result-object v5

    .line 339
    .local v5, "regId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 341
    :cond_1
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v8, "deleting unused registration %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    invoke-interface {v0, v5}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->deleteEDFRegistrationForSkypeNotifications(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_1

    .line 347
    :goto_1
    :try_start_1
    invoke-interface {v0, v5}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->deleteWithSkypeChatServiceForNotifications(Ljava/lang/String;)Z
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 348
    :catch_0
    move-exception v3

    .line 349
    .local v3, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v8, "Error in deleting skype endpoint"

    invoke-static {v7, v8, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 343
    .end local v3    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_1
    move-exception v3

    .line 344
    .restart local v3    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v8, "Error in deleting edf endpoint"

    invoke-static {v7, v8, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 356
    .end local v1    # "binding":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;
    .end local v2    # "bindings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;>;"
    .end local v3    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v5    # "regId":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public static conversationHasUnreadMessages(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z
    .locals 10
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 178
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 180
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->sentTime:Ljava/util/Date;

    if-eqz v7, :cond_2

    .line 181
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->sentTime:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 183
    .local v0, "lastMessageArrivalTime":J
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->consumptionHorizon:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 184
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->consumptionHorizon:Ljava/lang/String;

    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 186
    .local v4, "lastSeenTime":[Ljava/lang/String;
    array-length v7, v4

    if-lez v7, :cond_0

    .line 187
    aget-object v7, v4, v6

    const-wide/16 v8, -0x1

    invoke-static {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 188
    .local v2, "lastSeenOriginalArrivalTime":J
    cmp-long v7, v2, v0

    if-gez v7, :cond_1

    .line 197
    .end local v0    # "lastMessageArrivalTime":J
    .end local v2    # "lastSeenOriginalArrivalTime":J
    .end local v4    # "lastSeenTime":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .restart local v0    # "lastMessageArrivalTime":J
    .restart local v2    # "lastSeenOriginalArrivalTime":J
    .restart local v4    # "lastSeenTime":[Ljava/lang/String;
    :cond_1
    move v5, v6

    .line 188
    goto :goto_0

    .end local v0    # "lastMessageArrivalTime":J
    .end local v2    # "lastSeenOriginalArrivalTime":J
    .end local v4    # "lastSeenTime":[Ljava/lang/String;
    :cond_2
    move v5, v6

    .line 197
    goto :goto_0
.end method

.method public static getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 100
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;

    move-result-object v0

    .line 101
    .local v0, "addMemberMetadata":Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;
    if-eqz v0, :cond_0

    .line 102
    iget-object v1, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;->initiator:Ljava/lang/String;

    .line 105
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getAddMemberMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;
    .locals 4
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 110
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 113
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;

    invoke-virtual {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v2

    .line 114
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse content xml"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 118
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v3, "Empty messageType or content in skype message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;

    move-result-object v0

    .line 91
    .local v0, "addMemberMetadata":Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;
    if-eqz v0, :cond_0

    .line 92
    iget-object v1, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;->target:Ljava/util/ArrayList;

    .line 95
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 127
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 129
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeRemoveMemberFormat;

    invoke-virtual {v4, v1, v5}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeRemoveMemberFormat;

    .line 130
    .local v2, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeRemoveMemberFormat;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeRemoveMemberFormat;->target:Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeRemoveMemberFormat;
    :cond_0
    :goto_0
    return-object v3

    .line 131
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v5, "Unable to parse content xml"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "is":Ljava/io/InputStream;
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v5, "Empty messageType or content in skype message"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 276
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 309
    :cond_0
    return-object v0

    .line 280
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v3

    .line 282
    .local v3, "skypeConversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    .line 289
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 290
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 291
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v5, :cond_3

    .line 292
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 293
    .local v1, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    if-eqz v1, :cond_3

    .line 294
    iget-object v5, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 299
    .end local v1    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_3
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    if-eqz v5, :cond_2

    .line 300
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 301
    .restart local v1    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    if-eqz v1, :cond_2

    .line 302
    iget-object v5, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getGroupSenderGamertagText(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p0, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 217
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 221
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getGroupTopic(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;
    .locals 13
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x2

    .line 246
    if-eqz p0, :cond_5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 247
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 248
    .local v5, "topicTokens":[Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 249
    .local v2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v6, v5

    invoke-static {v6, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 250
    .local v3, "maxCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_3

    .line 251
    aget-object v6, v5, v1

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 252
    .local v4, "skypeIdTokens":[Ljava/lang/String;
    array-length v6, v4

    if-ne v6, v10, :cond_2

    .line 253
    aget-object v6, v4, v11

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "xbox"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 254
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "8:xbox:%s"

    new-array v8, v12, [Ljava/lang/Object;

    aget-object v9, v4, v12

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    .line 271
    .end local v1    # "i":I
    .end local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "maxCount":I
    .end local v4    # "skypeIdTokens":[Ljava/lang/String;
    .end local v5    # "topicTokens":[Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v0

    .line 259
    .restart local v1    # "i":I
    .restart local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "maxCount":I
    .restart local v4    # "skypeIdTokens":[Ljava/lang/String;
    .restart local v5    # "topicTokens":[Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    goto :goto_1

    .line 263
    .end local v4    # "skypeIdTokens":[Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupSenderGamertagText(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "groupTopic":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 265
    array-length v6, v5

    if-le v6, v10, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2026

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 268
    :cond_4
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070632

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 271
    .end local v0    # "groupTopic":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "maxCount":I
    .end local v5    # "topicTokens":[Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fromUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 202
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "urlTokens":[Ljava/lang/String;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 206
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    .line 210
    .end local v0    # "urlTokens":[Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 169
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;

    move-result-object v0

    .line 170
    .local v0, "topicMetaData":Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;->initiator:Ljava/lang/String;

    .line 174
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getTopicMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;
    .locals 4
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 143
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 144
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 146
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;

    invoke-virtual {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v2

    .line 147
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse content xml"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 151
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v3, "Empty messageType or content in skype message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "messageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "content"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 159
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;

    move-result-object v0

    .line 160
    .local v0, "topicMetaData":Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;
    if-eqz v0, :cond_0

    .line 161
    iget-object v1, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeTopicUpdateFormat;->value:Ljava/lang/String;

    .line 164
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getXuid(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "link"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 226
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 227
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "urlTokens":[Ljava/lang/String;
    array-length v3, v2

    if-lez v3, :cond_0

    .line 230
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v2, v3

    .line 231
    .local v0, "id":Ljava/lang/String;
    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "subIds":[Ljava/lang/String;
    array-length v3, v1

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 233
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    .line 238
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "subIds":[Ljava/lang/String;
    .end local v2    # "urlTokens":[Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getXuidFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Ljava/lang/String;
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 77
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 79
    const-string v2, ":"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "subIds":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 82
    aget-object v2, v0, v4

    invoke-static {v2}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v1, v0, v4

    .line 85
    :cond_0
    return-object v1
.end method

.method public static isSkypeGroupMessage(Ljava/lang/String;)Z
    .locals 3
    .param p0, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 47
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "subIds":[Ljava/lang/String;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 50
    const/4 v1, 0x0

    aget-object v1, v0, v1

    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Group:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 52
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid skype id format"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static isSkypeServiceMessage(Ljava/lang/String;)Z
    .locals 3
    .param p0, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 57
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 59
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "subIds":[Ljava/lang/String;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 62
    const/4 v1, 0x0

    aget-object v1, v0, v1

    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 64
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid skype id format"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic lambda$showFixMessagingDialog$0()V
    .locals 3

    .prologue
    .line 366
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "https://support.xbox.com/fix-messaging"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 367
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 368
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/XLEApplication;->startActivity(Landroid/content/Intent;)V

    .line 369
    return-void
.end method

.method public static registerWithSkypeUsingRefreshedAuthTokens()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 313
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 314
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    move-result-object v0

    const-string v1, "https://skypexbox.skype.com"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXTokenString(Ljava/lang/String;Z)Ljava/lang/String;

    .line 315
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isNotificationsMessageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerPushNotificationWithWithSkype()V

    .line 318
    :cond_0
    return-void
.end method

.method public static showFixMessagingDialog()V
    .locals 7

    .prologue
    .line 359
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->TAG:Ljava/lang/String;

    const-string v1, "Displaying \'Fix Messaging\' dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070756

    .line 362
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070753

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "https://support.xbox.com/fix-messaging"

    aput-object v6, v4, v5

    .line 363
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070757

    .line 364
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil$$Lambda$1;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07060d

    .line 370
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 361
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 373
    return-void
.end method
