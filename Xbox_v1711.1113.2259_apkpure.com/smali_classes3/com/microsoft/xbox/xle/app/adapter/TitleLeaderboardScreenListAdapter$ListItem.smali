.class public Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;
.super Ljava/lang/Object;
.source "TitleLeaderboardScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListItem"
.end annotation


# instance fields
.field private gamerRealNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private gamerTagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rankTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private scoreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const v0, 0x7f0e0ac6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->gamerTagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    const v0, 0x7f0e0ac7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->gamerRealNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 56
    const v0, 0x7f0e0ac8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->scoreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    const v0, 0x7f0e0ac5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->rankTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    return-void
.end method


# virtual methods
.method public updateContent(Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;Z)V
    .locals 5
    .param p1, "user"    # Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    .param p2, "shouldFormat"    # Z

    .prologue
    const/4 v4, 0x0

    .line 61
    if-eqz p1, :cond_1

    .line 62
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->gamerTagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->gamertag:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->gamerRealNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->getGamerRealName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 65
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->values:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->values:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 67
    if-eqz p2, :cond_2

    .line 68
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->values:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "timeStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->scoreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    .end local v1    # "timeStr":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->rankTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget v3, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->rank:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "myXuid":Ljava/lang/String;
    if-eqz v0, :cond_3

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->rankTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundColor(I)V

    .line 84
    .end local v0    # "myXuid":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 71
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->scoreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->values:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    .restart local v0    # "myXuid":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->rankTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f0c0004

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundResource(I)V

    goto :goto_1
.end method
