.class public final enum Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;
.super Ljava/lang/Enum;
.source "ActivityFeedsService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;",
        ">;",
        "Lcom/microsoft/xbox/service/activityFeed/IActivityFeedsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

.field private static final ACTIVITY_FEED_HIDE_ENDPOINT:Ljava/lang/String; = "https://avty.xboxlive.com/users/me/hide"

.field private static final ACTIVITY_FEED_ME_SETTINGS_ENDPOINT:Ljava/lang/String; = "https://avty.xboxlive.com/users/me/settings"

.field private static final ACTIVITY_FEED_PIN_ENDPOINT:Ljava/lang/String; = "https://avty.xboxlive.com/timelines/%s/%s/pins"

.field private static final ACTIVITY_FEED_UNHIDE_ENDPOINT:Ljava/lang/String; = "https://avty.xboxlive.com/users/me/unhide"

.field private static final ACTIVITY_FEED_UNPIN_ENDPOINT:Ljava/lang/String; = "https://avty.xboxlive.com/timelines/%s/%s/unpin"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    .line 27
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->TAG:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    .line 39
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    const-string v3, "13"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic lambda$getActivityFeedMeSettings$0()Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    const-string v0, "https://avty.xboxlive.com/users/me/settings"

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getFeedPinInfo$3(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "endpoint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$hideActivityItem$1(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "hide"    # Z
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    if-eqz p0, :cond_0

    const-string v0, "https://avty.xboxlive.com/users/me/hide"

    :goto_0
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    .line 64
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 61
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "https://avty.xboxlive.com/users/me/unhide"

    goto :goto_0
.end method

.method static synthetic lambda$pinActivityItem$2(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "endpoint"    # Ljava/lang/String;
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->STATIC_HEADERS:Ljava/util/List;

    .line 80
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    return-object v0
.end method


# virtual methods
.method public getActivityFeedMeSettings()Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->TAG:Ljava/lang/String;

    const-string v1, "getActivityFeedMeSettings"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService$$Lambda$1;->lambdaFactory$()Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSettings;

    return-object v0
.end method

.method public getFeedPinInfo(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    .locals 7
    .param p1, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p2, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->TAG:Ljava/lang/String;

    const-string v2, "check pin on %s timeline %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v4

    aput-object p2, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 87
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 89
    const-string v1, "https://avty.xboxlive.com/timelines/%s/%s/pins"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object p2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "endpoint":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    return-object v1
.end method

.method public hideActivityItem(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "hide"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 56
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->TAG:Ljava/lang/String;

    const-string v2, "%s activity feed: %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    const-string v0, "hide"

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 60
    invoke-static {p2, p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService$$Lambda$2;->lambdaFactory$(ZLjava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0

    .line 56
    :cond_0
    const-string v0, "unhide"

    goto :goto_0
.end method

.method public pinActivityItem(Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z
    .locals 8
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "pin"    # Z
    .param p3, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p4, "timelineId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 69
    sget-object v2, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->TAG:Ljava/lang/String;

    const-string v3, "%s activity feed %s to %s timeline %s"

    const/4 v1, 0x4

    new-array v4, v1, [Ljava/lang/Object;

    if-eqz p2, :cond_0

    const-string v1, "pin"

    :goto_0
    aput-object v1, v4, v5

    aput-object p1, v4, v6

    aput-object p3, v4, v7

    const/4 v1, 0x3

    aput-object p4, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 71
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 72
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 74
    if-eqz p2, :cond_1

    const-string v1, "https://avty.xboxlive.com/timelines/%s/%s/pins"

    :goto_1
    new-array v2, v7, [Ljava/lang/Object;

    aput-object p3, v2, v5

    aput-object p4, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "endpoint":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v1

    return v1

    .line 69
    .end local v0    # "endpoint":Ljava/lang/String;
    :cond_0
    const-string v1, "unpin"

    goto :goto_0

    .line 74
    :cond_1
    const-string v1, "https://avty.xboxlive.com/timelines/%s/%s/unpin"

    goto :goto_1
.end method
