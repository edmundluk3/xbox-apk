.class public Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ImageViewerScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;,
        Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;
    }
.end annotation


# static fields
.field private static final SWIPE_MAX_OFF_PATH:I = 0xfa

.field private static final SWIPE_MIN_DISTANCE:I = 0x78

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0xc8


# instance fields
.field private bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

.field private gestureDector:Landroid/view/GestureDetector;

.field private imageFrame:Landroid/widget/ViewFlipper;

.field private scaleDetector:Landroid/view/ScaleGestureDetector;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    .line 59
    const v0, 0x7f0e0712

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->screenBody:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setClickable(Z)V

    .line 64
    new-instance v0, Landroid/view/GestureDetector;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$MyGestureDetector;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->gestureDector:Landroid/view/GestureDetector;

    .line 65
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$ScaleListener;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->scaleDetector:Landroid/view/ScaleGestureDetector;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 82
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->addImages()V

    .line 96
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->outToRightAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->inFromRightAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->outToLeftAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->getCurrentImage()Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->scaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->gestureDector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->inFromLeftAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)Landroid/widget/ViewFlipper;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method private addImages()V
    .locals 9

    .prologue
    .line 105
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;->getImageUris()Ljava/util/List;

    move-result-object v0

    .line 106
    .local v0, "imageUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 108
    .local v5, "vi":Landroid/view/LayoutInflater;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 110
    .local v4, "uri":Ljava/lang/String;
    const v7, 0x7f030152

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 111
    .local v3, "root":Landroid/widget/RelativeLayout;
    const v7, 0x7f0e023e

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;

    .line 112
    .local v2, "progressView":Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;
    if-eqz v2, :cond_0

    .line 113
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;->setVisibility(I)V

    .line 116
    :cond_0
    const v7, 0x7f0e0710

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    .line 117
    .local v1, "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    invoke-virtual {v7, v3}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 118
    const v7, 0x7f0e001d

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {v1, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setTag(ILjava/lang/Object;)V

    .line 119
    sget v7, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    const v8, 0x7f0201fa

    invoke-static {v1, v4, v7, v8}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load1080p(Landroid/widget/ImageView;Ljava/lang/String;II)V

    goto :goto_0

    .line 121
    .end local v1    # "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
    .end local v2    # "progressView":Lcom/microsoft/xbox/toolkit/ui/FastProgressBar;
    .end local v3    # "root":Landroid/widget/RelativeLayout;
    .end local v4    # "uri":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private getCurrentImage()Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
    .locals 2

    .prologue
    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;->imageFrame:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 125
    .local v0, "root":Landroid/widget/RelativeLayout;
    const v1, 0x7f0e0710

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;

    return-object v1
.end method

.method private inFromLeftAnimation()Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 306
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const v2, -0x40666666    # -1.2f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 307
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 308
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 309
    return-object v0
.end method

.method private inFromRightAnimation()Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 292
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const v2, 0x3f99999a    # 1.2f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 293
    .local v0, "inFromRight":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 294
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 295
    return-object v0
.end method

.method private outToLeftAnimation()Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 299
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const v4, -0x40666666    # -1.2f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 300
    .local v0, "outtoLeft":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 301
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 302
    return-object v0
.end method

.method private outToRightAnimation()Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 313
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const v4, 0x3f99999a    # 1.2f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 314
    .local v0, "outtoRight":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 315
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 316
    return-object v0
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method
