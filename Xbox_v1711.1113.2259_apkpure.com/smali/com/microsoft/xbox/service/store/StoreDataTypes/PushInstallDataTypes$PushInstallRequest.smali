.class public Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;
.super Ljava/lang/Object;
.source "PushInstallDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PushInstallRequest"
.end annotation


# instance fields
.field public final checkSatisfyingEntitlements:Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CheckSatisfyingEntitlements"
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final installOnXboxHomeConsole:Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "InstallOnXboxHomeConsole"
    .end annotation
.end field

.field public final language:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Language"
    .end annotation
.end field

.field public final market:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Market"
    .end annotation
.end field

.field private final productSkuIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductSkuIds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "productSkuIds":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;>;"
    const/4 v1, 0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->market:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->language:Ljava/lang/String;

    .line 47
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->installOnXboxHomeConsole:Z

    .line 48
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->checkSatisfyingEntitlements:Z

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->productSkuIds:Ljava/util/List;

    .line 50
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 54
    if-ne p1, p0, :cond_0

    .line 55
    const/4 v1, 0x1

    .line 61
    :goto_0
    return v1

    .line 56
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;

    if-nez v1, :cond_1

    .line 57
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 59
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;

    .line 61
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->productSkuIds:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->productSkuIds:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 67
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->hashCode:I

    if-nez v0, :cond_0

    .line 68
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->hashCode:I

    .line 69
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->productSkuIds:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->hashCode:I

    .line 71
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
