.class public Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PopupScreen;
.source "HomeScreenPopupScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final vmp:Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V
    .locals 0
    .param p1, "vmp"    # Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->vmp:Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;

    .line 23
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->onCreate()V

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->onCreateContentView()V

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->vmp:Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 30
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f030141

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;->setContentView(I)V

    .line 35
    return-void
.end method
