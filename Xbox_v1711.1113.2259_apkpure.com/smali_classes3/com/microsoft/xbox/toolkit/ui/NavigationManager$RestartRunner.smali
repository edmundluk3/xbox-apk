.class Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;
.super Ljava/lang/Object;
.source "NavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RestartRunner"
.end annotation


# instance fields
.field private final params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 0
    .param p2, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 912
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .line 914
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;

    .prologue
    .line 909
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;-><init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 919
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$902(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Z)Z

    .line 920
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 921
    .local v0, "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 923
    if-eqz v0, :cond_0

    .line 924
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 925
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 926
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 929
    :cond_0
    const-string v5, "navigationParameters cannot be empty!"

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$1000(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/Stack;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    :goto_0
    invoke-static {v5, v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 930
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$1000(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/Stack;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 931
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$1000(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/Stack;

    move-result-object v3

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v3, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 933
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 934
    .local v2, "to":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 936
    if-eqz v2, :cond_1

    .line 937
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 938
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 939
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 940
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onAnimateInStarted()V

    .line 954
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$1100(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/List;

    move-result-object v5

    monitor-enter v5

    .line 955
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$1100(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;

    .line 956
    .local v1, "listener":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;->onPageRestarted(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    goto :goto_1

    .line 958
    .end local v1    # "listener":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .end local v2    # "to":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_2
    move v3, v4

    .line 929
    goto :goto_0

    .line 958
    .restart local v2    # "to":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_3
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 961
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->access$902(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Z)Z

    .line 962
    return-void
.end method
