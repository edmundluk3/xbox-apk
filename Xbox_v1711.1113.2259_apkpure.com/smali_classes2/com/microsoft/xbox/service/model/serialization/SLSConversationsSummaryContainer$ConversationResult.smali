.class public Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
.super Ljava/lang/Object;
.source "SLSConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConversationResult"
.end annotation


# instance fields
.field public conversation:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 144
    const-class v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;

    return-object v0
.end method
