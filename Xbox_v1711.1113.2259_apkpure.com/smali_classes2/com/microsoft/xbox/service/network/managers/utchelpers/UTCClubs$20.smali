.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$20;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateCancel(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$createClubPageNum:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 440
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$20;->val$createClubPageNum:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 2

    .prologue
    .line 443
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$20;->val$createClubPageNum:I

    packed-switch v0, :pswitch_data_0

    .line 454
    const-string v0, "Clubs - Create Cancel"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 458
    :goto_0
    return-void

    .line 445
    :pswitch_0
    const-string v0, "Clubs - Create Cancel"

    const-string v1, "Clubs - Create Club Choose Type view"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 448
    :pswitch_1
    const-string v0, "Clubs - Create Cancel"

    const-string v1, "Clubs - Create Club Choose Name view"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 451
    :pswitch_2
    const-string v0, "Clubs - Create Cancel"

    const-string v1, "Clubs - Create Club Confirmation view"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
