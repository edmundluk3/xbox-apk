.class public Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "ClubDiscoveryScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;


# instance fields
.field private getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

.field private final invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

.field private isLoadingRecommendations:Z

.field private isLoadingSocialTags:Z

.field private loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final recommendationCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->TAG:Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>()V

    .line 67
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->setScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 69
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubDiscoveryScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 74
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationList:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationCategoryList:Ljava/util/List;

    .line 78
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingRecommendations:Z

    .line 79
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingSocialTags:Z

    .line 80
    return-void
.end method

.method static synthetic access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingRecommendations:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->onGetRecommendedClubsCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingSocialTags:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->onSocialTagsLoaded()V

    return-void
.end method

.method private buildRecommendationLists()V
    .locals 12

    .prologue
    .line 207
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationCategoryList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 208
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 210
    .local v5, "recommendationMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 211
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v4

    .line 214
    .local v4, "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    if-eqz v4, :cond_0

    .line 215
    sget-object v6, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v1

    .line 216
    .local v1, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->reasons()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;

    .line 217
    .local v2, "reason":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;->localizedText()Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "reasonText":Ljava/lang/String;
    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 219
    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->addCardModel(Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V

    goto :goto_0

    .line 220
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 221
    new-instance v6, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 223
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->criteria()Ljava/lang/String;

    move-result-object v9

    .line 224
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 225
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->associatedTitles()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-direct {v6, v3, v9, v10, v11}, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 221
    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 232
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .end local v2    # "reason":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;
    .end local v3    # "reasonText":Ljava/lang/String;
    .end local v4    # "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationCategoryList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 233
    return-void
.end method

.method private cancelTasks()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 97
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->cancel()V

    .line 102
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->cancel()V

    .line 107
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    .line 109
    :cond_2
    return-void
.end method

.method private onGetRecommendedClubsCompleted(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "recommendedClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGetRecommendedClubsCompleted "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_1

    const-string v0, "NULL"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 242
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingRecommendations:Z

    .line 244
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->buildRecommendationLists()V

    .line 246
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateViewModelState()Z

    .line 247
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateAdapter()V

    .line 248
    return-void

    .line 236
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private onSocialTagsLoaded()V
    .locals 2

    .prologue
    .line 251
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onSocialTagsLoaded"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingSocialTags:Z

    .line 255
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateViewModelState()Z

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateAdapter()V

    .line 257
    return-void
.end method

.method private updateViewModelState()Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 195
    .local v0, "oldState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingRecommendations:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->isLoadingSocialTags:Z

    if-eqz v1, :cond_1

    .line 196
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 203
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationList:Ljava/util/List;

    if-nez v1, :cond_2

    .line 198
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 200
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 203
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getInvitationsViewModel()Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->invitationsViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    return-object v0
.end method

.method public getRecommendationCategoryList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->recommendationCategoryList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchCreate()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateJoinParticipateClubs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverCreate()V

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;

    invoke-virtual {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 183
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 176
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 178
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070494

    .line 179
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 177
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 180
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 175
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public launchRecommendations(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "criteria"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 164
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 165
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 166
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    invoke-direct {v0, p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .local v0, "params":Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 168
    return-void
.end method

.method public launchSearch()V
    .locals 1

    .prologue
    .line 159
    const-string v0, "Clubs - Discover Search"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 160
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 161
    return-void
.end method

.method public launchYourClubs()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "Clubs - Discover Search"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPeopleHubSocialScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 190
    :cond_0
    return-void
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->load(Z)V

    .line 120
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_1

    .line 121
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 124
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->cancelTasks()V

    .line 126
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->load(Z)V

    .line 129
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->hasValidData()Z

    move-result v0

    if-nez v0, :cond_2

    .line 130
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->loadSocialTagsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->load(Z)V

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateViewModelState()Z

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateAdapter()V

    .line 136
    return-void
.end method

.method protected onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "child"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->updateAdapter()V

    .line 147
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onRehydrate()V

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubDiscoveryScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 86
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onStopOverride()V

    .line 91
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->cancelTasks()V

    .line 92
    return-void
.end method
