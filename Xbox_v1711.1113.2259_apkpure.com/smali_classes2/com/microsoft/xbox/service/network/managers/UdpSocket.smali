.class public Lcom/microsoft/xbox/service/network/managers/UdpSocket;
.super Ljava/lang/Object;
.source "UdpSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final PACKET_SIZE:I = 0x1388


# instance fields
.field private group:Ljava/net/InetAddress;

.field private messageReceivedDelegate:I

.field private socket:Ljava/net/MulticastSocket;

.field private stopped:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 6
    .param p1, "groupAddr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "delegate"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v1, "UdpSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating udpsocket for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->messageReceivedDelegate:I

    .line 28
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    .line 31
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    .line 32
    new-instance v1, Ljava/net/MulticastSocket;

    invoke-direct {v1, p2}, Ljava/net/MulticastSocket;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    invoke-virtual {v1, v2}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/InetAddress;)V

    .line 34
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    .line 35
    const-string v1, "UdpSocket"

    const-string v2, "socket successfully created"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UdpSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to join group with exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iput-object v5, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    .line 39
    iput-object v5, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    .line 40
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    goto :goto_0
.end method

.method private cleanup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 87
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 90
    :try_start_0
    const-string v1, "UdpSocket"

    const-string v2, "leave the multicast group"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    invoke-virtual {v1, v2}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/InetAddress;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    invoke-virtual {v1}, Ljava/net/MulticastSocket;->close()V

    .line 99
    :cond_0
    iput-object v4, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    .line 100
    iput-object v4, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->group:Ljava/net/InetAddress;

    .line 101
    return-void

    .line 87
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "UdpSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to leave the group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$run$0(Lcom/microsoft/xbox/service/network/managers/UdpSocket;Ljava/lang/String;I[BII)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/network/managers/UdpSocket;
    .param p1, "hostAddr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "receivedData"    # [B
    .param p4, "length"    # I
    .param p5, "delegatePtr"    # I

    .prologue
    .line 67
    const-string v0, "UdpSocket"

    const-string v1, " calling onMulticastMessageReceived"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    if-nez v0, :cond_0

    .line 69
    invoke-static {p1, p2, p3, p4, p5}, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->onMulticastMessageReceived(Ljava/lang/String;I[BII)V

    .line 71
    :cond_0
    return-void
.end method

.method static synthetic lambda$run$1(Lcom/microsoft/xbox/service/network/managers/UdpSocket;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/service/network/managers/UdpSocket;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->cleanup()V

    return-void
.end method

.method public static native onMulticastMessageReceived(Ljava/lang/String;I[BII)V
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v11, 0x1388

    .line 54
    const-string v0, "UdpSocket"

    const-string v9, "the thread started ..."

    invoke-static {v0, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :goto_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    if-nez v0, :cond_0

    .line 56
    new-array v6, v11, [B

    .line 57
    .local v6, "data":[B
    new-instance v8, Ljava/net/DatagramPacket;

    invoke-direct {v8, v6, v11}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 59
    .local v8, "packet":Ljava/net/DatagramPacket;
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->socket:Ljava/net/MulticastSocket;

    invoke-virtual {v0, v8}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 60
    invoke-virtual {v8}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v3

    .line 61
    .local v3, "receivedData":[B
    invoke-virtual {v8}, Ljava/net/DatagramPacket;->getLength()I

    move-result v4

    .line 62
    .local v4, "length":I
    invoke-virtual {v8}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "hostAddr":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/net/DatagramPacket;->getPort()I

    move-result v2

    .line 64
    .local v2, "port":I
    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->messageReceivedDelegate:I

    .line 66
    .local v5, "delegatePtr":I
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/UdpSocket$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/UdpSocket;Ljava/lang/String;I[BII)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 73
    .end local v1    # "hostAddr":Ljava/lang/String;
    .end local v2    # "port":I
    .end local v3    # "receivedData":[B
    .end local v4    # "length":I
    .end local v5    # "delegatePtr":I
    :catch_0
    move-exception v7

    .line 76
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "UdpSocket"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failed to receive packet with exception "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v6    # "data":[B
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "packet":Ljava/net/DatagramPacket;
    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/UdpSocket$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/UdpSocket;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 83
    const-string v0, "UdpSocket"

    const-string v9, "the thread is exiting..."

    invoke-static {v0, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 46
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->stopped:Z

    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/UdpSocket;->cleanup()V

    .line 49
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
