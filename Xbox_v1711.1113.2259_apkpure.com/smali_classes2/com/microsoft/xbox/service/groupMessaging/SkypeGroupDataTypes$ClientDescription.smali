.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientDescription"
.end annotation


# instance fields
.field private final appId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;->appId:Ljava/lang/String;

    .line 426
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 429
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;

    return-object v0
.end method
