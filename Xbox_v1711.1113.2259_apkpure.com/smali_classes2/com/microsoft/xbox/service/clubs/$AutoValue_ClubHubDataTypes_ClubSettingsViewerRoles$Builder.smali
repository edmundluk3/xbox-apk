.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private localizedRole:Ljava/lang/String;

.field private roles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;-><init>()V

    .line 74
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;-><init>()V

    .line 76
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->roles:Lcom/google/common/collect/ImmutableList;

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->localizedRole()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->localizedRole:Ljava/lang/String;

    .line 78
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    .locals 3

    .prologue
    .line 91
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->roles:Lcom/google/common/collect/ImmutableList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->localizedRole:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/String;)V

    return-object v0
.end method

.method public localizedRole(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;
    .locals 0
    .param p1, "localizedRole"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->localizedRole:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method public roles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "roles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettingsViewerRoles$Builder;->roles:Lcom/google/common/collect/ImmutableList;

    .line 82
    return-object p0

    .line 81
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method
