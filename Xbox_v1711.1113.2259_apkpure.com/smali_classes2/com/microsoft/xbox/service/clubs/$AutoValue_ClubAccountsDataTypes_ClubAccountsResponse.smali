.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
.source "$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse.java"


# instance fields
.field private final canDeleteImmediately:Z

.field private final code:I

.field private final created:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final id:J

.field private final name:Ljava/lang/String;

.field private final owner:Ljava/lang/String;

.field private final suspensionRequiredAfter:Ljava/util/Date;

.field private final suspensions:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;"
        }
    .end annotation
.end field

.field private final type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "owner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "id"    # J
    .param p7, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "created"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "canDeleteImmediately"    # Z
    .param p10, "suspensionRequiredAfter"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Date;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p11, "suspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;-><init>()V

    .line 34
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->code:I

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    .line 38
    iput-wide p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    .line 39
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 40
    iput-object p8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    .line 41
    iput-boolean p9, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->canDeleteImmediately:Z

    .line 42
    iput-object p10, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    .line 43
    iput-object p11, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    .line 44
    return-void
.end method


# virtual methods
.method public canDeleteImmediately()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->canDeleteImmediately:Z

    return v0
.end method

.method public code()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->code:I

    return v0
.end method

.method public created()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    if-ne p1, p0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v1

    .line 124
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 125
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 126
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->code:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->code()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 127
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 128
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->name()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 129
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->owner()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    .line 130
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->id()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-nez v3, :cond_6

    .line 131
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 132
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->created()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->canDeleteImmediately:Z

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->canDeleteImmediately()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    if-nez v3, :cond_8

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensionRequiredAfter()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_9

    .line 135
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 127
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 128
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 129
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->owner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 131
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 132
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->created()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 134
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensionRequiredAfter()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 135
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :cond_a
    move v1, v2

    .line 137
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 142
    const/4 v0, 0x1

    .line 143
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 144
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->code:I

    xor-int/2addr v0, v1

    .line 145
    mul-int/2addr v0, v3

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 147
    mul-int/2addr v0, v3

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 149
    mul-int/2addr v0, v3

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 151
    mul-int/2addr v0, v3

    .line 152
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 153
    mul-int/2addr v0, v3

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 155
    mul-int/2addr v0, v3

    .line 156
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 157
    mul-int/2addr v0, v3

    .line 158
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->canDeleteImmediately:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x4cf

    :goto_5
    xor-int/2addr v0, v1

    .line 159
    mul-int/2addr v0, v3

    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    xor-int/2addr v0, v1

    .line 161
    mul-int/2addr v0, v3

    .line 162
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_7

    :goto_7
    xor-int/2addr v0, v2

    .line 163
    return v0

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->hashCode()I

    move-result v1

    goto :goto_3

    .line 156
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 158
    :cond_5
    const/16 v1, 0x4d5

    goto :goto_5

    .line 160
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    goto :goto_6

    .line 162
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v2

    goto :goto_7
.end method

.method public id()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    return-wide v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    return-object v0
.end method

.method public owner()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    return-object v0
.end method

.method public suspensionRequiredAfter()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    return-object v0
.end method

.method public suspensions()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubAccountsResponse{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->code:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", created="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->created:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canDeleteImmediately="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->canDeleteImmediately:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", suspensionRequiredAfter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensionRequiredAfter:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", suspensions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->suspensions:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method
