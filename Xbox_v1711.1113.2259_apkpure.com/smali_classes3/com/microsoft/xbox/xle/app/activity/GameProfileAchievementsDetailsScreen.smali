.class public Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsDetailsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;
.source "GameProfileAchievementsDetailsScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;-><init>()V

    .line 9
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const-string v0, "Game Profile Achievement Details"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;->onCreate()V

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsDetailsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 16
    return-void
.end method
