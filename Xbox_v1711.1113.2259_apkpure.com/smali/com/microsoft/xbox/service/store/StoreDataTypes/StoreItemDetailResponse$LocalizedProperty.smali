.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalizedProperty"
.end annotation


# instance fields
.field public final developerName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DeveloperName"
    .end annotation
.end field

.field public final eligibilityProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "EligibilityProperties"
    .end annotation
.end field

.field private final images:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Images"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;",
            ">;"
        }
    .end annotation
.end field

.field public final productDescription:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductDescription"
    .end annotation
.end field

.field public final productTitle:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductTitle"
    .end annotation
.end field

.field public final publisherName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PublisherName"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eligibilityProperties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "developerName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "productDescription"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "productTitle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p6, "publisherName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 930
    .local p3, "images":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 931
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 933
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->eligibilityProperties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;

    .line 934
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->developerName:Ljava/lang/String;

    .line 935
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->images:Ljava/util/List;

    .line 936
    iput-object p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->productDescription:Ljava/lang/String;

    .line 937
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->productTitle:Ljava/lang/String;

    .line 938
    iput-object p6, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->publisherName:Ljava/lang/String;

    .line 939
    return-void
.end method


# virtual methods
.method public getImages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 943
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;->images:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
