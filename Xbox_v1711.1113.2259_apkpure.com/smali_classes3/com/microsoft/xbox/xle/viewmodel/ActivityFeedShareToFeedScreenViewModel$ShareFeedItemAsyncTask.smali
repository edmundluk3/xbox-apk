.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedShareToFeedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShareFeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final caption:Ljava/lang/String;

.field private final locator:Ljava/lang/String;

.field private final pinAfterPosting:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Z)V
    .locals 1
    .param p2, "pinAfterPosting"    # Z

    .prologue
    .line 273
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getCaption()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->caption:Ljava/lang/String;

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getLocator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->locator:Ljava/lang/String;

    .line 274
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->pinAfterPosting:Z

    .line 275
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 6

    .prologue
    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 299
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$600()Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->locator:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->caption:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->sharePostToClubFeed(Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 301
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$600()Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->locator:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->sharePostToUserFeed(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->onError()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    .line 285
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->pinAfterPosting:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    .line 308
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 268
    check-cast p1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 294
    return-void
.end method
