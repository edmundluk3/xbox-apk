.class public abstract Lcom/microsoft/xbox/data/repository/PagedRepository;
.super Ljava/lang/Object;
.source "PagedRepository.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SERVICE_TYPE:",
        "Ljava/lang/Object;",
        "C",
        "LIENT_TYPE:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TC",
            "LIENT_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/data/repository/DataMapper",
            "<TSERVICE_TYPE;TC",
            "LIENT_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private isReloading:Z

.field private loadDisposable:Lio/reactivex/disposables/Disposable;

.field private resultSubject:Lio/reactivex/subjects/AsyncSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/AsyncSubject",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<TC",
            "LIENT_TYPE;",
            ">;>;"
        }
    .end annotation
.end field

.field protected final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final subjectLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/DataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/data/repository/DataMapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/DataMapper",
            "<TSERVICE_TYPE;TC",
            "LIENT_TYPE;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    .local p1, "dataMapper":Lcom/microsoft/xbox/data/repository/DataMapper;, "Lcom/microsoft/xbox/data/repository/DataMapper<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->subjectLock:Ljava/lang/Object;

    .line 44
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;

    .line 48
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 49
    return-void
.end method

.method static synthetic lambda$load$0(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0
    .param p0, "collection"    # Ljava/util/ArrayList;
    .param p1, "item"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic lambda$load$1(Lcom/microsoft/xbox/data/repository/PagedRepository;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "result"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 118
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/AsyncSubject;->onNext(Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->onComplete()V

    .line 122
    return-void
.end method

.method static synthetic lambda$refresh$2(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0
    .param p0, "collection"    # Ljava/util/ArrayList;
    .param p1, "item"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic lambda$refresh$3(Lcom/microsoft/xbox/data/repository/PagedRepository;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "list"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/AsyncSubject;->onNext(Ljava/lang/Object;)V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->isReloading:Z

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->onComplete()V

    .line 176
    return-void
.end method

.method static synthetic lambda$refresh$4(Lcom/microsoft/xbox/data/repository/PagedRepository;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->isReloading:Z

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/AsyncSubject;->onError(Ljava/lang/Throwable;)V

    .line 181
    return-void
.end method

.method private loadNextPageSafe()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<TSERVICE_TYPE;>;"
        }
    .end annotation

    .prologue
    .line 193
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadNextPage()Lio/reactivex/Observable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 195
    :goto_0
    return-object v1

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {v0}, Lio/reactivex/Observable;->error(Ljava/lang/Throwable;)Lio/reactivex/Observable;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected abstract hasMoreToLoad()Z
.end method

.method public load()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<TC",
            "LIENT_TYPE;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->subjectLock:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->hasComplete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    monitor-exit v1

    .line 127
    :goto_0
    return-object v0

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository;->hasMoreToLoad()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 100
    :cond_1
    :try_start_1
    invoke-static {}, Lio/reactivex/subjects/AsyncSubject;->create()Lio/reactivex/subjects/AsyncSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    .line 101
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->data:Ljava/util/List;

    .line 105
    invoke-static {v0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 107
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadNextPageSafe()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/DataMapper;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 109
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 103
    invoke-static {v0, v1}, Lio/reactivex/Observable;->concat(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/BiConsumer;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->collectInto(Ljava/lang/Object;Lio/reactivex/functions/BiConsumer;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 112
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 113
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/PagedRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$4;->lambdaFactory$(Lio/reactivex/subjects/AsyncSubject;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 114
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadDisposable:Lio/reactivex/disposables/Disposable;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract loadNextPage()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<TSERVICE_TYPE;>;"
        }
    .end annotation
.end method

.method public refresh()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<TC",
            "LIENT_TYPE;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lcom/microsoft/xbox/data/repository/PagedRepository;, "Lcom/microsoft/xbox/data/repository/PagedRepository<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->subjectLock:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->hasComplete()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->isReloading:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    monitor-exit v1

    .line 184
    :goto_0
    return-object v0

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->hasComplete()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    :cond_1
    invoke-static {}, Lio/reactivex/subjects/AsyncSubject;->create()Lio/reactivex/subjects/AsyncSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    .line 151
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 158
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository;->resetContinuationToken()V

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->isReloading:Z

    .line 161
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository;->loadNextPageSafe()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/DataMapper;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 163
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/BiConsumer;

    move-result-object v2

    .line 165
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->collectInto(Ljava/lang/Object;Lio/reactivex/functions/BiConsumer;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 166
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 167
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/PagedRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/PagedRepository$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/PagedRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 168
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/PagedRepository;->resultSubject:Lio/reactivex/subjects/AsyncSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/AsyncSubject;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract resetContinuationToken()V
.end method
