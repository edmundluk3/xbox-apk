.class public Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "GameGalleryScreenAdapter.java"


# instance fields
.field private images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 28
    const v0, 0x7f0e0618

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    const v0, 0x7f0e0619

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    .line 32
    const v0, 0x7f0e061a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030114

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->updateLoadingIndicator(Z)V

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 50
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->images:Ljava/util/List;

    if-eq v1, v0, :cond_1

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->images:Ljava/util/List;

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->clear()V

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->images:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->images:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryListAdapter;->addAll(Ljava/util/Collection;)V

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 58
    :cond_1
    return-void
.end method
