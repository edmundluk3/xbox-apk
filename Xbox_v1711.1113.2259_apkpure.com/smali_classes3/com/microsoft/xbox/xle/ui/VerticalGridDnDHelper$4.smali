.class Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;
.super Landroid/view/View$DragShadowBuilder;
.source "VerticalGridDnDHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 321
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;->val$v:Landroid/view/View;

    invoke-direct {p0}, Landroid/view/View$DragShadowBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;"
    const v0, 0x3f8ccccd    # 1.1f

    .line 330
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;->val$v:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 332
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 3
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;"
    const v2, 0x3f8ccccd    # 1.1f

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;->val$v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;->val$v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 325
    iget v0, p1, Landroid/graphics/Point;->x:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p1, Landroid/graphics/Point;->y:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 326
    return-void
.end method
