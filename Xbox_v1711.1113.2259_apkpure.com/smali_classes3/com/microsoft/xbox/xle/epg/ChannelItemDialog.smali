.class public Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ChannelItemDialog.java"


# instance fields
.field private final animationName:Ljava/lang/String;

.field private final channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field private final mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 34
    const v9, 0x7f0802d3

    invoke-direct {p0, p1, v9}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 31
    const-string v9, "Screen"

    iput-object v9, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->animationName:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 36
    const v9, 0x7f03023e

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->setContentView(I)V

    .line 37
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->setCanceledOnTouchOutside(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setLayout(II)V

    .line 41
    const v9, 0x7f0e0aff

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->dialogBody:Landroid/view/View;

    .line 42
    const-string v9, "Screen"

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->setBodyAnimationName(Ljava/lang/String;)V

    .line 44
    const v9, 0x7f0e0b00

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 45
    .local v7, "root":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 46
    new-instance v9, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$1;

    invoke-direct {v9, p0, v7}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$1;-><init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Landroid/view/View;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :cond_0
    const v9, 0x7f0e056b

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 60
    .local v3, "mLogoView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const/4 v0, 0x0

    .line 61
    .local v0, "didHaveLogoToShow":Z
    if-eqz v3, :cond_1

    .line 62
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "logo":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 65
    const v9, 0x7f020167

    const v10, 0x7f0201a3

    invoke-virtual {v3, v1, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 66
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 67
    const/4 v0, 0x1

    .line 73
    .end local v1    # "logo":Ljava/lang/String;
    :cond_1
    :goto_0
    const v9, 0x7f0e056a

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v9, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 74
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v9, :cond_2

    .line 75
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v10, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;

    invoke-direct {v10, p0, p2}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;-><init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->updateFavoriteStatus()V

    .line 88
    const v9, 0x7f0e056c

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 89
    .local v5, "mTitleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v5, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f070473

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelNumber()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :cond_3
    const v9, 0x7f0e0b01

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 94
    .local v2, "mCallSignView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v2, :cond_4

    .line 95
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 97
    if-eqz v0, :cond_a

    .line 98
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    const/4 v10, -0x2

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 102
    :goto_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->requestLayout()V

    .line 105
    :cond_4
    const v9, 0x7f0e0b02

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 106
    .local v8, "space":Landroid/view/View;
    if-eqz v8, :cond_5

    .line 107
    if-eqz v0, :cond_b

    .line 108
    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 114
    :cond_5
    :goto_2
    const v9, 0x7f0e0589

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 115
    .local v6, "mTuneButton":Landroid/view/View;
    if-eqz v6, :cond_6

    .line 116
    new-instance v9, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;

    invoke-direct {v9, p0, p2}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;-><init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    invoke-virtual {v6, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :cond_6
    const v9, 0x7f0e058a

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 133
    .local v4, "mStreamButton":Landroid/view/View;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->shouldEnableStreaming(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 134
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 137
    :cond_7
    if-eqz v4, :cond_8

    .line 138
    new-instance v9, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;

    invoke-direct {v9, p0, p2}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;-><init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    :cond_8
    return-void

    .line 69
    .end local v2    # "mCallSignView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v4    # "mStreamButton":Landroid/view/View;
    .end local v5    # "mTitleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v6    # "mTuneButton":Landroid/view/View;
    .end local v8    # "space":Landroid/view/View;
    .restart local v1    # "logo":Ljava/lang/String;
    :cond_9
    const/16 v9, 0x8

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    goto/16 :goto_0

    .line 100
    .end local v1    # "logo":Ljava/lang/String;
    .restart local v2    # "mCallSignView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .restart local v5    # "mTitleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_a
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f090293

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    .line 110
    .restart local v8    # "space":Landroid/view/View;
    :cond_b
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->onClosePressed()V

    return-void
.end method

.method private onClosePressed()V
    .locals 0

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->safeDismiss()V

    .line 186
    return-void
.end method

.method public static show(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 175
    new-instance v0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 176
    .local v0, "dlg":Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 177
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->onClosePressed()V

    .line 182
    return-void
.end method

.method protected updateFavoriteStatus()V
    .locals 6

    .prologue
    .line 155
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_1

    .line 157
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 159
    .local v1, "r":Landroid/content/res/Resources;
    const v3, 0x7f070f2f

    .line 160
    .local v3, "textId":I
    const v0, 0x7f070d40

    .line 162
    .local v0, "narratorContentId":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 163
    const v3, 0x7f070f33

    .line 164
    const v0, 0x7f070d77

    .line 167
    :cond_0
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "text":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->mFavoriteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 172
    .end local v0    # "narratorContentId":I
    .end local v1    # "r":Landroid/content/res/Resources;
    .end local v2    # "text":Ljava/lang/String;
    .end local v3    # "textId":I
    :cond_1
    return-void
.end method
