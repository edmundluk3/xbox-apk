.class Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;
.super Ljava/lang/Object;
.source "TitleLeaderboardScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 68
    .local v0, "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 72
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->trackShowFriendProfile(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-void
.end method
