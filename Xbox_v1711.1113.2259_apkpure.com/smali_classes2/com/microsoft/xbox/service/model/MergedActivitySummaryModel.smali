.class public Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "MergedActivitySummaryModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;",
        ">;"
    }
.end annotation


# static fields
.field private static HASH_TABLE_SIZE:I = 0x0

.field private static final KEY_FORMAT:Ljava/lang/String; = "%s.%d.%s.%s"

.field private static companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

.field private final mediaGroup:I

.field private final mediaId:Ljava/lang/String;

.field private final mediaType:Ljava/lang/String;

.field private parentDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

.field private runner:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;

.field private final titleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const/16 v0, 0x14

    sput v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->HASH_TABLE_SIZE:I

    .line 25
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    sget v1, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->HASH_TABLE_SIZE:I

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 2
    .param p1, "mediaId"    # Ljava/lang/String;
    .param p2, "mediaType"    # Ljava/lang/String;
    .param p3, "mediaGroup"    # I
    .param p4, "titleId"    # J

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaId:Ljava/lang/String;

    .line 40
    iput-wide p4, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->titleId:J

    .line 41
    iput p3, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaGroup:I

    .line 42
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaType:Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;-><init>(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->runner:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 21
    iget v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->mediaGroup:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->onLoadCompanionCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public static getModel(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 9
    .param p0, "mediaId"    # Ljava/lang/String;
    .param p1, "mediaType"    # Ljava/lang/String;
    .param p2, "mediaGroup"    # I

    .prologue
    const/4 v8, 0x3

    .line 47
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s.%d.%s.%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    const-string v4, ""

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "key":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 49
    .local v0, "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-nez v0, :cond_3

    .line 50
    const/4 v7, -0x1

    .line 51
    .local v7, "titleId":I
    const/4 v1, 0x5

    if-eq p2, v1, :cond_0

    const-string v1, "6D96DEDC-F3C9-43F8-89E3-0C95BF76AD2A"

    invoke-static {p0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    :cond_0
    const v7, 0x18ffc9f4

    .line 54
    :cond_1
    if-ne p2, v8, :cond_2

    .line 55
    const v7, 0x3d705025

    .line 57
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    int-to-long v4, v7

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 58
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v6, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 61
    .end local v7    # "titleId":I
    :cond_3
    return-object v0
.end method

.method public static getModel(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 7
    .param p0, "mediaId"    # Ljava/lang/String;
    .param p1, "mediaType"    # Ljava/lang/String;
    .param p2, "mediaGroup"    # I
    .param p3, "titleId"    # J

    .prologue
    .line 65
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s.%d.%s.%s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    const/4 v4, 0x3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "key":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 67
    .local v0, "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 69
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionsTable:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v6, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 72
    :cond_0
    return-object v0
.end method

.method private onLoadCompanionCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;>;"
    const v6, 0x3d705025

    .line 105
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 106
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_2

    .line 107
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    .line 111
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->titleId:J

    const-wide/32 v4, 0x3d705025

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    if-eqz v2, :cond_2

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 113
    .local v1, "companion":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getAllowedTitleIds()Ljava/util/ArrayList;

    move-result-object v0

    .line 114
    .local v0, "allowedTitleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 115
    :cond_1
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->addAllowedTitleId(I)V

    goto :goto_0

    .line 121
    .end local v0    # "allowedTitleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v1    # "companion":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_2
    return-void
.end method


# virtual methods
.method public getActivitiesList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->getDefault()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getHasActivities()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->companionList:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHasFeaturedActivity()Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 97
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->ActivitiesSummary:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->runner:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 98
    return-void
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->runner:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method
