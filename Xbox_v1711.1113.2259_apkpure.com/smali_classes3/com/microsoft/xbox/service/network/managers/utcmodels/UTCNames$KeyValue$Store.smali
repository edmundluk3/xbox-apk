.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Store;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Store"
.end annotation


# static fields
.field public static final AddOns:Ljava/lang/String; = "Add-ons"

.field public static final Apps:Ljava/lang/String; = "Apps"

.field public static final ComingSoon:Ljava/lang/String; = "ComingSoon"

.field public static final Featured:Ljava/lang/String; = "Featured"

.field public static final GamePass:Ljava/lang/String; = "Game Pass"

.field public static final GamePassAction:Ljava/lang/String; = "GamePass - Action, shooters, and RPGs"

.field public static final GamePassAll:Ljava/lang/String; = "GamePass - All games A-Z"

.field public static final GamePassAwards:Ljava/lang/String; = "GamePass - Award Winners and Critically Acclaimed"

.field public static final GamePassFamily:Ljava/lang/String; = "GamePass - Fun family games"

.field public static final GamePassFeatured:Ljava/lang/String; = "GamePass - Featured"

.field public static final GamePassIndies:Ljava/lang/String; = "GamePass - Awesome Indies from ID@Xbox"

.field public static final GamePassPuzzles:Ljava/lang/String; = "GamePass - Puzzles and platformers"

.field public static final GamePassRecent:Ljava/lang/String; = "GamePass - Recently Added"

.field public static final GamePassSports:Ljava/lang/String; = "GamePass - Sports and Racing"

.field public static final Games:Ljava/lang/String; = "Games"

.field public static final Gold:Ljava/lang/String; = "Gold"

.field public static final New:Ljava/lang/String; = "New"

.field public static final None:Ljava/lang/String; = "NotUsed"

.field public static final Popular:Ljava/lang/String; = "Popular"

.field public static final Recent:Ljava/lang/String; = "Recent"

.field public static final Release:Ljava/lang/String; = "Release"

.field public static final TopFree:Ljava/lang/String; = "TopFree"

.field public static final TopPaid:Ljava/lang/String; = "TopPaid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
