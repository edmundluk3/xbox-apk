.class abstract Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;
.super Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
.source "$AutoValue_UserPostsDataTypes_Timeline.java"


# instance fields
.field private final timelineOwner:Ljava/lang/String;

.field private final timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field private final timelineUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .param p2, "timelineOwner"    # Ljava/lang/String;
    .param p3, "timelineUri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timelineType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timelineOwner"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineOwner:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 63
    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    .line 64
    .local v0, "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineOwner:Ljava/lang/String;

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineOwner()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 66
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    :cond_4
    move v1, v2

    .line 68
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 73
    const/4 v0, 0x1

    .line 74
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 76
    mul-int/2addr v0, v2

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineOwner:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 78
    mul-int/2addr v0, v2

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 80
    return v0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public timelineOwner()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineOwner:Ljava/lang/String;

    return-object v0
.end method

.method public timelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public timelineUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Timeline{timelineType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timelineOwner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineOwner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timelineUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_Timeline;->timelineUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
