.class public Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;
.super Ljava/lang/Object;
.source "ThreadSafeHashSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private data:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation
.end field

.field private syncObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->data:Ljava/util/HashSet;

    .line 13
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->syncObject:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<TT;>;"
    .local p1, "v":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->data:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ifNotContainsAdd(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<TT;>;"
    .local p1, "v":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->syncObject:Ljava/lang/Object;

    monitor-enter v2

    .line 30
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->data:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 32
    .local v0, "rv":Z
    if-nez v0, :cond_0

    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->data:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_0
    monitor-exit v2

    return v0

    .line 37
    .end local v0    # "rv":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<TT;>;"
    .local p1, "v":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->data:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 18
    monitor-exit v1

    .line 19
    return-void

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
