.class Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;
.super Ljava/lang/Object;
.source "JsonGamePad.java"


# instance fields
.field public final buttons:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;"
        }
    .end annotation
.end field

.field public final leftThumbstickX:F

.field public final leftThumbstickY:F

.field public final leftTrigger:F

.field public final rightThumbstickX:F

.field public final rightThumbstickY:F

.field public final rightTrigger:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 26
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "buttons"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->fromInt(I)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->buttons:Ljava/util/EnumSet;

    .line 27
    const-string v1, "leftTrigger"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftTrigger:F

    .line 28
    const-string v1, "rightTrigger"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightTrigger:F

    .line 29
    const-string v1, "leftThumbstickX"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftThumbstickX:F

    .line 30
    const-string v1, "leftThumbstickY"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftThumbstickY:F

    .line 31
    const-string v1, "rightThumbstickX"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightThumbstickX:F

    .line 32
    const-string v1, "rightThumbstickY"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightThumbstickY:F

    .line 33
    return-void
.end method
