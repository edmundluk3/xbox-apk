.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;
.super Ljava/lang/Object;
.source "UTCClientError.java"


# static fields
.field public static final CLIENT_ERROR_VERSION:I = 0x1

.field static final MAXCALLSTACKLENGTH:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getShortendText(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 133
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x64

    if-le v2, v3, :cond_0

    .line 136
    const/16 v2, 0x63

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 139
    :cond_0
    const-string v1, "unknown"

    .line 141
    .local v1, "target":Ljava/lang/String;
    :try_start_0
    const-string v2, "[^a-zA-Z0-9.()]"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 145
    :goto_0
    return-object v1

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "getShortendText: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "errorMessage"    # Ljava/lang/String;
    .param p1, "errorCode"    # Ljava/lang/String;
    .param p2, "callStack"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 36
    if-nez p0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 41
    :cond_0
    :try_start_0
    new-instance v3, Lxbox/smartglass/ClientError;

    invoke-direct {v3}, Lxbox/smartglass/ClientError;-><init>()V

    .line 42
    .local v3, "error":Lxbox/smartglass/ClientError;
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->getShortendText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "errMessage":Ljava/lang/String;
    invoke-virtual {v3, v2}, Lxbox/smartglass/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 45
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v5

    invoke-virtual {v3, v5}, Lxbox/smartglass/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 48
    if-eqz p2, :cond_2

    .line 49
    invoke-static {p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->getShortendText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "stack":Ljava/lang/String;
    invoke-virtual {v3, v4}, Lxbox/smartglass/ClientError;->setCallStack(Ljava/lang/String;)V

    .line 56
    .end local v4    # "stack":Ljava/lang/String;
    :goto_1
    if-eqz p1, :cond_1

    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->getShortendText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "errCode":Ljava/lang/String;
    invoke-virtual {v3, v1}, Lxbox/smartglass/ClientError;->setErrorCode(Ljava/lang/String;)V

    .line 61
    .end local v1    # "errCode":Ljava/lang/String;
    :cond_1
    const-string v5, "ClientError:ErrorMessage %s, errorCode %s, \nCALLSTACK:\n%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    .end local v2    # "errMessage":Ljava/lang/String;
    .end local v3    # "error":Lxbox/smartglass/ClientError;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "Failed to track ClientError: %s"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "errMessage":Ljava/lang/String;
    .restart local v3    # "error":Lxbox/smartglass/ClientError;
    :cond_2
    :try_start_1
    const-string v5, "unknown"

    invoke-virtual {v3, v5}, Lxbox/smartglass/ClientError;->setCallStack(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static trackException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 9
    .param p0, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "exception"    # Ljava/lang/Exception;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 107
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 108
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 111
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;-><init>(Ljava/lang/Throwable;)V

    .line 113
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    new-instance v1, Lxbox/smartglass/ClientError;

    invoke-direct {v1}, Lxbox/smartglass/ClientError;-><init>()V

    .line 114
    .local v1, "error":Lxbox/smartglass/ClientError;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getFirstCallstackFrame()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lxbox/smartglass/ClientError;->setCallStack(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v1, p0}, Lxbox/smartglass/ClientError;->setErrorCode(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lxbox/smartglass/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 118
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 119
    const-string v3, "ClientError:trackException %s, errorCode %s, \nCALLSTACK:\n%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getErrorMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p0, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getFirstCallstackFrame()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v1    # "error":Lxbox/smartglass/ClientError;
    .end local v2    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Failed in UTCClientError::trackException: %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static trackPartyChatError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p0, "errorName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "errorCode"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "exception"    # Ljava/lang/Exception;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 95
    return-void
.end method
