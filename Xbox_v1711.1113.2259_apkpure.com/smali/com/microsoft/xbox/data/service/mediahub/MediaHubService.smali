.class public interface abstract Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
.super Ljava/lang/Object;
.source "MediaHubService.java"


# virtual methods
.method public abstract createCustomPic(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lretrofit2/Call;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "customPics/create"
    .end annotation
.end method

.method public abstract createCustomPicRx(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)Lio/reactivex/Single;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "customPics/create"
    .end annotation
.end method

.method public abstract getBroadcasts(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lio/reactivex/Single;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "broadcasts/search"
    .end annotation
.end method

.method public abstract getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "gameclips/batch"
    .end annotation
.end method

.method public abstract getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "gameclips/search"
    .end annotation
.end method

.method public abstract getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "screenshots/batch"
    .end annotation
.end method

.method public abstract getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;
    .param p1    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "screenshots/search"
    .end annotation
.end method

.method public abstract incrementGameclipViewCount(Ljava/lang/String;)Lio/reactivex/Completable;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "contentId"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "gameclips/{contentId}/views"
    .end annotation
.end method

.method public abstract incrementScreenshotViewCount(Ljava/lang/String;)Lio/reactivex/Completable;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "contentId"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "screenshots/{contentId}/views"
    .end annotation
.end method
