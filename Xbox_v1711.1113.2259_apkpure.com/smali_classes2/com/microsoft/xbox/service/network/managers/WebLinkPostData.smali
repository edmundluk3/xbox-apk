.class public Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
.super Ljava/lang/Object;
.source "WebLinkPostData.java"


# instance fields
.field public final description:Ljava/lang/String;

.field public final displayLink:Ljava/lang/String;

.field public final image:Ljava/lang/String;

.field public final link:Ljava/lang/String;

.field public final linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;

.field public final linkType:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V
    .locals 1
    .param p1, "preview"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->title:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->description:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->image:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->displayLink:Ljava/lang/String;

    .line 44
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->link:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->link:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->linkType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->Default:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->linkType:Ljava/lang/String;

    .line 46
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;

    .line 47
    return-void

    .line 45
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->linkType:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "image"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "link"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "displayLink"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "linkType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "linkData"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->description:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->title:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->image:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->link:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->displayLink:Ljava/lang/String;

    .line 33
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->Default:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->toString()Ljava/lang/String;

    move-result-object p6

    .end local p6    # "linkType":Ljava/lang/String;
    :cond_0
    iput-object p6, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->linkType:Ljava/lang/String;

    .line 34
    iput-object p7, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;->linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;

    .line 35
    return-void
.end method
