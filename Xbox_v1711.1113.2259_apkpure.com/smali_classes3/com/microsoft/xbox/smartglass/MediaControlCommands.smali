.class public final enum Lcom/microsoft/xbox/smartglass/MediaControlCommands;
.super Ljava/lang/Enum;
.source "MediaControlCommands.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Back:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum ChannelDown:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum ChannelUp:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum FastForward:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Menu:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum NextTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum PlayPauseToggle:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum PreviousTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Record:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Rewind:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Seek:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum Stop:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field public static final enum View:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "None"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Play"

    invoke-direct {v2, v3, v9, v6}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Pause"

    invoke-direct {v2, v3, v6, v7}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "PlayPauseToggle"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v8}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PlayPauseToggle:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Stop"

    const/16 v4, 0x10

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Stop:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Record"

    const/4 v4, 0x5

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Record:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "NextTrack"

    const/4 v4, 0x6

    const/16 v5, 0x40

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->NextTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "PreviousTrack"

    const/4 v4, 0x7

    const/16 v5, 0x80

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PreviousTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "FastForward"

    const/16 v4, 0x100

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->FastForward:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Rewind"

    const/16 v4, 0x9

    const/16 v5, 0x200

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Rewind:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "ChannelUp"

    const/16 v4, 0xa

    const/16 v5, 0x400

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->ChannelUp:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "ChannelDown"

    const/16 v4, 0xb

    const/16 v5, 0x800

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->ChannelDown:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Back"

    const/16 v4, 0xc

    const/16 v5, 0x1000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Back:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "View"

    const/16 v4, 0xd

    const/16 v5, 0x2000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->View:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Menu"

    const/16 v4, 0xe

    const/16 v5, 0x4000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Menu:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v3, "Seek"

    const/16 v4, 0xf

    const v5, 0x8000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Seek:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 12
    const/16 v2, 0x10

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v3, v2, v9

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v3, v2, v6

    const/4 v3, 0x3

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PlayPauseToggle:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Stop:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v3, v2, v7

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Record:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->NextTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PreviousTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->FastForward:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v3, v2, v8

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Rewind:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->ChannelUp:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->ChannelDown:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Back:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->View:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Menu:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Seek:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 47
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_lookup:Landroid/util/SparseArray;

    .line 50
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->values()[Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    move-result-object v2

    array-length v3, v2

    .local v0, "cap":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 51
    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_value:I

    .line 57
    return-void
.end method

.method public static enumValueFromInt(I)Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 73
    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 74
    .local v0, "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    if-nez v0, :cond_0

    .line 75
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 78
    :cond_0
    return-object v0
.end method

.method public static fromInt(I)Ljava/util/EnumSet;
    .locals 6
    .param p0, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    const-class v2, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 88
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->values()[Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 89
    .local v0, "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 90
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :cond_1
    return-object v1
.end method

.method public static getValue(Ljava/util/EnumSet;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "commands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    const/4 v1, 0x0

    .line 105
    .local v1, "value":I
    if-eqz p0, :cond_0

    .line 106
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 107
    .local v0, "cap":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v3

    or-int/2addr v1, v3

    .line 108
    goto :goto_0

    .line 111
    .end local v0    # "cap":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :cond_0
    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/MediaControlCommands;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->_value:I

    return v0
.end method
