.class public Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "CustomizeProfileScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private addBioButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private addLocationButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private bioEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private bioEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private changeGamertagButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private locationEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private locationEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private maxBioCount:I

.field private maxLocationCount:I

.field private profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

.field private saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    const v1, 0x7f0e04e5

    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 51
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->screenBody:Landroid/view/View;

    .line 53
    const v0, 0x7f0e04e4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/XLERootView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    .line 54
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 55
    const v0, 0x7f0e04e6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 56
    const v0, 0x7f0e04e7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 57
    const v0, 0x7f0e04e8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamertagButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 58
    const v0, 0x7f0e04e9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 59
    const v0, 0x7f0e04ea

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addLocationButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 60
    const v0, 0x7f0e04eb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 61
    const v0, 0x7f0e04ec

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->maxLocationCount:I

    .line 63
    const v0, 0x7f0e04ed

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addBioButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 64
    const v0, 0x7f0e04ee

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 65
    const v0, 0x7f0e04ef

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->maxBioCount:I

    .line 67
    const v0, 0x7f0e04f0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 68
    const v0, 0x7f0e04f1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 69
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showGamerpicPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showGamertagPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showColorPickerDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToShareContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addLocationButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showEditLocationTextDialog()V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showPrivilegeError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToShareContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showEditLocationTextDialog()V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showPrivilegeError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToShareContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addBioButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showEditBioTextDialog()V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07049f

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showPrivilegeError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToShareContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showEditBioTextDialog()V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07049f

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showPrivilegeError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$7(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->commitChanges()V

    return-void
.end method

.method static synthetic lambda$onStart$8(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->cancel()V

    return-void
.end method

.method private updateEditTextCount(Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 8
    .param p1, "count"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "maxCount"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 172
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 174
    .local v0, "length":I
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d/%d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 175
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    .line 176
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    .line 175
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 180
    .end local v0    # "length":I
    :goto_0
    return-void

    .line 178
    :cond_0
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamertagButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamertagButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addLocationButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addBioButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->showKeyboard()V

    .line 135
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamertagButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 142
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 143
    return-void
.end method

.method public updateView()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->updateView()V

    .line 74
    return-void
.end method

.method protected updateViewOverride()V
    .locals 5

    .prologue
    const v4, 0x7f020125

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentColor()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/XLERootView;->setBackgroundColor(I)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 154
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentGamerpic()Ljava/lang/String;

    move-result-object v3

    .line 153
    invoke-virtual {v0, v3, v4, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addBioButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->bioEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->maxBioCount:I

    invoke-direct {p0, v0, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->updateEditTextCount(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->addLocationButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->locationEditTextCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->maxLocationCount:I

    invoke-direct {p0, v0, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->updateEditTextCount(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 167
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeColorButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getIsLoadingProfileColorList()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->changeGamerpicButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getIsLoadingGamerpicList()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 169
    return-void

    :cond_2
    move v0, v2

    .line 167
    goto :goto_0

    :cond_3
    move v1, v2

    .line 168
    goto :goto_1
.end method
