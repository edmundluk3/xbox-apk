.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
.super Ljava/lang/Object;
.source "IPeopleSelectorControl.java"


# virtual methods
.method public abstract addPersonToSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
.end method

.method public abstract addPersonToUnSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
.end method

.method public abstract getPeopleSelectedList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSelectedPerson()Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
.end method

.method public abstract isBusy()Z
.end method

.method public abstract navigateToPeopleSelectorActivity()V
.end method

.method public abstract removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
.end method

.method public abstract removePersonFromUnselected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
.end method

.method public abstract selectionActivityCompleted(Z)V
.end method

.method public abstract setSelectedPerson(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
.end method
