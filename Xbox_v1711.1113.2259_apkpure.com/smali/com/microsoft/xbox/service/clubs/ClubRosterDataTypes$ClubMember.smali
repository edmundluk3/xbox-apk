.class public abstract Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
.super Ljava/lang/Object;
.source "ClubRosterDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubMember"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static unknownErrorWithXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 11
    .param p0, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 163
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->unknown()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    invoke-static/range {v0 .. v10}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->with(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v0

    return-object v0
.end method

.method public static with(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 12
    .param p0, "userId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "error"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "errorCode"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "errorDescription"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "followQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "followQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "memberQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "memberQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "moderatorQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "moderatorQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "roles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;

    .line 149
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    move-object v1, p0

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object v9, p2

    move-object v10, p3

    move-object/from16 v11, p4

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 147
    return-object v0
.end method


# virtual methods
.method public abstract error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract errorCode()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "code"
    .end annotation
.end method

.method public abstract errorDescription()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "description"
    .end annotation
.end method

.method public abstract followQuotaMax()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract followQuotaRemaining()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getError()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorCode()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorCode()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorDescription()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->with(ILjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v0

    goto :goto_0

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract memberQuotaMax()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract memberQuotaRemaining()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract moderatorQuotaMax()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract moderatorQuotaRemaining()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract roles()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end method

.method public abstract userId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
