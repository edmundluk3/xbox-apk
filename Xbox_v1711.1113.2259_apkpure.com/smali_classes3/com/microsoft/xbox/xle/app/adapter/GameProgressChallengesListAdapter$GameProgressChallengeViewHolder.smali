.class public Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
.super Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;
.source "GameProgressChallengesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProgressChallengeViewHolder"
.end annotation


# instance fields
.field private challengeDescriptionTextView:Landroid/widget/TextView;

.field private challengeIcon:Landroid/widget/TextView;

.field private challengeImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private challengeNameTextView:Landroid/widget/TextView;

.field private timeRemainingLabelTextView:Landroid/widget/TextView;

.field private timerTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;-><init>()V

    return-void
.end method

.method public static inflateView(Landroid/content/Context;)Landroid/view/View;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 53
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f0300ff

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;-><init>()V

    .line 56
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
    const v3, 0x7f0e0596

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 57
    const v3, 0x7f0e0598

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timeRemainingLabelTextView:Landroid/widget/TextView;

    .line 58
    const v3, 0x7f0e0599

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    .line 59
    const v3, 0x7f0e059a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeNameTextView:Landroid/widget/TextView;

    .line 60
    const v3, 0x7f0e059b

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeDescriptionTextView:Landroid/widget/TextView;

    .line 61
    const v3, 0x7f0e0597

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeIcon:Landroid/widget/TextView;

    .line 62
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 64
    return-object v1
.end method


# virtual methods
.method public updateUI(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V
    .locals 26
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .prologue
    .line 69
    if-eqz p1, :cond_3

    .line 71
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeIcon:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 73
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 74
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;

    .line 75
    .local v23, "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    const-string v6, "Gamerscore"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 77
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeIcon:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 83
    .end local v23    # "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    :cond_1
    new-instance v17, Ljava/util/Date;

    invoke-direct/range {v17 .. v17}, Ljava/util/Date;-><init>()V

    .line 84
    .local v17, "currentDate":Ljava/util/Date;
    const-wide/32 v18, 0x5265c00

    .line 85
    .local v18, "DAY_IN_MILLISECONDS":J
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long v7, v4, v10

    .line 86
    .local v7, "endTimeDiff":J
    const-wide/32 v4, 0x5265c00

    div-long v20, v7, v4

    .line 88
    .local v20, "endDateDayDiff":J
    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    cmp-long v4, v4, v10

    if-gez v4, :cond_6

    .line 90
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long v2, v4, v10

    .line 91
    .local v2, "startTimeDiff":J
    const-wide/32 v4, 0x5265c00

    div-long v24, v2, v4

    .line 92
    .local v24, "startDateDayDiff":J
    const-wide/16 v4, 0x1

    cmp-long v4, v24, v4

    if-ltz v4, :cond_4

    .line 93
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c0142

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timeRemainingLabelTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070618

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 118
    .end local v2    # "startTimeDiff":J
    .end local v24    # "startDateDayDiff":J
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeNameTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->name:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 120
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progressState:Ljava/lang/String;

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 121
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->description:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 126
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v4, :cond_3

    .line 127
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->mediaAssets:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 128
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->mediaAssets:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 129
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->mediaAssets:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;

    .line 130
    .local v22, "mediaAsset":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;
    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->type:Ljava/lang/String;

    const-string v6, "Icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 131
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual/range {v22 .. v22}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->getMediaAssetUri()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0200a5

    const v9, 0x7f0200a6

    invoke-virtual {v4, v5, v6, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 140
    .end local v7    # "endTimeDiff":J
    .end local v17    # "currentDate":Ljava/util/Date;
    .end local v18    # "DAY_IN_MILLISECONDS":J
    .end local v20    # "endDateDayDiff":J
    .end local v22    # "mediaAsset":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;
    :cond_3
    return-void

    .line 96
    .restart local v2    # "startTimeDiff":J
    .restart local v7    # "endTimeDiff":J
    .restart local v17    # "currentDate":Ljava/util/Date;
    .restart local v18    # "DAY_IN_MILLISECONDS":J
    .restart local v20    # "endDateDayDiff":J
    .restart local v24    # "startDateDayDiff":J
    :cond_4
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_5

    .line 97
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timeRemainingLabelTextView:Landroid/widget/TextView;

    move-object/from16 v11, p0

    invoke-static/range {v2 .. v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V

    .line 99
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_1

    .line 105
    .end local v2    # "startTimeDiff":J
    .end local v24    # "startDateDayDiff":J
    :cond_6
    const-wide/16 v4, 0x1

    cmp-long v4, v20, v4

    if-ltz v4, :cond_7

    .line 106
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 107
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c0142

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timeRemainingLabelTextView:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070619

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 109
    :cond_7
    const-wide/16 v4, 0x0

    cmp-long v4, v7, v4

    if-lez v4, :cond_8

    .line 110
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    const/high16 v11, -0x10000

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v16, p0

    invoke-static/range {v7 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V

    .line 112
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_4

    .line 123
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->challengeDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->lockedDescription:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method
