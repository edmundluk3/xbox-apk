.class final Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;
.super Lcom/microsoft/xbox/domain/beam/BeamChannel;
.source "AutoValue_BeamChannel.java"


# instance fields
.field private final channelId:Ljava/lang/Integer;

.field private final imageUrl:Ljava/lang/String;

.field private final subTitle:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final viewerCount:I


# direct methods
.method constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "channelId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subTitle"    # Ljava/lang/String;
    .param p4, "imageUrl"    # Ljava/lang/String;
    .param p5, "viewerCount"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    .line 24
    if-nez p2, :cond_0

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null title"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->title:Ljava/lang/String;

    .line 28
    if-nez p3, :cond_1

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null subTitle"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_1
    iput-object p3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->subTitle:Ljava/lang/String;

    .line 32
    if-nez p4, :cond_2

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null imageUrl"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_2
    iput-object p4, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->imageUrl:Ljava/lang/String;

    .line 36
    iput p5, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->viewerCount:I

    .line 37
    return-void
.end method


# virtual methods
.method public channelId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 85
    check-cast v0, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    .line 86
    .local v0, "that":Lcom/microsoft/xbox/domain/beam/BeamChannel;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->channelId()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->title:Ljava/lang/String;

    .line 87
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->title()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->subTitle:Ljava/lang/String;

    .line 88
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->subTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->imageUrl:Ljava/lang/String;

    .line 89
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->imageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->viewerCount:I

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->viewerCount()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 86
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->channelId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/domain/beam/BeamChannel;
    :cond_4
    move v1, v2

    .line 92
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 97
    const/4 v0, 0x1

    .line 98
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 100
    mul-int/2addr v0, v2

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 102
    mul-int/2addr v0, v2

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->subTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 104
    mul-int/2addr v0, v2

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->imageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 106
    mul-int/2addr v0, v2

    .line 107
    iget v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->viewerCount:I

    xor-int/2addr v0, v1

    .line 108
    return v0

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public imageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public subTitle()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->subTitle:Ljava/lang/String;

    return-object v0
.end method

.method public title()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->title:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BeamChannel{channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->channelId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->subTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", viewerCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->viewerCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public viewerCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/microsoft/xbox/domain/beam/AutoValue_BeamChannel;->viewerCount:I

    return v0
.end method
