.class public Lcom/microsoft/xbox/service/model/TrendingModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "TrendingModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

.field private categoryFilter:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field private maxItems:I

.field private queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

.field private result:Lcom/microsoft/xbox/toolkit/AsyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
            ">;"
        }
    .end annotation
.end field

.field private titleId:Ljava/lang/String;

.field private topicId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/TrendingModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "maxItems"    # I
    .param p2, "queryType"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .param p3, "categoryFilter"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    .param p4, "topicId"    # Ljava/lang/String;
    .param p5, "xuid"    # Ljava/lang/String;
    .param p6, "titleId"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 46
    iput p1, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->maxItems:I

    .line 47
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    .line 48
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->categoryFilter:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 49
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->topicId:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->xuid:Ljava/lang/String;

    .line 51
    iput-object p6, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->titleId:Ljava/lang/String;

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivityHubService()Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/TrendingModel$TrendingRunnable;-><init>(Lcom/microsoft/xbox/service/model/TrendingModel;Lcom/microsoft/xbox/service/model/TrendingModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 56
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->categoryFilter:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->queryType:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/TrendingModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->maxItems:I

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/TrendingModel;)Lcom/microsoft/xbox/service/activityHub/IActivityHubService;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->topicId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/TrendingModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TrendingModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->titleId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/TrendingModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static newGlobalInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 7
    .param p0, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p1, "categoryFilter"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 59
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 60
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 62
    new-instance v0, Lcom/microsoft/xbox/service/model/TrendingModel;

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Global:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    move v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/TrendingModel;-><init>(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newTitleInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 7
    .param p0, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p1, "categoryFilter"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 81
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 82
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 83
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 85
    new-instance v0, Lcom/microsoft/xbox/service/model/TrendingModel;

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Title:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v4, ""

    const-string v5, ""

    move v1, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/TrendingModel;-><init>(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newTopicInstance(ILjava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 7
    .param p0, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p1, "topicId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 66
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 67
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v0, Lcom/microsoft/xbox/service/model/TrendingModel;

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->Topic:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    sget-object v3, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Unknown:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v5, ""

    const-string v6, ""

    move v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/TrendingModel;-><init>(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newUserInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 7
    .param p0, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p1, "categoryFilter"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 73
    const-wide/16 v0, 0x1

    int-to-long v2, p0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 74
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 75
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v0, Lcom/microsoft/xbox/service/model/TrendingModel;

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;->User:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;

    const-string v4, ""

    const-string v6, ""

    move v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/TrendingModel;-><init>(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    goto :goto_0
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TrendingModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 102
    return-void
.end method
