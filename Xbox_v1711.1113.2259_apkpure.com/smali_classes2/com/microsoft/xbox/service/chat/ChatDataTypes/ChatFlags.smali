.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatFlags;
.super Ljava/lang/Object;
.source "ChatFlags.java"


# static fields
.field public static final CONTAINS_LINKS:I = 0x4

.field public static final DEFAULT:I = 0x0

.field public static final IS_DELETED:I = 0x1

.field public static final SERVER_ORIGINATED:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static containsFlag(II)Z
    .locals 1
    .param p0, "flags"    # I
    .param p1, "flag"    # I

    .prologue
    .line 10
    and-int v0, p0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
