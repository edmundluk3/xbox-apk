.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PeopleHubAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LeaderboardHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        ">;"
    }
.end annotation


# instance fields
.field leaderboardSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08ff
    .end annotation
.end field

.field leaderboardSummaryView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08fe
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V
    .locals 4
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .line 134
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 135
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSummaryView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030202

    .line 142
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getFilterItems()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 144
    .local v0, "leaderboardSpinnerAdapter":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 145
    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->leaderboardSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 147
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->goToDetailView()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->setFilterIndex(I)V

    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V
    .locals 0
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 152
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 126
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardHeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V

    return-void
.end method
