.class final Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;
.source "AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem.java"


# instance fields
.field private final count:I


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;-><init>()V

    .line 13
    iput p1, p0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;->count:I

    .line 14
    return-void
.end method


# virtual methods
.method count()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;->count:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    if-ne p1, p0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;

    .line 35
    .local v0, "that":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;
    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;->count:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;->count()I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;
    :cond_2
    move v1, v2

    .line 37
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x1

    .line 43
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 44
    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;->count:I

    xor-int/2addr v0, v1

    .line 45
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AndMoreItem{count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_AndMoreItem;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
