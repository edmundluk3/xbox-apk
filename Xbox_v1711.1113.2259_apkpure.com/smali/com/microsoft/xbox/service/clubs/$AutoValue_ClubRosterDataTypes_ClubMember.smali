.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;
.super Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
.source "$AutoValue_ClubRosterDataTypes_ClubMember.java"


# instance fields
.field private final error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

.field private final errorCode:Ljava/lang/Integer;

.field private final errorDescription:Ljava/lang/String;

.field private final followQuotaMax:Ljava/lang/Integer;

.field private final followQuotaRemaining:Ljava/lang/Integer;

.field private final memberQuotaMax:Ljava/lang/Integer;

.field private final memberQuotaRemaining:Ljava/lang/Integer;

.field private final moderatorQuotaMax:Ljava/lang/Integer;

.field private final moderatorQuotaRemaining:Ljava/lang/Integer;

.field private final roles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private final userId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "followQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "followQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "memberQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "memberQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "moderatorQuotaMax"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "moderatorQuotaRemaining"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "error"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "errorCode"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "errorDescription"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;-><init>()V

    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null userId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->userId:Ljava/lang/String;

    .line 41
    if-nez p2, :cond_1

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null roles"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->roles:Lcom/google/common/collect/ImmutableList;

    .line 45
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    .line 46
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    .line 47
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    .line 48
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    .line 49
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    .line 50
    iput-object p8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    .line 51
    iput-object p9, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 52
    iput-object p10, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    .line 53
    iput-object p11, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 143
    if-ne p1, p0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v1

    .line 146
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    if-eqz v3, :cond_c

    move-object v0, p1

    .line 147
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 148
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->userId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->roles:Lcom/google/common/collect/ImmutableList;

    .line 149
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    if-nez v3, :cond_3

    .line 150
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaMax()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    if-nez v3, :cond_4

    .line 151
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaRemaining()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    if-nez v3, :cond_5

    .line 152
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaMax()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    if-nez v3, :cond_6

    .line 153
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaRemaining()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    if-nez v3, :cond_7

    .line 154
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaMax()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    if-nez v3, :cond_8

    .line 155
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaRemaining()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    if-nez v3, :cond_9

    .line 156
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    if-nez v3, :cond_a

    .line 157
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorCode()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 158
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 150
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaMax()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 151
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaRemaining()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 152
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaMax()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 153
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaRemaining()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 154
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaMax()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 155
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaRemaining()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 156
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_7

    .line 157
    :cond_a
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorCode()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_8

    .line 158
    :cond_b
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_c
    move v1, v2

    .line 160
    goto/16 :goto_0
.end method

.method public error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    return-object v0
.end method

.method public errorCode()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "code"
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public errorDescription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "description"
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public followQuotaMax()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    return-object v0
.end method

.method public followQuotaRemaining()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 165
    const/4 v0, 0x1

    .line 166
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->userId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 168
    mul-int/2addr v0, v3

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->roles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 170
    mul-int/2addr v0, v3

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 172
    mul-int/2addr v0, v3

    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 174
    mul-int/2addr v0, v3

    .line 175
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 176
    mul-int/2addr v0, v3

    .line 177
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 178
    mul-int/2addr v0, v3

    .line 179
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 180
    mul-int/2addr v0, v3

    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 182
    mul-int/2addr v0, v3

    .line 183
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    xor-int/2addr v0, v1

    .line 184
    mul-int/2addr v0, v3

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    xor-int/2addr v0, v1

    .line 186
    mul-int/2addr v0, v3

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    if-nez v1, :cond_8

    :goto_8
    xor-int/2addr v0, v2

    .line 188
    return v0

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    .line 179
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    .line 181
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    .line 183
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_6

    .line 185
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    .line 187
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8
.end method

.method public memberQuotaMax()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    return-object v0
.end method

.method public memberQuotaRemaining()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    return-object v0
.end method

.method public moderatorQuotaMax()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    return-object v0
.end method

.method public moderatorQuotaRemaining()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    return-object v0
.end method

.method public roles()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->roles:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubMember{userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->roles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", followQuotaMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", followQuotaRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->followQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberQuotaMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberQuotaRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->memberQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", moderatorQuotaMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaMax:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", moderatorQuotaRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->moderatorQuotaRemaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->error:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->errorDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;->userId:Ljava/lang/String;

    return-object v0
.end method
