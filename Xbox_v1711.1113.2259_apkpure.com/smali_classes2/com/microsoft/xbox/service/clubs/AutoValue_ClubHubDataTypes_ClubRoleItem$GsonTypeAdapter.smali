.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_ClubRoleItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final actorXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final createdDateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultActorXuid:J

.field private defaultCreatedDate:Ljava/lang/String;

.field private defaultRole:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

.field private final roleAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 3
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultRole:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultActorXuid:J

    .line 25
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultCreatedDate:Ljava/lang/String;

    .line 27
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->roleAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->actorXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->createdDateAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 63
    const/4 v5, 0x0

    .line 94
    :goto_0
    return-object v5

    .line 65
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 66
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultRole:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 67
    .local v4, "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultActorXuid:J

    .line 68
    .local v2, "actorXuid":J
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultCreatedDate:Ljava/lang/String;

    .line 69
    .local v1, "createdDate":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 75
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 75
    :sswitch_0
    const-string v6, "role"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "actorXuid"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "createdDate"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    .line 77
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->roleAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    check-cast v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 78
    .restart local v4    # "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    goto :goto_1

    .line 81
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->actorXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 82
    goto :goto_1

    .line 85
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->createdDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "createdDate":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 86
    .restart local v1    # "createdDate":Ljava/lang/String;
    goto :goto_1

    .line 93
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 94
    new-instance v5, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem;

    invoke-direct {v5, v4, v2, v3, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;JLjava/lang/String;)V

    goto :goto_0

    .line 75
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1d3ad14a -> :sswitch_2
        0x358076 -> :sswitch_0
        0x162f6c6d -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultActorXuid(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultActorXuid"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultActorXuid:J

    .line 37
    return-object p0
.end method

.method public setDefaultCreatedDate(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreatedDate"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultCreatedDate:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public setDefaultRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRole"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->defaultRole:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 33
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    if-nez p2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 58
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 51
    const-string v0, "role"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->roleAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 53
    const-string v0, "actorXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->actorXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->actorXuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    const-string v0, "createdDate"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->createdDateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;)V

    return-void
.end method
