.class public interface abstract Lcom/microsoft/xbox/toolkit/MyProfileProvider;
.super Ljava/lang/Object;
.source "MyProfileProvider.java"


# virtual methods
.method public abstract getMyEmail()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getMyGamerpicUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getMyGamertag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getMyProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
