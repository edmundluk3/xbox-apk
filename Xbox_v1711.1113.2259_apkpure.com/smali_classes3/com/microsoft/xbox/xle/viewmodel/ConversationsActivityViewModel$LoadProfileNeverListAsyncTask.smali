.class Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadProfileNeverListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
    .locals 0

    .prologue
    .line 618
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$1;

    .prologue
    .line 618
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 622
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 623
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshNeverList()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadUserNeverList(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 646
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 628
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 629
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 630
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 641
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 642
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 618
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 634
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 635
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Z)Z

    .line 636
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 637
    return-void
.end method
