.class public interface abstract Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;
.super Ljava/lang/Object;
.source "StumpProtocol.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IProtocolListener"
.end annotation


# virtual methods
.method public abstract onAppChannelDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAppChannelLineupsReceived(Ljava/lang/Object;)V
.end method

.method public abstract onAppChannelProgramDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
.end method

.method public abstract onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
.end method

.method public abstract onNotifyChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onNotifyChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
.end method

.method public abstract onNotifyConfigChanged()V
.end method

.method public abstract onNotifyDeviceUIChanged(Ljava/lang/String;)V
.end method

.method public abstract onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
.end method

.method public abstract onNotifyProgramInfoChanged()V
.end method

.method public abstract onNotifyStreamError(Ljava/lang/String;)V
.end method

.method public abstract onNotifyStreamFormatChanged()V
.end method

.method public abstract onNotifyTunerStateChange(Ljava/lang/String;)V
.end method

.method public abstract onStreamingStartedReceived(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V
.end method

.method public abstract onTunerChannelsReceived([Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V
.end method

.method public abstract onTvRecentsReceived(Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;)V
.end method
