.class public interface abstract Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
.super Ljava/lang/Object;
.source "BranchSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IStreamListener"
.end annotation


# virtual methods
.method public abstract onChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
.end method

.method public abstract onStreamError(Ljava/lang/String;)V
.end method

.method public abstract onStreamFormatChanged()V
.end method

.method public abstract onStreamStarted(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V
.end method

.method public abstract onTunerStateChange(Ljava/lang/String;)V
.end method
