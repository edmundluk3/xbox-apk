.class final synthetic Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;

.field private final arg$2:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final arg$3:Ljava/lang/String;

.field private final arg$4:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

.field private final arg$5:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

.field private final arg$6:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$2:Lcom/microsoft/xbox/service/model/MessageModel;

    iput-object p3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$3:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$4:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iput-object p5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$5:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    iput-object p6, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$6:Ljava/lang/String;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/XLEObserver;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$2:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$3:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$4:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$5:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->arg$6:Ljava/lang/String;

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->lambda$processSkypeMessage$0(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method
