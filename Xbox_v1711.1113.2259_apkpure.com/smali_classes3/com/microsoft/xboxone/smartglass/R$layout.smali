.class public final Lcom/microsoft/xboxone/smartglass/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f030009

.field public static final abc_alert_dialog_material:I = 0x7f03000a

.field public static final abc_alert_dialog_title_material:I = 0x7f03000b

.field public static final abc_dialog_title_material:I = 0x7f03000c

.field public static final abc_expanded_menu_layout:I = 0x7f03000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000e

.field public static final abc_list_menu_item_icon:I = 0x7f03000f

.field public static final abc_list_menu_item_layout:I = 0x7f030010

.field public static final abc_list_menu_item_radio:I = 0x7f030011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f030012

.field public static final abc_popup_menu_item_layout:I = 0x7f030013

.field public static final abc_screen_content_include:I = 0x7f030014

.field public static final abc_screen_simple:I = 0x7f030015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030016

.field public static final abc_screen_toolbar:I = 0x7f030017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030018

.field public static final abc_search_view:I = 0x7f030019

.field public static final abc_select_dialog_material:I = 0x7f03001a

.field public static final about_activity:I = 0x7f03001b

.field public static final account_picker:I = 0x7f03001c

.field public static final account_picker_tile:I = 0x7f03001d

.field public static final account_tile:I = 0x7f03001e

.field public static final achievement_compare_button:I = 0x7f03001f

.field public static final achievement_share_button:I = 0x7f030020

.field public static final achievements_friends_who_earned_list_row:I = 0x7f030021

.field public static final activity_alert_list_item:I = 0x7f030022

.field public static final activity_alert_screen:I = 0x7f030023

.field public static final activity_feed_action_list_row:I = 0x7f030024

.field public static final activity_feed_action_list_row_spinner:I = 0x7f030025

.field public static final activity_feed_actions_comment_container:I = 0x7f030026

.field public static final activity_feed_actions_feed_item_container:I = 0x7f030027

.field public static final activity_feed_actions_screen:I = 0x7f030028

.field public static final activity_feed_add_link_preview_layout:I = 0x7f030029

.field public static final activity_feed_filters:I = 0x7f03002a

.field public static final activity_feed_share_to_feed_screen:I = 0x7f03002b

.field public static final activity_feed_status_post_screen:I = 0x7f03002c

.field public static final activity_feed_status_post_text_entry_layout:I = 0x7f03002d

.field public static final activity_feed_unshared_feed_screen:I = 0x7f03002e

.field public static final activity_overview_activity:I = 0x7f03002f

.field public static final activity_tvstreaming_activity:I = 0x7f030030

.field public static final activityfeed_image_top_gradient:I = 0x7f030031

.field public static final activityfeed_social_bar_actions_layout:I = 0x7f030032

.field public static final activityfeed_social_bar_layout:I = 0x7f030033

.field public static final activityfeed_social_bar_values_layout:I = 0x7f030034

.field public static final add_account_tile:I = 0x7f030035

.field public static final airing_list_row:I = 0x7f030036

.field public static final album_details_screen:I = 0x7f030037

.field public static final appdetail_header_screen:I = 0x7f030038

.field public static final appdetail_overview_screen:I = 0x7f030039

.field public static final appdetail_overview_screen_common:I = 0x7f03003a

.field public static final artist_detail_header:I = 0x7f03003b

.field public static final artistdetail_album_list_row:I = 0x7f03003c

.field public static final artistdetail_albums_screen:I = 0x7f03003d

.field public static final artistdetail_biography_screen:I = 0x7f03003e

.field public static final artistdetail_topsongs_screen:I = 0x7f03003f

.field public static final attachment_loading:I = 0x7f030040

.field public static final attachment_loading_error:I = 0x7f030041

.field public static final attainment_detail_header_data:I = 0x7f030042

.field public static final attainment_detail_screen:I = 0x7f030043

.field public static final base_screen:I = 0x7f030044

.field public static final blocking_dialog:I = 0x7f030045

.field public static final bundles_list_row:I = 0x7f030046

.field public static final bundles_screen:I = 0x7f030047

.field public static final button_pin:I = 0x7f030048

.field public static final cancellable_blocking_dialog:I = 0x7f030049

.field public static final canvas:I = 0x7f03004a

.field public static final canvas_webview_activity:I = 0x7f03004b

.field public static final change_friendship_dialog:I = 0x7f03004c

.field public static final change_friendship_dialog_header:I = 0x7f03004d

.field public static final change_friendship_remove_button:I = 0x7f03004e

.field public static final change_gamertag_dialog:I = 0x7f03004f

.field public static final choose_gamerpic_dialog:I = 0x7f030050

.field public static final choose_gamerpic_item:I = 0x7f030051

.field public static final choose_gamertag_suggestion_item:I = 0x7f030052

.field public static final choose_profile_color_dialog:I = 0x7f030053

.field public static final choose_profile_color_item:I = 0x7f030054

.field public static final club_admin_banned_row:I = 0x7f030055

.field public static final club_admin_banned_screen:I = 0x7f030056

.field public static final club_admin_home_screen:I = 0x7f030057

.field public static final club_admin_members_controls:I = 0x7f030058

.field public static final club_admin_members_row:I = 0x7f030059

.field public static final club_admin_members_screen:I = 0x7f03005a

.field public static final club_admin_report_base_row:I = 0x7f03005b

.field public static final club_admin_report_comment_row:I = 0x7f03005c

.field public static final club_admin_report_controls:I = 0x7f03005d

.field public static final club_admin_report_feed_row:I = 0x7f03005e

.field public static final club_admin_report_message_row:I = 0x7f03005f

.field public static final club_admin_reports_screen:I = 0x7f030060

.field public static final club_admin_request_header:I = 0x7f030061

.field public static final club_admin_request_row:I = 0x7f030062

.field public static final club_admin_requests_screen:I = 0x7f030063

.field public static final club_admin_settings_screen:I = 0x7f030064

.field public static final club_admin_switch_panel_screen:I = 0x7f030065

.field public static final club_background_achievement_row:I = 0x7f030066

.field public static final club_cant_view_screen:I = 0x7f030067

.field public static final club_card:I = 0x7f030068

.field public static final club_card_category:I = 0x7f030069

.field public static final club_chat_direct_mention_row:I = 0x7f03006a

.field public static final club_chat_edit_report_dialog:I = 0x7f03006b

.field public static final club_chat_menu:I = 0x7f03006c

.field public static final club_chat_messageoftheday_dialog:I = 0x7f03006d

.field public static final club_chat_notification_screen:I = 0x7f03006e

.field public static final club_chat_row:I = 0x7f03006f

.field public static final club_chat_row_status:I = 0x7f030070

.field public static final club_chat_screen:I = 0x7f030071

.field public static final club_chat_settings_controls:I = 0x7f030072

.field public static final club_create_confirmation:I = 0x7f030073

.field public static final club_create_container:I = 0x7f030074

.field public static final club_create_name_club:I = 0x7f030075

.field public static final club_create_name_club_dialog:I = 0x7f030076

.field public static final club_create_select_type:I = 0x7f030077

.field public static final club_create_type:I = 0x7f030078

.field public static final club_customize_background_achievement:I = 0x7f030079

.field public static final club_customize_background_achievement_dialog:I = 0x7f03007a

.field public static final club_customize_background_screenshot:I = 0x7f03007b

.field public static final club_customize_club_background:I = 0x7f03007c

.field public static final club_customize_home:I = 0x7f03007d

.field public static final club_customize_image_row:I = 0x7f03007e

.field public static final club_customize_pic_dialog:I = 0x7f03007f

.field public static final club_customize_tag_dialog:I = 0x7f030080

.field public static final club_discovery_screen:I = 0x7f030081

.field public static final club_discovery_switch_panel_screen:I = 0x7f030082

.field public static final club_feed_screen:I = 0x7f030083

.field public static final club_gradient_overlay_simple:I = 0x7f030084

.field public static final club_home_player_icon:I = 0x7f030085

.field public static final club_home_screen:I = 0x7f030086

.field public static final club_image_overlay_simple:I = 0x7f030087

.field public static final club_invitations_screen:I = 0x7f030088

.field public static final club_invitations_switch_panel_screen:I = 0x7f030089

.field public static final club_invite:I = 0x7f03008a

.field public static final club_invite_row:I = 0x7f03008b

.field public static final club_invite_screen:I = 0x7f03008c

.field public static final club_member_item:I = 0x7f03008d

.field public static final club_mod_picker_dialog:I = 0x7f03008e

.field public static final club_mod_picker_row:I = 0x7f03008f

.field public static final club_picker_dialog:I = 0x7f030090

.field public static final club_picker_item:I = 0x7f030091

.field public static final club_pivot_screen:I = 0x7f030092

.field public static final club_play_error_screen:I = 0x7f030093

.field public static final club_play_no_content_screen:I = 0x7f030094

.field public static final club_play_screen:I = 0x7f030095

.field public static final club_play_switch_panel_screen:I = 0x7f030096

.field public static final club_recommendation_screen:I = 0x7f030097

.field public static final club_recommendation_switch_panel_screen:I = 0x7f030098

.field public static final club_search_screen:I = 0x7f030099

.field public static final club_search_switch_panel_screen:I = 0x7f03009a

.field public static final club_watch_content_screen:I = 0x7f03009b

.field public static final club_watch_error_screen:I = 0x7f03009c

.field public static final club_watch_no_content_screen:I = 0x7f03009d

.field public static final club_whos_here_row:I = 0x7f03009e

.field public static final club_whos_here_screen:I = 0x7f03009f

.field public static final com_facebook_activity_layout:I = 0x7f0300a0

.field public static final com_facebook_device_auth_dialog_fragment:I = 0x7f0300a1

.field public static final com_facebook_login_fragment:I = 0x7f0300a2

.field public static final com_facebook_smart_device_dialog_fragment:I = 0x7f0300a3

.field public static final com_facebook_tooltip_bubble:I = 0x7f0300a4

.field public static final companion_override_dialog:I = 0x7f0300a5

.field public static final companiondetail_header_screen:I = 0x7f0300a6

.field public static final comparison_list_header_imagebar:I = 0x7f0300a7

.field public static final compose_message_box_with_send_button:I = 0x7f0300a8

.field public static final composemessage_activity:I = 0x7f0300a9

.field public static final composemessage_activity_header:I = 0x7f0300aa

.field public static final composemessagewithattachement_activity:I = 0x7f0300ab

.field public static final connect_dialog:I = 0x7f0300ac

.field public static final connect_dialog_new_console:I = 0x7f0300ad

.field public static final connect_dialog_picker:I = 0x7f0300ae

.field public static final connect_dialog_row:I = 0x7f0300af

.field public static final connect_dialog_row_expanded_section:I = 0x7f0300b0

.field public static final connect_dialog_row_footer:I = 0x7f0300b1

.field public static final connect_dialog_row_no_console:I = 0x7f0300b2

.field public static final connect_to_button:I = 0x7f0300b3

.field public static final console_connection_screen:I = 0x7f0300b4

.field public static final console_management_dialog:I = 0x7f0300b5

.field public static final consolebar:I = 0x7f0300b6

.field public static final consolebar_expanded:I = 0x7f0300b7

.field public static final conversation_detail_activity:I = 0x7f0300b8

.field public static final conversation_details_header_icon:I = 0x7f0300b9

.field public static final conversation_details_header_menu:I = 0x7f0300ba

.field public static final conversation_details_list_row:I = 0x7f0300bb

.field public static final conversation_details_list_row_attachment:I = 0x7f0300bc

.field public static final conversation_details_report_screen:I = 0x7f0300bd

.field public static final conversation_details_row_other:I = 0x7f0300be

.field public static final conversation_details_row_self:I = 0x7f0300bf

.field public static final conversation_details_row_status_info:I = 0x7f0300c0

.field public static final conversation_details_xbox360_attachment:I = 0x7f0300c1

.field public static final conversations_activity:I = 0x7f0300c2

.field public static final conversations_header_icon:I = 0x7f0300c3

.field public static final conversations_list_item:I = 0x7f0300c4

.field public static final conversations_selector_activity:I = 0x7f0300c5

.field public static final conversations_selector_header:I = 0x7f0300c6

.field public static final conversations_selector_list_item:I = 0x7f0300c7

.field public static final conversations_view_all_members:I = 0x7f0300c8

.field public static final conversations_view_all_members_item:I = 0x7f0300c9

.field public static final crop_image_activity:I = 0x7f0300ca

.field public static final crop_image_view:I = 0x7f0300cb

.field public static final customize_profile_screen:I = 0x7f0300cc

.field public static final design_bottom_sheet_dialog:I = 0x7f0300cd

.field public static final design_layout_snackbar:I = 0x7f0300ce

.field public static final design_layout_snackbar_include:I = 0x7f0300cf

.field public static final design_layout_tab_icon:I = 0x7f0300d0

.field public static final design_layout_tab_text:I = 0x7f0300d1

.field public static final design_menu_item_action_area:I = 0x7f0300d2

.field public static final design_navigation_item:I = 0x7f0300d3

.field public static final design_navigation_item_header:I = 0x7f0300d4

.field public static final design_navigation_item_separator:I = 0x7f0300d5

.field public static final design_navigation_item_subheader:I = 0x7f0300d6

.field public static final design_navigation_menu:I = 0x7f0300d7

.field public static final design_navigation_menu_item:I = 0x7f0300d8

.field public static final design_text_input_password_icon:I = 0x7f0300d9

.field public static final details_more_or_less_layout:I = 0x7f0300da

.field public static final details_pivot_activity:I = 0x7f0300db

.field public static final details_providers_grid_row:I = 0x7f0300dc

.field public static final details_related_activity:I = 0x7f0300dd

.field public static final details_related_activity_content:I = 0x7f0300de

.field public static final dev_loading_view:I = 0x7f0300df

.field public static final discover_grid_item2:I = 0x7f0300e0

.field public static final dlc_detail_header:I = 0x7f0300e1

.field public static final dlc_overview_purchase_screen:I = 0x7f0300e2

.field public static final dlc_overview_screen:I = 0x7f0300e3

.field public static final dlc_overview_screen_common:I = 0x7f0300e4

.field public static final drawer:I = 0x7f0300e5

.field public static final edit_profile_text_dialog:I = 0x7f0300e6

.field public static final edit_real_name_dialog:I = 0x7f0300e7

.field public static final edit_view_fixed_length:I = 0x7f0300e8

.field public static final enforcement_screen:I = 0x7f0300e9

.field public static final epg_appchannel_header_view:I = 0x7f0300ea

.field public static final epg_appchannel_item_view:I = 0x7f0300eb

.field public static final epg_appchannel_view:I = 0x7f0300ec

.field public static final epg_channel_header_view:I = 0x7f0300ed

.field public static final epg_header_day_view:I = 0x7f0300ee

.field public static final epg_header_time_view:I = 0x7f0300ef

.field public static final epg_inline_details_view:I = 0x7f0300f0

.field public static final epg_inpage_progress_bar_phone_impl:I = 0x7f0300f1

.field public static final epg_inpage_progress_bar_tablet_impl:I = 0x7f0300f2

.field public static final epg_placeholder_view:I = 0x7f0300f3

.field public static final epg_program_view:I = 0x7f0300f4

.field public static final epg_recent_channels_item_view:I = 0x7f0300f5

.field public static final epg_recent_channels_view:I = 0x7f0300f6

.field public static final epg_scroll_accelerator_view:I = 0x7f0300f7

.field public static final epg_too_many_favorites_overlay:I = 0x7f0300f8

.field public static final error_screen_with_refresh:I = 0x7f0300f9

.field public static final esrb_rating_descriptor_item:I = 0x7f0300fa

.field public static final esrb_rating_disclaimer_item:I = 0x7f0300fb

.field public static final exo_playback_control_view:I = 0x7f0300fc

.field public static final exo_simple_player_view:I = 0x7f0300fd

.field public static final exo_video_player_activity:I = 0x7f0300fe

.field public static final featured_challenge_tile_layout:I = 0x7f0300ff

.field public static final featured_landing_screen:I = 0x7f030100

.field public static final featured_list_item:I = 0x7f030101

.field public static final feedback_dialog:I = 0x7f030102

.field public static final feedback_form_dialog:I = 0x7f030103

.field public static final font_star_rating:I = 0x7f030104

.field public static final fps_view:I = 0x7f030105

.field public static final friend_finder_confirm_dialog:I = 0x7f030106

.field public static final friendfinder_facebook_login_behavior_dialog:I = 0x7f030107

.field public static final friends_list_header:I = 0x7f030108

.field public static final friends_picker_activity:I = 0x7f030109

.field public static final friends_selector_row:I = 0x7f03010a

.field public static final friends_who_play_list_item:I = 0x7f03010b

.field public static final friends_who_play_screen_layout:I = 0x7f03010c

.field public static final game_achievement_comparison_list_item:I = 0x7f03010d

.field public static final game_achievement_comparison_screen:I = 0x7f03010e

.field public static final game_available_on_icon_layout:I = 0x7f03010f

.field public static final game_desktop_system_requirement:I = 0x7f030110

.field public static final game_detail_header:I = 0x7f030111

.field public static final game_details_common:I = 0x7f030112

.field public static final game_details_progress_phone_screen:I = 0x7f030113

.field public static final game_gallery_list_row:I = 0x7f030114

.field public static final game_gallery_screen:I = 0x7f030115

.field public static final game_overview_screen:I = 0x7f030116

.field public static final game_streaming_network_test_screen:I = 0x7f030117

.field public static final game_xbox_play_anywhere_icon:I = 0x7f030118

.field public static final gameprofile_achievements_screen_achievement_header_row:I = 0x7f030119

.field public static final gameprofile_achievements_screen_leaderboard_header_row:I = 0x7f03011a

.field public static final gameprofile_achievements_screen_with_list:I = 0x7f03011b

.field public static final gameprofile_capture_list_item_data:I = 0x7f03011c

.field public static final gameprofile_captures_screen:I = 0x7f03011d

.field public static final gameprofile_club_membership_section:I = 0x7f03011e

.field public static final gameprofile_friends_screen:I = 0x7f03011f

.field public static final gameprofile_friends_who_play_list_item:I = 0x7f030120

.field public static final gameprofile_gamehub_button:I = 0x7f030121

.field public static final gameprofile_info_follow_button:I = 0x7f030122

.field public static final gameprofile_info_play_button:I = 0x7f030123

.field public static final gameprofile_info_screen:I = 0x7f030124

.field public static final gameprofile_info_view_in_store_button:I = 0x7f030125

.field public static final gameprofile_lfg_screen:I = 0x7f030126

.field public static final gameprofile_pivot_screen:I = 0x7f030127

.field public static final gameprofile_spotlight_section_item:I = 0x7f030128

.field public static final gameprofile_statistics_list_item:I = 0x7f030129

.field public static final gameprogress_achievements_list_row_locked:I = 0x7f03012a

.field public static final gameprogress_achievements_list_row_normal:I = 0x7f03012b

.field public static final gameprogress_challenges_screen:I = 0x7f03012c

.field public static final gameprogress_gameclips_screen:I = 0x7f03012d

.field public static final gamerprofile_achievements_screen_no_data:I = 0x7f03012e

.field public static final gamerscore_leaderboard_list_item:I = 0x7f03012f

.field public static final gamerscore_leaderboard_screen:I = 0x7f030130

.field public static final gamertag_search_flyout:I = 0x7f030131

.field public static final generic_error_screen:I = 0x7f030132

.field public static final generic_loading_screen:I = 0x7f030133

.field public static final generic_no_content_screen:I = 0x7f030134

.field public static final header_row_with_count:I = 0x7f030135

.field public static final home_screen:I = 0x7f030136

.field public static final home_screen_activity_feed_empty_row:I = 0x7f030137

.field public static final home_screen_activity_feed_error_row:I = 0x7f030138

.field public static final home_screen_activity_feed_loading_row:I = 0x7f030139

.field public static final home_screen_activity_feed_recommendation:I = 0x7f03013a

.field public static final home_screen_activity_feed_trending_container:I = 0x7f03013b

.field public static final home_screen_activity_feed_warning_row:I = 0x7f03013c

.field public static final home_screen_grid_cell:I = 0x7f03013d

.field public static final home_screen_grid_cell_featured:I = 0x7f03013e

.field public static final home_screen_grid_cell_more:I = 0x7f03013f

.field public static final home_screen_grid_cell_snap:I = 0x7f030140

.field public static final home_screen_popup:I = 0x7f030141

.field public static final home_screen_popup_cmd:I = 0x7f030142

.field public static final home_screen_section:I = 0x7f030143

.field public static final home_screen_section_bat_full_app:I = 0x7f030144

.field public static final home_screen_section_bat_full_game:I = 0x7f030145

.field public static final home_screen_section_bat_full_no_stats:I = 0x7f030146

.field public static final home_screen_section_bat_full_promo:I = 0x7f030147

.field public static final home_screen_section_featured:I = 0x7f030148

.field public static final home_screen_section_grid:I = 0x7f030149

.field public static final home_screen_section_now_playing:I = 0x7f03014a

.field public static final home_screen_section_now_playing_recents_promo:I = 0x7f03014b

.field public static final hover_chat_menu_screen:I = 0x7f03014c

.field public static final iconfont_ring_btn_view:I = 0x7f03014d

.field public static final iconfont_toggle_btn_view:I = 0x7f03014e

.field public static final image_gradient_overlay:I = 0x7f03014f

.field public static final image_top_gradient_overlay:I = 0x7f030150

.field public static final image_top_gradient_overlay_simple:I = 0x7f030151

.field public static final imageviewer_content:I = 0x7f030152

.field public static final imageviwerscreen:I = 0x7f030153

.field public static final include_tv_mychannels_screen_firstrun:I = 0x7f030154

.field public static final included_content_list_row:I = 0x7f030155

.field public static final included_content_screen:I = 0x7f030156

.field public static final install_button:I = 0x7f030157

.field public static final lfg_create_details_screen:I = 0x7f030158

.field public static final lfg_details_screen:I = 0x7f030159

.field public static final lfg_interested_row:I = 0x7f03015a

.field public static final lfg_list_detail_header_data:I = 0x7f03015b

.field public static final lfg_list_header_data:I = 0x7f03015c

.field public static final lfg_list_row:I = 0x7f03015d

.field public static final lfg_member_row:I = 0x7f03015e

.field public static final lfg_nodata_row:I = 0x7f03015f

.field public static final lfg_vetting_screen:I = 0x7f030160

.field public static final lfg_view_all_details_dialog:I = 0x7f030161

.field public static final load_more_listview_footer:I = 0x7f030162

.field public static final login_activity:I = 0x7f030163

.field public static final main_activity:I = 0x7f03028e

.field public static final main_activity_normal:I = 0x7f030164

.field public static final main_activity_xlarge:I = 0x7f030165

.field public static final media_buttons:I = 0x7f030166

.field public static final media_extras_list_row:I = 0x7f030167

.field public static final media_extras_list_row_with_margin:I = 0x7f030168

.field public static final media_extras_screen:I = 0x7f030169

.field public static final media_futureshowtimes_list_row:I = 0x7f03016a

.field public static final media_futureshowtimes_screen:I = 0x7f03016b

.field public static final media_item_list_row:I = 0x7f03016c

.field public static final media_progress_bar:I = 0x7f03016d

.field public static final messenger_button_send_blue_large:I = 0x7f03016e

.field public static final messenger_button_send_blue_round:I = 0x7f03016f

.field public static final messenger_button_send_blue_small:I = 0x7f030170

.field public static final messenger_button_send_white_large:I = 0x7f030171

.field public static final messenger_button_send_white_round:I = 0x7f030172

.field public static final messenger_button_send_white_small:I = 0x7f030173

.field public static final metacritic_rating_view:I = 0x7f030174

.field public static final movie_castcrew_list_row:I = 0x7f030175

.field public static final movie_castcrew_screen:I = 0x7f030176

.field public static final movie_details_header:I = 0x7f030177

.field public static final movie_overview_screen:I = 0x7f030178

.field public static final movie_overview_screen_common:I = 0x7f030179

.field public static final mru_row_full_app:I = 0x7f03017a

.field public static final mru_row_full_game:I = 0x7f03017b

.field public static final mru_row_full_no_stats:I = 0x7f03017c

.field public static final mru_row_full_promo:I = 0x7f03017d

.field public static final mru_row_recent:I = 0x7f03017e

.field public static final mru_row_snap:I = 0x7f03017f

.field public static final msaauthenticatorview:I = 0x7f030180

.field public static final my_clubs_screen:I = 0x7f030181

.field public static final my_clubs_screen_empty:I = 0x7f030182

.field public static final notification:I = 0x7f030183

.field public static final notification_action:I = 0x7f030184

.field public static final notification_action_tombstone:I = 0x7f030185

.field public static final notification_media_action:I = 0x7f030186

.field public static final notification_media_cancel_action:I = 0x7f030187

.field public static final notification_template_big_media:I = 0x7f030188

.field public static final notification_template_big_media_custom:I = 0x7f030189

.field public static final notification_template_big_media_narrow:I = 0x7f03018a

.field public static final notification_template_big_media_narrow_custom:I = 0x7f03018b

.field public static final notification_template_custom_big:I = 0x7f03018c

.field public static final notification_template_icon_group:I = 0x7f03018d

.field public static final notification_template_lines_media:I = 0x7f03018e

.field public static final notification_template_media:I = 0x7f03018f

.field public static final notification_template_media_custom:I = 0x7f030190

.field public static final notification_template_part_chronometer:I = 0x7f030191

.field public static final notification_template_part_time:I = 0x7f030192

.field public static final nowplaying_genericapp:I = 0x7f030193

.field public static final nowplaying_ie:I = 0x7f030194

.field public static final nowplaying_ie_controls:I = 0x7f030195

.field public static final nowplaying_media:I = 0x7f030196

.field public static final nowplaying_media_controls:I = 0x7f030197

.field public static final nowplaying_oneguide_button:I = 0x7f030198

.field public static final nowplaying_stream_tv_button:I = 0x7f030199

.field public static final nowplaying_title_panel:I = 0x7f03019a

.field public static final oobe_screen:I = 0x7f03019b

.field public static final pageuser_info_screen:I = 0x7f03019c

.field public static final pageuser_pivot_screen:I = 0x7f03019d

.field public static final parentitem_list_row:I = 0x7f03019e

.field public static final parentitem_screen:I = 0x7f03019f

.field public static final party_and_lfg_screen:I = 0x7f0301a0

.field public static final party_details_header:I = 0x7f0301a1

.field public static final party_details_screen:I = 0x7f0301a2

.field public static final party_list_row:I = 0x7f0301a3

.field public static final party_member_full:I = 0x7f0301a4

.field public static final party_member_icon:I = 0x7f0301a5

.field public static final party_text_row:I = 0x7f0301a6

.field public static final party_text_screen:I = 0x7f0301a7

.field public static final pegi_rating_descriptor_item:I = 0x7f0301a8

.field public static final people_activityfeed_leaderboard:I = 0x7f0301a9

.field public static final people_activityfeed_lfg:I = 0x7f0301aa

.field public static final people_activityfeed_list_row:I = 0x7f0301ab

.field public static final people_activityfeed_list_row_attachment:I = 0x7f0301ac

.field public static final people_activityfeed_list_row_entry_text:I = 0x7f0301ad

.field public static final people_activityfeed_list_row_hidden:I = 0x7f0301ae

.field public static final people_activityfeed_list_row_metadata:I = 0x7f0301af

.field public static final people_activityfeed_list_row_shared:I = 0x7f0301b0

.field public static final people_activityfeed_list_row_trending_content:I = 0x7f0301b1

.field public static final people_activityfeed_screen:I = 0x7f0301b2

.field public static final people_list_club_item:I = 0x7f0301b3

.field public static final people_list_discover_clubs_item:I = 0x7f0301b4

.field public static final people_list_dummy_item:I = 0x7f0301b5

.field public static final people_list_person_item:I = 0x7f0301b6

.field public static final people_list_seeall_button:I = 0x7f0301b7

.field public static final people_screen:I = 0x7f0301b8

.field public static final peoplefeed_screen_activity_feed_loading_row:I = 0x7f0301b9

.field public static final peoplehub_achievement_header:I = 0x7f0301ba

.field public static final peoplehub_achievements_error_row:I = 0x7f0301bb

.field public static final peoplehub_achievements_header_row:I = 0x7f0301bc

.field public static final peoplehub_achievements_leaderboard_header_row:I = 0x7f0301bd

.field public static final peoplehub_achievements_leaderboard_list_row:I = 0x7f0301be

.field public static final peoplehub_achievements_list_row:I = 0x7f0301bf

.field public static final peoplehub_achievements_loading_row:I = 0x7f0301c0

.field public static final peoplehub_achievements_screen:I = 0x7f0301c1

.field public static final peoplehub_achievements_warning_row:I = 0x7f0301c2

.field public static final peoplehub_activity:I = 0x7f0301c3

.field public static final peoplehub_feed_screen:I = 0x7f0301c4

.field public static final peoplehub_info_screen:I = 0x7f0301c5

.field public static final peoplehub_info_watermark:I = 0x7f0301c6

.field public static final peoplehub_pivot_screen:I = 0x7f0301c7

.field public static final peoplehub_social_game_row:I = 0x7f0301c8

.field public static final peoplehub_social_person_row:I = 0x7f0301c9

.field public static final peoplehub_social_screen:I = 0x7f0301ca

.field public static final phone_contact_add_phone_number_dialog:I = 0x7f0301cb

.field public static final phone_contact_change_region_dialog:I = 0x7f0301cc

.field public static final phone_contact_finder_dialog:I = 0x7f0301cd

.field public static final phone_contact_invite_friends_dialog:I = 0x7f0301ce

.field public static final phone_contact_selector_row:I = 0x7f0301cf

.field public static final phone_contact_verify_phone_dialog:I = 0x7f0301d0

.field public static final phone_purchase_button:I = 0x7f0301d1

.field public static final phone_send_contacts_invitation_item:I = 0x7f0301d2

.field public static final pins_screen:I = 0x7f0301d3

.field public static final playto_button:I = 0x7f0301d4

.field public static final popular_now_list_row:I = 0x7f0301d5

.field public static final popular_with_friends_list_row:I = 0x7f0301d6

.field public static final popular_with_friends_screen:I = 0x7f0301d7

.field public static final profile_showcase_grid_item:I = 0x7f0301d8

.field public static final progress_dialog:I = 0x7f0301d9

.field public static final provider_header_row:I = 0x7f0301da

.field public static final provider_list_row:I = 0x7f0301db

.field public static final provider_picker:I = 0x7f0301dc

.field public static final purchase_button:I = 0x7f0301dd

.field public static final purchase_result_dialog:I = 0x7f0301de

.field public static final purchasewebview_activity:I = 0x7f0301df

.field public static final rating_level_and_descriptors:I = 0x7f0301e0

.field public static final rating_level_and_descriptors_pegi:I = 0x7f0301e1

.field public static final recent_list_row:I = 0x7f0301e2

.field public static final recents_screen:I = 0x7f0301e3

.field public static final recordthat_button_component:I = 0x7f0301e4

.field public static final redbox_item_frame:I = 0x7f0301e5

.field public static final redbox_item_title:I = 0x7f0301e6

.field public static final redbox_view:I = 0x7f0301e7

.field public static final remote_control:I = 0x7f0301e8

.field public static final remote_tabs_layout:I = 0x7f0301e9

.field public static final rename_conversation_dialog:I = 0x7f0301ea

.field public static final reward_item_layout:I = 0x7f0301eb

.field public static final rotten_tomatoes_rating_view:I = 0x7f0301ec

.field public static final search_bar_layout:I = 0x7f0301ed

.field public static final search_filters_activity:I = 0x7f0301ee

.field public static final search_filters_list_row:I = 0x7f0301ef

.field public static final search_flyout:I = 0x7f0301f0

.field public static final search_results_activity:I = 0x7f0301f1

.field public static final section_separator:I = 0x7f0301f2

.field public static final select_dialog_item_material:I = 0x7f0301f3

.field public static final select_dialog_multichoice_material:I = 0x7f0301f4

.field public static final select_dialog_singlechoice_material:I = 0x7f0301f5

.field public static final settings_facebook_button:I = 0x7f0301f6

.field public static final settings_facebook_secondary_button:I = 0x7f0301f7

.field public static final settings_general_page:I = 0x7f0301f8

.field public static final settings_language_page:I = 0x7f0301f9

.field public static final settings_notifications_page:I = 0x7f0301fa

.field public static final settings_phone_contacts_finder:I = 0x7f0301fb

.field public static final settings_pivot_screen:I = 0x7f0301fc

.field public static final settings_screen_column_about:I = 0x7f0301fd

.field public static final settings_screen_column_stayawake:I = 0x7f0301fe

.field public static final settings_screen_edit_and_share_real_name:I = 0x7f0301ff

.field public static final settings_screen_home:I = 0x7f030200

.field public static final settings_signout_button:I = 0x7f030201

.field public static final sg_simple_spinner_item:I = 0x7f030202

.field public static final sg_urc_power_view_line:I = 0x7f030203

.field public static final sg_urc_view_control:I = 0x7f030204

.field public static final share_decision_dialog:I = 0x7f030205

.field public static final sign_out_custom_content_view:I = 0x7f030206

.field public static final simple_spinner_item_dark:I = 0x7f030207

.field public static final smartglass_play:I = 0x7f030208

.field public static final song_detail_list_row:I = 0x7f030209

.field public static final spinner_item_dropdown:I = 0x7f03020a

.field public static final spinner_item_dropdown_right:I = 0x7f03020b

.field public static final star_rating_dialog:I = 0x7f03020c

.field public static final star_rating_gray_with_user_count:I = 0x7f03020d

.field public static final star_rating_with_user_count:I = 0x7f03020e

.field public static final static_page:I = 0x7f03020f

.field public static final static_page_with_buttons:I = 0x7f030210

.field public static final store_addons_screen:I = 0x7f030211

.field public static final store_apps_screen:I = 0x7f030212

.field public static final store_gamepass_screen:I = 0x7f030213

.field public static final store_games_screen:I = 0x7f030214

.field public static final store_gold_screen:I = 0x7f030215

.field public static final store_item:I = 0x7f030216

.field public static final store_item_apps:I = 0x7f030217

.field public static final store_item_gamepass:I = 0x7f030218

.field public static final store_item_gold:I = 0x7f030219

.field public static final store_pivot_screen:I = 0x7f03021a

.field public static final store_redeem_button:I = 0x7f03021b

.field public static final store_search_button:I = 0x7f03021c

.field public static final suggestions_filter_spinner:I = 0x7f03021d

.field public static final suggestions_invalid_state_text_row:I = 0x7f03021e

.field public static final suggestions_people_list_item:I = 0x7f03021f

.field public static final suggestions_people_screen:I = 0x7f030220

.field public static final support_simple_spinner_dropdown_item:I = 0x7f030221

.field public static final switch_pane_with_refresh_layout:I = 0x7f030222

.field public static final switch_panel_screen:I = 0x7f030223

.field public static final tab_indicator:I = 0x7f030224

.field public static final tag_header:I = 0x7f030225

.field public static final tag_item:I = 0x7f030226

.field public static final tag_picker_dialog:I = 0x7f030227

.field public static final tag_system_tag_selector:I = 0x7f030228

.field public static final textentry:I = 0x7f030229

.field public static final textinput_control:I = 0x7f03022a

.field public static final third_party_notice_screen:I = 0x7f03022b

.field public static final three_button_dialog:I = 0x7f03022c

.field public static final timezone_spinner_item:I = 0x7f03022d

.field public static final timezone_spinner_item_dropdown:I = 0x7f03022e

.field public static final title_bar:I = 0x7f03022f

.field public static final title_leaderboard_list_row:I = 0x7f030230

.field public static final title_leaderboard_screen:I = 0x7f030231

.field public static final title_leaderboard_screen_imagebar:I = 0x7f030232

.field public static final title_picker_row:I = 0x7f030233

.field public static final title_picker_screen:I = 0x7f030234

.field public static final tournament_details_screen:I = 0x7f030235

.field public static final trending_beam_channel_item:I = 0x7f030236

.field public static final trending_beam_screen:I = 0x7f030237

.field public static final trending_pivot_screen:I = 0x7f030238

.field public static final trending_topic_header:I = 0x7f030239

.field public static final trending_topic_item:I = 0x7f03023a

.field public static final trending_topic_screen:I = 0x7f03023b

.field public static final trending_xbl_screen:I = 0x7f03023c

.field public static final tutorial_screen:I = 0x7f03023d

.field public static final tv_channel_item:I = 0x7f03023e

.field public static final tv_common_boxart_list_row:I = 0x7f03023f

.field public static final tv_epg_header:I = 0x7f030240

.field public static final tv_episode_overview_screen:I = 0x7f030241

.field public static final tv_episode_overview_screen_common:I = 0x7f030242

.field public static final tv_error_dialog:I = 0x7f030243

.field public static final tv_listings_screen:I = 0x7f030244

.field public static final tv_mychannels_screen:I = 0x7f030245

.field public static final tv_myshows_screen:I = 0x7f030246

.field public static final tv_program_item:I = 0x7f030247

.field public static final tv_recent_channels_screen:I = 0x7f030248

.field public static final tv_series_details_header:I = 0x7f030249

.field public static final tv_streamer_control_bar:I = 0x7f03024a

.field public static final tv_streamer_progress_bar:I = 0x7f03024b

.field public static final tv_trending_livetop10_list_row:I = 0x7f03024c

.field public static final tv_trending_screen:I = 0x7f03024d

.field public static final tvepisode_details_header:I = 0x7f03024e

.field public static final tvepisode_season_screen:I = 0x7f03024f

.field public static final tvhub_phone_pivot:I = 0x7f030250

.field public static final tvhub_tablet_screen:I = 0x7f030251

.field public static final tvseries_overview_screen:I = 0x7f030252

.field public static final tvseries_overview_screen_common:I = 0x7f030253

.field public static final tvseries_seasons_list_row:I = 0x7f030254

.field public static final tvseries_seasons_screen:I = 0x7f030255

.field public static final tvstreamer_controls:I = 0x7f030256

.field public static final universal_remote_control:I = 0x7f030257

.field public static final universal_remote_extra:I = 0x7f030258

.field public static final universal_remote_power_dialog:I = 0x7f030259

.field public static final unshared_activity_feed_list_row:I = 0x7f03025a

.field public static final upload_pic_edit_screen:I = 0x7f03025b

.field public static final upload_pic_error_screen:I = 0x7f03025c

.field public static final upload_pic_loading_screen:I = 0x7f03025d

.field public static final upload_pic_success_screen:I = 0x7f03025e

.field public static final upload_pic_switch_panel_screen:I = 0x7f03025f

.field public static final urc_action_button:I = 0x7f030260

.field public static final urc_control:I = 0x7f030261

.field public static final urc_more_view:I = 0x7f030262

.field public static final urc_navigation_cluster:I = 0x7f030263

.field public static final urc_numpad_button:I = 0x7f030264

.field public static final urc_numpad_small_button:I = 0x7f030265

.field public static final urc_playback_button:I = 0x7f030266

.field public static final urc_power_control:I = 0x7f030267

.field public static final urc_power_view_line:I = 0x7f030268

.field public static final urc_view_control:I = 0x7f030269

.field public static final urc_volume_cluster:I = 0x7f03026a

.field public static final utilitybar_alerts_icon_actionview:I = 0x7f03026b

.field public static final utilitybar_connect_icon_actionview:I = 0x7f03026c

.field public static final utilitybar_friends_icon_actionview:I = 0x7f03026d

.field public static final utilitybar_message_icon_actionview:I = 0x7f03026e

.field public static final utilitybar_party_icon_actionview:I = 0x7f03026f

.field public static final utilitybar_refresh_icon_actionview:I = 0x7f030270

.field public static final videoplayer_activity:I = 0x7f030271

.field public static final virtualgrid_baserow_rightlayout:I = 0x7f030272

.field public static final volume_dialog:I = 0x7f030273

.field public static final web_flow_buttons:I = 0x7f030274

.field public static final webview_dialog:I = 0x7f030275

.field public static final welcome_card_layout:I = 0x7f030276

.field public static final whats_new_details_screen:I = 0x7f030277

.field public static final whats_new_list_row:I = 0x7f030278

.field public static final widget_remote_control:I = 0x7f030279

.field public static final xbid_activity_auth_flow:I = 0x7f03027a

.field public static final xbid_activity_error:I = 0x7f03027b

.field public static final xbid_activity_sign_up:I = 0x7f03027c

.field public static final xbid_activity_welcome:I = 0x7f03027d

.field public static final xbid_fragment_busy:I = 0x7f03027e

.field public static final xbid_fragment_error_ban:I = 0x7f03027f

.field public static final xbid_fragment_error_buttons:I = 0x7f030280

.field public static final xbid_fragment_error_catch_all:I = 0x7f030281

.field public static final xbid_fragment_error_creation:I = 0x7f030282

.field public static final xbid_fragment_error_offline:I = 0x7f030283

.field public static final xbid_fragment_header:I = 0x7f030284

.field public static final xbid_fragment_introducing:I = 0x7f030285

.field public static final xbid_fragment_sign_up:I = 0x7f030286

.field public static final xbid_fragment_welcome:I = 0x7f030287

.field public static final xbid_row_suggestion:I = 0x7f030288

.field public static final xbox_remote_control:I = 0x7f030289

.field public static final xbox_remote_control_buttons:I = 0x7f03028a

.field public static final xbox_remote_dpad:I = 0x7f03028b

.field public static final xbt_tow_line_header_view:I = 0x7f03028c

.field public static final xbt_view_pager_with_tabs:I = 0x7f03028d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
