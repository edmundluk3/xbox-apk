.class public Lcom/microsoft/xbox/toolkit/functions/LinearFunction;
.super Ljava/lang/Object;
.source "LinearFunction.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/Function;


# instance fields
.field private final k:D

.field private final x1:D

.field private final y1:D


# direct methods
.method public constructor <init>(DDD)V
    .locals 1
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "k"    # D

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-wide p5, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->k:D

    .line 14
    iput-wide p1, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->x1:D

    .line 15
    iput-wide p3, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y1:D

    .line 16
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 9
    .param p1, "x1"    # D
    .param p3, "x2"    # D
    .param p5, "y1"    # D
    .param p7, "y2"    # D

    .prologue
    .line 10
    sub-double v0, p7, p5

    sub-double v2, p3, p1

    div-double v6, v0, v2

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;-><init>(DDD)V

    .line 11
    return-void
.end method


# virtual methods
.method public y(D)D
    .locals 7
    .param p1, "x"    # D

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y1:D

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->k:D

    iget-wide v4, p0, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->x1:D

    sub-double v4, p1, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method
