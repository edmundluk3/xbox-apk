.class final synthetic Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

.field private final arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;->arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Lio/reactivex/ObservableTransformer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;-><init>(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;->arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;->arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->lambda$new$4(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
