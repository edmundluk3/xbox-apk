.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "ActivityOverviewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;
    }
.end annotation


# instance fields
.field protected activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field protected currentCapabilities:I

.field protected detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

.field protected deviceRequirementString:Ljava/lang/String;

.field protected isLoading:Z

.field protected loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

.field protected parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 32
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->currentCapabilities:I

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getActivityParentMediaItemData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 52
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getDefaultParentalRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceRequirementString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->deviceRequirementString:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviderText()Ljava/lang/String;
    .locals 6

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getProviderString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0700fc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getProviderString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getProviderString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPublisherText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->PublisherName:Ljava/lang/String;

    .line 97
    :goto_0
    return-object v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;->PublisherName:Ljava/lang/String;

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->publisherName:Ljava/lang/String;

    goto :goto_0

    .line 97
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getReleaseYear()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->isLoading:Z

    return v0
.end method

.method public launchActivity()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeOrHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 161
    return-void
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->cancel()V

    .line 135
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->load(Z)V

    .line 137
    return-void
.end method

.method protected onLoadDetailsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->isLoading:Z

    .line 165
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 188
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->updateAdapter()V

    .line 189
    return-void

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 172
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 174
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 182
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 183
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 157
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivityParentMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 145
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->cancel()V

    .line 152
    :cond_0
    return-void
.end method
