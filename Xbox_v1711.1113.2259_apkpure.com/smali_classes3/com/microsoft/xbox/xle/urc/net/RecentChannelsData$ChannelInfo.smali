.class public Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
.super Ljava/lang/Object;
.source "RecentChannelsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChannelInfo"
.end annotation


# instance fields
.field public channelId:Ljava/lang/String;

.field public channelNum:Ljava/lang/String;

.field public providerId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "channelNum"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelNum:Ljava/lang/String;

    .line 35
    const-string v0, "channelId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelId:Ljava/lang/String;

    .line 36
    const-string v0, "providerId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->providerId:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public isTheSameChannel(Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;

    .prologue
    .line 40
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelId:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelNum:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelNum:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->providerId:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->providerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
