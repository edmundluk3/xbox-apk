.class public Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "WhatsNewDetailsScreen.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "What\'s New"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    .line 15
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;->setShowUtilityBar(Z)V

    .line 16
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;->setDesiredDrawerLockState(I)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "What\'s New"

    return-object v0
.end method

.method public getShouldShowAppbar()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;->onCreateContentView()V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/WhatsNewDetailsScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f030277

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;->setContentView(I)V

    .line 29
    return-void
.end method
