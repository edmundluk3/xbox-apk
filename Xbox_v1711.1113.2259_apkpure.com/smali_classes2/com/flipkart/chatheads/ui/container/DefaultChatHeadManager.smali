.class public Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;
.super Ljava/lang/Object;
.source "DefaultChatHeadManager.java"

# interfaces
.implements Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;
.implements Lcom/flipkart/chatheads/ui/ChatHeadManager;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;,
        Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;",
        "Lcom/flipkart/chatheads/ui/ChatHeadManager",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final OVERLAY_TRANSITION_DURATION:I = 0xc8


# instance fields
.field private activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

.field private activeArrangementBundle:Landroid/os/Bundle;

.field private final arrangements:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation
.end field

.field private arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

.field private final chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

.field private chatHeads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

.field private closeButtonShadow:Landroid/widget/ImageView;

.field private config:Lcom/flipkart/chatheads/ui/ChatHeadConfig;

.field private final context:Landroid/content/Context;

.field private currentFragment:Landroid/support/v4/app/Fragment;

.field private displayMetrics:Landroid/util/DisplayMetrics;

.field private fragmentManager:Landroid/support/v4/app/FragmentManager;

.field private itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

.field private maxHeight:I

.field private maxWidth:I

.field private overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

.field private overlayVisible:Z

.field private requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager",
            "<TT;>.ArrangementChangeRequest;"
        }
    .end annotation
.end field

.field private springSystem:Lcom/facebook/rebound/SpringSystem;

.field private viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadContainer;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chatHeadContainer"    # Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    .prologue
    .line 73
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    .line 74
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->context:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    .line 76
    invoke-interface {p2}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 77
    new-instance v0, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;

    invoke-direct {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->init(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V

    .line 78
    return-void
.end method

.method private init(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chatHeadDefaultConfig"    # Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 290
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-interface {v4, p0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->onInitialized(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    .line 291
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 292
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    const-string v4, "window"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 293
    .local v3, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 294
    iput-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 295
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->config:Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .line 296
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    .line 297
    new-instance v4, Lcom/flipkart/chatheads/ui/UpArrowLayout;

    invoke-direct {v4, p1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    .line 298
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    iget-object v6, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 300
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    invoke-virtual {v4, v10}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setVisibility(I)V

    .line 301
    invoke-static {}, Lcom/facebook/rebound/SpringSystem;->create()Lcom/facebook/rebound/SpringSystem;

    move-result-object v4

    iput-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->springSystem:Lcom/facebook/rebound/SpringSystem;

    .line 302
    new-instance v4, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    iget v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    iget v6, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    invoke-direct {v4, p1, p0, v5, v6}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;-><init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadManager;II)V

    iput-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    .line 303
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-virtual {p2}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getCloseButtonHeight()I

    move-result v5

    invoke-virtual {p2}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getCloseButtonWidth()I

    move-result v6

    const v7, 0x800033

    invoke-interface {v4, v5, v6, v7, v9}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 304
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v4, p0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setListener(Lcom/flipkart/chatheads/ui/ChatHeadCloseButton$CloseButtonListener;)V

    .line 305
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-interface {v4, v5, v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    .line 307
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    const/4 v5, -0x2

    const/16 v6, 0x50

    invoke-interface {v4, v5, v8, v6, v9}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 308
    .local v2, "shadowLayoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    sget v5, Lcom/flipkart/chatheads/R$drawable;->dismiss_shadow:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 309
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    invoke-interface {v4, v5, v2}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 311
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    const-class v5, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    new-instance v6, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-direct {v6, p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    const-class v5, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    new-instance v6, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    invoke-direct {v6, p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setupOverlay(Landroid/content/Context;)V

    .line 314
    invoke-virtual {p0, p2}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setConfig(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V

    .line 315
    invoke-static {}, Lcom/facebook/rebound/SpringConfigRegistry;->getInstance()Lcom/facebook/rebound/SpringConfigRegistry;

    move-result-object v4

    sget-object v5, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    const-string v6, "dragging mode"

    invoke-virtual {v4, v5, v6}, Lcom/facebook/rebound/SpringConfigRegistry;->addSpringConfig(Lcom/facebook/rebound/SpringConfig;Ljava/lang/String;)Z

    .line 316
    invoke-static {}, Lcom/facebook/rebound/SpringConfigRegistry;->getInstance()Lcom/facebook/rebound/SpringConfigRegistry;

    move-result-object v4

    sget-object v5, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    const-string v6, "not dragging mode"

    invoke-virtual {v4, v5, v6}, Lcom/facebook/rebound/SpringConfigRegistry;->addSpringConfig(Lcom/facebook/rebound/SpringConfig;Ljava/lang/String;)Z

    .line 317
    return-void
.end method

.method private onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;Z)V
    .locals 2
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "userTriggered"    # Z

    .prologue
    .line 273
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 274
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->onRemove()V

    .line 275
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-interface {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->removeView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onChatHeadRemoved(Ljava/io/Serializable;Z)V

    .line 282
    :cond_1
    return-void
.end method

.method private setArrangementImpl(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager",
            "<TT;>.ArrangementChangeRequest;)V"
        }
    .end annotation

    .prologue
    .line 372
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "requestedArrangementParam":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>.ArrangementChangeRequest;"
    const/4 v6, 0x0

    .line 373
    .local v6, "hasChanged":Z
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->getArrangement()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 374
    .local v0, "requestedArrangement":Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    const/4 v8, 0x0

    .line 375
    .local v8, "oldArrangement":Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    move-object v7, v0

    .line 376
    .local v7, "newArrangement":Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 377
    .local v2, "extras":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eq v1, v0, :cond_0

    const/4 v6, 0x1

    .line 378
    :cond_0
    if-nez v2, :cond_1

    new-instance v2, Landroid/os/Bundle;

    .end local v2    # "extras":Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 380
    .restart local v2    # "extras":Landroid/os/Bundle;
    :cond_1
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v1, :cond_2

    .line 381
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->getRetainBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 382
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    invoke-virtual {v1, v3, v4}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onDeactivate(II)V

    .line 383
    iget-object v8, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 385
    :cond_2
    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 386
    iput-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangementBundle:Landroid/os/Bundle;

    .line 388
    invoke-static {p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->access$000(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 389
    const-string v1, "hero_index"

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-static {p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->access$000(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 392
    :cond_3
    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->isAnimated()Z

    move-result v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 394
    if-eqz v6, :cond_4

    .line 395
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-interface {v1, v8, v7}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->onArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V

    .line 396
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    if-eqz v1, :cond_4

    .line 397
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    invoke-interface {v1, v8, v7}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onChatHeadArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V

    .line 399
    :cond_4
    return-void
.end method

.method private setupOverlay(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 320
    new-instance v1, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-direct {v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    .line 321
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    sget v2, Lcom/flipkart/chatheads/R$drawable;->overlay_transition:I

    invoke-virtual {v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->setBackgroundResource(I)V

    .line 322
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v1

    invoke-interface {v1, v3, v3, v4, v4}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 323
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-interface {v1, v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 324
    return-void
.end method


# virtual methods
.method public addChatHead(Ljava/io/Serializable;ZZ)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 10
    .param p2, "isSticky"    # Z
    .param p3, "animated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ZZ)",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    const-wide/high16 v8, -0x3fa7000000000000L    # -100.0

    .line 204
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    .line 205
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    if-nez v0, :cond_2

    .line 206
    new-instance v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->springSystem:Lcom/facebook/rebound/SpringSystem;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3, p2}, Lcom/flipkart/chatheads/ui/ChatHead;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/facebook/rebound/SpringSystem;Landroid/content/Context;Z)V

    .line 207
    .restart local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHead;->setKey(Ljava/io/Serializable;)V

    .line 208
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v4

    const v5, 0x800033

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 211
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-interface {v2, v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->config:Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    iget v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    iget v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    invoke-virtual {v3, v4, v5}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getMaxChatHeads(II)I

    move-result v3

    if-le v2, v3, :cond_0

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v2, :cond_0

    .line 213
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->removeOldestChatHead()V

    .line 215
    :cond_0
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->reloadDrawable(Ljava/io/Serializable;)V

    .line 216
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v2, :cond_3

    .line 217
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v2, v0, p3}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onChatHeadAdded(Lcom/flipkart/chatheads/ui/ChatHead;Z)V

    .line 222
    :goto_0
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    invoke-interface {v2, p1}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onChatHeadAdded(Ljava/io/Serializable;)V

    .line 225
    :cond_1
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 227
    .end local v1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    return-object v0

    .line 219
    .restart local v1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 220
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    goto :goto_0
.end method

.method public attachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 496
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "activeChatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v2

    invoke-interface {v1, v2, p1, p2}, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;->attachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 497
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 459
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    invoke-static {v0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->access$002(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/flipkart/chatheads/ui/ChatHead;

    goto :goto_0
.end method

.method public captureChatHeads(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 1
    .param p1, "causingChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 346
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onCapture(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 347
    return-void
.end method

.method public detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 2
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 508
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;->detachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 509
    return-void
.end method

.method public findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 232
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 233
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActiveArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .locals 1

    .prologue
    .line 147
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 150
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArrangement(Ljava/lang/Class;)Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;)",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;"
        }
    .end annotation

    .prologue
    .line 352
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "arrangementType":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    return-object v0
.end method

.method public getArrangementType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->getArrangement()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;
    .locals 1

    .prologue
    .line 341
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    return-object v0
.end method

.method public getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;
    .locals 1

    .prologue
    .line 81
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    return-object v0
.end method

.method public getChatHeadCoordsForCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)[I
    .locals 5
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 427
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 428
    .local v0, "coords":[I
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getEndValueX()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v1, v3, v4

    .line 429
    .local v1, "x":I
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getEndValueY()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v2, v3, v4

    .line 430
    .local v2, "y":I
    const/4 v3, 0x0

    aput v1, v0, v3

    .line 431
    const/4 v3, 0x1

    aput v2, v0, v3

    .line 432
    return-object v0
.end method

.method public getChatHeads()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    return-object v0
.end method

.method public getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;
    .locals 1

    .prologue
    .line 117
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    return-object v0
.end method

.method public getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;
    .locals 1

    .prologue
    .line 514
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->config:Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 132
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 1

    .prologue
    .line 86
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->displayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method public getDistanceCloseButtonFromHead(FF)D
    .locals 11
    .param p1, "touchX"    # F
    .param p2, "touchY"    # F

    .prologue
    .line 327
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v8, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v8}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->isDisappeared()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 328
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 335
    :goto_0
    return-wide v0

    .line 330
    :cond_0
    iget-object v8, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v8}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getLeft()I

    move-result v2

    .line 331
    .local v2, "left":I
    iget-object v8, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v8}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getTop()I

    move-result v3

    .line 332
    .local v3, "top":I
    int-to-float v8, v2

    sub-float v8, p1, v8

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v9

    iget-object v10, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-interface {v9, v10}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->getViewX(Landroid/view/View;)I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v9}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-double v4, v8

    .line 333
    .local v4, "xDiff":D
    int-to-float v8, v3

    sub-float v8, p2, v8

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v9

    iget-object v10, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-interface {v9, v10}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->getViewY(Landroid/view/View;)I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v9}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->getMeasuredHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-double v6, v8

    .line 334
    .local v6, "yDiff":D
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    .line 335
    .local v0, "distance":D
    goto :goto_0
.end method

.method public getListener()Lcom/flipkart/chatheads/ui/ChatHeadListener;
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    return-object v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 127
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    return v0
.end method

.method public getOverlayView()Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;
    .locals 1

    .prologue
    .line 286
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    return-object v0
.end method

.method public getSpringSystem()Lcom/facebook/rebound/SpringSystem;
    .locals 1

    .prologue
    .line 491
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->springSystem:Lcom/facebook/rebound/SpringSystem;

    return-object v0
.end method

.method public getViewAdapter()Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    return-object v0
.end method

.method public hideOverlayView(Z)V
    .locals 4
    .param p1, "animated"    # Z

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v3, 0x0

    .line 403
    iget-boolean v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayVisible:Z

    if-eqz v2, :cond_1

    .line 404
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 405
    .local v0, "drawable":Landroid/graphics/drawable/TransitionDrawable;
    const/16 v1, 0xc8

    .line 406
    .local v1, "duration":I
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 407
    :cond_0
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 408
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-virtual {v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->setClickable(Z)V

    .line 409
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayVisible:Z

    .line 411
    .end local v0    # "drawable":Landroid/graphics/drawable/TransitionDrawable;
    .end local v1    # "duration":I
    :cond_1
    return-void
.end method

.method public onCloseButtonAppear()V
    .locals 2

    .prologue
    .line 468
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->isCloseButtonHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    :cond_0
    return-void
.end method

.method public onCloseButtonDisappear()V
    .locals 2

    .prologue
    .line 475
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 476
    return-void
.end method

.method public onItemRollOut(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;->onChatHeadRollOut(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 455
    :cond_0
    return-void
.end method

.method public onItemRollOver(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 447
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;->onChatHeadRollOver(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 449
    :cond_0
    return-void
.end method

.method public onItemSelected(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 442
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;->onChatHeadSelected(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 7
    .param p1, "height"    # I
    .param p2, "width"    # I

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v6, 0x0

    .line 170
    const/4 v2, 0x0

    .line 171
    .local v2, "needsLayout":Z
    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    if-eq p1, v3, :cond_0

    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    if-eq p2, v3, :cond_0

    .line 172
    const/4 v2, 0x1

    .line 174
    :cond_0
    iput p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    .line 175
    iput p2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    .line 177
    int-to-float v3, p2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    .line 178
    .local v0, "closeButtonCenterX":I
    int-to-float v3, p1

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 180
    .local v1, "closeButtonCenterY":I
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->onParentHeightRefreshed()V

    .line 181
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v3, v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setCenter(II)V

    .line 183
    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxHeight:I

    if-lez v3, :cond_1

    iget v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->maxWidth:I

    if-lez v3, :cond_1

    .line 184
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    if-eqz v3, :cond_2

    .line 185
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    invoke-direct {p0, v3}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setArrangementImpl(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)V

    .line 186
    iput-object v6, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    .line 192
    :cond_1
    :goto_0
    return-void

    .line 187
    :cond_2
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 189
    new-instance v3, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v6, v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;-><init>(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;Ljava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-direct {p0, v3}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setArrangementImpl(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 10
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v9, 0x0

    .line 559
    instance-of v7, p1, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;

    if-eqz v7, :cond_1

    move-object v5, p1

    .line 560
    check-cast v5, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;

    .line 561
    .local v5, "savedState":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->getActiveArrangement()Ljava/lang/Class;

    move-result-object v1

    .line 562
    .local v1, "activeArrangementClass":Ljava/lang/Class;
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->getActiveArrangementBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 563
    .local v0, "activeArrangementBundle":Landroid/os/Bundle;
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->getChatHeads()Ljava/util/Map;

    move-result-object v2

    .line 564
    .local v2, "chatHeads":Ljava/util/Map;, "Ljava/util/Map<+Ljava/io/Serializable;Ljava/lang/Boolean;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 565
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<+Ljava/io/Serializable;Ljava/lang/Boolean;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/Serializable;

    .line 566
    .local v4, "key":Ljava/io/Serializable;, "TT;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    .line 567
    .local v6, "sticky":Ljava/lang/Boolean;
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p0, v4, v8, v9}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->addChatHead(Ljava/io/Serializable;ZZ)Lcom/flipkart/chatheads/ui/ChatHead;

    goto :goto_0

    .line 569
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<+Ljava/io/Serializable;Ljava/lang/Boolean;>;"
    .end local v4    # "key":Ljava/io/Serializable;, "TT;"
    .end local v6    # "sticky":Ljava/lang/Boolean;
    :cond_0
    if-eqz v1, :cond_1

    .line 570
    invoke-virtual {p0, v1, v0, v9}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V

    .line 577
    .end local v0    # "activeArrangementBundle":Landroid/os/Bundle;
    .end local v1    # "activeArrangementClass":Ljava/lang/Class;
    .end local v2    # "chatHeads":Ljava/util/Map;, "Ljava/util/Map<+Ljava/io/Serializable;Ljava/lang/Boolean;>;"
    .end local v5    # "savedState":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 7
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 542
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    new-instance v3, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;

    invoke-direct {v3, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 543
    .local v3, "savedState":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;
    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v5, :cond_0

    .line 544
    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->setActiveArrangement(Ljava/lang/Class;)V

    .line 545
    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->getRetainBundle()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->setActiveArrangementBundle(Landroid/os/Bundle;)V

    .line 547
    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 548
    .local v1, "chatHeadState":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<TT;Ljava/lang/Boolean;>;"
    iget-object v5, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 549
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v2

    .line 550
    .local v2, "key":Ljava/io/Serializable;, "TT;"
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v4

    .line 551
    .local v4, "sticky":Z
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 553
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    .end local v2    # "key":Ljava/io/Serializable;, "TT;"
    .end local v4    # "sticky":Z
    :cond_1
    invoke-virtual {v3, v1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$SavedState;->setChatHeads(Ljava/util/LinkedHashMap;)V

    .line 554
    return-object v3
.end method

.method public onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 581
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->onParentHeightRefreshed()V

    .line 584
    :cond_0
    return-void
.end method

.method public recreateView(Ljava/io/Serializable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 482
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 483
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->removeView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 484
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onReloadFragment(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 487
    :cond_0
    return-void
.end method

.method public reloadDrawable(Ljava/io/Serializable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-interface {v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;->getChatHeadDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 243
    .local v0, "chatHeadDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-interface {v2, p1}, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;->getChatHeadDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/flipkart/chatheads/ui/ChatHead;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    :cond_0
    return-void
.end method

.method public removeAllChatHeads(Z)V
    .locals 3
    .param p1, "userTriggered"    # Z

    .prologue
    .line 250
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/flipkart/chatheads/ui/ChatHead<TT;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 252
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 253
    invoke-direct {p0, v0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;Z)V

    goto :goto_0

    .line 256
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    :cond_0
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    if-eqz v2, :cond_1

    .line 257
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    invoke-interface {v2, p1}, Lcom/flipkart/chatheads/ui/ChatHeadListener;->onAllChatHeadsRemoved(Z)V

    .line 259
    :cond_1
    return-void
.end method

.method public removeChatHead(Ljava/io/Serializable;Z)Z
    .locals 2
    .param p2, "userTriggered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    .prologue
    .line 263
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    .line 264
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    if-eqz v0, :cond_0

    .line 265
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeads:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 266
    invoke-direct {p0, v0, p2}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;Z)V

    .line 267
    const/4 v1, 0x1

    .line 269
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 2
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 502
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;->removeView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 503
    return-void
.end method

.method public selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 155
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->activeArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 157
    :cond_0
    return-void
.end method

.method public selectChatHead(Ljava/io/Serializable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    .line 162
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0, v0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 165
    :cond_0
    return-void
.end method

.method public setArrangement(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 1
    .param p2, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "arrangement":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V

    .line 358
    return-void
.end method

.method public setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V
    .locals 1
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "animated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Landroid/os/Bundle;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 362
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "arrangement":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    new-instance v0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;-><init>(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;Ljava/lang/Class;Landroid/os/Bundle;Z)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->requestedArrangement:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    .line 363
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->chatHeadContainer:Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->requestLayout()V

    .line 364
    return-void
.end method

.method public setConfig(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
    .locals 4
    .param p1, "config"    # Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 519
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->config:Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .line 520
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    if-eqz v1, :cond_0

    .line 526
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->isCloseButtonHidden()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 527
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v1, v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setVisibility(I)V

    .line 528
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 534
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->arrangements:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 535
    .local v0, "arrangementEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;->onConfigChanged(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V

    goto :goto_1

    .line 530
    .end local v0    # "arrangementEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    :cond_1
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButton:Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    invoke-virtual {v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setVisibility(I)V

    .line 531
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->closeButtonShadow:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 538
    :cond_2
    return-void
.end method

.method public setListener(Lcom/flipkart/chatheads/ui/ChatHeadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/flipkart/chatheads/ui/ChatHeadListener;

    .prologue
    .line 97
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->listener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    .line 98
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 437
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    .local p1, "onItemSelectedListener":Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;, "Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->itemSelectedListener:Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;

    .line 438
    return-void
.end method

.method public setViewAdapter(Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;)V
    .locals 0
    .param p1, "chatHeadViewAdapter"    # Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    .prologue
    .line 112
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->viewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    .line 113
    return-void
.end method

.method public showOverlayView(Z)V
    .locals 4
    .param p1, "animated"    # Z

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>;"
    const/4 v3, 0x1

    .line 415
    iget-boolean v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayVisible:Z

    if-nez v2, :cond_1

    .line 416
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 417
    .local v0, "drawable":Landroid/graphics/drawable/TransitionDrawable;
    const/16 v1, 0xc8

    .line 418
    .local v1, "duration":I
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 419
    :cond_0
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 420
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayView:Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    invoke-virtual {v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->setClickable(Z)V

    .line 421
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;->overlayVisible:Z

    .line 423
    .end local v0    # "drawable":Landroid/graphics/drawable/TransitionDrawable;
    .end local v1    # "duration":I
    :cond_1
    return-void
.end method
