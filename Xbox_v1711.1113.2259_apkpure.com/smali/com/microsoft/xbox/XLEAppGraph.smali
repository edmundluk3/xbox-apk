.class public interface abstract Lcom/microsoft/xbox/XLEAppGraph;
.super Ljava/lang/Object;
.source "XLEAppGraph.java"


# virtual methods
.method public abstract inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/base/ReactActivityBase;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/MessageModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/PagesModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/ProfileModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/SocialTagModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/MainActivity;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V
.end method

.method public abstract inject(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)V
.end method

.method public abstract mediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
.end method
