.class Lcom/microsoft/xbox/xle/ui/SearchBarView$2;
.super Ljava/lang/Object;
.source "SearchBarView.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/SearchBarView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/SearchBarView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x1

    .line 98
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$300(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ne p2, v4, :cond_0

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$300(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;->dismissKeyboard()V

    .line 112
    :cond_0
    :goto_0
    return v3

    .line 105
    :cond_1
    const-string v0, "SearchBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "actionid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$200(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-eq p2, v0, :cond_2

    if-eqz p2, :cond_2

    if-ne p2, v4, :cond_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$200(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;->onQueryTextSubmit(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$300(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;->this$0:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->access$300(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;->dismissKeyboard()V

    goto :goto_0
.end method
