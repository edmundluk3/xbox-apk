.class final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RelatedInfo;
.super Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_RelatedInfo;
.source "AutoValue_MultiplayerSessionDataTypes_RelatedInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RelatedInfo$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V
    .locals 0
    .param p1, "closed"    # Ljava/lang/Boolean;
    .param p2, "joinRestriction"    # Ljava/lang/String;
    .param p3, "maxMembersCount"    # Ljava/lang/Integer;
    .param p4, "membersCount"    # Ljava/lang/Integer;
    .param p6, "visibility"    # Ljava/lang/String;
    .param p7, "postedTime"    # Ljava/util/Date;
    .param p8, "scheduledTime"    # Ljava/util/Date;
    .param p9, "description"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    .param p10, "clubId"    # Ljava/lang/String;
    .param p11, "searchHandleVisibility"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    .local p5, "sessionOwners":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_RelatedInfo;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V

    .line 24
    return-void
.end method
