.class public Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConversationsDetailsActivity.java"


# instance fields
.field private final parameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 0
    .param p1, "parameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->parameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .line 24
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "Messaging - Conversation View"

    return-object v0
.end method

.method public getUseUTCTelemetry()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->onCreateContentView()V

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->parameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    if-nez v0, :cond_0

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 35
    return-void

    .line 32
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->parameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f0300b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->setContentView(I)V

    .line 41
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 46
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 47
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onScreenTouchEvent()V

    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public trackPageViewTelemetry()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .line 63
    .local v0, "detailsViewModel":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v2

    iget-object v1, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 65
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackConversationView(Ljava/lang/String;)V

    .line 67
    .end local v1    # "id":Ljava/lang/String;
    :cond_0
    return-void
.end method
