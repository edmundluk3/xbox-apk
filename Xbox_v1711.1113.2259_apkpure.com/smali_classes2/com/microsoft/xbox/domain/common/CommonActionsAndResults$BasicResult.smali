.class public abstract Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
.super Ljava/lang/Object;
.source "CommonActionsAndResults.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/common/CommonActionsAndResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BasicResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "content":Ljava/lang/Object;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 151
    new-instance v0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_BasicResult;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_BasicResult;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public abstract content()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult<TT;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
