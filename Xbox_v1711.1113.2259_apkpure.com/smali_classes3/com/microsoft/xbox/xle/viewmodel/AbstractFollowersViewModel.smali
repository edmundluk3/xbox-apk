.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "AbstractFollowersViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;
    }
.end annotation


# instance fields
.field private favoriteFollowersData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private favoritesAndFriendsData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private favoritesAndFriendsOfflineCount:I

.field private favoritesAndFriendsOnlineCount:I

.field private favoritesOffline:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private favoritesOfflineCount:I

.field private favoritesOnline:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private favoritesOnlineCount:I

.field protected followingData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field protected isLoadingFollowingData:Z

.field protected loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

.field protected model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>()V

    .line 35
    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnlineCount:I

    .line 36
    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOfflineCount:I

    .line 37
    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOnlineCount:I

    .line 38
    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOfflineCount:I

    .line 40
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 44
    return-void
.end method


# virtual methods
.method protected buildFavorites()V
    .locals 4

    .prologue
    .line 248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    .line 249
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 253
    .local v0, "person":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v2, :cond_0

    .line 254
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v3, :cond_1

    .line 255
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 257
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v0    # "person":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    .line 264
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnlineCount:I

    .line 267
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOfflineCount:I

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 271
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/service/model/FollowersData;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 274
    :cond_3
    return-void
.end method

.method protected buildFavoritesAndFriendsList()V
    .locals 5

    .prologue
    .line 277
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    .line 280
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 284
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    .line 285
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    .local v0, "friendsOffline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    .line 288
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 289
    .local v1, "person":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-boolean v3, v1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-nez v3, :cond_2

    .line 290
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v3, v4, :cond_3

    .line 292
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 294
    :cond_3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    .end local v1    # "person":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOnlineCount:I

    .line 301
    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOfflineCount:I

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOfflineCount:I

    .line 303
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 306
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/xbox/service/model/FollowersData;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Z)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 311
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 314
    .end local v0    # "friendsOffline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_6
    return-void
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->forceRefresh()V

    .line 118
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->load(Z)V

    .line 119
    return-void
.end method

.method public getFavorites()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFavoritesAndFriends()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsData:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFollowing()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOfflineFavorites()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOffline:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOfflineFavoritesAndFriendsCount()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOfflineCount:I

    return v0
.end method

.method public getOfflineFavoritesCount()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOfflineCount:I

    return v0
.end method

.method public getOnlineFavorites()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnline:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOnlineFavoritesAndFriendsCount()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesAndFriendsOnlineCount:I

    return v0
.end method

.method public getOnlineFavoritesCount()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoritesOnlineCount:I

    return v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAppDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueryResults(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "queryText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v1, "resultPeople":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 199
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 200
    .local v0, "person":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 201
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    .end local v0    # "person":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_1
    return-object v1
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->isLoadingFollowingData:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->cancel()V

    .line 151
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->load(Z)V

    .line 153
    return-void
.end method

.method public navigateToFriendProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 7
    .param p1, "friendData"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 156
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 157
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 160
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 162
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 163
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 164
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 167
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToViewProfile()Z

    move-result v3

    if-eqz v0, :cond_1

    .line 168
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->canViewOtherProfiles()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704b5

    .line 169
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061c

    .line 170
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 166
    invoke-static {v3, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 176
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    :goto_1
    return-void

    .line 168
    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 174
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1
.end method

.method public onLoadFollowingCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 122
    const-string v0, "Followers ViewModel"

    const-string v1, "onLoadFollowing Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->isLoadingFollowingData:Z

    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 143
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->updateAdapter()V

    .line 144
    return-void

    .line 129
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    .line 130
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->buildFavorites()V

    .line 131
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->buildFavoritesAndFriendsList()V

    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->updateViewModelState()V

    goto :goto_0

    .line 137
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 138
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public abstract onRehydrate()V
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 106
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->cancel()V

    .line 113
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->favoriteFollowersData:Ljava/util/ArrayList;

    .line 180
    return-void
.end method

.method protected updateViewModelState()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->followingData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 94
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->isLoadingFollowingData:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 98
    :goto_1
    return-void

    .line 94
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 96
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method
