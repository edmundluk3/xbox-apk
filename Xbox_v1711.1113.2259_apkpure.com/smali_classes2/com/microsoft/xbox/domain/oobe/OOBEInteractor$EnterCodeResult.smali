.class public final enum Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
.super Ljava/lang/Enum;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnterCodeResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;",
        ">;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

.field public static final enum BAD_INPUT:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

.field public static final enum OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

.field public static final enum SERVICE_ERROR:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 261
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    .line 262
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    const-string v1, "SERVICE_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->SERVICE_ERROR:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    .line 263
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    const-string v1, "BAD_INPUT"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->BAD_INPUT:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    .line 260
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->OK:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->SERVICE_ERROR:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->BAD_INPUT:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 260
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 260
    const-class v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;
    .locals 1

    .prologue
    .line 260
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ErrorCodeResult "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeResult;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
