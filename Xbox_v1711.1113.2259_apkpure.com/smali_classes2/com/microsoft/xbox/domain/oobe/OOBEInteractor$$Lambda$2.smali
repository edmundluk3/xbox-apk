.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$3:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p3, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$3:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/ObservableTransformer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;-><init>(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$2;->arg$3:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$new$11(Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
