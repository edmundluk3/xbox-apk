.class public Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
.super Ljava/lang/Object;
.source "WebComponentVersion.java"


# instance fields
.field public final majorVersion:I

.field public final minorVersion:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    .line 21
    iput p2, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .prologue
    .line 30
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGreaterThan(Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLessThan(Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .prologue
    .line 39
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->majorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->minorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
