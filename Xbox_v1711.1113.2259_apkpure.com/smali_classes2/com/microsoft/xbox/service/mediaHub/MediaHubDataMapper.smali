.class public final Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;
.super Ljava/lang/Object;
.source "MediaHubDataMapper.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 15
    .param p0, "gameClip"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    new-instance v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;-><init>()V

    .line 57
    .local v7, "profileGameclip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    const-string v12, ""

    invoke-virtual {v7, v12}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->setGamerTag(Ljava/lang/String;)V

    .line 58
    const-string v12, "00000000-0000-0000-0000-000000000000"

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->scid:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->ownerXuid()Ljava/lang/Long;

    move-result-object v6

    .line 61
    .local v6, "ownerXuid":Ljava/lang/Long;
    if-eqz v6, :cond_1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    :goto_0
    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->titleName()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->userCaption()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 67
    .local v0, "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    .line 68
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 70
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    .line 71
    .local v5, "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v10

    .line 73
    .local v10, "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->isThumbnail()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 74
    new-instance v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v9}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 75
    .local v9, "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 76
    iget-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    .end local v9    # "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    :cond_0
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;-><init>()V

    .line 80
    .local v4, "gameClipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    .line 82
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->fileSize()Ljava/lang/Integer;

    move-result-object v3

    .line 83
    .local v3, "fileSize":Ljava/lang/Integer;
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v3, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->fileSize:I

    .line 85
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_2
    iput-object v12, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    .line 86
    iget-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 61
    .end local v0    # "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .end local v3    # "fileSize":Ljava/lang/Integer;
    .end local v4    # "gameClipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    .end local v5    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .end local v10    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_1
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 85
    .restart local v0    # "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .restart local v3    # "fileSize":Ljava/lang/Integer;
    .restart local v4    # "gameClipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    .restart local v5    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .restart local v10    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_2
    const-string v12, ""

    goto :goto_2

    .line 89
    .end local v3    # "fileSize":Ljava/lang/Integer;
    .end local v4    # "gameClipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    .end local v5    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .end local v10    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->viewCount()Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->views:I

    .line 91
    new-instance v12, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v12}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 92
    iget-object v13, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->commentCount()Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    .line 93
    iget-object v13, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->likeCount()Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 94
    iget-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    const-string v13, ""

    iput-object v13, v12, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 95
    iget-object v13, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->shareCount()Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentSegments()Lcom/google/common/collect/ImmutableList;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 98
    .local v1, "contentSegments":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_4

    .line 99
    iget-object v13, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    const/4 v12, 0x0

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->creationType()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->type:Ljava/lang/String;

    .line 101
    const/4 v12, 0x0

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;->recordDate()Ljava/util/Date;

    move-result-object v8

    .line 102
    .local v8, "recordedDate":Ljava/util/Date;
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_3
    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->dateRecorded:Ljava/lang/String;

    .line 105
    .end local v8    # "recordedDate":Ljava/util/Date;
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentState()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    .line 106
    const-string v12, ""

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->capturerRealName:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->uploadDate()Ljava/util/Date;

    move-result-object v11

    .line 109
    .local v11, "uploadDate":Ljava/util/Date;
    iput-object v11, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->rawDate:Ljava/util/Date;

    .line 110
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_4
    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->lastModified:Ljava/lang/String;

    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->contentId()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;->durationInSeconds()Ljava/lang/Integer;

    move-result-object v2

    .line 115
    .local v2, "durationInSeconds":Ljava/lang/Integer;
    if-eqz v2, :cond_7

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    :goto_5
    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->durationInSeconds:Ljava/lang/String;

    .line 117
    const-string v12, ""

    iput-object v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    .line 118
    const/4 v12, 0x0

    iput v12, v7, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->rating:I

    .line 120
    return-object v7

    .line 102
    .end local v2    # "durationInSeconds":Ljava/lang/Integer;
    .end local v11    # "uploadDate":Ljava/util/Date;
    .restart local v8    # "recordedDate":Ljava/util/Date;
    :cond_5
    const-string v12, ""

    goto :goto_3

    .line 110
    .end local v8    # "recordedDate":Ljava/util/Date;
    .restart local v11    # "uploadDate":Ljava/util/Date;
    :cond_6
    const-string v12, ""

    goto :goto_4

    .line 115
    .restart local v2    # "durationInSeconds":Ljava/lang/Integer;
    :cond_7
    const-string v12, ""

    goto :goto_5
.end method

.method public static from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 6
    .param p0, "searchResponse"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;-><init>()V

    .line 38
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;-><init>()V

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    .line 39
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->continuationToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 41
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 44
    .local v1, "gameClips":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    .line 46
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;

    .line 47
    .local v0, "gameClip":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    .end local v0    # "gameClip":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;
    :cond_0
    return-object v2
.end method

.method public static from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 6
    .param p0, "searchResponse"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 125
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 127
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;-><init>()V

    .line 129
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;-><init>()V

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    .line 130
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;->continuationToken()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 132
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 135
    .local v2, "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    .line 137
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .line 138
    .local v1, "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    .end local v1    # "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    :cond_0
    return-object v0
.end method

.method public static from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .locals 13
    .param p0, "screenshot"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 146
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;-><init>()V

    .line 148
    .local v4, "profileScreenshot":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    const-string v9, ""

    invoke-virtual {v4, v9}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->setGamerTag(Ljava/lang/String;)V

    .line 149
    const-string v9, "00000000-0000-0000-0000-000000000000"

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->scid:Ljava/lang/String;

    .line 151
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->ownerXuid()Ljava/lang/Long;

    move-result-object v3

    .line 152
    .local v3, "ownerXuid":Ljava/lang/Long;
    if-eqz v3, :cond_1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    :goto_0
    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->userCaption()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->userCaption:Ljava/lang/String;

    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 158
    .local v1, "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->thumbnails:Ljava/util/ArrayList;

    .line 159
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    .line 161
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    .line 162
    .local v2, "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 163
    .local v6, "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v6, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 164
    iget-object v10, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;-><init>()V

    .line 167
    .local v5, "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uri:Ljava/lang/String;

    .line 169
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v7

    .line 170
    .local v7, "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    if-eqz v7, :cond_0

    .line 171
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uriType:Ljava/lang/String;

    .line 174
    :cond_0
    iget-object v10, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotUris:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    .end local v1    # "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .end local v2    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .end local v5    # "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    .end local v6    # "thumbnail":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    .end local v7    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 177
    .restart local v1    # "contentLocators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->viewCount()Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->views:I

    .line 179
    new-instance v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v9}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 180
    iget-object v10, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->commentCount()Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v10, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    .line 181
    iget-object v10, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->likeCount()Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v10, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 182
    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    const-string v10, ""

    iput-object v10, v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 183
    iget-object v10, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->shareCount()Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v10, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    .line 185
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentState()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->state:Ljava/lang/String;

    .line 186
    const-string v9, ""

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->capturerRealName:Ljava/lang/String;

    .line 188
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->dateUploaded()Ljava/util/Date;

    move-result-object v8

    .line 189
    .local v8, "uploadDate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->captureDate()Ljava/util/Date;

    move-result-object v0

    .line 190
    .local v0, "captureDate":Ljava/util/Date;
    iput-object v8, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->rawDate:Ljava/util/Date;

    .line 191
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    :goto_2
    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->lastModified:Ljava/lang/String;

    .line 192
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    :goto_3
    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->dateTaken:Ljava/lang/String;

    .line 194
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentId()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    .line 195
    iput v12, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->rating:I

    .line 197
    return-object v4

    .line 191
    :cond_3
    const-string v9, ""

    goto :goto_2

    .line 192
    :cond_4
    const-string v9, ""

    goto :goto_3
.end method
