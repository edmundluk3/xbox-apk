.class public Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "FriendsSelectorScreenAdapter.java"


# instance fields
.field private clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

.field private listOrGridView:Landroid/widget/AbsListView;

.field private recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private recipientholderButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private recipientsHolderString:Ljava/lang/String;

.field private recipientsHolderView:Landroid/widget/LinearLayout;

.field private searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private searchbarLayout:Landroid/view/View;

.field private selectedRecipientsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private separator:Landroid/view/View;

.field private showFilteredList:Z

.field private title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 58
    const v0, 0x7f0e05c3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->screenBody:Landroid/view/View;

    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    .line 60
    const v0, 0x7f0e05d0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 61
    const v0, 0x7f0e05d1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    .line 62
    const v0, 0x7f0e05c6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07063b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 67
    const v0, 0x7f0e05c7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchbarLayout:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchbarLayout:Landroid/view/View;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 70
    const v0, 0x7f0e04ad

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->separator:Landroid/view/View;

    .line 71
    const v0, 0x7f0e041a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 72
    const v0, 0x7f0e041b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientholderButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientholderButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPadding(IIII)V

    .line 75
    const v0, 0x7f0e05cb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderView:Landroid/widget/LinearLayout;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    .line 79
    const v0, 0x7f0e05ca

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03010a

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;-><init>(Landroid/app/Activity;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 136
    const v0, 0x7f0e05ce

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 137
    const v0, 0x7f0e05cf

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 138
    const v0, 0x7f0e05c5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 140
    const v0, 0x7f0e05c9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->showFilteredList:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchbarLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->separator:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/AbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->updateRecipientHolderString()V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method private clear()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->onSearchBarClear()V

    .line 334
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->clear()V

    return-void
.end method

.method static synthetic lambda$restoreCheckedItems$1(Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)Z
    .locals 1
    .param p0, "f"    # Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .prologue
    .line 288
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private markAlreadySelectedMember(Ljava/util/List;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "friends":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    .local p2, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 318
    .local v1, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 319
    .local v0, "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v4, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamertag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 320
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setSelected(Z)V

    .line 321
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setEnable(Z)V

    goto :goto_0

    .line 326
    .end local v0    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .end local v1    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_2
    return-void
.end method

.method private restoreCheckedItems(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "friends":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    const/4 v7, 0x1

    .line 282
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getConversationMembers()Ljava/util/Collection;

    move-result-object v3

    .line 283
    .local v3, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    if-eqz v3, :cond_0

    .line 284
    invoke-direct {p0, p1, v3}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->markAlreadySelectedMember(Ljava/util/List;Ljava/util/Collection;)V

    .line 287
    :cond_0
    if-eqz p1, :cond_4

    .line 288
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 289
    .local v0, "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    .line 290
    .local v1, "index":I
    if-ltz v1, :cond_2

    .line 291
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v2

    .line 292
    .local v2, "isSelected":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v5, v1, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 294
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 295
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 296
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    .end local v2    # "isSelected":Z
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamertag()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 304
    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setSelected(Z)V

    .line 305
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->setEnable(Z)V

    .line 306
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v5, v1, v7}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 307
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 308
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    .end local v0    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .end local v1    # "index":I
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->updateRecipientHolderString()V

    .line 314
    :cond_4
    return-void
.end method

.method private updateRecipientHolderString()V
    .locals 5

    .prologue
    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070668

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    .line 181
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 183
    .local v1, "recipientCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 184
    add-int/lit8 v2, v1, -0x1

    if-ne v0, v2, :cond_0

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    .line 183
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->selectedRecipientsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    goto :goto_1

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->recipientsHolderString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->onStart()V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 231
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->onStop()V

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsPickerClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    :cond_2
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->updateLoadingIndicator(Z)V

    .line 248
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 250
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->showFilteredList:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getFilteredPeopleBasedOnSearchText()Ljava/util/List;

    move-result-object v0

    .line 251
    .local v0, "newFriendSelectorData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsList:Ljava/util/List;

    if-eq v1, v0, :cond_1

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->clear()V

    .line 253
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 254
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->restoreCheckedItems(Ljava/util/List;)V

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;->onDataUpdated()V

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    .line 259
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsList:Ljava/util/List;

    .line 264
    :goto_1
    return-void

    .line 250
    .end local v0    # "newFriendSelectorData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->friendsViewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getFriends()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 261
    .restart local v0    # "newFriendSelectorData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method
