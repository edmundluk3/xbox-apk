.class final synthetic Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;->arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;-><init>(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;->arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->lambda$null$4(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
