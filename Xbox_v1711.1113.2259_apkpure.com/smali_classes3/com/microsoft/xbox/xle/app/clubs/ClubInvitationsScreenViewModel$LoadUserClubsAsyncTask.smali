.class Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubInvitationsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserClubsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 439
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 451
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 420
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->access$402(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Z)Z

    .line 445
    return-void
.end method
