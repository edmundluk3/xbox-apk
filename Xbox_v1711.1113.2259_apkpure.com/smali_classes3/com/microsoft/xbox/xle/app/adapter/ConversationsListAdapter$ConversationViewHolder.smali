.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ConversationsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConversationViewHolder"
.end annotation


# instance fields
.field attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0484
    .end annotation
.end field

.field favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04b0
    .end annotation
.end field

.field groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e046b
    .end annotation
.end field

.field groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e046c
    .end annotation
.end field

.field groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e046e
    .end annotation
.end field

.field groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e046f
    .end annotation
.end field

.field iconsContainerRow2:Landroid/widget/TableRow;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e046d
    .end annotation
.end field

.field presenceIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0231
    .end annotation
.end field

.field sender:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04b3
    .end annotation
.end field

.field senderRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04b4
    .end annotation
.end field

.field sentTime:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04b5
    .end annotation
.end field

.field private summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

.field tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04af
    .end annotation
.end field

.field title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04a5
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    .line 118
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 119
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;)Landroid/view/View$OnLongClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 169
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->navigateToConversationDetails(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 124
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;Landroid/view/View;)Z
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v2, :cond_2

    .line 128
    new-instance v1, Landroid/widget/PopupMenu;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-direct {v1, v2, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 129
    .local v1, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f0f0006

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 131
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c2a

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 132
    .local v0, "blockItem":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isSkypeSenderService(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v2, :cond_3

    .line 133
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 140
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldShowHoverChatOption()Z

    move-result v2

    if-nez v2, :cond_1

    .line 141
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0e0c28

    invoke-interface {v2, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 144
    :cond_1
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 164
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 167
    .end local v0    # "blockItem":Landroid/view/MenuItem;
    .end local v1    # "popup":Landroid/widget/PopupMenu;
    :cond_2
    const/4 v2, 0x1

    return v2

    .line 134
    .restart local v0    # "blockItem":Landroid/view/MenuItem;
    .restart local v1    # "popup":Landroid/widget/PopupMenu;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isUserBlocked(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$600()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0

    .line 137
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$700()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;Landroid/view/MenuItem;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 161
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 147
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->openInHoverChat(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .line 150
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->deleteSkypeConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .line 153
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isUserBlocked(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->unblockUser(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->blockSender(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c28
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 10
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    const/4 v9, 0x1

    const v7, 0x7f020180

    const v8, 0x7f020125

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 173
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 175
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->itemView:Landroid/view/View;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f020165

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$000()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setTextColor(I)V

    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$000()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 179
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$000()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 187
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 188
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 190
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v2, :cond_3

    .line 191
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sender:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupTopic(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->presenceIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v6, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v6, :cond_5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v2, :cond_5

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 199
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->getIsFavorite()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    :goto_4
    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 200
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->getIsFavorite()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v2, :cond_1

    .line 201
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->presenceIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 202
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v5, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v5, :cond_7

    .line 203
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$300()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 209
    :cond_1
    :goto_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->senderRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->senderRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v4

    :goto_6
    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 211
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v5, v6, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getConversationSentTimeString(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v5, v6, v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getConversationSentTimeString(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v2, :cond_a

    .line 215
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 217
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v2, :cond_9

    .line 218
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-virtual {v2, v5, v7, v7}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 223
    :goto_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 225
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 226
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 288
    :goto_8
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAttachment()Z

    move-result v5

    if-eqz v5, :cond_c

    :goto_9
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 289
    return-void

    .line 181
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->itemView:Landroid/view/View;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f020166

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$100()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setTextColor(I)V

    .line 183
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->attachmentIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$100()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 184
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->sentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$100()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->title:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const-string v5, ""

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 196
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto/16 :goto_2

    :cond_5
    move v2, v4

    .line 197
    goto/16 :goto_3

    :cond_6
    move v2, v4

    .line 199
    goto/16 :goto_4

    .line 204
    :cond_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v5, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v2, v5, :cond_1

    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->favoriteIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$400()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    goto/16 :goto_5

    :cond_8
    move v2, v3

    .line 210
    goto/16 :goto_6

    .line 220
    :cond_9
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_7

    .line 228
    :cond_a
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageResource(I)V

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 232
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 233
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 234
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 235
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 238
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v5, v6, v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getMessageModel()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v1

    .line 243
    .local v1, "membersCount":I
    const/4 v2, 0x4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 247
    .local v0, "actualImagesCount":I
    const/4 v2, 0x3

    if-lt v0, v2, :cond_b

    .line 248
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-virtual {v2, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 255
    :goto_a
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_8

    .line 257
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 258
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->tile:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 259
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-virtual {v2, v4}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 260
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 261
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 262
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 263
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 250
    :cond_b
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->iconsContainerRow2:Landroid/widget/TableRow;

    invoke-virtual {v2, v4}, Landroid/widget/TableRow;->setVisibility(I)V

    goto :goto_a

    .line 266
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 267
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 268
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 269
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 272
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 273
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 274
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 275
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 278
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile1:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 279
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile2:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 280
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile3:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 281
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter$ConversationViewHolder;->groupTile4:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    goto/16 :goto_8

    .end local v0    # "actualImagesCount":I
    .end local v1    # "membersCount":I
    :cond_c
    move v3, v4

    .line 288
    goto/16 :goto_9

    .line 255
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
