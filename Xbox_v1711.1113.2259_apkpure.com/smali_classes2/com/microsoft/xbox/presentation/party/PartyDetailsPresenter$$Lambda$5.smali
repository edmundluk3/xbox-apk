.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

.field private final arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$5;->arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    check-cast p1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->lambda$null$12(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
