.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "PeopleHubInfoScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

.field private changeRealNameBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private clubBanBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private clubInviteBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private customizeProfileBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

.field private pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

.field private profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

.field private profileFollowersText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileFollowingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileGamerScore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileGamerScoreIcon:Landroid/widget/TextView;

.field private profileGamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

.field private profileJoinableTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileLocationHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

.field private profileRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

.field private reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private reputationBanner:Landroid/view/View;

.field private reputationBannerText:Landroid/widget/TextView;

.field private rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

.field private sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private warningBanner:Landroid/view/View;

.field private warningBannerText:Landroid/widget/TextView;

.field private watermarkListAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 6
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 70
    const v1, 0x7f0e0911

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->screenBody:Landroid/view/View;

    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .line 74
    const v1, 0x7f0e0910

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLERootView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    .line 75
    const v1, 0x7f0e0912

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->warningBanner:Landroid/view/View;

    .line 76
    const v1, 0x7f0e0913

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->warningBannerText:Landroid/widget/TextView;

    .line 77
    const v1, 0x7f0e0914

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reputationBanner:Landroid/view/View;

    .line 78
    const v1, 0x7f0e0915

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reputationBannerText:Landroid/widget/TextView;

    .line 79
    const v1, 0x7f0e0918

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 80
    const v1, 0x7f0e0921

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinableTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 81
    const v1, 0x7f0e0923

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    .line 82
    const v1, 0x7f0e0922

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    .line 83
    const v1, 0x7f0e0243

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerScore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 84
    const v1, 0x7f0e0930

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 85
    const v1, 0x7f0e0916

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 86
    const v1, 0x7f0e0929

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowersText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 87
    const v1, 0x7f0e092b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 88
    const v1, 0x7f0e0242

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerScoreIcon:Landroid/widget/TextView;

    .line 89
    const v1, 0x7f0e092c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 90
    const v1, 0x7f0e092d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 91
    const v1, 0x7f0e092e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 92
    const v1, 0x7f0e0919

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    .line 93
    const v1, 0x7f0e091a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubInviteBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 94
    const v1, 0x7f0e091b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubBanBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 95
    const v1, 0x7f0e091c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 96
    const v1, 0x7f0e091f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    .line 97
    const v1, 0x7f0e0920

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 98
    const v1, 0x7f0e091e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->changeRealNameBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 99
    const v1, 0x7f0e091d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->customizeProfileBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 100
    const v1, 0x7f0e090f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 102
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f0301c6

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->watermarkListAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;

    .line 103
    const v1, 0x7f0e092f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .line 104
    .local v0, "watermarkList":Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->watermarkListAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->changeRealNameBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->customizeProfileBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setVisibility(I)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubInviteBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubBanBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->customizeProfileBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->changeRealNameBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setVisibility(I)V

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubInviteBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubBanBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubInviteBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->clubBanBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 117
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackCustomizeAction()V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->navigateToCustomize()V

    .line 119
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 122
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackRealNameSettingAction()V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->navigateToSettings()V

    .line 124
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 8
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBroadcastProvider()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "broadcastProvider":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBroadcastId()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "broadcastId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 147
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBroadcastTitleId()J

    move-result-wide v4

    long-to-int v4, v4

    int-to-long v2, v4

    .line 149
    .local v2, "titleId":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "Watch Broadcast"

    const/4 v6, 0x0

    long-to-int v7, v2

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    .end local v2    # "titleId":J
    :goto_0
    return-void

    .line 151
    :cond_0
    const-string v4, "Can\'t launch broadcast due to missing information"

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->toggle()V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->blockUser()V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->unblockUser()V

    goto :goto_0
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->navigateToEnforcement()V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isCallerFollowingTarget()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipAction(Ljava/lang/String;Z)V

    .line 170
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 171
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 173
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    .line 174
    .local v1, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 176
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFacebookFriend()Z

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->shouldAutoAddFollowing(ZLcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addFollowingUser()V

    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->addUserToShareIdentityList()V

    .line 179
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isFacebookFriend()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipDoneAutoAdd(Ljava/lang/String;Z)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showChangeFriendshipDialog()V

    goto :goto_0
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->inviteToClub()V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->showBanFromClubPicker()V

    return-void
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 189
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackSendMessageAction()V

    .line 190
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 192
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .line 193
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isSendMessageAllowed()Z

    move-result v2

    if-eqz v0, :cond_1

    .line 194
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0704a9

    .line 195
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07061c

    .line 196
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 192
    invoke-static {v2, v1, v3, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->navigateToSendMessage()V

    .line 199
    :cond_0
    return-void

    .line 194
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->joinParty()V

    return-void
.end method


# virtual methods
.method public onSetActive()V
    .locals 2

    .prologue
    .line 207
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onSetActive()V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPreferredColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 211
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 3

    .prologue
    .line 215
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onSetInactive()V

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 219
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 14

    .prologue
    .line 223
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBusy()Z

    move-result v7

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->updateLoadingIndicator(Z)V

    .line 225
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->isActive:Z

    if-eqz v7, :cond_0

    .line 226
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPreferredColor()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 228
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    if-eqz v7, :cond_1

    .line 229
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->rootView:Lcom/microsoft/xbox/xle/ui/XLERootView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPreferredColor()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/XLERootView;->setBackgroundColor(I)V

    .line 232
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v7, :cond_2

    .line 233
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f020125

    const v10, 0x7f020125

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 236
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerScore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_3

    .line 237
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerScore()Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "gamerScore":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 239
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerScore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-static {v7, v1, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 240
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerScoreIcon:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 244
    .end local v1    # "gamerScore":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_5

    .line 245
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "gamerTag":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 247
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileGamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 251
    .end local v2    # "gamerTag":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getRealName()Ljava/lang/String;

    move-result-object v6

    .line 252
    .local v6, "realName":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 253
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 259
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowersText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_6

    .line 260
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowersText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getFollowerText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :cond_6
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_7

    .line 264
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileFollowingText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getFollowingText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    :cond_7
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    if-eqz v7, :cond_8

    .line 268
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v4

    .line 269
    .local v4, "location":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_c

    .line 270
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 271
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setVisibility(I)V

    .line 272
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    .end local v4    # "location":Ljava/lang/String;
    :cond_8
    :goto_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    if-eqz v7, :cond_9

    .line 280
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "bio":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_d

    .line 282
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setVisibility(I)V

    .line 283
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    .end local v0    # "bio":Ljava/lang/String;
    :cond_9
    :goto_2
    const/4 v3, 0x0

    .line 290
    .local v3, "joinable":Z
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBroadcasting()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isViewingBroadcastsRestricted()Z

    move-result v7

    if-nez v7, :cond_e

    .line 291
    const/4 v3, 0x1

    .line 292
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setVisibility(I)V

    .line 293
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBroadcastCount()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setCount(Ljava/lang/String;)V

    .line 294
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getBroadcastingTitleString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setSubText(Ljava/lang/String;)V

    .line 299
    :goto_3
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isInJoinableParty()Z

    move-result v7

    if-eqz v7, :cond_10

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v7

    if-nez v7, :cond_10

    .line 300
    const/4 v3, 0x1

    .line 301
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setVisibility(I)V

    .line 302
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPartyCount()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setCount(Ljava/lang/String;)V

    .line 303
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPartyDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setSubText(Ljava/lang/String;)V

    .line 304
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPartyCount()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 305
    .local v5, "partyCount":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070a31

    .line 306
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v7, 0x1

    if-ne v5, v7, :cond_f

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070a33

    .line 307
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_4
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .line 308
    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getPartyDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 305
    invoke-virtual {v8, v7}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 314
    .end local v5    # "partyCount":I
    :goto_5
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinableTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_11

    const/4 v7, 0x0

    :goto_6
    invoke-virtual {v8, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 316
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->watermarkListAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;->clear()V

    .line 317
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->watermarkListAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getWatermarks()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenWatermarkListAdapter;->addAll(Ljava/util/Collection;)V

    .line 319
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isMeProfile()Z

    move-result v7

    if-nez v7, :cond_a

    .line 320
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->sendMessageBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBusy()Z

    move-result v7

    if-nez v7, :cond_12

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getIsBlocked()Z

    move-result v7

    if-nez v7, :cond_12

    const/4 v7, 0x1

    :goto_7
    invoke-virtual {v8, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 322
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isCallerFollowingTarget()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setChecked(Z)V

    .line 324
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getIsAddingUserToBlockList()Z

    move-result v7

    if-nez v7, :cond_13

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getIsRemovingUserFromBlockList()Z

    move-result v7

    if-nez v7, :cond_13

    .line 325
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    .line 326
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getIsBlocked()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setChecked(Z)V

    .line 332
    :cond_a
    :goto_8
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->followBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->hasLoadedBasicData()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    .line 334
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reportBtn:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->isBusy()Z

    move-result v7

    if-nez v7, :cond_14

    const/4 v7, 0x1

    :goto_9
    invoke-virtual {v8, v7}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 336
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->shouldShowQuarantinedBanner()Z

    move-result v7

    if-eqz v7, :cond_15

    .line 337
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->warningBanner:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->warningBannerText:Landroid/widget/TextView;

    const v8, 0x7f070d28

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 343
    :goto_a
    sget-object v7, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ReputationLevel:[I

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->getReputation()Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/ReputationLevel;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 354
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reputationBanner:Landroid/view/View;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 357
    :goto_b
    return-void

    .line 256
    .end local v3    # "joinable":Z
    :cond_b
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileRealName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 274
    .restart local v4    # "location":Ljava/lang/String;
    :cond_c
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationHeaderText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 275
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileLocationValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 285
    .end local v4    # "location":Ljava/lang/String;
    .restart local v0    # "bio":Ljava/lang/String;
    :cond_d
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBioValueText:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 296
    .end local v0    # "bio":Ljava/lang/String;
    .restart local v3    # "joinable":Z
    :cond_e
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileBroadcastButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 307
    .restart local v5    # "partyCount":I
    :cond_f
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f070a32

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_4

    .line 311
    .end local v5    # "partyCount":I
    :cond_10
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->profileJoinPartyButton:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 314
    :cond_11
    const/16 v7, 0x8

    goto/16 :goto_6

    .line 320
    :cond_12
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 328
    :cond_13
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->blockBtn:Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/IconFontToggleButton;->setEnabled(Z)V

    goto/16 :goto_8

    .line 334
    :cond_14
    const/4 v7, 0x0

    goto/16 :goto_9

    .line 340
    :cond_15
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->warningBanner:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a

    .line 345
    :pswitch_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reputationBanner:Landroid/view/View;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 346
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;->reputationBannerText:Landroid/widget/TextView;

    const v8, 0x7f070ae5

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    goto :goto_b

    .line 343
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
