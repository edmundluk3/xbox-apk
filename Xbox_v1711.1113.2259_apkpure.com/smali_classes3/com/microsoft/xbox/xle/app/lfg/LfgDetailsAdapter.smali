.class public Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "LfgDetailsAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementIdHashCode:I

.field private description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private headerLayout:Landroid/widget/RelativeLayout;

.field private interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

.field private interestedButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private shareButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tagBackgroundColor:I

.field private tagIdHashCode:I

.field private tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field private titleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private titleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 62
    const v3, 0x7f0c014e

    iput v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagBackgroundColor:I

    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .line 68
    const v3, 0x7f0e0744

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 69
    .local v0, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v3, 0x7f0e0742

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->headerLayout:Landroid/widget/RelativeLayout;

    .line 72
    const v3, 0x7f0e0746

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 73
    const v3, 0x7f0e0747

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 74
    const v3, 0x7f0e074a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 76
    const v3, 0x7f0e075a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 77
    const v3, 0x7f0e0758

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 78
    const v3, 0x7f0e075c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 79
    const v3, 0x7f0e074b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 81
    const v3, 0x7f0e0750

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 82
    .local v1, "interestedList":Landroid/support/v7/widget/RecyclerView;
    const v3, 0x7f0e074f

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 83
    const v3, 0x7f0e074e

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 84
    const v3, 0x7f0e074c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 85
    const v3, 0x7f0e074d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->shareButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 87
    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .line 97
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 99
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagBackgroundColor:I

    .line 101
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 103
    const v3, 0x7f0e0749

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 104
    .local v2, "tagsList":Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 105
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 107
    const v3, 0x7f0e0741

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 108
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->shareButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onTapCloseButton()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
    .param p1, "rosterItem"    # Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->getMemberSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 91
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
    .param p1, "rosterItem"    # Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->isHeaderRow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->declineMember(Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V

    .line 96
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->joinSession()V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->onTapInterestedButton()V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->navigateToEnforcement()V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showLfgViewAllDetailDialog()V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->showLfgShareDialog()V

    return-void
.end method

.method private updateJoinButton()V
    .locals 3

    .prologue
    .line 262
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isMemberOfSession()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0706ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 271
    :goto_1
    return-void

    .line 262
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isHost()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0706c7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->joinButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0706cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 32

    .prologue
    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSelectedTitle()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v25

    .line 122
    .local v25, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v13

    .line 123
    .local v13, "handleData":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->updateJoinButton()V

    .line 124
    if-eqz v25, :cond_2

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 127
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_0

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    move-object/from16 v28, v0

    const v29, 0x7f020122

    const v30, 0x7f020122

    invoke-virtual/range {v27 .. v30}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 138
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isBlockingBusy()Z

    move-result v27

    sget-object v28, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v29, 0x7f07013a

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->isHost()Z

    move-result v28

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->setIsHost(Z)V

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->shareButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->canShareLfg()Z

    move-result v28

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 144
    if-eqz v13, :cond_10

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v27

    if-eqz v27, :cond_5

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getMemberSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v14

    .line 148
    .local v14, "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v14, :cond_5

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->headerLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v27, v0

    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->primaryColor:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 150
    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->secondaryColor:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagBackgroundColor:I

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->clear()V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    new-instance v28, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    sget-object v29, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v30, 0x7f0706c9

    invoke-virtual/range {v29 .. v30}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->add(Ljava/lang/Object;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    new-instance v28, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v29, v0

    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getTimePlayedForMember(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-direct {v0, v14, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->add(Ljava/lang/Object;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getApprovedMembers()Ljava/util/List;

    move-result-object v10

    .line 159
    .local v10, "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v27

    if-nez v27, :cond_3

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    new-instance v28, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    sget-object v29, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v30, 0x7f0706c6

    invoke-virtual/range {v29 .. v30}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->add(Ljava/lang/Object;)V

    .line 162
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_1
    :goto_1
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_3

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 163
    .local v17, "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v28, v0

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getMemberSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v19

    .line 165
    .local v19, "profileSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v19, :cond_1

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v28, v0

    new-instance v29, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v30, v0

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getTimePlayedForMember(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V

    invoke-virtual/range {v28 .. v29}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 134
    .end local v10    # "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .end local v14    # "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v17    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v19    # "profileSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    const-string v28, ""

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->titleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v27, v0

    const v28, 0x7f020122

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 171
    .restart local v10    # "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .restart local v14    # "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getHostMember()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v27

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    .line 173
    .local v16, "isHost":Z
    if-eqz v16, :cond_4

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getInterestedMembers()Ljava/util/List;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v15

    .line 176
    .local v15, "interestedCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    move-object/from16 v28, v0

    if-lez v15, :cond_9

    const/16 v27, 0x1

    :goto_2
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    move-object/from16 v27, v0

    sget-object v28, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v29, 0x7f070713

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 183
    .end local v15    # "interestedCount":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->interestedAdapter:Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->notifyDataSetChanged()V

    .line 187
    .end local v10    # "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .end local v14    # "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v16    # "isHost":Z
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->updateJoinButton()V

    .line 189
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v27

    if-eqz v27, :cond_6

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v27

    if-eqz v27, :cond_6

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 192
    :cond_6
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v27

    if-eqz v27, :cond_7

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v27

    if-eqz v27, :cond_7

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    sget-object v28, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v29, 0x7f0706e5

    invoke-virtual/range {v28 .. v29}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 196
    :cond_7
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getRemainingNeedCount()I

    move-result v18

    .line 197
    .local v18, "needValue":I
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getConfirmedCount()I

    move-result v11

    .line 199
    .local v11, "confirmedCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    invoke-static/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    invoke-static/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v27, v0

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getStartTimeString()Ljava/lang/CharSequence;

    move-result-object v28

    const/16 v29, 0x0

    invoke-static/range {v27 .. v29}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 204
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v27

    if-eqz v27, :cond_10

    .line 205
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->tags()Ljava/util/List;

    move-result-object v22

    .line 206
    .local v22, "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->achievementIds()Ljava/util/List;

    move-result-object v6

    .line 207
    .local v6, "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->hashCode()I

    move-result v23

    .line 208
    .local v23, "tagIdsHashCode":I
    invoke-interface {v6}, Ljava/util/List;->hashCode()I

    move-result v7

    .line 212
    .local v7, "achievementIdsHashCode":I
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v27

    if-eqz v27, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagIdHashCode:I

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->achievementIdHashCode:I

    move/from16 v27, v0

    move/from16 v0, v27

    if-eq v0, v7, :cond_10

    .line 213
    :cond_8
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagIdHashCode:I

    .line 214
    move-object/from16 v0, p0

    iput v7, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->achievementIdHashCode:I

    .line 216
    move-object/from16 v0, v25

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v26

    .line 217
    .local v26, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 219
    .local v9, "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v8, "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v24, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v12, "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    if-eqz v25, :cond_a

    invoke-virtual/range {v25 .. v25}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v27

    if-eqz v27, :cond_a

    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v27

    if-eqz v27, :cond_a

    .line 224
    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_3
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 225
    .local v4, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v0, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v9, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 176
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "achievementIdsHashCode":I
    .end local v8    # "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    .end local v9    # "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v11    # "confirmedCount":I
    .end local v12    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v18    # "needValue":I
    .end local v22    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v23    # "tagIdsHashCode":I
    .end local v24    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v26    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    .restart local v10    # "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .restart local v14    # "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .restart local v15    # "interestedCount":I
    .restart local v16    # "isHost":Z
    :cond_9
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 227
    .end local v10    # "approvedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .end local v14    # "hostProfile":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v15    # "interestedCount":I
    .end local v16    # "isHost":Z
    .restart local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v7    # "achievementIdsHashCode":I
    .restart local v8    # "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    .restart local v9    # "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .restart local v11    # "confirmedCount":I
    .restart local v12    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .restart local v18    # "needValue":I
    .restart local v22    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v23    # "tagIdsHashCode":I
    .restart local v24    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .restart local v26    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_a
    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v27

    if-eqz v27, :cond_b

    .line 228
    invoke-virtual/range {v26 .. v26}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_4
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 229
    .restart local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v0, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v9, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 233
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_b
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_c
    :goto_5
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_d

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 234
    .local v5, "achievementId":Ljava/lang/String;
    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 236
    .restart local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    if-eqz v4, :cond_c

    .line 237
    invoke-static {v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->with(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 241
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v5    # "achievementId":Ljava/lang/String;
    :cond_d
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_6
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_f

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 242
    .local v21, "tagId":Ljava/lang/String;
    sget-object v28, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v20

    .line 244
    .local v20, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v20, :cond_e

    .line 245
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 247
    :cond_e
    invoke-static/range {v21 .. v21}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 251
    .end local v20    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v21    # "tagId":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsAdapter;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 259
    .end local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "achievementIdsHashCode":I
    .end local v8    # "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    .end local v9    # "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v11    # "confirmedCount":I
    .end local v12    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v18    # "needValue":I
    .end local v22    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v23    # "tagIdsHashCode":I
    .end local v24    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v26    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_10
    return-void
.end method
