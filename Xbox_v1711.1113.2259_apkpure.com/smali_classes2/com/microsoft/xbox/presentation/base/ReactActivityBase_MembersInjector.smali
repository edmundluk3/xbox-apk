.class public final Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;
.super Ljava/lang/Object;
.source "ReactActivityBase_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/presentation/base/ReactActivityBase;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final reactInstanceManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->reactInstanceManagerProvider:Ljavax/inject/Provider;

    .line 19
    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/base/ReactActivityBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectReactInstanceManager(Lcom/microsoft/xbox/presentation/base/ReactActivityBase;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/base/ReactActivityBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/base/ReactActivityBase;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactInstanceManager;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 37
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/presentation/base/ReactActivityBase;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/presentation/base/ReactActivityBase;

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->reactInstanceManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactInstanceManager;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/base/ReactActivityBase;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 32
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/microsoft/xbox/presentation/base/ReactActivityBase;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->injectMembers(Lcom/microsoft/xbox/presentation/base/ReactActivityBase;)V

    return-void
.end method
