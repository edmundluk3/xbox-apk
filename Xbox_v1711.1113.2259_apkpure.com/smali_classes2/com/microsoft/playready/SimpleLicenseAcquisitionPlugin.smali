.class public Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;
.super Ljava/lang/Object;
.source "SimpleLicenseAcquisitionPlugin.java"

# interfaces
.implements Lcom/microsoft/playready/ILicenseAcquisitionPlugin;


# instance fields
.field private mChallengeCustomData:Ljava/lang/String;

.field private mCurrentDomainInfo:Lcom/microsoft/playready/DomainInfo;

.field private mLicenseServerUriOverride:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mLicenseServerUriOverride:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mChallengeCustomData:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mCurrentDomainInfo:Lcom/microsoft/playready/DomainInfo;

    .line 17
    return-void
.end method


# virtual methods
.method public doLicenseRequest([BLjava/lang/String;)[B
    .locals 13
    .param p1, "licenseChallenge"    # [B
    .param p2, "embeddedLicenseServerUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 68
    const/4 v2, 0x0

    .line 69
    .local v2, "licenseResponse":Lcom/microsoft/playready/HttpResponse;
    const/4 v3, 0x0

    .line 70
    .local v3, "retryCount":I
    const/4 v1, 0x0

    .line 71
    .local v1, "fSuccess":Z
    move-object v4, p2

    .line 72
    .local v4, "serverUri":Ljava/lang/String;
    const/4 v5, 0x0

    .line 74
    .local v5, "urlToTry":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mLicenseServerUriOverride:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 76
    iget-object v4, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mLicenseServerUriOverride:Ljava/lang/String;

    .line 79
    :cond_0
    move-object v5, v4

    .line 83
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 85
    new-instance v6, Ljava/lang/InterruptedException;

    invoke-direct {v6}, Ljava/lang/InterruptedException;-><init>()V

    throw v6

    .line 92
    :cond_2
    const/4 v6, 0x0

    .line 93
    :try_start_0
    const-string v7, "Content-Type: text/xml; charset=utf-8\r\nSOAPAction: \"http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense\"\r\n"

    .line 90
    invoke-static {v5, v6, v7, p1}, Lcom/microsoft/playready/HttpClient;->doTransaction(Ljava/lang/String;Lcom/microsoft/playready/HttpClient$ConnectionSpec;Ljava/lang/String;[B)Lcom/microsoft/playready/HttpResponse;
    :try_end_0
    .catch Lcom/microsoft/playready/HttpRedirectRequestedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 96
    const/4 v1, 0x1

    .line 114
    :goto_0
    if-eqz v1, :cond_1

    .line 116
    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v6

    const/16 v7, 0x1f4

    if-ne v6, v7, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getResponse()[B

    move-result-object v6

    if-nez v6, :cond_6

    .line 123
    :cond_3
    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v6

    const/16 v7, 0xc8

    if-lt v6, v7, :cond_4

    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v6

    const/16 v7, 0x12b

    if-le v6, v7, :cond_6

    .line 129
    :cond_4
    new-instance v6, Lcom/microsoft/playready/HttpException;

    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v7

    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getStatusMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/microsoft/playready/HttpException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v6

    .line 98
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Lcom/microsoft/playready/HttpRedirectRequestedException;
    sget v6, Lcom/microsoft/playready/HttpClient;->RECCOMENDED_RETRY_COUNT:I

    if-ge v3, v6, :cond_5

    .line 102
    const-string v6, "LicenseAcquirer"

    const-string v7, "License server redirected ( %d ) from %s to %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    aput-object v5, v8, v12

    const/4 v9, 0x2

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpRedirectRequestedException;->getRedirectUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    add-int/lit8 v3, v3, 0x1

    .line 104
    invoke-virtual {v0}, Lcom/microsoft/playready/HttpRedirectRequestedException;->getRedirectUrl()Ljava/lang/String;

    move-result-object v5

    .line 105
    goto :goto_0

    .line 108
    :cond_5
    const-string v6, "LicenseAcquirer"

    const-string v7, "Exceeded maximum number of redirects attempting to reach %s, giving up."

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v4, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    throw v0

    .line 132
    .end local v0    # "e":Lcom/microsoft/playready/HttpRedirectRequestedException;
    :cond_6
    invoke-virtual {v2}, Lcom/microsoft/playready/HttpResponse;->getResponse()[B

    move-result-object v6

    return-object v6
.end method

.method public getChallengeCustomData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mChallengeCustomData:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentDomainInfo()Lcom/microsoft/playready/DomainInfo;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mCurrentDomainInfo:Lcom/microsoft/playready/DomainInfo;

    return-object v0
.end method

.method public getLicenseServerUriOverride()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mLicenseServerUriOverride:Ljava/lang/String;

    return-object v0
.end method

.method public setChallengeCustomData(Ljava/lang/String;)V
    .locals 0
    .param p1, "customData"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mChallengeCustomData:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setCurrentDomainInfo(Lcom/microsoft/playready/DomainInfo;)V
    .locals 0
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mCurrentDomainInfo:Lcom/microsoft/playready/DomainInfo;

    .line 54
    return-void
.end method

.method public setLicenseServerUriOverride(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 25
    const/4 v1, 0x0

    .line 27
    .local v1, "urlString":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 30
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "u":Ljava/net/URL;
    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    .line 34
    .end local v0    # "u":Ljava/net/URL;
    :cond_0
    iput-object v1, p0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;->mLicenseServerUriOverride:Ljava/lang/String;

    .line 35
    return-void
.end method
