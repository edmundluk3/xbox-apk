.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;
.super Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
.source "$AutoValue_ClubModerationDataTypes_ClubReportedItem.java"


# instance fields
.field private final contentId:Ljava/lang/String;

.field private final contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

.field private final lastReported:Ljava/util/Date;

.field private final reportCount:I

.field private final reportId:J

.field private final reports:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;Ljava/util/Date;IJLcom/google/common/collect/ImmutableList;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "contentType"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "lastReported"    # Ljava/util/Date;
    .param p4, "reportCount"    # I
    .param p5, "reportId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;",
            "Ljava/util/Date;",
            "IJ",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p7, "reports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;-><init>()V

    .line 27
    if-nez p1, :cond_0

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null contentId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentId:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 32
    if-nez p3, :cond_1

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null lastReported"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->lastReported:Ljava/util/Date;

    .line 36
    iput p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportCount:I

    .line 37
    iput-wide p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    .line 38
    if-nez p7, :cond_2

    .line 39
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null reports"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_2
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reports:Lcom/google/common/collect/ImmutableList;

    .line 42
    return-void
.end method


# virtual methods
.method public contentId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentId:Ljava/lang/String;

    return-object v0
.end method

.method public contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    if-ne p1, p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 94
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .line 95
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    if-nez v3, :cond_3

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->lastReported:Ljava/util/Date;

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportCount:I

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reportCount()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reportId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reports:Lcom/google/common/collect/ImmutableList;

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reports()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 96
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    :cond_4
    move v1, v2

    .line 102
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 107
    const/4 v0, 0x1

    .line 108
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v8

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 112
    mul-int/2addr v0, v8

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->lastReported:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 114
    mul-int/2addr v0, v8

    .line 115
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportCount:I

    xor-int/2addr v0, v1

    .line 116
    mul-int/2addr v0, v8

    .line 117
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 118
    mul-int/2addr v0, v8

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reports:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 120
    return v0

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public lastReported()Ljava/util/Date;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->lastReported:Ljava/util/Date;

    return-object v0
.end method

.method public reportCount()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportCount:I

    return v0
.end method

.method public reportId()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    return-wide v0
.end method

.method public reports()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reports:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubReportedItem{contentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->contentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastReported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->lastReported:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reportId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;->reports:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
