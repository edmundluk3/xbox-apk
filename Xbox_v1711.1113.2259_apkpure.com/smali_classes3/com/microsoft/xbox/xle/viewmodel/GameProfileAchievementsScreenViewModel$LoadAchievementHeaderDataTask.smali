.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAchievementHeaderDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

.field private titleModel:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;JLjava/lang/String;)V
    .locals 2
    .param p2, "titleId"    # J
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 592
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 589
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 593
    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 594
    invoke-static {p4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 595
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCompareAchievements(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 629
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_3

    .line 630
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 631
    .local v0, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleStatisticsForUser(ZLjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 632
    const-string v2, "LoadAchievementHeaderDataTask"

    const-string v3, "Failed to load statistics"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadTitleProgress(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 636
    const-string v2, "LoadAchievementHeaderDataTask"

    const-string v3, "Unable to get title details"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneCompareAchievements(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 640
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 641
    const-string v2, "LoadAchievementHeaderDataTask"

    const-string v3, "Unable to get title achievements"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    .end local v0    # "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    :goto_0
    return-object v1

    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 624
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 604
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 609
    :goto_0
    return-void

    .line 607
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 619
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 620
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 587
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 615
    return-void
.end method
