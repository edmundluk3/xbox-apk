.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EligibilityProperties"
.end annotation


# instance fields
.field private final affirmations:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Affirmations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;",
            ">;"
        }
    .end annotation
.end field

.field private final remediations:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Remediations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Remediation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Remediation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 958
    .local p1, "affirmations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;>;"
    .local p2, "remediations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Remediation;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 959
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 960
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 962
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;->affirmations:Ljava/util/List;

    .line 963
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;->remediations:Ljava/util/List;

    .line 964
    return-void
.end method


# virtual methods
.method public getSubscriptionType(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 3
    .param p1, "affirmationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 968
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 969
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;->affirmations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;

    .line 970
    .local v0, "affirmation":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;
    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;->affirmationId:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;->affirmationProductId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 971
    iget-object v1, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;->affirmationProductId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->getSubscriptionFromAffirmationProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v1

    .line 976
    .end local v0    # "affirmation":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    goto :goto_0
.end method
