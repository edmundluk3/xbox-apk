.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameAchievementComparisonViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMeComparisonDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 498
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshTitleProgress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleImages(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unable to get title image details for title: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadTitleProgress(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 538
    :goto_0
    return-object v0

    .line 537
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProfileModel not available for current user"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 522
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 504
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 505
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 506
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 518
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 494
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 510
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 511
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1002(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 512
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 513
    return-void
.end method
