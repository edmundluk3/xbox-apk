.class public Lcom/microsoft/xbox/service/model/MessageModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;,
        Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$DeleteSkypeConversationRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;,
        Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final CONSUMPTION_HORIZON_TUPLE_FORMAT:Ljava/lang/String; = "%s;0;0"

.field private static final CONVERSATION_PAGINATION_COUNT:I = 0x1

.field private static final MAX_MESSAGE_DETAIL_MODELS:I = 0xc8

.field private static final MAX_TITLE_ENTRIES:I = 0xa

.field private static final NOT_FOUND:I = -0x1

.field private static final SERVICE_SENDER_GAMERTAG:Ljava/lang/String; = "Xbox Live"

.field private static final SKYPE_CONVERSATION_UPDATE_LIFETIME:J = 0x0L

.field private static final SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

.field public static final SKYPE_USER_ID_FORMAT:Ljava/lang/String; = "8:xbox:%s"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final conversationDetailsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

.field private conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private currentConversationXuid:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isDeleting:Z

.field private isDeletingConversation:Z

.field private isSending:Z

.field private final lastRefreshConversationDetailsTime:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;"
        }
    .end annotation
.end field

.field private skypeLongPollLocationUrl:Ljava/lang/String;

.field private subscribeSkypeLongPollLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private final typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private unreadMessageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSkypeGroupMessageService()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 102
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationDetailsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 103
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshConversationDetailsTime:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 107
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 131
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/model/MessageModel;)V

    .line 133
    iput v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->unreadMessageCount:I

    .line 134
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isLoading:Z

    .line 137
    const-wide/32 v0, 0x36ee80

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    .line 139
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    .line 142
    new-instance v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;)Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;-><init>(Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    .line 145
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel$1;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .prologue
    .line 89
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->getRealNameForUser(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    .prologue
    .line 89
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->buildConversationListFromSkypeMessageList(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->onGetConversationsListCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->onSubscribeSkypeLongPollCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->onSkypeLongPollCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/MessageModel;->onGetSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel;->onAddMemberToConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel;->onUpdateTopicNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->onMuteConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->onLeaveConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel;->onSkypeDeleteMessageCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->onDeleteSkypeConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    .prologue
    .line 89
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->checkPermissions(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->onSendMessageWithAttachmentCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->onSendMessageCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static buildConversationListFromSkypeMessageList(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 14
    .param p0, "list"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v12, -0x1

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1325
    if-nez p0, :cond_1

    .line 1394
    :cond_0
    return-object v3

    .line 1329
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1333
    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;-><init>()V

    .line 1334
    .local v3, "conversations":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    .line 1336
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v5, :cond_2

    .line 1337
    new-instance v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;-><init>()V

    iput-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    .line 1338
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    iput-object v8, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    .line 1339
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget v8, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    iput v8, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    .line 1342
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    .line 1343
    .local v2, "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    new-instance v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;-><init>()V

    .line 1345
    .local v4, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->senderGamerTag:Ljava/lang/String;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 1346
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->gamerPicUrl:Ljava/lang/String;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    .line 1347
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->realName:Ljava/lang/String;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    .line 1348
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 1350
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    if-eqz v5, :cond_5

    .line 1351
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->clearedat:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1352
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->clearedat:Ljava/lang/String;

    invoke-static {v5, v12, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1353
    .local v0, "clearedTimeInMs":J
    cmp-long v5, v0, v12

    if-lez v5, :cond_4

    .line 1354
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->clearedAt:Ljava/util/Date;

    .line 1358
    .end local v0    # "clearedTimeInMs":J
    :cond_4
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->alerts:Ljava/lang/Boolean;

    if-eqz v5, :cond_8

    .line 1359
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->alerts:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    .line 1363
    :goto_1
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;->getConsumptionHorizon()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->consumptionHorizon:Ljava/lang/String;

    .line 1366
    :cond_5
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    if-eqz v5, :cond_6

    .line 1367
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;->topic:Ljava/lang/String;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    .line 1370
    :cond_6
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    .line 1371
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeServiceMessage(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    .line 1372
    iget-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v5, :cond_9

    iget-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-nez v5, :cond_9

    invoke-static {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Ljava/lang/String;

    move-result-object v5

    :goto_2
    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 1373
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1374
    iget-boolean v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->unreadConversation:Z

    if-eqz v5, :cond_a

    move v5, v6

    :goto_3
    iput v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    .line 1375
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    .line 1376
    new-instance v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;-><init>()V

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .line 1377
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iput-object v9, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    .line 1378
    iget-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v5, :cond_7

    .line 1379
    const-string v5, "Xbox Live"

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 1382
    :cond_7
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderSkypeId()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    .line 1383
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    .line 1384
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iput-object v9, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->sentTime:Ljava/util/Date;

    .line 1385
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iput-boolean v7, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAudio:Z

    .line 1386
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iput-boolean v7, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasPhoto:Z

    .line 1387
    iget-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v9, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    const-wide/16 v10, 0x0

    invoke-static {v9, v10, v11}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageId:J

    .line 1388
    iget-object v9, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-boolean v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v5, :cond_b

    sget-object v5, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->Service:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_4
    iput-object v5, v9, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageType:Ljava/lang/String;

    .line 1390
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1361
    :cond_8
    iput-boolean v6, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    goto/16 :goto_1

    .line 1372
    :cond_9
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    goto :goto_2

    :cond_a
    move v5, v7

    .line 1374
    goto :goto_3

    .line 1388
    :cond_b
    sget-object v5, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->User:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4
.end method

.method private capNumberOfConversations()V
    .locals 4

    .prologue
    const/16 v3, 0x3e8

    .line 1518
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v1, v3, :cond_0

    .line 1519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1520
    .local v0, "summaryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1521
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1522
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1524
    .end local v0    # "summaryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    :cond_0
    return-void
.end method

.method private static checkPermissions(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .locals 6
    .param p0, "response"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1156
    if-eqz p0, :cond_2

    .line 1157
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;->getPermissionResponseList()Ljava/util/List;

    move-result-object v2

    .line 1158
    .local v2, "responses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1159
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;

    .line 1160
    .local v0, "permissionResponse":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;
    if-eqz v0, :cond_0

    .line 1161
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;->getPermissions()Ljava/util/List;

    move-result-object v1

    .line 1162
    .local v1, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1163
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;

    .line 1164
    .local v3, "userPermission":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->getIsAllowed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1165
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->getReason()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v4

    .line 1176
    .end local v0    # "permissionResponse":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;
    .end local v1    # "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;>;"
    .end local v2    # "responses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;>;"
    .end local v3    # "userPermission":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;
    :goto_0
    return-object v4

    .line 1171
    .restart local v2    # "responses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;>;"
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 1175
    .end local v2    # "responses":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;>;"
    :cond_2
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "Unexpected data returned from permission service"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    goto :goto_0
.end method

.method private findConversationSummaryById(Ljava/lang/String;)I
    .locals 6
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 729
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 730
    const/4 v1, 0x0

    .line 734
    .local v1, "response":I
    invoke-static {p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 736
    .local v3, "xuidFromSkypeId":Ljava/lang/String;
    move-object v0, p1

    .line 737
    .local v0, "id":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 739
    move-object v0, v3

    .line 742
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 743
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 744
    .local v2, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 753
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "response":I
    .end local v2    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v3    # "xuidFromSkypeId":Ljava/lang/String;
    :goto_1
    return v1

    .line 748
    .restart local v0    # "id":Ljava/lang/String;
    .restart local v1    # "response":I
    .restart local v2    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .restart local v3    # "xuidFromSkypeId":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 749
    goto :goto_0

    .line 753
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "response":I
    .end local v2    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v3    # "xuidFromSkypeId":Ljava/lang/String;
    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private findSkypeMessageById(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/lang/String;)I
    .locals 4
    .param p1, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "messageId"    # Ljava/lang/String;

    .prologue
    .line 713
    const/4 v1, 0x0

    .line 715
    .local v1, "response":I
    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 716
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 717
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 724
    .end local v0    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :goto_1
    return v2

    .line 720
    .restart local v0    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 721
    goto :goto_0

    .line 724
    .end local v0    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/MessageModel;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 176
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->access$100()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    return-object v0
.end method

.method private static getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;
    .locals 4
    .param p0, "user"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "settingId"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1312
    if-eqz p0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1313
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;

    .line 1314
    .local v0, "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1315
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 1320
    .end local v0    # "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getRealNameForUser(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;
    .locals 2
    .param p0, "user"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 758
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 759
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 760
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 761
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    .line 765
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getXuidsFromConversations(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "conversations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    const/4 v3, 0x0

    .line 541
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 553
    :goto_0
    return-object v3

    .line 545
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 546
    .local v2, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 547
    .local v1, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-boolean v5, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v5, :cond_2

    iget-boolean v5, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-nez v5, :cond_2

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 548
    .local v0, "senderXuid":Ljava/lang/String;
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 549
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v0    # "senderXuid":Ljava/lang/String;
    :cond_2
    move-object v0, v3

    .line 547
    goto :goto_2

    .end local v1    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_3
    move-object v3, v2

    .line 553
    goto :goto_0
.end method

.method private isConversationEmpty(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z
    .locals 4
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1501
    if-eqz p1, :cond_1

    .line 1502
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v2

    .line 1503
    .local v2, "messageListResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v2, :cond_1

    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1504
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->removeMessagesBeforeConversationClearedAtTime(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/lang/String;)V

    .line 1505
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "index":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1506
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1507
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1508
    const/4 v3, 0x0

    .line 1514
    .end local v0    # "index":I
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v2    # "messageListResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :goto_1
    return v3

    .line 1505
    .restart local v0    # "index":I
    .restart local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .restart local v2    # "messageListResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1514
    .end local v0    # "index":I
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v2    # "messageListResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method static synthetic lambda$canContributeToGroupConversationAsync$7(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 11
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "xuids"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "statusKey"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;

    .prologue
    .line 447
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    .line 448
    .local v2, "meXuid":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 449
    .local v7, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 450
    .local v8, "xuid":Ljava/lang/String;
    new-instance v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;

    invoke-direct {v6, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;-><init>(Ljava/lang/String;)V

    .line 451
    .local v6, "u":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 454
    .end local v6    # "u":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;
    .end local v8    # "xuid":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 455
    .local v3, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v9, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    :try_start_0
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    invoke-interface {v9, v2, v3, v7}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->getUserPermissions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    move-result-object v4

    .line 459
    .local v4, "response":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
    invoke-static {v4}, Lcom/microsoft/xbox/service/model/MessageModel;->checkPermissions(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v5

    .line 460
    .local v5, "result":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    if-eqz v5, :cond_1

    .line 461
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v10, "Privacy check failed for at least one or more user"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 465
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v5, :cond_2

    .line 466
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, p2, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_2
    invoke-static {p0, p3, v0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/UpdateType;Landroid/os/Bundle;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 477
    .end local v4    # "response":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
    .end local v5    # "result":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    :goto_1
    return-void

    .line 470
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 471
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v10, "Error getting privacy check data from service"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 473
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const/4 v9, 0x0

    invoke-virtual {v0, p2, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 475
    invoke-static {p0, p3, v0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/UpdateType;Landroid/os/Bundle;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 143
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 144
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MessageUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/UpdateType;Landroid/os/Bundle;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 469
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2, p2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/UpdateType;Landroid/os/Bundle;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 475
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2, p2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 530
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationProfilePresenceUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic lambda$saveSkypeConversationMessageCache$2(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 333
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredSkypeConversationMessageCache(Ljava/util/concurrent/ConcurrentHashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v2, "Error saving skypeConversationMessagesCache to storage"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$updateConsumptionHorizonAsync$3(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 14
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 351
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 352
    .local v0, "consumptionHorizonTime":Ljava/lang/Long;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s;0;0"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "currentUTCTime":Ljava/lang/String;
    :try_start_0
    iget-boolean v7, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v7, :cond_0

    iget-boolean v7, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v7, :cond_2

    :cond_0
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 359
    .local v4, "senderXuid":Ljava/lang/String;
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel;->SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    invoke-interface {v7, v4, v1}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->updateConsumptionHorizon(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v3, 0x1

    .line 360
    .local v3, "result":Z
    :goto_1
    if-nez v3, :cond_4

    .line 361
    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Error updating consumption horizon %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v12, 0x264b

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    .end local v3    # "result":Z
    .end local v4    # "senderXuid":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 355
    :cond_2
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "8:xbox:%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    aput-object v11, v9, v10

    .line 357
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 359
    .restart local v4    # "senderXuid":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 363
    .restart local v3    # "result":Z
    :cond_4
    iget-object v7, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v6

    .line 364
    .local v6, "summaryIndex":I
    const/4 v7, -0x1

    if-gt v6, v7, :cond_5

    .line 365
    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "skype conversation doesn\'t exist in summary list ! "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 376
    .end local v3    # "result":Z
    .end local v4    # "senderXuid":Ljava/lang/String;
    .end local v6    # "summaryIndex":I
    :catch_0
    move-exception v2

    .line 377
    .local v2, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Error updating consumption horizon %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v12, 0x264b

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 367
    .end local v2    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v3    # "result":Z
    .restart local v4    # "senderXuid":Ljava/lang/String;
    .restart local v6    # "summaryIndex":I
    :cond_5
    :try_start_1
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 368
    .local v5, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iput-object v1, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->consumptionHorizon:Ljava/lang/String;

    .line 369
    iget v7, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    if-lez v7, :cond_1

    .line 370
    iget v7, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    .line 371
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateUnreadCount()V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method static synthetic lambda$updateConsumptionHorizonAsync$4(Lcom/microsoft/xbox/service/model/MessageModel;ZLjava/lang/String;)V
    .locals 16
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "isGroupConversation"    # Z
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 390
    .local v2, "consumptionHorizonTime":Ljava/lang/Long;
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%s;0;0"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "currentUTCTime":Ljava/lang/String;
    if-eqz p1, :cond_0

    move-object/from16 v6, p2

    .line 395
    .local v6, "senderXuid":Ljava/lang/String;
    :goto_0
    :try_start_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->SKYPE_GROUP_MESSAGE_SERVICE:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    invoke-interface {v9, v6, v3}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->updateConsumptionHorizon(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v5, 0x1

    .line 396
    .local v5, "result":Z
    :goto_1
    if-nez v5, :cond_2

    .line 397
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Error updating consumption horizon %d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-wide/16 v14, 0x264b

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    .end local v5    # "result":Z
    .end local v6    # "senderXuid":Ljava/lang/String;
    :goto_2
    return-void

    .line 393
    :cond_0
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "8:xbox:%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 395
    .restart local v6    # "senderXuid":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 399
    .restart local v5    # "result":Z
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v8

    .line 400
    .local v8, "summaryIndex":I
    const/4 v9, -0x1

    if-gt v8, v9, :cond_3

    .line 401
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "skype conversation doesn\'t exist in summary list ! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 408
    .end local v5    # "result":Z
    .end local v6    # "senderXuid":Ljava/lang/String;
    .end local v8    # "summaryIndex":I
    :catch_0
    move-exception v4

    .line 409
    .local v4, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v9, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Error updating consumption horizon %d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-wide/16 v14, 0x264b

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 403
    .end local v4    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v5    # "result":Z
    .restart local v6    # "senderXuid":Ljava/lang/String;
    .restart local v8    # "summaryIndex":I
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 404
    .local v7, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iput-object v3, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->consumptionHorizon:Ljava/lang/String;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method static synthetic lambda$updateProfileAndPresenceDataAsync$9(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/ArrayList;)V
    .locals 15
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p1, "conversations"    # Ljava/util/ArrayList;

    .prologue
    .line 490
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getXuidsFromConversations(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    .line 491
    .local v5, "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 492
    new-instance v10, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;

    invoke-direct {v10, v5}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;-><init>(Ljava/util/ArrayList;)V

    .line 493
    .local v10, "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v12

    invoke-static {v10}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;->getUserPresenceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserListPresenceInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;

    move-result-object v2

    .line 495
    .local v2, "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    new-instance v6, Ljava/util/Hashtable;

    invoke-direct {v6}, Ljava/util/Hashtable;-><init>()V

    .line 496
    .local v6, "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    if-eqz v2, :cond_2

    iget-object v12, v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 497
    iget-object v12, v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    .line 498
    .local v8, "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    iget-object v13, v8, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 499
    iget-object v13, v8, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    iget-object v14, v8, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-static {v14}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v14

    invoke-virtual {v6, v13, v14}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 532
    .end local v2    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .end local v5    # "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .end local v8    # "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    .end local v10    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v12, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    :goto_1
    return-void

    .line 504
    .restart local v2    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .restart local v5    # "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .restart local v10    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    :cond_2
    :try_start_1
    new-instance v11, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    invoke-direct {v11, v5}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 505
    .local v11, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v12

    invoke-static {v11}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v3

    .line 507
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v3, :cond_6

    iget-object v12, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_6

    .line 508
    iget-object v12, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 509
    .local v9, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    sget-object v12, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-static {v9, v12}, Lcom/microsoft/xbox/service/model/MessageModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v1

    .line 511
    .local v1, "imageUrl":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_4
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 512
    .local v7, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-boolean v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-nez v12, :cond_4

    iget-boolean v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v12, :cond_4

    .line 514
    iget-object v4, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 515
    .local v4, "senderXuid":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    iget-object v12, v9, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 516
    iput-object v1, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    .line 517
    invoke-static {v9}, Lcom/microsoft/xbox/service/model/MessageModel;->getRealNameForUser(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    .line 518
    invoke-virtual {v6, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 519
    iget-object v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    if-nez v12, :cond_5

    .line 520
    sget-object v12, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 523
    :cond_5
    sget-object v12, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-static {v9, v12}, Lcom/microsoft/xbox/service/model/MessageModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_2

    .line 530
    .end local v1    # "imageUrl":Ljava/lang/String;
    .end local v4    # "senderXuid":Ljava/lang/String;
    .end local v7    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v9    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_6
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;)Ljava/lang/Runnable;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private loadSkypeConversationMessagesAsync(Ljava/lang/String;Z)V
    .locals 12
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "isGroupConversation"    # Z

    .prologue
    const/4 v11, 0x1

    .line 2353
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v7

    .line 2355
    .local v7, "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    if-nez v0, :cond_0

    .line 2356
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 2359
    :cond_0
    if-eqz v7, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v0, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2360
    :cond_1
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshTime:Ljava/util/Date;

    new-instance v10, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v10}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;ZLjava/lang/String;)V

    move v1, v11

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 2364
    :goto_0
    return-void

    .line 2362
    :cond_2
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshTime:Ljava/util/Date;

    new-instance v10, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v10}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;

    iget-object v1, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v5, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;ZLjava/lang/String;)V

    move v1, v11

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    goto :goto_0
.end method

.method private onAddMemberToConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "conversationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2322
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "onAddMemberToConversationCompleted "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2323
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 2324
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "%s successfully added to %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    aput-object p3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328
    :goto_0
    return-void

    .line 2326
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to add %s to %s conversation"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    aput-object p3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onDeleteSkypeConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 834
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 836
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v3, v6, :cond_5

    .line 837
    sget-object v3, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v6, "Skype conversation deleted"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v2

    .line 839
    .local v2, "summaryIndex":I
    const/4 v3, -0x1

    if-gt v2, v3, :cond_0

    .line 840
    sget-object v3, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "the conversation doesn\'t exist in the summary list "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    :goto_0
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    .line 851
    .local v0, "conversationDetails":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v0, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 852
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveSkypeConversationMessageCache()V

    .line 860
    .end local v0    # "conversationDetails":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v2    # "summaryIndex":I
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateUnreadCount()V

    .line 861
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeletingConversation:Z

    .line 862
    new-instance v3, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v6, v5}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v5

    invoke-direct {v3, v4, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 863
    return-void

    .line 842
    .restart local v2    # "summaryIndex":I
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 843
    .local v1, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v1, :cond_2

    .line 844
    iget-boolean v3, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v3, :cond_1

    iget-boolean v3, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v3, :cond_3

    :cond_1
    move v3, v5

    :goto_2
    invoke-direct {p0, p2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->updateConsumptionHorizonAsync(Ljava/lang/String;Z)V

    .line 846
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 847
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 844
    goto :goto_2

    .line 854
    .end local v1    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .restart local v0    # "conversationDetails":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v6, "The id doesn\'t exist in skype conversation detail cache"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 857
    .end local v0    # "conversationDetails":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v2    # "summaryIndex":I
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v6, "Skype conversation not deleted"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onGetConversationsListCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "syncState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;>;"
    const/4 v6, 0x0

    .line 1398
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1399
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1401
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v4, v5, :cond_b

    .line 1402
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->setSuccess()V

    .line 1404
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 1405
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "Conversation list fetched"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iput-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .line 1409
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1410
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1413
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v4, :cond_1

    .line 1414
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iput-object v5, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    .line 1417
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Loaded "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " conversations"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1420
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1421
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1422
    .local v2, "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 1423
    .local v3, "responseItem":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 1424
    .local v1, "item":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1425
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1417
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v2    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    .end local v3    # "responseItem":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 1431
    .restart local v2    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1432
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1439
    .end local v2    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1441
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Conversation list now contains "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " conversations"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->capNumberOfConversations()V

    .line 1455
    :goto_3
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 1456
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isLoadingConversation()Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1457
    :cond_7
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeEmptyConversations()V

    .line 1460
    :cond_8
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateConversationUnreadCount()V

    .line 1462
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateUnreadCount()V

    .line 1464
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 1465
    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    invoke-direct {v4, v5, p0, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1466
    return-void

    .line 1434
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :cond_9
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1435
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1445
    :cond_a
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "No new conversations to fetch"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    iput-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    goto :goto_3

    .line 1449
    :cond_b
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "Conversation list not fetched"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    iput-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .line 1452
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->setFailed(Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_3
.end method

.method private onGetSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 17
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "resetSyncState"    # Z
    .param p4, "syncState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1943
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1945
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v13

    sget-object v14, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v13, v14, :cond_12

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_12

    .line 1946
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->setSuccess()V

    .line 1948
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshConversationDetailsTime:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 1949
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshConversationDetailsTime:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1951
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    .line 1952
    .local v4, "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v4, :cond_11

    .line 1955
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1957
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 1958
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v3

    .line 1963
    .local v3, "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-nez v3, :cond_1

    .line 1964
    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    .end local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;-><init>()V

    .line 1967
    .restart local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_1
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    iput-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    .line 1969
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    if-eqz v13, :cond_3

    .line 1970
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    if-eqz v13, :cond_2

    .line 1972
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    .line 1973
    .local v6, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 1974
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1975
    .local v5, "id":Ljava/lang/String;
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    iget-object v15, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    invoke-interface {v15, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v14, v5, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1979
    .end local v5    # "id":Ljava/lang/String;
    .end local v6    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    iput-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    .line 1984
    :cond_3
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v13, :cond_5

    .line 1985
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-nez v13, :cond_4

    .line 1986
    new-instance v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    invoke-direct {v13}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;-><init>()V

    iput-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    .line 1988
    :cond_4
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v14, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    iput-object v14, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    .line 1989
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v14, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    iput-object v14, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    .line 1990
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v14, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->forwardLink:Ljava/lang/String;

    iput-object v14, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->forwardLink:Ljava/lang/String;

    .line 1991
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v14, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget v14, v14, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    iput v14, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    .line 1994
    :cond_5
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v13

    if-nez v13, :cond_c

    .line 1995
    iget-object v13, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_6
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 1996
    .local v10, "newMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    const/4 v8, 0x0

    .line 1997
    .local v8, "messageFound":Z
    const/4 v9, 0x0

    .line 1999
    .local v9, "messageToDelete":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v14}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 2000
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_7
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 2002
    .local v7, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v15, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    iget-object v15, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->skypeeditedid:Ljava/lang/String;

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 2003
    move-object v9, v7

    goto :goto_2

    .line 2004
    :cond_8
    iget-object v15, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    iget-object v0, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2005
    const/4 v8, 0x1

    goto :goto_2

    .line 2010
    .end local v7    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_9
    if-nez v9, :cond_b

    .line 2011
    if-nez v8, :cond_6

    iget-object v14, v10, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 2012
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    if-nez v14, :cond_a

    .line 2013
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    iput-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    .line 2015
    :cond_a
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v14, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2018
    :cond_b
    iget-object v14, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v14, v9}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2023
    .end local v8    # "messageFound":Z
    .end local v9    # "messageToDelete":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v10    # "newMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->removeMessagesBeforeConversationClearedAtTime(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/lang/String;)V

    .line 2024
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_d

    .line 2025
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2029
    :cond_d
    iget-object v13, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 2030
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2031
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v12

    .line 2032
    .local v12, "summaryIndex":I
    const/4 v13, -0x1

    if-gt v12, v13, :cond_10

    .line 2033
    sget-object v13, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "skype conversation doesn\'t exist in summary list ! "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2056
    .end local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v4    # "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v12    # "summaryIndex":I
    :cond_e
    :goto_3
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    .line 2057
    .local v2, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->isLoadingConversation()Z

    move-result v13

    if-nez v13, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 2058
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v12

    .line 2059
    .restart local v12    # "summaryIndex":I
    const/4 v13, -0x1

    if-gt v12, v13, :cond_14

    .line 2060
    sget-object v13, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "conversation doesn\'t exist in summary list ! "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068
    .end local v12    # "summaryIndex":I
    :cond_f
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveSkypeConversationMessageCache()V

    .line 2071
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 2073
    new-instance v13, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v14, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v15, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/16 v16, 0x1

    invoke-direct/range {v14 .. v16}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v13, v14, v0, v15}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 2074
    return-void

    .line 2035
    .end local v2    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .restart local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v4    # "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v12    # "summaryIndex":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v13, v13, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 2040
    .end local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v12    # "summaryIndex":I
    :cond_11
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_e

    .line 2041
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2046
    .end local v4    # "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_12
    if-eqz p3, :cond_13

    .line 2047
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v3

    .line 2048
    .restart local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v3, :cond_13

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_13

    .line 2049
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2052
    .end local v3    # "conversationEntry":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :cond_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->setFailed(Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto/16 :goto_3

    .line 2062
    .restart local v2    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .restart local v12    # "summaryIndex":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v13, v13, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 2063
    .local v11, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/microsoft/xbox/service/model/MessageModel;->updateConsumptionHorizonAsync(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_4
.end method

.method private onLeaveConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    const/4 v3, 0x1

    .line 2268
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 2270
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v4, "onLeaveConversationCompleted "

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2271
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v4, :cond_5

    .line 2272
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v4, "Successfully left conversation"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2273
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v1

    .line 2274
    .local v1, "summaryIndex":I
    const/4 v2, -0x1

    if-gt v1, v2, :cond_0

    .line 2275
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "the conversation doesn\'t exist in the summary list "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2294
    .end local v1    # "summaryIndex":I
    :goto_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v5, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {v2, v4, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 2295
    return-void

    .line 2277
    .restart local v1    # "summaryIndex":I
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 2278
    .local v0, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v0, :cond_2

    .line 2279
    iget-boolean v2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v2, :cond_1

    iget-boolean v2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v2, :cond_3

    :cond_1
    move v2, v3

    :goto_1
    invoke-direct {p0, p2, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->updateConsumptionHorizonAsync(Ljava/lang/String;Z)V

    .line 2281
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2282
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 2284
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2285
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveSkypeConversationMessageCache()V

    goto :goto_0

    .line 2279
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2287
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v4, "The id doesn\'t exist in skype conversation detail cache"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2291
    .end local v0    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v1    # "summaryIndex":I
    :cond_5
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to leave conversation"

    invoke-static {v2, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onMuteConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2298
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "onMuteConversationCompleted "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 2300
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "Successfully set muted property"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2304
    :goto_0
    return-void

    .line 2302
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to set muted property"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onSendMessageCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 866
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 868
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_2

    .line 869
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v3, "Message sent"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    .line 875
    const/4 v0, 0x0

    .line 876
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 877
    .local v1, "status":Ljava/lang/Boolean;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 878
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 879
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 880
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v2, "CONVERSATION_ID_FOR_NEW_GROUP"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 886
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-direct {v2, v3, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 887
    return-void

    .line 871
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "status":Ljava/lang/Boolean;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v3, "Message not sent"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onSendMessageWithAttachmentCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 890
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 892
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_1

    .line 893
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v2, "Message with attachment sent"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    .line 899
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 900
    .local v0, "status":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 901
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 903
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {v1, v2, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 904
    return-void

    .line 895
    .end local v0    # "status":Ljava/lang/Boolean;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v2, "Message with attachment not sent"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onSkypeDeleteMessageCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "messageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v7, -0x1

    const/4 v10, 0x0

    .line 769
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 771
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v5, v6, :cond_5

    .line 772
    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v6, "Skype message deleted"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    .line 775
    .local v0, "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 776
    invoke-direct {p0, v0, p3}, Lcom/microsoft/xbox/service/model/MessageModel;->findSkypeMessageById(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/lang/String;)I

    move-result v1

    .line 777
    .local v1, "messageIndex":I
    if-gt v1, v7, :cond_1

    .line 778
    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The message id doesn\'t exist in the conversation cache "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    .end local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v1    # "messageIndex":I
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveStoredConversationListResult()V

    .line 815
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->saveSkypeConversationMessageCache()V

    .line 817
    iput-boolean v10, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeleting:Z

    .line 818
    new-instance v5, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v6, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v7, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v8, 0x1

    invoke-direct {v6, v7, v8}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v7

    invoke-direct {v5, v6, p0, v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 819
    return-void

    .line 780
    .restart local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v1    # "messageIndex":I
    :cond_1
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 782
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v4

    .line 783
    .local v4, "summaryIndex":I
    if-gt v4, v7, :cond_2

    .line 784
    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The conversation doesn\'t exist in the summary list "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 786
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 787
    .local v3, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_4

    .line 788
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 789
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 793
    :cond_4
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v1, v5, :cond_0

    .line 794
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    iget-object v6, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 795
    .local v2, "newestMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iput-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    .line 796
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    .line 797
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iput-object v6, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->sentTime:Ljava/util/Date;

    .line 798
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iput-boolean v10, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAudio:Z

    .line 799
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iput-boolean v10, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasPhoto:Z

    .line 800
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    const-wide/16 v8, 0x0

    invoke-static {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageId:J

    .line 801
    iget-object v5, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iput-object v6, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageType:Ljava/lang/String;

    .line 802
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto/16 :goto_0

    .line 808
    .end local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v1    # "messageIndex":I
    .end local v2    # "newestMessage":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v3    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v4    # "summaryIndex":I
    :cond_5
    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v6, "Skype message not deleted"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private onSkypeLongPollCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1627
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1628
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;

    .line 1629
    .local v0, "event":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;
    instance-of v3, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    if-eqz v3, :cond_0

    .line 1630
    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    .end local v0    # "event":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;->getEventMessages()Ljava/util/List;

    move-result-object v2

    .line 1632
    .local v2, "skypeMessages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;

    .line 1633
    .local v1, "eventMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->processSkypeEventMessage(Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;)V

    goto :goto_0

    .line 1638
    .end local v1    # "eventMessage":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;
    .end local v2    # "skypeMessages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;>;"
    :cond_0
    return-void
.end method

.method private onSubscribeSkypeLongPollCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1565
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1567
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1568
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSubscribeSkypeLongPollCompleted succeed with url "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeLongPollLocationUrl:Ljava/lang/String;

    .line 1573
    :goto_0
    return-void

    .line 1571
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "subscribeSkypeLongPoll failed with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onUpdateTopicNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "topic"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2307
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v3, "onUpdateTopicNameCompleted "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_1

    .line 2309
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v1

    .line 2310
    .local v1, "summaryIndex":I
    const/4 v2, -0x1

    if-gt v1, v2, :cond_0

    .line 2311
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "conversation doesn\'t exist in summary list ! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319
    .end local v1    # "summaryIndex":I
    :goto_0
    return-void

    .line 2313
    .restart local v1    # "summaryIndex":I
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 2314
    .local v0, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iput-object p3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    goto :goto_0

    .line 2317
    .end local v0    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v1    # "summaryIndex":I
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to rename conversation topic"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processSkypeEventMessage(Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;)V
    .locals 9
    .param p1, "skypeEventMessage"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    .line 1641
    if-nez p1, :cond_1

    .line 1642
    sget-object v6, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v7, "processSkypeEventMessage - the skypeEventMessage is null"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1688
    :cond_0
    :goto_0
    return-void

    .line 1645
    :cond_1
    iget-object v7, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resourceType:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    move-result-object v4

    .line 1647
    .local v4, "resourceType":Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel$4;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeEventResourceType:[I

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1685
    sget-object v6, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processSkypeEventMessage - unhandled resourceType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1650
    :pswitch_0
    iget-object v7, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1651
    .local v1, "id":Ljava/lang/String;
    iget-object v7, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1652
    .local v5, "xuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v2, v6

    .line 1653
    .local v2, "isGroupConversation":Z
    :goto_1
    if-eqz v2, :cond_3

    move-object v0, v1

    .line 1657
    .local v0, "conversationId":Ljava/lang/String;
    :goto_2
    iget-object v7, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v3

    .line 1658
    .local v3, "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v7, Lcom/microsoft/xbox/service/model/MessageModel$4;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    .line 1678
    sget-object v6, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processSkypeEventMessage - unhandled messageType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1652
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v2    # "isGroupConversation":Z
    .end local v3    # "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .restart local v2    # "isGroupConversation":Z
    :cond_3
    move-object v0, v5

    .line 1653
    goto :goto_2

    .line 1660
    .restart local v0    # "conversationId":Ljava/lang/String;
    .restart local v3    # "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1661
    iget-object v6, p1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;->resource:Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->from:Ljava/lang/String;

    invoke-static {v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v0, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->processSkypeIsTypingMessage(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1673
    :pswitch_2
    invoke-virtual {p0, v0, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->updateFromNotification(Ljava/lang/String;Z)V

    .line 1674
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    goto/16 :goto_0

    .line 1647
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 1658
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private processSkypeIsTypingMessage(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "fromId"    # Ljava/lang/String;
    .param p2, "conversationId"    # Ljava/lang/String;
    .param p3, "isGroupConversation"    # Z

    .prologue
    .line 1691
    const-string v1, ""

    .line 1692
    .local v1, "gamerTag":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 1693
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 1694
    .local v2, "gamerTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1695
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "gamerTag":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1705
    .end local v2    # "gamerTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v1    # "gamerTag":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1707
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v1, v4}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->updateTypingIndicator(Ljava/lang/String;Ljava/util/Date;)V

    .line 1709
    :cond_1
    return-void

    .line 1698
    :cond_2
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    .line 1699
    .local v0, "currentConversation":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v0, :cond_0

    .line 1700
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_0
.end method

.method private removeEmptyConversations()V
    .locals 5

    .prologue
    .line 1483
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1484
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1486
    .local v0, "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 1487
    .local v1, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->isConversationEmpty(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1488
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1489
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1490
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1495
    .end local v1    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1497
    .end local v0    # "itemsToRemove":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    :cond_3
    return-void
.end method

.method private removeMessagesBeforeConversationClearedAtTime(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;Ljava/lang/String;)V
    .locals 7
    .param p1, "conversationEntry"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2077
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 2078
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2081
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->findConversationSummaryById(Ljava/lang/String;)I

    move-result v3

    .line 2082
    .local v3, "summaryIndex":I
    const/4 v4, -0x1

    if-gt v3, v4, :cond_2

    .line 2083
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "skype conversation doesn\'t exist in summary list ! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    :cond_0
    :goto_1
    return-void

    .line 2078
    .end local v3    # "summaryIndex":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 2085
    .restart local v3    # "summaryIndex":I
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 2087
    .local v2, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v2, :cond_0

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->clearedAt:Ljava/util/Date;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2088
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2089
    .local v1, "messagesToDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;>;"
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 2090
    .local v0, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->clearedAt:Ljava/util/Date;

    invoke-virtual {v5, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2091
    :cond_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2095
    .end local v0    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_5
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2096
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public static reset()V
    .locals 1

    .prologue
    .line 181
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->clearObservers()V

    .line 182
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->access$200()V

    .line 183
    return-void
.end method

.method private saveSkypeConversationMessageCache()V
    .locals 3

    .prologue
    .line 331
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$1;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel$1;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 338
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel$1;->execute()V

    .line 339
    return-void
.end method

.method private saveStoredConversationListResult()V
    .locals 2

    .prologue
    .line 343
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 344
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->saveStoredConversationListResult(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;)V

    .line 345
    return-void
.end method

.method private updateConsumptionHorizonAsync(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "isGroupConversation"    # Z

    .prologue
    .line 384
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 386
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p2, p1}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;ZLjava/lang/String;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 412
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 413
    return-void
.end method

.method private updateConversationUnreadCount()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1469
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    .line 1470
    .local v1, "meXuid":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 1471
    .local v2, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1472
    .local v0, "fromXuid":Ljava/lang/String;
    :goto_1
    iget-object v3, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1473
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1474
    :cond_1
    invoke-static {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->conversationHasUnreadMessages(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    iput v3, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    goto :goto_0

    .line 1471
    .end local v0    # "fromXuid":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .restart local v0    # "fromXuid":Ljava/lang/String;
    :cond_3
    move v3, v4

    .line 1474
    goto :goto_2

    .line 1476
    :cond_4
    iput v4, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    goto :goto_0

    .line 1480
    .end local v0    # "fromXuid":Ljava/lang/String;
    .end local v2    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_5
    return-void
.end method

.method private updateUnreadCount()V
    .locals 4

    .prologue
    .line 822
    const/4 v1, 0x0

    .line 823
    .local v1, "unreadCount":I
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 824
    .local v0, "sum":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 825
    iget v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    add-int/2addr v1, v3

    goto :goto_0

    .line 826
    :cond_1
    iget v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    if-lez v3, :cond_0

    .line 827
    iget v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->unreadMessageCount:I

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v1, v3

    goto :goto_0

    .line 830
    .end local v0    # "sum":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_2
    iput v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->unreadMessageCount:I

    .line 831
    return-void
.end method


# virtual methods
.method public addMemberToConversation(ZLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 557
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding member to conversation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 560
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 561
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 563
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;

    invoke-direct {v6, p0, p0, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public canContributeToGroupConversationAsync(Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "statusKey"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/UpdateType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 442
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 443
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 445
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 477
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 478
    return-void
.end method

.method public clearCurrentConversation()V
    .locals 1

    .prologue
    .line 2380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->currentConversationXuid:Ljava/lang/String;

    .line 2381
    return-void
.end method

.method public deleteSkypeConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 6
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 614
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "Deleting Skype conversation"

    invoke-static {v2, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 617
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 618
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 619
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 620
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeletingConversation:Z

    if-nez v2, :cond_0

    move v4, v3

    :cond_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 622
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeletingConversation:Z

    if-nez v2, :cond_1

    .line 623
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeletingConversation:Z

    .line 624
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackDeleteConversation(Ljava/lang/String;)V

    .line 625
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$DeleteSkypeConversationRunner;

    invoke-direct {v0, p0, p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel$DeleteSkypeConversationRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 626
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MessageModel$DeleteSkypeConversationRunner;
    new-instance v1, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 627
    .local v1, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 628
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->clearCurrentConversation()V

    .line 630
    .end local v0    # "runner":Lcom/microsoft/xbox/service/model/MessageModel$DeleteSkypeConversationRunner;
    .end local v1    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    :cond_1
    return-void

    :cond_2
    move v2, v4

    .line 618
    goto :goto_0

    :cond_3
    move v2, v4

    .line 619
    goto :goto_1
.end method

.method public deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "clientMessageId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 589
    sget-object v2, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleting Skype message "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 592
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 594
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 595
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 596
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 597
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v4, v3

    :cond_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 599
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeleting:Z

    if-nez v2, :cond_1

    .line 600
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 601
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageDeleted(Ljava/lang/String;)V

    .line 606
    :goto_2
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeleting:Z

    .line 607
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;
    new-instance v1, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 609
    .local v1, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 611
    .end local v0    # "runner":Lcom/microsoft/xbox/service/model/MessageModel$SkypeDeleteMessageRunner;
    .end local v1    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    :cond_1
    return-void

    :cond_2
    move v2, v4

    .line 595
    goto :goto_0

    :cond_3
    move v2, v4

    .line 596
    goto :goto_1

    .line 603
    :cond_4
    invoke-static {p2, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackDeleteMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getConversationDetails(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;
    .locals 1
    .param p1, "targetXuid"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationDetailsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;

    return-object v0
.end method

.method public getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 276
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    .line 278
    .local v0, "skypeConversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 283
    .end local v0    # "skypeConversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 202
    :goto_0
    return-object v0

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 197
    .local v0, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .end local v0    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_2
    move-object v0, v1

    .line 202
    goto :goto_0
.end method

.method public getDatabaseNeedsSync()Z
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z

    move-result v0

    return v0
.end method

.method public getIsDeleting()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeleting:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isDeletingConversation:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingMessageList()Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isLoading:Z

    return v0
.end method

.method public getIsSending()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    return v0
.end method

.method public getMostRecentConversationListCount()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMostRecentMessageWithText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .locals 5
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 424
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 426
    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;-><init>()V

    .line 427
    .local v3, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v2

    .line 428
    .local v2, "messageListResult":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v2, :cond_0

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 429
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "index":I
    :goto_0
    if-ltz v0, :cond_0

    .line 430
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 431
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    iget-object v4, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 433
    move-object v3, v1

    .line 438
    .end local v0    # "index":I
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_0
    return-object v3

    .line 429
    .restart local v0    # "index":I
    .restart local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getSLSConversationList()Ljava/util/ArrayList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .locals 4
    .param p1, "targetXuid"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 257
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 258
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 262
    .local v0, "result":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    if-eqz v1, :cond_1

    .line 263
    :cond_0
    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    .line 270
    .end local v0    # "result":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 265
    .restart local v0    # "result":Ljava/lang/Object;
    :cond_1
    const-string v1, "Unexpected type in conversation message cache, please investigate."

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 266
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid message list result ignored for target xuid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    .end local v0    # "result":Ljava/lang/Object;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTypingIndicatorList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->getTypingIndicatorList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUnReadMessageCount()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->unreadMessageCount:I

    return v0
.end method

.method public initConversationDataFromStorage()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 206
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v2, "Initializing conversation and messages from database"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getStoredConversationListResult()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;-><init>()V

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    .line 215
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getStoredSkypeConversationMessageCache()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 217
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateUnreadCount()V

    .line 219
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversations loaded from DB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 222
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 223
    new-instance v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    .line 224
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 226
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s error"

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public isCurrentConversation(Ljava/lang/String;)Z
    .locals 1
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 2372
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->currentConversationXuid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->currentConversationXuid:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveConversation(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leaving conversation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 308
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;

    invoke-direct {v6, p0, p0, p2}, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadMessageListAsync(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 292
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, "syncState":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    .line 299
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v2, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 300
    return-void
.end method

.method public loadSkypeConversationMessages(Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 12
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "isGroupConversation"    # Z
    .param p3, "forceLoad"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 2331
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v7

    .line 2333
    .local v7, "details":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    if-nez v0, :cond_0

    .line 2334
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 2337
    :cond_0
    if-eqz v7, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v0, :cond_1

    iget-object v0, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2338
    :cond_1
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;ZLjava/lang/String;)V

    move v1, v11

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 2340
    :goto_0
    return-object v0

    :cond_2
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;

    iget-object v1, v7, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget-object v5, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationMessagesRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;ZLjava/lang/String;)V

    move v1, v11

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0
.end method

.method public loadSkypeConversationMessages()V
    .locals 4

    .prologue
    .line 2345
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2346
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->mostRecentConversationResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->results:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 2347
    .local v0, "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-boolean v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessagesAsync(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2350
    .end local v0    # "summary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_0
    return-void
.end method

.method public moreConversationExists()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->conversationListResult:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    iget v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public muteConversation(Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "isMuted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_0

    const-string v0, "Muting"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " conversation "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 315
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 317
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;

    invoke-direct {v6, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Z)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    .line 312
    :cond_0
    const-string v0, "Unmuting"

    goto :goto_0
.end method

.method public sendSkypeGroupMessage(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 8
    .param p1, "conversationId"    # Ljava/lang/String;
    .param p3, "sendRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "recipientXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 659
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v3, "Sending Skype group message"

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 663
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 664
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 666
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 667
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageSent(Ljava/lang/String;)V

    .line 672
    :goto_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    .line 674
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 675
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;
    new-instance v7, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v7, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 676
    .local v7, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 677
    return-void

    .line 664
    .end local v0    # "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;
    .end local v7    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 669
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "UNKNOWN"

    :goto_2
    invoke-static {v1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessage(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    :cond_2
    move-object v1, p1

    goto :goto_2
.end method

.method public sendSkypeGroupMessage(Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 1
    .param p2, "sendRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 655
    .local p1, "recipientXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeGroupMessage(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 656
    return-void
.end method

.method public sendSkypeIsTypingMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;Z)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "sendRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "isGroupConversation"    # Z

    .prologue
    const/4 v4, 0x0

    .line 682
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 683
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 685
    if-eqz p3, :cond_0

    .line 686
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 687
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;
    new-instance v7, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v7, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 688
    .local v7, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 694
    .end local v0    # "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;
    :goto_0
    return-void

    .line 690
    .end local v7    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;

    invoke-direct {v0, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 691
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;
    new-instance v7, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v7, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 692
    .restart local v7    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    goto :goto_0
.end method

.method public sendSkypeMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "sendRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 633
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v7, "Sending Skype message"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 636
    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    if-nez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 638
    const-string v4, "8:xbox:%s"

    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v6

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 639
    .local v0, "conversationId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 640
    .local v1, "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v4}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 643
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageSent(Ljava/lang/String;)V

    .line 648
    :goto_1
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    .line 649
    new-instance v2, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;

    invoke-direct {v2, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 650
    .local v2, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;
    new-instance v3, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v3, v2}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 651
    .local v3, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 652
    return-void

    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v1    # "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;
    .end local v3    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    :cond_0
    move v4, v6

    .line 636
    goto :goto_0

    .line 645
    .restart local v0    # "conversationId":Ljava/lang/String;
    .restart local v1    # "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessage(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1
.end method

.method public sendSkypeMessageWithAttachment(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "sendRequest"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 567
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v7, "Sending Skype message with attachment"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 570
    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    if-nez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 572
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 573
    sget-object v4, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    const-string v5, "Invalid recipient id"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :goto_1
    return-void

    :cond_0
    move v4, v6

    .line 570
    goto :goto_0

    .line 577
    :cond_1
    const-string v4, "8:xbox:%s"

    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v6

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 578
    .local v0, "conversationId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v1, "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessage(Ljava/lang/String;Ljava/util/List;)V

    .line 582
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->isSending:Z

    .line 583
    new-instance v2, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;

    invoke-direct {v2, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 584
    .local v2, "runner":Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;
    new-instance v3, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v3, v2}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 585
    .local v3, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Boolean;>;"
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    goto :goto_1
.end method

.method public setCurrentConversation(Ljava/lang/String;)V
    .locals 0
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 2376
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel;->currentConversationXuid:Ljava/lang/String;

    .line 2377
    return-void
.end method

.method public shouldLoadConversationDetails(Ljava/lang/String;)Z
    .locals 4
    .param p1, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 2431
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lastRefreshConversationDetailsTime:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public skypeLongPoll()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->skypeLongPollLocationUrl:Ljava/lang/String;

    invoke-direct {v6, p0, p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public startTypingIndicatorManager()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->startTypingExpirationTask()V

    .line 149
    return-void
.end method

.method public stopTypingIndicatorManager()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->stopTypingExpirationTask()V

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->clear()V

    .line 154
    return-void
.end method

.method public subscribeSkypeLongPoll(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "edfRegId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 697
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subscribing to Skype long poll "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 701
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->subscribeSkypeLongPollLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    if-nez v0, :cond_0

    .line 702
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->subscribeSkypeLongPollLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 705
    :cond_0
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MessageModel;->subscribeSkypeLongPollLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;

    invoke-direct {v6, p0, p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateConsumptionHorizonAsync(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 3
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 348
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 350
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel$2;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 380
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel$2;->execute()V

    .line 381
    return-void
.end method

.method public updateFromNotification(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "senderXuid"    # Ljava/lang/String;
    .param p2, "isGroupConversation"    # Z

    .prologue
    .line 2367
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Z)V

    .line 2368
    .local v0, "loadMessageDetailAsyncTask":Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel$LoadConversationUpdateFromNotification;->load(Z)V

    .line 2369
    return-void
.end method

.method public updateGroupTopicNameConversation(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "topicName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Updating topic of group conversation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 324
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 325
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 327
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/MessageModel;->lifetime:J

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;

    invoke-direct {v6, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel$UpdateTopicNameRunner;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateProfileAndPresenceDataAsync()V
    .locals 4

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v0

    .line 484
    .local v0, "conversations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 538
    :goto_0
    return-void

    .line 488
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/service/model/MessageModel$3;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/MessageModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/ArrayList;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel$3;-><init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 537
    .local v1, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method
