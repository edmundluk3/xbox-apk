.class public Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "FriendsWhoPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;
    }
.end annotation


# instance fields
.field private friendsWhoPlayGame:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private gameTitleId:J

.field private isInGameDetailScreen:Z

.field private loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

.field private selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 6
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 33
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 37
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    .line 38
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->isInGameDetailScreen:Z

    .line 41
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    .line 43
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 45
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 47
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    .param p1, "x1"    # J

    .prologue
    .line 31
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    return-object p1
.end method

.method private hideScreen()V
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->isInGameDetailScreen:Z

    if-eqz v0, :cond_0

    .line 104
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 112
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 116
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->hideScreen()V

    goto :goto_0

    .line 119
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGameTitleId()J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public gotoCompareAchievements(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 4
    .param p1, "gamer"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 133
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Friends Who Play"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedTitleId(J)V

    .line 136
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 137
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 138
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->cancel()V

    .line 128
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->load(Z)V

    .line 130
    return-void
.end method

.method protected onLoadFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->updateAdapter()V

    .line 100
    return-void

    .line 88
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getResult(J)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 93
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->friendsWhoPlayGame:Ljava/util/ArrayList;

    .line 94
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->hideScreen()V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->cancel()V

    .line 80
    :cond_0
    return-void
.end method

.method public setInGameDetailScreenFlag()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->isInGameDetailScreen:Z

    .line 60
    return-void
.end method
