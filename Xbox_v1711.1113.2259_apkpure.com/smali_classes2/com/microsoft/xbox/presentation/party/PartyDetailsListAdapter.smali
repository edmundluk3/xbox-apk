.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PartyDetailsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final SQUAWKER_RING_SIZE_SILENT:I

.field private static final SQUAWKER_RING_SIZE_TALKING:I


# instance fields
.field private final intent:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field private final viewStateStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f090424

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->SQUAWKER_RING_SIZE_SILENT:I

    .line 46
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f090425

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->SQUAWKER_RING_SIZE_TALKING:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lio/reactivex/Observable;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "intent":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;>;"
    .local p2, "viewStateStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 54
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 55
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)V

    .line 57
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->intent:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 58
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->viewStateStream:Lio/reactivex/Observable;

    .line 59
    return-void
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->SQUAWKER_RING_SIZE_TALKING:I

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->SQUAWKER_RING_SIZE_SILENT:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->intent:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 108
    if-nez p1, :cond_0

    .line 109
    const v0, 0x7f0301a1

    .line 111
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0301a4

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 77
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->getItemViewType(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v1, p1

    .line 82
    check-cast v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    .line 83
    .local v1, "partyMemberFullViewHolder":Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;

    .line 85
    .local v0, "partyMember":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;
    instance-of v2, v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    if-eqz v2, :cond_0

    .line 86
    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    .end local v0    # "partyMember":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x7f0301a1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;ILjava/util/List;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p3, "payloads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 104
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p1

    .line 97
    check-cast v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    .line 99
    .local v1, "memberIconViewHolder":Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 100
    .local v0, "latestPayload":Ljava/lang/Object;
    instance-of v2, v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    if-eqz v2, :cond_0

    .line 101
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .end local v0    # "latestPayload":Ljava/lang/Object;
    invoke-static {v1, v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;->access$000(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 64
    .local v0, "v":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 71
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 72
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 66
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->intent:Lcom/microsoft/xbox/toolkit/generics/Action;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->viewStateStream:Lio/reactivex/Observable;

    invoke-direct {v1, v0, v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsHeaderViewHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/toolkit/generics/Action;Lio/reactivex/Observable;)V

    goto :goto_0

    .line 68
    :pswitch_2
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter$PartyMemberFullViewHolder;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x7f0301a1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
