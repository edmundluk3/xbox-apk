.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;
.super Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubChangeAsyncTask;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsChangeAsyncTask"
.end annotation


# instance fields
.field private final callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final newClubOnSuccess:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "updateRequest"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "newClubOnSuccess"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "callback"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    .prologue
    .line 1439
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 1440
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubChangeAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)V

    .line 1441
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1442
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1444
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->newClubOnSuccess:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 1445
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    .line 1446
    return-void
.end method


# virtual methods
.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1450
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1469
    :cond_0
    :goto_0
    return-void

    .line 1453
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->newClubOnSuccess:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0

    .line 1457
    :pswitch_1
    const-string v0, "Change request returned NO_CHANGE"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 1462
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SettingsChangeAsyncTaskFail: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->updateRequest:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    if-eqz v0, :cond_0

    .line 1465
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->callback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;->onFailure()V

    goto :goto_0

    .line 1450
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1430
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method
