.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Party;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Party"
.end annotation


# static fields
.field public static final Back:Ljava/lang/String; = "Party - Back"

.field public static final Cancel:Ljava/lang/String; = "Party - Cancel"

.field public static final EnableInviteOnly:Ljava/lang/String; = "Party - Make party invite only"

.field public static final EnableJoinable:Ljava/lang/String; = "Party - Make party joinable"

.field public static final GoToLFG:Ljava/lang/String; = "Party - Looking for Group"

.field public static final Invite:Ljava/lang/String; = "Party - Invite"

.field public static final Leave:Ljava/lang/String; = "Party - Leave party"

.field public static final MuteMember:Ljava/lang/String; = "Party - Mute party member"

.field public static final MuteParty:Ljava/lang/String; = "Party - Mute party"

.field public static final RemoveMember:Ljava/lang/String; = "Party - Remove member from party"

.field public static final SendMessage:Ljava/lang/String; = "Party - Send chat message"

.field public static final ShowMemberOptions:Ljava/lang/String; = "Party - Show party member options"

.field public static final ShowOptions:Ljava/lang/String; = "Party - Show party options"

.field public static final StartAParty:Ljava/lang/String; = "Party - Start a party"

.field public static final UnMuteMember:Ljava/lang/String; = "Party - UnMute party member"

.field public static final UnMuteParty:Ljava/lang/String; = "Party - UnMute party"

.field public static final ViewChat:Ljava/lang/String; = "Party - View chat"

.field public static final ViewLFGDetails:Ljava/lang/String; = "Party - View LFG Details"

.field public static final ViewPartyDetails:Ljava/lang/String; = "Party - View party details"

.field public static final ViewProfile:Ljava/lang/String; = "Party - View profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
