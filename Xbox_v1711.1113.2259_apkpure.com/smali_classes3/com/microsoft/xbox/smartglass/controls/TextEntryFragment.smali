.class public Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;
.super Landroid/app/Fragment;
.source "TextEntryFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;
    }
.end annotation


# static fields
.field private static final _inputScopes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/smartglass/TextInputScope;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _acceptButton:Landroid/widget/Button;

.field private _cancelButton:Landroid/widget/Button;

.field private _packageName:Ljava/lang/String;

.field private _prompt:Landroid/widget/TextView;

.field private _textBox:Landroid/widget/EditText;

.field private _textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x60

    const/16 v5, 0x70

    const/16 v4, 0x10

    const/4 v3, 0x1

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    .line 51
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Default:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Url:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->FullFilePath:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->FileName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailUserName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->EmailSmtpAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->LogOnName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalFullName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNamePrefix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalGivenName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalMiddleName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalSurname:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PersonalNameSuffix:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalAddress:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PostalCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStreet:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressStateOrProvince:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCity:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AddressCountryShortName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmountAndSymbol:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyAmount:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Date:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDay:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateYear:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateMonthName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->DateDayName:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Digits:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Number:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->OneChar:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Password:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneCountryCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneAreaCode:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TelephoneLocalNumber:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Time:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeHour:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->TimeMinorSec:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->NumberFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->AlphanumericFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->CurrencyChinese:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Bopomofo:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hiragana:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->KatakanaFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Hanja:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->HangulFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Search:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchTitleText:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->SearchIncremental:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseHalfWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->ChineseFullWidth:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->NativeScript:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->PhraseList:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->RegularExpression:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Srgs:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->Xml:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextInputScope;->EnumString:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    const-string v2, "layout"

    const-string v3, "textentry"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 129
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    const-string v2, "id"

    const-string v3, "promptView"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_prompt:Landroid/widget/TextView;

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    const-string v2, "id"

    const-string v3, "acceptButton"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_acceptButton:Landroid/widget/Button;

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_acceptButton:Landroid/widget/Button;

    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    const-string v2, "id"

    const-string v3, "cancelButton"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_cancelButton:Landroid/widget/Button;

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_cancelButton:Landroid/widget/Button;

    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$2;-><init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_packageName:Ljava/lang/String;

    const-string v2, "id"

    const-string v3, "textBox"

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ResourceFinder;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    .line 152
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;-><init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$3;-><init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 166
    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_acceptButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_acceptButton:Landroid/widget/Button;

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_cancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_cancelButton:Landroid/widget/Button;

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    .line 180
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    .line 181
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_prompt:Landroid/widget/TextView;

    .line 182
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 183
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3
    .param p1, "hidden"    # Z

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 193
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz p1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method public selectText(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 261
    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result p1

    .line 262
    :goto_0
    if-gez p2, :cond_1

    const/4 p2, 0x0

    .line 263
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    add-int v1, p1, p2

    invoke-virtual {v0, p1, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 265
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_1
.end method

.method public updateConfiguration(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 6
    .param p1, "configuration"    # Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 206
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->suspend(Z)V

    .line 209
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_prompt:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->prompt:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->prompt:Ljava/lang/String;

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    const/4 v1, 0x1

    .line 214
    .local v1, "inputType":I
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v5, Lcom/microsoft/xbox/smartglass/TextOptions;->Password:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v2, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    or-int/lit16 v1, v1, 0x80

    .line 232
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 235
    new-array v0, v4, [Landroid/text/InputFilter;

    .line 236
    .local v0, "filters":[Landroid/text/InputFilter;
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v4, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->maxTextLength:I

    invoke-direct {v2, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v3

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->suspend(Z)V

    .line 241
    return-void

    .line 209
    .end local v0    # "filters":[Landroid/text/InputFilter;
    .end local v1    # "inputType":I
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 218
    .restart local v1    # "inputType":I
    :cond_1
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v5, Lcom/microsoft/xbox/smartglass/TextOptions;->AcceptsReturn:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v2, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v5, Lcom/microsoft/xbox/smartglass/TextOptions;->MultiLine:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v2, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 219
    :cond_2
    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    .line 225
    :cond_3
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v5, Lcom/microsoft/xbox/smartglass/TextOptions;->PredictionEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v2, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/high16 v2, 0x80000

    :goto_2
    or-int/2addr v1, v2

    .line 226
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v5, Lcom/microsoft/xbox/smartglass/TextOptions;->SpellCheckEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v2, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x8000

    :goto_3
    or-int/2addr v1, v2

    .line 229
    sget-object v2, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->inputScope:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    sget-object v2, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_inputScopes:Ljava/util/Map;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->inputScope:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_4
    or-int/2addr v1, v2

    goto :goto_1

    :cond_4
    move v2, v3

    .line 225
    goto :goto_2

    :cond_5
    move v2, v3

    .line 226
    goto :goto_3

    :cond_6
    move v2, v4

    .line 229
    goto :goto_4
.end method

.method public updateText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->suspend(Z)V

    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textBox:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->_textWatcher:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->suspend(Z)V

    .line 253
    return-void
.end method
