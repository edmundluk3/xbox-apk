.class public abstract Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;
.super Ljava/lang/Object;
.source "ClubPresenceDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UpdateUserPresenceRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/service/clubs/ClubPresenceDataTypes$UpdateUserPresenceRequest;
    .locals 1
    .param p0, "userPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubPresenceDataTypes_UpdateUserPresenceRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    return-object v0
.end method


# virtual methods
.method public abstract userPresenceState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
.end method
