.class public final Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ActivityFeedDataTypes_PinsInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultMaxPins:I

.field private defaultNumItems:I

.field private final maxPinsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final numItemsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 21
    iput v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultNumItems:I

    .line 22
    iput v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultMaxPins:I

    .line 24
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->numItemsAdapter:Lcom/google/gson/TypeAdapter;

    .line 25
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->maxPinsAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 52
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 53
    const/4 v3, 0x0

    .line 79
    :goto_0
    return-object v3

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 56
    iget v2, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultNumItems:I

    .line 57
    .local v2, "numItems":I
    iget v1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultMaxPins:I

    .line 58
    .local v1, "maxPins":I
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 64
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 64
    :sswitch_0
    const-string v4, "numItems"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "maxPins"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 66
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->numItemsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 67
    goto :goto_1

    .line 70
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->maxPinsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 71
    goto :goto_1

    .line 78
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 79
    new-instance v3, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo;

    invoke-direct {v3, v2, v1}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo;-><init>(II)V

    goto :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        -0x71ca7026 -> :sswitch_0
        0x324e4902 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultMaxPins(I)Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMaxPins"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultMaxPins:I

    .line 33
    return-object p0
.end method

.method public setDefaultNumItems(I)Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultNumItems"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->defaultNumItems:I

    .line 29
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    if-nez p2, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 48
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 43
    const-string v0, "numItems"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->numItemsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->numItems()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 45
    const-string v0, "maxPins"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->maxPinsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->maxPins()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PinsInfo$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;)V

    return-void
.end method
