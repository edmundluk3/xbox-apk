.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceMessageContent"
.end annotation


# instance fields
.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;->message:Ljava/lang/String;

    .line 329
    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 336
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;

    return-object v0
.end method


# virtual methods
.method public getServiceMessageText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;->message:Ljava/lang/String;

    return-object v0
.end method
