.class public Lcom/microsoft/xbox/xle/app/activity/DLCOverviewScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "DLCOverviewScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string v0, "DLCOverview"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 14
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/DLCOverviewScreen;->onCreateContentView()V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    const/4 v2, 0x0

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    const v4, 0x7f0e0513

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/DLCOverviewScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 19
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 23
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0300e2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/DLCOverviewScreen;->setContentView(I)V

    .line 25
    return-void

    .line 23
    :cond_0
    const v0, 0x7f0300e3

    goto :goto_0
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method
