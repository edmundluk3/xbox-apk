.class public Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "GameProfileVipsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/GameProfileVipsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProfileVipDataModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private gameTitleId:J

.field private result:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;"
        }
    .end annotation
.end field

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->xuid:Ljava/lang/String;

    .line 61
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->gameTitleId:J

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->gameTitleId:J

    return-wide v0
.end method


# virtual methods
.method public getResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->result:Ljava/util/ArrayList;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;-><init>(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)V

    .line 74
    .local v0, "runner":Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->result:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 79
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->result:Ljava/util/ArrayList;

    .line 83
    :cond_0
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
