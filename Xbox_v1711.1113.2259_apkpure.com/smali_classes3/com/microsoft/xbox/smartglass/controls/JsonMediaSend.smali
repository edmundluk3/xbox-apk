.class Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;
.super Ljava/lang/Object;
.source "JsonMediaSend.java"


# instance fields
.field public final command:I

.field public final titleId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 17
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "titleId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;->titleId:I

    .line 18
    const-string v1, "command"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;->command:I

    .line 19
    return-void
.end method
