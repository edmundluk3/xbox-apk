.class public final enum Lcom/microsoft/xbox/service/comments/CommentsServiceStub;
.super Ljava/lang/Enum;
.source "CommentsServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/comments/ICommentsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/comments/CommentsServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/comments/ICommentsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

.field private static final ACTION_BATCH_FILE:Ljava/lang/String; = "stubdata/CommentActionBatch.json"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/comments/CommentsServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/comments/CommentsServiceStub;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/comments/CommentsServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/comments/CommentsServiceStub;

    return-object v0
.end method


# virtual methods
.method public getActionBatchResponse(Ljava/util/List;)Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x64L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "locators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x64

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 31
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v1, "stubdata/CommentActionBatch.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 32
    :catch_0
    move-exception v6

    .line 33
    .local v6, "ex":Ljava/io/IOException;
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x8

    invoke-direct {v0, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v0
.end method
