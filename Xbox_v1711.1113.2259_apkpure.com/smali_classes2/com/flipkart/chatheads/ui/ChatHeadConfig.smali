.class public Lcom/flipkart/chatheads/ui/ChatHeadConfig;
.super Ljava/lang/Object;
.source "ChatHeadConfig.java"


# instance fields
.field private circularFanOutRadius:I

.field private circularRingHeight:I

.field private circularRingWidth:I

.field private closeButtonBottomMargin:I

.field private closeButtonHeight:I

.field private closeButtonHidden:Z

.field private closeButtonWidth:I

.field private headHeight:I

.field private headHorizontalSpacing:I

.field private headVerticalSpacing:I

.field private headWidth:I

.field private initialPosition:Landroid/graphics/Point;

.field private maxChatHeads:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCircularFanOutRadius(II)I
    .locals 1
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 129
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularFanOutRadius:I

    return v0
.end method

.method public getCircularRingHeight()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularRingHeight:I

    return v0
.end method

.method public getCircularRingWidth()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularRingWidth:I

    return v0
.end method

.method public getCloseButtonBottomMargin()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonBottomMargin:I

    return v0
.end method

.method public getCloseButtonHeight()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonHeight:I

    return v0
.end method

.method public getCloseButtonWidth()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonWidth:I

    return v0
.end method

.method public getHeadHeight()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headHeight:I

    return v0
.end method

.method public getHeadHorizontalSpacing(II)I
    .locals 1
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 60
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headHorizontalSpacing:I

    return v0
.end method

.method public getHeadVerticalSpacing(II)I
    .locals 1
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 68
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headVerticalSpacing:I

    return v0
.end method

.method public getHeadWidth()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headWidth:I

    return v0
.end method

.method public getInitialPosition()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->initialPosition:Landroid/graphics/Point;

    return-object v0
.end method

.method public getMaxChatHeads()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->maxChatHeads:I

    return v0
.end method

.method public getMaxChatHeads(II)I
    .locals 1
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 84
    iget v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->maxChatHeads:I

    return v0
.end method

.method public isCloseButtonHidden()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonHidden:Z

    return v0
.end method

.method public setCircularFanOutRadius(I)V
    .locals 0
    .param p1, "circularFanOutRadius"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularFanOutRadius:I

    .line 33
    return-void
.end method

.method public setCircularRingHeight(I)V
    .locals 0
    .param p1, "circularRingHeight"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularRingHeight:I

    .line 126
    return-void
.end method

.method public setCircularRingWidth(I)V
    .locals 0
    .param p1, "circularRingWidth"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->circularRingWidth:I

    .line 118
    return-void
.end method

.method public setCloseButtonBottomMargin(I)V
    .locals 0
    .param p1, "closeButtonBottomMargin"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonBottomMargin:I

    .line 109
    return-void
.end method

.method public setCloseButtonHeight(I)V
    .locals 0
    .param p1, "closeButtonHeight"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonHeight:I

    .line 101
    return-void
.end method

.method public setCloseButtonHidden(Z)V
    .locals 0
    .param p1, "closeButtonHidden"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonHidden:Z

    .line 29
    return-void
.end method

.method public setCloseButtonWidth(I)V
    .locals 0
    .param p1, "closeButtonWidth"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->closeButtonWidth:I

    .line 93
    return-void
.end method

.method public setHeadHeight(I)V
    .locals 0
    .param p1, "headHeight"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headHeight:I

    .line 49
    return-void
.end method

.method public setHeadHorizontalSpacing(I)V
    .locals 0
    .param p1, "headHorizontalSpacing"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headHorizontalSpacing:I

    .line 65
    return-void
.end method

.method public setHeadVerticalSpacing(I)V
    .locals 0
    .param p1, "headVerticalSpacing"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headVerticalSpacing:I

    .line 73
    return-void
.end method

.method public setHeadWidth(I)V
    .locals 0
    .param p1, "headWidth"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->headWidth:I

    .line 57
    return-void
.end method

.method public setInitialPosition(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "initialPosition"    # Landroid/graphics/Point;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->initialPosition:Landroid/graphics/Point;

    .line 81
    return-void
.end method

.method public setMaxChatHeads(I)V
    .locals 0
    .param p1, "maxChatHeads"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->maxChatHeads:I

    .line 41
    return-void
.end method
