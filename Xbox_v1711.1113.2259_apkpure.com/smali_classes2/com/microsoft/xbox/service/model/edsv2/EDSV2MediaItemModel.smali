.class public abstract Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "EDSV2MediaItemModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final MAX_DETAIL_MODELS:I = 0xa

.field private static alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static bigCatProductIdToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;",
            ">;"
        }
    .end annotation
.end field

.field private static identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;",
            ">;"
        }
    .end annotation
.end field

.field private static partnerMediaIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;",
            ">;"
        }
    .end annotation
.end field

.field private static tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;",
            ">;"
        }
    .end annotation
.end field

.field private static tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;",
            ">;"
        }
    .end annotation
.end field

.field private static titleIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static zuneDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;


# instance fields
.field private shouldLoadFullDetail:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 46
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->bigCatProductIdToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->partnerMediaIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->titleIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    return-void
.end method

.method private static final addModelToCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;)V
    .locals 6
    .param p0, "model"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .prologue
    const-wide/16 v4, 0x0

    .line 254
    const-string v1, "At least one parameter must be valid."

    .line 255
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 254
    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 260
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v0

    const-wide/32 v2, 0x5848085b

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 261
    sput-object p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->zuneDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .line 284
    :cond_1
    :goto_1
    return-void

    .line 258
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 262
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 263
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 264
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 267
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 269
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 271
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 272
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 273
    instance-of v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 275
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v1

    check-cast p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .end local p0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 277
    .restart local p0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 278
    instance-of v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    if-eqz v0, :cond_7

    move-object v0, p0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    if-nez v0, :cond_7

    .line 279
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    check-cast p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .end local p0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 281
    .restart local p0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    :cond_7
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method protected static final createModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    .locals 7
    .param p0, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v6, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    .line 197
    .local v0, "mediaType":I
    sparse-switch v0, :sswitch_data_0

    .line 243
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-direct {v1, p0, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    .line 246
    :goto_0
    return-object v1

    .line 208
    :sswitch_0
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 217
    :sswitch_1
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 220
    :sswitch_2
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 222
    :sswitch_3
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 224
    :sswitch_4
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 227
    :sswitch_5
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 229
    :sswitch_6
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 233
    :sswitch_7
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 235
    :sswitch_8
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 237
    :sswitch_9
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 246
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    invoke-direct {v1, p0, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    goto :goto_0

    .line 248
    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Media type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
        0x12 -> :sswitch_1
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x22 -> :sswitch_1
        0x25 -> :sswitch_0
        0x39 -> :sswitch_0
        0x3a -> :sswitch_0
        0x3d -> :sswitch_7
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_5
        0x3ec -> :sswitch_8
        0x3ed -> :sswitch_9
        0x3ee -> :sswitch_3
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_2
        0x3f1 -> :sswitch_6
        0x2328 -> :sswitch_7
        0x2329 -> :sswitch_0
        0x232a -> :sswitch_0
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
        0x232e -> :sswitch_7
    .end sparse-switch
.end method

.method protected static final findModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    .locals 10
    .param p0, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const-wide/16 v8, 0x0

    .line 139
    const-string v5, "At least one parameter must be valid."

    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v6

    cmp-long v4, v6, v8

    if-lez v4, :cond_2

    :cond_0
    const/4 v4, 0x1

    .line 139
    :goto_0
    invoke-static {v5, v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 145
    const/4 v2, 0x0

    .line 148
    .local v2, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v4

    const-wide/32 v6, 0x5848085b

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    const/16 v5, 0x3d

    if-ne v4, v5, :cond_3

    .line 149
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->zuneDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    :cond_1
    :goto_1
    move-object v4, v2

    .line 191
    :goto_2
    return-object v4

    .line 143
    .end local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 150
    .restart local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 153
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModelFromBigCatProductIdWithType(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    goto :goto_1

    .line 154
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 155
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    .local v0, "bigCatId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModelFromBigCatProductIdWithType(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    goto :goto_1

    .line 159
    :cond_5
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .restart local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    goto :goto_1

    .line 161
    .end local v0    # "bigCatId":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 163
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->partnerMediaIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 164
    .local v1, "mediaCanonicalId":Ljava/lang/String;
    if-eqz v1, :cond_7

    .line 165
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .restart local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    goto :goto_1

    .line 169
    :cond_7
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 170
    .local v3, "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-eqz v3, :cond_1

    .line 171
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v4

    goto :goto_2

    .line 174
    .end local v1    # "mediaCanonicalId":Ljava/lang/String;
    .end local v3    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    :cond_8
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    .line 176
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->titleIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 177
    .restart local v0    # "bigCatId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 178
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModelFromBigCatProductIdWithType(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    goto/16 :goto_1

    .line 181
    :cond_9
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 182
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .restart local v2    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    goto/16 :goto_1

    .line 184
    :cond_a
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 185
    .restart local v3    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-eqz v3, :cond_1

    .line 186
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;->getInternalModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v4

    goto/16 :goto_2
.end method

.method public static final getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    .locals 8
    .param p0, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;",
            ">(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    const-string v2, "At least one parameter must be valid."

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 95
    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 101
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->findModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .line 103
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    if-nez v0, :cond_1

    .line 104
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->createModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->addModelToCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;)V

    .line 108
    :cond_1
    return-object v0

    .line 99
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getModelFromBigCatProductIdWithType(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    .locals 2
    .param p0, "bigCatProductId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "bigCatProductType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 381
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 382
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 389
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .line 390
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v1

    if-ne v1, p1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    if-ne p1, v1, :cond_1

    .line 391
    :cond_0
    const/4 v0, 0x0

    .line 392
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 395
    :cond_1
    return-object v0
.end method

.method public static getNowPlayingModel(JLjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    .locals 6
    .param p0, "titleId"    # J
    .param p2, "partnerMediaId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 112
    const-string v3, "At least one parameter must be valid."

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    cmp-long v2, p0, v4

    if-lez v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 114
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 115
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0, p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 116
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setPartnerMediaId(Ljava/lang/String;)V

    .line 118
    const/4 v1, 0x0

    .line 119
    .local v1, "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 120
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 122
    .restart local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-nez v1, :cond_1

    .line 123
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .end local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->findModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {v1, v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    .line 124
    .restart local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v2, p2, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 135
    :cond_1
    :goto_1
    return-object v1

    .line 112
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 126
    .restart local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    :cond_3
    cmp-long v2, p0, v4

    if-lez v2, :cond_1

    .line 127
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .line 129
    .restart local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    if-nez v1, :cond_1

    .line 130
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;

    .end local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->findModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {v1, v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    .line 131
    .restart local v1    # "nowPlayingModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2NowPlayingDetailModel;
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static getXboxMusicCanonicalId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 369
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getZuneModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .line 371
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 374
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getZuneCanonicalId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 358
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getZuneModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .line 360
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 363
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getZuneModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    .locals 4

    .prologue
    .line 348
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>()V

    .line 349
    .local v1, "zuneItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    const-wide/32 v2, 0x5848085b

    iput-wide v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    .line 350
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setMediaItemTypeFromInt(I)V

    .line 352
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    .line 354
    .local v0, "model":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
    return-object v0
.end method

.method public static final reset()V
    .locals 2

    .prologue
    .line 332
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 334
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 335
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 336
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->partnerMediaIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 337
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->titleIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 338
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 339
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 340
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 342
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->zuneDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->zuneDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    .line 345
    :cond_0
    return-void

    .line 332
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static final updateModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;Ljava/lang/String;)V
    .locals 8
    .param p0, "model"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "partnerMediaId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 287
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_0
    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 290
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "bigCatProductId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "legacyId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 297
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    move-object v1, v2

    .line 300
    .local v1, "identifier":Ljava/lang/String;
    :goto_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 301
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempPartnerMediaIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    .line 302
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->shouldLoadFullDetail:Z

    .line 305
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 306
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToNowPlayingModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    .line 309
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 310
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->tempTitleIdToModelMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    .line 313
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    .line 314
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v3, v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 318
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 319
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 320
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->identifierToModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 323
    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 324
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->partnerMediaIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, p1, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 329
    :cond_5
    :goto_2
    return-void

    .line 288
    .end local v0    # "bigCatProductId":Ljava/lang/String;
    .end local v1    # "identifier":Ljava/lang/String;
    .end local v2    # "legacyId":Ljava/lang/String;
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 297
    .restart local v0    # "bigCatProductId":Ljava/lang/String;
    .restart local v2    # "legacyId":Ljava/lang/String;
    :cond_7
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->alternateIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v1, v3

    goto/16 :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_1

    .line 325
    .restart local v1    # "identifier":Ljava/lang/String;
    :cond_9
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 326
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->titleIdToIdentifierMap:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public drainShouldLoadFullDetail()Z
    .locals 2

    .prologue
    .line 83
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->shouldLoadFullDetail:Z

    .line 84
    .local v0, "rv":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->shouldLoadFullDetail:Z

    .line 85
    return v0
.end method

.method public abstract getBackgroundImageUrl()Ljava/lang/String;
.end method

.method public abstract getBigCatProductId()Ljava/lang/String;
.end method

.method public getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->Non_BigCatProduct:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    return-object v0
.end method

.method public abstract getCanonicalId()Ljava/lang/String;
.end method

.method public abstract getPartnerMediaId()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getTitleId()J
.end method

.method public abstract load(Z)V
.end method
