.class Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter$1;
.super Ljava/lang/Object;
.source "ArtistDetailAlbumsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .line 36
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->NavigateToAlbumDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;)V

    .line 37
    return-void
.end method
