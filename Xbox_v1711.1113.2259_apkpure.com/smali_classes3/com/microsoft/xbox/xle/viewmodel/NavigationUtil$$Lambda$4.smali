.class final synthetic Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final arg$1:Ljava/lang/Runnable;

.field private final arg$2:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

.field private final arg$3:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final arg$4:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

.field private final arg$5:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field private final arg$6:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$1:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$2:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$3:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$4:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$5:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$6:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    return-void
.end method

.method public static lambdaFactory$(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)Landroid/view/View$OnClickListener;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;-><init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$1:Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$2:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$3:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$4:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$5:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$$Lambda$4;->arg$6:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->lambda$attachItemListeners$3(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Landroid/view/View;)V

    return-void
.end method
