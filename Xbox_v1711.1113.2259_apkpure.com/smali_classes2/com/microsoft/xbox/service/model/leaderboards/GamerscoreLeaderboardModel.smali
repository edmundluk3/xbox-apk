.class public Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "GamerscoreLeaderboardModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;,
        Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;,
        Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final LEADERBOARDS_SERVICE:Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

.field private static final LEADERBOARD_CACHE:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;",
            ">;"
        }
    .end annotation
.end field

.field public static final LEADERBOARD_URI_KEY:Ljava/lang/String; = "leaderboardUri"

.field private static final LEADERBOARD_XUIDS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_LEADERBOARD_COUNT:I = 0x3

.field private static final MAX_SERVICE_RETRIES:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private continuationToken:Ljava/lang/Long;

.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final focusData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingData:Z

.field private isLoadingFocusData:Z

.field private final month:Ljava/util/Calendar;

.field private serverInstanceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->TAG:Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getLeaderboardsService()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARDS_SERVICE:Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    .line 53
    new-instance v0, Landroid/util/LruCache;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Ljava/util/Calendar;)V
    .locals 2
    .param p1, "month"    # Ljava/util/Calendar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->serverInstanceId:Ljava/lang/String;

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->month:Ljava/util/Calendar;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    .line 100
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->continuationToken:Ljava/lang/Long;

    .line 101
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->serverInstanceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->tryCreateLeaderboard(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->month:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARDS_SERVICE:Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->continuationToken:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->onLeaderboardUriDataLoadComplete(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public static getCurrentLeaderboardMonth()Ljava/util/Calendar;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 169
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    return-object v0
.end method

.method public static getGamerscoreLeaderboardModel(Ljava/util/Calendar;)Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    .locals 4
    .param p0, "month"    # Ljava/util/Calendar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 66
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 68
    invoke-static {p0}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsService;->getMonthDescriptor(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "monthDescriptor":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    monitor-enter v3

    .line 70
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    invoke-virtual {v2, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .line 71
    .local v0, "model":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;-><init>(Ljava/util/Calendar;)V

    .line 73
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    sget-object v2, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    invoke-virtual {v2, v1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_0
    monitor-exit v3

    return-object v0

    .line 77
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getLeaderboardXuids()Ljava/util/Collection;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    sget-object v4, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    monitor-enter v4

    .line 175
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 176
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 177
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 178
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v1

    .line 179
    .local v1, "friends":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 180
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 181
    .local v0, "friend":Lcom/microsoft/xbox/service/model/FollowersData;
    sget-object v5, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    iget-object v6, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    .end local v0    # "friend":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "friends":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 186
    :cond_0
    :try_start_1
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method public static loadComparisonSync(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p0, "xuidA"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "xuidB"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 161
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 162
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 164
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardComparisonRunnable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method private onLeaderboardUriDataLoadComplete(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;>;"
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 228
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;

    iget-object v1, v2, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->postUri:Ljava/lang/String;

    .line 229
    .local v1, "uri":Ljava/lang/String;
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 230
    .local v0, "extra":Landroid/os/Bundle;
    const-string v2, "leaderboardUri"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 232
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboardUri:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v5, v6, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    invoke-direct {v2, v4, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 236
    :goto_1
    return-void

    .end local v0    # "extra":Landroid/os/Bundle;
    .end local v1    # "uri":Ljava/lang/String;
    :cond_0
    move-object v1, v3

    .line 228
    goto :goto_0

    .line 234
    .restart local v0    # "extra":Landroid/os/Bundle;
    .restart local v1    # "uri":Ljava/lang/String;
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboardUri:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v5, v6, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {v2, v4, p0, v3, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_1
.end method

.method public static reset()V
    .locals 2

    .prologue
    .line 87
    sget-object v1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    monitor-enter v1

    .line 88
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_CACHE:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 89
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->resetXuidList()V

    .line 91
    return-void

    .line 89
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static resetXuidList()V
    .locals 2

    .prologue
    .line 81
    sget-object v1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    monitor-enter v1

    .line 82
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 83
    monitor-exit v1

    .line 84
    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private tryCreateLeaderboard(Ljava/lang/String;)Z
    .locals 6
    .param p1, "callingXuid"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 240
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARDS_SERVICE:Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->month:Ljava/util/Calendar;

    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getLeaderboardXuids()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v3, p1, v4, v5}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->createGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Collection;)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;

    move-result-object v0

    .line 241
    .local v0, "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    if-eqz v0, :cond_0

    .line 242
    iget-object v3, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;->instanceId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->serverInstanceId:Ljava/lang/String;

    .line 252
    const/4 v2, 0x1

    .end local v0    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    :goto_0
    return v2

    .line 244
    .restart local v0    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to create leaderboard: unexpected service response"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 247
    .end local v0    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    :catch_0
    move-exception v1

    .line 248
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to create leaderboard"

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFocusData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingData:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingFocusData:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasData()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFocusData()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLoadedAllData()Z
    .locals 3

    .prologue
    .line 123
    sget-object v1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    monitor-enter v1

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v2, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->LEADERBOARD_XUIDS:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public loadFocusAsync(ZLjava/lang/String;J)V
    .locals 11
    .param p1, "forceRefresh"    # Z
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "maxItems"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 149
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    :cond_0
    iput-boolean v10, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingFocusData:Z

    .line 151
    const-wide/16 v8, 0x0

    new-instance v7, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v7}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;

    move-object v2, p0

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;-><init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;JLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;)V

    move v3, v10

    move-wide v4, v8

    move-object v8, v1

    invoke-static/range {v3 .. v8}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 153
    :cond_1
    return-void
.end method

.method public loadLeaderboardShareUriAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "meXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "youXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 157
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;

    invoke-direct {v6, p0, p1, p2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;-><init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 158
    return-void
.end method

.method public loadMoreAsync(ZJ)V
    .locals 8
    .param p1, "forceRefresh"    # Z
    .param p2, "maxItems"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 138
    if-eqz p1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 142
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->continuationToken:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 143
    :cond_1
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingData:Z

    .line 144
    const-wide/16 v2, 0x0

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;

    invoke-direct {v6, p0, p2, p3, v4}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;-><init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;JLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 146
    :cond_2
    return-void
.end method

.method public onLeaderboardDataLoaded(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V
    .locals 7
    .param p2, "isFocus"    # Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;>;"
    const/4 v6, 0x0

    .line 192
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 193
    .local v2, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 194
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->getLeaderboard()Ljava/util/List;

    move-result-object v1

    .line 196
    .local v1, "leaderboardEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    if-eqz p2, :cond_1

    .line 198
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 199
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->focusData:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 215
    :goto_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 218
    .end local v1    # "leaderboardEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    :cond_0
    if-eqz p2, :cond_4

    .line 219
    iput-boolean v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingFocusData:Z

    .line 224
    :goto_1
    new-instance v3, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->GamerscoreLeaderboard:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v5, 0x0

    invoke-direct {v3, v4, p0, v5, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 225
    return-void

    .line 201
    .restart local v1    # "leaderboardEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 203
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;

    iget-wide v4, v3, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;->rank:J

    long-to-int v3, v4

    add-int/lit8 v0, v3, -0x1

    .line 204
    .local v0, "firstNewIndex":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_2

    .line 206
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 209
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->data:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-interface {v3, v4, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 212
    .end local v0    # "firstNewIndex":I
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;->getContinuationToken()Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->continuationToken:Ljava/lang/Long;

    goto :goto_0

    .line 221
    .end local v1    # "leaderboardEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardEntry;>;"
    :cond_4
    iput-boolean v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->isLoadingData:Z

    goto :goto_1
.end method

.method public refreshServerInstance()V
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->serverInstanceId:Ljava/lang/String;

    .line 134
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->continuationToken:Ljava/lang/Long;

    .line 135
    return-void
.end method
