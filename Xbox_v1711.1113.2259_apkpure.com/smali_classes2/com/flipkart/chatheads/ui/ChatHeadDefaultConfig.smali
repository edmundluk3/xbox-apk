.class public Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;
.super Lcom/flipkart/chatheads/ui/ChatHeadConfig;
.source "ChatHeadDefaultConfig.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v6, 0x3e

    const/16 v5, 0x3d

    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 12
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;-><init>()V

    .line 13
    const/16 v0, 0x38

    .line 14
    .local v0, "diameter":I
    invoke-static {p1, v0}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setHeadHeight(I)V

    .line 15
    invoke-static {p1, v0}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setHeadWidth(I)V

    .line 16
    const/16 v1, 0xa

    invoke-static {p1, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setHeadHorizontalSpacing(I)V

    .line 17
    invoke-static {p1, v4}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setHeadVerticalSpacing(I)V

    .line 18
    new-instance v1, Landroid/graphics/Point;

    invoke-static {p1, v3}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setInitialPosition(Landroid/graphics/Point;)V

    .line 19
    invoke-virtual {p0, v3}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCloseButtonHidden(Z)V

    .line 20
    invoke-static {p1, v6}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCloseButtonWidth(I)V

    .line 21
    invoke-static {p1, v6}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCloseButtonHeight(I)V

    .line 22
    const/16 v1, 0x32

    invoke-static {p1, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCloseButtonBottomMargin(I)V

    .line 23
    invoke-static {p1, v5}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCircularRingWidth(I)V

    .line 24
    invoke-static {p1, v5}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setCircularRingHeight(I)V

    .line 25
    invoke-virtual {p0, v4}, Lcom/flipkart/chatheads/ui/ChatHeadDefaultConfig;->setMaxChatHeads(I)V

    .line 26
    return-void
.end method


# virtual methods
.method public getCircularFanOutRadius(II)I
    .locals 2
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 30
    int-to-float v0, p1

    const/high16 v1, 0x40200000    # 2.5f

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
