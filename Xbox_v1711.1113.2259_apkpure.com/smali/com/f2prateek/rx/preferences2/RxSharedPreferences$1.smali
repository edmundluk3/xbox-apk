.class Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;
.super Ljava/lang/Object;
.source "RxSharedPreferences.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/f2prateek/rx/preferences2/RxSharedPreferences;-><init>(Landroid/content/SharedPreferences;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field final synthetic val$preferences:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "this$0"    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;->this$0:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iput-object p2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;->val$preferences:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "emitter":Lio/reactivex/ObservableEmitter;, "Lio/reactivex/ObservableEmitter<Ljava/lang/String;>;"
    new-instance v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1$1;

    invoke-direct {v0, p0, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1$1;-><init>(Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;Lio/reactivex/ObservableEmitter;)V

    .line 47
    .local v0, "listener":Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    new-instance v1, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1$2;

    invoke-direct {v1, p0, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1$2;-><init>(Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 53
    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;->val$preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 54
    return-void
.end method
