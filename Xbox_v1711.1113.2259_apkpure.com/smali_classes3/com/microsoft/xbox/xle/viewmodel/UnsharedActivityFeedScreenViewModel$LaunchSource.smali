.class public final enum Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
.super Ljava/lang/Enum;
.source "UnsharedActivityFeedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LaunchSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

.field public static final enum ClubFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

.field public static final enum Message:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

.field public static final enum MyFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

.field public static final enum Share:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    const-string v1, "Share"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->Share:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 123
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    const-string v1, "Message"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->Message:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 124
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    const-string v1, "MyFeed"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->MyFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 125
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    const-string v1, "ClubFeed"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->ClubFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 121
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->Share:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->Message:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->MyFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->ClubFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 121
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    return-object v0
.end method
