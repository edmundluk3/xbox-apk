.class public final enum Lcom/microsoft/xbox/smartglass/SGError;
.super Ljava/lang/Enum;
.source "SGError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/SGError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Abort:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum AccessDenied:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum AlreadyConnected:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum AuthRequired:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum BigEndianStringNotTerminated:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum CancelShutdown:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Cancelled:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum ChannelAlreadyStarted:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum ChannelFailedToStart:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum ConsoleDisconnecting:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum ConsoleNotResponding:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum CryptoInvalidArguments:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum CryptoInvalidCertificate:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum CryptoInvalidSignature:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum CryptoUnexpectedError:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum ExpiredConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Fail:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum FailedToJoin:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum FailedToStartThread:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum HttpError:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum HttpStatus:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidArgument:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidData:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidHandle:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidPointer:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum InvalidState:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum JniCallError:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum JsonLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum MaximumChannelsStarted:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum MessageExpired:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum MessageLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NoInterface:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NoMoreCapacity:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NotConnected:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NotFound:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NotImplemented:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum NotInitialized:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum OutOfMemory:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Pending:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum SocketError:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Success:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum SuccessFalse:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum TimedOut:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum TimedOutConnect:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum TimedOutPresence:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Unexpected:Lcom/microsoft/xbox/smartglass/SGError;

.field public static final enum Unknown:Lcom/microsoft/xbox/smartglass/SGError;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/SGError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Success"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "SuccessFalse"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->SuccessFalse:Lcom/microsoft/xbox/smartglass/SGError;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Abort"

    const v4, -0x7ffffffc

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Abort:Lcom/microsoft/xbox/smartglass/SGError;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "AccessDenied"

    const v4, -0x7ffffffb

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->AccessDenied:Lcom/microsoft/xbox/smartglass/SGError;

    .line 21
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Fail"

    const v4, -0x7ffffffa

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    .line 23
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidHandle"

    const/4 v4, 0x5

    const v5, -0x7ffffff9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidHandle:Lcom/microsoft/xbox/smartglass/SGError;

    .line 25
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidArgument"

    const/4 v4, 0x6

    const v5, -0x7ffffff8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidArgument:Lcom/microsoft/xbox/smartglass/SGError;

    .line 27
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NoInterface"

    const/4 v4, 0x7

    const v5, -0x7ffffff7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NoInterface:Lcom/microsoft/xbox/smartglass/SGError;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NotImplemented"

    const/16 v4, 0x8

    const v5, -0x7ffffff6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NotImplemented:Lcom/microsoft/xbox/smartglass/SGError;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "OutOfMemory"

    const/16 v4, 0x9

    const v5, -0x7ffffff5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->OutOfMemory:Lcom/microsoft/xbox/smartglass/SGError;

    .line 33
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidPointer"

    const/16 v4, 0xa

    const v5, -0x7ffffff4

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidPointer:Lcom/microsoft/xbox/smartglass/SGError;

    .line 35
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Unexpected"

    const/16 v4, 0xb

    const v5, -0x7ffffff3

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Unexpected:Lcom/microsoft/xbox/smartglass/SGError;

    .line 37
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Pending"

    const/16 v4, 0xc

    const v5, -0x7ffffff2

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Pending:Lcom/microsoft/xbox/smartglass/SGError;

    .line 39
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidData"

    const/16 v4, 0xd

    const v5, -0x7ffffff1

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidData:Lcom/microsoft/xbox/smartglass/SGError;

    .line 41
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Cancelled"

    const/16 v4, 0xe

    const v5, -0x7ffffff0

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Cancelled:Lcom/microsoft/xbox/smartglass/SGError;

    .line 43
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidState"

    const/16 v4, 0xf

    const v5, -0x7fffffef

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidState:Lcom/microsoft/xbox/smartglass/SGError;

    .line 45
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NotFound"

    const/16 v4, 0x10

    const v5, -0x7fffffee

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NotFound:Lcom/microsoft/xbox/smartglass/SGError;

    .line 47
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NoMoreCapacity"

    const/16 v4, 0x11

    const v5, -0x7fffffed

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NoMoreCapacity:Lcom/microsoft/xbox/smartglass/SGError;

    .line 49
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "FailedToStartThread"

    const/16 v4, 0x12

    const v5, -0x7fffffec

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->FailedToStartThread:Lcom/microsoft/xbox/smartglass/SGError;

    .line 51
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "MessageExpired"

    const/16 v4, 0x13

    const v5, -0x7fffffeb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->MessageExpired:Lcom/microsoft/xbox/smartglass/SGError;

    .line 53
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "TimedOut"

    const/16 v4, 0x14

    const v5, -0x7fffffea

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->TimedOut:Lcom/microsoft/xbox/smartglass/SGError;

    .line 55
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NotInitialized"

    const/16 v4, 0x15

    const v5, -0x7fffffe9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NotInitialized:Lcom/microsoft/xbox/smartglass/SGError;

    .line 57
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "JsonLengthExceeded"

    const/16 v4, 0x16

    const v5, -0x7fffffe8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->JsonLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

    .line 59
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "MessageLengthExceeded"

    const/16 v4, 0x17

    const v5, -0x7fffffe7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->MessageLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

    .line 61
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "InvalidConfiguration"

    const/16 v4, 0x18

    const v5, -0x7fffffe6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->InvalidConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

    .line 63
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "ExpiredConfiguration"

    const/16 v4, 0x19

    const v5, -0x7fffffe5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->ExpiredConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

    .line 65
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "JniCallError"

    const/16 v4, 0x1a

    const v5, -0x7fffffe4

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->JniCallError:Lcom/microsoft/xbox/smartglass/SGError;

    .line 67
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "AuthRequired"

    const/16 v4, 0x1b

    const v5, -0x7fffffe3

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->AuthRequired:Lcom/microsoft/xbox/smartglass/SGError;

    .line 69
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "TimedOutPresence"

    const/16 v4, 0x1c

    const v5, -0x7fffffe2

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->TimedOutPresence:Lcom/microsoft/xbox/smartglass/SGError;

    .line 71
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "TimedOutConnect"

    const/16 v4, 0x1d

    const v5, -0x7fffffe1

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->TimedOutConnect:Lcom/microsoft/xbox/smartglass/SGError;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "SocketError"

    const/16 v4, 0x1e

    const v5, -0x7ffeffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->SocketError:Lcom/microsoft/xbox/smartglass/SGError;

    .line 77
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "HttpError"

    const/16 v4, 0x1f

    const v5, -0x7ffdffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->HttpError:Lcom/microsoft/xbox/smartglass/SGError;

    .line 79
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "CancelShutdown"

    const/16 v4, 0x20

    const v5, -0x7ffdfffe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->CancelShutdown:Lcom/microsoft/xbox/smartglass/SGError;

    .line 81
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "HttpStatus"

    const/16 v4, 0x21

    const v5, -0x7ffdfffd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->HttpStatus:Lcom/microsoft/xbox/smartglass/SGError;

    .line 84
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "CryptoUnexpectedError"

    const/16 v4, 0x22

    const v5, -0x7ffcffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->CryptoUnexpectedError:Lcom/microsoft/xbox/smartglass/SGError;

    .line 86
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "CryptoInvalidArguments"

    const/16 v4, 0x23

    const v5, -0x7ffcfffe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidArguments:Lcom/microsoft/xbox/smartglass/SGError;

    .line 88
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "CryptoInvalidSignature"

    const/16 v4, 0x24

    const v5, -0x7ffcfffd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidSignature:Lcom/microsoft/xbox/smartglass/SGError;

    .line 90
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "CryptoInvalidCertificate"

    const/16 v4, 0x25

    const v5, -0x7ffcfffc

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidCertificate:Lcom/microsoft/xbox/smartglass/SGError;

    .line 93
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "FailedToJoin"

    const/16 v4, 0x26

    const v5, -0x7ff9ffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->FailedToJoin:Lcom/microsoft/xbox/smartglass/SGError;

    .line 95
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "AlreadyConnected"

    const/16 v4, 0x27

    const v5, -0x7ff9fffe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->AlreadyConnected:Lcom/microsoft/xbox/smartglass/SGError;

    .line 97
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "NotConnected"

    const/16 v4, 0x28

    const v5, -0x7ff9fffd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->NotConnected:Lcom/microsoft/xbox/smartglass/SGError;

    .line 99
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "ConsoleNotResponding"

    const/16 v4, 0x29

    const v5, -0x7ff9fffc

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->ConsoleNotResponding:Lcom/microsoft/xbox/smartglass/SGError;

    .line 101
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "ConsoleDisconnecting"

    const/16 v4, 0x2a

    const v5, -0x7ff9fffb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->ConsoleDisconnecting:Lcom/microsoft/xbox/smartglass/SGError;

    .line 104
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "BigEndianStringNotTerminated"

    const/16 v4, 0x2b

    const v5, -0x7ff8ffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->BigEndianStringNotTerminated:Lcom/microsoft/xbox/smartglass/SGError;

    .line 107
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "ChannelAlreadyStarted"

    const/16 v4, 0x2c

    const v5, -0x7ff7ffff

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->ChannelAlreadyStarted:Lcom/microsoft/xbox/smartglass/SGError;

    .line 109
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "ChannelFailedToStart"

    const/16 v4, 0x2d

    const v5, -0x7ff7fffe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->ChannelFailedToStart:Lcom/microsoft/xbox/smartglass/SGError;

    .line 111
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "MaximumChannelsStarted"

    const/16 v4, 0x2e

    const v5, -0x7ff7fffd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->MaximumChannelsStarted:Lcom/microsoft/xbox/smartglass/SGError;

    .line 114
    new-instance v2, Lcom/microsoft/xbox/smartglass/SGError;

    const-string v3, "Unknown"

    const/16 v4, 0x2f

    const v5, -0x70000001

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/SGError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->Unknown:Lcom/microsoft/xbox/smartglass/SGError;

    .line 10
    const/16 v2, 0x30

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/SGError;

    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->SuccessFalse:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->Abort:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->AccessDenied:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidHandle:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidArgument:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NoInterface:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NotImplemented:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->OutOfMemory:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidPointer:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->Unexpected:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->Pending:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidData:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->Cancelled:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidState:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NotFound:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NoMoreCapacity:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->FailedToStartThread:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->MessageExpired:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->TimedOut:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NotInitialized:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->JsonLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->MessageLengthExceeded:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->InvalidConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->ExpiredConfiguration:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->JniCallError:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->AuthRequired:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->TimedOutPresence:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->TimedOutConnect:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->SocketError:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->HttpError:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->CancelShutdown:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->HttpStatus:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->CryptoUnexpectedError:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidArguments:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x24

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidSignature:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x25

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidCertificate:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x26

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->FailedToJoin:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x27

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->AlreadyConnected:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x28

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->NotConnected:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x29

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->ConsoleNotResponding:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->ConsoleDisconnecting:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->BigEndianStringNotTerminated:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->ChannelAlreadyStarted:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->ChannelFailedToStart:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->MaximumChannelsStarted:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->Unknown:Lcom/microsoft/xbox/smartglass/SGError;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->$VALUES:[Lcom/microsoft/xbox/smartglass/SGError;

    .line 117
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SGError;->_lookup:Landroid/util/SparseArray;

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGError;->values()[Lcom/microsoft/xbox/smartglass/SGError;

    move-result-object v2

    array-length v3, v2

    .local v0, "error":Lcom/microsoft/xbox/smartglass/SGError;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 121
    sget-object v4, Lcom/microsoft/xbox/smartglass/SGError;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/SGError;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    iput p3, p0, Lcom/microsoft/xbox/smartglass/SGError;->_value:I

    .line 141
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/SGError;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 131
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SGError;

    .line 132
    .local v0, "error":Lcom/microsoft/xbox/smartglass/SGError;
    if-nez v0, :cond_0

    .line 133
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGError;->Unknown:Lcom/microsoft/xbox/smartglass/SGError;

    .line 136
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/SGError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/SGError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SGError;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/SGError;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/SGError;->$VALUES:[Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/SGError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/SGError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/microsoft/xbox/smartglass/SGError;->_value:I

    return v0
.end method
