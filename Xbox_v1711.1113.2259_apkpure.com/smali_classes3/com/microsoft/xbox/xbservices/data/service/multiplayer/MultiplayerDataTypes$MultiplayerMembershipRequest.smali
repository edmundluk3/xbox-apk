.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerMembershipRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1556
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/util/List;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;"
        }
    .end annotation

    .prologue
    .line 1551
    .local p0, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 1552
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public abstract xuids()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
