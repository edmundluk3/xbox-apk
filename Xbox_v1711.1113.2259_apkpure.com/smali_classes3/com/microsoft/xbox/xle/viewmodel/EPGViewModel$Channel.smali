.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
.super Ljava/lang/Object;
.source "EPGViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Channel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;,
        Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    }
.end annotation


# virtual methods
.method public abstract addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
.end method

.method public abstract getChannelGuid()Ljava/lang/String;
.end method

.method public abstract getColors()[I
.end method

.method public abstract getData()Ljava/lang/Object;
.end method

.method public abstract getHDEquivalent()Ljava/lang/String;
.end method

.method public abstract getHeadendId()Ljava/lang/String;
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNumber()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
.end method

.method public abstract isAdult()Z
.end method

.method public abstract isFavorite()Z
.end method

.method public abstract isHD()Z
.end method

.method public abstract isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
.end method

.method public abstract removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
.end method

.method public abstract toggleFavorite()V
.end method

.method public abstract tuneToChannel()V
.end method
