.class Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;
.super Ljava/lang/Object;
.source "TitleLeaderboardScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->trackShowFriendHeroProfile(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 97
    :cond_0
    return-void
.end method
